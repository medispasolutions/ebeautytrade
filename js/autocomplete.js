
$(document).ready(function() {
var base_url = $('#baseurl').val();
if($("#searchProducts").length>0){
$( "#searchProducts" ).autocomplete({
//source: siteurl+"/establishment/restaurant",
minLength:1,
close:function(event,ui){
var baseurl=$('#baseurl').val();
var name = $('input[name="title"').val();
$('.product_stock').html('');
$('#loader_1').html('<div class="loader"></div>');
var id_brand = $('select[name="brands"').val();
var arr = name.split('_');
var id = arr [0];
var val = arr [1];
$('input[name="title"').val(val);
$.post(baseurl+'back_office/inventory/getStock',{id:id,id_brand:id_brand},function(data){
var Parsedata = JSON.parse(data);
$('.product_stock').html(Parsedata.html);
$('#loader_1').html('');
});
},
source: function( request, response ) {
var id_brand = $('select[name="brands"').val();
$.ajax({
  url: base_url+"back_office/inventory/getDeliveryItems",
  dataType: "json",
  data: {term: request.term,id_brand:id_brand},
  type:"POST",
  success: function(data) {
     response($.map(data, function(item) {
	
     return {
      label: item.label,
	  value: item.value,
	   id: item.id,
      };
    }));
   }
   
  });
},
});}

if($("#searchRelatedProducts").length>0){
$( "#searchRelatedProducts" ).autocomplete({
//source: siteurl+"/establishment/restaurant",
minLength:1,
close:function(event,ui){
var baseurl=$('#baseurl').val();
var name = $('input[name="related_product"').val();
$('#loader_1').html('<div class="loader"></div>');
var arr = name.split('_');
var id = arr [0];
var val = arr [1];
$('input[name="related_product"').val(val);
$('#id_related_product').val(id);
},
source: function( request, response ) {

$('#loader_1').html('<div class="loader"></div>');
$.ajax({
  url: base_url+"back_office/products/getRelatedProducts",
  dataType: "json",
  data: {term: request.term,id_brand:0},
  type:"POST",
  success: function(data) {$('#loader_1').html('');
     response($.map(data, function(item) {
		 
	
     return {
      label: item.label,
	  value: item.value,
	   id: item.id,
      };
    }));
   }
   
  });
},
});}

if($("#address_autocomplete").length>0){
$( "#address_autocomplete" ).autocomplete({
//source: siteurl+"/establishment/restaurant",
minLength:1,
close:function(event,ui){
	
var baseurl=$('#baseurl').val();
var name = $('input[name="address"').val();


var arr = name.split('_');
var id = arr [0];
var val = arr [1];
$('input[name="address"').val(val);
$('input[name="id_address"').val(id);
},
source: function( request, response ) {
var id_brand = $('select[name="brands"').val();
$.ajax({
  url: base_url+"back_office/orders/getAddresses",
  dataType: "json",
  data: {term: request.term,id_brand:id_brand},
  type:"POST",
  success: function(data) {
     response($.map(data, function(item) {
	
     return {
      label: item.label,
	  value: item.value,
	   id: item.id,
      };
    }));
   }
   
  });
},
});}

if($("#searchProducts2").length>0){
$( "#searchProducts2" ).autocomplete({
//source: siteurl+"/establishment/restaurant",
minLength:1,
close:function(event,ui){
	
var baseurl=$('#baseurl').val();
var name = $('input[name="product"').val();


var arr = name.split('_');
var id = arr [0];
var val = arr [1];
$('input[name="product"').val(val);
$('input[name="id_products"').val(id);

},
source: function( request, response ) {
$('#loader_1').html('<div class="loader"></div>');
var id_brand = "";
var id_supplier = $('select[name="supplier"').val();
$.ajax({
  url: base_url+"back_office/brand_certified_training/getProducts",
  dataType: "json",
  data: {term: request.term,id_brand:id_brand,id_supplier:id_supplier},
  type:"POST",
  success: function(data) {
	  $('#loader_1').html('');
     response($.map(data, function(item) {
	
     return {
      label: item.label,
	  value: item.value,
	   id: item.id,
      };
    }));
   }
   
  });
},
});}

if($("#order_id").length>0){
$( "#order_id" ).autocomplete({
//source: siteurl+"/establishment/restaurant",
minLength:1,
close:function(event,ui){
var baseurl=$('#baseurl').val();
var name = $('input[name="order_id"').val();
$('#loader_1').html('<div class="loader"></div>');
var id_brand = $('select[name="brands"').val();
var arr = name.split('_');
var id = arr [0];
var val = arr [1];
$('input[name="order_id"').val(val);
$.post(baseurl+'back_office/inventory/getOrdersLineItems',{id:id},function(data){
var Parsedata = JSON.parse(data);
$('.product_stock').html(Parsedata.html);
$('#loader_1').html('');
	checklist();
});
},
source: function( request, response ) {

$.ajax({
  url: base_url+"back_office/inventory/getOrders",
  dataType: "json",
  data: {term: request.term},
  type:"POST",
  success: function(data) {
     response($.map(data, function(item) {
	
     return {
      label: item.label,
	  value: item.value,
	   id: item.id,
      };
    }));
   }
   
  });
},
});}

});