$.fn.dowformvalidate = function( options ) {
	// Establish our default settings
	var settings = $.extend({
		formresult: ".FormResult",
		formerror: ".form-error",
		loader: "Loading...",
		validateonly: false,
		withScrolling: true,
		updateonly: false
	}, options);
	
	var FormObj = $(this);
	var formresult = settings.formresult;
	var formerror = settings.formerror;
	var upOnly = settings.updateonly;
var withScrolling = settings.withScrolling;
	FormObj.submit(function(event){
		  event.preventDefault();
		  var SubmitButton = FormObj.find("input[type='submit']");
		  SubmitButton.attr('disabled','disabled');
		  var formAction = FormObj.attr('action');
		   FormObj.find('#loader-bx').html('<div class="loader"></div>');
		    FormObj.find('#loader-bx').show();
		FormObj.find( formresult ).html('');
		var formData = new FormData($(this)[0]);
		  $(settings.formerror).html("");
		  $.ajax({
        url: FormObj.attr('action'),
        type: 'POST',
        data: formData,
        success: function (data) {
		  $(settings.formresult).html("");
		  var baseurl=$('#baseurl').val();
		   Parsedata = JSON.parse(data);
		   SubmitButton.attr('disabled',null);
		   if(Parsedata.result == 0) {

			    if(Parsedata.errors) {
					var x=[];
					 var errors = Parsedata.errors;
					 	  
				j=0;
			   $.each(errors, function(i, val){
				   
				 if(i=='expire_date'){$('#reactive_account').show();}
				 FormObj.find( '#'+i+'-error' ).html('<span class="inner-form-error"><i class="error-icon"></i>'+val+'</span>');
				    FormObj.find( 'input[name="'+i+'"], textarea[name="'+i+'"], select[name="'+i+'"]' ).addClass('vError');
				   });
			
				}
				if(Parsedata.message!='' || Parsedata.message!='Error!' ){
					 
					  FormObj.find( formresult ).html(Parsedata.message);
				 }
			   if(Parsedata.captcha != null) {
			   	FormObj.find( '.captchaImage' ).html(Parsedata.captcha);
			   	FormObj.find( 'input[name="captcha"]' ).val('');
			   }
			   FormObj.find( formresult ).html('');
			   FormObj.find('#loader-bx').html('');
			  var emptyy = '';
			  $(formerror).each(function(){
				  if ($(this).html().trim().length) {
					  if(emptyy == '') {
						  emptyy = $(this).attr('id');
					  }
				  }
			   });
			   if(emptyy != "") {
			   	var anmEror = $('#'+emptyy).offset().top - 100;
				if(withScrolling){
			   	$('html, body').animate({scrollTop : anmEror},1000);}
	 if(Parsedata.message!='' && Parsedata.message!='Error!' ){	
	 	var errorText='<ul class="messages"><li class="error-msg r-fullSide"><ul><li><span>'+Parsedata.message+'</span></li></ul></li></ul>';	  
 			
FormObj.find( formresult ).html(errorText);} }
		   }
		   else {
			 
			if($('.loader_input').length>0){
				$('.loader_input').addClass('loader_success');
				$('td').removeClass('loader_input');
				}
		   //////// //////// //////// //////// //////// //////// //////// ////////

		   if(Parsedata.message!='' && Parsedata.message!='Error!' ){
			  
 		var errorText='<ul class="messages"><li class="success-msg r-fullSide"><ul><li><span>'+Parsedata.message+'</span></li></ul></li></ul>';	
			FormObj.find( formresult ).html(errorText);}
			   if(upOnly) {
				   //
			   }
			   else {
			   if(!settings.validateonly) {

				FormObj.find( 'input[type="text"], input[type="hidden"], input[type="email"], input[type="password"], textarea, select' ).each(function(){
				   var Obj = $(this);
				   if(Obj.attr("type") != "hidden")
				   Obj.val('');	
				   Obj.removeClass('vError'); 
				   Obj.parent('div').find(formerror).html('');	
				   var anmEror = FormObj.find(formresult).offset().top - 100;
			   });
			   }
			     }
			   if(!settings.updateonly) {
			   FormObj.find( 'input[name="captcha"]' ).each(function(){
				   var Obj = $(this);
				   if(Obj.attr("type") != "hidden")
				   Obj.val('');	
				   Obj.removeClass('vError'); 
				   Obj.parent('div').find(formerror).html('');	
				   var anmEror = FormObj.find(formresult).offset().top - 100;
			   });
			   }else{
				  
				 FormObj.find ( 'input[name="captcha"]' ).each(function(){
				 var Obj = $(this);
				 Obj.val('');});
				 
				   if($('.change_pass').length>0){
					    FormObj.find( 'input[type="password"]' ).each(function(){
				 var Obj = $(this);
				 Obj.val('');	
							});
					   }
				   }
		 
			if(Parsedata.redirect_link != null)
			 window.location = Parsedata.redirect_link;
		   }
 FormObj.find('#loader-bx').hide();
 FormObj.find('#loader-bx').html('');
		  },
        cache: false,
        contentType: false,
        processData: false
    });	
        return false;
		 
		  
	 });
	 FormObj.find( 'input[type="text"], input[type="password"], textarea, select' ).focus(function(){
		 var Obj = $(this);
		 Obj.removeClass('vError');
		 Obj.parent('div').find(formerror).html('');		 
	 });
	
	if ( $.isFunction( settings.callback ) ) {
		settings.callback.call( this );
	}
}


