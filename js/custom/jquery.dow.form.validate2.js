$.fn.dowformvalidate2 = function( options ) {
	// Establish our default settings
	var settings = $.extend({
		formresult: ".FormResult",
		formerror: ".form-error",
		loader: "Loading...",
		validateonly: false,
		updateonly: false
	}, options);
	
	var FormObj = $(this);
	var formresult = settings.formresult;
	var formerror = settings.formerror;
	var upOnly = settings.updateonly;

	FormObj.submit(function(event){

		  event.preventDefault();
		  var SubmitButton = FormObj.find("input[type='submit']");
		  SubmitButton.attr('disabled','disabled');
		  var formAction = FormObj.attr('action');
		  FormObj.find('#loader-bx').html('<div class="loader"></div>');
		  FormObj.find('#loader-bx').show();
				  
		FormObj.find( formresult ).html('');
		var formData = new FormData($(this)[0]);
	

		  $(settings.formerror).html("");
	
		  $.ajax({
        url: FormObj.attr('action'),
        type: 'POST',
        data: formData,
        success: function (data) {
			
		  $(settings.formresult).html("");
		  var baseurl=$('#baseurl').val();
		   Parsedata = JSON.parse(data);
		   SubmitButton.attr('disabled',null);
		   if(Parsedata.result == 0) {

			    if(Parsedata.errors) {
					var x=[];
					 var errors = Parsedata.errors;
					 	  
				j=0;
			   $.each(errors, function(i, val){
				   
				 if(i=='expire_date'){$('#reactive_account').show();}
				 FormObj.find( '#'+i+'-error' ).html('<span class="inner-form-error"><i class="error-icon"></i>'+val+'</span>');
				    FormObj.find( 'input[name="'+i+'"], textarea[name="'+i+'"], select[name="'+i+'"]' ).addClass('vError');
				   });
			
				}
				if(Parsedata.message!='' || Parsedata.message!='Error!' ){
					 FormObj.find( formresult ).html(Parsedata.message);
				 }
			   if(Parsedata.captcha != null) {
			   	FormObj.find( '.captchaImage' ).html(Parsedata.captcha);
			   	FormObj.find( 'input[name="captcha"]' ).val('');
			   }
			   FormObj.find( formresult ).html('');
			   FormObj.find('#loader-bx').html('');
			  var emptyy = '';
			  $(formerror).each(function(){
				  if ($(this).html().trim().length) {
					  if(emptyy == '') {
						  emptyy = $(this).attr('id');
					  }
				  }
			   });
			
	if(Parsedata.message!='' ){
 			var errorText='<ul class="messages"><li class="error-msg r-fullSide"><ul><li><span>'+Parsedata.message+'</span></li></ul></li></ul>'; 
			FormObj.find( formresult ).html(errorText);}
			   if(emptyy != "") {
			   	var anmEror = $('#'+emptyy).offset().top - 100;
			   	$('html, body').animate({scrollTop : anmEror},1000);
	 if(Parsedata.message!='' && Parsedata.message!='Error!' ){		  
 			var errorText='<ul class="messages"><li class="error-msg r-fullSide"><ul><li><span>'+Parsedata.message+'</span></li></ul></li></ul>';
FormObj.find( formresult ).html(errorText);} }
		   }
		   else {
			   

		 //////// //////// //////// //////// //////// //////// //////// ////////

if(FormObj.attr('id')=='addToStockOrder'){
$('.order-operation', window.parent.document).css({'display':'inline-block'});
$('#order-details-table2', window.parent.document).html(Parsedata.html);
setTimeout(function(){ window.parent.jQuery.fancybox.close();}, 1000);
}else{
if(FormObj.attr('id')!="insertOrder" && FormObj.attr('id')!="addToReturnStock"){
	
		if($("#brand-"+Parsedata.id_brands).length>0){
		$( Parsedata.html ).appendTo("#brand-"+Parsedata.id_brands);	
			}else{

		var html='<tr><td><table id="brand-'+Parsedata.id_brands+'" class="table table-striped"><tr><th colspan="8" align="center" style="text-align:center;font-size:20px;">'+Parsedata.brand_name+'</th></tr>'+Parsedata.table_head+' '+Parsedata.html+'</table></td></tr>'
		$( html ).appendTo( ".order-details-table" );		
			}
}}

if(FormObj.attr('id')=='addToReturnStock'){
	$('.product_stock').html(Parsedata.html)}
		

		 $('.empty').hide();
		 $('.order-details-table .form-controls').show();
		   if(Parsedata.message!='' && Parsedata.message!='Error!' ){
			  
 			var errorText='<ul class="messages"><li class="success-msg r-fullSide"><ul><li><span>'+Parsedata.message+'</span></li></ul></li></ul>';
			FormObj.find( formresult ).html(errorText);}
			   if(upOnly) {
				   //
			   }
			   else {
				   
				if(FormObj.attr('id')=="insertOrder"){
					$('.order-table-row').remove();
					$('.empty').show();
					$('.order-details-table .form-controls').hide();}
					
				if(FormObj.attr('id')=="addToOrder"){
				$('.product_stock').html('');
				$('#title_bx').hide();
				scroll_To('.order-details-table',100);}
			if($("#delivery_request").length > 0){
						$('#title_bx').show();
						$('#title_bx input').val('');	}
			   if(!settings.validateonly) {
		if($("#delivery_request").length > 0){
}else{
				FormObj.find( 'input[type="text"], input[type="hidden"], input[type="email"], input[type="password"], textarea, select' ).each(function(){
				   var Obj = $(this);
				   if(Obj.attr("type") != "hidden")
				   Obj.val('');	
				   Obj.removeClass('vError'); 
				   Obj.parent('div').find(formerror).html('');	
				   
			   });
			   }}
			     }
			   if(!settings.updateonly) {
			
			   }else{
				  
		
				 }
		 
			if(Parsedata.redirect_link != null)
			 window.location = Parsedata.redirect_link;
		      }
			  
 FormObj.find('#loader-bx').hide();
 FormObj.find('#loader-bx').html('');
 if($('.date').length>0){
	$('.date').datepicker();}	  },
        cache: false,
        contentType: false,
        processData: false
    });	
        return false;
		 
		  
	 });
	 FormObj.find( 'input[type="text"], input[type="password"], textarea, select' ).focus(function(){
		 var Obj = $(this);
		 Obj.removeClass('vError');
		 Obj.parent('div').find(formerror).html('');		 
	 });
	
	if ( $.isFunction( settings.callback ) ) {
		settings.callback.call( this );
	}
}


