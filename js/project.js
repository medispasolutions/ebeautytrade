$(document).ready(function(){
	if($('#UserForm').length > 0) {
		 $('#UserForm').dowformvalidate({
			  parentObj: "fieldset",
			  callback:function(obj1){
				  if(obj1) {
					  var ParseObjData = JSON.parse(obj1.data);
					  if(ParseObjData.reload_data) {
						  $("#"+module).html(ParseObjData.reload_data);
					  }
					  $('.modal-backdrop').click();
				  }
			  }
		  });
	}
	

	
	renderMasterkeyImage();
	
	$(".StatusTabs .file-status").click(function(event){
		var id = this.id.replace("label-","");
		var Obj = $(this);
		event.preventDefault();
		//if(Obj.parent("tr").length == 0) {
		var idRadio = Obj.find("input[type='radio']");
		
		if(Obj.hasClass("active")) {
			Obj.removeClass("active");
			idRadio.prop("checked",false);
		}
		else {
			$(".StatusTabs .file-status").removeClass("active");
			Obj.addClass("active");
			idRadio.prop("checked",true);
		}
		//}
	});
	$('ul.realestateTabs li a').click(function(event){
		var Obj = $(this);
		event.preventDefault();
		var objRef = parseInt(Obj.attr("href").replace("#tab-",""));
		if(objRef==3){
			$('#gallery-script').html('<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>');}else{
				$('#gallery-script').html('');
				}
		var ObjContainer = $("#realEstateTabContainer-"+objRef);
		if(!ObjContainer.is(":visible")) {
			if(objRef == 9) {
				var siteurl_admin = $("#siteurl_admin").val();
				//alert(siteurl_admin);
				if(ObjContainer.hasClass("map"))
				$("#MapLoaded").load(siteurl_admin+'units/loadMap/'+$("input[name='id']").val());
				else
				$("#MapLoaded").load(siteurl_admin+'properties/loadMap/'+$("input[name='id']").val());
			}
			//else {
				window.location.hash = "#tab-"+objRef;
				$('ul.realestateTabs li a').removeClass("active");
				$(".realEstateTabContainer").css("display","none");
				ObjContainer.css("display","block");
				Obj.addClass("active");
			//}
		}
	});
	var WindowHash = window.location.hash;
	if(WindowHash != "" && WindowHash != null) {
		WindowHashID = parseInt(WindowHash.replace("#tab-",""));
		var FindObj = $('ul.realestateTabs li a[href="'+WindowHash+'"]').click();
	}
	
	if($('#propertyForm').length > 0) {
		$('#propertyForm').dowformvalidate({
			  parentObj: "fieldset",
			  return_result:function(Parsedata){
			 $("#error_message").html('');
			 $('.alert-success').remove();
				  if(Parsedata.message!="" && Parsedata.result==0) {
				 $("#error_message").html(Parsedata.message);
				 scroll_To("#error_message",50);
				  }
			  },
			  callback:function(obj1){
			
				  if(obj1) {
					  var ParseObjData = JSON.parse(obj1.data);
					  //if(ParseObjData.reload_data) {
						  //$("#"+module).html(ParseObjData.reload_data);
					  //}
				  }
			  }
		});
	}

	if($(".MultiSelectDropDown").length > 0) {
		$(".MultiSelectDropDown").multiselect({
		   selectedList: 4 // 0-based index
		});
	}
	
	if($('.phoneMask').length > 0) {
		$('.phoneMask').inputmask("(999) (99) (999 9999)",{"placeholder": ""});
	}
	if($('.percentageMask').length > 0) {
		$('.percentageMask').inputmask("99",{"placeholder": ""});
	}
	if($('.yearsMask').length > 0) {
		$('.yearsMask').inputmask("9999",{"placeholder": ""});
	}
	if
	($('.numberMask').length > 0) {
		$('.numberMask').inputmask("999999",{"placeholder": ""});
	}
	
	if($("#multipleSelection-facilities").length > 0) {
		$("#multipleSelection-facilities").multiSelect();
	}
	if($(".multipleSelection-amenities").length > 0){
	$('.multipleSelection-amenities').each(function(){
		var id=this.id;
		$("#"+id).multiSelect();
		});
	}
	
	if($("#multipleSelection-fittings").length > 0)
	$("#multipleSelection-fittings").multiSelect();
	
	if($("#multipleSelection-fixtures").length > 0)
    $("#multipleSelection-fixtures").multiSelect();

	if($('.timePicker').length > 0)
	$('.timePicker').timepicker({ 'timeFormat': 'H:i:s' });
	
	if($("textarea[name='property_overview']").length > 0) {
		$("textarea[name='property_overview']").keydown(function(){
			var inputValLength = $("textarea[name='property_overview']").val().length;
			$("#count_1").html(inputValLength);
		});
		$("textarea[name='property_overview']").blur(function(){
			var inputValLength = $("textarea[name='property_overview']").val().length;
			$("#count_1").html(inputValLength);
		});
	}
	
	if($("textarea[name='web_remarks']").length > 0) {
		$("textarea[name='web_remarks']").keydown(function(){
			var inputValLength = $("textarea[name='web_remarks']").val().length;
			$("#count_2").html(inputValLength);
		});
		$("textarea[name='web_remarks']").blur(function(){
			var inputValLength = $("textarea[name='web_remarks']").val().length;
			$("#count_2").html(inputValLength);
		});
	}
	
	if($("textarea[name='local_area_and_amenities_desc']").length > 0) {
		$("textarea[name='local_area_and_amenities_desc']").keydown(function(){
			var inputValLength = $("textarea[name='local_area_and_amenities_desc']").val().length;
			$("#count_3").html(inputValLength);
		});
		$("textarea[name='local_area_and_amenities_desc']").blur(function(){
			var inputValLength = $("textarea[name='local_area_and_amenities_desc']").val().length;
			$("#count_3").html(inputValLength);
		});
	}
	
	if($("textarea[name='site_info_and_amenities_desc']").length > 0) {
		$("textarea[name='site_info_and_amenities_desc']").keydown(function(){
			var inputValLength = $("textarea[name='site_info_and_amenities_desc']").val().length;
			$("#count_4").html(inputValLength);
		});
		$("textarea[name='site_info_and_amenities_desc']").blur(function(){
			var inputValLength = $("textarea[name='site_info_and_amenities_desc']").val().length;
			$("#count_4").html(inputValLength);
		});
	}
	
	if($("textarea[name='developer_desc']").length > 0) {
		$("textarea[name='developer_desc']").keydown(function(){
			var inputValLength = $("textarea[name='developer_desc']").val().length;
			$("#count_5").html(inputValLength);
		});
		$("textarea[name='developer_desc']").blur(function(){
			var inputValLength = $("textarea[name='developer_desc']").val().length;
			$("#count_5").html(inputValLength);
		});
	}
	
	if($("textarea[name='marketing_description']").length > 0) {
		$("textarea[name='marketing_description']").keydown(function(){
			var inputValLength = $("textarea[name='marketing_description']").val().length;
			$("#count_6").html(inputValLength);
		});
		$("textarea[name='marketing_description']").blur(function(){
			var inputValLength = $("textarea[name='marketing_description']").val().length;
			$("#count_6").html(inputValLength);
		});
	}
	
});
function addMoreInfo(module,ref_module)
{
	var siteurl_admin = $("#siteurl_admin").val();
	var url = siteurl_admin+'add_more/loadTemplate';
	var refVal = "";
	var proceed = true;
	if(ref_module != "") {
		refVal = $("#"+ref_module).val();
		if(refVal == "") {
			alert("Please select the "+ref_module);
			proceed = false;
		}
	}
	if(proceed) {
		$.ajax({
			url:url,
			type:'POST',
			data:{module: module,ref_module : ref_module, ref : refVal, property_id : $("input[name='id']").val()},
			success:function(data){
				var parseData = JSON.parse(data);
				$('#myModal').html(parseData.content);
				//if($('#addMore'+model+'Form').length > 0) {
					  //$('#addMore'+model+'Form').dowformvalidate({
				if($('#addMoreRecordsForm').length > 0) {
					 $('#addMoreRecordsForm').dowformvalidate({
						  parentObj: "fieldset",
						  callback:function(obj1){
							  if(obj1) {
								  var ParseObjData = JSON.parse(obj1.data);
								  if(ParseObjData.reload_data) {
									  $("#"+module).html(ParseObjData.reload_data);
								  }
								  $('.modal-backdrop').click();
								  
								  if(module == "facilities")
								  $("#multipleSelection-facilities").multiSelect();
								  if(module == "amenities")
								  $("#multipleSelection-amenities").multiSelect();
								  
								  if($("#multipleSelection-fittings").length > 0)
								  $("#multipleSelection-fittings").multiSelect();
								  
								  if($("#multipleSelection-fixtures").length > 0)
								  $("#multipleSelection-fixtures").multiSelect();
				
							  }
						  }
					  });
				}
				if($('#UserForm').length > 0) {
					 $('#UserForm').dowformvalidate({
						  parentObj: "fieldset",
						  callback:function(obj1){
							  if(obj1) {
								  var ParseObjData = JSON.parse(obj1.data);
								  if(ParseObjData.reload_data) {
									  $("#"+module).html(ParseObjData.reload_data);
								  }
								  $('.modal-backdrop').click();
							  }
						  }
					  });
				}
			},
		});
	}
	else {
		setTimeout("removeModalBack()",200);
	}
}
function renderMasterkeyImage()
{
	if($('.masterkeyImg').length > 0) {
		$('.masterkeyImg').each(function(){
			var Obj = $(this);
			var ObjDataLs = Obj.attr("data-ls");
			var ObjDataLs = JSON.parse(ObjDataLs);
			var ObjSrc = ObjDataLs.src;
			Obj.attr("src",ObjSrc);
			Obj.removeAttr("data-ls");
		});
	}
}
function removeModalBack()
{
	$('.modal-backdrop').click();
}
function loadSubModule(module,ref_module)
{
	var siteurl_admin = $("#siteurl_admin").val();
	var url = siteurl_admin+'add_more/reloadData';
	var refVal = "";
	var proceed = true;
	if(module != "") {
		refVal = $("#"+module).val();
		if(refVal == "") {
			//alert("Please select the "+module);
			proceed = false;
		}
	}
	if(proceed) {
		$("select, input").attr("disabled","disabled");
		$.ajax({
			url:url,
			type:'POST',
			data:{module: module,ref_module : ref_module, ref : refVal},
			success:function(data){
				var parseData = JSON.parse(data);
				$('#'+ref_module).html(parseData.content);
				$("select, input").removeAttr("disabled");
			},
		});
		if(module == 'units_types') {
			var url = siteurl_admin+'units/getRelatedFields';
			$.ajax({
				url:url,
				type:"POST",
				data:{id : refVal},
				success:function(data){
					var PraseData = JSON.parse(data);
					if(PraseData.result == 1) {
						var fields = PraseData.fields;
						$.each( fields, function( key, value ) {
							if(value == 1) {
								$("#id-"+key).slideDown("fast");
							}
							else {
								$("#id-"+key).slideUp("fast");
							}
						});
					}
				}
			})
		}
		
	}
	else {
		return false;
		//setTimeout("removeModalBack()",200);
	}
}
$(document).ready(function(){
	if($("#units_types").length > 0)
	ChangeUnitType();
});
function ChangeUnitType()
{
	var siteurl_admin = $("#siteurl_admin").val();
	var id = $("#units_types").val();
	if(id == "" || id == null) {
		//$().attr("disabled","disabled");
	}
	else {
		var url = siteurl_admin+'units/getRelatedFields';
		$.ajax({
			url:url,
			type:"POST",
			data:{id : id},
			success:function(data){
				var PraseData = JSON.parse(data);
				if(PraseData.result == 1) {
					var fields = PraseData.fields;
					$.each( fields, function( key, value ) {
						if(value == 1) {
							$("#id-"+key).slideDown("fast");
						}
						else {
							$("#id-"+key).slideUp("fast");
						}
					});
				}
			}
		})
	}
}
/**************************************************************************************/
function addMoreFinanceCompanies(id)
{
	var siteurl_admin = $("#siteurl_admin").val();
	var url = siteurl_admin+'add_more/addFinanceCompany';
	$("select, input").attr("disabled","disabled");
	$.ajax({
		url:url,
		type:'POST',
		data:{id: id},
		success:function(data){
			var parseData = JSON.parse(data);
			$('#financeCompaniesTable').append(parseData.content);
			$("select, input").removeAttr("disabled");
			if($('.percentageMask').length > 0) {
				$('.percentageMask').inputmask("99",{"placeholder": ""});
			}
			if($('.yearsMask').length > 0) {
				$('.yearsMask').inputmask("9999",{"placeholder": ""});
			}
		},
	});
}
function deleteFinanceCompany(id)
{
	if(confirm("Are you sure?")) {
		var siteurl_admin = $("#siteurl_admin").val();
		var url = siteurl_admin+'add_more/removeFinanceCompany';
		$("select, input").attr("disabled","disabled");
		$.ajax({
			url:url,
			type:'POST',
			data:{id: id},
			success:function(data){
				var parseData = JSON.parse(data);
				if(parseData.result == 1) {
					$("#fc-"+id).remove();
				}
				$("select, input").removeAttr("disabled");
			},
		});
	}
}
/**************************************************************************************/
function addMorePaymentPlans(id)
{
	var siteurl_admin = $("#siteurl_admin").val();
	var url = siteurl_admin+'add_more/addMorePaymentPlans';
	$("select, input").attr("disabled","disabled");
	$.ajax({
		url:url,
		type:'POST',
		data:{id: id},
		success:function(data){
			var parseData = JSON.parse(data);
			$('#paymentPlansTable').append(parseData.content);
			$("select, input").removeAttr("disabled");
			if($('.percentageMask').length > 0) {
				$('.percentageMask').inputmask("99",{"placeholder": ""});
			}
			if($('.yearsMask').length > 0) {
				$('.yearsMask').inputmask("9999",{"placeholder": ""});
			}
		},
	});
}
function deletePaymentPlan(id)
{
	if(confirm("Are you sure?")) {
		var siteurl_admin = $("#siteurl_admin").val();
		var url = siteurl_admin+'add_more/removePaymentPlan';
		$("select, input").attr("disabled","disabled");
		$.ajax({
			url:url,
			type:'POST',
			data:{id: id},
			success:function(data){
				var parseData = JSON.parse(data);
				if(parseData.result == 1) {
					$("#pp-"+id).remove();
				}
				$("select, input").removeAttr("disabled");
			},
		});
	}
}
/**************************************************************************************/
function addMoreUnitParking(id)
{
	var siteurl_admin = $("#siteurl_admin").val();
	var url = siteurl_admin+'add_more/addMoreUnitParking';
	$("select, input").attr("disabled","disabled");
	$.ajax({
		url:url,
		type:'POST',
		data:{id: id},
		success:function(data){
			var parseData = JSON.parse(data);
			$('#unitParkingTable').append(parseData.content);
			$("select, input").removeAttr("disabled");
			if
	($('.numberMask').length > 0) {
		$('.numberMask').inputmask("999999",{"placeholder": ""});
	}
		},
	});
}
function deleteUnitParking(id)
{
	if(confirm("Are you sure?")) {
		var siteurl_admin = $("#siteurl_admin").val();
		var url = siteurl_admin+'add_more/removeUnitParking';
		$("select, input").attr("disabled","disabled");
		$.ajax({
			url:url,
			type:'POST',
			data:{id: id},
			success:function(data){
				var parseData = JSON.parse(data);
				if(parseData.result == 1) {
					$("#up-"+id).remove();
				}
				$("select, input").removeAttr("disabled");
			},
		});
	}
}
function checkAgentDifferentMarket(obj)
{
	if($("input[name='make_as_different_agent']").is(":checked")) {
		$("#marketing_agent_display").slideDown("fast");
	}
	else {
		$("#marketing_agent_display").slideUp("fast");
	}
}
/************************************************************************************************************/
function addMoreOpenHouse(id)
{
	var siteurl_admin = $("#siteurl_admin").val();
	var url = siteurl_admin+'add_more/addMoreOpenHouse';
	$("select, input").attr("disabled","disabled");
	$.ajax({
		url:url,
		type:'POST',
		data:{id: id},
		success:function(data){
			var parseData = JSON.parse(data);
			$('#unitOpenHouse').append(parseData.content);
			$("select, input").removeAttr("disabled");
			if($('.date').length > 0)
			$('.date').datepicker();
			if($('.timePicker').length > 0)
			$('.timePicker').timepicker({ 'timeFormat': 'H:i:s' });
		},
	});
}
function deleteOpenHouse(id)
{
	if(confirm("Are you sure?")) {
		var siteurl_admin = $("#siteurl_admin").val();
		var url = siteurl_admin+'add_more/removeOpenHouse';
		$("select, input").attr("disabled","disabled");
		$.ajax({
			url:url,
			type:'POST',
			data:{id: id},
			success:function(data){
				var parseData = JSON.parse(data);
				if(parseData.result == 1) {
					$("#oh-"+id).remove();
				}
				$("select, input").removeAttr("disabled");
			},
		});
	}
}
/**** Selling Details ********************************************************************************************/
function ChangeBasePrice()
{
	var BasePrice = $("input[name='base_price']").val();
	
	if($("input[name='premium_price_percentage']").val() != '')
	//$("input[name='premium_price']").val(getPercentageValue($("input[name='premium_price_percentage']").val(),BasePrice));
	
	if($("input[name='agency_fee_seller_perce']").val() != '') {
		var p = BasePrice + $("input[name='premium_price']").val();
		//$("input[name='agency_fee_seller']").val(getPercentageValue($("input[name='agency_fee_seller_percentage']").val(),p));
	}
}
function getPercentageValue(percent,value)
{
	return (value * percent) / 100;
}

