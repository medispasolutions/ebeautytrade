<?php

class __Mustache_8a8dcee49447bdbd2ed7e26f2adb6109 extends Mustache_Template
{
    private $lambdaHelper;
    protected $strictCallables = true;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        // 'WRAPPER' section
        $value = $context->find('WRAPPER');
        $buffer .= $this->section2c679b5d82d30616776ef7d71f71e7b2($context, $indent, $value);
        // 'CONTENT' section
        $value = $context->find('CONTENT');
        $buffer .= $this->section6a1eb5fbe1028ccc85540284293cb494($context, $indent, $value);
        // 'WRAPPER' section
        $value = $context->find('WRAPPER');
        $buffer .= $this->section9c4d7b23ba7b7cfa2303df3a0912b674($context, $indent, $value);

        return $buffer;
    }

    private function section2c679b5d82d30616776ef7d71f71e7b2(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (is_object($value) && is_callable($value)) {
            $source = '
<div class="form-group__control__msg  js-form-control-message">
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '<div class="form-group__control__msg  js-form-control-message">
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section2ea58fcac8cea3c59930e2990fe7372d(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (is_object($value) && is_callable($value)) {
            $source = '
		{{{ MESSAGE }}}
	';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '		';
                $value = $this->resolveValue($context->find('MESSAGE'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section6a1eb5fbe1028ccc85540284293cb494(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (is_object($value) && is_callable($value)) {
            $source = '
	{{#RENDER_UNESCAPED}}
		{{{ MESSAGE }}}
	{{/RENDER_UNESCAPED}}
	{{^RENDER_UNESCAPED}}
		{{ MESSAGE }}
	{{/RENDER_UNESCAPED}}
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                // 'RENDER_UNESCAPED' section
                $value = $context->find('RENDER_UNESCAPED');
                $buffer .= $this->section2ea58fcac8cea3c59930e2990fe7372d($context, $indent, $value);
                // 'RENDER_UNESCAPED' inverted section
                $value = $context->find('RENDER_UNESCAPED');
                if (empty($value)) {
                    
                    $buffer .= $indent . '		';
                    $value = $this->resolveValue($context->find('MESSAGE'), $context);
                    $buffer .= call_user_func($this->mustache->getEscape(), $value);
                    $buffer .= '
';
                }
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section9c4d7b23ba7b7cfa2303df3a0912b674(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (is_object($value) && is_callable($value)) {
            $source = '
</div>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '</div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
