    $.fn.dowformvalidate = function (options) {
        // Establish our default settings
        var settings = $.extend({
            formresult: ".FormResult",
            formerror: ".form-error",
            loader: "Loading...",
            validateonly: false,
            withScrolling: true,
            updateonly: false
        }, options);

        var FormObj = $(this);
        var formresult = settings.formresult;
        var formerror = settings.formerror;
        var upOnly = settings.updateonly;
        var withScrolling = settings.withScrolling;
        FormObj.submit(function (event) {
            if ((FormObj.attr('id') == "cart-form" && !validateForm('true', '#cart-form')) || (FormObj.attr('id') == "cart-form-details" && !validateForm('true', '#cart-form-details'))) {
            } else {
                event.preventDefault();
                var SubmitButton = FormObj.find("input[type='submit']");
                SubmitButton.attr('disabled', 'disabled');
                var formAction = FormObj.attr('action');
                FormObj.find('#loader-bx').html('<div class="loader"></div>');
                FormObj.find('#loader-bx').show();
                FormObj.find(formresult).html('');
                var formData = new FormData($(this)[0]);
                $(settings.formerror).html("");
                $.ajax({
                    url: FormObj.attr('action'),
                    type: 'POST',
                    data: formData,
                    success: function (data) {

                        $(settings.formresult).html("");
                        var baseurl = $('#baseurl').val();
                        Parsedata = JSON.parse(data);
                        SubmitButton.attr('disabled', null);
                        if (Parsedata.count_items != null) {
                            if ($('#cart_items_count .count').length > 0) {
                                $('#cart_items_count .count').remove();
                            }
                            $('<span  class="count">' + Parsedata.count_items + '</span>').appendTo('#cart_items_count');
                        }

                        if (Parsedata.return_popup_message != null) {
                            popup_return_message(Parsedata.return_popup_message, Parsedata.result, Parsedata.id);
                        }

                        if (Parsedata.remaining_miles != null) {
                            $('#remaining-miles').html(Parsedata.remaining_miles);
                        }
                        if ($('.loader_btn').length > 0) {
                            $('a, button, div, input').removeClass('loader_btn');
                        }

                        if (Parsedata.result == 0) {

                            if (Parsedata.login == 1) {
                                var session_url = $('#siteurl').val() + 'home/session';
                                $.post(session_url, {session: current_url}, function (data) {
                                });
                                var url = $('#siteurl').val() + 'user/login2';

                                CallTBMInfoDetails3(url, 2, 'login_popup');
                            }

                            if (Parsedata.errors) {
                                var x = [];
                                var errors = Parsedata.errors;

                                j = 0;
                                $.each(errors, function (i, val) {

                                    if (i == 'expire_date') {
                                        $('#reactive_account').show();
                                    }
                                    FormObj.find('#' + i + '-error').html('<span class="inner-form-error"><i class="error-icon"></i>' + val + '</span>');
                                });

                            }
                            if (Parsedata.message != '' && Parsedata.message != 'Error!') {

                                FormObj.find(formresult).html(Parsedata.message);
                            }
                            if (Parsedata.captcha != null) {
                                FormObj.find('.captchaImage').html(Parsedata.captcha);
                                FormObj.find('input[name="captcha"]').val('');
                            }
                            FormObj.find(formresult).html('');
                            FormObj.find('#loader-bx').html('');
                            var emptyy = '';
                            $(formerror).each(function () {
                                if ($(this).html().trim().length) {
                                    if (emptyy == '') {
                                        emptyy = $(this).attr('id');
                                    }
                                }
                            });
                            if (emptyy != "") {
                                var anmEror = $('#' + emptyy).offset().top - 100;
                                if (withScrolling) {
                                    $('html, body').animate({scrollTop: anmEror}, 1000);
                                }
                                if (Parsedata.message != '' && Parsedata.message != 'Error!') {
                                    var errorText = '<ul class="messages"><li class="error-msg r-fullSide"><ul><li><span>' + Parsedata.message + '</span></li></ul></li></ul>';
                                    FormObj.find(formresult).html(errorText);
                                }
                            }


                            if (FormObj.attr('id') == "update_cart_form") {
                                if (Parsedata.message != '' && Parsedata.message != 'Error!') {
                                    var errorText = '<ul class="messages"><li class="error-msg r-fullSide"><ul><li><span>' + Parsedata.message + '</span></li></ul></li></ul>';
                                    FormObj.find(formresult).html(errorText);
                                }
                            }
                        } else {
                            if (Parsedata.thank_you_content != null) {
                                $('.thank_you_bx').html(Parsedata.thank_you_content);
                            }
                            if (Parsedata.html_appendTo_before != null) {
                                $(Parsedata.html_appendTo_before).before(Parsedata.html_append);
                                alert(Parsedata.html_appendTo_before);
                            }

                            if (FormObj.attr('id') == "updatePopUpForm2") {
                                $(Parsedata.html).appendTo("#stocklist_categories_l");
                                $("#stocklist_categories_l .empty").remove();
                                $('#addNewCategory_pd').slideUp('fast');
                                scroll_To('#addNewCategory_pd', 150);
                            }
                            ///////////////CHECKOUT ADDRESSES////////////
                            if (FormObj.attr('id') == "checkOutAddressForm") {

                                var id_address = Parsedata.id;

                                if (Parsedata.billing == 1) {
                                    var type = "billing";
                                } else {
                                    var type = "shipping";
                                }
                                if ($('#addressBox_' + type + '_' + id_address).length > 0) {
                                    $('#addressBox_billing_' + id_address).html(Parsedata.billing_html);
                                    $('#addressBox_shipping_' + id_address).html(Parsedata.shipping_html);
                                } else {


                                    if (Parsedata.billing == 1) {
                                        $('#billing_addresses_bxs .addressBox').removeClass('active');
                                        $('input[name="billing_select"]').val(id_address);
                                    } else {
                                        $('#shipping_addresses_bxs .addressBox').removeClass('active');
                                        $('input[name="shipping_select"]').val(id_address);
                                    }
                                    $('#billing_addresses_bxs').prepend(Parsedata.billing_html);
                                    $('#shipping_addresses_bxs').prepend(Parsedata.shipping_html);
                                    scroll_To('#addressBox_' + type + '_' + id_address, 150);

                                }
                                addressBtn();

                                closePopup();
                            }


                            //////////////////////////////////////////////// ///////////////////

                            if (Parsedata.cart != '') {

                                $('#shopping-cart-cont').html(Parsedata.cart);

                                if (Parsedata.total_price_cont != '') {
                                    $('#total_price_cont').html(Parsedata.total_price_cont);
                                }
                                updateQty_cart();
                            }
                            if (Parsedata.checklist != '') {

                                $('#favorites-cart').html('');
                                $('#favorites-cart').html(Parsedata.checklist);

                                if ($('#wishlist-table tr').length > 1) {

                                    $('.bulk-option').removeClass('hide');
                                } else {
                                    $('.bulk-option').addClass('hide');
                                }

                                if (FormObj.attr('id') == "updatePopUpForm") {

                                    closePopup();
                                }
                                if (FormObj.attr('id') == "sendQuotation") {

                                    setTimeout('closePopup()', 2000);
                                }
                                if (Parsedata.favorites_form != '') {
                                    $('#favorites_form').html(Parsedata.favorites_form);
                                }
                                updateQty();
                                checkAllAuto();
                            }

                            ///////////////////////////new updates\\\\\\\\\\\\\\\\\\\\
                            if ($('#add_email').length > 0) {
                                $("#id_user").val(Parsedata.id_user);
                                $("#correlation_id").val(Parsedata.correlation_id);
                                $(".email_register").val(Parsedata.email);
                                $('#opc-email .new-users').slideUp('fast');
                                $('#opc-register .new-users').slideDown('fast');
                                scroll_To('#opc-register', 250);

                                $('.edit_register').show();

                                loadPhone();
                            }
                            //////// //////// //////// //////// //////// //////// //////// ////////
                            if (FormObj.attr('id') == "favorites-form") {
                                setTimeout(function () {
                                    closePopup()
                                }, 3000);
                            }
                            if ($('.meeting_request').length > 0 && $('#favorites-form').length == 0 && Parsedata.message != '' && Parsedata.message != 'Error!') {

                                var errorText = '<div class="success-result"><img src="' + baseurl + 'front/img/i_msg-success.png" />' + Parsedata.message + '</div>';
                                FormObj.find(formresult).html(errorText);
                              // document.getElementById('Send Request').title.style.display = 'none';
                              //alert('test');
                            } else {

                                if (Parsedata.message != '' && Parsedata.message != 'Error!') {

                                    if ($('#creditsForm').length > 0) {
                                        $('#credits_message').html(Parsedata.message);
                                    } else {

                                        var mod_message = "";
                                        if (Parsedata.mod_message != "") {
                                            var mod_message = Parsedata.mod_message;
                                        }
                                        var errorText = '<ul class="messages ' + mod_message + ' "><li class="success-msg r-fullSide"><ul><li><span>' + Parsedata.message + '</span></li></ul></li></ul>';
                                        FormObj.find(formresult).html(errorText);
                                    if (Parsedata.redirect != null)
                                        window.location = Parsedata.redirect;
                                    }
                                }
                            }
                            if (upOnly) {
                                //
                            }
                            else {
                                if (!settings.validateonly) {

                                    FormObj.find('input[type="text"], input[type="hidden"], input[type="email"], input[type="password"], textarea, select').each(function () {
                                        var Obj = $(this);
                                        if (Obj.attr("type") != "hidden")
                                            Obj.val('');
                                        Obj.removeClass('vError');
                                        Obj.parent('div').find(formerror).html('');
                                        var anmEror = FormObj.find(formresult).offset().top - 100;
                                    });
                                }
                            }
                            if (!settings.updateonly) {
                                FormObj.find('input[name="captcha"]').each(function () {
                                    var Obj = $(this);
                                    if (Obj.attr("type") != "hidden")
                                        Obj.val('');
                                    Obj.removeClass('vError');
                                    Obj.parent('div').find(formerror).html('');
                                    var anmEror = FormObj.find(formresult).offset().top - 100;
                                });
                            } else {

                                FormObj.find('input[name="captcha"]').each(function () {
                                    var Obj = $(this);
                                    Obj.val('');
                                });

                                if ($('.change_pass').length > 0) {
                                    FormObj.find('input[type="password"]').each(function () {
                                        var Obj = $(this);
                                        Obj.val('');
                                    });
                                }
                            }
                            if (Parsedata.payment != null && $('#payment').length > 0) {
                                $('#payment').html(Parsedata.payment);
                            }
                            if (Parsedata.redirect_link != null)
                                window.location = Parsedata.redirect_link;
                        }
                        FormObj.find('#loader-bx').hide();
                        FormObj.find('#loader-bx').html('');
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
                return false;


            }
        });
        FormObj.find('input[type="text"], input[type="password"], textarea, select').focus(function () {
            var Obj = $(this);
            Obj.removeClass('vError');
            Obj.parent('div').find(formerror).html('');
        });

        if ($.isFunction(settings.callback)) {
            settings.callback.call(this);
        }
    }

    function popup_return_message(message, result, id) {

        if (result == 1 && ($('#product-list-' + id).length > 0 || $('#btn-cart-' + id).length > 0 )) {

            closePopup();
            $('.product').removeClass('added');

            if (result == 1 && $('#product-list-' + id).length > 0) {
                productAnimate('#product-list-' + id);
                $('#product-list-' + id).addClass('added');
                $('.product').removeClass('hide_added');
            } else {

                if (result == 1 && $('#btn-cart-' + id).length > 0) {

                    $('.action-products-details').removeClass('hide_added');
                    $('#btn-cart-' + id).parent().parent().addClass('added');
                }
            }

            setTimeout(function () {
                $('.added').addClass('hide_added');
            }, 1500);
        } else {

            if (result == 0) {
                var class_result = "error_result";
            }
            if (result == 1) {
                var class_result = "success_result";
            }

            if ($('#PopupContainer').is(":visible")) {
            } else {
                $('#PopupContainer').fadeIn('fast');
                $('.shadow').fadeIn('fast');
                setTimeout('setPopupBlockPosition()', 400);
            }
            $('#PopupLoader').html('<div class="return_popup_message ' + class_result + '">' + message + '</div>');
        }
    }