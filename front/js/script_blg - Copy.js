function validateSearchForm(){
	var  keywords=jQuery('.keywords').val();
	if(keywords==""){return false;}else{
		return true;}}

function limitText(limitField, limitCount, limitNum) {

	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}


/////////////////////TABS\\\\\\\\\\\\\\\\\\\\\\\\\\\
jQuery(document).ready(function() {
popupCall();	
if(jQuery('a.popUpGallery').length>0){
 jQuery("a.popUpGallery[rel^='prettyPhoto']").prettyPhoto({deeplinking: false,social_tools:false});}

	if(jQuery('.question_and_answer_cont .question_and_answer').length>0){
	
	 var ias = jQuery.ias({
      container: ".question_and_answer_cont",
      item: ".question_and_answer",
	  trigger: '??? ??????',
      pagination: "#pagination",
	  loader: ' <img src="'+baseurl+'front/img/482.GIF" />',
      next: ".next a"
    });

	ias.on('rendered', function(items) { });
	  
	  ias.extension(new IASSpinnerExtension({
    src: baseurl+'front/img/482.GIF', // optionally
}));
   
    ias.extension(new IASTriggerExtension({offset: 0}));
    ias.extension(new IASNoneLeftExtension({text: ''}));
	}
	
	if(jQuery('.category_toggle').length>0){
	jQuery('.category_toggle').click(function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent();
	var id=obj.attr('id').replace('category_toggle_',"");
	if(jQuery("#sub_category_"+id).is(":visible")){
		obj_parent.removeClass('active');
    jQuery("#sub_category_"+id).stop().slideUp('10');}else{
	obj_parent.addClass('active');
	jQuery("#sub_category_"+id).stop().slideDown('10');	}

	});
	}
	
	if(jQuery('.category-a').length>0){

jQuery(".category-li.hvr").stop(true,true).hover(function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	var id=obj.attr('id').replace('category-li-',"");
    obj.addClass('a_active');
	
	obj_parent.find('#category-a-'+id).addClass('a_active_2');
	obj_parent.find('#sub-category-'+id).stop(true,true).slideDown(100,function(){
	obj_parent.find('#sub-category-'+id).addClass('active_2');});
	
    },function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	obj.removeClass('a_active');
	var id=obj.attr('id').replace('category-li-',"");
    obj_parent.find('#category-a-'+id).removeClass('a_active_2');
obj_parent.find('#sub-category-'+id).stop(true,true).slideUp('fast',function(){
	obj_parent.find('#sub-category-'+id).removeClass('active_2');});
	if(obj_parent.find('#sub-category-'+id).hasClass('active')){}else{
	obj_parent.find('#sub-category-'+id).stop(true,true).slideUp('fast',function(){
	obj_parent.find('#sub-category-'+id).removeClass('active_2');});}
	<!--obj_parent.find('#sub-category-'+id).removeClass('active_2');-->
	});
	
jQuery(".sub_category_2").stop().hover(function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	var id=obj.attr('id').replace('sub-category-',"");
    obj.addClass('a_active');

	obj_parent.find('#category-a-'+id).addClass('a_active_2');
	obj_parent.find('#sub-category-'+id).stop(true,true).slideDown('fast',function(){
	obj_parent.find('#sub-category-'+id).addClass('active_2');});
	
    },function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	obj.removeClass('a_active');
	var id=obj.attr('id').replace('sub-category-',"");
    obj_parent.find('#category-a-'+id).removeClass('a_active_2');
	if(obj_parent.find('#sub-category-'+id).hasClass('active')){}else{
	obj_parent.find('#sub-category-'+id).stop(true,true).slideUp('fast',function(){
	obj_parent.find('#sub-category-'+id).removeClass('active_2');});}
	<!--obj_parent.find('#sub-category-'+id).removeClass('active_2');-->
	
   
	     		
});

	
	}
	

if(jQuery(".consultants_btn").length>0){
	jQuery(".consultants_btn").click(function(){
		var obj=jQuery(this);
		var ParentObj=obj.parent().parent();
		var id=ParentObj.attr('id').replace('consultants_',"");
		selectConsultant(id);
		});
		
		jQuery(".remove_consultant").click(function(){
		if(confirm('Are you sure you want to remove this consultant?')) {
		jQuery('#consultant').val("");
		jQuery('.consultants_btn').removeClass('hide');
		jQuery('.remove_consultant').addClass('hide');
		jQuery('#PopupLoader').removeClass('active_consultant');
		jQuery('.consultants_cont .supplier_blk').removeClass('active');
		}});}		
	


//////////////////////////////////// drop down ////////////////////////////////
jQuery(".dropdown").stop().hover(function(){
	var id = parseInt(this.id.replace('dropdown-',''));
    jQuery("#sub-dropdown-"+id).css({'display':'block'});

	brand_dropdown_slick();
	},function(){
			var id = parseInt(this.id.replace('dropdown-',''));
			 jQuery("#sub-dropdown-"+id).css({'display':'none'});
			/*jQuery("#sub-dropdown-"+id).stop(true,true).slideUp(100);*/
	     		
});
    //Default Action
	if(jQuery(".req_c input[type='checkbox']").length>0){
	jQuery(".req_c input[type='checkbox']").click(function() {
			var obj=jQuery(this);
			var name=jQuery(this).attr('name').replace('_c','');
		
			if(jQuery("input[name='"+name+"_c']").length>0){
			 if (jQuery(this).prop('checked')==true){ 
			jQuery("input[name='"+name+"']").val('1');
				}else{
			jQuery("input[name='"+name+"']").val("");}}
			});
		}
	
	    //Default Action
	if(jQuery(".req_c input[type='file']").length>0){
		jQuery(".req_c input[type='file']").change(function() {
			var obj=jQuery(this);
			var name=jQuery(this).attr('name');
			var val=obj.val();
			if(jQuery("input[name='"+name+"_f']").length>0){
			jQuery("input[name='"+name+"_f']").val(val);
			jQuery("#"+name+"_f-error").html('');
	      	}
			});
		}	
		

 
});

function selectConsultant(id){
	
		if(confirm('Are you sure you want to choose this consultant?')) {

		jQuery('#consultant').val(id);
		jQuery('#consultants_'+id).addClass('active');
		jQuery('#PopupLoader').addClass('active_consultant');
		jQuery('.consultants_btn').addClass('hide');
		jQuery('#consultants_'+id).find('.remove_consultant').removeClass('hide');
		}}

jQuery(window).load(function(){

jQuery(jQuery(".mainMenu").html()).appendTo('#menuMobCont');
jQuery(jQuery(".search_mb").html()).appendTo('#menuMobCont');
jQuery(jQuery(".quarterLeftSideBar").html()).appendTo('#menuMobCont');
loadMenu();

});
		

function scroll_To(sec,m){
	if(m){}else{m=10}
var anmSec = jQuery(sec).offset().top-m;
 jQuery('html, body').stop(true,true).animate({scrollTop : anmSec},1000);}	
 
function meetingRequest(){
	jQuery('.meeting_request').slideDown('fast');
	scroll_To('.meeting_request',100);
	} 
	
function newsLetter(){
	if(jQuery(".newsletter_cont").is(":visible")){
		jQuery('.newsletter_cont').slideUp('fast');}else{
	jQuery('.newsletter_cont').slideDown('fast');
	jQuery("#NewsLetterForm").dowformvalidate({withScrolling:false});
	/*scroll_To('.newsletter_cont',100);*/}
	} 	
	

	   
	
	
   


function changeCurrency(){
		var id=jQuery('#currency').val();
		var baseurl=jQuery('#baseurl').val();
	
		window.location=baseurl+'home/change_currency/'+id;}
		

		
function loadMenu(){
jQuery(".menu-bar-btn").click(function(){
if(jQuery('.mainMenu').is(":visible")){
jQuery(".mainMenu").css({'display':'none'});
jQuery(".header_bottom").removeClass('active_menu');
}else{
	jQuery(".mainMenu").css({'display':'block'});
jQuery(".header_bottom").addClass('active_menu');
}});
if(jQuery('.lbl_shdw_bx').length>0){
jQuery(".lbl_shdw_bx").click(function(){
		if(jQuery('.currency_bx').hasClass('active_currencies')){
			jQuery('.currency_bx').removeClass('active_currencies');
			}else{
			jQuery('.currency_bx').addClass('active_currencies');
			}
		});
		
jQuery(document).click(function (e) {    		
   var container = jQuery('.currency_bx');

   if (!container.is(e.target) && container.has(e.target).length === 0)
  {jQuery('.currency_bx').removeClass('active_currencies');}
});		
}
if(jQuery('.menu-toggle').length>0){
	jQuery('.menu-toggle').click(function () {
		var obj=jQuery(this);
		var id=obj.attr('id').replace('mob_','');

if(jQuery('.menuMobCont .'+id).is(":visible")){
jQuery('.menuMobCont .'+id).css({'display':'none'});
jQuery('.menuMobCont .'+id).removeClass('activeMenu');
obj.removeClass('active');
}else{ 
	
jQuery('.menuMobCont .menuBlg').css({'display':'none'});
jQuery('.menuMobCont .menuBlg').removeClass('activeMenu');
jQuery('.menu-toggle').removeClass('active');

jQuery('.menuMobCont .'+id).addClass('activeMenu');
jQuery('.menuMobCont  .'+id).css({'display':'block'});
obj.addClass('active');
popupCall();	
}
		});}
		
if(jQuery('.mainMenu-mob-li-a').length>0){
	jQuery('.mainMenu-mob-li-a').click(function () {
	var parent=jQuery(this).parent();
	var id=parent.attr('id').replace('dropdown-','');
	if(jQuery('.dropdown.active').length>0){
	jQuery('.menuMobCont').find('.dropdown').removeClass('active');	
    jQuery('.menuMobCont').find('.sub-dropdown').stop(true,true).slideUp('fast',function(){});
    }
   if(parent.find('.sub-dropdown').is(":visible")){
	 parent.removeClass('active');	
	 parent.find('.sub-dropdown').stop(true,true).slideUp('fast',function(){});
	}else{ if(id==3){ brand_dropdown_slick();}
	parent.find('.sub-dropdown').stop(true,true).slideDown('fast',function(){});
	parent.addClass('active');	

	}
		});}
}	

jQuery(window).bind('resize',function(){
	/*resizeInnerSection();*/

	if(jQuery('.mainMenu_resize').length>0){}else{jQuery('.mainMenu').addClass('mainMenu_resize');}
});	
function mobile(){
	if(jQuery('.ul-header-account-m').is(":visible")){
	jQuery("#search_mb").css({'display':'none'});}else{
	jQuery("#search_mb").css({'display':'block !important'});}
	if(CheckIfMobWidth(800)){ /*music.pause();*/
	jQuery('body').addClass('mobile');
	jQuery('body').removeClass('web');
	/*jQuery('.mobile .mainMenu').css({'max-height':jQuery(window).height()-100});*/
	/*jQuery(".mobile .mainMenu").mCustomScrollbar();*/
	jQuery(".mainMenu, .ul_sub_mainMenu").css({'display':'none'});
	jQuery(".menu-bar-btn").removeClass('close');
	}else{
	if(jQuery('.active_menu').length>0){	
	jQuery(".header_bottom").removeClass('active_menu');}
	jQuery(".ul_sub_mainMenu").css({'display':'none'});
	jQuery(".mainMenu").css({'display':'block'});
	var WindowHeight = jQuery(window).height();
   	jQuery('body').removeClass('mobile');
	jQuery('body').addClass('web');
}}

function CheckIfMobWidth(width){
	var windowWidth=jQuery(window).width();
	if(windowWidth<=width) return true ;else return false
	} 

function CallTBMInfoDetails(item_href)
{

	var baseurl=jQuery('#baseurl').val();
 jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');
 jQuery('#PopupContainer').fadeIn('fast');
 var url = '';
 jQuery.post(url,{id : id},function(data){
  jQuery('#PopupLoader').html(data);
  setTimeout('setPopupBlockPosition()',400);
 });
}
	
function setPopupBlockPosition()
{

 var Bheight = jQuery('#PopupContainer').height();
 var WindowHeight = jQuery(window).height();
 var BodyTop = jQuery(window).scrollTop();
 if(Bheight > WindowHeight) {
  var Pos = BodyTop + 10;
 }
 else {
  var Pos = ((WindowHeight - Bheight) / 2) + BodyTop;
 }
 jQuery('#PopupContainer').animate({top : Pos},1000);
}
function brand_dropdown_slick(){
	
if(jQuery('.brand_dropdown_slick').length > 0) { 
 jQuery('.brand_dropdown_slick_cont').html('');
 jQuery('.brand_dropdown_slick_cont').html(jQuery('#brands_init').html());
 jQuery('.header .brand_dropdown_slick').slick({
   arrows: true,
   infinite: true,
   draggable: true,
   autoplay: true,
   pauseOnHover: true,
   fade:false,
   autoplaySpeed: 2500,
   speed: 500,
   slidesToShow: 8,
   slidesToScroll:1,
   adaptiveHeight: true,
   pauseOnHover: true,

   dots:false,
  onAfterChange: function() {
/*callProductZoom();*/
        },
   focusOnSelect: false,
    responsive: [
  {
    breakpoint: 900,
    settings: {
   slidesToShow: 5,
   slidesToScroll:1
    }
  },
   {
    breakpoint: 650,
    settings: {
   slidesToShow: 4,
   slidesToScroll:1
    }
  },
   {
    breakpoint: 450,
    settings: {
   slidesToShow: 3,
   slidesToScroll:1
    }
  },
    {
    breakpoint: 350,
    settings: {
   slidesToShow: 2,
   slidesToScroll:1
    }
  }
   ]
 });} } 	

function closePopup()
{
 jQuery('.shadow').fadeOut('fast');
 jQuery('#PopupContainer').fadeOut('fast');
 jQuery('#PopupLoader').html('');
}

if(jQuery('.zoom_01').length > 0) {
      jQuery('.zoom_01').zoom();
  }

function popupCall(){
	  
	  if(jQuery(".popup_call").length>0){
		jQuery(".popup_call").click(function() {
	var baseurl=jQuery('#baseurl').val();
			var url=jQuery(this).attr('href');
			 jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');
 jQuery('#PopupContainer').fadeIn('fast');

 jQuery.post(url,{id : 1},function(data){

  jQuery('#PopupLoader').html(data);
  setTimeout('setPopupBlockPosition()',400);
 });});}}

