/* == jquery mousewheel plugin == Version: 3.1.11, License: MIT License (MIT) */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof exports?module.exports=a:a(jQuery)}(function(a){function b(b){var g=b||window.event,h=i.call(arguments,1),j=0,l=0,m=0,n=0,o=0,p=0;if(b=a.event.fix(g),b.type="mousewheel","detail"in g&&(m=-1*g.detail),"wheelDelta"in g&&(m=g.wheelDelta),"wheelDeltaY"in g&&(m=g.wheelDeltaY),"wheelDeltaX"in g&&(l=-1*g.wheelDeltaX),"axis"in g&&g.axis===g.HORIZONTAL_AXIS&&(l=-1*m,m=0),j=0===m?l:m,"deltaY"in g&&(m=-1*g.deltaY,j=m),"deltaX"in g&&(l=g.deltaX,0===m&&(j=-1*l)),0!==m||0!==l){if(1===g.deltaMode){var q=a.data(this,"mousewheel-line-height");j*=q,m*=q,l*=q}else if(2===g.deltaMode){var r=a.data(this,"mousewheel-page-height");j*=r,m*=r,l*=r}if(n=Math.max(Math.abs(m),Math.abs(l)),(!f||f>n)&&(f=n,d(g,n)&&(f/=40)),d(g,n)&&(j/=40,l/=40,m/=40),j=Math[j>=1?"floor":"ceil"](j/f),l=Math[l>=1?"floor":"ceil"](l/f),m=Math[m>=1?"floor":"ceil"](m/f),k.settings.normalizeOffset&&this.getBoundingClientRect){var s=this.getBoundingClientRect();o=b.clientX-s.left,p=b.clientY-s.top}return b.deltaX=l,b.deltaY=m,b.deltaFactor=f,b.offsetX=o,b.offsetY=p,b.deltaMode=0,h.unshift(b,j,l,m),e&&clearTimeout(e),e=setTimeout(c,200),(a.event.dispatch||a.event.handle).apply(this,h)}}function c(){f=null}function d(a,b){return k.settings.adjustOldDeltas&&"mousewheel"===a.type&&b%120===0}var e,f,g=["wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"],h="onwheel"in document||document.documentMode>=9?["wheel"]:["mousewheel","DomMouseScroll","MozMousePixelScroll"],i=Array.prototype.slice;if(a.event.fixHooks)for(var j=g.length;j;)a.event.fixHooks[g[--j]]=a.event.mouseHooks;var k=a.event.special.mousewheel={version:"3.1.11",setup:function(){if(this.addEventListener)for(var c=h.length;c;)this.addEventListener(h[--c],b,!1);else this.onmousewheel=b;a.data(this,"mousewheel-line-height",k.getLineHeight(this)),a.data(this,"mousewheel-page-height",k.getPageHeight(this))},teardown:function(){if(this.removeEventListener)for(var c=h.length;c;)this.removeEventListener(h[--c],b,!1);else this.onmousewheel=null;a.removeData(this,"mousewheel-line-height"),a.removeData(this,"mousewheel-page-height")},getLineHeight:function(b){var c=a(b)["offsetParent"in a.fn?"offsetParent":"parent"]();return c.length||(c=a("body")),parseInt(c.css("fontSize"),10)},getPageHeight:function(b){return a(b).height()},settings:{adjustOldDeltas:!0,normalizeOffset:!0}};a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})});
/* == malihu jquery custom scrollbar plugin == Version: 3.0.4, License: MIT License (MIT) */
(function(b,a,c){(function(d){d(jQuery)}(function(j){var g="mCustomScrollbar",d="mCS",m=".mCustomScrollbar",h={setWidth:false,setHeight:false,setTop:0,setLeft:0,axis:"y",scrollbarPosition:"inside",scrollInertia:950,autoDraggerLength:true,autoHideScrollbar:false,autoExpandScrollbar:false,alwaysShowScrollbar:0,snapAmount:null,snapOffset:0,mouseWheel:{enable:true,scrollAmount:"auto",axis:"y",preventDefault:false,deltaFactor:"auto",normalizeDelta:false,invert:false,disableOver:["select","option","keygen","datalist","textarea"]},scrollButtons:{enable:false,scrollType:"stepless",scrollAmount:"auto"},keyboard:{enable:true,scrollType:"stepless",scrollAmount:"auto"},contentTouchScroll:25,advanced:{autoExpandHorizontalScroll:false,autoScrollOnFocus:"input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",updateOnContentResize:true,updateOnImageLoad:true,updateOnSelectorChange:false},theme:"light",callbacks:{onScrollStart:false,onScroll:false,onTotalScroll:false,onTotalScrollBack:false,whileScrolling:false,onTotalScrollOffset:0,onTotalScrollBackOffset:0,alwaysTriggerOffsets:true,onOverflowY:false,onOverflowX:false,onOverflowYNone:false,onOverflowXNone:false},live:false,liveSelector:null},l=0,o={},f=function(p){if(o[p]){clearTimeout(o[p]);i._delete.call(null,o[p])}},k=(b.attachEvent&&!b.addEventListener)?1:0,n=false,e={init:function(q){var q=j.extend(true,{},h,q),p=i._selector.call(this);if(q.live){var s=q.liveSelector||this.selector||m,r=j(s);if(q.live==="off"){f(s);return}o[s]=setTimeout(function(){r.mCustomScrollbar(q);if(q.live==="once"&&r.length){f(s)}},500)}else{f(s)}q.setWidth=(q.set_width)?q.set_width:q.setWidth;q.setHeight=(q.set_height)?q.set_height:q.setHeight;q.axis=(q.horizontalScroll)?"x":i._findAxis.call(null,q.axis);q.scrollInertia=q.scrollInertia>0&&q.scrollInertia<17?17:q.scrollInertia;if(typeof q.mouseWheel!=="object"&&q.mouseWheel==true){q.mouseWheel={enable:true,scrollAmount:"auto",axis:"y",preventDefault:false,deltaFactor:"auto",normalizeDelta:false,invert:false}}q.mouseWheel.scrollAmount=!q.mouseWheelPixels?q.mouseWheel.scrollAmount:q.mouseWheelPixels;q.mouseWheel.normalizeDelta=!q.advanced.normalizeMouseWheelDelta?q.mouseWheel.normalizeDelta:q.advanced.normalizeMouseWheelDelta;q.scrollButtons.scrollType=i._findScrollButtonsType.call(null,q.scrollButtons.scrollType);i._theme.call(null,q);return j(p).each(function(){var u=j(this);if(!u.data(d)){u.data(d,{idx:++l,opt:q,scrollRatio:{y:null,x:null},overflowed:null,contentReset:{y:null,x:null},bindEvents:false,tweenRunning:false,sequential:{},langDir:u.css("direction"),cbOffsets:null,trigger:null});var w=u.data(d).opt,v=u.data("mcs-axis"),t=u.data("mcs-scrollbar-position"),x=u.data("mcs-theme");if(v){w.axis=v}if(t){w.scrollbarPosition=t}if(x){w.theme=x;i._theme.call(null,w)}i._pluginMarkup.call(this);e.update.call(null,u)}})},update:function(q){var p=q||i._selector.call(this);return j(p).each(function(){var t=j(this);if(t.data(d)){var v=t.data(d),u=v.opt,r=j("#mCSB_"+v.idx+"_container"),s=[j("#mCSB_"+v.idx+"_dragger_vertical"),j("#mCSB_"+v.idx+"_dragger_horizontal")];if(!r.length){return}if(v.tweenRunning){i._stop.call(null,t)}if(t.hasClass("mCS_disabled")){t.removeClass("mCS_disabled")}if(t.hasClass("mCS_destroyed")){t.removeClass("mCS_destroyed")}i._maxHeight.call(this);i._expandContentHorizontally.call(this);if(u.axis!=="y"&&!u.advanced.autoExpandHorizontalScroll){r.css("width",i._contentWidth(r.children()))}v.overflowed=i._overflowed.call(this);i._scrollbarVisibility.call(this);if(u.autoDraggerLength){i._setDraggerLength.call(this)}i._scrollRatio.call(this);i._bindEvents.call(this);var w=[Math.abs(r[0].offsetTop),Math.abs(r[0].offsetLeft)];if(u.axis!=="x"){if(!v.overflowed[0]){i._resetContentPosition.call(this);if(u.axis==="y"){i._unbindEvents.call(this)}else{if(u.axis==="yx"&&v.overflowed[1]){i._scrollTo.call(this,t,w[1].toString(),{dir:"x",dur:0,overwrite:"none"})}}}else{if(s[0].height()>s[0].parent().height()){i._resetContentPosition.call(this)}else{i._scrollTo.call(this,t,w[0].toString(),{dir:"y",dur:0,overwrite:"none"});v.contentReset.y=null}}}if(u.axis!=="y"){if(!v.overflowed[1]){i._resetContentPosition.call(this);if(u.axis==="x"){i._unbindEvents.call(this)}else{if(u.axis==="yx"&&v.overflowed[0]){i._scrollTo.call(this,t,w[0].toString(),{dir:"y",dur:0,overwrite:"none"})}}}else{if(s[1].width()>s[1].parent().width()){i._resetContentPosition.call(this)}else{i._scrollTo.call(this,t,w[1].toString(),{dir:"x",dur:0,overwrite:"none"});v.contentReset.x=null}}}i._autoUpdate.call(this)}})},scrollTo:function(r,q){if(typeof r=="undefined"||r==null){return}var p=i._selector.call(this);return j(p).each(function(){var u=j(this);if(u.data(d)){var x=u.data(d),w=x.opt,v={trigger:"external",scrollInertia:w.scrollInertia,scrollEasing:"mcsEaseInOut",moveDragger:false,timeout:60,callbacks:true,onStart:true,onUpdate:true,onComplete:true},s=j.extend(true,{},v,q),y=i._arr.call(this,r),t=s.scrollInertia>0&&s.scrollInertia<17?17:s.scrollInertia;y[0]=i._to.call(this,y[0],"y");y[1]=i._to.call(this,y[1],"x");if(s.moveDragger){y[0]*=x.scrollRatio.y;y[1]*=x.scrollRatio.x}s.dur=t;setTimeout(function(){if(y[0]!==null&&typeof y[0]!=="undefined"&&w.axis!=="x"&&x.overflowed[0]){s.dir="y";s.overwrite="all";i._scrollTo.call(this,u,y[0].toString(),s)}if(y[1]!==null&&typeof y[1]!=="undefined"&&w.axis!=="y"&&x.overflowed[1]){s.dir="x";s.overwrite="none";i._scrollTo.call(this,u,y[1].toString(),s)}},s.timeout)}})},stop:function(){var p=i._selector.call(this);return j(p).each(function(){var q=j(this);if(q.data(d)){i._stop.call(null,q)}})},disable:function(q){var p=i._selector.call(this);return j(p).each(function(){var r=j(this);if(r.data(d)){var t=r.data(d),s=t.opt;i._autoUpdate.call(this,"remove");i._unbindEvents.call(this);if(q){i._resetContentPosition.call(this)}i._scrollbarVisibility.call(this,true);r.addClass("mCS_disabled")}})},destroy:function(){var p=i._selector.call(this);return j(p).each(function(){var s=j(this);if(s.data(d)){var u=s.data(d),t=u.opt,q=j("#mCSB_"+u.idx),r=j("#mCSB_"+u.idx+"_container"),v=j(".mCSB_"+u.idx+"_scrollbar");if(t.live){f(p)}i._autoUpdate.call(this,"remove");i._unbindEvents.call(this);i._resetContentPosition.call(this);s.removeData(d);i._delete.call(null,this.mcs);v.remove();q.replaceWith(r.contents());s.removeClass(g+" _"+d+"_"+u.idx+" mCS-autoHide mCS-dir-rtl mCS_no_scrollbar mCS_disabled").addClass("mCS_destroyed")}})}},i={_selector:function(){return(typeof j(this)!=="object"||j(this).length<1)?m:this},_theme:function(s){var r=["rounded","rounded-dark","rounded-dots","rounded-dots-dark"],q=["rounded-dots","rounded-dots-dark","3d","3d-dark","3d-thick","3d-thick-dark","inset","inset-dark","inset-2","inset-2-dark","inset-3","inset-3-dark"],p=["minimal","minimal-dark"],u=["minimal","minimal-dark"],t=["minimal","minimal-dark"];s.autoDraggerLength=j.inArray(s.theme,r)>-1?false:s.autoDraggerLength;s.autoExpandScrollbar=j.inArray(s.theme,q)>-1?false:s.autoExpandScrollbar;s.scrollButtons.enable=j.inArray(s.theme,p)>-1?false:s.scrollButtons.enable;s.autoHideScrollbar=j.inArray(s.theme,u)>-1?true:s.autoHideScrollbar;s.scrollbarPosition=j.inArray(s.theme,t)>-1?"outside":s.scrollbarPosition},_findAxis:function(p){return(p==="yx"||p==="xy"||p==="auto")?"yx":(p==="x"||p==="horizontal")?"x":"y"},_findScrollButtonsType:function(p){return(p==="stepped"||p==="pixels"||p==="step"||p==="click")?"stepped":"stepless"},_pluginMarkup:function(){var y=j(this),x=y.data(d),r=x.opt,t=r.autoExpandScrollbar?" mCSB_scrollTools_onDrag_expand":"",B=["<div id='mCSB_"+x.idx+"_scrollbar_vertical' class='mCSB_scrollTools mCSB_"+x.idx+"_scrollbar mCS-"+r.theme+" mCSB_scrollTools_vertical"+t+"'><div class='mCSB_draggerContainer'><div id='mCSB_"+x.idx+"_dragger_vertical' class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>","<div id='mCSB_"+x.idx+"_scrollbar_horizontal' class='mCSB_scrollTools mCSB_"+x.idx+"_scrollbar mCS-"+r.theme+" mCSB_scrollTools_horizontal"+t+"'><div class='mCSB_draggerContainer'><div id='mCSB_"+x.idx+"_dragger_horizontal' class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"],u=r.axis==="yx"?"mCSB_vertical_horizontal":r.axis==="x"?"mCSB_horizontal":"mCSB_vertical",w=r.axis==="yx"?B[0]+B[1]:r.axis==="x"?B[1]:B[0],v=r.axis==="yx"?"<div id='mCSB_"+x.idx+"_container_wrapper' class='mCSB_container_wrapper' />":"",s=r.autoHideScrollbar?" mCS-autoHide":"",p=(r.axis!=="x"&&x.langDir==="rtl")?" mCS-dir-rtl":"";if(r.setWidth){y.css("width",r.setWidth)}if(r.setHeight){y.css("height",r.setHeight)}r.setLeft=(r.axis!=="y"&&x.langDir==="rtl")?"989999px":r.setLeft;y.addClass(g+" _"+d+"_"+x.idx+s+p).wrapInner("<div id='mCSB_"+x.idx+"' class='mCustomScrollBox mCS-"+r.theme+" "+u+"'><div id='mCSB_"+x.idx+"_container' class='mCSB_container' style='position:relative; top:"+r.setTop+"; left:"+r.setLeft+";' dir="+x.langDir+" /></div>");var q=j("#mCSB_"+x.idx),z=j("#mCSB_"+x.idx+"_container");if(r.axis!=="y"&&!r.advanced.autoExpandHorizontalScroll){z.css("width",i._contentWidth(z.children()))}if(r.scrollbarPosition==="outside"){if(y.css("position")==="static"){y.css("position","relative")}y.css("overflow","visible");q.addClass("mCSB_outside").after(w)}else{q.addClass("mCSB_inside").append(w);z.wrap(v)}i._scrollButtons.call(this);var A=[j("#mCSB_"+x.idx+"_dragger_vertical"),j("#mCSB_"+x.idx+"_dragger_horizontal")];A[0].css("min-height",A[0].height());A[1].css("min-width",A[1].width())},_contentWidth:function(p){return Math.max.apply(Math,p.map(function(){return j(this).outerWidth(true)}).get())},_expandContentHorizontally:function(){var q=j(this),s=q.data(d),r=s.opt,p=j("#mCSB_"+s.idx+"_container");if(r.advanced.autoExpandHorizontalScroll&&r.axis!=="y"){p.css({position:"absolute",width:"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:(Math.ceil(p[0].getBoundingClientRect().right+0.4)-Math.floor(p[0].getBoundingClientRect().left)),position:"relative"}).unwrap()}},_scrollButtons:function(){var s=j(this),u=s.data(d),t=u.opt,q=j(".mCSB_"+u.idx+"_scrollbar:first"),r=["<a href='#' class='mCSB_buttonUp' oncontextmenu='return false;' />","<a href='#' class='mCSB_buttonDown' oncontextmenu='return false;' />","<a href='#' class='mCSB_buttonLeft' oncontextmenu='return false;' />","<a href='#' class='mCSB_buttonRight' oncontextmenu='return false;' />"],p=[(t.axis==="x"?r[2]:r[0]),(t.axis==="x"?r[3]:r[1]),r[2],r[3]];if(t.scrollButtons.enable){q.prepend(p[0]).append(p[1]).next(".mCSB_scrollTools").prepend(p[2]).append(p[3])}},_maxHeight:function(){var t=j(this),w=t.data(d),v=w.opt,r=j("#mCSB_"+w.idx),q=t.css("max-height"),s=q.indexOf("%")!==-1,p=t.css("box-sizing");if(q!=="none"){var u=s?t.parent().height()*parseInt(q)/100:parseInt(q);if(p==="border-box"){u-=((t.innerHeight()-t.height())+(t.outerHeight()-t.innerHeight()))}r.css("max-height",Math.round(u))}},_setDraggerLength:function(){var u=j(this),s=u.data(d),p=j("#mCSB_"+s.idx),v=j("#mCSB_"+s.idx+"_container"),y=[j("#mCSB_"+s.idx+"_dragger_vertical"),j("#mCSB_"+s.idx+"_dragger_horizontal")],t=[p.height()/v.outerHeight(false),p.width()/v.outerWidth(false)],q=[parseInt(y[0].css("min-height")),Math.round(t[0]*y[0].parent().height()),parseInt(y[1].css("min-width")),Math.round(t[1]*y[1].parent().width())],r=k&&(q[1]<q[0])?q[0]:q[1],x=k&&(q[3]<q[2])?q[2]:q[3];y[0].css({height:r,"max-height":(y[0].parent().height()-10)}).find(".mCSB_dragger_bar").css({"line-height":q[0]+"px"});y[1].css({width:x,"max-width":(y[1].parent().width()-10)})},_scrollRatio:function(){var t=j(this),v=t.data(d),q=j("#mCSB_"+v.idx),r=j("#mCSB_"+v.idx+"_container"),s=[j("#mCSB_"+v.idx+"_dragger_vertical"),j("#mCSB_"+v.idx+"_dragger_horizontal")],u=[r.outerHeight(false)-q.height(),r.outerWidth(false)-q.width()],p=[u[0]/(s[0].parent().height()-s[0].height()),u[1]/(s[1].parent().width()-s[1].width())];v.scrollRatio={y:p[0],x:p[1]}},_onDragClasses:function(r,t,q){var s=q?"mCSB_dragger_onDrag_expanded":"",p=["mCSB_dragger_onDrag","mCSB_scrollTools_onDrag"],u=r.closest(".mCSB_scrollTools");if(t==="active"){r.toggleClass(p[0]+" "+s);u.toggleClass(p[1]);r[0]._draggable=r[0]._draggable?0:1}else{if(!r[0]._draggable){if(t==="hide"){r.removeClass(p[0]);u.removeClass(p[1])}else{r.addClass(p[0]);u.addClass(p[1])}}}},_overflowed:function(){var t=j(this),u=t.data(d),q=j("#mCSB_"+u.idx),s=j("#mCSB_"+u.idx+"_container"),r=u.overflowed==null?s.height():s.outerHeight(false),p=u.overflowed==null?s.width():s.outerWidth(false);return[r>q.height(),p>q.width()]},_resetContentPosition:function(){var t=j(this),v=t.data(d),u=v.opt,q=j("#mCSB_"+v.idx),r=j("#mCSB_"+v.idx+"_container"),s=[j("#mCSB_"+v.idx+"_dragger_vertical"),j("#mCSB_"+v.idx+"_dragger_horizontal")];i._stop(t);if((u.axis!=="x"&&!v.overflowed[0])||(u.axis==="y"&&v.overflowed[0])){s[0].add(r).css("top",0);i._scrollTo(t,"_resetY")}if((u.axis!=="y"&&!v.overflowed[1])||(u.axis==="x"&&v.overflowed[1])){var p=dx=0;if(v.langDir==="rtl"){p=q.width()-r.outerWidth(false);dx=Math.abs(p/v.scrollRatio.x)}r.css("left",p);s[1].css("left",dx);i._scrollTo(t,"_resetX")}},_bindEvents:function(){var r=j(this),t=r.data(d),s=t.opt;if(!t.bindEvents){i._draggable.call(this);if(s.contentTouchScroll){i._contentDraggable.call(this)}if(s.mouseWheel.enable){function q(){p=setTimeout(function(){if(!j.event.special.mousewheel){q()}else{clearTimeout(p);i._mousewheel.call(r[0])}},1000)}var p;q()}i._draggerRail.call(this);i._wrapperScroll.call(this);if(s.advanced.autoScrollOnFocus){i._focus.call(this)}if(s.scrollButtons.enable){i._buttons.call(this)}if(s.keyboard.enable){i._keyboard.call(this)}t.bindEvents=true}},_unbindEvents:function(){var s=j(this),t=s.data(d),p=d+"_"+t.idx,u=".mCSB_"+t.idx+"_scrollbar",r=j("#mCSB_"+t.idx+",#mCSB_"+t.idx+"_container,#mCSB_"+t.idx+"_container_wrapper,"+u+" .mCSB_draggerContainer,#mCSB_"+t.idx+"_dragger_vertical,#mCSB_"+t.idx+"_dragger_horizontal,"+u+">a"),q=j("#mCSB_"+t.idx+"_container");if(t.bindEvents){j(a).unbind("."+p);r.each(function(){j(this).unbind("."+p)});clearTimeout(s[0]._focusTimeout);i._delete.call(null,s[0]._focusTimeout);clearTimeout(t.sequential.step);i._delete.call(null,t.sequential.step);clearTimeout(q[0].onCompleteTimeout);i._delete.call(null,q[0].onCompleteTimeout);t.bindEvents=false}},_scrollbarVisibility:function(q){var t=j(this),v=t.data(d),u=v.opt,p=j("#mCSB_"+v.idx+"_container_wrapper"),r=p.length?p:j("#mCSB_"+v.idx+"_container"),w=[j("#mCSB_"+v.idx+"_scrollbar_vertical"),j("#mCSB_"+v.idx+"_scrollbar_horizontal")],s=[w[0].find(".mCSB_dragger"),w[1].find(".mCSB_dragger")];if(u.axis!=="x"){if(v.overflowed[0]&&!q){w[0].add(s[0]).add(w[0].children("a")).css("display","block");r.removeClass("mCS_no_scrollbar_y mCS_y_hidden")}else{if(u.alwaysShowScrollbar){if(u.alwaysShowScrollbar!==2){s[0].add(w[0].children("a")).css("display","none")}r.removeClass("mCS_y_hidden")}else{w[0].css("display","none");r.addClass("mCS_y_hidden")}r.addClass("mCS_no_scrollbar_y")}}if(u.axis!=="y"){if(v.overflowed[1]&&!q){w[1].add(s[1]).add(w[1].children("a")).css("display","block");r.removeClass("mCS_no_scrollbar_x mCS_x_hidden")}else{if(u.alwaysShowScrollbar){if(u.alwaysShowScrollbar!==2){s[1].add(w[1].children("a")).css("display","none")}r.removeClass("mCS_x_hidden")}else{w[1].css("display","none");r.addClass("mCS_x_hidden")}r.addClass("mCS_no_scrollbar_x")}}if(!v.overflowed[0]&&!v.overflowed[1]){t.addClass("mCS_no_scrollbar")}else{t.removeClass("mCS_no_scrollbar")}},_coordinates:function(q){var p=q.type;switch(p){case"pointerdown":case"MSPointerDown":case"pointermove":case"MSPointerMove":case"pointerup":case"MSPointerUp":return[q.originalEvent.pageY,q.originalEvent.pageX];break;case"touchstart":case"touchmove":case"touchend":var r=q.originalEvent.touches[0]||q.originalEvent.changedTouches[0];return[r.pageY,r.pageX];break;default:return[q.pageY,q.pageX]}},_draggable:function(){var u=j(this),s=u.data(d),p=s.opt,r=d+"_"+s.idx,t=["mCSB_"+s.idx+"_dragger_vertical","mCSB_"+s.idx+"_dragger_horizontal"],v=j("#mCSB_"+s.idx+"_container"),w=j("#"+t[0]+",#"+t[1]),A,y,z;w.bind("mousedown."+r+" touchstart."+r+" pointerdown."+r+" MSPointerDown."+r,function(E){E.stopImmediatePropagation();E.preventDefault();if(!i._mouseBtnLeft(E)){return}n=true;if(k){a.onselectstart=function(){return false}}x(false);i._stop(u);A=j(this);var F=A.offset(),G=i._coordinates(E)[0]-F.top,B=i._coordinates(E)[1]-F.left,D=A.height()+F.top,C=A.width()+F.left;if(G<D&&G>0&&B<C&&B>0){y=G;z=B}i._onDragClasses(A,"active",p.autoExpandScrollbar)}).bind("touchmove."+r,function(C){C.stopImmediatePropagation();C.preventDefault();var D=A.offset(),E=i._coordinates(C)[0]-D.top,B=i._coordinates(C)[1]-D.left;q(y,z,E,B)});j(a).bind("mousemove."+r+" pointermove."+r+" MSPointerMove."+r,function(C){if(A){var D=A.offset(),E=i._coordinates(C)[0]-D.top,B=i._coordinates(C)[1]-D.left;if(y===E){return}q(y,z,E,B)}}).add(w).bind("mouseup."+r+" touchend."+r+" pointerup."+r+" MSPointerUp."+r,function(B){if(A){i._onDragClasses(A,"active",p.autoExpandScrollbar);A=null}n=false;if(k){a.onselectstart=null}x(true)});function x(B){var C=v.find("iframe");if(!C.length){return}var D=!B?"none":"auto";C.css("pointer-events",D)}function q(D,E,G,B){v[0].idleTimer=p.scrollInertia<233?250:0;if(A.attr("id")===t[1]){var C="x",F=((A[0].offsetLeft-E)+B)*s.scrollRatio.x}else{var C="y",F=((A[0].offsetTop-D)+G)*s.scrollRatio.y}i._scrollTo(u,F.toString(),{dir:C,drag:true})}},_contentDraggable:function(){var y=j(this),K=y.data(d),I=K.opt,F=d+"_"+K.idx,v=j("#mCSB_"+K.idx),z=j("#mCSB_"+K.idx+"_container"),w=[j("#mCSB_"+K.idx+"_dragger_vertical"),j("#mCSB_"+K.idx+"_dragger_horizontal")],E,G,L,M,C=[],D=[],H,A,u,t,J,x,r=0,q,s=I.axis==="yx"?"none":"all";z.bind("touchstart."+F+" pointerdown."+F+" MSPointerDown."+F,function(N){if(!i._pointerTouch(N)||n){return}var O=z.offset();E=i._coordinates(N)[0]-O.top;G=i._coordinates(N)[1]-O.left}).bind("touchmove."+F+" pointermove."+F+" MSPointerMove."+F,function(Q){if(!i._pointerTouch(Q)||n){return}Q.stopImmediatePropagation();A=i._getTime();var P=v.offset(),S=i._coordinates(Q)[0]-P.top,U=i._coordinates(Q)[1]-P.left,R="mcsLinearOut";C.push(S);D.push(U);if(K.overflowed[0]){var O=w[0].parent().height()-w[0].height(),T=((E-S)>0&&(S-E)>-(O*K.scrollRatio.y))}if(K.overflowed[1]){var N=w[1].parent().width()-w[1].width(),V=((G-U)>0&&(U-G)>-(N*K.scrollRatio.x))}if(T||V){Q.preventDefault()}x=I.axis==="yx"?[(E-S),(G-U)]:I.axis==="x"?[null,(G-U)]:[(E-S),null];z[0].idleTimer=250;if(K.overflowed[0]){B(x[0],r,R,"y","all",true)}if(K.overflowed[1]){B(x[1],r,R,"x",s,true)}});v.bind("touchstart."+F+" pointerdown."+F+" MSPointerDown."+F,function(N){if(!i._pointerTouch(N)||n){return}N.stopImmediatePropagation();i._stop(y);H=i._getTime();var O=v.offset();L=i._coordinates(N)[0]-O.top;M=i._coordinates(N)[1]-O.left;C=[];D=[]}).bind("touchend."+F+" pointerup."+F+" MSPointerUp."+F,function(P){if(!i._pointerTouch(P)||n){return}P.stopImmediatePropagation();u=i._getTime();var N=v.offset(),T=i._coordinates(P)[0]-N.top,V=i._coordinates(P)[1]-N.left;if((u-A)>30){return}J=1000/(u-H);var Q="mcsEaseOut",R=J<2.5,W=R?[C[C.length-2],D[D.length-2]]:[0,0];t=R?[(T-W[0]),(V-W[1])]:[T-L,V-M];var O=[Math.abs(t[0]),Math.abs(t[1])];J=R?[Math.abs(t[0]/4),Math.abs(t[1]/4)]:[J,J];var U=[Math.abs(z[0].offsetTop)-(t[0]*p((O[0]/J[0]),J[0])),Math.abs(z[0].offsetLeft)-(t[1]*p((O[1]/J[1]),J[1]))];x=I.axis==="yx"?[U[0],U[1]]:I.axis==="x"?[null,U[1]]:[U[0],null];q=[(O[0]*4)+I.scrollInertia,(O[1]*4)+I.scrollInertia];var S=parseInt(I.contentTouchScroll)||0;x[0]=O[0]>S?x[0]:0;x[1]=O[1]>S?x[1]:0;if(K.overflowed[0]){B(x[0],q[0],Q,"y",s,false)}if(K.overflowed[1]){B(x[1],q[1],Q,"x",s,false)}});function p(P,N){var O=[N*1.5,N*2,N/1.5,N/2];if(P>90){return N>4?O[0]:O[3]}else{if(P>60){return N>3?O[3]:O[2]}else{if(P>30){return N>8?O[1]:N>6?O[0]:N>4?N:O[2]}else{return N>8?N:O[3]}}}}function B(P,R,S,O,N,Q){if(!P){return}i._scrollTo(y,P.toString(),{dur:R,scrollEasing:S,dir:O,overwrite:N,drag:Q})}},_mousewheel:function(){var w=j(this),v=w.data(d);if(v){var p=v.opt,s=d+"_"+v.idx,q=j("#mCSB_"+v.idx),x=[j("#mCSB_"+v.idx+"_dragger_vertical"),j("#mCSB_"+v.idx+"_dragger_horizontal")],t=j("#mCSB_"+v.idx+"_container").find("iframe"),r=q;if(t.length){t.each(function(){var y=this;if(u(y)){r=r.add(j(y).contents().find("body"))}})}r.bind("mousewheel."+s,function(C,G){i._stop(w);if(i._disableMousewheel(w,C.target)){return}var E=p.mouseWheel.deltaFactor!=="auto"?parseInt(p.mouseWheel.deltaFactor):(k&&C.deltaFactor<100)?100:C.deltaFactor||100;if(p.axis==="x"||p.mouseWheel.axis==="x"){var z="x",F=[Math.round(E*v.scrollRatio.x),parseInt(p.mouseWheel.scrollAmount)],B=p.mouseWheel.scrollAmount!=="auto"?F[1]:F[0]>=q.width()?q.width()*0.9:F[0],H=Math.abs(j("#mCSB_"+v.idx+"_container")[0].offsetLeft),D=x[1][0].offsetLeft,A=x[1].parent().width()-x[1].width(),y=C.deltaX||C.deltaY||G}else{var z="y",F=[Math.round(E*v.scrollRatio.y),parseInt(p.mouseWheel.scrollAmount)],B=p.mouseWheel.scrollAmount!=="auto"?F[1]:F[0]>=q.height()?q.height()*0.9:F[0],H=Math.abs(j("#mCSB_"+v.idx+"_container")[0].offsetTop),D=x[0][0].offsetTop,A=x[0].parent().height()-x[0].height(),y=C.deltaY||G}if((z==="y"&&!v.overflowed[0])||(z==="x"&&!v.overflowed[1])){return}if(p.mouseWheel.invert){y=-y}if(p.mouseWheel.normalizeDelta){y=y<0?-1:1}if((y>0&&D!==0)||(y<0&&D!==A)||p.mouseWheel.preventDefault){C.stopImmediatePropagation();C.preventDefault()}i._scrollTo(w,(H-(y*B)).toString(),{dir:z})})}function u(z){var y=null;try{var B=z.contentDocument||z.contentWindow.document;y=B.body.innerHTML}catch(A){}return(y!==null)}},_disableMousewheel:function(r,t){var p=t.nodeName.toLowerCase(),q=r.data(d).opt.mouseWheel.disableOver,s=["select","textarea"];return j.inArray(p,q)>-1&&!(j.inArray(p,s)>-1&&!j(t).is(":focus"))},_draggerRail:function(){var s=j(this),t=s.data(d),q=d+"_"+t.idx,r=j("#mCSB_"+t.idx+"_container"),u=r.parent(),p=j(".mCSB_"+t.idx+"_scrollbar .mCSB_draggerContainer");p.bind("touchstart."+q+" pointerdown."+q+" MSPointerDown."+q,function(v){n=true}).bind("touchend."+q+" pointerup."+q+" MSPointerUp."+q,function(v){n=false}).bind("click."+q,function(z){if(j(z.target).hasClass("mCSB_draggerContainer")||j(z.target).hasClass("mCSB_draggerRail")){i._stop(s);var w=j(this),y=w.find(".mCSB_dragger");if(w.parent(".mCSB_scrollTools_horizontal").length>0){if(!t.overflowed[1]){return}var v="x",x=z.pageX>y.offset().left?-1:1,A=Math.abs(r[0].offsetLeft)-(x*(u.width()*0.9))}else{if(!t.overflowed[0]){return}var v="y",x=z.pageY>y.offset().top?-1:1,A=Math.abs(r[0].offsetTop)-(x*(u.height()*0.9))}i._scrollTo(s,A.toString(),{dir:v,scrollEasing:"mcsEaseInOut"})}})},_focus:function(){var r=j(this),t=r.data(d),s=t.opt,p=d+"_"+t.idx,q=j("#mCSB_"+t.idx+"_container"),u=q.parent();q.bind("focusin."+p,function(x){var w=j(a.activeElement),y=q.find(".mCustomScrollBox").length,v=0;if(!w.is(s.advanced.autoScrollOnFocus)){return}i._stop(r);clearTimeout(r[0]._focusTimeout);r[0]._focusTimer=y?(v+17)*y:0;r[0]._focusTimeout=setTimeout(function(){var C=[w.offset().top-q.offset().top,w.offset().left-q.offset().left],B=[q[0].offsetTop,q[0].offsetLeft],z=[(B[0]+C[0]>=0&&B[0]+C[0]<u.height()-w.outerHeight(false)),(B[1]+C[1]>=0&&B[0]+C[1]<u.width()-w.outerWidth(false))],A=(s.axis==="yx"&&!z[0]&&!z[1])?"none":"all";if(s.axis!=="x"&&!z[0]){i._scrollTo(r,C[0].toString(),{dir:"y",scrollEasing:"mcsEaseInOut",overwrite:A,dur:v})}if(s.axis!=="y"&&!z[1]){i._scrollTo(r,C[1].toString(),{dir:"x",scrollEasing:"mcsEaseInOut",overwrite:A,dur:v})}},r[0]._focusTimer)})},_wrapperScroll:function(){var q=j(this),r=q.data(d),p=d+"_"+r.idx,s=j("#mCSB_"+r.idx+"_container").parent();s.bind("scroll."+p,function(t){s.scrollTop(0).scrollLeft(0)})},_buttons:function(){var u=j(this),w=u.data(d),v=w.opt,p=w.sequential,r=d+"_"+w.idx,t=j("#mCSB_"+w.idx+"_container"),s=".mCSB_"+w.idx+"_scrollbar",q=j(s+">a");q.bind("mousedown."+r+" touchstart."+r+" pointerdown."+r+" MSPointerDown."+r+" mouseup."+r+" touchend."+r+" pointerup."+r+" MSPointerUp."+r+" mouseout."+r+" pointerout."+r+" MSPointerOut."+r+" click."+r,function(z){z.preventDefault();if(!i._mouseBtnLeft(z)){return}var y=j(this).attr("class");p.type=v.scrollButtons.scrollType;switch(z.type){case"mousedown":case"touchstart":case"pointerdown":case"MSPointerDown":if(p.type==="stepped"){return}n=true;w.tweenRunning=false;x("on",y);break;case"mouseup":case"touchend":case"pointerup":case"MSPointerUp":case"mouseout":case"pointerout":case"MSPointerOut":if(p.type==="stepped"){return}n=false;if(p.dir){x("off",y)}break;case"click":if(p.type!=="stepped"||w.tweenRunning){return}x("on",y);break}function x(A,B){p.scrollAmount=v.snapAmount||v.scrollButtons.scrollAmount;i._sequentialScroll.call(this,u,A,B)}})},_keyboard:function(){var u=j(this),t=u.data(d),q=t.opt,x=t.sequential,s=d+"_"+t.idx,r=j("#mCSB_"+t.idx),w=j("#mCSB_"+t.idx+"_container"),p=w.parent(),v="input,textarea,select,datalist,keygen,[contenteditable='true']";r.attr("tabindex","0").bind("blur."+s+" keydown."+s+" keyup."+s,function(D){switch(D.type){case"blur":if(t.tweenRunning&&x.dir){y("off",null)}break;case"keydown":case"keyup":var A=D.keyCode?D.keyCode:D.which,B="on";if((q.axis!=="x"&&(A===38||A===40))||(q.axis!=="y"&&(A===37||A===39))){if(((A===38||A===40)&&!t.overflowed[0])||((A===37||A===39)&&!t.overflowed[1])){return}if(D.type==="keyup"){B="off"}if(!j(a.activeElement).is(v)){D.preventDefault();D.stopImmediatePropagation();y(B,A)}}else{if(A===33||A===34){if(t.overflowed[0]||t.overflowed[1]){D.preventDefault();D.stopImmediatePropagation()}if(D.type==="keyup"){i._stop(u);var C=A===34?-1:1;if(q.axis==="x"||(q.axis==="yx"&&t.overflowed[1]&&!t.overflowed[0])){var z="x",E=Math.abs(w[0].offsetLeft)-(C*(p.width()*0.9))}else{var z="y",E=Math.abs(w[0].offsetTop)-(C*(p.height()*0.9))}i._scrollTo(u,E.toString(),{dir:z,scrollEasing:"mcsEaseInOut"})}}else{if(A===35||A===36){if(!j(a.activeElement).is(v)){if(t.overflowed[0]||t.overflowed[1]){D.preventDefault();D.stopImmediatePropagation()}if(D.type==="keyup"){if(q.axis==="x"||(q.axis==="yx"&&t.overflowed[1]&&!t.overflowed[0])){var z="x",E=A===35?Math.abs(p.width()-w.outerWidth(false)):0}else{var z="y",E=A===35?Math.abs(p.height()-w.outerHeight(false)):0}i._scrollTo(u,E.toString(),{dir:z,scrollEasing:"mcsEaseInOut"})}}}}}break}function y(F,G){x.type=q.keyboard.scrollType;x.scrollAmount=q.snapAmount||q.keyboard.scrollAmount;if(x.type==="stepped"&&t.tweenRunning){return}i._sequentialScroll.call(this,u,F,G)}})},_sequentialScroll:function(r,u,s){var w=r.data(d),q=w.opt,y=w.sequential,x=j("#mCSB_"+w.idx+"_container"),p=y.type==="stepped"?true:false;switch(u){case"on":y.dir=[(s==="mCSB_buttonRight"||s==="mCSB_buttonLeft"||s===39||s===37?"x":"y"),(s==="mCSB_buttonUp"||s==="mCSB_buttonLeft"||s===38||s===37?-1:1)];i._stop(r);if(i._isNumeric(s)&&y.type==="stepped"){return}t(p);break;case"off":v();if(p||(w.tweenRunning&&y.dir)){t(true)}break}function t(z){var F=y.type!=="stepped",J=!z?1000/60:F?q.scrollInertia/1.5:q.scrollInertia,B=!z?2.5:F?7.5:40,I=[Math.abs(x[0].offsetTop),Math.abs(x[0].offsetLeft)],E=[w.scrollRatio.y>10?10:w.scrollRatio.y,w.scrollRatio.x>10?10:w.scrollRatio.x],C=y.dir[0]==="x"?I[1]+(y.dir[1]*(E[1]*B)):I[0]+(y.dir[1]*(E[0]*B)),H=y.dir[0]==="x"?I[1]+(y.dir[1]*parseInt(y.scrollAmount)):I[0]+(y.dir[1]*parseInt(y.scrollAmount)),G=y.scrollAmount!=="auto"?H:C,D=!z?"mcsLinear":F?"mcsLinearOut":"mcsEaseInOut",A=!z?false:true;if(z&&J<17){G=y.dir[0]==="x"?I[1]:I[0]}i._scrollTo(r,G.toString(),{dir:y.dir[0],scrollEasing:D,dur:J,onComplete:A});if(z){y.dir=false;return}clearTimeout(y.step);y.step=setTimeout(function(){t()},J)}function v(){clearTimeout(y.step);i._stop(r)}},_arr:function(r){var q=j(this).data(d).opt,p=[];if(typeof r==="function"){r=r()}if(!(r instanceof Array)){p[0]=r.y?r.y:r.x||q.axis==="x"?null:r;p[1]=r.x?r.x:r.y||q.axis==="y"?null:r}else{p=r.length>1?[r[0],r[1]]:q.axis==="x"?[null,r[0]]:[r[0],null]}if(typeof p[0]==="function"){p[0]=p[0]()}if(typeof p[1]==="function"){p[1]=p[1]()}return p},_to:function(v,w){if(v==null||typeof v=="undefined"){return}var C=j(this),B=C.data(d),u=B.opt,D=j("#mCSB_"+B.idx+"_container"),r=D.parent(),F=typeof v;if(!w){w=u.axis==="x"?"x":"y"}var q=w==="x"?D.outerWidth(false):D.outerHeight(false),x=w==="x"?D.offset().left:D.offset().top,E=w==="x"?D[0].offsetLeft:D[0].offsetTop,z=w==="x"?"left":"top";switch(F){case"function":return v();break;case"object":if(v.nodeType){var A=w==="x"?j(v).offset().left:j(v).offset().top}else{if(v.jquery){if(!v.length){return}var A=w==="x"?v.offset().left:v.offset().top}}return A-x;break;case"string":case"number":if(i._isNumeric.call(null,v)){return Math.abs(v)}else{if(v.indexOf("%")!==-1){return Math.abs(q*parseInt(v)/100)}else{if(v.indexOf("-=")!==-1){return Math.abs(E-parseInt(v.split("-=")[1]))}else{if(v.indexOf("+=")!==-1){var s=(E+parseInt(v.split("+=")[1]));return s>=0?0:Math.abs(s)}else{if(v.indexOf("px")!==-1&&i._isNumeric.call(null,v.split("px")[0])){return Math.abs(v.split("px")[0])}else{if(v==="top"||v==="left"){return 0}else{if(v==="bottom"){return Math.abs(r.height()-D.outerHeight(false))}else{if(v==="right"){return Math.abs(r.width()-D.outerWidth(false))}else{if(v==="first"||v==="last"){var y=D.find(":"+v),A=w==="x"?j(y).offset().left:j(y).offset().top;return A-x}else{if(j(v).length){var A=w==="x"?j(v).offset().left:j(v).offset().top;return A-x}else{D.css(z,v);e.update.call(null,C[0]);return}}}}}}}}}}break}},_autoUpdate:function(q){var t=j(this),F=t.data(d),z=F.opt,v=j("#mCSB_"+F.idx+"_container");if(q){clearTimeout(v[0].autoUpdate);i._delete.call(null,v[0].autoUpdate);return}var s=v.parent(),p=[j("#mCSB_"+F.idx+"_scrollbar_vertical"),j("#mCSB_"+F.idx+"_scrollbar_horizontal")],D=function(){return[p[0].is(":visible")?p[0].outerHeight(true):0,p[1].is(":visible")?p[1].outerWidth(true):0]},E=y(),x,u=[v.outerHeight(false),v.outerWidth(false),s.height(),s.width(),D()[0],D()[1]],H,B=G(),w;C();function C(){clearTimeout(v[0].autoUpdate);v[0].autoUpdate=setTimeout(function(){if(z.advanced.updateOnSelectorChange){x=y();if(x!==E){r();E=x;return}}if(z.advanced.updateOnContentResize){H=[v.outerHeight(false),v.outerWidth(false),s.height(),s.width(),D()[0],D()[1]];if(H[0]!==u[0]||H[1]!==u[1]||H[2]!==u[2]||H[3]!==u[3]||H[4]!==u[4]||H[5]!==u[5]){r();u=H}}if(z.advanced.updateOnImageLoad){w=G();if(w!==B){v.find("img").each(function(){A(this.src)});B=w}}if(z.advanced.updateOnSelectorChange||z.advanced.updateOnContentResize||z.advanced.updateOnImageLoad){C()}},60)}function G(){var I=0;if(z.advanced.updateOnImageLoad){I=v.find("img").length}return I}function A(L){var I=new Image();function K(M,N){return function(){return N.apply(M,arguments)}}function J(){this.onload=null;r()}I.onload=K(I,J);I.src=L}function y(){if(z.advanced.updateOnSelectorChange===true){z.advanced.updateOnSelectorChange="*"}var I=0,J=v.find(z.advanced.updateOnSelectorChange);if(z.advanced.updateOnSelectorChange&&J.length>0){J.each(function(){I+=j(this).height()+j(this).width()})}return I}function r(){clearTimeout(v[0].autoUpdate);e.update.call(null,t[0])}},_snapAmount:function(r,p,q){return(Math.round(r/p)*p-q)},_stop:function(p){var r=p.data(d),q=j("#mCSB_"+r.idx+"_container,#mCSB_"+r.idx+"_container_wrapper,#mCSB_"+r.idx+"_dragger_vertical,#mCSB_"+r.idx+"_dragger_horizontal");q.each(function(){i._stopTween.call(this)})},_scrollTo:function(q,s,u){var I=q.data(d),E=I.opt,D={trigger:"internal",dir:"y",scrollEasing:"mcsEaseOut",drag:false,dur:E.scrollInertia,overwrite:"all",callbacks:true,onStart:true,onUpdate:true,onComplete:true},u=j.extend(D,u),G=[u.dur,(u.drag?0:u.dur)],v=j("#mCSB_"+I.idx),B=j("#mCSB_"+I.idx+"_container"),K=E.callbacks.onTotalScrollOffset?i._arr.call(q,E.callbacks.onTotalScrollOffset):[0,0],p=E.callbacks.onTotalScrollBackOffset?i._arr.call(q,E.callbacks.onTotalScrollBackOffset):[0,0];I.trigger=u.trigger;if(s==="_resetY"&&!I.contentReset.y){if(t("onOverflowYNone")){E.callbacks.onOverflowYNone.call(q[0])}I.contentReset.y=1}if(s==="_resetX"&&!I.contentReset.x){if(t("onOverflowXNone")){E.callbacks.onOverflowXNone.call(q[0])}I.contentReset.x=1}if(s==="_resetY"||s==="_resetX"){return}if((I.contentReset.y||!q[0].mcs)&&I.overflowed[0]){if(t("onOverflowY")){E.callbacks.onOverflowY.call(q[0])}I.contentReset.x=null}if((I.contentReset.x||!q[0].mcs)&&I.overflowed[1]){if(t("onOverflowX")){E.callbacks.onOverflowX.call(q[0])}I.contentReset.x=null}if(E.snapAmount){s=i._snapAmount(s,E.snapAmount,E.snapOffset)}switch(u.dir){case"x":var x=j("#mCSB_"+I.idx+"_dragger_horizontal"),z="left",C=B[0].offsetLeft,H=[v.width()-B.outerWidth(false),x.parent().width()-x.width()],r=[s,s===0?0:(s/I.scrollRatio.x)],L=K[1],J=p[1],A=L>0?L/I.scrollRatio.x:0,w=J>0?J/I.scrollRatio.x:0;break;case"y":var x=j("#mCSB_"+I.idx+"_dragger_vertical"),z="top",C=B[0].offsetTop,H=[v.height()-B.outerHeight(false),x.parent().height()-x.height()],r=[s,s===0?0:(s/I.scrollRatio.y)],L=K[0],J=p[0],A=L>0?L/I.scrollRatio.y:0,w=J>0?J/I.scrollRatio.y:0;break}if(r[1]<0||(r[0]===0&&r[1]===0)){r=[0,0]}else{if(r[1]>=H[1]){r=[H[0],H[1]]}else{r[0]=-r[0]}}if(!q[0].mcs){F()}clearTimeout(B[0].onCompleteTimeout);if(!I.tweenRunning&&((C===0&&r[0]>=0)||(C===H[0]&&r[0]<=H[0]))){return}i._tweenTo.call(null,x[0],z,Math.round(r[1]),G[1],u.scrollEasing);i._tweenTo.call(null,B[0],z,Math.round(r[0]),G[0],u.scrollEasing,u.overwrite,{onStart:function(){if(u.callbacks&&u.onStart&&!I.tweenRunning){if(t("onScrollStart")){F();E.callbacks.onScrollStart.call(q[0])}I.tweenRunning=true;i._onDragClasses(x);I.cbOffsets=y()}},onUpdate:function(){if(u.callbacks&&u.onUpdate){if(t("whileScrolling")){F();E.callbacks.whileScrolling.call(q[0])}}},onComplete:function(){if(u.callbacks&&u.onComplete){if(E.axis==="yx"){clearTimeout(B[0].onCompleteTimeout)}var M=B[0].idleTimer||0;B[0].onCompleteTimeout=setTimeout(function(){if(t("onScroll")){F();E.callbacks.onScroll.call(q[0])}if(t("onTotalScroll")&&r[1]>=H[1]-A&&I.cbOffsets[0]){F();E.callbacks.onTotalScroll.call(q[0])}if(t("onTotalScrollBack")&&r[1]<=w&&I.cbOffsets[1]){F();E.callbacks.onTotalScrollBack.call(q[0])}I.tweenRunning=false;B[0].idleTimer=0;i._onDragClasses(x,"hide")},M)}}});function t(M){return I&&E.callbacks[M]&&typeof E.callbacks[M]==="function"}function y(){return[E.callbacks.alwaysTriggerOffsets||C>=H[0]+L,E.callbacks.alwaysTriggerOffsets||C<=-J]}function F(){var O=[B[0].offsetTop,B[0].offsetLeft],P=[x[0].offsetTop,x[0].offsetLeft],M=[B.outerHeight(false),B.outerWidth(false)],N=[v.height(),v.width()];q[0].mcs={content:B,top:O[0],left:O[1],draggerTop:P[0],draggerLeft:P[1],topPct:Math.round((100*Math.abs(O[0]))/(Math.abs(M[0])-N[0])),leftPct:Math.round((100*Math.abs(O[1]))/(Math.abs(M[1])-N[1])),direction:u.dir}}},_tweenTo:function(r,u,s,q,B,t,K){var K=K||{},H=K.onStart||function(){},C=K.onUpdate||function(){},I=K.onComplete||function(){},z=i._getTime(),x,v=0,E=r.offsetTop,F=r.style,A;if(u==="left"){E=r.offsetLeft}var y=s-E;r._mcsstop=0;if(t!=="none"){D()}p();function J(){if(r._mcsstop){return}if(!v){H.call()}v=i._getTime()-z;G();if(v>=r._mcstime){r._mcstime=(v>r._mcstime)?v+x-(v-r._mcstime):v+x-1;if(r._mcstime<v+1){r._mcstime=v+1}}if(r._mcstime<q){r._mcsid=A(J)}else{I.call()}}function G(){if(q>0){r._mcscurrVal=w(r._mcstime,E,y,q,B);F[u]=Math.round(r._mcscurrVal)+"px"}else{F[u]=s+"px"}C.call()}function p(){x=1000/60;r._mcstime=v+x;A=(!b.requestAnimationFrame)?function(L){G();return setTimeout(L,0.01)}:b.requestAnimationFrame;r._mcsid=A(J)}function D(){if(r._mcsid==null){return}if(!b.requestAnimationFrame){clearTimeout(r._mcsid)}else{b.cancelAnimationFrame(r._mcsid)}r._mcsid=null}function w(N,M,R,Q,O){switch(O){case"linear":case"mcsLinear":return R*N/Q+M;break;case"mcsLinearOut":N/=Q;N--;return R*Math.sqrt(1-N*N)+M;break;case"easeInOutSmooth":N/=Q/2;if(N<1){return R/2*N*N+M}N--;return -R/2*(N*(N-2)-1)+M;break;case"easeInOutStrong":N/=Q/2;if(N<1){return R/2*Math.pow(2,10*(N-1))+M}N--;return R/2*(-Math.pow(2,-10*N)+2)+M;break;case"easeInOut":case"mcsEaseInOut":N/=Q/2;if(N<1){return R/2*N*N*N+M}N-=2;return R/2*(N*N*N+2)+M;break;case"easeOutSmooth":N/=Q;N--;return -R*(N*N*N*N-1)+M;break;case"easeOutStrong":return R*(-Math.pow(2,-10*N/Q)+1)+M;break;case"easeOut":case"mcsEaseOut":default:var P=(N/=Q)*N,L=P*N;return M+R*(0.499999999999997*L*P+-2.5*P*P+5.5*L+-6.5*P+4*N)}}},_getTime:function(){if(b.performance&&b.performance.now){return b.performance.now()}else{if(b.performance&&b.performance.webkitNow){return b.performance.webkitNow()}else{if(Date.now){return Date.now()}else{return new Date().getTime()}}}},_stopTween:function(){var p=this;if(p._mcsid==null){return}if(!b.requestAnimationFrame){clearTimeout(p._mcsid)}else{b.cancelAnimationFrame(p._mcsid)}p._mcsid=null;p._mcsstop=1},_delete:function(r){try{delete r}catch(q){r=null}},_mouseBtnLeft:function(p){return !(p.which&&p.which!==1)},_pointerTouch:function(q){var p=q.originalEvent.pointerType;return !(p&&p!=="touch"&&p!==2)},_isNumeric:function(p){return !isNaN(parseFloat(p))&&isFinite(p)}};j.fn[g]=function(p){if(e[p]){return e[p].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof p==="object"||!p){return e.init.apply(this,arguments)}else{j.error("Method "+p+" does not exist")}}};j[g]=function(p){if(e[p]){return e[p].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof p==="object"||!p){return e.init.apply(this,arguments)}else{j.error("Method "+p+" does not exist")}}};j[g].defaults=h;b[g]=true;j(b).load(function(){j(m)[g]()})}))}(window,document));


// JavaScript Document
jQuery(document).ready(function() {

popupCall();		
popupCall_checklist();
// if(jQuery('#newsletter_add').length>0){
//	jQuery('#newsletter_add').click(function(){
//	var url=jQuery(this).attr('href');
//	jQuery('.shadow').fadeIn('fast');
// jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/images/712.GIF" align="center" /></div>');
// jQuery('#PopupContainer').fadeIn('fast');
//
// jQuery.post(url,{id : 1},function(data){
//  jQuery('#PopupLoader').html(data);
//  setTimeout('setPopupBlockPosition()',400);
//  jQuery("#NewsLetterForm").dowformvalidate();
// });});}
});
function CallTBMInfoDetails(item_href)
{
 var baseurl=jQuery('#baseurl').val();
 jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');
 jQuery('#PopupContainer').fadeIn('fast');
 var url = '';
 jQuery.post(url,{id : id},function(data){
  jQuery('#PopupLoader').html(data);
  setTimeout('setPopupBlockPosition()',400);
 });
}
function setPopupBlockPosition()
{

 var Bheight = jQuery('#PopupContainer').height();
 var WindowHeight = jQuery(window).height();
 var BodyTop = jQuery(window).scrollTop();
 if(Bheight > WindowHeight) {
  var Pos = BodyTop + 10;
 }
 else {
  var Pos = ((WindowHeight - Bheight) / 2) + BodyTop;
 }
 jQuery('#PopupContainer').animate({top : Pos},1000);
}
function closePopup()
{
 jQuery('#shadow1').fadeOut('fast');
 jQuery('#PopupContainer').fadeOut('fast',function(){
	 jQuery('#PopupLoader').html('');});

}

function closePopup2()
{

 jQuery('#shadow2').fadeOut('fast');
 jQuery('#PopupContainer2').fadeOut('fast',function(){
	 jQuery('#PopupLoader2').html('');});

}

if(jQuery('.zoom_01').length > 0) {
      jQuery('.zoom_01').zoom();
  }
  
  function popupCall(){
	  
	  if(jQuery(".popup_call").length>0){
		jQuery(".popup_call").click(function() {
	var baseurl=jQuery('#baseurl').val();
			var url=jQuery(this).attr('href');
			 jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');
 jQuery('#PopupContainer').fadeIn('fast');

 jQuery.post(url,{id : 1},function(data){

  jQuery('#PopupLoader').html(data);
  setTimeout('setPopupBlockPosition()',400);
 });});}}
 
 
   function popupCall_checklist(){
	  
	  if(jQuery(".popup_call_checklist").length>0){
		jQuery(".popup_call_checklist").click(function() {
			jQuery('#popup_cls').val('');
	var baseurl=jQuery('#baseurl').val();
			var url=jQuery(this).attr('href');
			 jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');
 jQuery('#PopupContainer').fadeIn('fast');

 jQuery.post(url,{id : 1},function(data){
if(data!=""){
  jQuery('#PopupLoader').html(data);
  setTimeout('setPopupBlockPosition()',400);
  	if(jQuery("#updateChecklist").length > 0){
	jQuery("#updateChecklist").dowformvalidate({updateonly : true});
}

jQuery('#PopupContainer').removeClass('add_category');

if(jQuery('#popup_cls').length>0){
		jQuery('#PopupContainer').addClass(jQuery('#popup_cls').val());}
checklist_call();}else{
	window.location = baseurl+'user/login';}	


  
 });});}}

jQuery(document).ready(function(){
	
	  if(jQuery('.header_text_slider').length > 0) { 
 jQuery('.header_text_slider').slick({
   arrows: true,
   infinite: true,
   draggable: false,
   autoplay: true,
	  vertical:true,
   autoplaySpeed: 5000,
   speed: 500,
   slidesToShow: 1,
   slidesToScroll:1,

   pauseOnHover: false,
   dots:false,
   focusOnSelect: false,
   responsive: []
 });
} 
	
	checkAllAuto();
	updateQty_favorites();
loadPhone();
if(jQuery("#my-calendar").length>0){


    jQuery("#my-calendar").zabuto_calendar({
			 action: function (e,oo) {
				console.log(this);
                return myDateFunction(this.id, false);
            },
            action_nav: function () {
                return myNavFunction(this.id);
            },
      
            legend: [],
			
        });
  }
if(jQuery(".timepicker1").length>0){	jQuery('#time_from').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '6:00pm',
    defaultTime: '11:00 AM',
    startTime: '12:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});

jQuery('#time_to').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '6:00pm',
    defaultTime: '1:00 PM',
    startTime: '10:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});
}
if(jQuery('.ads_step').length>0){
var checkCookie=getCookie('ads_step');
if(checkCookie==""){
	jQuery('.ads_step').show();
	};}
/*document.onmousedown=disableclick;
status="Right Click Disabled";*/
updateQty_cart();
if(jQuery('.textbox.onenter').length>0){
jQuery('.textbox.onenter').keypress(function (e) {
  if (e.which == 13) {

   submitSearchForm();
   
    return false;    //<---- Add this line
  }
});}
});
jQuery(document).scroll(function(){
checkAdsStep();});
function disableclick(event)
{
  if(event.button==2)
   {
  
     return false;    
   }
}

function checkAdsStep(){
	if(jQuery('.ads_step').length>0){
	var scrollTop=jQuery(document).scrollTop();
	var offsetAds = jQuery('.ads_step').offset().top;
if(scrollTop>offsetAds){
	var checkCookie=getCookie('ads_step');
	if(checkCookie==""){
setCookie('ads_step',1,1);}
}
}
setCookie('ads_step',"",1);
}

function checkAllAuto(){
if(jQuery('.checkAllAuto').length>0){
jQuery('.checkAllAuto').click(function(){	
if(jQuery(this).is(':checked')){
jQuery("INPUT[type='checkbox']").prop('checked', true);}else{
jQuery("INPUT[type='checkbox']").attr('checked', false);	}    
});}}

function submitSearchForm(){
	 jQuery('#keyowrds').val(jQuery( "#keyowrd_autocomplete" ).val());
	  jQuery('#form_search').submit();
	}
	
    function myNavFunction(id) {
         var nav = jQuery("#" + id).data("navigation");
        var to = jQuery("#" + id).data("to");
    }
	
function myDateFunction(id) {
	
  var date = jQuery("#" + id).data("date");

  jQuery('#date').val(date);
  jQuery(".dow-clickable").removeClass('active');
  jQuery("#" + id).addClass('active');
  document.getElementById("#" + id).style.color = "blue";
}	
	
	
	
jQuery(function() {
var baseurl = jQuery('#baseurl').val();
if(jQuery( "#keyowrd_autocomplete" ).length>0){
jQuery( "#keyowrd_autocomplete" ).autocomplete({

minLength:2,
select: function (event, ui) {

	if(jQuery('#keyowrds').length>0){	
	 jQuery('#keyowrds').val(ui.item.label);}
	 
	 	if(jQuery('#keyowrd_autocomplete').length>0){	
	 jQuery('#keyowrd_autocomplete').val(ui.item.label);}
	 
	if(jQuery('#form_search').length>0){
		  submitForm('#form_search');
	}
	if(jQuery('#formRefineSearch').length>0){
		  submitForm('#formRefineSearch');}
    },
source: function( request, response ) {
	
var id_brands=jQuery('#id_brands').val();
var id_categories=jQuery('#id_categories').val();

jQuery('.search_brand_keyword').addClass('loader_input');
jQuery.ajax({
  url: baseurl+"products/search_by_keyword",

  dataType: "json",

  data: {term: request.term,id_brands:id_brands,id_categories:id_categories},

  type:"POST",
  

  success: function(data) {
jQuery('.search_brand_keyword').removeClass('loader_input');
			  response(jQuery.map(data, function(item) {
				 

			  return {

				  label: item.label,
				 

				  };

		  }));

	  }

  });

},

});}
});
function validateSearchForm(){
	var  keywords=jQuery('.keywords').val();
	if(keywords==""){return false;}else{
		return true;}}
		
	
function removeFile(section,field,image,id)
{
	if(confirm('Are you sure you want to delete this file ?')) {
		jQuery('#'+field+'_'+id).html('Loading...');
		var url = jQuery('#baseurl').val()+section+'/delete_file';
		jQuery.post(url,{field : field,image : image,id : id},function(data){
			jQuery('#'+field+'_'+id).html(data);
			if(jQuery("input[name='"+field+"_f']").length>0){
				jQuery("input[name='"+field+"_f']").val('');}
		});
	}
	else {
		return false;
	}
}

function removeItem(rowid)
{
	if(confirm('Are you sure you want to delete this item ?')) {
	jQuery('#delete_'+rowid+' a').addClass('loader');
	jQuery('#delete_'+rowid+' a').html('');
	var url = jQuery('#baseurl').val()+'cart/remove/'+rowid;
	jQuery.post(url,{id : rowid},function(data){
	jQuery('.FormResult').html('');
	var Parsedata = JSON.parse(data);
	if(Parsedata.cart!='' ){
			jQuery('#shopping-cart-cont').html(Parsedata.cart);
			if(Parsedata.total_price_cont!='' ){
			jQuery('#total_price_cont').html(Parsedata.total_price_cont);
				}
			updateQty_cart();}
			
	if(Parsedata.remaining_miles!=null){
	jQuery('#remaining-miles').html(Parsedata.remaining_miles);}
			
			if(Parsedata.count_items!=null){
			if(jQuery('#cart_items_count .count').length>0){
			jQuery('#cart_items_count .count').remove();}
			if(Parsedata.count_items==0){
			jQuery('.cart-btns').remove();}
			jQuery('<span  class="count">'+Parsedata.count_items+'</span>').appendTo('#cart_items_count');
			   }
		});
	}
	else {
		return false;
	}
}

function removeQuotationLineItem(id,rand){
	if(confirm('Are you sure you want to delete this Item ?')) {
		/*jQuery('#'+field+'_'+id).html('Loading...');*/
		var url = jQuery('#baseurl').val()+'user/removeQuotationLineItem';
		jQuery.post(url,{id : id,rand : rand},function(data){
			var Parsedata = JSON.parse(data);
			
			if(Parsedata.result==1){
				if(Parsedata.cart!='' ){
				jQuery('#shopping-cart-cont').html(Parsedata.cart);}
				
				}});
	}
	else {
		return false;
	}
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}


function removeDropFile(section,field,image,id)
{
	if(confirm('Are you sure you want to delete this file ?')) {
		jQuery('#'+field+'_'+id).html('Loading...');
		var url = jQuery('#baseurl').val()+'user/deleteDropFile';
		jQuery.post(url,{section:section,field : field,image : image,id : id},function(data){
			jQuery('#'+field+'_'+id).html(data);
		});
	}else{
		return false;
	}
}

function submitBulkOption(){

	jQuery('input[name="bulk_option"]').val(jQuery('#bulk_option').val());
	jQuery('#updateChecklist').submit();
	jQuery('input[name="bulk_option"]').val('');}

function checkCheckout(){
var url_img=jQuery('#baseurl').val()+'/front/img/i_msg-note.png';
if(jQuery('.exported_1').length>0){
	jQuery.confirm({
		 confirmButton: 'Confirm',
    cancelButton: 'Close',
    title: '<p style="text-align:center;margin:0;padding:0;"><img src="'+url_img+'" width="30"></p>',
    content: 'Some quantities cannot be exported. Please confirm if you are willing to ship them at your own responsibility.',
    confirm: function(){
    jQuery('#CheckoutForm').submit();
    },

});}else{
  jQuery('#CheckoutForm').submit();	}
	
/*	if(confirm('Some quantities cannot be exported./nPlease confirm if you are willing to ship them at your own responsibility.')) {
	alert('okokok');
	}*/
	}

function delete_file_m(section,field,image,id)
{
	if(confirm('Are you sure you want to delete this file ?')) {
		jQuery('#'+field+'_'+id).html('Loading...');
		var url = jQuery('#baseurl').val()+'user/delete_file_m';
		jQuery.post(url,{section:section,field : field,image : image,id : id},function(data){
			jQuery('#'+field+'_'+id).html(data);
			if(jQuery("input[name='"+field+"_f']").length>0){
				jQuery("input[name='"+field+"_f']").val('');
				}
		});
	}
	else {
		return false;
	}
}

function delete_row(section,id){

	var baseurl=jQuery('#baseurl').val()

window.location=baseurl+"user/delete_row/"+section+"/"+id;
return false;
}
function callComboxQty(){
	if(jQuery(".combobox").length>0){
	jQuery( ".combobox" ).combobox();}}
	/////////////////////TABS\\\\\\\\\\\\\\\\\\\\\\\\\\\
jQuery(document).ready(function() {
	checkAllAuto();
	CustomScrollbarBrands();
	var hash=location.hash;
	if(hash=="#meetingRequest"){
		meetingRequest();
		jQuery('#meetingRequest textarea[name=message]').val('Access Code Request')}
checklist_call();

callComboxQty();
	
if(jQuery(".add_field_button").length>0){
	
	addRow();
	}
if(jQuery('a.popUpGallery').length>0){
 jQuery("a.popUpGallery[rel^='prettyPhoto']").prettyPhoto({deeplinking: false,social_tools:false});}

if(jQuery(".tab_head").length>0){
jQuery(".tab_head").stop(true,true).click(function(){
	var obj=jQuery(this).parent();
	if(jQuery('.tab_head.active').length>0){
		jQuery('.tab_head').removeClass('active');	
		jQuery('.tab_head').find('.tab_icon').html('+');
    jQuery('.tab_body').stop(true,true).slideUp('fast',function(){});
    }
	var id = parseInt(this.id.replace('tab_head_',''));
	 if(obj.find('.tab_body').is(":visible")){
	
			   jQuery('#tab_head_'+id).removeClass('active');	
			   jQuery('#tab_head_'+id).find('.tab_icon').html('+');
		       obj.find('.tab_body').stop(true,true).slideUp('fast',function(){});
	}else{
	obj.find('.tab_body').stop(true,true).slideDown('fast',function(){});
	jQuery('#tab_head_'+id).addClass('active');	
	jQuery('#tab_head_'+id).find('.tab_icon').html('__');
	}
});}	

var baseurl=jQuery('#baseurl').val()
	/*if(jQuery('#products_section .products-container').length>0){
	
	 var ias = jQuery.ias({
      container: "#products_section .products-container",
      item: ".product",
	  trigger: '??? ??????',
      pagination: "#pagination",
	  loader: ' <img src="'+baseurl+'front/img/482.GIF" />',
      next: ".next a"
    });

	ias.on('rendered', function(items) { 
	loadAddCartBtn();});
	  
	  ias.extension(new IASSpinnerExtension({
    src: baseurl+'front/img/482.GIF', // optionally
}));
   
    ias.extension(new IASTriggerExtension({offset: 5}));
    ias.extension(new IASNoneLeftExtension({text: ''}));
	}*/
	
	if(jQuery('.my_credits .shopping_cart_table').length>0){
	
	 var ias = jQuery.ias({
      container: ".my_credits .shopping_cart_table",
      item: ".credits_item",
	  trigger: '??? ??????',
      pagination: "#pagination",
	  loader: ' <img src="'+baseurl+'front/img/482.GIF" />',
      next: ".next a"
    });

	ias.on('rendered', function(items) { });
	  
	  ias.extension(new IASSpinnerExtension({
    src: baseurl+'front/img/482.GIF', // optionally
}));
   
    ias.extension(new IASTriggerExtension({offset: 1}));
    ias.extension(new IASNoneLeftExtension({text: ''}));
	}


	if(jQuery('.question_and_answer_cont .question_and_answer').length>0){
	
	 var ias = jQuery.ias({
      container: ".question_and_answer_cont",
      item: ".question_and_answer",
	  trigger: '??? ??????',
      pagination: "#pagination",
	  loader: ' <img src="'+baseurl+'front/img/482.GIF" />',
      next: ".next a"
    });

	ias.on('rendered', function(items) { });
	  
	  ias.extension(new IASSpinnerExtension({
    src: baseurl+'front/img/482.GIF', // optionally
}));
   
    ias.extension(new IASTriggerExtension({offset: 0}));
    ias.extension(new IASNoneLeftExtension({text: ''}));
	}
	
	if(jQuery('.category_toggle').length>0){
	jQuery('.category_toggle').click(function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent();
	var id=obj.attr('id').replace('category_toggle_',"");
	if(jQuery("#sub_category_"+id).is(":visible")){
		obj_parent.removeClass('active');
    jQuery("#sub_category_"+id).stop().slideUp('10');}else{
		obj_parent.addClass('active');
	jQuery("#sub_category_"+id).stop().slideDown('10');	}

	});
	}
	
	if(jQuery('.category-a').length>0){

	
	/*jQuery(".category-li").stop().hover(function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	var id=obj.attr('id').replace('category-li-',"");
    obj.addClass('a_active');
	
	obj_parent.find('#category-a-'+id).addClass('a_active_2');
	obj_parent.find('#sub-category-'+id).stop(true,true).slideDown(500,function(){
	obj_parent.find('#sub-category-'+id).addClass('active_2');});
	
    },function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	obj.removeClass('a_active');
	var id=obj.attr('id').replace('category-li-',"");
    obj_parent.find('#category-a-'+id).removeClass('a_active_2');
	if(obj_parent.find('#sub-category-'+id).hasClass('active')){}else{
		obj_parent.find('#sub-category-'+id).stop(true,true).slideUp('fast',function(){
	obj_parent.find('#sub-category-'+id).removeClass('active_2');});}
	<!--obj_parent.find('#sub-category-'+id).removeClass('active_2');-->
	
   
	     		});*/

jQuery(".category-li.hvr").stop(true,true).hover(function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	var id=obj.attr('id').replace('category-li-',"");
    obj.addClass('a_active');
	
	obj_parent.find('#category-a-'+id).addClass('a_active_2');
	obj_parent.find('#sub-category-'+id).stop(true,true).slideDown(100,function(){
	obj_parent.find('#sub-category-'+id).addClass('active_2');});
	
    },function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	obj.removeClass('a_active');
	var id=obj.attr('id').replace('category-li-',"");
    obj_parent.find('#category-a-'+id).removeClass('a_active_2');
obj_parent.find('#sub-category-'+id).stop(true,true).slideUp('fast',function(){
	obj_parent.find('#sub-category-'+id).removeClass('active_2');});
	if(obj_parent.find('#sub-category-'+id).hasClass('active')){}else{
	obj_parent.find('#sub-category-'+id).stop(true,true).slideUp('fast',function(){
	obj_parent.find('#sub-category-'+id).removeClass('active_2');});}
	<!--obj_parent.find('#sub-category-'+id).removeClass('active_2');-->
	});
	
jQuery(".sub_category_2").stop().hover(function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	var id=obj.attr('id').replace('sub-category-',"");
    obj.addClass('a_active');

	obj_parent.find('#category-a-'+id).addClass('a_active_2');
	obj_parent.find('#sub-category-'+id).stop(true,true).slideDown('fast',function(){
	obj_parent.find('#sub-category-'+id).addClass('active_2');});
	
    },function(){
	var obj=jQuery(this);
	var obj_parent=obj.parent().parent();
	obj.removeClass('a_active');
	var id=obj.attr('id').replace('sub-category-',"");
    obj_parent.find('#category-a-'+id).removeClass('a_active_2');
	if(obj_parent.find('#sub-category-'+id).hasClass('active')){}else{
	obj_parent.find('#sub-category-'+id).stop(true,true).slideUp('fast',function(){
	obj_parent.find('#sub-category-'+id).removeClass('active_2');});}
	<!--obj_parent.find('#sub-category-'+id).removeClass('active_2');-->
	
   
	     		
});

	
	}
	
	if(jQuery('.sub_category_bx').length>0){

	


jQuery(".sub_category_bx").stop().hover(function(){
	
	var obj=jQuery(this);
	var arr=obj.attr('id').replace('sub_category_bx_',"").split('_');
	var id_parent=arr[0];
	var id_category=arr[1];

	jQuery(".sub_category_bx").removeClass('active_n');
	jQuery(".category-a").removeClass('active_n');

	jQuery('#category-parent-li-'+id_parent+'-'+id_category+ ' #category-a-'+id_category).addClass('active_n');
	jQuery('#sub-category-'+id_parent+' #sub_category_bx_'+id_parent+'_'+id_category).addClass('active_n');
  
    },function(){

	jQuery(".sub_category_bx").removeClass('active_n');
	jQuery(".category-a").removeClass('active_n');	
		});
	
jQuery(".category_title_2").stop().hover(function(){
	
	var obj=jQuery(this);
	var arr=obj.attr('id').replace('category-parent-li-',"").split('-');
	var id_parent=arr[0];
	var id_category=arr[1];
	jQuery(".sub_category_bx").removeClass('active_n');
	jQuery(".category-a").removeClass('active_n');

jQuery('#category-parent-li-'+id_parent+'-'+id_category+ ' #category-a-'+id_category).addClass('active_n');
	jQuery('#sub-category-'+id_parent+' #sub_category_bx_'+id_parent+'_'+id_category).addClass('active_n');
    },function(){

	jQuery(".sub_category_bx").removeClass('active_n');
	jQuery(".category-a").removeClass('active_n');	
		});

	
	}
	
if(jQuery(".btn_voucher").length>0){
	jQuery(".btn_voucher").click(function(){
		if(confirm('Are you sure you want to redeem this voucher ?')) {
		var obj=jQuery(this);
		var ParentObj=obj.parent().parent();
		obj.addClass('active');
		jQuery('.FormResult').html('');
		ParentObj.find('.loader-bx').html('<span class="loader loader-miles"></span>');
	var id=obj.attr('id').replace('voucher_','');
  	var url=jQuery("#baseurl").val()+'vouchers/send';
	var baseurl=jQuery("#baseurl").val();
	jQuery.post(url,{id : id},function(data){
	    ParentObj.find('.loader-bx').html('');
		obj.removeClass('active');
		var Parsedata = JSON.parse(data);
		if(Parsedata.result==0){
ParentObj.find('.FormResult').html('<ul class="messages"><li class="error-msg r-fullSide"><ul><li><span>'+Parsedata.message+'</span></li></ul></li></ul>');
			}else{
			ParentObj.find('.FormResult').html('<ul class="messages"><li class="success-msg r-fullSide"><ul><li><span>'+Parsedata.message+'</span></li></ul></li></ul>');
			 if(Parsedata.redirect_link != null)
			 window.location = Parsedata.redirect_link;}

	});
		}});}
		
if(jQuery(".consultants_btn").length>0){
	jQuery(".consultants_btn").click(function(){
		var obj=jQuery(this);
		var ParentObj=obj.parent().parent();
		var id=ParentObj.attr('id').replace('consultants_',"");
	/*selectConsultant(id);
		*/
		jQuery('.meeting_request').slideUp('fast');
	jQuery('#consultants_request_'+id).find('.checked_input').val(1);	
	jQuery('#consultants_request_'+id).slideDown('fast');
	scroll_To('#consultants_request_'+id,100);
		
		});
		
		jQuery(".remove_consultant").click(function(){
		/*if(confirm('Are you sure you want to remove this consultant?')) {*/
		jQuery('#consultant').val("");
		jQuery('.consultants_btn').removeClass('hide');
		jQuery('.remove_consultant').addClass('hide');
		jQuery('#PopupLoader').removeClass('active_consultant');
		jQuery('.consultants_cont .supplier_blk').removeClass('active');
		/*}*/});}		
	optionsBtn();
	loadAddCartBtn();
	if(jQuery('#opc-register').length>0){
jQuery('input[name=role_c]').click(function(){

	var id_role=jQuery(this).val();

		jQuery('.innerUserForm').hide(100,function(){
    jQuery('.innerUserForm').show();});
	jQuery('.rol').css({"display":'block'});
	if(id_role==5){
	jQuery('.rol_5').css({"display":'block'});}else{
	jQuery('.rol_5').css({"display":'none'});	}
	jQuery('input[name=id_role]').val(id_role);
	});}	
//////////////////////////////////// drop down ////////////////////////////////
jQuery(".dropdown").stop().hover(function(){
	var parent=jQuery(this).parent();
	
	var id = parseInt(this.id.replace('dropdown-',''));
/*	if(jQuery(this).parent().hasClass('ul-menu-categories') || jQuery(this).parent().hasClass('category-dropdown-menu')){
setTimeout(function(){jQuery("#sub-dropdown-"+id).fadeIn('100')},50);
	}else{
		jQuery("#sub-dropdown-"+id).css({'display':'block'});
  }*/
  
   parent.find("#dropdown-"+id).addClass('subdropactive');

/*jQuery("#sub-dropdown-"+id).stop(true,true).slideDown('fast');*/
	/*jQuery("#sub-dropdown-"+id).stop(true,true).slideDown(100);*/
	/*if(id==3){brand_dropdown_slick();
	if(jQuery('.dropdownSlickActive').length>0 && jQuery('.mainMenu_resize').length==0){}else{
	brand_dropdown_slick();
	if(jQuery('.mainMenu_resize').length>0){
	jQuery('.mainMenu').removeClass('mainMenu_resize');}
	jQuery('.brand_dropdown_slick').addClass('dropdownSlickActive');}
	}	*/
	/*brand_dropdown_slick();*/
	},function(){

			var id = parseInt(this.id.replace('dropdown-',''));
			
/*			 jQuery("#sub-dropdown-"+id).css({'display':'none'});
			 jQuery("#sub-dropdown-"+id).fadeOut('40');*/
			 
			 jQuery("#dropdown-"+id).removeClass('subdropactive');
			 
			 
			/*jQuery("#sub-dropdown-"+id).stop(true,true).slideUp('fast');*/
			/*jQuery("#sub-dropdown-"+id).stop(true,true).slideUp(100);*/
	     		
});

if(jQuery(".bulk-options-a").length>0){
	jQuery(".bulk-options-a").click(function() {
		if(jQuery(".bulk-options-ul").is(":visible")){
		jQuery('.bulk-options-ul').slideUp('fast');}else{
		jQuery('.bulk-options-ul').slideDown('fast');	}
		});}
    //Default Action
	if(jQuery(".req_c input[type='checkbox']").length>0){
	jQuery(".req_c input[type='checkbox']").click(function() {
			var obj=jQuery(this);
			var name=jQuery(this).attr('name').replace('_c','');
		
			if(jQuery("input[name='"+name+"_c']").length>0){
			 if (jQuery(this).prop('checked')==true){ 
			jQuery("input[name='"+name+"']").val('1');
				}else{
			jQuery("input[name='"+name+"']").val("");}}
			});
		}
		
	
	if(jQuery(".req_c input[type='radio']").length>0){
	jQuery(".req_c input[type='radio']").click(function() {
	
			var obj=jQuery(this);
			var name=jQuery(this).attr('name');
		
			if(jQuery("input[name='"+name+"_c']").length>0){
			 if (jQuery(this).prop('checked')==true){ 
			jQuery("input[name='"+name+"_c']").val('1');
				}else{
			jQuery("input[name='"+name+"_c']").val("");}}
			});
		}	
		
			/*if(jQuery(".checklist-chkbx").length>0){
			jQuery(".checklist-chkbx").click(function() {
			var obj=jQuery(this);
			var parent=jQuery(this).parent().parent();
			if(jQuery(".checklist_qty").length>0){
			 var qty_id=parent.find('.checklist_qty').attr('id');
			 if (jQuery(this).prop('checked')==true){ 
			 document.getElementById(qty_id).disabled = false;
				}else{
			document.getElementById(qty_id).disabled = true;}}
			});
		}*/
		

		

	if(jQuery("#qty").length>0){
		jQuery("#qty").change(function() {
			var val=jQuery(this).val();
			if(jQuery('.qty-frm').length>0){
				jQuery('.qty-frm').val(val);}
				
				});	 }
				
		
				
		if(jQuery(".checklist_qty").length>0){
		jQuery(".checklist_qty").change(function() {
			var val=jQuery(this).val();
			var id_favorites=jQuery(this).attr('id').replace('fav_qty_','');
			
			if(jQuery('#add_form_'+id_favorites).length>0){
				jQuery("#add_form_"+id_favorites+" input[name='qty']").val(val);}});	 }			
	    //Default Action
	if(jQuery(".req_c input[type='file']").length>0){
		jQuery(".req_c input[type='file']").change(function() {
			var obj=jQuery(this);
			var name=jQuery(this).attr('name');
			var val=obj.val();
			if(jQuery("input[name='"+name+"_f']").length>0){
			jQuery("input[name='"+name+"_f']").val(val);
			jQuery("#"+name+"_f-error").html('');
	      	}
			});
		}	
		
	if(jQuery(".tab_content").length>0 && jQuery(".page_products").length>0){
    jQuery(".tab_content").hide(); //Hide all content
    jQuery("ul.tabs li:first").addClass("active").show(); //Activate first tab
    jQuery(".tab_content:first").show(); //Show first tab content
     
    //On Click Event
    jQuery("ul.tabs li").click(function() {
		
        jQuery("ul.tabs li").removeClass("active"); //Remove any "active" class
        jQuery(this).addClass("active"); //Add "active" class to selected tab
        jQuery(".tab_content").hide(); //Hide all tab content
        var activeTab = jQuery(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
        jQuery(activeTab).fadeIn(); //Fade in the active content
        return false;
    });}
 
});

function selectConsultant(id){
	
		/*if(confirm('Are you sure you want to choose this consultant?')) {*/
		jQuery('#consultant').val(id);
		jQuery('#consultants_'+id).addClass('active');
		jQuery('#PopupLoader').addClass('active_consultant');
		jQuery('.consultants_btn').addClass('hide');
		jQuery('#consultants_'+id).find('.remove_consultant').removeClass('hide');
		/*}*/}

jQuery(window).load(function(){
	jQuery('div.slick-slider, .header-new-txt').show()

jQuery(jQuery(".mainMenu").html()).appendTo('#menuMobCont');
jQuery(jQuery(".search_mb").html()).appendTo('#menuMobCont');
jQuery(jQuery(".menu-categories").html()).appendTo('#menuMobCont');
jQuery('<ul class="new-header-ul">'+jQuery(".new-header-ul").html()+'</ul>').appendTo('#menuMobCont');

jQuery('<ul class="my-list-menu">'+jQuery("#sub-dropdown-7").html()+'</ul>').appendTo('#menuMobCont');
/*jQuery(jQuery(".quarterLeftSideBar").html()).appendTo('#menuMobCont');*/
loadMenu();
popupCall_checklist();
if(jQuery('#widgets').length>0){
		var url=jQuery("#baseurl").val()+'home/getWidgets';

	jQuery.post(url,{id : 1},function(data){
	  var Parsedata = JSON.parse(data); 
jQuery('#widgets').html(Parsedata.widgets);

	});}
	


});
		
jQuery(document).ready(function(){
	

		
if(jQuery('.products_gallery .item .innerItem').length>0){
	
		jQuery('.item .innerItem').css({'display':'inline-block'});}
		if(jQuery('#banner').length>0){
	var url=jQuery("#baseurl").val()+'home/loadBanner';
	jQuery.post(url,{id : 1},function(data){
	var Parsedata = JSON.parse(data);
	jQuery('#banner').html(Parsedata);
	jQuery('div.slick-slider').show();
	if(jQuery(".meetingRequest").length>0){
		jQuery(".meetingRequest").each(function(){
		var obj=jQuery(this);
		var id=obj.attr('id');
		jQuery("#"+id).dowformvalidate();
	});
	}
	/* bannerSlick();*/
	 newBannerSlick();
	});
	
	 }
});

function resizeBanner(){

	
	if(jQuery('#new_banner_slick').length>0){
		
	var defaultWidth=965; 
	var defaultHeight=292; 
	var windowWidth=jQuery(window).width();
	var newHeight=(windowWidth*defaultHeight)/defaultWidth;
	if(windowWidth>1100){
		newHeight=defaultHeight;}
	if(newHeight>defaultHeight){newHeight=defaultHeight;
	}

	
	jQuery('#new_banner_slick .banImg').height(newHeight);
	var newHeight=newHeight-32;
	jQuery('.supplier_blk').height(newHeight);

}
	
 }
 

function edit_register(){
	jQuery('.edit_register').hide();
	jQuery("#id_user").val("");
	jQuery("#correlation_id").val("");
	jQuery('#opc-email .new-users').slideDown('fast');
	jQuery('#opc-register .new-users').slideUp('fast');}
function scroll_To(sec,m){
	if(m){}else{m=10}
var anmSec = jQuery(sec).offset().top-m;
 jQuery('html, body').stop(true,true).animate({scrollTop : anmSec},1000);
}	
 
function meetingRequest(id){
	if(id!=null){
		
		var m_id='#meeting_request_'+id;
		}else{
	
	var m_id='.meeting_request';}

	if(jQuery(m_id).is(":visible")){
		jQuery(m_id).slideUp('fast');}else{
		jQuery(m_id).slideDown('fast');	}
	
	/*scroll_To('.meeting_request',100);*/
	} 
	
function newsLetter(){
	if(jQuery(".newsletter_cont").is(":visible")){
		jQuery('.newsletter_cont').slideUp('fast');}else{
	jQuery('.newsletter_cont').slideDown('fast');
	jQuery("#NewsLetterForm").dowformvalidate({withScrolling:false});
	/*scroll_To('.newsletter_cont',100);*/}
	} 	
	
function setLocation(url){
	   window.location=url;}	
	   
function validateForm(withMsgAppear,form){
	
	if (typeof(form) !== 'undefined') {
var formObj=form+" ";}else{
	formObj="";}

	jQuery(formObj+'.form-error.mod').hide();
	var bool=true;

	jQuery(formObj+'.req_input').each(function(){
		var obj=jQuery(this);
		var val=obj.val();
		var id=obj.attr('id').replace('attr_input_',"");
		
		if(val==""){
			bool=false;
			if(withMsgAppear!=false){
			jQuery(formObj+'#attr-error-'+id).show();}
			}
	});
		
		return bool;
}	
	
function optionsBtn(){
	if(jQuery(".op").length>0){
	jQuery(".op").click(function() {
	var obj=jQuery(this);
	var val=obj.find('input').val();
	if(jQuery("#cart-form").length>0){
		var formObj="#cart-form ";
		}else{
		var formObj="";}
	

	var arr=obj.parent().attr('id').replace('attr_',"").split('_');
	
	var parent_id=arr[0];
	var id_product=arr[1];

	jQuery(formObj+'#attr_'+parent_id+"_"+id_product).find('.op').removeClass('active_op');

	//////////////////////SELECTED OPTION//////////////////////////////
if(parent_id==2){
jQuery(formObj+'#attr_select_'+parent_id+'_'+id_product).find('.selected_option_val').html("<div class='selected_option_color' style='background:#"+jQuery('#option_val_'+val).val()+"'>"+"</div>");}else{
	jQuery(formObj+'#attr_select_'+parent_id+'_'+id_product).find('.selected_option_val').html(jQuery('#option_val_'+val).val());}
	
	
	obj.addClass('active_op');
		
	if(jQuery(formObj+'#fav_attr_input_'+parent_id).length>0){
	jQuery(formObj+'#fav_attr_input_'+parent_id).val(val);
		}
	jQuery(formObj+'#attr_input_'+parent_id).val(val);
	if(validateForm(false)){

	ChangeProductOption(id_product,parent_id,'details',formObj);
		}
	});}
	}	   

		
function ChangeProductOption(id_products,id_attributes,sec,formObj)
{

	 var siteurl=jQuery('#siteurl').val();
 var baseurl=jQuery('#baseurl').val();
	var arr = [];
	
	jQuery(formObj+'.req_input').each(function(){
		var obj=jQuery(this);
		var val=obj.val();
		var id=obj.attr('id').replace('attr_input_',"");
		arr.push(val);
	});
	var redeem=0;
	if(jQuery('input[name="redeem"]').length>0){
		 redeem=jQuery('input[name="redeem"]').val();}

	jQuery('#loader-'+id_products).html('<img src="'+baseurl+'front/img/482.GIF" />');
	var url = siteurl+'products/checkStock/'+sec;
	jQuery.post(url,{id_products : id_products,redeem:redeem,id_attributes : id_attributes,arr : arr},function(data){
		Parsedata = JSON.parse(data);
		jQuery('#p-miles-'+id_products).html(Parsedata.miles);
	jQuery('#sku-'+id_products).html(Parsedata.sku);
jQuery(formObj+'#r-PriceSegments-'+id_products).html(Parsedata.price_segments);
jQuery(formObj+'.choosed_segement').html('');
callComboxQty();

//////////////////////////FAVORITES///////////////////
	jQuery('.stock-list-m').hide();	
	if(Parsedata.favorites_results==1){
	jQuery('#stock-list-m1').show();
	}else{jQuery('#stock-list-m2').show();}	
////////////////////////////////////////////////////	

		if(Parsedata.result == 0) {
			
			/*jQuery('#p-btn-'+id_products).hide();*/
			jQuery('#p-stock-'+id_products).html(Parsedata.status)
				jQuery('#p-cart-'+id_products).fadeOut('fast');
				
				jQuery('#p-quantity-'+id_products).fadeOut('fast');		
				if(Parsedata.price_segments != '') {
					/*jQuery('#r-PriceSegments-'+id_products).slideDown("fast");
					jQuery('#r-PriceSegments-'+id_products).html(Parsedata.price_segments);*/
					
				}
				else {
					
					/*jQuery('#r-PriceSegments-'+id_products).html("");
					jQuery('#r-PriceSegments-'+id_products).slideUp("fast");*/
				}
		}
		else {
			jQuery('#p-btn-'+id_products).show();
		
			jQuery('#p-stock-'+id_products).html("");
			if(Parsedata.hide_price == 0) {
				jQuery('#p-cart-'+id_products).fadeIn('fast');
				jQuery('#p-quantity-'+id_products).fadeIn('fast');
				/*jQuery('#r-PriceSegments-'+id_products).html("");*/
				/*jQuery('#r-PriceSegments-'+id_products).slideUp("fast");*/
				if(Parsedata.price_segments != '') {
		/*			jQuery('#r-PriceSegments-'+id_products).slideDown("fast");
					jQuery('#r-PriceSegments-'+id_products).html(Parsedata.price_segments);*/
				}
				else {
				/*	jQuery('#r-PriceSegments-'+id_products).html("");
					jQuery('#r-PriceSegments-'+id_products).slideUp("fast");*/
				}
			}
			else {
				jQuery('#p-cart-'+id_products).fadeOut('fast');
				jQuery('#p-quantity-'+id_products).fadeOut('fast');
				/*jQuery('#r-PriceSegments-'+id_products).html("");*/
				/*jQuery('#r-PriceSegments-'+id_products).slideUp("fast");*/
			}
		}
		jQuery('#p-stock-'+id_products).html(Parsedata.status);
		jQuery('#p-sku-'+id_products).html(Parsedata.sku);
		jQuery('#p-price-'+id_products).html(Parsedata.price);
		jQuery('#cart-form-'+id_products+' .attributes').each(function(){
/*			var id = parseInt(this.id.replace('attribute_',''));
			var val = jQuery(this).val();
			jQuery(this).attr('disabled',null);*/
		});
		jQuery('#loader-'+id_products).html('');
		loadCombobox();
	});
}	

function changeCurrency(){
		var id=jQuery('#currency').val();
		var baseurl=jQuery('#baseurl').val();
	
window.location=baseurl+'home/change_currency/'+id;}
		
function submitChecklistForm(submit_status){
		jQuery('#submit_status').val(submit_status);
		if(jQuery('#updatePopUpForm').length>0){
		jQuery('#updatePopUpForm').submit();}else{
		jQuery('#updateChecklist').submit();}
		}	
		
function loadMenu(){
jQuery(".menu-bar-btn").click(function(){
if(jQuery('.mainMenu').is(":visible")){
jQuery(".mainMenu").css({'display':'none'});
jQuery(".header_bottom").removeClass('active_menu');
}else{
	jQuery(".mainMenu").css({'display':'block'});
jQuery(".header_bottom").addClass('active_menu');
}});
if(jQuery('.lbl_shdw_bx').length>0){
jQuery(".lbl_shdw_bx").click(function(){
		if(jQuery('.currency_bx').hasClass('active_currencies')){
			jQuery('.currency_bx').removeClass('active_currencies');
			}else{
			jQuery('.currency_bx').addClass('active_currencies');
			}
		});
		
jQuery(document).click(function (e) {    		
   var container = jQuery('.currency_bx');

   if (!container.is(e.target) && container.has(e.target).length === 0)
  {jQuery('.currency_bx').removeClass('active_currencies');}
});		
}
if(jQuery('.menu-toggle').length>0){
	jQuery('.menu-toggle').click(function () {
		var obj=jQuery(this);
	
		var id=obj.attr('id').replace('mob_','');

		
/*if(jQuery('.menu-toggle.active').length>0){
	var id_old=jQuery('.menu-toggle.active').attr('id').replace('mob_','');
	alert(id);
if(id_old!=id){
jQuery('.menuMobCont .menuBlg').css({'display':'none'});
jQuery('.menuMobCont .menuBlg').removeClass('activeMenu');
jQuery('.menu-toggle').removeClass('active');}}*/

if(jQuery('.menuMobCont .'+id).is(":visible")){
/*jQuery('.menuMobCont .'+id).css({'display':'none'});*/
jQuery('.menuMobCont div, .menuMobCont ul').removeClass('activeMenu');
jQuery('.menuMobCont .'+id).removeClass('activeMenu');
obj.removeClass('active');
}else{ 
jQuery('.menuMobCont div, .menuMobCont ul').removeClass('activeMenu');
jQuery('.menuMobCont .menuBlg').css({'display':'none'});
jQuery('.menuMobCont .menuBlg').removeClass('activeMenu');
jQuery('.menu-toggle').removeClass('active');

jQuery('.menuMobCont .'+id).addClass('activeMenu');
/*jQuery('.menuMobCont  .'+id).css({'display':'block'});*/
obj.addClass('active');
popupCall();	
}
		});}


		
if(jQuery('.mainMenu-mob-li-a').length>0){
	jQuery('.mainMenu-mob-li-a').click(function () {
	var parent=jQuery(this).parent();
	var id=parent.attr('id').replace('dropdown-','');
	if(jQuery('.dropdown.active').length>0){
	jQuery('.menuMobCont').find('.dropdown').removeClass('active');	
    jQuery('.menuMobCont').find('.sub-dropdown').stop(true,true).slideUp('fast',function(){});
    }
   if(parent.find('.sub-dropdown').is(":visible")){
	 parent.removeClass('active');	
	 parent.find('.sub-dropdown').stop(true,true).slideUp('fast',function(){});
	}else{ 
	parent.find('.sub-dropdown').stop(true,true).slideDown('fast',function(){ if(id==3){ }});
	brand_dropdown_slick();
	parent.addClass('active');	

	}
		});}
		loadMenuBtns();
		CustomScrollbarBrands();
}	

jQuery(window).bind('resize',function(){
	/*resizeInnerSection();*/
	resizeBanner();
if(jQuery('.mainMenu_resize').length>0){}else{jQuery('.mainMenu').addClass('mainMenu_resize');}
});	
function mobile(){
	if(jQuery('.ul-header-account-m').is(":visible")){
	jQuery("#search_mb").css({'display':'none'});}else{
	jQuery("#search_mb").css({'display':'block !important'});}
	if(CheckIfMobWidth(800)){ /*music.pause();*/
	jQuery('body').addClass('mobile');
	jQuery('body').removeClass('web');
	/*jQuery('.mobile .mainMenu').css({'max-height':jQuery(window).height()-100});*/
	/*jQuery(".mobile .mainMenu").mCustomScrollbar();*/
	jQuery(".mainMenu, .ul_sub_mainMenu").css({'display':'none'});
	jQuery(".menu-bar-btn").removeClass('close');
	}else{
	if(jQuery('.active_menu').length>0){	
	jQuery(".header_bottom").removeClass('active_menu');}
	jQuery(".ul_sub_mainMenu").css({'display':'none'});
	jQuery(".mainMenu").css({'display':'block'});
	var WindowHeight = jQuery(window).height();
   	jQuery('body').removeClass('mobile');
	jQuery('body').addClass('web');
}}

function CheckIfMobWidth(width){
	var windowWidth=jQuery(window).width();
	if(windowWidth<=width) return true ;else return false
	} 
	
function sendInquiry(url){

	var ajax=1;
	 jQuery.post(url,{ajax : ajax},function(data){
if(Parsedata.return_popup_message !=null){
popup_return_message(Parsedata.return_popup_message,1);
}

if(Parsedata.redirect_link != null)
			 window.location = Parsedata.redirect_link;

		 });
	}	
	
function loadAddCartBtn(){
	if(jQuery('.add_to_cart').length>0){
jQuery('.add_to_cart').click(function(){
	
	var obj=jQuery(this);
	obj.addClass('loader_btn');
	var product_url=obj.attr('id').replace('product-','');
	var redeem="";
if(jQuery('#cart-form-'+product_url).length>0){
	var redeem=jQuery('#cart-form-'+product_url).find('input[name="redeem"]').val();

	}
	   var baseurl=jQuery("#baseurl").val();
		var access_code_url=baseurl+'products/checkAccessCode';

 jQuery.post(access_code_url,{id : product_url},function(data){
	var Parsedata = JSON.parse(data);
		if(Parsedata.result!=4){
	 if(Parsedata.result==0){
jQuery('.header').addClass('n_access_code');
jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');	
jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html(Parsedata.html);

 jQuery('#PopupContainer').fadeIn('fast')
 
  setTimeout('setPopupBlockPosition()',400);
obj.removeClass('loader_btn');
    }else{
	
		
  	var url=jQuery("#baseurl").val()+'products/attributesOptions';

	var baseurl=jQuery("#baseurl").val();
	jQuery.post(url,{product_url : product_url},function(data){
		var Parsedata = JSON.parse(data);
		
if(Parsedata.result==1){
 var new_url=jQuery("#baseurl").val()+'products/getProductAttributesOptions';
 jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');
 jQuery('#PopupContainer').fadeIn('fast');
 jQuery.post(new_url,{product_url : product_url,redeem:redeem},function(data){
	 var Parsedata = JSON.parse(data);
	 if(Parsedata.result==1){
 jQuery('#PopupLoader').html(Parsedata.html);
 optionsBtn();
	obj.removeClass('loader_btn');
 /*qtyAutoComplete();*/
  setTimeout('setPopupBlockPosition()',400);
  loadCombobox();
  setTimeout('callComboxQty()',400);
 }else{
	  }
  });
		    }else{
	
			callFormAjax('cart-form','product',product_url);
			
			jQuery('#cart-form-'+product_url).submit();
			}
		
		});
		}}else{
			 window.location = Parsedata.redirect_link;}

});

	
	});}}
	
function submitFavoritesToCart(id_favorites){

	 jQuery('#favorites-'+id_favorites).addClass('loader_btn');
		callFormAjax('add_form','favorites',id_favorites);
		jQuery('#add_form-'+id_favorites).submit();
	}	
	
	
function callFormAjax(formName,btnName,id){
	
	if(jQuery('.active_btn#'+btnName+'-'+id).length>0){}else{
				jQuery('.active#'+btnName+'-'+id).addClass('active_btn');
				jQuery('#'+formName+'-'+id).dowformvalidate({updateonly : true});
	        }}	
	

function checkAccessCode(id){
	
	    var baseurl=jQuery("#baseurl").val();
		var url=baseurl+'products/checkAccessCode';
jQuery('#btn-cart-'+id).addClass('loader_btn');

	 jQuery.post(url,{id : id},function(data){
		 jQuery('#btn-cart-'+id).removeClass('loader_btn');
	var Parsedata = JSON.parse(data);
if(Parsedata.redirect_link!=null && Parsedata.redirect_link!=""){
	 window.location = Parsedata.redirect_link;
	}
	 if(Parsedata.result==0){

jQuery('.header').addClass('n_access_code');
jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');	
jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html(Parsedata.html);
 jQuery('#PopupContainer').fadeIn('fast')
  setTimeout('setPopupBlockPosition()',400);
    }else{
		callFormAjax('cart-form','product','details');
	jQuery('#cart-form-details').submit();	
/*jQuery('#cart-formDetails-'+id).submit();*/}

});



}	

function checkStockList(id,withMsgAppear,form){

	if(validateForm(withMsgAppear,form)){
		
		
	    var baseurl=jQuery("#baseurl").val();
		var url=baseurl+'products/checkStockList';

	 jQuery.post(url,{id : id,details:1},function(data){
	var Parsedata = JSON.parse(data);
	
	if(Parsedata.result==4){
		if(Parsedata.redirect_link != null)
			 window.location = Parsedata.redirect_link;
		}else{
if(Parsedata.result==0){	
jQuery('.header').addClass('n_access_code');
jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');	
jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html(Parsedata.html);
 jQuery('#PopupContainer').addClass(jQuery('#popup_cls').val());
 jQuery('#PopupContainer').fadeIn('fast')
  setTimeout('setPopupBlockPosition()',400);
  	if(jQuery("#updatePopUpForm2").length > 0)
	jQuery("#updatePopUpForm2").dowformvalidate();
	jQuery('#cart-form-details .req_input').each(function(){
		var obj=jQuery(this);
		var val=obj.val();
		var id=obj.attr('id').replace('attr_input_',"");
		jQuery('#fav_attr_input_'+id).val(val);
	});
    }else{
jQuery('#favorites-form').submit();}}

});}
}

function checkListStockList(id){  var baseurl=jQuery("#baseurl").val();
		var url=baseurl+'products/checkStockList';

	 jQuery.post(url,{id : id,list:1},function(data){
	var Parsedata = JSON.parse(data);
		if(Parsedata.result==4){
		if(Parsedata.redirect_link != null)
			 window.location = Parsedata.redirect_link;
		}else{
	 if(Parsedata.result==0){
		
jQuery('.header').addClass('n_access_code');
jQuery('#PopupLoader').html('<div style="text-align:center"><img src="'+baseurl+'front/img/482.GIF" align="center" /></div>');	
jQuery('.shadow').fadeIn('fast');
 jQuery('#PopupLoader').html(Parsedata.html);
 jQuery('#PopupContainer').addClass(jQuery('#popup_cls').val());
 jQuery('#PopupContainer').fadeIn('fast')
  setTimeout('setPopupBlockPosition()',400);
  	if(jQuery("#updatePopUpForm2").length > 0)
	jQuery("#updatePopUpForm2").dowformvalidate();
	 optionsBtn();
    }else{
jQuery('#favorites-form').submit();}}

});}
function submitForm(formId){
	jQuery(formId).submit();}

function addNewCategory(){
	if(jQuery('#addNewCategory_pd').is(":visible")){
		jQuery('#addNewCategory_pd').slideUp('fast');
		scroll_To('#PopupLoader',50);}else{
		jQuery('#addNewCategory_pd').slideDown('fast');
		scroll_To('#addNewCategory_pd',50);}}

function addToStockList(){

	if(validateForm()){
		
	var i=0;
	var checkedValue = jQuery('.stocklist_category:checked').val();
    	var favorite = [];
            jQuery.each(jQuery(".stocklist_category:checked"), function(){            
                favorite.push(jQuery(this).val());
            });
          var categories=favorite.join(",");
		  jQuery('#categories_stocklist').val(categories);
		  jQuery('#favorites-form').submit();
 	jQuery("#PopupLoader input:checkbox").attr('checked', false);
		  /*closePopup();*/}
}	

function getAccessCodeResult(){ 
 if(jQuery('.n_acces_code').length>0){
	return false;}else{
	return true;}
	}
function AccessCodeValidate(){
	
	jQuery('#access_code-error').html('');
	  var baseurl=jQuery("#baseurl").val();
	  var url=baseurl+'products/validateAccessCode';
	  var access_code=jQuery('#access_code').val();
	 	var id_brands=jQuery('#id_brandss').val();
	   var id_product=jQuery('#product_idd').val();
	if(access_code==""){
	jQuery('#access_code-error').html('The access code field is required.');}else{
		 
	 jQuery.post(url,{access_code : access_code,id_product:id_product,id_brands:id_brands},function(data){
	 var Parsedata = JSON.parse(data);
	 if(Parsedata.result==0){
		jQuery('#access_code-error').html('Incorrect access code!');}else{
			
		 closePopup();
		 if(jQuery('#cart-form-details').length>0){
		jQuery('#cart-form-details').submit();}else{
			 jQuery('#product-'+id_product).click(); }}
	 		 
  });	}
	 } 
	 
function addRow(){

	
    var max_fields      = 10;
	 //maximum input boxes allowed
    var wrapper         = jQuery(".input_fields_wrap"); //Fields wrapper
    var add_button      = jQuery(".add_field_button"); //Add button ID
    var baseurl=jQuery('#baseurl').val();
   
	 //initlal text box count
    jQuery(add_button).click(function(e){
		 var x = jQuery('.categories-section tr').length;
	  var rand=Math.floor((Math.random() * 1000) + 1)+"a";//on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
    jQuery(wrapper).append('<tr id="section_repeat_'+rand+'"><td style="width:40px;">'+x+'</td><td><div class="form-item"><input class="textbox" type="text" name="categories['+rand+']" value=""><span id="first_name-error" class="form-error"></span></div></td><td><a tiltle="Delete" alt="Delete"  onclick=removeRow("checklist_categories","id_checklist_categories","'+rand+'") ><img src="'+baseurl+'front/img/delete.png" /></a></td></tr>');
		
/*		var id = 'description_'+rand;
		 CKEDITOR.replace( id,{
       	 filebrowserUploadUrl : '<?php echo base_url(); ?>texteditor/upload'
		  
    	});*/
	
        }
    });
    
    jQuery(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); jQuery(this).parent('div').remove(); x--;
    })	
	
} 				
		
	function checklist_call(){
				if(jQuery(".checklist-chkbx").length>0){
			jQuery(".checklist-chkbx").click(function() {
			var obj=jQuery(this);
			var parent=jQuery(this).parent().parent();
			if(jQuery(".checklist_qty").length>0){
				parent.find('.checklist_qty').each(function(){
					
			 var qty_id=jQuery(this).attr('id');
			 
			 if (obj.prop('checked')==true){ 
			document.getElementById(qty_id).disabled = false;
				}else{
			document.getElementById(qty_id).disabled = true;}})};
			});
			checkAllAuto();
		}
	if(jQuery("#updatePopUpForm").length > 0)
	jQuery("#updatePopUpForm").dowformvalidate();
		}	
		
///////////////////////MULTI QUANTITY/////////////////
jQuery(document).ready(function() {

	
	updateQty();
	
if(jQuery("#password_change").length>0){

jQuery("#password_change").stop(true,true).click(function(){
var obj=jQuery(this).parent();
if(jQuery(".active_change_pass").length>0){
	jQuery(".change_pass").stop(true,true).slideUp('fast',function(){});
	jQuery('#password_change').removeClass('active_change_pass');
	}else{
	jQuery(".change_pass").stop(true,true).slideDown('fast',function(){});
	jQuery('#password_change').addClass('active_change_pass');
	}

});}	
if(jQuery('.readmore').length>0){
jQuery('.readmore').readmore({
  collapsedHeight: 100,
  speed: 200
});}



if(jQuery('.product_details_descrioption').length>0 && jQuery('.inner_product_details_descrioption').height()>58 ){
	
jQuery('.products_details  .read_more').show();
jQuery(".products_details  .read_more").stop(true,true).click(function(){
	scroll_To('.tabs_cont',0);});
}

/*qtyAutoComplete();*/

});	

function updateQty(){

jQuery(".updateQty").click(function() {

jQuery(this).keyup(function(){
var obj=jQuery(this);
var return_qty=jQuery(this).val();
jQuery('#updateChecklist').submit();
	
	});});
	updateQty_favorites();}
function checkDiscountType(obj){
		var discount_type=jQuery(obj).val();
		var id=jQuery(obj).attr('id').replace('discount_type_','');
	var price_value=jQuery('#list_price_'+id).val();
	var discount=jQuery('#discount_'+id).val();
	 if(discount_type==1){
		 if(discount>100 || discount<0){
			 /* jQuery('#discount_'+id).val(0);*/
			 alert('Discount should be between 0 and 100.');
			 jQuery(obj).val(2);
		 }
		 }else{
			 if(price_value<discount){
			/*jQuery('#discount_'+id).val(0);*/
			jQuery(obj).val(1);
			alert('Discount exceed the amount of selected product.');
			  } 
			 }
		}
function updateQty_cart(){
	
if(jQuery(".updateQty_cart").length>0){
jQuery(".updateQty_cart").focusout(function() {
if(jQuery(this).val()==""){
	jQuery(this).val(jQuery('#old_qty').val());}
  })
jQuery(".updateQty_cart").focus(function() {
	
jQuery('#old_qty').val(jQuery(this).val());
jQuery(this).val('');

jQuery(this).keyup(function(){	
var obj=jQuery(this);
var bool=true;
if(jQuery('#quotation_cart').length>0){
	
	var id=obj.attr('id').replace('discount_','');
	var price_value=parseInt(jQuery('#list_price_'+id).val());
	var discount=parseInt(jQuery('#discount_'+id).val());
	var discount_type=jQuery('#discount_type_'+id).val();
	
	 if(discount_type==1){
		 if(discount>100 || discount<0){
			 jQuery('#discount_'+id).val(0);
			 bool=false;
			 alert('Discount should be between 0 and 100.');
		}}else{ if(price_value<discount){
	
			jQuery('#discount_'+id).val(0);
			bool=false;
			alert('Discount exceed the amount of selected product.');
			  } 
			 }
	
	}
if(bool){
/*var return_qty=jQuery(this).val();*/
jQuery('#update_cart_form').submit();}

});});}}

function updateQty_favorites(){
	
if(jQuery(".updateQty_favorites").length>0){
	
jQuery(".updateQty_favorites").focus(function() {

jQuery(this).keyup(function(){	

var obj=jQuery(this);
var bool=true;
if(jQuery('#quotation_cart').length>0){}
if(bool){
/*var return_qty=jQuery(this).val();*/
jQuery('#updateChecklist').submit();}

});});}}

function qtyAutoComplete(){
var base_url = jQuery('#baseurl').val();
if(jQuery('#qty').length>0){
jQuery('#qty').autocomplete({
//source: siteurl+"/establishment/restaurant",
minLength:1,
close:function(event,ui){
var qty = jQuery('input[name="qty"]').val();
var arr = qty.split('__');
var val = arr [0];
var id = arr [1];

jQuery('input[name="qty"').val(val);

},
source: function( request, response ) {

var combination="";
var i=0;

	jQuery('.product-options-dd .form-item').each(function(){
	
			var arr=jQuery(this).attr('id').split('_');
			var id_attribute=arr[1];

			var val=jQuery('#attr_input_'+id_attribute).val();
					
	if(val!=""){
			if(i!=0){
			combination=combination+","+val;}else{
			combination=val;}
			i++;
			}})
	
var qty = jQuery('input[name="qty"]').val();

var id_products=jQuery('#id_product').val();
jQuery.ajax({
  url: base_url+"products/getMultiQty",
  dataType: "json",
  data: {term: request.term,qty:qty,id_products:id_products,combination:combination},
  type:"POST",
  success: function(data) {
     response(jQuery.map(data, function(item) {
		
	 return {
      label: item.label,
	  value: item.value,
	   id: item.id,
      };
    }));
   }
   
  });
},
});}}	
function zoomHoverCall(){

	if(jQuery(".zoom-image").length>0){
	jQuery(".zoom-image").stop(true,true).hover(function(){
	var obj=jQuery(this);
	var id=obj.attr('id');
zoomCall(id);
	  },function(){
		}); }}
		
function zoomCall(id){
var background=jQuery("#img_"+id).attr('data-zoom-image');
jQuery('.zoomWindow').css({'background-image': ''});


var a = document.getElementById('popup_details'); //or grab it by tagname etc
a.href = background.replace('854x854/','');

/*jQuery('.zoomWindow').css({'background-image': 'url("'+background+'")'});*/
jQuery("#img_"+id).elevateZoom({
/*	responsive : true,
	scrollZoom : true,
    cursor: 'crosshair'*/
	constrainType:"height", constrainSize:274, zoomType: "lens", containLensZoom: true, cursor: 'pointer'
	});

}

function unseletConsultant(id){
	jQuery('#consultants_request_'+id).find('.checked_input').val('');	
	jQuery('#consultants_request_'+id).slideUp('fast');
	}
	
function productAnimate(id){
	
	var animateLeftTo=jQuery('#cart_items_count').offset().left;
	var animateTopTo=jQuery('#cart_items_count').offset().top;
	


	var animateLeftFrom=jQuery(id).offset().left;
	var animateTopFrom=jQuery(id).offset().top;
	
	
var newElement=jQuery(id).eq(0).clone();
newElement.removeClass('added');

newElement.css({
	'position':'absolute',
	'overflow':'hidden',
	'top':animateTopFrom,
	'background':'white',
	'left':animateLeftFrom,
	})
jQuery("body").append(newElement); //add the element 

//Now animate
var speed=200;
newElement.stop().animate({ 'top': animateTopTo,'left': animateLeftTo,opacity:0,width:0,height:0}, speed,function() {
	setTimeout(function(){newElement.remove();}, 600);
  });

}		


 function loadSubModuleBrand(url,module,ref_module,obj,id)
{
	var siteurl = jQuery("#baseurl").val();
var id_brands = jQuery("input[name='id_brands']").val();
	
	var refVal = "";
	var proceed = true;
	if(obj.value == "") {
//alert("Please select the "+module);

if(module=="categories" && id!=""){
	
	refVal=id;
	}else{
			proceed = false;}
	
	}else{
	refVal=obj.value;	}

	if(proceed) {
		jQuery(obj).parent().addClass('loader-select');
		if(module=="categories"){
			jQuery('<div class="bx-loader"></div>').appendTo('.search_leftside');
		
		jQuery("select, input").attr("disabled","disabled");
		jQuery.ajax({
			url:url,
			type:'POST',
			data:{module: module,ref_module : ref_module, ref : refVal,id_category:refVal,id_brands:id_brands},
			success:function(data){
				
				var parseData = JSON.parse(data);

				jQuery('#search-brand').html(parseData.html);
				jQuery("select, input").removeAttr("disabled");
				jQuery(obj).parent().removeClass('loader-select');
			},
		});}
		
		
	}
	else {
		return false;
		//setTimeout("removeModalBack()",200);
	}
}

function loadPhone(){
	if(jQuery('.mobile-m').length>0){
  var baseurl=jQuery('#baseurl').val();
 jQuery(".mobile-m").intlTelInput({
  utilsScript: baseurl+"front/lib/libphonenumber/build/utils.js",defaultCountry: "auto",
 });}}
 

	 
	function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
} 


function loadMenuBtns(){
	
	if(jQuery('.menu-toggle2').length>0){
	jQuery('.menu-toggle2').click(function () {
var obj=jQuery(this);
var obj_parent=jQuery(this).parent();



		
/*if(jQuery('.menu-toggle.active').length>0){
	var id_old=jQuery('.menu-toggle.active').attr('id').replace('mob_','');
	alert(id);
if(id_old!=id){
jQuery('.menuMobCont .menuBlg').css({'display':'none'});
jQuery('.menuMobCont .menuBlg').removeClass('activeMenu');
jQuery('.menu-toggle').removeClass('active');}}*/

if(obj_parent.find('.sub-dropdown').is(":visible")){
obj_parent.removeClass('active');	
}else{
obj_parent.addClass('active');}
		});}
	}
	
function CustomScrollbarBrands(){
	if(jQuery(".CustomScrollbarBrands").length>0){
	 jQuery(".CustomScrollbarBrands").mCustomScrollbar();}}	

