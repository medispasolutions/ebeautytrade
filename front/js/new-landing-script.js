function displaydiv(buttondiv) {
    if (!$("input[name='optradio']:checked").val()) {
        alert('Please select above whether you are a supplier in UAE or Overseas to proceed.');
        return false;
    }
        
    if (buttondiv == 'companyDetailsBack') {
        $("#contact-person").show();
        $("#company-details").hide();
        $("#brands-details").hide();
        $("#bundle-details").hide();
        $("#step1").addClass("active");
        $("#step2").removeClass("active");
        $("#step3").removeClass("active");
        $("#step4").removeClass("active");
    } else if (buttondiv == 'contactperson' || buttondiv == 'brandsDetailsBack') {
        /* Remove all above*/
        $(".landing-top").hide();
        $(".partner-section").hide();
        $(".manage-section").hide();
        $(".dashboard-section").hide();
        $(".partners-section").hide();
        $(".additional-services-section").hide();
        $(".arrow_box").hide();
        $(".becomeTop").hide();
        $(".brand-section").addClass("clearSection");
        
        $(".form-selected").addClass("application-selected");
        $(".overseasFrom").hide();
        $(".footer-msg").hide();
        if (document.getElementById("first-name").value == "") {
            alert("Please enter your first Name");
            return false;
        } else if (document.getElementById("last-name").value == "") {
            alert("Please enter your Last Name");
            return false;
        } else if (document.getElementById("position").value == "") {
            alert("Please enter Your Position");
            return false;
        } else if (document.getElementById("phone").value == "") {
            alert("Please enter Your phone Number");
            return false;
        } else if (document.getElementById("email").value == "") {
            alert("Please enter Your email");
            return false;
        } else if (document.getElementById("email").value != "") {
            var email = document.getElementById('email');
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(email.value)) {
                alert('Please provide a valid email address');
                email.focus;
                return false;
            }
            var email = document.getElementById("email").value;
            var confemail = document.getElementById("confemail").value;
            if (email != confemail) {
                alert('Email Not Matching!');
                return false;
            }
            $("#contact-person").hide();
            $("#company-details").show();
            $("#brands-details").hide();
            $("#bundle-details").hide();
            $("#step2").addClass("active");
            $("#step1").removeClass("active");
            $("#step3").removeClass("active");
            $("#step4").removeClass("active");
            
            var fname = document.getElementById("first-name").value;
            var lname = document.getElementById("last-name").value;
            var position = document.getElementById("position").value;
            var phone = document.getElementById("phone").value;
            var email = document.getElementById("email").value;
            var applicationtype = document.getElementById("application-type").value;
        
            var dataString = 'fname1=' + fname + '&lname1=' + lname + '&position1=' + position + '&phone1=' + phone + '&email1=' + email + '&applicationtype1=' + applicationtype;
        
            // AJAX code to submit form.
            $.ajax({
                type: "POST",
                url: "Landing/contactperson",
                data: dataString,
                cache: false,
                success: function(html) {
                    $('#userid').val(html);
                }
            });
        }
    } else if (buttondiv == 'companyDetailsNext') {
        if (document.getElementById("company-name").value == "") {
            alert("Please enter your Company Legal Name");
            return false;
        } else if (document.getElementById("company-address").value == "") {
            alert("Please enter your Company Address");
            return false;
        } else if (document.getElementById("company-number").value == "") {
            alert("Please enter your Company Phone Number");
            return false;
        } else if (document.getElementById("city").value == "") {
            alert("Please name the City where your Company is based");
            return false;
        } else if (document.getElementById("business-based").value == "") {
            alert("Please select the Country where your company is based");
            return false;
        } else {
            $("#contact-person").hide();
            $("#company-details").hide();
            $("#brands-details").show();
            $("#bundle-details").hide();
            $("#step3").addClass("active");
            $("#step2").removeClass("active");
            $("#step1").removeClass("active");
            $("#step4").removeClass("active");
            $(".sellerTitle").show();
            $(".application-steps").show();
            $(".backToBrand").hide();
            
            var companyName = document.getElementById("company-name").value;
            var registrationNumber = document.getElementById("registration-number").value;
            var companyAddress = document.getElementById("company-address").value;
            var companyNumber = document.getElementById("company-number").value;
            var city = document.getElementById("city").value;
            var businessBased = document.getElementById("business-based").value;
            var website = document.getElementById("website").value;
            var userid = document.getElementById("userid").value;
        
            var dataString = 'companyName1=' + companyName + '&registrationNumber1=' + registrationNumber + '&companyAddress1=' + companyAddress + '&companyNumber1=' + companyNumber + '&businessBased1=' + businessBased + '&city1=' + city + '&website1=' + website + '&userid1=' + userid;
        
            // AJAX code to submit form.
            $.ajax({
                type: "POST",
                url: "Landing/companydetails",
                data: dataString,
                cache: false,
                success: function(html) {
                    return true;
                }
            });
            
        }
    } else if (buttondiv == 'brandsDetailsNext') {
        if (document.getElementById("brand-name1").value == "") {
            alert("Please enter at least 1 brand name");
            return false;
        } else if (document.getElementById("country1").value == "") {
            alert("Please select the Country made of the Brand");
            return false;
        } else {
            $("#contact-person").hide();
            $("#company-details").hide();
            $("#brands-details").hide();
            $("#bundle-details").show();
            $("#step4").addClass("active");
            $("#step3").removeClass("active");
            $("#step1").removeClass("active");
            $("#step2").removeClass("active");
            $(".sellerTitle").hide();
            $(".application-steps").hide();
            $(".backToBrand").show();
            
            var amountofbrands = 0;
            var amountofbrands = document.getElementById("amountofbrands").value;

            for (i = 1; i <= amountofbrands; i++) {
                var brandname = document.getElementById("brand-name" + i).value;
                var country = document.getElementById("country" + i).value;
                var brandwebsite = document.getElementById("brand-website" + i).value;
                var random = document.getElementById("random-number").value;
                
                dataString = 'brandname1=' + brandname + '&country1=' + country + i + '&brandwebsite1=' + brandwebsite + '&random1=' + random;
                
                // AJAX code to submit form.
                $.ajax({
                    type: "POST",
                    url: "Landing/brandsdetails",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        return true;
                    }
                });
            }
        }
    }

    if (buttondiv == 'OverseascompanyDetailsBack') {
        $(".overseacontact-person").show();
        $(".overseacompany-details").hide();
        $(".overseabrands-details").hide();
        $(".overseabundle-details").hide();
        $("#overseas_step1").addClass("active");
        $("#overseas_step2").removeClass("active");
        $("#overseas_step3").removeClass("active");
        $("#overseas_step4").removeClass("active");
    } else if (buttondiv == 'Overseascontactperson' || buttondiv == 'OverseasbrandsDetailsBack') {
        $(".landing-top").hide();
        $(".partner-section").hide();
        $(".manage-section").hide();
        $(".dashboard-section").hide();
        $(".partners-section").hide();
        $(".additional-services-section").hide();
        $(".arrow_box").hide();
        $(".becomeTop").hide();
        $(".brand-section").addClass("clearSection");
        
        $(".applicationForm").hide();
        $(".form-selected").addClass("overseas-selected");
        
        if (document.getElementById("overseas_first-name").value == "") {
            alert("Please enter first name");
            return false;
        } else if (document.getElementById("overseas_last-name").value == "") {
            alert("Please enter last name");
            return false;
        } else if (document.getElementById("overseas_position").value == "") {
            alert("Please enter position");
            return false;
        } else if (document.getElementById("overseas_phone").value == "") {
            alert("Please enter phone");
            return false;
        } else if (document.getElementById("overseas_email").value == "") {
            alert("Please enter email");
            return false;
        } else if (document.getElementById("overseas_email").value != "") {
            var email = document.getElementById('overseas_email');
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!filter.test(email.value)) {
                alert('Please provide a valid email address');
                email.focus;
                return false;
            }
            var email = document.getElementById("overseas_email").value
            var confemail = document.getElementById("overseas_confemail").value
            if (email != confemail) {
                alert('Email Not Matching!');
                return false;
            }
            $(".overseacontact-person").hide();
            $(".overseacontact-person").addClass("dvdffd");
            $(".overseacompany-details").show();
            $(".overseabrands-details").hide();
            $(".overseabundle-details").hide();
            $("#overseas_step2").addClass("active");
            $("#overseas_step1").removeClass("active");
            $("#overseas_step3").removeClass("active");
            $("#overseas_step4").removeClass("active");
            
            var fname = document.getElementById("overseas_first-name").value;
            var lname = document.getElementById("overseas_last-name").value;
            var position = document.getElementById("overseas_position").value;
            var phone = document.getElementById("overseas_phone").value;
            var email = document.getElementById("overseas_email").value;
            var applicationtype = document.getElementById("overseas_application-type").value;
        
            var dataString = 'fname1=' + fname + '&lname1=' + lname + '&position1=' + position + '&phone1=' + phone + '&email1=' + email + '&applicationtype1=' + applicationtype;
        
            // AJAX code to submit form.
            $.ajax({
                type: "POST",
                url: "Landing/contactperson",
                data: dataString,
                cache: false,
                success: function(html) {
                    $('#overseas_userid').val(html);
                }
            });
        }
    } else if (buttondiv == 'OverseascompanyDetailsNext') {
        if (document.getElementById("overseas_company-name").value == "") {
            alert("Please enter your Company Legal Name");
            return false;
        } else if (document.getElementById("overseas_company-address").value == "") {
            alert("Please enter your Company Address");
            return false;
        } else if (document.getElementById("overseas_company-number").value == "") {
            alert("Please enter your Company Phone Number");
            return false;
        } else if (document.getElementById("overseas_city").value == "") {
            alert("Please name the City where your Company is based");
            return false;
        } else if (document.getElementById("overseas_business-based").value == "") {
            alert("Please select the Country where your company is based");
            return false;
        } else {
            $(".overseacontact-person").hide();
            $(".overseacompany-details").hide();
            $(".overseabrands-details").show();
            $(".overseabundle-details").hide();
            $("#overseas_step3").addClass("active");
            $("#overseas_step2").removeClass("active");
            $("#overseas_step1").removeClass("active");
            $("#overseas_step4").removeClass("active");
            $(".sellerTitle").show();
            $(".application-steps").show();
            $(".backToBrand").hide();
            
            var companyName = document.getElementById("overseas_company-name").value;
            var registrationNumber = document.getElementById("overseas_registration-number").value;
            var companyAddress = document.getElementById("overseas_company-address").value;
            var companyNumber = document.getElementById("overseas_company-number").value;
            var city = document.getElementById("overseas_city").value;
            var businessBased = document.getElementById("overseas_business-based").value;
            var website = document.getElementById("overseas_website").value;
            var userid = document.getElementById("overseas_userid").value;
        
            var dataString = 'companyName1=' + companyName + '&registrationNumber1=' + registrationNumber + '&companyAddress1=' + companyAddress + '&companyNumber1=' + companyNumber + '&businessBased1=' + businessBased + '&city1=' + city + '&website1=' + website + '&userid1=' + userid;
        
            // AJAX code to submit form.
            $.ajax({
                type: "POST",
                url: "Landing/companydetails",
                data: dataString,
                cache: false,
                success: function(html) {
                    return true;
                }
            });
        }
    } else if (buttondiv == 'OverseasbrandsDetailsNext') {
        $(".footer-msg").hide();
        if (document.getElementById("overseas_brand-name1").value == "") {
            alert("Please enter at least 1 brand name");
            return false;
        } else if (document.getElementById("overseas_country1").value == "") {
            alert("Please select the Country made of the Brand");
            return false;
        } else {
            $(".overseacontact-person").hide();
            $(".overseacompany-details").hide();
            $(".overseabrands-details").hide();
            $(".overseabundle-details").show();
            $("#overseas_step4").addClass("active");
            $("#overseas_step3").removeClass("active");
            $("#overseas_step1").removeClass("active");
            $("#overseas_step2").removeClass("active");
            $(".sellerTitle").hide();
            $(".application-steps").hide();
            $(".backToBrand").show();
            
            var amountofbrands = 0;
            var amountofbrands = document.getElementById("Overseasamountofbrands").value;

            for (i = 1; i <= amountofbrands; i++) {
                var brandname = document.getElementById("overseas_brand-name" + i).value;
                var country = document.getElementById("overseas_country" + i).value;
                var brandwebsite = document.getElementById("overseas_brand-website" + i).value;
                var random = document.getElementById("overseas_random-number").value;
                
                dataString = 'brandname1=' + brandname + '&country1=' + country + i + '&brandwebsite1=' + brandwebsite + '&random1=' + random;
                
                // AJAX code to submit form.
                $.ajax({
                    type: "POST",
                    url: "Landing/brandsdetails",
                    data: dataString,
                    cache: false,
                    success: function(html) {
                        return true;
                    }
                });
            }

            var branddescription = document.getElementById("Overseasbrand-description").value;
            var random = document.getElementById("random-number").value;
            var userid = document.getElementById("overseas_userid").value;
        
            var dataString = 'branddescription1=' + branddescription +'&random1=' + random + '&userid1=' + userid;
            
            // AJAX code to submit form.
            $.ajax({
                type: "POST",
                url: "Landing/brandinfo",
                data: dataString,
                cache: false,
                success: function(html) {
                    return true;
                }
            });
        }
    }
}

$(function () {
    $("input#first-name").keyup(function(){
        if (!$("input[name='optradio']:checked").val()) {
            alert('Please select above whether you are a supplier in UAE or Overseas to proceed.');
            $(this).val('');
            return false;
        }
    });
    
    $("input#last-name").keyup(function(){
        if (!$("input[name='optradio']:checked").val()) {
            alert('Please select above whether you are a supplier in UAE or Overseas to proceed.');
            $(this).val('');
            return false;
        }
    });
    
    $("input#position").keyup(function(){
        if (!$("input[name='optradio']:checked").val()) {
            alert('Please select above whether you are a supplier in UAE or Overseas to proceed.');
            $(this).val('');
            return false;
        }
    });
    
    $("input#phone").keyup(function(){
        if (!$("input[name='optradio']:checked").val()) {
            alert('Please select above whether you are a supplier in UAE or Overseas to proceed.');
            $(this).val('');
            return false;
        }
        /*
        var max_chars = 5;
        //debugger;
        if ($(this).val().length >= max_chars) {
        
        var telInput = $("#phone"),  errorMsg = $("#error-msg"),  validMsg = $("#valid-msg");

        // initialise plugin
        telInput.intlTelInput({
        utilsScript:"https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.4/js/utils.js"
        });
        
        var reset = function() {
          telInput.removeClass("error");
          errorMsg.addClass("hide");
          validMsg.addClass("hide");
        };
        
        // on blur: validate
        telInput.blur(function() {
          reset();
          if ($.trim(telInput.val())) {
            if (telInput.intlTelInput("isValidNumber")) {
              validMsg.removeClass("hide");
              var getCode = telInput.intlTelInput('getSelectedCountryData').dialCode;
              alert(getCode);
            } else {
              telInput.addClass("error");
              errorMsg.removeClass("hide");
            }
          }
        });
        }*/
    });
    
    $("input#email").keyup(function(){
        if (!$("input[name='optradio']:checked").val()) {
            alert('Please select above whether you are a supplier in UAE or Overseas to proceed.');
            $(this).val('');
            return false;
        }
    });
    
    $("input#confemail").keyup(function(){
        if (!$("input[name='optradio']:checked").val()) {
            alert('Please select above whether you are a supplier in UAE or Overseas to proceed.');
            $(this).val('');
            return false;
        }
    });
    
    $('#email').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $('#overseas_email').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $('#overseas_confemail').bind("cut copy paste", function (e) {
        e.preventDefault();
    });

    $('#confemail').bind("cut copy paste", function (e) {
        e.preventDefault();
    });
});

$(document).ready(function () {
    $('input[type="radio"]').click(function () {
        if ($(this).attr("value") == "application") {
            $("#sapplication").show();
            $("#soverseas").hide();
        }
        if ($(this).attr("value") == "overseas") {
            $("#sapplication").hide();
            $("#soverseas").show();
        }
        if ($(this).attr("value") == "1") {
            $("#p1").addClass("active");
            $("#p2").removeClass("active");
            $("#p3").removeClass("active");
            $("#p4").removeClass("active");
            $("#bundlep1").addClass("active");
            $("#bundlep2").removeClass("active");
            $("#bundlep3").removeClass("active");
            $("#bundlep4").removeClass("active");
            $("#bundlep5").removeClass("active");
            $("#bundlep6").removeClass("active");
            $("#bundlep7").removeClass("active");
            $("#bundlep8").removeClass("active");
        }
        if ($(this).attr("value") == "2") {
            $("#p1").removeClass("active");
            $("#p2").addClass("active");
            $("#p3").removeClass("active");
            $("#p4").removeClass("active");
            $("#bundlep1").removeClass("active");
            $("#bundlep2").addClass("active");
            $("#bundlep3").removeClass("active");
            $("#bundlep4").removeClass("active");
            $("#bundlep5").removeClass("active");
            $("#bundlep6").removeClass("active");
            $("#bundlep7").removeClass("active");
            $("#bundlep8").removeClass("active");
        }
        if ($(this).attr("value") == "3") {
            $("#p1").removeClass("active");
            $("#p2").removeClass("active");
            $("#p3").addClass("active");
            $("#p4").removeClass("active");
            $("#bundlep1").removeClass("active");
            $("#bundlep2").removeClass("active");
            $("#bundlep3").addClass("active");
            $("#bundlep4").removeClass("active");
            $("#bundlep5").removeClass("active");
            $("#bundlep6").removeClass("active");
            $("#bundlep7").removeClass("active");
            $("#bundlep8").removeClass("active");
        }
        if ($(this).attr("value") == "4") {
            $("#p1").removeClass("active");
            $("#p2").removeClass("active");
            $("#p3").removeClass("active");
            $("#p4").addClass("active");
            $("#bundlep1").removeClass("active");
            $("#bundlep2").removeClass("active");
            $("#bundlep3").removeClass("active");
            $("#bundlep4").addClass("active");
            $("#bundlep5").removeClass("active");
            $("#bundlep6").removeClass("active");
            $("#bundlep7").removeClass("active");
            $("#bundlep8").removeClass("active");
        }
        if ($(this).attr("value") == "9") {
            $("#p1").removeClass("active");
            $("#p2").removeClass("active");
            $("#p3").removeClass("active");
            $("#p4").addClass("active");
            $("#bundlep1").removeClass("active");
            $("#bundlep2").removeClass("active");
            $("#bundlep3").removeClass("active");
            $("#bundlep4").removeClass("active");
            $("#bundlep5").addClass("active");
            $("#bundlep6").removeClass("active");
            $("#bundlep7").removeClass("active");
            $("#bundlep8").removeClass("active");
        }
        if ($(this).attr("value") == "10") {
            $("#p1").removeClass("active");
            $("#p2").removeClass("active");
            $("#p3").removeClass("active");
            $("#p4").addClass("active");
            $("#bundlep1").removeClass("active");
            $("#bundlep2").removeClass("active");
            $("#bundlep3").removeClass("active");
            $("#bundlep4").removeClass("active");
            $("#bundlep5").removeClass("active");
            $("#bundlep6").addClass("active");
            $("#bundlep7").removeClass("active");
            $("#bundlep8").removeClass("active");
        }
        if ($(this).attr("value") == "11") {
            $("#p1").removeClass("active");
            $("#p2").removeClass("active");
            $("#p3").removeClass("active");
            $("#p4").addClass("active");
            $("#bundlep1").removeClass("active");
            $("#bundlep2").removeClass("active");
            $("#bundlep3").removeClass("active");
            $("#bundlep4").removeClass("active");
            $("#bundlep5").removeClass("active");
            $("#bundlep6").removeClass("active");
            $("#bundlep7").addClass("active");
            $("#bundlep8").removeClass("active");
        }
        if ($(this).attr("value") == "12") {
            $("#p1").removeClass("active");
            $("#p2").removeClass("active");
            $("#p3").removeClass("active");
            $("#p4").addClass("active");
            $("#bundlep1").removeClass("active");
            $("#bundlep2").removeClass("active");
            $("#bundlep3").removeClass("active");
            $("#bundlep4").removeClass("active");
            $("#bundlep5").removeClass("active");
            $("#bundlep6").removeClass("active");
            $("#bundlep7").removeClass("active");
            $("#bundlep8").addClass("active");
        }
        if ($(this).attr("value") == "5") {
            $("#bundlep9").addClass("active");
            $("#bundlep10").removeClass("active");
        }
        if ($(this).attr("value") == "6") {
            $("#bundlep9").removeClass("active");
            $("#bundlep10").addClass("active");
        }
    });

    /* UAE */
    $("#freebrandnumber").on("change",function(){
        /*
        $("#free-terms").show();
        $("#gold-terms").hide();
        $("#fbe-terms").hide();
        */
        
        if ($("#freebrandnumber").val() === "") {
            var sendbtn = document.getElementById('uae-free-button');
            sendbtn.disabled = true;
        } else {
            var sendbtn = document.getElementById('uae-free-button');
            sendbtn.disabled = false;
        }
        //$("#free-terms input").attr('required', true);
        
        var sendbtn = document.getElementById('uae-gold-button');
        sendbtn.disabled = true;
        //$("#gold-terms input").attr('required', false);
        
        var sendbtn = document.getElementById('uae-fbe-button');
        sendbtn.disabled = true;
        //$("#fbe-terms input").attr('required', false);
        
        $("#goldbrandnumber").get(0).selectedIndex = 0;
        $("#fbebrandnumber").get(0).selectedIndex = 0;
            
    });
    $("#goldbrandnumber").on("change",function(){
        /*
        $("#free-terms").hide();
        $("#gold-terms").show();
        $("#fbe-terms").hide();
        */
        
        var sendbtn = document.getElementById('uae-free-button');
        sendbtn.disabled = true;
        //$("#free-terms input").attr('required', false);
        
        if ($("#goldbrandnumber").val() === "") {
            var sendbtn = document.getElementById('uae-gold-button');
            sendbtn.disabled = true;
        } else {
            var sendbtn = document.getElementById('uae-gold-button');
            sendbtn.disabled = false;
        }
        
        //$("#gold-terms input").attr('required', true);
        
        var sendbtn = document.getElementById('uae-fbe-button');
        sendbtn.disabled = true;
        //$("#fbe-terms input").attr('required', false);
        
        $("#freebrandnumber").get(0).selectedIndex = 0;
        $("#fbebrandnumber").get(0).selectedIndex = 0;
        
        var val=$(this).find('option:selected').val();
        if(val == "gold1"){
            $("#gold-price-year").text("1 548");
            $("#gold-price-month").text("129");
        } else if(val == "gold2"){
            $("#gold-price-year").text("2 148")
            $("#gold-price-month").text("179");;
        } else if(val == "gold3"){
            $("#gold-price-year").text("2 748");
            $("#gold-price-month").text("229");
        } else if(val == "gold4"){
            $("#gold-price-year").text("3 588");
            $("#gold-price-month").text("299");
        }
    });
    $("#fbebrandnumber").on("change",function(){
        /*
        $("#free-terms").hide();
        $("#gold-terms").hide();
        $("#fbe-terms").show();
        */
        $("#freebrandnumber").get(0).selectedIndex = 0;
        $("#goldbrandnumber").get(0).selectedIndex = 0;
        
        var sendbtn = document.getElementById('uae-free-button');
        sendbtn.disabled = true;
        //$("#free-terms input").attr('required', false);
        
        var sendbtn = document.getElementById('uae-gold-button');
        sendbtn.disabled = true;
        //$("#gold-terms input").attr('required', false);
        
        if ($("#fbebrandnumber").val() === "") {
            var sendbtn = document.getElementById('uae-fbe-button');
            sendbtn.disabled = true;
        } else {
            var sendbtn = document.getElementById('uae-fbe-button');
            sendbtn.disabled = false;
        }
        //$("#fbe-terms input").attr('required', true);
        
        var val=$(this).find('option:selected').val();
        if(val == "fbe1"){
            $("#fbe-price-year").text("2 748");
            $("#fbe-price-month").text("229");
        } else if(val == "fbe2"){
            $("#fbe-price-year").text("4 548")
            $("#fbe-price-month").text("379");;
        } else if(val == "fbe3"){
            $("#fbe-price-year").text("6 348");
            $("#fbe-price-month").text("529");
        } else if(val == "fbe4"){
            $("#fbe-price-year").text("8 148");
            $("#fbe-price-month").text("679");
        }
    });
    
    /* overseas */
    $("#freebrandnumberoverseas").on("change",function(){
        /*
        $("#free-terms-overseas").show();
        $("#gold-terms-overseas").hide();
        $("#fbe-terms-overseas").hide();
        */
        $("#goldbrandnumberoverseas").get(0).selectedIndex = 0;
        $("#fbebrandnumberoverseas").get(0).selectedIndex = 0;
        
        if ($("#freebrandnumberoverseas").val() === "") {
            var sendbtn = document.getElementById('overseas-free-button');
            sendbtn.disabled = true;
        } else {
            var sendbtn = document.getElementById('overseas-free-button');
            sendbtn.disabled = false;
        }
        //$("#free-terms-overseas input").attr('required', true);
        
        var sendbtn = document.getElementById('overseas-gold-button');
        sendbtn.disabled = true;
        //$("#gold-terms-overseas input").attr('required', false);
        
        var sendbtn = document.getElementById('overseas-fbe-button');
        sendbtn.disabled = true;
        //$("#fbe-terms-overseas input").attr('required', false);
        
    });
    $("#goldbrandnumberoverseas").on("change",function(){
        /*
        $("#free-terms-overseas").hide();
        $("#gold-terms-overseas").show();
        $("#fbe-terms-overseas").hide();
        */
        $("#freebrandnumberoverseas").get(0).selectedIndex = 0;
        $("#fbebrandnumberoverseas").get(0).selectedIndex = 0;
        
        var sendbtn = document.getElementById('overseas-free-button');
        sendbtn.disabled = true;
        //$("#free-terms-overseas input").attr('required', false);
        
        if ($("#goldbrandnumberoverseas").val() === "") {
            var sendbtn = document.getElementById('overseas-gold-button');
            sendbtn.disabled = true;
        } else {
            var sendbtn = document.getElementById('overseas-gold-button');
            sendbtn.disabled = false;
        }
        //$("#gold-terms-overseas input").attr('required', true);
        
        var sendbtn = document.getElementById('overseas-fbe-button');
        sendbtn.disabled = true;
        //$("#fbe-terms-overseas input").attr('required', false);
        
        var val=$(this).find('option:selected').val();
        if(val == "gold1-overseas"){
            $("#gold-price-year-overseas").text("948");
            $("#gold-price-month-overseas").text("79");
        } else if(val == "gold2-overseas"){
            $("#gold-price-year-overseas").text("1 548")
            $("#gold-price-month-overseas").text("119");;
        } else if(val == "gold3-overseas"){
            $("#gold-price-year-overseas").text("2 748");
            $("#gold-price-month-overseas").text("229");
        }
    });
    $("#fbebrandnumberoverseas").on("change",function(){
        /*
        $("#free-terms-overseas").hide();
        $("#gold-terms-overseas").hide();
        $("#fbe-terms-overseas").show();
        */
        $("#freebrandnumberoverseas").get(0).selectedIndex = 0;
        $("#goldbrandnumberoverseas").get(0).selectedIndex = 0;
        
        var sendbtn = document.getElementById('overseas-free-button');
        sendbtn.disabled = true;
        //$("#free-terms-overseas input").attr('required', false);
        
        var sendbtn = document.getElementById('overseas-gold-button');
        sendbtn.disabled = true;
        //$("#gold-terms-overseas input").attr('required', false);
        
        if ($("#fbebrandnumberoverseas").val() === "") {
            var sendbtn = document.getElementById('overseas-fbe-button');
            sendbtn.disabled = true;
        } else {
            var sendbtn = document.getElementById('overseas-fbe-button');
            sendbtn.disabled = false;
        }
        //$("#fbe-terms-overseas input").attr('required', true);
        
        var val=$(this).find('option:selected').val();
        if(val == "fbe1-overseas"){
            $("#fbe-price-year-overseas").text("1 200");
            $("#fbe-price-month-overseas").text("100");
        } else if(val == "fbe2-overseas"){
            $("#fbe-price-year-overseas").text("2 400");
            $("#fbe-price-month-overseas").text("200");
        } else if(val == "fbe3-overseas"){
            $("#fbe-price-year-overseas").text("3 600");
            $("#fbe-price-month-overseas").text("300");
        }
    });
    
    
    /* Mobile Version */
    
    $('#uae-free-button-mobile').keypress(function (e) {
        if (e.keyCode == 13) {
            return false;
        } else {
            alert("select number of brands");
        }
    });
    $('#uae-free-button-mobile').click(function () {
        if ($("#freebrandnumber-mobile").val() === "") {
            alert("select number of brands");
            return false;
        }
            $("#goldbrandnumber-mobile").get(0).selectedIndex = 0;
            $("#fbebrandnumber-mobile").get(0).selectedIndex = 0;
    });
    
    $('#uae-gold-button-mobile').keypress(function (e) {
        if (e.keyCode == 13) {
            return false;
        } else {
            alert("select number of brands");
        }
    });
    $('#uae-gold-button-mobile').click(function () {
        if ($("#goldbrandnumber-mobile").val() === "") {
            alert("select number of brands");
            return false;
        }
            $("#freebrandnumber-mobile").get(0).selectedIndex = 0;
            $("#fbebrandnumber-mobile").get(0).selectedIndex = 0;
    });
    
    $('#uae-fbe-button-mobile').keypress(function (e) {
        if (e.keyCode == 13) {
            return false;
        } else {
            alert("select number of brands");
        }
    });
    $('#uae-fbe-button-mobile').click(function () {
        if ($("#fbebrandnumber-mobile").val() === "") {
            alert("select number of brands");
            return false;
        }
            $("#freebrandnumber-mobile").get(0).selectedIndex = 0;
            $("#goldbrandnumber-mobile").get(0).selectedIndex = 0;
    });
    
    $('#overseas-free-button-mobile').keypress(function (e) {
        if (e.keyCode == 13) {
            return false;
        } else {
            alert("select number of brands");
        }
    });
    $('#overseas-free-button-mobile').click(function () {
        if ($("#freebrandnumberoverseas-mobile").val() === "") {
            alert("select number of brands");
            return false;
        }
            $("#freebrandnumberoverseas-mobile").get(0).selectedIndex = 0;
            $("#goldbrandnumberoverseas-mobile").get(0).selectedIndex = 0;
    });
    
    $('#overseas-gold-button-mobile').keypress(function (e) {
        if (e.keyCode == 13) {
            return false;
        } else {
            alert("select number of brands");
        }
    });
    $('#overseas-gold-button-mobile').click(function () {
        if ($("#goldbrandnumberoverseas-mobile").val() === "") {
            alert("select number of brands");
            return false;
        }
            $("#freebrandnumberoverseas-mobile").get(0).selectedIndex = 0;
            $("#goldbrandnumberoverseas-mobile").get(0).selectedIndex = 0;
    });
    
    $('#overseas-fbe-button-mobile').keypress(function (e) {
        if (e.keyCode == 13) {
            return false;
        } else {
            alert("select number of brands");
        }
    });
    $('#overseas-fbe-button-mobile').click(function () {
        if ($("#fbebrandnumber-mobile").val() === "") {
            alert("Please select number of brands");
            return false;
        }
            $("#freebrandnumberoverseas-mobile").get(0).selectedIndex = 0;
            $("#goldbrandnumberoverseas-mobile").get(0).selectedIndex = 0;
    });


});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
/*
var path = "https://free.currencyconverterapi.com/api/v6/convert?q=USD_AED&compact=ultra&apiKey=d2621398a3cec8d75a47";
$(document).ready(function () {
        $.getJSON(path, function (data) {
            $.each(data, function (i, field) {
                //$("#convertprice").append(JSON.stringify(field)); // parse the object according to your need.
                
                 document.cookie = "fbdata = " +JSON.stringify(field);
                 console.log(`document.cookie`);
            });
        });
});
*/
$(document).ready(function() {
    if ($(window).width() < 600) {
     $('html').find('.desktopVersion').remove(); //use class or tag name or id
    }
    if ($(window).width() > 600) {
     $('html').find('.mobileVersion').remove(); //use class or tag name or id
    }
})