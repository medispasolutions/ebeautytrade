<?php

namespace core;

abstract class AbstractCollection implements CollectionInterface
{

    protected $count;
    protected $elements = [];

    /** @var \core\ServiceLocator */
    public $serviceLocator;

    public function __construct($elements)
    {
        if ($elements === null) {
            $elements = [];
        } elseif (!is_array($elements)) {
            $elements = [$elements];
        }
        $this->elements = $elements;
        $this->count = count($this->elements);

        $this->serviceLocator = get_service_locator();
    }

    public function isEmpty()
    {
        return empty($this->elements);
    }

    public function first()
    {
        if ($this->isEmpty()) {
            return false;
        }
        reset($this->elements);
        return current($this->elements);
    }

    public function merge(CollectionInterface $anotherCollection, $overwriteKeys = true)
    {
        foreach ($anotherCollection as $id => $object) {
            if ($overwriteKeys) {
                $this->elements[$id] = $object;
            } elseif (isset($this->elements[$id])) {
                $this->elements[] = $object;
            }
        }
        $this->count = count($this->elements);

        return $this;
    }

    public function keys()
    {
        return array_keys($this->elements);
    }

    //==========================================================================
    // ArrayAccess interface

    public function offsetExists($offset) {
        return isset($this->elements[$offset]);
    }

    public function offsetGet($offset) {
        return $this->elements[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->elements[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        $this->elements[$offset] = null;
    }

    //==========================================================================
    // Countable interface

    public function count()
    {
        return $this->count;
    }
}
