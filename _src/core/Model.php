<?php

namespace core;

use Exception;

abstract class Model
{

    protected $id;
    protected $_data = [];
    protected $_exists = false;
    /** @var \core\ServiceLocator */
    public $serviceLocator;

    final public function __construct($id = null)
    {
        $this->serviceLocator = get_service_locator();

        if ($id) {
            $this->id = (int)$id;
            $this->read();
        }
        $this->serviceLocator->register($this, $this->id);
    }

    abstract public function schemeName();

    protected function idColumnName()
    {
        $onlyClassName = $this->schemeName();
        return "id_".strtolower($onlyClassName);
    }

    public function getId()
    {
        return $this->id;
    }

    public function read()
    {
        $this->_data = [];
        $this->db->where([$this->idColumnName() => $this->id]);
        $query = $this->db->get($this->schemeName());
        if ($query->num_rows() > 0) {
            $this->_exists = true;
            $this->_data = $query->row_array();
            $this->afterRead();
        }
    }

    protected function afterRead()
    {

    }

    public function validate($data)
    {
        return $data;
    }

    protected function beforeSave(& $data)
    {

    }

    protected function beforeUpdate(& $data)
    {

    }

    protected function beforeCreate(& $data)
    {

    }

    public function save($data)
    {
        if ( ($dataToSave = $this->validate($data)) === false) {
            return false;
        }

        if (!is_array($dataToSave)) {
            throw new Exception("Saving with no data - probably validation doesn't return it");
        }

        $this->beforeSave($dataToSave);

        if ($this->id) {
            $this->beforeUpdate($dataToSave);

            $this->db->where([$this->idColumnName() => $this->id]);
            $this->db->update($this->schemeName(), $dataToSave);

        } else {
            $this->beforeCreate($dataToSave);

            $this->db->insert($this->schemeName(), $dataToSave);
            $this->id = (int)$this->db->insert_id();
            $this->_data[$this->idColumnName()] = $this->id;

            //unregister blank model in service locator
            $this->serviceLocator->unregister(get_class($this));
            //register this model
            $this->serviceLocator->register($this, $this->id);
        }

        $this->cleanRelations();
        //read again to have proper data as read() runs afterRead()
        $this->read();

        return true;
    }

    protected function cleanRelations()
    {

    }

    public function toArray()
    {
        return $this->_data;
    }

    /**
     * @return bool Whether the model exists in db
     */
    public function exists()
    {
        return $this->_exists;
    }

    /**
     * Access to CI internals
     */
    public function __get($key)
    {
        $CI =& get_instance();
        return $CI->$key;
    }

    /**
     * @param string|int|[] $params
     * @return static
     */
    public function findFirst($params)
    {
        //if passed variable is not an array then assume it's id
        if (!is_array($params)) {
            return $this->serviceLocator->getObject($this, $params);
        }

        $this->db->where($params);
        $queryResult = $this->db->get($this->schemeName())->row_array();

        $foundId = $queryResult[$this->idColumnName()];

        return $this->serviceLocator->getObject($this, $foundId);
    }

    /**
     * @param string|int|[] $params
     * @return \core\ModelCollection
     */
    public function find($params, $limit = null, $offset = 0, $orderBy = null, $orderHow = 'asc')
    {
        //if passed variable is not an array then assume it's id
        if (!is_array($params)) {
            $ids = [$params];
        } else {
            $this->db->select($this->idColumnName());
            $this->db->where($params);
            if ($limit && $offset) {
                $this->db->limit($limit, $offset);
            }
            if ($orderBy && $orderHow) {
                $this->db->order_by($orderBy, $orderHow);
            }
            $queryResults = $this->db->get($this->schemeName())->result_array();
            $ids = [];
            foreach ($queryResults as $result) {
                $ids[] = $result[$this->idColumnName()];
            }
        }

        return new ModelCollection($ids, $this);
    }

    /**
     * @param $fileArrayField $_FILES field name
     * @param $dbField Name of the file in db (to unlink old one)
     * @param $path Where the files are stored
     * @return bool|string False on error, filename on success
     */
    protected function uploadImage($fileArrayField, $dbField, $path)
    {
        if (!empty($_FILES[$fileArrayField]["name"])) {
            if ($this->_data[$dbField] && file_exists(ltrim($this->_data[$dbField], '/'))) {
                unlink(ltrim($this->_data[$dbField], '/'));
            }

            list($result, $uploadedFileName) = $this->serviceLocator->imageUploader()->upload($fileArrayField, $path);

            if (!$result) {
                return false;
            }
            return $uploadedFileName;
        }

        return false;
    }

    /**
     * @param $fileArrayField $_FILES field name
     * @param $dbField Name of the file in db (to unlink old one)
     * @param $path Where the files are stored
     * @return bool|string False on error, filename on success
     */
    protected function uploadFile($fileArrayField, $dbField, $path)
    {
        if (!empty($_FILES[$fileArrayField]["name"])) {
            if ($this->_data[$dbField] && file_exists(ltrim($this->_data[$dbField], '/'))) {
                unlink(ltrim($this->_data[$dbField], '/'));
            }

            list($result, $uploadedFileName) = $this->serviceLocator->fileUploader()->upload($fileArrayField, $path);

            if (!$result) {
                return false;
            }
            return $uploadedFileName;
        }

        return false;
    }
}