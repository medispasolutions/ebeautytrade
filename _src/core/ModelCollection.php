<?php

namespace core;

use Exception;
use Iterator;

class ModelCollection extends AbstractCollection implements Iterator
{

    protected $class;
    protected $position;

    public function __construct($idsArray, $class)
    {
        if ($idsArray === null) {
            $idsArray = [];
        } elseif (!is_array($idsArray)) {
            $idsArray = [$idsArray];
        }

        $elements = [];
        foreach ($idsArray as $id) {
            $elements[$id] = $id;
        }
        parent::__construct($elements);

        $this->position = key($elements);

        $this->class = is_object($class) ? get_class($class) : $class;
    }

    protected function getElement($offset)
    {
        if (!isset($this->elements[$offset])) {
            throw new Exception("No element with offset {$offset} in collection");
        }

        return $this->serviceLocator->getObject($this->class, $this->elements[$offset]);
    }

    public function first()
    {
        if ($this->isEmpty()) {
            return false;
        }
        $this->rewind();
        return $this->current();
    }

    //==========================================================================
    // ArrayAccess interface

    public function offsetGet($offset)
    {
        return $this->getElement($offset);
    }

    public function offsetSet($offset, $value)
    {
        throw new Exception("Unable to change \core\ModelCollection");
    }

    public function offsetUnset($offset)
    {
        throw new Exception("Unable to change \core\ModelCollection");
    }

    //==========================================================================
    // IteratorAggregate interface

    public function getIterator()
    {
        return $this;
    }

    public function rewind()
    {
        reset($this->elements);
        $this->position = key($this->elements);
    }

    public function current()
    {
        return $this->getElement($this->position);
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        $nextKey = next($this->elements);
        if ($nextKey == $this->position) {
            $this->position = -1;
        } else {
            $this->position = $nextKey;
        }
    }

    public function valid()
    {
        return isset($this->elements[$this->position]);
    }

}
