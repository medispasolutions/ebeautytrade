<?php

namespace core;

use IteratorAggregate;
use ArrayIterator;

class Collection extends AbstractCollection implements IteratorAggregate
{

    //==========================================================================
    // IteratorAggregate interface

    public function getIterator()
    {
        return new ArrayIterator($this->elements);
    }

}
