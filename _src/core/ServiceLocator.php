<?php

namespace core;

class ServiceLocator
{

    const SINGLETON = "singleton";

    protected $loadedServices = [];

    /**
     * @param null|int $id
     * @return \spamiles\users\User
     */
    public function user($id = null)
    {
        return $this->getObject('\spamiles\users\User', $id);
    }

    /**
     * @param null|int $id
     * @return \spamiles\brands\Brand
     */
    public function brand($id = null)
    {
        return $this->getObject('\spamiles\brands\Brand', $id);
    }

    /**
     * @param null|int $id
     * @return \spamiles\products\Product
     */
    public function product($id = null)
    {
        return $this->getObject('\spamiles\products\Product', $id);
    }

    /**
     * @param null|int $id
     * @return \spamiles\orders\Order
     */
    public function order($id = null)
    {
        return $this->getObject('\spamiles\orders\Order', $id);
    }

    /**
     * @return \spamiles\utils\uploaders\ImageUploader
     */
    public function imageUploader()
    {
        return $this->getObject('\spamiles\utils\uploaders\ImageUploader', self::SINGLETON);
    }

    /**
     * @return \spamiles\utils\uploaders\FileUploader
     */
    public function fileUploader()
    {
        return $this->getObject('\spamiles\utils\uploaders\FileUploader', self::SINGLETON);
    }

    /**
     * @return \spamiles\utils\captcha\GoogleRecaptcha
     */
    public function captcha()
    {
        return $this->getObject('\spamiles\utils\captcha\GoogleRecaptcha', self::SINGLETON);
    }

    /**
     * @return \spamiles\clicks\Service
     */
    public function clicksService()
    {
        return $this->getObject('\spamiles\clicks\Service', self::SINGLETON);
    }

    /**
     * @return \spamiles\users\buyers\Service
     */
    public function buyersService()
    {
        return $this->getObject('\spamiles\users\buyers\Service', self::SINGLETON);
    }

    /**
     * @return \spamiles\utils\dictionaries\Dictionary
     */
    public function dictionary()
    {
        return $this->getObject('\spamiles\utils\dictionaries\Dictionary', self::SINGLETON);
    }

    public function getObject($classnameOrObject, $id = null)
    {
        $serviceId  = $this->getServiceId($classnameOrObject, $id);
        if (!isset($this->loadedServices[$serviceId])) {
            if ($id == self::SINGLETON) {
                $object = new $classnameOrObject();
            } else {
                $object = new $classnameOrObject($id);
            }
            $this->register($object, $id);
        }

        return $this->loadedServices[$serviceId];
    }

    protected function getServiceId($classnameOrObject, $id)
    {
        if (is_object($classnameOrObject)) {
            $classnameOrObject = get_class($classnameOrObject);
        } else {
            $classnameOrObject = ltrim($classnameOrObject, '\\');
        }

        return "{$classnameOrObject}({$id})";
    }

    public function register($object, $id)
    {
        $serviceId  = $this->getServiceId($object, $id);
        if (!isset($this->loadedServices[$serviceId])) {
            $this->loadedServices[$serviceId] = $object;
        }

        return $this;
    }

    public function unregister($classnameOrObject, $id = null)
    {
        $serviceId  = $this->getServiceId($classnameOrObject, $id);
        if (isset($this->loadedServices[$serviceId])) {
            unset($this->loadedServices[$serviceId]);

        //if null passed try singleton also
        } elseif ($id === null) {
            $serviceId  = $this->getServiceId($classnameOrObject, self::SINGLETON);
            if (isset($this->loadedServices[$serviceId])) {
                unset($this->loadedServices[$serviceId]);
            }
        }

        return $this;
    }
}