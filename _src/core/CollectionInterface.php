<?php

namespace core;

use ArrayAccess;
use IteratorAggregate;
use Countable;

interface CollectionInterface extends ArrayAccess, Countable
{

    public function isEmpty(); //:boolean

    public function first(); //:mixed

    public function merge(CollectionInterface $anotherCollection, $overwriteKeys = true);

    public function keys();

    public function count();

}