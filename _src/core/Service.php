<?php

namespace core;

abstract class Service
{

    /** @var \core\ServiceLocator */
    protected $serviceLocator;

    public function __construct()
    {
        $this->serviceLocator = get_service_locator();
    }

    /**
     * Access to CI internals
     */
    public function __get($key)
    {
        $CI =& get_instance();
        return $CI->$key;
    }

}