<?php

namespace spamiles\products;

use core\Model;
use DateTime;

class Product extends Model
{

    protected $brand;
    protected $supplier;

    protected function cleanRelations()
    {
        $this->brand = null;
        $this->supplier = null;
    }

    public function schemeName()
    {
        return "products";
    }

    /**
     * @return \spamiles\brands\Brand
     */
    public function getBrand()
    {
        if (!$this->brand) {
            $this->brand = $this->serviceLocator->brand($this->_data['id_brands']);
        }

        return $this->brand;
    }

    /**
     * @return \spamiles\users\User
     */
    public function getSupplier()
    {
        if (!$this->supplier) {
            $this->supplier = $this->serviceLocator->user($this->_data['id_user']);
        }

        return $this->supplier;
    }

    /**
     * @return self
     */
    public function expireDiscount($force = false)
    {
        if (
            //products has discount
            $this->_data['discount'] > 0
            && (
                //discount has an expiration date
                $this->_data['discount_expiration']
                && (
                    //we force it
                    $force
                    //or expiration date has passed
                    || $this->_data['discount_expiration'] < date("Y-m-d")
                )
            )
        ) {
            $change = [
                'price' => $this->_data['list_price'],
                'discount_active' => 0,
            ];
            $this->save($change);
        }

        return $this;
    }

    /**
     * @return self
     */
    public function applyDiscount($discount, $discount_expiration)
    {
        $discount = (int)$discount;
        if ($discount < 0 || $discount >= 100) {
            return;
        }
        if ($discount_expiration !== null) {
            $discount_expiration = new DateTime($discount_expiration);
            if ($discount_expiration->format('Y-m-d') < date('Y-m-d')) {
                return;
            }
        }

        $change = [
            'price' => round($this->_data['list_price']*(1-$discount/100), 2),
            'discount' => $discount,
            'discount_expiration' => $discount_expiration && $discount > 0 ? $discount_expiration->format("Y-m-d") : null,
            'discount_active' => $discount > 0,
        ];
        $this->save($change);
        return $this;
    }
}