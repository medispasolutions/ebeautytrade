<?php

namespace spamiles\orders;

interface OrderInterface
{

    //STATUSES, max 100 chars
    //every new order
    const STATUS_UNREAD = "unread";
    //order that was read and is being processed
    const STATUS_PENDING = "pending";
    //order that was prepared or shipped
    const STATUS_PREPARED = "prepared";
    //order that was completed
    const STATUS_COMPLETED = "completed";
    //order that was cancelled
    const STATUS_CANCELED = "canceled";
    //legacy
    const STATUS_PAID = "paid";

    //PAYMENT STATUSES, max 30 chars
    const PAYMENT_STATUS_PAID = "paid";
    const PAYMENT_STATUS_UNPAID = "unpaid";

    //PAYMENT METHODS, max 30 chars
    const PAYMENT_METHOD_CHEQUE_ON_DELIVERY = "cheque-on-delivery";
    const PAYMENT_METHOD_CASH_ON_DELIVERY = "cash-on-delivery";
    const PAYMENT_METHOD_LATER = "later-payment";
    const PAYMENT_METHOD_ONLINE = "online";

    public function listStatuses();
    public function listPaymentStatuses();
    public function listPaymentMethods();
}