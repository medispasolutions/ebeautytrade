<?php

namespace spamiles\users\buyers;

use core\Service as BaseService;

class Service extends BaseService
{

    protected function isUserACustomer($user_id)
    {
        //assume not logged users aren't buyers
        if (!$user_id) {
            return false;
        }
        $user = $this->serviceLocator->user($user_id);
        return $user->isCustomer();
    }

    protected function addBuyer($user_id, $type, $supplier_id = null, $brand_id = null, $product_id = null)
    {
        if (!$this->isUserACustomer($user_id)) {
            return;
        }

        //determine supplier id
        if ($supplier_id) {
            //do nothing
        } elseif ($brand_id) {
            $brand = $this->serviceLocator->brand($brand_id);
            if ($brand->exists()) {
                $supplier_id = $brand->getSupplier()->getId();
            }
        } elseif ($product_id) {
            $product = $this->serviceLocator->product($product_id);
            if ($product->exists()) {
                $supplier_id = $product->getSupplier()->getId();
            }
        }

        if (!$supplier_id) {
            return;
        }

        $buyer = $this->serviceLocator->getObject('spamiles\users\buyers\Buyer');

        //if we already have an entry for this user and supplier, then reuse it
        $modelForWork = $buyer->findFirst([
            'id_user' => $user_id,
            'id_supplier' => $supplier_id,
        ]);
        $modelForWork->save([
            'last_action' => $type,
            'id_user' => $user_id,
            'id_supplier' => $supplier_id,
        ]);
    }

    //some of the products have id_user set to 0!
    public function addBuyerForProductView($user_id, $brand_id)
    {
        return $this->addBuyer($user_id, Buyer::ACTION_TYPE_PRODUCT_VIEW, null, $brand_id);
    }

    public function addBuyerForBrandView($user_id, $brand_id)
    {
        return $this->addBuyer($user_id, Buyer::ACTION_TYPE_BRAND_VIEW, null, $brand_id, null);
    }

    public function addBuyerForEnquiry($user_id, $supplier_id = null, $brand_id = null, $product_id = null)
    {
        return $this->addBuyer($user_id, Buyer::ACTION_TYPE_ENQUIRY, $supplier_id, $brand_id, $product_id);
    }

    public function addBuyerForCalling($user_id, $supplier_id = null, $brand_id = null, $product_id = null)
    {
        return $this->addBuyer($user_id, Buyer::ACTION_TYPE_CALL_SUPPLIER, $supplier_id, $brand_id, $product_id);
    }

    public function addBuyerForOrder($user_id, $supplier_id = null, $brand_id = null, $product_id = null)
    {
        return $this->addBuyer($user_id, Buyer::ACTION_TYPE_ORDER, $supplier_id, $brand_id, $product_id);
    }

    /**
     * @param string|int|[] $params
     * @return \core\ModelCollection
     */
    public function find($params, $limit = null, $offset = null, $orderBy = null, $orderHow = 'asc')
    {
        $buyer = $this->serviceLocator->getObject('spamiles\users\buyers\Buyer');
        return $buyer->find($params, $limit, $offset, $orderBy, $orderHow);
    }

    /**
     * @param string|int|[] $params
     * @return \spamiles\users\buyers\Buyer
     */
    public function findFirst($params)
    {
        $buyer = $this->serviceLocator->getObject('spamiles\users\buyers\Buyer');
        return $buyer->findFirst($params);
    }

    public function getCountedBuyersForSupplier($supplier_id)
    {
        $buyer = $this->serviceLocator->getObject('spamiles\users\buyers\Buyer');

        $this->db->where('id_supplier', $supplier_id);
        return $this->db->get($buyer->schemeName())->num_rows();
    }
}