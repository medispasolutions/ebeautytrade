<?php

namespace spamiles\users\buyers;

use core\Model;

class Buyer extends Model
{

    //maxlength 20
    const ACTION_TYPE_ORDER = "order";
    const ACTION_TYPE_ENQUIRY = "enquiry";
    const ACTION_TYPE_CALL_SUPPLIER = "call-supplier";
    const ACTION_TYPE_PRODUCT_VIEW = "product-view";
    const ACTION_TYPE_BRAND_VIEW = "brand-view";

    protected $user;

    protected function cleanRelations()
    {
        $this->user = null;
    }

    public function schemeName()
    {
        return "buyers";
    }

    protected function idColumnName()
    {
        return "id";
    }

    protected function beforeCreate(& $data)
    {
        $data['created_date'] = date("Y-m-d H:i:s");
    }

    protected function beforeSave(& $data)
    {
        $data['updated_date'] = date("Y-m-d H:i:s");
    }

    /**
     * @return \spamiles\users\User
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = $this->serviceLocator->user($this->_data['id_user']);
        }

        return $this->user;
    }
}