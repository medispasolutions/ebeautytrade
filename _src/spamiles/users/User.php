<?php

namespace spamiles\users;

use core\Model;

class User extends Model
{

    public function schemeName()
    {
        return "user";
    }

    protected function afterRead()
    {
        if ($this->_data['logo']) {
            $tmp = $this->_data['logo'];
            $this->_data['logo'] = '/uploads/user/'.$this->_data['logo'];
            $logoThumb = 'uploads/user/112x122/';
            if (file_exists($logoThumb.$tmp)) {
                $this->_data['logo_thumb'] = '/'.$logoThumb.$tmp;
            }
        }

        if ($this->_data['trading_licence']) {
            $this->_data['trading_licence_src'] = '/uploads/user/'.$this->_data['trading_licence'];
        }

        if ($this->_data['photo']) {
            $tmp = $this->_data['photo'];
            $this->_data['photo'] = '/uploads/user/avatars/'.$this->_data['photo'];
            $photoThumb = 'uploads/user/avatars/200x200/';
            if (file_exists($photoThumb.$tmp)) {
                $this->_data['photo_thumb'] = '/'.$photoThumb.$tmp;
            }
        }

        $this->_data['payment_methods'] = json_decode($this->_data['payment_methods'], true);
        if (!is_array($this->_data['payment_methods'])) {
            $this->_data['payment_methods'] = [];
        }
    }

    protected function beforeSave(& $data)
    {
        //upload logo
        if ($data['logo']) {
            $result = $this->uploadImage('logo', 'logo', '/uploads/user/');
            if ($result) {
                $data['logo'] = $result;
                //delete old thumb
                if ($this->_data['logo_thumb'] && file_exists(ltrim($this->_data['logo_thumb'], '/'))) {
                    unlink(ltrim($this->_data['logo_thumb'], '/'));
                }
                $this->serviceLocator->imageUploader()->createThumb($result, '/uploads/user/', 112, 122);
            } else {
                unset($data['logo']);
            }
        }

        //upload trading licence
        if ($data['trading_licence']) {
            $data['trading_licence'] = $this->uploadFile('trading_licence', 'trading_licence', '/uploads/user/');
            if (!$data['trading_licence']) {
                unset($data['trading_licence']);
            }
        }

        //upload photo
        if ($data['photo']) {
            $result = $this->uploadImage('photo', 'photo', '/uploads/user/avatars/');
            if ($result) {
                $data['photo'] = $result;
                //delete old thumb
                if ($this->_data['photo_thumb'] && file_exists(ltrim($this->_data['photo_thumb'], '/'))) {
                    unlink(ltrim($this->_data['photo_thumb'], '/'));
                }
                $this->serviceLocator->imageUploader()->createThumb($result, '/uploads/user/avatars/', 200, 200);
            } else {
                unset($data['photo']);
            }
        }
    }

    public function getDeliveryTime($readable = false)
    {
        if ($readable) {
            switch ($this->_data['delivery_time']) {
                case '1-3-days':
                    return '1-3 days';
                case '3-5-weeks':
                    return '3-5 weeks';
                case 'other':
                    if ($this->_data['delivery_time_period'] && $this->_data['delivery_time_period_type']) {
                        return $this->_data['delivery_time_period'].' '.$this->_data['delivery_time_period_type'];
                    }
                    return null;
                default:
                    return null;
            }
        }

        return $this->_data['delivery_time'];
    }

    public function getDeliveryFee($readable = false)
    {
        if ($readable) {
            switch ($this->_data['delivery_fee']) {
                case 'free':
                    return 'free';
                case 'courier-price':
                    return 'as per the courier rate';
                case 'free-on-bigger-orders':
                    if ($this->_data['delivery_fee_price']) {
                        return 'free for orders bigger than ' . $this->_data['delivery_fee_price'] . ' AED';
                    }
                    return null;
                case 'fixed-fee':
                    if ($this->_data['delivery_fee_price']) {
                        return $this->_data['delivery_fee_price'] . ' AED';
                    }
                    return null;
                default:
                    return null;
            }
        }

        return $this->_data['delivery_fee'];
    }

    public function getCreditTerms($readable = false)
    {
        if ($readable) {
            switch ($this->_data['credit_terms']) {
                case 'consult-with-supplier':
                    return "Consult with Supplier";
                case 'do-not-publish':
                    return "Do not publish";
                default:
                    return null;
            }
        }

        return $this->_data['credit_terms'];
    }

    public function getPaymentMethods($readable = false)
    {
        if ($readable) {
            $tmp = [];
            foreach ($this->_data['payment_methods'] as $method) {
                switch ($method) {
                    case "cheque-on-delivery":
                        $tmp[] = "cheque on delivery";
                        break;
                    case "cash-on-delivery":
                        $tmp[] = "cash on delivery";
                        break;
                    case "later-payment":
                        $tmp[] = "later payment";
                        break;
                    case "credit-card-online":
                        $tmp[] = "credit card online";
                        break;
                }
            }
            return implode(', ', $tmp);
        }

        return $this->_data['payment_methods'];
    }

    public function validate($data)
    {
        $data = trimAll($data);

        //we have to have an email
        if (!$data['email'] && !$this->_data['email']) {
            return false;
        }
        //we have to have a password
        if (!$data['password'] && !$this->_data['password']) {
            return false;
        }

        //delivery details
        if (isset($data['delivery_time'])) {
            switch ($data['delivery_time']) {
                case '1-3-days':
                case '3-5-weeks':
                    $data['delivery_time_period'] = $data['delivery_time_period_type'] = null;
                    break;
                case 'other':
                    if ($data['delivery_time_period_type'] != 'days' && $data['delivery_time_period_type'] != 'weeks') {
                        $data['delivery_time_period_type'] = 'days';
                    }
                    break;
                default:
                    $data['delivery_time'] = $data['delivery_time_period'] = $data['delivery_time_period_type'] = null;
            }
        }

        //delivery fee
        if (isset($data['delivery_fee'])) {
            switch ($data['delivery_fee']) {
                case 'free':
                case 'courier-price':
                    $data['delivery_fee_price'] = null;
                    break;
                case 'free-on-bigger-orders':
                case 'fixed-fee':

                    break;
                default:
                    $data['delivery_fee'] = $data['delivery_fee_price'] = null;
            }
        }

        //credit terms
        if (isset($data['credit_terms'])) {
            switch ($data['credit_terms']) {
                case 'consult-with-supplier':
                case 'do-not-publish':
                    break;
                default:
                    $data['credit_terms'] = null;
            }
        }

        //payment methods
        if (isset($data['payment_methods'])) {

            if (!is_array($data['payment_methods'])) {
                $data['payment_methods'] = [];
            }

            foreach ($data['payment_methods'] as & $method) {
                switch ($method) {
                    case "cheque-on-delivery":
                    case "cash-on-delivery":
                    case "later-payment":
                    case "credit-card-online":
                        break;
                    default:
                        unset($method);
                }
            }
            $data['payment_methods'] = json_encode($data['payment_methods']);
        }

        //password
        if (isset($data['password']) && $data['password']) {
            $data['password'] = $this->encodePassword($data['password']);
        }

        if (isset($data['first_name']) && isset($data['last_name'])) {
            $data['name'] = $data['first_name'].' '.$data['last_name'];
        }

        return $data;
    }

    public function verifyPassword($password)
    {
        return $this->_data['password'] == $this->encodePassword($password);
    }

    protected function encodePassword($password)
    {
        return md5($password);
    }

    /**
     * Returns information whether the user is a customer (spa or saloon)
     * @return bool
     */
    public function isCustomer()
    {
        //consumers, spa/salon
        return $this->_data['id_roles'] == 2 || $this->_data['id_roles'] == 4;
    }

}