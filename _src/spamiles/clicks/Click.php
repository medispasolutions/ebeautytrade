<?php

namespace spamiles\clicks;

use core\Model;

class Click extends Model
{

    //type maxlength 20
    const TYPE_CALL_SUPPLIER = "call-supplier";
    const TYPE_BRAND = "brand";
    const TYPE_PRODUCT = "product";

    public function schemeName()
    {
        return "clicks";
    }

    protected function idColumnName()
    {
        return "id";
    }

    protected function beforeSave(& $data)
    {
        $data['created_date'] = date("Y-m-d H:i:s");
    }

}