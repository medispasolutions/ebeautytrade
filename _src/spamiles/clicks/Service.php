<?php

namespace spamiles\clicks;

use core\Service as BaseService;

class Service extends BaseService
{

    protected function isUserACustomer($user_id)
    {
        //assume not logged users are customers
        if (!$user_id) {
            return true;
        }
        $user = $this->serviceLocator->user($user_id);
        return $user->isCustomer();
    }

    public function addClickOnCallSupplier($user_id, $supplier_id = null, $brand_id = null, $product_id = null)
    {
        if (!$this->isUserACustomer($user_id)) {
            return;
        }

        //check brand existence and ownership
        if ($brand_id) {
            $brand = $this->serviceLocator->brand($brand_id);
            if (!$brand->exists()) {
                $brand_id = null;
            } elseif (!$supplier_id) {
                $supplier_id = $brand->getSupplier()->getId();
            } elseif ($brand->getSupplier()->getId() != $supplier_id) {
                $brand_id = null;
            }
        }

        //check product existence and ownership
        if ($product_id) {
            $product = $this->serviceLocator->product($product_id);
            if (!$product->exists()) {
                $product_id = null;
            } elseif (!$supplier_id) {
                $supplier_id = $product->getSupplier()->getId();
            } elseif ($product->getSupplier()->getId() != $supplier_id) {
                $product_id = null;
            } elseif (!$brand_id) {
                $brand_id = $product->getBrand()->getId();
            }
        }

        if (!$supplier_id) {
            return;
        }

        $click = new Click();
        $click->save([
            'type' => Click::TYPE_CALL_SUPPLIER,
            'id_user' => $user_id ? $user_id : null,
            'id_supplier' => $supplier_id,
            'id_brand' => $brand_id ? $brand_id : null,
            'id_product' => $product_id ? $product_id : null
        ]);
    }

    public function addClickOnBrand($user_id, $brand_id)
    {
        if (!$this->isUserACustomer($user_id)) {
            return;
        }

        //get brand details to get supplier_id
        $brand = $this->serviceLocator->brand($brand_id);
        if (!$brand->exists()) {
            return;
        }

        $click = new Click();
        $click->save([
            'type' => Click::TYPE_BRAND,
            'id_user' => $user_id ? $user_id : null,
            'id_supplier' => $brand->getSupplier()->getId(),
            'id_brand' => $brand->getId()
        ]);
    }

    //some of the products have id_user set to 0!
    public function addClickOnProduct($user_id, $brand_id, $product_id)
    {
        if (!$this->isUserACustomer($user_id)) {
            return;
        }

        //get brand details to get supplier_id
        $brand = $this->serviceLocator->brand($brand_id);
        if (!$brand->exists()) {
            return;
        }

        //get brand details to get supplier_id
        $product = $this->serviceLocator->product($product_id);
        if (!$product->exists()) {
            return;
        }

        $click = new Click();
        $click->save([
            'type' => Click::TYPE_PRODUCT,
            'id_user' => $user_id ? $user_id : null,
            'id_supplier' => $brand->getSupplier()->getId(),
            'id_brand' => $brand->getId(),
            'id_product' => $product->getId()
        ]);
    }

    public function getCountedClicksForSupplier($supplier_id)
    {
        $click = new Click();
        $ret = [
            'clicks_on_brands' => 0,
            'clicks_on_products' => 0,
            'clicks_on_call' => 0,
        ];

        $this->db->where('id_supplier', $supplier_id);
        $this->db->where('type', $click::TYPE_CALL_SUPPLIER);
        $ret['clicks_on_call'] = $this->db->get($click->schemeName())->num_rows();

        $this->db->where('id_supplier', $supplier_id);
        $this->db->where('type', $click::TYPE_PRODUCT);
        $ret['clicks_on_products'] = $this->db->get($click->schemeName())->num_rows();

        $this->db->where('id_supplier', $supplier_id);
        $this->db->where('type', $click::TYPE_BRAND);
        $ret['clicks_on_brands'] = $this->db->get($click->schemeName())->num_rows();

        return $ret;
    }
}