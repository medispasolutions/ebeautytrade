<?php

namespace spamiles\financial;

use core\Model;

class Financial extends Model implements FinancialInterface
{

    public function listStatuses()
    {
        return [
            self::STATUS_UNREAD,
            self::STATUS_PENDING,
            self::STATUS_PREPARED,
            self::STATUS_COMPLETED,
            self::STATUS_CANCELED,
        ];
    }

    public function listPaymentStatuses()
    {
        return [
            self::PAYMENT_STATUS_UNPAID,
            self::PAYMENT_STATUS_PAID,
        ];
    }

    public function listPaymentMethods()
    {
        return [
            self::PAYMENT_METHOD_CHEQUE_ON_DELIVERY,
            self::PAYMENT_METHOD_CASH_ON_DELIVERY,
            self::PAYMENT_METHOD_LATER,
        ];
    }

    public function schemeName()
    {
        return "financial";
    }

    protected function idColumnName()
    {
        return "id_orders";
    }

    public function validate($data)
    {
        $currentData = $this->toArray();

        //validate status
        if (isset($data['status'])) {
            if (!in_array($data['status'], $this->listStatuses())) {
                unset($data['status']);
            } else {
                //nothings
            }
        }

        //validate payment method
        if (isset($data['payment_method'])) {
            if (!in_array($data['payment_method'], $this->listPaymentMethods())) {
                unset($data['payment_method']);
            } else {
                //nothings
            }
        }

        //validate payment status
        if (isset($data['payment_status'])) {
            if (!in_array($data['payment_status'], $this->listPaymentStatuses())) {
                unset($data['payment_method']);
            } elseif($data['payment_status'] != $currentData['payment_status']) {
                if ($data['payment_status'] == self::PAYMENT_STATUS_PAID) {
                    $data['payment_date'] = date('Y-m-d h:i:s');
                } else {
                    $data['payment_date'] = null;
                }
            }
        }

        //validate discount
        $discount = $currentData['discount'];
        if (isset($data['discount'])) {
            $data['discount'] = round((float)$data['discount'], 2);
            if ($data['discount'] < 0) {
                $data['discount'] = 0;
            }
            $discount = $data['discount'];
        }

        //validate shipping charge
        $shipping = $currentData['shipping_charge_currency'];
        if (isset($data['shipping_charge_currency'])) {
            $data['shipping_charge_currency'] = round((float)$data['shipping_charge_currency'], 2);
            if ($data['shipping_charge_currency'] < 0) {
                $data['shipping_charge_currency'] = 0;
            }
            $shipping = $data['shipping_charge_currency'];
        }

        $amount = $currentData['total_price_currency'] + $shipping;

        if ($amount > $discount) {
            $amount = $amount - $discount;
        } else {
            unset($data['discount']);
        }

        $data['amount'] = changeCurrency($amount, "", $this->config->item('default_currency'), $currentData['currency']);
        $data['amount_currency'] = $amount;

        return $data;
    }

}