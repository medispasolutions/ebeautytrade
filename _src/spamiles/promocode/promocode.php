<?php

namespace spamiles\promocode;

use core\Model;

class Brand extends Model
{

    protected $supplier;

    public function schemeName()
    {
        return "promocode";
    }

    /**
     * @return \spamiles\users\User
     */
    public function getSupplier()
    {
        if (!$this->supplier) {
            $this->supplier = $this->serviceLocator->user($this->_data['id_user']);
        }

        return $this->supplier;
    }

    protected function cleanRelations()
    {
        $this->supplier = null;
    }
}