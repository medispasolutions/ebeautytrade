<?php

namespace spamiles\utils\uploaders;

class FileUploader
{

    public function upload($fileArrayField, $path, $maxSize = '10000')
    {
        $path = rtrim(ltrim($path, '/'), '/');

        $config = array();
        $config['allowed_types'] = 'jpg|png|gif|jpeg|txt|pdf|docx|doc|csv|ppt|ppt|pptx';
        $config['upload_path'] = $path;
        $config['max_size'] = (int)$maxSize;

        $CI = & get_instance();
        $CI->load->library('upload');
        $CI->upload->initialize($config);
        $this->fullmkdir($path);
        if (!$CI->upload->do_upload($fileArrayField, true)) {
            return [false, $CI->upload->display_errors()];
        } else {
            $path = $CI->upload->data();
            return [true, $path['file_name']];
        }
    }

    protected function fullmkdir($path)
    {
        $path = explode("/", $path);
        $currentPath = "";

        foreach ($path as $part) {
            if (!trim($part)) {
                continue;
            }
            $currentPath .= $part.'/';
            if (!is_dir($currentPath)) {
                mkdir($currentPath);
            }
        }
    }

}