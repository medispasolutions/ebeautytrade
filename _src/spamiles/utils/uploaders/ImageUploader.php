<?php

namespace spamiles\utils\uploaders;

class ImageUploader
{

    public function upload($fileArrayField, $path, $maxSize = '1024')
    {
        $path = rtrim(ltrim($path, '/'), '/');

        $config = array();
        $config['allowed_types'] = 'jpg|png|gif|jpeg';
        $config['upload_path'] = $path;
        $config['max_size'] = (int)$maxSize;

        $CI = & get_instance();
        $CI->load->library('upload');
        $CI->upload->initialize($config);
        $this->fullmkdir($path);
        if (!$CI->upload->do_upload($fileArrayField, true)) {
            return [false, $CI->upload->display_errors()];
        } else {
            $path = $CI->upload->data();
            return [true, $path['file_name']];
        }
    }

    public function createThumb($existingFileName, $path, $width, $height)
    {
        $path = rtrim(ltrim($path, '/'), '/');

        $config = array(
            'path' => $path."/{$width}x{$height}/",
            'source_image' => $existingFileName,
            'new_image' => "{$width}x{$height}/" . $existingFileName,
            'width' => $width,
            'height' => $height,
            'maintain_ratio' => FALSE
        );

        $CI = & get_instance();
        $CI->load->library('image_lib');
        $this->fullmkdir($config['path']);
        $CI->image_lib->thumb($config, FCPATH . '' . $path . '/');
    }

    protected function fullmkdir($path)
    {
        $path = explode("/", $path);
        $currentPath = "";

        foreach ($path as $part) {
            if (!trim($part)) {
                continue;
            }
            $currentPath .= $part.'/';
            if (!is_dir($currentPath)) {
                mkdir($currentPath);
            }
        }
    }
}