<?php

namespace spamiles\utils\paginators;

class TwigPaginator
{

    public $page;
    public $per_page;
    public $offset;
    public $total_elements = 0;

    public function __construct()
    {
        $CI = & get_instance();

        if ($CI->input->get('page')) {
            $this->page = (int)$CI->input->get('page');
        } elseif ($CI->input->post('page')) {
            $this->page = (int)$CI->input->post('page');
        }

        if ($this->page <= 0) {
            $this->page = 1;
        }

        if ($CI->input->get('per_page')) {
            $this->per_page = (int)$CI->input->get('per_page');
        } elseif ($CI->input->post('per_page')) {
            $this->per_page = (int)$CI->input->post('per_page');
        }

        if ($this->per_page <= 0) {
            $this->per_page = 25;
        }

        $this->offset = ($this->page-1)*$this->per_page;
    }

    public function setTotalElements($count)
    {
        $this->total_elements = $count;
    }

    public function setViewData(& $viewData, $url)
    {
        $viewData['mergeConfig']['pagination'] = [
            'PREV_HREF' => $this->page > 1 ? $url."?page=".($this->page-1)."&per_page=".$this->per_page : '',
            'NEXT_HREF' => $this->page*$this->per_page < $this->total_elements ? $url."?page=".($this->page+1)."&per_page=".$this->per_page : '',
            'RESULTS_START' => $this->total_elements > 0 ? (($this->page-1)*$this->per_page)+1 : 0,
            'RESULTS_END' => $this->total_elements > 0 ? min($this->page*$this->per_page, $this->total_elements) : 0,
            'RESULTS_TOTAL' => $this->total_elements,
            'options' => [
                [
                    'TEXT' => 25,
                    'VALUE' => 25,
                    'IS_SELECTED' => true,
                ],
                [
                    'TEXT' => 50,
                    'VALUE' => 50,
                    'IS_SELECTED' => false,
                ],
                [
                    'TEXT' => 100,
                    'VALUE' => 100,
                    'IS_SELECTED' => false,
                ],
            ],
        ];

        return;
    }

}