<?php

namespace spamiles\utils\dictionaries;

use spamiles\orders\OrderInterface;

class Dictionary
{

    protected $dictionary;

    public function __construct()
    {
        $this->dictionary = [
            'orders' => [
                OrderInterface::STATUS_UNREAD => "Unread",
                OrderInterface::STATUS_PENDING => "Pending",
                OrderInterface::STATUS_PREPARED => "Prepared",
                OrderInterface::STATUS_COMPLETED => "Completed",
                OrderInterface::STATUS_CANCELED => "Canceled",
                OrderInterface::PAYMENT_STATUS_PAID => "Paid",
                OrderInterface::PAYMENT_STATUS_UNPAID => "Unpaid",
                OrderInterface::PAYMENT_METHOD_CHEQUE_ON_DELIVERY => "Cheque on delivery",
                OrderInterface::PAYMENT_METHOD_CASH_ON_DELIVERY => "Cash on delivery",
                OrderInterface::PAYMENT_METHOD_LATER => "Later payment",
                OrderInterface::PAYMENT_METHOD_ONLINE => "Online payment",
                //some rows in db have 0 set as payment method
                0 => "",
            ],
        ];
    }

    public function orders($term)
    {
        return $this->response($this->dictionary['orders'], $term);
    }

    protected function response($array, $term)
    {
        if (!isset($array[$term])) {
            return $term;
        }
        return $array[$term];
    }
}