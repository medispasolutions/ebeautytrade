<?php

namespace spamiles\utils\captcha;

class GoogleRecaptcha
{

    public function validate($code)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($ch, CURLOPT_POST, 1);
        $params = array(
            'secret' => _getenv('GOOGLE_RECAPTCHA_SECRET_KEY'),
            'response' => $code,
            'remoteip' => $_SERVER['REMOTE_ADDR'],
        );

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = json_decode(curl_exec($ch), true);

        curl_close ($ch);
        if ($response['success'] !== true) {
            return false;
        }

        return true;
    }

}