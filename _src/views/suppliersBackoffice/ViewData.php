<?php

namespace views\suppliersBackoffice;

/**
 * @property string $context
 * @property array $error
 * @property array $success
 * @property string $title
 */
class ViewData
{

    protected $context;
    protected $success = [];
    protected $error = [];
    protected $title;

    protected $otherData = [];

    public function __set($name, $value)
    {
        switch ($name) {
            case "context":
            case "title":
                $this->$name = $value;
                break;
            case "error":
            case "success":
                if (!$value || !trim($value)) {
                    return;
                }
                if (!is_array($value)) {
                    $value = [$value];
                }
                $this->$name = array_merge($this->$name, $value);
                break;
            default:
                $this->$name = $value;
                $this->otherData[] = $name;
        }
    }

    /**
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * @return array
     */
    public function getViewData()
    {
        $ret = [];
        foreach ($this->otherData as $name) {
            $ret[$name] = $this->$name;
        }
        return $ret;
    }
}