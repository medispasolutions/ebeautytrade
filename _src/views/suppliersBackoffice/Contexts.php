<?php
namespace views\suppliersBackoffice;

class Contexts
{
    const TEST = "test";
    const DASHBOARD = "dashboard";
    const BRANDS_LISTING = "brandsListing";
    const CATEGORIES_LISTING = "categoriesListing";
    const PRODUCTS_LISTING_LIVE_ON_SITE = "productsListingLiveOnSite";
    const PRODUCTS_LISTING_UNPUBLISHED = "productsListingUnpublished";
    const PRODUCTS_LISTING_SPECIALS = "productsListingSpecials";
    const BUYERS_LISTING = "buyersListing";
    const ORDERS_LISTING = "ordersListing";
    const MESSAGES_LISTING = "messagesListing";
    const PROFILE_MANAGE = "profileManage";
    const BRAND_ADD = "brandAdd";
    const BRAND_EDIT = "brandEdit";
    const CATEGORY_ADD = "categoryAdd";
    const CATEGORY_EDIT = "categoryEdit";
    const PRODUCT_ADD = "productAdd";
    const PRODUCT_EDIT = "productEdit";
    const PROMOCODE_LISTING = "promocodeListing";
    const PROMOCODE_ADD = "promocodeAdd";
    const PROMOCODE_EDIT = "promocodeEdit";
    const ORDER_VIEW = "orderView";
    const LOGIN_VIEW = "loginView";
    const FORGOTTEN_PASSWORD_VIEW = "forgottenPasswordView";
    const RESET_PASSWORD_VIEW = "resetPasswordView";
    const WELCOME_VIEW = "welcomeView";
    const SUPPLIER_LISTING = "supplierListing";
    const SUPPLIER_ADD = "supplierAdd";
    const SUPPLIER_EDIT = "supplierEdit";
    const FINANCIAL_LISTING = "financialListing";
    const FINANCIAL_REQUEST = "financialRequest";
    const FINANCIAL_ORDER = "financialOrders";
}