<?php
namespace views\suppliersBackoffice;

use views\ManagerInterface;
use views\TwigView;
use views\FrontCache;
use views\twigExtensions\Currency;
use views\twigExtensions\International;
use views\twigExtensions\Mustache;
use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_Extension_Debug;

class Manager implements ManagerInterface
{
    
    protected $twig;
    protected $loader;
    protected $filesystem;

    /** @var \Acl */
    protected $acl;
    /** @var \core\ServiceLocator */
    protected $serviceLocator;


    public function __construct() 
    {
        $this->filesystem = array('templates/suppliers-backoffice');
        
        $this->loader = new Twig_Loader_Filesystem($this->filesystem);
        
    	$this->twig = new Twig_Environment($this->loader, array('debug' => true));
    	$this->twig->addExtension(new Twig_Extension_Debug());

    	//load extensions
        $currency = new Currency();
        $currency->extend($this->twig);
        
        $intl = new International();
        $intl->extend($this->twig);

        $mustache = new Mustache();
        $mustache->extend($this->twig);

        $CI =& get_instance();
        $this->acl = $CI->acl;
        $this->db = $CI->db;
        $this->serviceLocator = get_service_locator();
    }

    public function commonConfig() 
    {
        FrontCache::setSrcDir('public/suppliers-backoffice/build');

        if (isset($_COOKIE['isSiteSidebarVisible'])) {
            if ($_COOKIE['isSiteSidebarVisible'] == 'false') $isSiteSidebarVisible = false;
            else $isSiteSidebarVisible = true;
        } else {
            $isSiteSidebarVisible = true;
        }

        $currentUser = $this->serviceLocator->user($this->acl->getCurrentUserId());
        $currentUserData = $currentUser->toArray();


        //list unread messages
        if ($this->acl->isCurrentUserLoggedIn()) {
            $this->db->where(['id_user' => $this->acl->getCurrentUserId()]);
            $tmp = $this->db->get("brands")->result_array();
            $user_brands_ids = [];
            foreach ($tmp as $row) {
                $user_brands_ids[] = $row['id_brands'];
            }
            $this->db->where('readed', 0);
            if (count($user_brands_ids) > 0) {
                $this->db->where_in('id_brands', $user_brands_ids);
                $newMessages = $this->db->get('contactform')->num_rows();
            }
        }

        $commonConfig = [
            'BIACONFIG' => [
                'app' => [
                    'ENVIRONMENT' => _getenv('ENV'), //'development', 'production',
                    'environments' => [
                        'DEVELOPMENT' => 'development',
                        'PRODUCTION' => 'production',
                    ],
                    'LANGUAGE' => 'en',
                    'CKEDITOR_PATH' => '/public/suppliers-backoffice/build/ckeditor',
                    'breakpoints' => [
                        'BP_XS_MIN' => 320,
                        'BP_SM_MIN' => 768,
                        'BP_MD_MIN' => 1024,
                        'BP_LG_MIN' => 1200,
                        'BP_XL_MIN' => 1366,
                    ],
                ],
                'context' => [
                    'id' => '',
                    'contexts' => [
                        'TEST' => Contexts::TEST,
                        'DASHBOARD' => Contexts::DASHBOARD,
                        'BRANDS_LISTING' => Contexts::BRANDS_LISTING,
                        'CATEGORIES_LISTING' => Contexts::CATEGORIES_LISTING,
                        'PRODUCTS_LISTING_LIVE_ON_SITE' => Contexts::PRODUCTS_LISTING_LIVE_ON_SITE,
                        'PRODUCTS_LISTING_UNPUBLISHED' => Contexts::PRODUCTS_LISTING_UNPUBLISHED,
                        'PRODUCTS_LISTING_SPECIALS' => Contexts::PRODUCTS_LISTING_SPECIALS,
                        'BUYERS_LISTING' => Contexts::BUYERS_LISTING,
                        'ORDERS_LISTING' => Contexts::ORDERS_LISTING,
                        'MESSAGES_LISTING' => Contexts::MESSAGES_LISTING,
                        'PROFILE_MANAGE' => Contexts::PROFILE_MANAGE,
                        'BRAND_ADD' => Contexts::BRAND_ADD,
                        'BRAND_EDIT' => Contexts::BRAND_EDIT,
                        'CATEGORY_ADD' => Contexts::CATEGORY_ADD,
                        'CATEGORY_EDIT' => Contexts::CATEGORY_EDIT,
                        'PRODUCT_ADD' => Contexts::PRODUCT_ADD,
                        'PRODUCT_EDIT' => Contexts::PRODUCT_EDIT,
                        'PROMOCODE_LISTING' => Contexts::PROMOCODE_LISTING,
                        'PROMOCODE_ADD' => Contexts::PROMOCODE_ADD,
                        'PROMOCODE_EDIT' => Contexts::PROMOCODE_EDIT,
                        'SUPPLIER_LISTING' => Contexts::SUPPLIER_LISTING,
                        'SUPPLIER_ADD' => Contexts::SUPPLIER_ADD,
                        'SUPPLIER_EDIT' => Contexts::SUPPLIER_EDIT,
                        'ORDER_VIEW' => Contexts::ORDER_VIEW,
                        'LOGIN_VIEW' => Contexts::LOGIN_VIEW,
                        'FORGOTTEN_PASSWORD_VIEW' => Contexts::FORGOTTEN_PASSWORD_VIEW,
                        'RESET_PASSWORD_VIEW' => Contexts::RESET_PASSWORD_VIEW,
                        'WELCOME_VIEW' => Contexts::WELCOME_VIEW,
                        'FINANCIAL_LISTING' => Contexts::FINANCIAL_LISTING,
                        'FINANCIAL_REQUEST' => Contexts::FINANCIAL_REQUEST,
                        'FINANCIAL_ORDER' => Contexts::FINANCIAL_ORDER
                    ],
                    'user' => [
                        'id' => $currentUserData['id_user'],
                        'EMAIL' => $currentUserData['email'],
                        'FIRST_NAME' => $currentUserData['first_name'],
                        'LAST_NAME' => $currentUserData['last_name'],
                        'USER_AVATAR_URL' => $currentUserData['photo_thumb'],
                        'COMPANY_LOGO_URL' => $currentUserData['logo'],
                        // Set to 100 to hide the progressbar
                        'PROFILE_COMPLETION_PERCENT' => $this->acl->profileCompletionPercent(),
                        'UNREAD_MESSAGES' => $newMessages,
                    ],
                    'ENABLE_UNLOAD_PREVENTION' => false,
                ],
                'state' => [
                    // Set to `true` if we have a user cookie (login state is not important)
                    'IS_USER_KNOWN' => true,
                    // Set to `true` if user is logged
                    'IS_USER_LOGGED_IN' => $this->acl->isCurrentUserLoggedIn(),
                    'IS_SITE_SIDEBAR_VISIBLE' => $isSiteSidebarVisible,
                ],
                'build' => [
                    'img' => FrontCache::getInstance()->get('images'),
                    'js' => FrontCache::getInstance()->get('scripts'),
                    'css' => FrontCache::getInstance()->get('styles'),
                ],
            
                'international' => [
                ],

                'CommunicationInterface' => [
                    'locale' => [
                        'ERROR_TITLE' => 'An error occurred while processing your request',
                        'ERROR_INFO' => 'Sending your request has failed. Please try again.',
                        'SUPPORT_INFO' => 'In case of further problems please contact Support Department attaching the following information:',
                    ],
                ],

                'EasyEdit' => [
                    'locale' => [
                        'ERROR_TITLE' => 'An error occurred while saving',
                        'ERROR_CONTENT' => 'An error occurred while saving. Please try again or contact Support Department.',
                    ],
                ],

                'Mark' => [
                    'locale' => [
                        'ERROR_TITLE' => 'An error occurred while saving',
                        'ERROR_CONTENT' => 'An error occurred while marking as read/unread. Please try again or contact Support Department.',
                    ],
                ],

                'Gallery' => [
                    'locale' => [
                        'ERROR_TITLE' => 'An error occurred while saving',
                        'ERROR_CONTENT' => 'An error occurred while saving. Please try again or contact Support Department.',
                    ],
                ],
            ],

            'seo' => [
                'ROBOTS' => 'noindex,nofollow',
                'PAGE_TITLE_SUFFIX' => ' | EBeautyTrade',
                'PAGE_TITLE' => '',
            ],

            'externalLinks' => [
                'Ebeautytrade' => [
                    'URL' => 'https://ebeautytrade.com',
                ],
            ],
            
            'links' => [
                'dashboard' => [
                    'URL' => '/back_office/dashboard/home',
                ],
                'brandsListing' => [
                    'URL' => '/back_office/brands/listing',
                ],
                'categoriesListing' => [
                    'URL' => '/back_office/categories/listing',
                ],
                'promocodeListing' => [
                    'URL' => '/back_office/promocode/listing',
                ],
                'productsListingLiveOnSite' => [
                    'URL' => '/back_office/products/live_on_site',
                ],
                'productsListingUnpublished' => [
                    'URL' => '/back_office/products/unpublished',
                ],
                'productsListingSpecials' => [
                    'URL' => '/back_office/products/specials',
                ],
                'buyersListing' => [
                    'URL' => '/back_office/buyers/listing',
                ],
                'ordersListing' => [
                    'URL' => '/back_office/orders/listing',
                ],
                'financialListing' => [
                    'URL' => '/back_office/financial/listing',
                ],
                'financialOrders' => [
                    'URL' => '/back_office/financial/order',
                ],
                'financialRequest' => [
                    'URL' => '/back_office/financial/request',
                ],
                'messagesListing' => [
                    'URL' => '/back_office/messages/listing',
                ],
                'messageDelete' => [
                    'URL' => '/back_office/messages/remove',
                ],
                'profileManage' => [
                    'URL' => '/back_office/profile/edit',
                ],
                'brandAdd' => [
                    'URL' => '/back_office/brands/create',
                ],
                'brandEdit' => [
                    'URL' => '/back_office/brands/revise',
                ],
                'promocodeAdd' => [
                    'URL' => '/back_office/promocode/create',
                ],
                'promocodeEdit' => [
                    'URL' => '/back_office/promocode/revise',
                ],
                'promocodeDelete' => [
                    'URL' => '/back_office/promocode/delete',
                ],
                'supplierListing' => [
                    'URL' => '/back_office/supplier/listing',
                ],
                'supplierAdd' => [
                    'URL' => '/back_office/supplier/create',
                ],
                'supplierDelete' => [
                    'URL' => '/back_office/supplier/delete',
                ],
                'supplierEdit' => [
                    'URL' => '/back_office/supplier/revise',
                ],
                //'supplierDelete' =>['URL' => '/back_office/supplier/delete',],
                'categoryAdd' => [
                    'URL' => '/back_office/categories/create',
                ],
                'categoryEdit' => [
                    'URL' => '/back_office/categories/revise',
                ],
                'brandDelete' => [
                    'URL' => '', //'/back_office/brands/delete',
                ],
                // This should take the brand ID and redirect the user to the Spamiles shop, to the particular brand page.
                'externalBrandView' => [
                    'URL' => '/brands/details',
                ],
                'productAdd' => [
                    'URL' => '/back_office/products/create',
                ],
                'productEdit' => [
                    'URL' => '/back_office/products/revise',
                ],
                'productUnpublish' => [
                    'URL' => '/back_office/products/unpublish',
                ],
                'productSendToReview' => [
                    'URL' => '/back_office/products/send_to_review',
                ],
                'productDelete' => [
                    'URL' => '', //'/back_office/products/delete'
                ],
                'productStockStatus' => [
                    'URL' => '/back_office/products/stockStatus', //'/back_office/products/delete'
                ],
                // This should take the product ID and redirect the user to the Spamiles shop, to the particular product page.
                'externalProductView' => [
                    'URL' => '/products/details',
                ],
                'messageModal' => [
                    'URL' => '/back_office/messages/view',
                ],
                'buyerModal' => [
                    'URL' => '/back_office/buyers/view',
                ],
                'productDiscountModal' => [
                    'URL' => '/back_office/products/editDiscount',
                ],
                'multiBuyModal' => [
                    'URL' => '/back_office/products/editBulkPricing',
                ],
                'productPropertiesModal' => [
                    'URL' => '/back_office/products/editProperties',
                ],
                'stockStatusModal' => [
                    'URL' => '/back_office/products/editStockStatus',
                ],
                'orderView' => [
                    'URL' => '/back_office/orders/revise',
                ],
                //disabled
                /*'orderDelete' => [
                    'URL' => '/back_office/orders/delete',
                ],*/
                'logout' => [
                    'URL' => '/back_office/home/logout',
                ],
                'loginView' => [
                    'URL' => '/back_office/home',
                ],
                'resetPasswordView' => [
                    'URL' => '/back_office/home/update_password',
                ],
                'forgottenPasswordView' => [
                    'URL' => '/back_office/home/password',
                ],
                'welcomeView' => [
                    'URL' => '/view/welcome',
                ],
            ],

            'pagination' => [
                'PREV_HREF' => '', // No previous page
                'NEXT_HREF' => '/page/2',
                'RESULTS_START' => 1,
                'RESULTS_END' => 10,
                'RESULTS_TOTAL' => 17,
                'options' => [
                    [
                        'TEXT' => 25,
                        'VALUE' => 25,
                        'IS_SELECTED' => true,
                    ],
                    [
                        'TEXT' => 50,
                        'VALUE' => 50,
                        'IS_SELECTED' => false,
                    ],
                    [
                        'TEXT' => 100,
                        'VALUE' => 100,
                        'IS_SELECTED' => false,
                    ],
                ],
            ],
        ];

        return $commonConfig;
    }
    
    public function render($template, $data = array(), $return = false)
    {
        $obj = new TwigView();
        
        $obj->setManager($this);
        $obj->setViewData($data);
        $obj->setTwig($this->twig);
        $obj->setTemplate($template);
        
        //below is rewritten from CI's systems/core/Loader::_ci_load()
        ob_start();
        
        echo $obj->render();
        
        if ($return === true)
		{
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}
        
        $_ci_CI =& get_instance();
		$_ci_CI->output->append_output(ob_get_contents());
		@ob_end_clean();
    }
}
