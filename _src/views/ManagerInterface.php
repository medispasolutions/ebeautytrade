<?php

namespace views;

interface ManagerInterface {
    
    public function commonConfig();
    
    public function render($template, $data = array(), $return = false);
    
}
