<?php

namespace views;

class FrontCache
{

    static $instance = NULL;
    static $srcDir = NULL;
    private $cacheList = array();

    public static function setSrcDir($srcDir) {
        self::$srcDir = $srcDir;
    }

    public static function getInstance()
    {
        if (self::$instance == NULL) {
            self::$instance = new self(self::$srcDir);
        }

        return self::$instance;
    }

    public function __construct($srcDir)
    {
        if (!preg_match('/\/$/', $srcDir)) {
            $srcDir .= "/";
        }
        self::$srcDir = $srcDir;
        if (!is_dir(self::$srcDir)) {
            die("FrontCache::__construct() -> this is not a directory: " . self::$srcDir);
        }
        $this->readDir(self::$srcDir);
    }

    private function readDir($srcDir) {
        if ($handle = opendir($srcDir)) {
            $dirsToVisit = array();
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($srcDir.$file)) {
                        $dirsToVisit[] = $srcDir.$file."/";
                    } elseif (is_file($srcDir.$file) && preg_match('/\.manifest$/', $file)) {
                        $this->readManifest($srcDir.$file);
                    }
                }
            }
            closedir($handle);
            foreach ($dirsToVisit as $w) {
                $this->readDir($w);
            }
        }
    }

    private function readManifest($srcFile) {
        $path = pathinfo($srcFile);
        $json = json_decode(file_get_contents($srcFile), true);
        foreach ($json as $i=>$w) {
            $json[$i] = "/".$path['dirname']."/".$w;
        }
        $this->cacheList = array_merge($this->cacheList, array($path['filename']=>$json));
    }

    public function get($name) {
      if (!isset($this->cacheList[$name])) {
         $msg = "FrontCache->get() - no element with id: {$name}";
         if (! TEST_SERVER) {
             die($msg);
         }
         echo "<h1 style=\"color: red;\"> {$msg}</h1>";
      }

      return $this->cacheList[$name];
   }
}
