<?php

namespace views\twigExtensions;

use Twig_SimpleFilter;
use Twig_Markup;

class International {

    private $language = 'en';

    public function getWordForm($params) {
        switch ($this->language) {
            case 'en':
                return $this->getWordFormEN($params);
            // no default
        }
    }

    private function getWordFormEN($params) {
        $absolute = abs($params['NUMBER']);
        return $absolute === 1 ? $params['wordVariants']['singular'] : $params['wordVariants']['plural'];
    }
    
    public function extend($twig) {
        /**
         * Returns a proper word form based on a number
         *
         * @param Array   $wordVariants Array with all available word forms
         * @param String  $context   Required word form context
         * @param String  $number    Number for which the proper word form will be chosen
         *
         * @return String Chosen proper word form
         */     
        $international = new Twig_SimpleFilter('international', function ($wordVariants, $context, $number) {
                    
            if ($wordVariants instanceof Twig_Markup || !is_array($wordVariants)) {
                return 'TypeError: $wordForm is not a valid array';
            }
    
            $filter = new \application\twigViews\twigExtensions\International();
    
            $params = array(
                'wordVariants' => $wordVariants,
                'CONTEXT' => $context,
                'NUMBER' => $number
            );
    
            $final_form = $filter->getWordForm($params);
    
            return $final_form;
        });
        
        $twig->addFilter($international);
    }
}
