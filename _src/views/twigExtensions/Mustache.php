<?php

namespace views\twigExtensions;

use Twig_SimpleFilter;
use Twig_Markup;
use Mustache_Engine;
use Mustache_Loader_FilesystemLoader;
use Mustache_Logger_StreamLogger;

class Mustache {
    public static function fullmkdir($dir) {
        if (!is_dir($dir)) {
           $path = explode("/", $dir);
           $pom = "";
           foreach ($path as $i=>$w) {
              if ($w=="")
                 continue;
                 
              $pom.="{$w}/";
              if (!is_dir($pom))
                 mkdir($pom);
           }
        }
    }

    public function extend($twig) {
        /**
         * Returns a compiled mustache template
         *
         * @param Array  $data      Array with data for the template
         * @param String $template  Mustache template path
         *
         * @return String $compiled	Compiled template
         */     
        $mustache = new Twig_SimpleFilter('mustache', function ($data, $template) {
            if ($data instanceof Twig_Markup || !is_array($data)) {
                return 'TypeError: $data is not a valid array';
            }
            $appRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/';
            $cacheDir = $appRoot . 'cache/mustache';
            $templatesDir = $appRoot . 'templates/suppliers-backoffice';
            $partialsDir = $templatesDir;
            Mustache::fullmkdir($cacheDir);
            Mustache::fullmkdir($partialsDir);

            $mustache = new Mustache_Engine(array(
                'cache' => $cacheDir,
                'cache_file_mode' => 0755, // Please, configure your umask instead of doing this :)
                'cache_lambda_templates' => true,
                'loader' => new Mustache_Loader_FilesystemLoader( $templatesDir ),
                'partials_loader' => new Mustache_Loader_FilesystemLoader( $partialsDir ),
                'escape' => function($value) {
                    return htmlspecialchars($value, ENT_COMPAT | ENT_SUBSTITUTE, 'UTF-8');
                },
                'charset' => 'UTF-8',
                'logger' => new Mustache_Logger_StreamLogger('php://stderr'),
                'strict_callables' => true,
                'pragmas' => array(Mustache_Engine::PRAGMA_FILTERS),
            ));
                
            $tpl = $mustache->loadTemplate($template); // loads __DIR__.'/views/foo.mustache';
            return $tpl->render($data);
        });

        /**
         * https://github.com/bobthecow/mustache.php/wiki/Template-Loading
         */
        $mustacheStr = new Twig_SimpleFilter('mustacheStr', function ($data, $template) {

            if ($data instanceof Twig_Markup || !is_array($data)) {
                return 'TypeError: $data is not a valid array';
            }
            $appRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/';
            $cacheDir = $appRoot . 'cache/mustache';
            $templatesDir = $appRoot . 'templates/suppliers-backoffice';
            $partialsDir = $templatesDir;
            Mustache::fullmkdir($cacheDir);
            Mustache::fullmkdir($partialsDir);

            $mustache = new Mustache_Engine(array(
                'cache' => $cacheDir,
                'cache_file_mode' => 0755, // Please, configure your umask instead of doing this :)
                'escape' => function($value) {
                    return htmlspecialchars($value, ENT_COMPAT | ENT_SUBSTITUTE, 'UTF-8');
                },
                'charset' => 'UTF-8',
                'logger' => new Mustache_Logger_StreamLogger('php://stderr'),
                'strict_callables' => true,
                'pragmas' => array(Mustache_Engine::PRAGMA_FILTERS),
            ));

            $tpl = $mustache->loadTemplate($template); // loads __DIR__.'/views/foo.mustache';
            return $tpl->render($data);
        });

        $twig->addFilter($mustache);
        $twig->addFilter($mustacheStr);
    }
}
