<?php

namespace views\twigExtensions;

use Twig_SimpleFilter;

class Currency {

    public function __construct() {
    
    }
    
    public function extend($twig) {
        $currency = new Twig_SimpleFilter('currency', function ($string, $options = 0) {
            $string = (string) $string;
            
            if (is_array($options)) {
                $position = $options['position'];
                $symbol = $options['symbol'];
    
                if ($position == 'prefix') {
                    $replace = 'prefix';
                    $clear = 'suffix';
                }
                else {
                    $replace = 'suffix';
                    $clear = 'prefix';
                }
    
                if (array_key_exists('value', $options)) {
                    $value = $options['value'];
                }
                else {
                    $value = '';
                }
    
                $string = str_replace('{'.$replace.'}', $symbol, $string);
                $string = str_replace('{'.$clear.'}', '', $string);
                $string = str_replace('{price}', $value, $string);
            }
    
            return $string;
        });  
        $twig->addFilter($currency);
    }
}
