<?php

namespace views;

use Twig_Environment;

class TwigView implements ViewInterface 
{
    
    protected $data = array();
    protected $twig;
    protected $manager;
    protected $template;
    
    public function setTemplate($template) 
    {
        $this->template = $template.".twig";
    }
    
    public function setTwig(Twig_Environment $twig) 
    {
        $this->twig = $twig;
    }
    
    public function setViewData(array $viewData) 
    {
        $this->data = $viewData;
    }
    
    public function setManager(ManagerInterface $manager) 
    {
        $this->manager = $manager;
    }
    
    public function config() 
    {
        return $this->manager->commonConfig();
    }
    
    public function render() 
    {
        $config = $this->config();
        $config = $this->array_merge_recursive_distinct($config, $this->data);
        $content = $this->twig->render($this->template, $config);
        
        return $content;
    }
    
    private function array_merge_recursive_distinct(array &$array1, array &$array2)
    {
        $merged = $array1;
    
        foreach ( $array2 as $key => &$value ) {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) ) {
                $merged [$key] = $this->array_merge_recursive_distinct( $merged [$key], $value );
            } else {
                $merged [$key] = $value;
            }
        }
    
        return $merged;
    }
    
}
