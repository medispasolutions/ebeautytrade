<?php

namespace views;

interface ViewInterface {
    
    public function setTemplate($template);
    
    public function setViewData(array $viewData);
    
    public function config();
    
    public function setManager(ManagerInterface $manager);
    
    public function render();
    
}
