{% extends '_layouts/_default.twig' %}
{% import '_macros/__listing-filters.twig' as listingFilters %}
{% import '_macros/__listing-table.twig' as listingTable %}

{% block content %}
  <div class="container  [ margin-top--20 margin-bottom--30 ]">
    {% include 'productsListingPage/_filters.twig' %}
  </div>

  {% include 'productsListingPage/_statistics.twig' %}

  <div class="container  margin-top--20">
    {{ listingTable.listingHeader({
      'TABLE_ID': 'listingTable',
      'TITLE': 'Products list:',
      'tabs': [
        {
          'HREF': links.productsListingLiveOnSite.URL,
          'LABEL': 'Live On Site',
          'IS_ACTIVE': BIACONFIG.context.id == BIACONFIG.context.contexts.PRODUCTS_LISTING_LIVE_ON_SITE
        },
        {
          'HREF': links.productsListingUnpublished.URL,
          'LABEL': 'Unpublished',
          'IS_ACTIVE': BIACONFIG.context.id == BIACONFIG.context.contexts.PRODUCTS_LISTING_UNPUBLISHED
        },
        {
          'HREF': links.productsListingSpecials.URL,
          'LABEL': 'Specials',
          'IS_ACTIVE': BIACONFIG.context.id == BIACONFIG.context.contexts.PRODUCTS_LISTING_SPECIALS
        }
      ],
      'buttons': [
        {
          'HREF': links.productAdd.URL,
          'LABEL': 'Add Product',
          'buttonClasses': [
            'btn--primary',
            'btn--small-padding'
          ],
          'iconClasses': [
            'icon--o-plus',
            'icon--white'
          ]
        }
      ]
    }) }}
      {{ listingTable.table({
        'TABLE_ID': 'listingTable',
        'structure': [
          {
            'TYPE': 'thumbnail',
            'HEADER_CONTENT': 'Image'
          },
          {
            'TYPE': 'textinput-long',
            'HEADER_CONTENT': 'SKU',
            'thClasses': ['text--center'],
            'tdClasses': ['text--center']
          },
          {
            'TYPE': 'name',
            'HEADER_CONTENT': 'Product Name'
          },
          {
            'TYPE': 'datetime-precise',
            'HEADER_CONTENT': 'Last Updated',
            'thClasses': ['text--center'],
            'tdClasses': ['text--center']
          },
          {
            'TYPE': 'numberinput-medium',
            'HEADER_CONTENT': 'Retail Price (AED)'
          },
          {
            'TYPE': 'numberinput-medium',
            'HEADER_CONTENT': 'Trade Price (AED)'
          },
          {
            'TYPE': 'button',
            'HEADER_CONTENT': 'Discount Offer'
          },
          {
            'TYPE': 'button',
            'HEADER_CONTENT': 'Bulk Pricing'
          },
          {
            'TYPE': 'button',
            'HEADER_CONTENT': 'Properties'
          },
          {
            'TYPE': 'status-product',
            'HEADER_CONTENT': 'Status'
          },
          {
            'TYPE': 'icon-actions',
            'HEADER_CONTENT': 'Actions',
            'actions': [
              {
                'classes': [],
                'HREF': links.productEdit.URL ~ '/${rowId}',
                'LABEL': 'Edit',
                'iconClasses': [
                  'icon--white',
                  'icon--pencil',
                  'icon--10'
                ]
              },
              {
                'classes': [],
                'HREF': links.externalProductView.URL ~ '/${rowId}',
                'LABEL': 'View',
                'iconClasses': [
                  'icon--white',
                  'icon--eye'
                ],
                'TARGET': '_blank'
              }
            ]
          }
        ],
        'data': data.info
      }) }}
    {{ listingTable.listingFooter({
      'TABLE_ID': 'listingTable',
      'pagination': pagination
    }) }}

    {#deletion disabled{
    'classes': [],
    'HREF': links.productDelete.URL ~ '/${rowId}',
    'LABEL': 'Delete',
    'iconClasses': [
    'icon--white',
    'icon--x',
    'icon--10'
    ],
    'CONFIRM_MSG': 'Are you sure you want to delete this product? This cannot be undone.'
    },#}
  </div>
{% endblock %}

{% block modals %}
  {{ parent() }}
  {% include '_modals/_multi-buy.twig' %}
  {% include '_modals/_product-properties.twig' %}
  {% include '_modals/_product-discount.twig' %}
{% endblock %}
