<?php 
global $qode_options_proya;
$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments'])) {
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];
}
$blog_share_like_layout = 'in_post_info';
if (isset($qode_options_proya['blog_share_like_layout'])) {
    $blog_share_like_layout = $qode_options_proya['blog_share_like_layout'];
}
$enable_social_share = 'no';
if(isset($qode_options_proya['enable_social_share'])){
    $enable_social_share = $qode_options_proya['enable_social_share'];
}
$blog_author_info="no";
if (isset($qode_options_proya['blog_author_info'])) {
	$blog_author_info = $qode_options_proya['blog_author_info'];
}
$qode_like = "on";
if (isset($qode_options_proya['qode_like'])) {
    $qode_like = $qode_options_proya['qode_like'];
}

$params = array(
    'blog_share_like_layout' => $blog_share_like_layout,
    'enable_social_share' => $enable_social_share,
    'qode_like' => $qode_like
);

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="post_content_holder"> 
    
    <!--date-->
    <div class="post_info"> <span class="time">
      <?php the_time('d F, Y'); ?>
      </span>
     
      <?php if($blog_hide_comments != "yes"){ ?>
      / <a class="post_comments" href="<?php comments_link(); ?>" target="_self">
      <?php comments_number('0 ' . __('Comments','qode'), '1 '.__('Comment','qode'), '% '.__('Comments','qode') ); ?>
      </a>
      <?php } ?>
       <span class="back"><a href="<?php echo get_home_url(); ?>">Back</a></span>
    </div>
    <!--date--> 
    <!--title-->
    <h1>
      <?php the_title(); ?>
    </h1>
    
    <!--title-->
    
    <?php if(get_post_meta(get_the_ID(), "qode_hide-featured-image", true) != "yes") {
					if ( has_post_thumbnail() ) { ?>
    <div class="post_image">
      <?php the_post_thumbnail('full'); ?>
    </div>
    <?php } } ?>
    <div class="post_text">
      <div class="post_text_inner">
        <?php the_content(); ?>
      </div>
    </div>
  </div>
  <?php if( has_tag()) { ?>
  <div class="single_tags clearfix">
    <div class="tags_text">
      <h5>
        <?php _e('Tags:','qode'); ?>
      </h5>
      <?php 
				if ((isset($qode_options_proya['tags_border_style']) && $qode_options_proya['tags_border_style'] !== '') || (isset($qode_options_proya['tags_background_color']) && $qode_options_proya['tags_background_color'] !== '')){
					the_tags('', ' ', '');
				}
				else{
					the_tags('', ', ', '');
				}
				?>
    </div>
  </div>
  <?php } ?>
  <?php qode_get_template_part('templates/blog-parts/blog','share-like-below-text',$params); ?>
  <?php 
		$args_pages = array(
			'before'           => '<p class="single_links_pages">',
			'after'            => '</p>',
			'link_before'      => '<span>',
			'link_after'       => '</span>',
			'pagelink'         => '%'
		);

		wp_link_pages($args_pages);
	?>
</article>
