
<?php $domain = chop(get_site_url(), 'blog'); ;?>
<?php global $qode_options_proya, $wp_query, $qode_toolbar, $qodeIconCollections; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php
	if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
		echo('<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">');
	} ?>
<title>
<?php wp_title(''); ?>
</title>
<?php
	/**
	 * qode_header_meta hook
	 *
	 * @see qode_header_meta() - hooked with 10
	 * @see qode_user_scalable_meta() - hooked with 10
	 */
	do_action('qode_header_meta');
	?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url($qode_options_proya['favicon_image']); ?>">
<link rel="apple-touch-icon" href="<?php echo esc_url($qode_options_proya['favicon_image']); ?>"/>
<?php wp_head(); ?>
<link href="http://spamiles.com/front/slick/slick.css" rel="stylesheet">
<link rel="stylesheet" href="http://www.spamiles.com/front/css/css_blg.css">
<script src="http://spamiles.com/front/slick/slick.js"></script>
<script type="text/javascript" src="http://www.spamiles.com/front/js/script_blg.js"></script>
  <script>
        jQuery(document).ready(function() {
            setTimeout(function(){

                var images = jQuery('.header_text_slider .item')
                current = 0;
                images.hide();
                jQuery(".header_text_slider").show();
                Rotator();
                function Rotator() {
                    jQuery(images[current]).fadeIn('slow').delay(2000).fadeOut('slow');
                    jQuery(images[current]).queue(function() {
                        current = current < images.length - 1 ? current + 1 : 0;
                        Rotator();
                        jQuery(this).dequeue();
                    });
                }

            }, 2000);

        });
    </script>

</head>
<body <?php body_class(); ?>>
<?php
		$loading_animation = true;
		if (isset($qode_options_proya['loading_animation'])){ if($qode_options_proya['loading_animation'] == "off") { $loading_animation = false; }};

		if (isset($qode_options_proya['loading_image']) && $qode_options_proya['loading_image'] != ""){ $loading_image = $qode_options_proya['loading_image'];}else{ $loading_image =  ""; }
	?>
<?php if($loading_animation){ ?>
<div class="ajax_loader">
  <div class="ajax_loader_1">
    <?php if($loading_image != ""){ ?>
    <div class="ajax_loader_2"><img src="<?php echo $loading_image; ?>" alt="" /></div>
    <?php } else{ qode_loading_spinners(); } ?>
  </div>
</div>
<?php } ?>
<?php
		$enable_side_area = "yes";
		if (isset($qode_options_proya['enable_side_area'])){ if($qode_options_proya['enable_side_area'] == "no") { $enable_side_area = "no"; }};

        $enable_popup_menu = "no";
        if (isset($qode_options_proya['enable_popup_menu'])){
            if($qode_options_proya['enable_popup_menu'] == "yes" && has_nav_menu('popup-navigation')) {
                $enable_popup_menu = "yes";
            }
            if (isset($qode_options_proya['popup_menu_animation_style']) && !empty($qode_options_proya['popup_menu_animation_style'])) {
                $popup_menu_animation_style = 'qode_'.$qode_options_proya['popup_menu_animation_style'];
            }
        };
		
		$enable_fullscreen_search="no";
		if(isset($qode_options_proya['enable_search']) && $qode_options_proya['enable_search'] == "yes" && isset($qode_options_proya['search_type']) && $qode_options_proya['search_type'] == "fullscreen_search" ){ 
			$enable_fullscreen_search="yes";
		}

		$fullscreen_search_animation="fade";
		if(isset($qode_options_proya['search_type']) && $qode_options_proya['search_type'] == "fullscreen_search" && isset($qode_options_proya['search_animation']) && $qode_options_proya['search_animation'] !== "" ){ 
			$fullscreen_search_animation = $qode_options_proya['search_animation'];
		}
		
		$enable_vertical_menu = false;
		if(isset($qode_options_proya['vertical_area']) && $qode_options_proya['vertical_area'] =='yes'){
			$enable_vertical_menu = true;
		}

        $header_button_size = '';
        if(isset($qode_options_proya['header_buttons_size'])){
            $header_button_size = $qode_options_proya['header_buttons_size'];
        }
	?>
<?php if($enable_side_area == "yes" && $enable_popup_menu == 'no') {
		//generate side area classes
		$side_area_classes = '';

		if(isset($qode_options_proya['side_area_close_icon_style']) && $qode_options_proya['side_area_close_icon_style'] != '') {
			$side_area_classes .= $qode_options_proya['side_area_close_icon_style'];
		}
		if (isset($qode_options_proya['side_area_alignment']) && ($qode_options_proya['side_area_alignment'] !== '')) {
			$side_area_classes .= " side_area_alignment_" . $qode_options_proya['side_area_alignment'];
		}
	?>
<section class="side_menu right <?php echo $side_area_classes; ?>">
  <?php if(isset($qode_options_proya['side_area_title']) && $qode_options_proya['side_area_title'] != "") { ?>
  <div class="side_menu_title">
    <h5><?php echo $qode_options_proya['side_area_title'] ?></h5>
  </div>
  <?php } ?>
  <a href="#" target="_self" class="close_side_menu"></a>
  <?php dynamic_sidebar('sidearea'); ?>
</section>
<?php } ?>
<?php if(isset($qode_toolbar)) include("toolbar_examples.php") ?>
<div class="wrapper">
<div class="wrapper_inner">
<!-- Google Analytics start -->
<?php if (isset($qode_options_proya['google_analytics_code'])){
				if($qode_options_proya['google_analytics_code'] != "") {
	?>
<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', '<?php echo $qode_options_proya['google_analytics_code']; ?>']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
<?php }
		}
	?>
<!-- Google Analytics end -->

<?php

$paspartu_header_alignment = false;
if(isset($qode_options_proya['paspartu_header_alignment']) && $qode_options_proya['paspartu_header_alignment'] == 'yes' && isset($qode_options_proya['paspartu']) && $qode_options_proya['paspartu'] == 'yes'){$paspartu_header_alignment = true;}

$header_in_grid = true;
if((isset($qode_options_proya['header_in_grid']) && $qode_options_proya['header_in_grid'] == "no") || $paspartu_header_alignment){ $header_in_grid = false; }

	$menu_position = "right";
	if(isset($qode_options_proya['menu_position']) && $qode_options_proya['menu_position'] !== ''){$menu_position = $qode_options_proya['menu_position']; }

	$centered_logo = false;
	if (isset($qode_options_proya['center_logo_image'])){ if($qode_options_proya['center_logo_image'] == "yes") { $centered_logo = true; }};

    $centered_logo_animate = false;
    if (isset($qode_options_proya['center_logo_image_animate'])){ if($qode_options_proya['center_logo_image_animate'] == "yes") { $centered_logo_animate = true; }};

    if(isset($qode_options_proya['header_bottom_appearance']) && $qode_options_proya['header_bottom_appearance'] == "fixed_hiding"){
        $centered_logo = true;
        $centered_logo_animate = true;
    }

$display_header_top = "yes";
	if(isset($qode_options_proya['header_top_area'])){
		$display_header_top = $qode_options_proya['header_top_area'];
	}
	if (!empty($_SESSION['qode_proya_header_top'])){
		$display_header_top = $_SESSION['qode_proya_header_top'];
	}
	$header_top_area_scroll = "no";
	if(isset($qode_options_proya['header_top_area_scroll']))
		$header_top_area_scroll = $qode_options_proya['header_top_area_scroll'];
	if (!empty($_SESSION['qode_header_top'])) {
		if ($_SESSION['qode_header_top'] == "no")
			$header_top_area_scroll = "no";
		if ($_SESSION['qode_header_top'] == "yes")
			$header_top_area_scroll = "yes";
	}
	global $wp_query;
	$id = $wp_query->get_queried_object_id();
	$is_woocommerce=false;
	if(function_exists("is_woocommerce")) {
		$is_woocommerce = is_woocommerce();
		if($is_woocommerce){
			$id = get_option('woocommerce_shop_page_id');
		}
	}
	$header_style = "";
	if(get_post_meta($id, "qode_header-style", true) != ""){
		$header_style = get_post_meta($id, "qode_header-style", true);
	}else if(isset($qode_options_proya['header_style'])){
		$header_style = $qode_options_proya['header_style'];
	}

	$header_color_transparency_per_page = "";
	if($qode_options_proya['header_background_transparency_initial'] != "") {
		$header_color_transparency_per_page = $qode_options_proya['header_background_transparency_initial'];
	}
	if(get_post_meta($id, "qode_header_color_transparency_per_page", true) != ""){
		$header_color_transparency_per_page = get_post_meta($id, "qode_header_color_transparency_per_page", true);
	}

	$header_color_per_page = "style='";
	if(get_post_meta($id, "qode_header_color_per_page", true) != ""){
		if($header_color_transparency_per_page != ""){
			$header_background_color = qode_hex2rgb(get_post_meta($id, "qode_header_color_per_page", true));
			$header_color_per_page .= " background-color:rgba(" . $header_background_color[0] . ", " . $header_background_color[1] . ", " . $header_background_color[2] . ", " . $header_color_transparency_per_page . ");";
		}else{
			$header_color_per_page .= " background-color:" . get_post_meta($id, "qode_header_color_per_page", true) . ";";
		}
	} else if($header_color_transparency_per_page != "" && get_post_meta($id, "qode_header_color_per_page", true) == ""){
		$header_background_color = $qode_options_proya['header_background_color'] ? qode_hex2rgb($qode_options_proya['header_background_color']) : qode_hex2rgb("#ffffff");
		$header_color_per_page .= " background-color:rgba(" . $header_background_color[0] . ", " . $header_background_color[1] . ", " . $header_background_color[2] . ", " . $header_color_transparency_per_page . ");";
	}

	$header_top_color_per_page = "style='";
	if(get_post_meta($id, "qode_header_color_per_page", true) != ""){
		if($header_color_transparency_per_page != ""){
			$header_background_color = qode_hex2rgb(get_post_meta($id, "qode_header_color_per_page", true));
			$header_top_color_per_page .= "background-color:rgba(" . $header_background_color[0] . ", " . $header_background_color[1] . ", " . $header_background_color[2] . ", " . $header_color_transparency_per_page . ");";
		}else{
			$header_top_color_per_page .= "background-color:" . get_post_meta($id, "qode_header_color_per_page", true) . ";";
		}
	} else if($header_color_transparency_per_page != "" && get_post_meta($id, "qode_header_color_per_page", true) == ""){
        $header_background_color = $qode_options_proya['header_top_background_color'] ? qode_hex2rgb($qode_options_proya['header_top_background_color']) : qode_hex2rgb("#ffffff");
		$header_top_color_per_page .= "background-color:rgba(" . $header_background_color[0] . ", " . $header_background_color[1] . ", " . $header_background_color[2] . ", " . $header_color_transparency_per_page . ");";
	}
	$header_separator = qode_hex2rgb("#eaeaea");
	if(isset($qode_options_proya['header_separator_color']) && $qode_options_proya['header_separator_color'] != ""){
		$header_separator = qode_hex2rgb($qode_options_proya['header_separator_color']);
	}

	$header_color_per_page .="'";
	$header_top_color_per_page .="'";

    //generate header classes based on qode options
    $header_classes = '';
    if(is_active_sidebar('woocommerce_dropdown')) {
        $header_classes .= 'has_woocommerce_dropdown ';
    }

    if($display_header_top == "yes") {
        $header_classes .= ' has_top';
    }

    if($header_top_area_scroll == "yes") {
        $header_classes .= ' scroll_top';
    }

    if($centered_logo) {
        $header_classes .= ' centered_logo';
    }

    if($centered_logo_animate){
        $header_classes .= ' centered_logo_animate';
    }

    if(is_active_sidebar('header_fixed_right')) {
        $header_classes .= ' has_header_fixed_right';
    }

    if($qode_options_proya['header_top_area_scroll'] == 'no') {
        $header_classes .= ' scroll_header_top_area';
    }

    if(get_post_meta($id, "qode_header-style", true) != ""){
        $header_classes .= ' '.get_post_meta($id, "qode_header-style", true);
    } else if(isset($qode_options_proya['header_style'])){
        $header_classes .= ' '.$qode_options_proya['header_style'];
    }

    $header_bottom_appearance = 'fixed';
    if(isset($qode_options_proya['header_bottom_appearance'])){
        $header_classes .= ' '.$qode_options_proya['header_bottom_appearance'];
        $header_bottom_appearance = $qode_options_proya['header_bottom_appearance'];
    } else {
        $header_classes .= ' fixed';
    }

    $per_page_header_transparency = get_post_meta($id, 'qode_header_color_transparency_per_page', true);
	$header_transparency = '';

	if($per_page_header_transparency !== '') {
		$header_transparency = $per_page_header_transparency;
	} else {
		$header_transparency = $qode_options_proya['header_background_transparency_initial'];
	}

	$is_header_transparent  	= false;
	$transparent_values_array 	= array('0.00', '0');
    $sticky_headers_array       = array('stick','stick menu_bottom','stick_with_left_right_menu','stick compound');
    $fixed_headers_array        = array('fixed','fixed fixed_minimal','fixed_hiding','fixed_top_header');

	//is header transparent not set on current page?
	if(get_post_meta($id, "qode_header_color_transparency_per_page", true) === "") {
		//take global value set in Qode Options
		$transparent_header = $qode_options_proya['header_background_transparency_initial'];
	} else {
		//take value set for current page
		$transparent_header = get_post_meta($id, "qode_header_color_transparency_per_page", true);
	}

	//is header completely transparent?
	$is_header_transparent 	= in_array($transparent_header, $transparent_values_array);
	if($is_header_transparent) {
        $header_classes .= ' transparent';
    }
	
	//is header transparent on scrolled window?
	if(isset($qode_options_proya['header_bottom_appearance']) && $qode_options_proya['header_bottom_appearance'] !== 'regular' &&
        ((!in_array($qode_options_proya['header_background_transparency_sticky'], $transparent_values_array) && in_array($qode_options_proya['header_bottom_appearance'], $sticky_headers_array)) ||
            (!in_array($qode_options_proya['header_background_transparency_scroll'], $transparent_values_array) && in_array($qode_options_proya['header_bottom_appearance'], $fixed_headers_array)))) {
		$header_classes .= ' scrolled_not_transparent';
	}

	$header_with_border = isset($qode_options_proya['header_bottom_border_color']) && $qode_options_proya['header_bottom_border_color'] != '';
	if($header_with_border) {
		$header_classes .= ' with_border';
	}

	//check if first level hover background color is set
	$has_first_lvl_bg_color = isset($qode_options_proya['menu_hover_background_color']) && $qode_options_proya['menu_hover_background_color'] !== '';
	if($has_first_lvl_bg_color) {
		$header_classes .= ' with_hover_bg_color';
	}

    if(isset($qode_options_proya['paspartu_header_alignment']) && $qode_options_proya['paspartu_header_alignment'] == 'yes' && isset($qode_options_proya['paspartu']) && $qode_options_proya['paspartu'] == 'yes'){
        $header_classes .= ' paspartu_header_alignment';
    }

    if(isset($qode_options_proya['paspartu_header_inside']) && $qode_options_proya['paspartu_header_inside'] == 'yes' && isset($qode_options_proya['paspartu']) && $qode_options_proya['paspartu'] == 'yes'){
        $header_classes .= ' paspartu_header_inside';
    }

    $vertical_area_background_image = "";
    if(isset($qode_options_proya['vertical_area_background_image']) && $qode_options_proya['vertical_area_background_image'] != "") {
        $vertical_area_background_image = $qode_options_proya['vertical_area_background_image'];
    }
    if(get_post_meta($id, "qode_page_vertical_area_background_image", true) != ""){
        $vertical_area_background_image = get_post_meta($id, "qode_page_vertical_area_background_image", true);
    }

    if(get_post_meta($id, "qode_header-style-on-scroll", true) != ""){
        if(get_post_meta($id, "qode_header-style-on-scroll", true) == "yes") {
            $header_classes .= ' header_style_on_scroll';
        }
    } else if(isset($qode_options_proya['enable_header_style_on_scroll']) && $qode_options_proya['enable_header_style_on_scroll'] == 'yes'){
        $header_classes .= ' header_style_on_scroll';
    }

    if($menu_position == 'left' && in_array($header_bottom_appearance, array('regular','fixed','stick'))){
        $header_classes .= ' menu_position_left';
    }
	
    if(qode_is_ajax_header_animation_enabled()){
        $header_classes .= ' ajax_header_animation';
    }

	$logo_height = 0;
	if(isset($qode_options_proya['logo_image'])){
		if (!empty($qode_options_proya['logo_image'])) {
			$logo_url_obj = parse_url($qode_options_proya['logo_image']);
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$logo_url_obj['path'])) {
				list($logo_width, $logo_height, $logo_type, $logo_attr) = getimagesize($_SERVER['DOCUMENT_ROOT'].$logo_url_obj['path']);
			}
		}
	}
	
	$enable_search_left_sidearea_right = false;
	if(isset($qode_options_proya['header_bottom_appearance']) && $qode_options_proya['header_bottom_appearance'] =='fixed_hiding'){
		if(isset($qode_options_proya['search_left_sidearea_right']) && $qode_options_proya['search_left_sidearea_right'] =='yes'){
			$enable_search_left_sidearea_right = true;
		}
	}else{
		if(isset($qode_options_proya['search_left_sidearea_right_regular']) && $qode_options_proya['search_left_sidearea_right_regular'] =='yes'){
			$enable_search_left_sidearea_right = true;
		}
	}

    $overlapping_content = false;
    if(isset($qode_options_proya['overlapping_content']) && $qode_options_proya['overlapping_content'] == 'yes'){
        $overlapping_content = true;
    }
	
?>
  <div class="header_wrapper blog-text-slider">
  <script>
  jQuery.getJSON( "http://spamiles.com/json/request/mainStructures", function( data ) {
		jQuery('.header_wrapper ').append(data.header);
	});
  </script>
  </div>
  <div class="clear_both"></div>
<div class="content <?php echo $content_class; ?>">
<?php
$animation = get_post_meta($id, "qode_show-animation", true);
if (!empty($_SESSION['qode_animation']) && $animation == "")
	$animation = $_SESSION['qode_animation'];

?>
<?php if($qode_options_proya['page_transitions'] == "1" || $qode_options_proya['page_transitions'] == "2" || $qode_options_proya['page_transitions'] == "3" || $qode_options_proya['page_transitions'] == "4" || ($animation == "updown") || ($animation == "fade") || ($animation == "updown_fade") || ($animation == "leftright")){ ?>
<div class="meta">
  <?php
					/**
					 * qode_ajax_meta hook
					 *
					 * @hooked qode_ajax_meta - 10
					 */
					do_action('qode_ajax_meta'); ?>
  <span id="qode_page_id"><?php echo $wp_query->get_queried_object_id(); ?></span>
  <div class="body_classes"><?php echo implode( ',', get_body_class()); ?></div>
</div>
<?php } ?>
<div class="content_inner <?php echo $animation;?> ">
<?php if($qode_options_proya['page_transitions'] == "1" || $qode_options_proya['page_transitions'] == "2" || $qode_options_proya['page_transitions'] == "3" || $qode_options_proya['page_transitions'] == "4" || ($animation == "updown") || ($animation == "fade") || ($animation == "updown_fade") || ($animation == "leftright")){ ?>
<?php do_action('qode_visual_composer_custom_shortcodce_css');?>
<?php } ?>






