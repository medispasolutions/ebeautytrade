<?php 
header('Content-Type: application/json');
require("../../../../wp-config.php");
require("../../../../wp-load.php");
$limit = 4;
if(!empty($_GET['limit'])){
	$limit = $_GET['limit'];	
}
$args =  array ( 'orderby' => 'date', 'order' => 'DESC',  'post_status' => 'publish','posts_per_page'   => $limit);
$posts = get_posts( $args );
$items = array();
$item_size = "json_size";
foreach ($posts as $post) {
	setup_postdata( $post ); 
	$thumbnail_id = get_post_thumbnail_id( $post_id );
	$thumbnail = wp_get_attachment_image_src( $thumbnail_id , $item_size );
 	$items[] = array( 'id' => get_the_ID(),'title'=> get_the_title(),'raw_date'  => get_the_date('Y-m-d'), 'formatted_date'  => get_the_date() .' '.get_the_time() , 'image'  => $thumbnail[0],'link'  => get_the_permalink(), 'description' => get_the_content());
 }
wp_reset_postdata();
//print "<pre>"; var_dump( $items ); print "</pre>";
$json =  json_encode($items);
print $json;
