<?php 
global $qode_options_proya;
$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments'])) {
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];
}
$qode_like = "on";
if (isset($qode_options_proya['qode_like'])) {
	$qode_like = $qode_options_proya['qode_like'];
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--date-->
<div class="post_info"> <span class="time">
        <?php the_time('d F, Y'); ?>
        </span>
        <?php if($blog_hide_comments != "yes"){ ?>
        / <a class="post_comments" href="<?php comments_link(); ?>" target="_self">
        <?php comments_number('0 ' . __('Comments','qode'), '1 '.__('Comment','qode'), '% '.__('Comments','qode') ); ?>
        </a>
        <?php } ?>
      </div>
<!--date-->
<!--title-->
<h5><a href="<?php the_permalink(); ?>" target="_self" title="<?php the_title_attribute(); ?>">
        <?php the_title(); ?>
        </a></h5> 
<!--title-->  
<!--image--> 
  <?php if ( has_post_thumbnail() ) { ?>
  <div class="post_image"> <a href="<?php the_permalink(); ?>" target="_self" title="<?php the_title_attribute(); ?>">
    <?php the_post_thumbnail('full'); ?>
    </a> </div>
  <?php } ?>
<!--image--> 
<!--description-->
  <div class="post_text">
    <div class="post_text_inner">
      <?php qode_excerpt(); ?>
       <a href="<?php the_permalink(); ?>" target="_self" title="<?php the_title_attribute(); ?>">Read More</a>
    </div>
  </div>
<!--description-->
</article>
