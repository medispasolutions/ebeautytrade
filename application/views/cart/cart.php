<?php
$login = checkUserIfLogin();
$settings = $this->fct->getonerow("settings", array("id_settings" => 1));
if ($this->router->class == 'cart' && $this->router->method == 'checkout' || (isset($section) && $section == "checkout")) {
    $section = 'checkout';
} else {
    $section = 'cart';
}
/*$user_miles=0;   
if(checkUserIfLogin()){
$user=$this->ecommerce_model->getUserInfo();
$user_miles=$user['miles'];
}*/
?>

<section class="cart <?php if ($section == 'cart') echo "innerContent"; else echo " innerContentCheckout"; ?>">
    <?php if ($section == 'checkout') { ?>
        <!--<a class="back" href="<?php echo route_to('cart'); ?>">Back</a>-->
    <?php } ?>
    <?php if ($section == 'cart') { ?>
        <div class="page-title border">
            <h1><i class="icon"> <img src="<?php echo base_url(); ?>front/img/cart.png"> </i> Shopping Cart </h1>
        </div>
    <?php } ?>

    <?php
    if (!empty($products)) {
        ?>
        <?php if ($section == 'checkout') { ?>
            <div class="edit-cart"><a href="<?php echo route_to('cart#Edit'); ?>">[Edit Cart]</a></div>
        <?php } ?>
        <div class="shopping_cart_cont" id="Edit">
            <div class="table-b">
                <div class="n-table">
                    <?php if ($section == 'cart') { ?>
                    <form id="update_cart_form" method="post" accept-charset="utf-8"
                          action="<?php echo site_url('cart/update_quantity'); ?>">
                        <?php } ?>
                        <input type="hidden" name="action_cart" id="action_cart" value=""/>
                        <input type="hidden" name="id_supplier" id="id_supplier" value=""/>
                        <div class="row" id="shopping-cart-cont">
                            <?php
                            $data['section'] = $section;
                            ?>
                            <?php $this->load->view('blocks/cart', $data); ?>
                            <?php if ($section == 'cart') { ?>
                        </div>
                        <div id="loader-bx"></div>
                        <div class="FormResult"></div>
                    </form>
                <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($login) { ?>
        <div class="row" id="total_price_cont">
            <?php $data['section'] = 'cart'; ?>
            <?php echo $this->load->view('blocks/total_price_cont', $data); ?>
        </div>
    <?php } ?>
</section>