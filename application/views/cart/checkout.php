<div class="row cartTable">
    <div class="cartRow">
        <div class="cartCell head product_name"><?php echo lang('cart_product_name'); ?></div>
        <div class="cartCell head product_id"><?php echo lang('cart_product_price'); ?></div>
        <div class="cartCell head product_quantity"><?php echo lang('cart_product_quantity'); ?></div>
        <div class="cartCell head total_price"><?php echo lang('cart_total_price'); ?></div>
        <div class="cartCell head cart_remove"><?php echo lang('cart_remove'); ?></div>
    </div>

    <?php if (!empty($products)) { ?>
        <form action="<?php echo route_to('cart/update_quantity'); ?>" method="post" id="update_cart_form">
            <?php
            $net_price = 0;
            foreach ($products as $pro) {
                ?>
                <input type="hidden" name="product_id[]" value="<?php echo $pro['product_info']['id_products']; ?>"/>
                <input type="hidden" name="product_rowid[]" value="<?php echo $pro['rowid']; ?>"/>
                <div class="cartRow">
                    <div class="cartCell product_name">
                        <?php if (!empty($pro['product_info']['gallery'])) { ?>
                            <a href="<?php echo route_to('products/details/' . $pro['product_info']['title_url' . getUrlFieldLanguage($lang)]); ?>">
                                <img align="top" width="50"
                                     src="<?php echo base_url(); ?>uploads/products/gallery/120x120/<?php echo $pro['product_info']['gallery'][0]['image']; ?>"
                                     title="<?php echo $pro['product_info']['title' . getFieldLanguage($lang)]; ?>"
                                     alt="<?php echo $pro['product_info']['title' . getFieldLanguage($lang)]; ?>"/>
                            </a>
                        <?php } ?>
                        <a href="<?php echo route_to('products/details/' . $pro['product_info']['title_url' . getUrlFieldLanguage($lang)]); ?>"><?php echo $pro['product_info']['title' . getFieldLanguage($lang)]; ?></a>
                    </div>
                    <div class="cartCell product_id"><?php echo changeCurrency($pro['product_info']['price']); ?></div>
                    <div class="cartCell product_quantity"><input type="text" class="cartQtyTextBox"
                                                                  name="product_quantity[]"
                                                                  value="<?php echo $pro['qty']; ?>"/></div>
                    <div class="cartCell total_price">
                        <?php
                        $total_price = $pro['product_info']['price'] * $pro['qty'];
                        $net_price = $net_price + $total_price;
                        echo changeCurrency($total_price);
                        ?>
                    </div>
                    <div class="cartCell cart_remove">
                        <a href="<?php echo route_to('cart/remove/' . $pro['rowid']); ?>"><img
                                    src="<?php echo base_url(); ?>images/delete.png"/>
                        </a></div>
                </div>
            <?php } ?>
        </form>
    <?php } else { ?>
        <div class="cartRow centerText">
            <?php echo lang('empty_cart'); ?>
        </div>
    <?php } ?>
</div>
<?php if (!empty($products)) { ?>
    <div class="clear"></div>
    <div class="row">
        <div class="cartNetPrice"></span><?php echo lang('cart_net_price'); ?>
            :</span> <?php echo changeCurrency($net_price); ?></div>
        <a onclick="$('#update_cart_form').submit();"
           class="cartQtySubmit link"><?php echo lang('update_quantity'); ?></a>

        <a href="<?php echo str_replace("http://", "https://", route_to('cart/checkout')); ?>"
           class="cartQtySubmit link"><?php echo lang('checkout'); ?></a>
    </div>
<?php } ?>
<a href="<?php echo route_to('cart/notice'); ?>" class="colorbox" id="checkoutNotice" style="display:none;"></a>
