<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Shopping Cart</title>
	<meta charset="utf-8">
    <!-- Favicon --> 
	<link rel="shortcut icon" href="<?php echo base_url(); ?>front/images/favicon.ico">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>front/css/style.css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-1.10.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.PrintArea.js"></script>
	<script type="text/javascript">
    	function calculateIDPos(){
			var WindowWidth = $(window).width();
			var RightPos = ((WindowWidth - 600) / 2) - 80;
			$('#printButton').css('right',RightPos); 
		}
		$(document).ready(function(){
			calculateIDPos();
			$('#printButton').click(function(){
				$('#printThis').printArea();
			});
		});
		$(window).bind("load",function(){
			calculateIDPos();
		});
    </script>
   
</head>
<body>
<input type="submit" style="position:absolute;" class="printButton btn" id="printButton" value="<?php echo lang("print"); ?>" />
<div class="printing-container" id="printThis" style="width:1140px; margin:0 auto">
<?php 
	$section = 'checkout';
?>
<img style="margin-top:15px;" src="<?php echo base_url();?>front/img/logo.png">
<h1 class="h1_title">Shopping Cart</h1>

<div class="shopping_cart">

<table class="shopping_cart_table"  border=0>
<tr>
<?php if($section == 'cart') {?>
   	<th>Remove</th>
<?php } ?>

<th>Item Code</th>
<th class="product_cart_details">Item</th>
<th>Price</th>
<th style="text-align:center;">Quantity</th>
<th>Delivery Date</th>
<th>Total</th>
</tr>
                            
<?php 
$net_price = 0;
$net_discount_price = 0;
if(!empty($products)) {?>
<?php 
$net_price = 0;
$net_discount_price = 0;
foreach($products as $product) {
$rra_p=array();
$arr_p['id_products']=$product['product_info']['id_products'];
$arr_p['list_price']=$product['product_info']['list_price'];
$arr_p['price']=$product['product_info']['price'];
$arr_p['qty']=$product['qty'];
$arr_p['discount_product']=$product['product_info']['discount'];
$arr_p['discount_product_expiration']=$product['product_info']['discount_expiration'];
$arr_p['withSegment']="true";

	 $prices=$this->ecommerce_model->getPrices($arr_p);
	 $n_price=$prices['n_price'];
	 $o_price=$prices['o_price'];

	?>
    
<input type="hidden" name="product_id[]" value="<?php echo $product['product_info']['id_products']; ?>" />
<input type="hidden" name="product_rowid[]" value="<?php echo $product['rowid']; ?>" />
<tr>
<?php if($section == 'cart') {?>
<td><input type="checkbox" name="arr_remove_product[]" /></td>
<?php } ?>
<td><?php echo $product['product_info']['sku'];?></td>
<td>

<div class="product_img_cart">
<img src="<?php echo base_url();?>uploads/products/114x165/<?php echo $product['product_info']['image'] ;  ?>" />
</div>
<div class="product_info_cart">
<div class="product_info_cart_title"><?php echo $product['product_info']['title'];?></div>
<div class="lbl_product_info">Size: </div>
<div class="lbl_product_info">Color:</div>
</div>

</td>
<td><?php if(!empty($o_price)){?>
    <div class="old_price">
    <?php echo changeCurrency($o_price);?>
    </div>
    <?php } ?>
	<?php echo changeCurrency($n_price);?></td>
<td style="text-align:center;">    <?php if($this->router->class == 'cart' && $this->router->method == 'index') {?>
	<span class="btn_qty minus"><img src="<?php echo base_url();?>front/img/minus-qty.png" /></span>
  	    <input type="text" class="cartQtyTextBox" name="product_quantity[]" value="<?php echo $product['qty']; ?>" />
          <span class="btn_qty plus"><img src="<?php echo base_url();?>front/img/plus-qty.png" /></span> 
    <?php } else {?>
   		<?php echo $product['qty']; ?>
    <?php }?></td>
<td><?php echo date('d/m/Y');?></td>
<td>		<?php 
		    $total_price = $product['product_info']['list_price'] * $product['qty'];
			$total_discount_price = $n_price * $product['qty'];
			$net_price = $net_price + $total_price;
			$net_discount_price = $net_discount_price + $total_discount_price;

		if($total_discount_price<$total_price) echo '<span class="old_price">'.changeCurrency($total_price).'</span>';
			echo '<div>'.changeCurrency($total_discount_price).'</div>'; 
		?></td>
</tr>
<?php }} ?>
</table>


</div>
</div>
</body>
</html>















