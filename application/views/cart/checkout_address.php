<?php
$login = checkUserIfLogin();
if (isset($userData)) {
} else {
    $userData['first_name'] = "";
    $userData['last_name'] = "";
    $userData['phone'] = "";
    $userData['company_name'] = "";
}

if ($login) {

} else {
    $userData['first_name'] = "";
    $userData['address_1'] = "";
    $userData['address_2'] = "";
    $userData['last_name'] = "";
    $userData['company'] = '';
    $userData['phone'] = '';
    $userData['mobile'] = '';
    $userData['city'] = '';
    $userData['fax'] = '';
    $userData['street_one'] = '';
    $userData['street_two'] = '';
    $userData['state'] = '';
    $address['city'] = '';
    $userData['country'] = '';
    $userData['id_countries'] = '';
    $userData['postal_code'] = '';
    $userData['billing'] = '';
    $userData['shipping'] = '';
    $userData['username'] = "";
}
?>

<div class="innerContent checkout account-create <?php if (!checkUserIfLogin()) {
    echo " login_false";
} ?>">
    <div class="page-title">
        <?php $n = 0; ?>
        <div class="page-title border">
            <h1>
                <i class="icon">
                    <img src="<?php echo base_url(); ?>front/img/cart.png">
                </i>
                Checkout
            </h1>
        </div>
    </div>
    <form method="post" id="CheckoutForm" action="<?php echo route_to('cart/checkout'); ?>">
        <ol id="checkoutSteps" class="opc <?php if (!$login) echo " opc-mod"; ?>">
            <div class="col-md-9">
                <!--STEP 1-->
                <li id="opc-billing" class="section active_step">
                    <div class="step-title">
                        <span class="number"><?php echo ++$n; ?></span>
                        <h2>Billing Address</h2>
                        <a class="edit_register" id="edit_register_<?php echo $n; ?>">Edit</a>
                        <a class="edit_register add_address" id="" onclick="addNewAddress('billing')">ADD</a>
                    </div>
    
                    <div id="checkout-step-billing" class="step a-item" style="">
                        <?php if (checkUserIfLogin()) { ?>
                            <div class="row_form">
                                <label class="checkout_label">Select one Billing Address.</label>
                                <div class="form-item">
                                    <div class="row_addresses req_c" id="billing_addresses_bxs">
                                        <?php if (!empty($user_addresses_billing)) {
                                            $i = 0;
                                            foreach ($user_addresses_billing as $address) {
                                                $i++;
                                                $d['address'] = $address;
                                                $d['pickup'] = 1;
                                                $d['formType'] = 'billing'; ?>
                                                <?php $this->load->view('blocks/address_box', $d); ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <input type="hidden" name="billing_select" value=""/>
                                        <span id="billing_select-error" class="form-error"></span>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div id="billing_form" <?php if (empty($user_addresses_billing)) echo " style='display:block'"; ?> >
                                <!--ROW 1-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required"> First Name <em>*</em> </label>
                                        <input class="textbox" type="text" name="billing_first_name"
                                               value="<?php echo $userData['first_name']; ?>">
                                        <span id="billing_first_name-error" class="form-error"></span></div>
                                    <div class="form-item gutter-right">
                                        <label class="required"> Last Name <em>*</em> </label>
                                        <input class="textbox" type="text" name="billing_last_name"
                                               value="<?php echo $userData['last_name']; ?>">
                                        <span id="billing_last_name-error" class="form-error"></span>
                                    </div>
                                </div>
    
                                <!--ROW 2-->
                                <div class="row_form ">
                                    <div class="form-item gutter-right">
                                        <label class="required" for="email"> Email Address <em>*</em> </label>
                                        <input class="textbox" type="text" name="billing_email" value="">
                                        <span id="billing_email-error" class="form-error"></span>
                                    </div>
                                    <div class="form-item gutter-right">
                                        <label class="required"> Country <em>*</em> </label>
                                        <select id="country" name="billing_country" class="textbox selectbox"
                                                title="Country">
                                            <option value="">--Select Country--</option>
                                            <?php foreach ($countries as $country) { ?>
                                                <option <?php if ($country['id_countries'] == $userData['id_countries']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $country['id_countries']; ?>"><?php echo $country['title']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span id="billing_country-error" class="form-error"></span>
                                    </div>
                                </div>
    
                                <!--ROW 3-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required">Company </label>
                                        <input class="textbox" type="text" name="billing_company" value="">
                                        <span id="billing_company-error" class="form-error"></span></div>
                                    <div class="form-item gutter-right">
                                        <label class="required">Address <em>*</em> </label>
                                        <input class="textbox" type="text" name="billing_street_one" value="">
                                        <span id="billing_street_one-error" class="form-error"></span>
                                        <input class="textbox" type="text" name="billing_street_two" style="margin-top:5px;"
                                               value="<?php echo $userData['address_2']; ?>">
                                    </div>
                                </div>
    
                                <!--ROW 4-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required"> City <em>*</em> </label>
                                        <input class="textbox" type="text" name="billing_city"
                                               value="<?php echo $userData['city']; ?>">
                                        <span id="billing_city-error" class="form-error"></span></div>
                                    <div class="form-item gutter-right">
                                        <label class="required"> State/Province</label>
                                        <input class="textbox" type="text" name="billing_state"
                                               value="<?php echo $userData['state']; ?>">
                                        <span id="billing_state-error" class="form-error"></span></div>
                                </div>
    
                                <!--ROW 6-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required"> Telephone <em>*</em> </label>
                                        <!--<input class="textbox" type="text" name="billing_phone" value="<?php echo $userData['phone']; ?>" >-->
                                        <input class="textbox phone_1" type="text" maxlength="4"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Zip Code" style="width:50px; margin-left:2px;" name="billing_phone[0]"
                                               value="">
                                        <input class="textbox phone_2" type="text" maxlength="4"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Area Code" style="width:50px; margin-left:2px;"
                                               name="billing_phone[1]" value="">
                                        <input class="textbox phone_3" type="text" maxlength="10"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Phone Number" style="width:150px; margin-left:2px;"
                                               name="billing_phone[2]" value="">
                                        <span id="billing_phone-error" class="form-error"></span>
                                    </div>
                                    <div class="form-item gutter-right">
                                        <label class="required"> Fax </label>
                                        <input class="textbox phone_1" type="text" maxlength="4"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Zip Code" style="width:50px; margin-left:2px;" name="billing_fax[0]"
                                               value="">
                                        <input class="textbox phone_2" type="text" maxlength="4"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Area Code" style="width:50px; margin-left:2px;" name="billing_fax[1]"
                                               value="">
                                        <input class="textbox phone_3" type="text" maxlength="10"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Phone Number" style="width:150px; margin-left:2px;"
                                               name="billing_fax[2]" value="">
    
                                        <span id="fax_phone-error" class="form-error"></span>
                                    </div>
                                </div>
    
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required"> Mobile </label>
                                        <input class="textbox phone_1" type="text" maxlength="4"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Zip Code" style="width:50px; margin-left:2px;"
                                               name="billing_mobile[0]" value="">
                                        <input class="textbox phone_2" type="text" maxlength="4"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Area Code" style="width:50px; margin-left:2px;"
                                               name="billing_mobile[1]" value="">
                                        <input class="textbox phone_3" type="text" maxlength="10"
                                               onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                               title="Phone Number" style="width:150px; margin-left:2px;"
                                               name="billing_mobile[2]" value="">
                                        <span id="billing_mobile-error" class="form-error"></span></div>
    
                                    <div class="form-item gutter-right">
                                        <label class="required"> Zip/Postal Code </label>
                                        <input class="textbox" type="text" name="billing_postal_code">
                                        <span id="billing_postal_code-error" class="form-error"></span></div>
                                </div>
                                <!--ROW 7-->
    
                                <!--ROW 8-->
                                <div class="row_form save_address">
                                    <?php if (checkUserIfLogin()) { ?>
                                        <div class="form-item mod ">
                                            <input class="checkbox validation-passed " type="checkbox" name="save_billing"
                                                   title="Save in address book">
                                            <label>Save in address book</label>
                                            <span id="is_subscribed-error" class="form-error"></span>
                                        </div>
                                    <?php } ?>
    
                                </div>
                            </div>
                        <?php } ?>
                        <div class="buttons-set">
                            <div class="loader-bx"></div>
                            <div class="FormResult"></div>
                            <div class="form-item row aramex">
                                <input class="hidden" type="hidden" name="aramex" value="">
                                <span id="aramex-error" class="form-error"></span></div>
    
                            <a style="float:right;" title="Continue" onclick="stepTwo()" class="btn">Continue</a>
                        </div>
                    </div>
                </li>
                <!--STEP 2-->
                <li id="opc-shipping" class="section">
                    <div class="step-title">
                        <span class="number"><?php echo ++$n; ?></span>
                        <h2><span id="ship_tab">Shipping Address</span></h2>
                        <a class="edit_register" id="edit_register_<?php echo $n; ?>">Edit</a>
                        <a class="edit_register add_address" id="" onclick="addNewAddress('shipping')">ADD</a>
                    </div>
    
                    <div id="checkout-step-delivery" class="step a-item" style="">
                        <div id="shipping_information">
    
                            <div id="delivery_form" style="display:block;">
                                <!--ROW 1-->
                                <label class="checkout_label">Select a shipping address from your address book.</label>
                                <div class="row_addresses req_c" id="shipping_addresses_bxs">
    
                                    <?php if (checkUserIfLogin()) { ?>
                                        <?php if (!empty($user_addresses_delivery)) {
                                            $i = 0;
                                            foreach ($user_addresses_delivery as $address) {
                                                $i++;
                                                $d['address'] = $address;
                                                $d['formType'] = 'shipping';
                                                $d['pickup'] = 1;
                                                ?>
    
                                                <?php $this->load->view('blocks/address_box', $d); ?>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if (!empty($pickup_addresses)) {
                                            $i = 0;
                                            foreach ($pickup_addresses as $address) {
                                                $i++;
                                                $dd['address'] = $address;
                                                $dd['pickup'] = 2;
                                                $dd['formType'] = 'shipping';
                                                ?>
                                                <?php $this->load->view('blocks/address_box', $dd); ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div id="delivery_form" <?php if (empty($user_addresses_delivery)) echo " style='display:block'"; ?> >
                                            <!--ROW 1-->
                                            <div class="row_form">
                                                <div class="form-item gutter-right">
                                                    <label class="required"> First Name <em>*</em> </label>
                                                    <input class="textbox" type="text" name="delivery_first_name"
                                                           value="<?php echo $userData['first_name']; ?>">
                                                    <span id="delivery_first_name-error" class="form-error"></span></div>
                                                <div class="form-item gutter-right">
                                                    <label class="required"> Last Name <em>*</em> </label>
                                                    <input class="textbox" type="text" name="delivery_last_name"
                                                           value="<?php echo $userData['last_name']; ?>">
                                                    <span id="delivery_last_name-error" class="form-error"></span></div>
                                            </div>
    
                                            <!--ROW 2-->
                                            <!--<div class="row_form">
                                            <div class="form-item">
                                                      <label class="required" >Company</label>
                                                      <input class="textbox" type="text" name="delivery_company" >
                                                      <span id="delivery_company-error" class="form-error"></span> </div>     
                                            </div>-->
    
                                            <!--ROW 3-->
                                            <div class="row_form">
                                                <div class="form-item gutter-right">
                                                    <label class="required"> Telephone <em>*</em> </label>
                                                    <input class="textbox phone_1" type="text" maxlength="4"
                                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                           title="Zip Code" style="width:50px; margin-left:2px;"
                                                           name="delivery_phone[0]" value="">
                                                    <input class="textbox phone_2" type="text" maxlength="4"
                                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                           title="Area Code" style="width:50px; margin-left:2px;"
                                                           name="delivery_phone[1]" value="">
                                                    <input class="textbox phone_3" type="text" maxlength="10"
                                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                           title="Phone Number" style="width:150px; margin-left:2px;"
                                                           name="delivery_phone[2]" value="">
                                                    <span id="delivery_phone-error" class="form-error"></span></div>
                                                <div class="form-item gutter-right">
                                                    <label class="required">Address <em>*</em> </label>
                                                    <input class="textbox" type="text" name="delivery_street_one"
                                                           value="<?php echo $userData['address_1']; ?>">
                                                    <span id="delivery_street_one-error" class="form-error"></span>
                                                    <input class="textbox" type="text" name="delivery_street_two"
                                                           style="margin-top:5px;"
                                                           value="<?php echo $userData['address_2']; ?>">
                                                </div>
                                            </div>
    
                                            <!--ROW 4-->
                                            <div class="row_form" id="branch_name">
                                                <div class="form-item gutter-right">
                                                    <label class="required"> Branch Name <em>*</em> </label>
                                                    <input class="textbox" type="text" name="branch_name" value="">
                                                    <span id="branch_name-error" class="form-error"></span></div>
                                            </div>
    
                                            <!--ROW 5-->
                                            <div class="row_form">
                                                <div class="form-item gutter-right">
                                                    <label class="required"> City <em>*</em> </label>
                                                    <input class="textbox" type="text" name="delivery_city"
                                                           value="<?php echo $userData['city']; ?>">
                                                    <span id="delivery_city-error" class="form-error"></span></div>
                                                <div class="form-item gutter-right">
                                                    <label class="required"> State/Province </label>
                                                    <input class="textbox" type="text" name="delivery_state"
                                                           value="<?php echo $userData['state']; ?>">
                                                    <span id="delivery_state-error" class="form-error"></span></div>
                                            </div>
    
                                            <!--ROW 6-->
                                            <div class="row_form">
    
                                                <div class="form-item gutter-right">
                                                    <label class="required"> Country <em>*</em> </label>
                                                    <select id="country" name="delivery_country" class="textbox selectbox"
                                                            title="Country">
                                                        <option value="">--Select Country--</option>
                                                        <?php foreach ($countries as $country) { ?>
                                                            <option <?php if ($country['id_countries'] == $userData['id_countries']) {
                                                                echo "selected";
                                                            } ?> value="<?php echo $country['id_countries']; ?>"><?php echo $country['title']; ?></option>
                                                        <?php } ?>
    
                                                    </select>
                                                    <span id="delivery_country-error" class="form-error"></span></div>
                                            </div>
    
                                            <!--ROW 7-->
                                            <div class="row_form">
                                                <div class="form-item gutter-right">
                                                    <label class="required"> Mobile </label>
                                                    <input class="textbox phone_1" type="text" maxlength="4"
                                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                           title="Zip Code" style="width:50px; margin-left:2px;"
                                                           name="delivery_mobile[0]" value="">
                                                    <input class="textbox phone_2" type="text" maxlength="4"
                                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                           title="Area Code" style="width:50px; margin-left:2px;"
                                                           name="delivery_mobile[1]" value="">
                                                    <input class="textbox phone_3" type="text" maxlength="10"
                                                           onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                           title="Phone Number" style="width:150px; margin-left:2px;"
                                                           name="delivery_mobile[2]" value="">
                                                    <span id="delivery_mobile-error" class="form-error"></span></div>
                                                <div class="form-item gutter-right">
                                                    <label class="required"> Zip/Postal Code </label>
                                                    <input class="textbox" type="text" name="delivery_postal_code">
                                                    <span id="delivery_postal_code-error" class="form-error"></span></div>
                                            </div>

                                            <?php if (checkUserIfLogin()) { ?>
                                                <div class="row_form save_address">
                                                    <div class="form-item mod">
                                                        <input class="checkbox validation-passed " type="checkbox"
                                                               name="save_shipping" title="Save in address book" value="1">
                                                        <label>Save in address book</label>
                                                        <span id="is_subscribed-error" class="form-error"></span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                    <input type="hidden" name="shipping_select" value=""/>
                                    <span id="shipping_select-error" class="form-error"></span>
                                </div>
                            </div>
                            <div id="shippingPrice"></div>
                            <div class="buttons-set">
                                <div class="loader-bx"></div>
                                <div class="FormResult"></div>
                                <div class="form-item row aramex">
                                    <input class="hidden" type="hidden" name="aramex" value="">
                                    <span id="aramex-error" class="form-error"></span></div>
                                <a style="float:right;" title="Continue" onclick="stepThree()" class="btn">Continue</a>
                            </div>
                        </div>
                    </div>
                </li>
                <!--STEP 4-->
                <li id="opc-payment" class="section">
                    <div class="step-title">
                        <span class="number"><?php echo ++$n; ?></span>
                        <h2>Payment Methods</h2>
                        <a class="edit_register" id="edit_register_<?php echo $n; ?>">Edit</a>
                    </div>
                    <div id="checkout-step-billing" class="step a-item" style="">
                        <div class="row_form" id="cash">
                            <div class="form-item mod">
                                <label>
                                <input class="checkbox validation-passed" type="radio" value="checque" title="Cheque"
                                       name="payment_method_r" style="float: left;margin: 4px;">
                                Cheque on Delivery</label>
                                </span>
                            </div>
                            <div class="form-item mod">
                                <label>
                                <input class="checkbox validation-passed" type="radio" value="cash" title="Cash"
                                       name="payment_method_r" style="float: left;margin: 4px;">
                                Cash on Delivery</label>
                                </span>
    
                            </div>
                        </div>
                        <script type="text/javascript" src="https://www.ebeautytrade.com/front/js/jquery-1.10.0.min.js"></script>
                        <script type="application/javascript">      
                        jQuery(document).ready(function () {
                            jQuery('input[type="radio"]').click(function () {
                                if (jQuery(this).attr("value") == "credit_cards") {
                                    alert("Please confirm availability with the Supplier before submitting your payment online.");
                                }
                            });
                            jQuery('#order-products').click(function () {
                                debugger;
                                jQuery('.all-payment').show();
                            });
                        });
                        </script>
                        <div class="row_form">
                            <div class="form-item mod">
                                <label>
                                <input class="checkbox validation-passed" type="radio" value="credit_cards" title="Credit cards" name="payment_method_r" style="float: left;margin: 4px;">
                                Online Payment</label>
                                <span id="payment_method-error" class="form-error"></span>
                            </div>
                        </div>
                        <div class="buttons-set">
                            <div class="loader-bx"></div>
                            <div class="FormResult"></div>
                            <a title="Continue" onclick="stepFour()" id="order-products" class="btn">Continue</a>
                        </div>
                        <input type="hidden" value="" title="Credit cards" name="payment_method">
                    </div>
                </li>
            </div>
            <div class="col-md-3">
                <!--STEP 5-->
                <li id="opc-review" class="section">
                <div class="step-title">
                    <h2>Order Review</h2>
                </div>
                <div id="checkout-review-load" class="order-review">
                    <div id="shoppingCart">
                        <?php $this->load->view('cart/checkbox-cart'); ?>
                    </div>
                    <?php
                    $exportable = false;
                    foreach ($products as $product) {
                        if ($product['product_info']['set_as_clearance'] == 1) {
                            $exportable = true;
                            break;
                        }
                    } ?>

                    <?php if ($exportable) { ?>
                        <input type="hidden" name="exported" value="1" class="exported_1"/>
                    <?php } ?>
                    <div class="form-item mod ">
                        <input type="hidden" name="redeem_miles" value="">
                        <span id="redeem_miles-error" class="form-error"></span>
                    </div>
                    <div class="buttons-set all-payment" style="display:none">
                        <div class="FormResult"></div>
                        <div id="loader-bx"></div>
                        <!--<input class="btn"  type="submit" value="Send" title="Send Order">-->
                        <a class="btn" onclick="checkCheckout()" alt="Submit Order" title="Submit Order"><i
                                    class="fa fa-fw"></i>Place Order</a>
                    </div>
                </div>
            </li>
            </div>
        </ol>
    </form>
    <div id="payment"></div>
</div>