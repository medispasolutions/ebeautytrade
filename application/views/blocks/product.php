<?php

if (!empty($product['brand'])) {
    $title_img = $product['brand']['name'];
} else {
    $title_img = $product['title'];
}
$image = '';

if (isset($product['gallery']) && !empty($product['gallery']))

    $image = $product['gallery'][0];

$attributes = $product['attributes'];

$first_available_stock = array();

if (isset($product['first_available_stock']) && !empty($product['first_available_stock'])) {

    $first_available_stock = $product['first_available_stock'];

}

if (isset($first_available_stock) && !empty($first_available_stock)) {

    $quantity = $first_available_stock['quantity'];
    $product['miles'] = $first_available_stock['miles'];
    $list_price = $first_available_stock['list_price'];
    $price = $first_available_stock['price'];
    $sku = $first_available_stock['sku'];
    $retail_price = $first_available_stock['retail_price'];
    $discount = $first_available_stock['discount'];
    $discount_expiration = $first_available_stock['discount_expiration'];
    $stock_options = unSerializeStock($first_available_stock['combination']);
    $hide_price = $first_available_stock['hide_price'];
    $price_segments = $this->fct->getAll_cond("product_price_segments", 'sort_order', array("id_products" => $first_available_stock['id_products'], "id_stock" => $first_available_stock['id_products_stock']));
    //print '<pre>';print_r($stock_options);exit;

} else {

    $quantity = $product['quantity'];
    $list_price = $product['list_price'];
    $price = $product['price'];
    $sku = $product['sku'];
    $retail_price = $product['retail_price'];
    $discount = $product['discount'];
    $discount_expiration = $product['discount_expiration'];
    $hide_price = $product['hide_price'];
    $price_segments = $this->fct->getAll_cond("product_price_segments", 'sort_order', array("id_products" => $product['id_products']));
}

/////////////MULTI PRICES//////////

$multi_prices = "";

foreach ($price_segments as $segment) {
    $multi_prices .= '' . $segment['min_qty'] . ' for ' . strip_tags(changeCurrency($segment['price'], true)) . ' each &#13;';
}

?>


<div class="product <?php if (isset($trans_delay)) { ?>trans top-trans delay-<?php echo $trans_delay; ?><?php } ?>"
     id="product-list-<?php echo $product['id_products']; ?>">

    <?php if ($product['set_as_new'] == 1) { ?>
        <span class="non-exportable-img"><img src="<?php echo base_url(); ?>front/img/new-arrivals.png"/></span>
    <?php } ?>

    <?php if ($product['set_as_clearance'] == 1) { ?>
        <span class="non-exportable-img"><img src="<?php echo base_url(); ?>front/img/clearance.png"/></span>
    <?php } ?>

    <?php if ($product['set_as_soon'] == 1) { ?>
        <span class="non-exportable-img"><img src="<?php echo base_url(); ?>front/img/soon.png"/></span>
    <?php } ?>

    <?php if ($discount > 0) { ?>
        <span class="non-exportable-img"><img src="<?php echo base_url(); ?>front/img/offers.png"/></span>
    <?php } ?>

    <?php if ($product['set_as_pro'] == 1) { ?>
        <span class="pro-img"><img src="<?php echo base_url(); ?>front/img/pro.png"/></span>
    <?php } ?>
    
    <?php if ($product['set_as_oed'] == 1) { ?>
        <span class="pro-img online-exclusive">Save <?php echo $product['discount']; ?> %</span>
    <?php } ?>

    <?php $product_url = route_to('products/details/' . $product['id_products']); ?>

    <?php if ($page == "miles" && checkUserIfLogin()) {
        $product_url = route_to('products/details/' . $product['id_products']) . '?miles=1';
    } ?>

    <div class="inner-product">

        <?php /*?>     <?php if(!empty($product['redeem_miles']) && $product['set_as_redeemed_by_miles']==1){ ?>
        <div class="miles_title"> <?php echo $product['redeem_miles'].' Miles';?> </div>
        <?php } ?><?php */ ?>
        <span class="product-img">
            <a href="<?php echo $product_url; ?>">

            <? $altt = character_limiter(strip_tags($product['brief']), 150); ?>

                <?php if (!empty($product['gallery'][0]['image'])) { ?>

                    <img class="base_image" title="<?php echo $altt; ?>" alt="<?php echo $altt; ?>"
                         src="<?php echo base_url(); ?>uploads/products/gallery/295x295/<?php echo $product['gallery'][0]['image']; ?>"/>

                <?php } else { ?>

                    <img class="base_image" title="<?php echo $altt; ?>" alt="<?php echo $altt; ?>"
                         src="<?php echo base_url(); ?>front/img/default_product.png"/>

                <?php } ?>

             </a>
        </span>


        <div class="product-content box-sizing">

            <!--<span class="product-title"><a href="<?php echo route_to('products/details/' . $product['id_products']); ?>"><?php echo $product['title']; ?></a></span>-->
            <div class="product-description description mod">
                <?php /*?><?php echo character_limiter(strip_tags($product['brief']),68); ?><?php */ ?>
                <!--  <div class="row"><?php echo $product['brief_one']; ?></div>
                <div class="row"><?php echo $product['brief_two']; ?></div>-->

                <div class="row">
                    <div class="col-sm-12">
                        <a href="<?php echo $product_url; ?>"><?php echo character_limiter(strip_tags($product['brief']), 150); ?></a>
                    </div>
                </div>
            </div>

            <!--<div class="row" style="margin-top:7px;"><label><strong>SKU:</strong> </span><?php echo $sku; ?></label></div> -->
            
            <?php if (!$login && $page != 'miles') { ?>

                <div class="row retail_price">
                    <?php if (!empty($retail_price) && $retail_price > 0) { ?>
                        <!--<label><strong><?php echo lang('suggester_retail_price'); ?>:</strong><?php echo changeCurrency($retail_price); ?></label>-->
                    <?php } ?>
                    <?php if (!$login) { ?>
                        <div class="login-text" style="text-align: center;font-style: normal;margin: 0;"><a class=""
                                                                                         onclick="checkUrlIfLogin('https://www.spamiles.com/')">Login
                                to view price</a></div>
                    <?php } ?>
                </div>
            <?php } ?>
            
            <?php if ($page == "miles" && checkUserIfLogin()) { ?>
                <div class="products_details_2">
                    <label>
                        <div class="p-price">

                            <?php if (!empty($first_available_stock)) {
                                echo '<span class="price price_selected_option">' . lang('miles_based_on_selection') . '</span>';
                            } else { ?>
                                <span class="price new_price">
                                    <?php echo $product['redeem_miles']; ?>
                                    <span class="currency_txt"> Miles</span>
                                </span>
                            <?php } ?>
                        </div>
                    </label>
                </div>
            <?php } ?>

            <?php if ($login || $product['id_group_categories'] != 1) { ?>
                <?php
                ///////////////////////////checkoptions\\\\\\\\\\\\\\\\\\\
                $bool_option = false;
                /*if(!empty($attributes)){
                    foreach($attributes as $attr){
                        if(!empty($attr['options'])){
                            $bool_option=true;}}}*/

                if (!empty($first_available_stock)) {
                    $bool_option = true;
                }
                ?>

                <?php if ($page != "miles" && $login) { ?>
                    <div class="products_details_2">
                        <label><span class="lbl_t">Price: </span>
                            <div class="p-price">
                                <?php if ($bool_option) {
                                    echo '<span class="price price_selected_option">' . lang('based_on_selection') . '</span>';
                                } else { ?>
                                    <?php if ($hide_price == 0) { ?>
                                        <?php
                                        $user = $this->ecommerce_model->getUserInfo();
                                        $rolesUser = $user['id_roles'];
                                        $userID = $user['id_user'];
                                        $parentID = $user['id_parent'];
                                        $productUserID = $product['brand']['id_user'];
                                        ?>
                                        <?php if ($rolesUser == '5' && ($userID == $productUserID || $parentID == $productUserID)) { ?>
                                            <?php if (displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price, $discount_expiration, $price)) { ?>
                                                <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price)); ?></span>
                                                <span class="price old_price"
                                                      itemprop="price"><?php echo changeCurrency(displayWasCustomerPrice($list_price)); ?></span>
                                            <?php } else { ?>
                                                <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price)); ?></span>
                                            <?php } ?>
                                        <?php } else if ($rolesUser != '5') { ?>
                                            <?php if (displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price, $discount_expiration, $price)) { ?>
                                                <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price)); ?></span>
                                                <span class="price old_price"
                                                      itemprop="price"><?php echo changeCurrency(displayWasCustomerPrice($list_price)); ?></span>
                                            <?php } else { ?>
                                                <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price)); ?></span>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php }
                                } ?>
                            </div>
                        </label>
                    </div>
                <?php } ?>

                <form id="cart-form-<?php echo $product['id_products']; ?>" action="<?php echo site_url('cart/add'); ?>"
                      style="display:none;" method="post">
                    <?php if ($page == "miles") { ?>
                        <input type="hidden" name="redeem" value="1"/>
                    <?php } ?>
                    <input type="hidden" name="product_id" value="<?php echo $product['id_products']; ?>"/>
                </form>
            <?php } ?>

            <div class="row ta-r">
                <?php if ($page != "miles" && $product['set_as_redeemed_by_miles'] == 1 && checkUserIfLogin()) { ?>
                    <?php if ($rolesUser == '5' && ($userID == $productUserID || $parentID == $productUserID)) { ?>
                        <span class="smily-face smily-face-m popover_c"><img
                                    alt="<?php echo $product['redeem_miles']; ?> miles"
                                    title="<?php echo $product['redeem_miles']; ?> miles"
                                    src="<?php echo base_url(); ?>front/img/smily-face.png"/>
                            <span class="popover_m"><?php echo $product['redeem_miles']; ?> miles</span>
                        </span>
                    <?php } ?>
                <?php } ?>
                <?php if ($login && !empty($multi_prices) && $page != "miles") { ?>
                    <?php if ($rolesUser == '5' && ($userID == $productUserID || $parentID == $productUserID)) { ?>
                        <span class="smily-face popover_c multi_prices_m">
                            <img src="<?php echo base_url(); ?>front/img/multi_prices.png"
                                 alt="<?php echo $multi_prices; ?>"
                                 title="<?php echo $multi_prices; ?>"/>
                            <span class="popover_m"><?php echo lang('multibuy_prices'); ?></span>
                        </span>
                    <?php } ?>
                <?php } ?>
                <?php if ($page != "miles") { ?>
                    <?php if (($rolesUser == '5' && ($userID == $productUserID || $parentID == $productUserID)) || $rolesUser == '4') { ?>
                    <?php if ($login) { ?>
                        <span class="smily-face favorites_heart  popover_c">
                            <a onclick="checkListStockList(<?php echo $product['id_products']; ?>)"
                               alt='<?php echo lang('add_my_list'); ?>'
                               title='<?php echo lang('add_my_list'); ?>'><img
                                        src="<?php echo base_url(); ?>front/img/heart.png"/></a>
                            <span class="popover_m"><?php echo lang('add_my_list'); ?></span>
                        </span>
                    <?php } ?>
                    <?php } ?>
                <?php } ?>

                <?php if ($page == "miles") { ?>
                    <?php $login_txt = "+ Add Reward"; ?>
                <?php } else { ?>
                    <?php $login_txt = "+ Add to Cart"; ?>
                <?php } ?>
                
                <?php if (($rolesUser == '5' && ($userID == $productUserID || $parentID == $productUserID)) || $rolesUser == '4') { ?>
                <?php if ($login) { ?>
                    <a class="add_to_cart popover_c btn-cart" alt="<?php echo $login_txt; ?>" title="<?php echo $login_txt; ?>" id="product-<?php echo $product['id_products']; ?>"><?php echo $login_txt; ?><?php if (!$login) { ?>
                        <span class="popover_m"><?php echo $login_txt; ?></span><?php } ?></a>
                <?php } ?>       
                <?php } ?>
                
            </div>
        </div>
    </div>
</div>