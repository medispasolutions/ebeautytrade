
<?php
$user=$this->ecommerce_model->getUserInfo();

$available_balance="";
if($user['available_balance']>0){
$available_balance=" (".changeCurrency($user['available_balance'],true).")";  }

$available_miles="";
if($user['miles']>0){
$available_miles =" (".$user['miles'].")";}


?>
<div class="page-title border">
<h1>
<i class="fa fa-fw"></i>
My Account
</h1>
</div>
<ul class="tabs">
    <li class="<?php if($this->router->class=="user" && $this->router->method=="index") { echo "active" ; } ?>">
    <a <?php if($this->router->class=="user" && $this->router->method=="index") { }else{?> href="<?php echo route_to('user');?>" <?php }?> >Dashboard</a>
    </li>
   <li class="<?php if($this->router->class=="user" && $this->router->method=="profile") { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="profile")  { }else{?> href="<?php echo route_to('user/profile');?>" <?php } ?> >Profile</a>
    </li>
<?php if(checkIfSupplier()){?>
      <li class="<?php if(($this->router->class=="user" && $this->router->method=="brands")  || ($this->router->class=="user" && $this->router->method=="brand")) { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="brands")  { }else{?> href="<?php echo route_to('user/brands');?>" <?php } ?> >My Brands</a>
    </li>
<?php } ?>
    
    <li class="<?php if(($this->router->class=="user" && $this->router->method=="addresses") || ($this->router->class=="user" && $this->router->method=="address")) { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="addresses")  { }else{?> href="<?php echo route_to('user/addresses');?>" <?php } ?> >Address Book</a>
    </li>
    
    
    <li class="<?php if($this->router->class=="user" && $this->router->method=="orders") { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="orders")  { }else{?> href="<?php echo route_to('user/orders');?>" <?php } ?> >Orders History</a>
    </li>
    
       <li class="<?php if($this->router->class=="user" && $this->router->method=="quotation") { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="quotation")  { }else{?> href="<?php echo route_to('user/quotation');?>" <?php } ?> >Quotations</a>
    </li>
    
    <li style='display: none;' class="<?php if(($this->router->class=="user" && $this->router->method=="miles")) { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="miles")  { }else{?> href="<?php echo route_to('user/miles');?>" <?php } ?> >My Miles <?php echo $available_miles;?></a>
    </li>
    <!--<li class="<?php if(($this->router->class=="user" && $this->router->method=="vouchers")   || ($this->router->class=="user" && $this->router->method=="voucherDetails")) { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="vouchers")  { }else{?> href="<?php echo route_to('user/vouchers');?>" <?php } ?> >My Vouchers</a>
    </li>-->
    <li style='display: none;' class="<?php if(($this->router->class=="user" && $this->router->method=="credits")  || ($this->router->class=="user" && $this->router->method=="add_credits")) { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="credits")  { }else{?> href="<?php echo route_to('user/credits');?>" <?php } ?> >My Wallet <?php echo $available_balance;?></a>
    </li>
    <?php if(checkIfSupplier()){?>
    <li class="<?php if(($this->router->class=="user" && $this->router->method=="dropfiles")) { echo "active" ; } ?>">
<a <?php if($this->router->class=="user" && $this->router->method=="dropfiles")  { }else{?> href="<?php echo route_to('user/dropfiles');?>" <?php } ?> >Drop Files</a><?php } ?>
    </li>
  </ul>