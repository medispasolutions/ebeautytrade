<?php
/*
    This page will submit the order information to paypal website.
    After the customer completed the payment she will return to this site
*/
//require_once 'paypal.inc.php';
//$paypal['item_name'] = "PlainCart Purchase";
$paypal['url'] = "https://www.sandbox.paypal.com/sg/cgi-bin/webscr";
//$paypal['url'] = "https://www.paypal.com/sg/cgi-bin/webscr";
$paypal['invoice'] = $id_orders;
$paypal['amount'] = changeCurrency($amount_default_currency,0,"USD");
$paypal['currency_code'] = "USD";
$paypal['site_url'] = str_replace("http://","https://",site_url());
$paypal['success_url'] = str_replace("http://","https://",route_to("cart/paypal_success"));
$paypal['cancel_url'] = str_replace("http://","https://",route_to("cart/paypal_cancel"));
$paypal['notify_url'] = str_replace("http://","https://",route_to("cart/paypal_error"));
$paypal['cmd'] = "_xclick";
//$paypal['cmd'] = "_cart";
$paypal['return_method'] = "2";
$paypal['lc'] = "US";
$paypal['bn']  = "toolkit-php";
$paypal['display_shipping_address'] = "1";
$paypal['business'] = "rzklt@gmail.com";

//$paypal['business'] = "live@gmail.com";
?>
<!--<center>
    <p>&nbsp;</p>
    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="333333">Processing
        Transaction . . . </font>
<script src="<?=base_url()?>scripts/paypal-button.min.js?merchant=PDX4BSLUXCU36" data-button="buynow" data-name="My product" data-amount="1.00"></script></p>
</center>-->
<form action="<?php echo $paypal['url']; ?>" method="post" name="frmPaypal" id="frmPaypal">
<input type="hidden" name="charset" value="utf-8">
<input type="hidden" name="return" value="<?php echo $paypal['success_url']; ?>">
<input type="hidden" name="currency_code" value="<?php echo $paypal['currency_code']; ?>">
<input type="hidden" name="amount" value="<?php echo $paypal['amount']; ?>">
<input type="hidden" name="invoice" value="<?php echo $paypal['invoice']; ?>">
<!--<input type="hidden" name="item_name" value="< ?php echo $paypal['item_name']; ?>">-->
<input type="hidden" name="business" value="<?php echo $paypal['business']; ?>">
<input type="hidden" name="cmd" value="<?php echo $paypal['cmd']; ?>">
<input type="hidden" name="cancel_return" value="<?php echo $paypal['cancel_url']; ?>">
<input type="hidden" name="notify_url" value="<?php echo  $paypal['notify_url']; ?>">
<input type="hidden" name="rm" value="<?php echo $paypal['return_method']; ?>">
<input type="hidden" name="lc" value="<?php echo $paypal['lc']; ?>">
<input type="hidden" name="bn" value="<?php echo $paypal['bn']; ?>">
<input type="hidden" name="no_shipping" value="<?php echo $paypal['display_shipping_address']; ?>">
</form>
<script language="JavaScript" type="text/javascript">
$(document).ready(function(){
 $("#frmPaypal").submit();
});
</script>