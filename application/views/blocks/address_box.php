<?php if (isset($new) || !isset($submit_address)) { ?>
    <div class="<?php if (isset($pickup) && $pickup == 2) {echo "pickup-address";} ?> <?php if (isset($pickup)) {
        echo "pickup_address";
    } ?> addressBox addressBox_c <?php if (isset($new) && (($formType == "billing" && $address['billing'] == 1) || ($formType == "shipping" && $address['shipping'] == 1))) {
        echo "active";
    } ?>" id="addressBox_<?php echo $formType; ?>_<?php echo $address['id_users_addresses']; ?>">
<?php } ?>
    <div class="address_ctrls">
        <?php if (isset($pickup) && $pickup == 2) {
            echo lang('pickup');

        } else {
            ?>
            <a onclick="addNewAddress('<?php echo $formType; ?>',<?php echo $address['id_users_addresses']; ?>)">Edit</a> |
            <a onclick="if(confirm('Are you sure you want delete this Address ?')){ remove_address(<?php echo $address['id_users_addresses']; ?>,'<?php echo $formType; ?>'); }">Remove</a>
        <?php } ?>
    </div>
    <div class="innerAddressBox" id="address_<?php echo $formType; ?>_<?php echo $address['id_users_addresses']; ?>">

        <input type="hidden" name="type_address" value="<?php echo $formType; ?>"/>
        <span class="radiobutton_address"><!--<input type="radio" name="<?php echo $formType; ?>_select" value=""  <?php if (isset($new)) { ?> checked="checked"  <?php } ?> />--></span>
        <div class="description">

            <!-- <label>Contact Information</label>-->
            <br>
            <?php if (!empty($address['company'])) { ?>
                <?php echo $address['company'] ?> <br>
            <?php } ?>

            <?php if (!empty($address['first_name'])) { ?>
                <?php echo $address['first_name'] ?><?php echo $address['last_name'] ?>    <br>
            <?php } ?>

            T: <?php echo $address['phone'] ?><?php if (!empty($address['mobile']) && $address['mobile'] != 0) { ?>, M: <?php echo $address['mobile'] ?>
            <?php } ?>
            <?php if (!empty($address['fax']) && $address['fax'] != 0) { ?>
                <br> F: <?php echo $address['fax'] ?>
            <?php } ?>


            <br/>
            <!--  <label>Address</label>  -->

            <?php echo $address['street_one']; ?> <br>
            <?php if (!empty($address['street_two'])) { ?>
                <?php echo $address['street_two']; ?> <br>
            <?php } ?>
            <?php echo $address['city'] ?>, <?php echo $address['state'] ?>
            <?php if (!empty($address['postal_code'])) { ?>
                , <?php echo $address['postal_code']; ?>
            <?php } ?>

            <br>
            <?php echo $address['countries_title']; ?>


        </div>
    </div>

<?php if (isset($new) || !isset($submit_address)) { ?></div> <?php } ?>