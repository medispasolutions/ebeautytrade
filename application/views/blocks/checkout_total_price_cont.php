<?php

$cart_items = $this->cart->contents();

if (empty($cart_items))

    $cart_items = $this->ecommerce_model->loadUserCart();
$products = $this->ecommerce_model->getCartProducts($cart_items);
$cartPrices = $this->ecommerce_model->getCheckoutAttr();
$net_price = $cartPrices['net_price'];
$net_discount_price = $cartPrices['net_discount_price'];
$discount = $cartPrices['discount'];
$sub_total = $cartPrices['sub_total'];
$net_redeem_miles_price = $cartPrices['net_redeem_miles_price'];
$total_redeem_miles = $cartPrices['total_redeem_miles'];
$net_discount_redeem_miles_price = $cartPrices['net_discount_redeem_miles_price'];

if (isset($shippingCharge)) {
    $net_discount_price = $net_discount_price + $shippingCharge;
} else {
    $shippingCharge = 0;
}

$i = 0;

$bool_price = false;

if ($net_redeem_miles_price != $sub_total) {
    $bool_price = true;
}

?>
    <!--<div class="note">* the total price does not include VAT & Shipping</div>-->

<?php if (!empty($products)) { ?>

    <?php if ($section == 'cart') { ?>
        <!--<div class="cart-box-right"> <a class="btn_cart icon-shopping-cart" href="<?php echo route_to('products'); ?>">continue shopping</a> <a class="btn_cart icon-update" onclick="$('#update_cart_form').submit();">update quantity</a> <a class="btn_cart icon-print" target="_blank" href="<?php echo route_to('cart/print_c'); ?>" >print</a> <a class="btn_checkout" onclick="checkout()">checkout</a></div>-->
    <?php } ?>

    <div class="cart-collaterals nested-container">
        <div class="col-md-3"></div>
        <div class="cart-right-column col-md-9">
            <div class="totals grid-full alpha omega">
                <div class="totals-inner">
                    <table id="shopping-cart-totals-table">
                        <colgroup>
                            <col>
                            <col width="1">
                        </colgroup>
                        <tfoot>
                        <?php if ($bool_price) { ?>
                            <tr>
                                <td class="a-right" colspan="1" style=""><strong>Total Amount</strong></td>
                                <td class="a-right"><span
                                                class="price"><?php echo changeCurrency($net_discount_price); ?></span>
                                    </td>
                            </tr>
                            <?php if (!empty($cartPrices['total_redeem_miles']) && $cartPrices['total_redeem_miles'] > 0) { ?>
                                <tr>
                                <td class="a-right" colspan="1" style=""><strong>Total Redeemed Miles</strong></td>
                                <td class="a-left"><span class="price"><?php echo $cartPrices['total_redeem_miles']; ?>
                                        <span class="currency_txt">Miles</span></span></td>
                                </tr>
                                <?php } ?>
                        <?php } ?>
                        </tfoot>
                        <tbody>

                        <?php if ($bool_price) { ?>
                            <!--<tr><td class="a-right" colspan="1" style="">Subtotal:</td><td class="a-right" style="width:50px;"><span class="price"><?php echo $sub_total; ?></span></td></tr>-->
                            <?php if ($discount > 0) { ?>
                                <tr>
                                    <td class="a-right" colspan="1" style="">Discount :</td>
                                    <td class="a-right"><?php echo changeCurrency($discount); ?></td>
                                </tr>
                            <?php } ?><?php } ?>
                        <?php /*?>  <?php if($net_discount_redeem_miles_price>0){ ?><tr><td class="a-right" colspan="1" style="">Redeemed Miles: </td><td class="a-right" ><span class="price"><?php echo changeCurrency($net_discount_redeem_miles_price); ?></span></td></tr><?php } ?><?php */ ?>
                        <input type="hidden" class="shippingCharge" value="<?php echo $shippingCharge; ?>"/>
                        <?php if ($shippingCharge > 0) { ?>
                            <tr>
                                <td class="a-right" colspan="1" style=""><strong>Shipping Charge</strong></td>
                                <td class="a-right"><span class="price"><?php echo changeCurrency($shippingCharge); ?></span></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="cart-empty">
        <?php echo lang('empty_cart'); ?>
    </div>
<?php } ?>