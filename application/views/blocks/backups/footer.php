<?php $website_info=$this->fct->getonerow('settings',array('id_settings'=>1)); ?>

<?php $about_spamiles=$this->fct->getAll('about_spamiles','sort_order'); ?>

<?php $customer_service=$this->fct->getAll('customer_service','sort_order'); ?>





<footer class="footer">

<div class="centered">

<div class="middleSide">

<div class="footerContent">

<div class="footer-blk first ">

<span class="footer-blk-title">Newsletter Sign Up</span>  

<form id="NewsLetterForm" method="post" accept-charset="utf-8" action="<?php echo route_to('user/add_newsletter');?>">

    <div class="form-item bordered small-margin">

      <input class="textbox" type="email" name="email_newsletter" placeholder="Enter email address" >

    </div>
    <div class="form-item small-margin">
        <div class="g-recaptcha" id="footer-captcha" data-sitekey="6Le5kkMUAAAAAFyu9LqReVIG0Xi_EMLvbfVjAnft"></div>

    </div>
    <div class="form-item bordered">
        <input type="submit" title="SUBMIT" value="SUBMIT" class="btn">
    </div>

    <div id="email_newsletter-error" class="form-error"></div>
    <div id="g-recaptcha-response-error" class="form-error"></div>

    <div class="buttons-set">

      <div class="FormResult"></div>

      <div id="loader-bx"></div>

    </div>

  </form>

  

  <div class="description">

  Be the first to know about special offers, news and promotion.

  </div>

</div>

<div class="footer-blk ">

<span class="footer-blk-title">Social Media</span>      

<ul class="footer-ul">

<?php if(!empty($website_info['facebook'])){?>

<li><a  href="<?php echo prep_url($website_info['facebook']);?>" target="_blank"><i class="fa fa-fw"></i>Facebook</a></li>

<?php } ?>

<?php if(!empty($website_info['google_plus'])){?>

<li><a  href="<?php echo prep_url($website_info['google_plus']);?>" target="_blank"><i class="fa google-plus"></i>Google Plus</a></li>

<?php } ?>



<?php if(!empty($website_info['twitter'])){?>

<li><a  href="<?php echo prep_url($website_info['twitter']);?>" target="_blank"><i class="fa fa-fw"></i>Twitter</a></li>

<?php } ?>

<?php if(!empty($website_info['instagram'])){?>

<li><a  href="<?php echo prep_url($website_info['instagram']);?>" target="_blank"><i class="fa fa-fw"></i>Instagram</a></li>

<?php } ?>

<?php  if(!empty($website_info['youtube'])){?>

<li><a href="<?php echo prep_url($website_info['youtube']);?>" target="_blank"><i class="fa fa-fw"></i>Youtube</a></li>

<?php } ?>

</ul>

</div>



<div class="footer-blk">

<span class="footer-blk-title">Customer Service</span>      

<ul class="footer-ul">

<?php foreach($customer_service as $val){?>

<li><a href="<?php echo route_to('pages/customer_service/'.$val['title_url']);?>" ><?php echo $val['title'];?></a></li>

<?php }?>

</ul>

</div>

<div class="footer-blk">

<span class="footer-blk-title">About Spamiles</span>      

<ul class="footer-ul">

<?php foreach($about_spamiles as $val){?>

<li><a href="<?php echo route_to('pages/about_spamiles/'.$val['id_about_spamiles']);?>" ><?php echo $val['title'];?></a></li>

<?php }?>

<li><a href="<?php echo route_to('suppliers');?>" >Suppliers</a></li>
<li><a href="<?php echo route_to('back_office');?>" target="_blank">Supplier Access</a></li>

</ul>

</div>



<div class="footer-blk">

<a class="footer-blk-title" href="<?php echo route_to('contactus');?>">Contact Us</a>      

<?php echo $website_info['address'];?>

</div>

<!--<div class="footer-blk">

 <img src="<?php echo base_url();?>front/img/footer_img.jpg" />     

</div>-->

</div>

</div></div>



<div id="Copyright" class="row-fluid">

<div class="centered">

Copyright <?php echo date("Y"); ?> all rights reserved. 


</div>

</div>

</footer>
