<input type="hidden" id="old_qty" value=""/>
<?php

$login = checkUserIfLogin();
$cart_items = $this->cart->contents();
if (empty($cart_items)) {
    $cart_items = $this->ecommerce_model->loadUserCart();
}

$ids_products = $this->ecommerce_model->getIds('id', $cart_items);
if (!empty($ids_products)) {
    $suppliers = $this->ecommerce_model->getCartDeliveredByBrands(array('ids_products' => $ids_products));
} else {
    $suppliers = array();
}

$i = 0;
foreach ($suppliers as $supplier) {
    $i++;

    $ids_brands = array();
    $brands = array();
    if (!empty($supplier['id_user'])) {
        $brands = $this->fct->getAll_cond('brands', 'sort_order asc', array('id_user' => $supplier['id_user']));
        $ids_brands = $this->ecommerce_model->getIds('id_brands', $brands);
    } else {
    }
    $cond['ids_brands'] = $ids_brands;
    $check = $this->ecommerce_model->getCartProducts($cart_items, "", $cond);
    ?>
    <?php if (!empty($check)) {
        ?>
        <?php if ((!empty($supplier['id_user']) || (empty($supplier['id_user']) && count($suppliers) > 1))) {

            ?>
            <div class="cart_deliver_by"><label><?php echo $supplier['deliver_by']; ?></label>
                <div class="supplier_support">
                    <?php if (isset($supplier['phone']) && !empty($supplier['phone'])) { ?>
                        <div class="phone_cart">
                            <i title="<?php echo $supplier['phone']; ?>" alt="<?php echo $supplier['phone']; ?>" class="fa fa-phone"></i>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <?php
        $data = array();
        $data['section'] = $section;
        $data['miles'] = 0;
        $data['supplier'] = $supplier;
        $data['ids_brands'] = $ids_brands;
        $this->load->view('load/checkbox-cart', $data) ?>

        <?php
        $data = array();
        $data['section'] = $section;
        $data['miles'] = 1;
        $data['supplier'] = $supplier;
        $data['ids_brands'] = $ids_brands;
        $this->load->view('load/checkbox-cart', $data); ?>
    <?php } ?>
<?php } ?>

<?php if ($section == 'cart') { ?>
    <!-- <table class="cart-btns" width="100%"> <tr class="first odd"> <td class="a-right last" colspan="50"> <a class="btn send_all" onclick="sumbitOrder('all')" title="SUBMIT ALL ORDERS" type="button"> SUBMIT ALL ORDERS</a> </td></tr></table>-->
<?php } ?>      
