<?php
$brands = $supplier['brands'];
$count = count($brands);
$url_logo = "";
$brandID = $brand['id_brands'];

$shipping_country=$this->fct->getAll_limit_cond('user','sort_order','asc',24,array('id_user'=>$userID));
 
$currentURL = current_url(); //http://myhost/main
$params   = $_SERVER['QUERY_STRING']; //my_id=1,3
$fullURL = $currentURL . '?' . $params;

$firstPart = strtok( $fullURL, '=' );
$allTheRest = strtok( '' );

// keywords are between =
$value = $fullURL;
$value = strstr($value, "="); //gets all text from needle on
$value = strstr($value, "&", true); //gets all text before needle
$categorySelected = substr($value, 1, 3);

$firstPart = strtok( $fullURL, '?' );
$selectedCategory = strtok( '' );
$allTheRest = substr($selectedCategory, 13, 3);

//$selectedCategorytest = substr($selectedCategory, 0, 10);
$selectedCategory = substr($selectedCategory, 0, 10);

$productRange = substr($fullURL, -3);
?>
<div class="searchPart">
    <h3 class="searchTitle">Refine Search</h3>
    <a class="searchClear" href="<?php echo $currentURL; ?>">Clear All</a>
    <div style="clear:both;margin:0 0 4px"></div>
    <div class="supplier_bx">
        <div class="supplier_brands">
            <h3>Brands</h3>
            <ul>
                <?php
                foreach ($brands as $brand) {
                    $sub_categories = $brand['sub_categories'];
                    if($brand['status']==1){
                    ?>
                    <li class="supplier_brand">
                        <input style="position: absolute;z-index: 0;" type="checkbox" name="brandName" value="<?php echo $brand['title']; ?>" <?php if($brandID == $brand['id_brands']){ ?>checked <?php } ?> >
                        <a title="<?php echo $brand['title']; ?>" alt="<?php echo $brand['title']; ?>" href="<?php echo route_to('brands/details/' . $brand['id_brands']); ?>">
                            <?php echo $brand['title']; ?></a>
                    </li>
                <?php } } ?>
            </ul>
        </div>
        <div class="supplier_brands">
            <h3>Categories</h3>
            <ul>
                <?php
                $categories2 = $this->db->query("SELECT DISTINCT categories.id_categories, categories.title FROM products_categories join categories on categories.id_categories = products_categories.id_categories join products on products.id_products = products_categories.id_products WHERE products.deleted=0 and products.id_brands=".$brandID." AND categories.id_parent = 0 AND products.status = 0 AND categories.status = 0 AND categories.deleted = 0 ");
                foreach ($categories2->result() as $row) { ?>
                    <li class="supplier_brand">
                        <input style="position: absolute;z-index: 0;" type="checkbox" name="brandName" value="<?php echo $row->title; ?>" <?php if($row->id_categories == $allTheRest){ ?>checked <?php } else if($row->id_categories == $categorySelected) { ?> checked <?php } ?> >
                        <a title="<?php echo $row->title; ?>" alt="<?php echo $row->title; ?>" href="?categories[]=<?php echo $row->id_categories; ?>">
                            <?php echo $row->title; ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php if(!empty($allTheRest)){ ?>
            <div class="supplier_brands">
                <?php if(!empty($sub_categories_cc) && count($sub_categories_cc)>1){?>
                    <h3>Product Range</h3>
                        <ul>
                          <?php
                          foreach($sub_categories_cc as $val){
                              if($val['deleted'] == 0){
                        	  if($selectedCategory=="categories"){
                        	  $url=$fullURL."&categories%5B%5D=".$val['id_categories'];
                        	  } else {
                        		$url="?category=".$val['id_categories'];
                        		}?>
                          <li class="supplier_brand <?php if($productRange==$val['id_categories']) echo "active";?>">
                              <input style="position: absolute;z-index: 0;" type="checkbox" name="ProductRange" value="<?php echo $val['title'.$lang];?>" <?php if($productRange==$val['id_categories']){ ?>checked <?php } ?> >
                              <a href="<?php echo $url; ?>#products_section"><?php echo $val['title'.$lang];?></a></li>
                          <?php }
                          }
                          ?>
                          
                        </ul>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>