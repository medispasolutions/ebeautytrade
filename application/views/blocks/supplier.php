<?php
$brands = $supplier['brands'];
$count = count($brands);
$url_logo = "";
?>
<div class="supplier_bx <?php if (count($brands) < 4) echo " mod"; ?>">
    <div class="inner_supplier_bx">
        <div class="supplier_logo">
            <div class="imgTable">
                <div class="imgCell">
                    <a class="addImgEffect" title="<?php echo $supplier['trading_name']; ?>"
                       alt="<?php echo $supplier['trading_name']; ?>"
                       href="<?php echo route_to('brands/details/' . $brands[0]['id_brands']); ?>">
                        <?php if (!empty($supplier['logo'])) {
                            $url_logo = base_url() . 'uploads/user/' . $supplier['logo'];
                        } else {
                            if (isset($brands[0]['logo']) && !empty($brands[0]['logo'])) {
                                $url_logo = base_url() . 'uploads/brands/112x122/' . $brands[0]['logo'];
                            }
                        } ?>
                        <?php if (!empty($supplier['logo'])) { ?>
                            <img alt="<?php echo $supplier['trading_name']; ?>"
                                 title="<?php echo $supplier['trading_name']; ?>" src="<?php echo $url_logo; ?>"/>
                        <?php } ?>

                    </a>
                </div>
            </div>
        </div>

        <div class="supplier_brands">
            <ul>
                <?php $k = 0;
                foreach ($brands as $brand) {
                    $k++;
                    $sub_categories = $brand['sub_categories'];
                    if ($brand['status'] == 1) {
                        ?>
                        <li class="supplier_brand
                    <?php if ($k > 3 && $count > 4) {
                            echo "hide_brand";
                        } ?>">
                            <a title="<?php echo $brand['title']; ?>" alt="<?php echo $brand['title']; ?>"
                               href="<?php echo route_to('brands/details/' . $brand['id_brands']); ?>">
                                <?php echo $brand['title']; ?></a>
                        </li>

                        <?php if ($k == 3 && $count > 4) { ?>
                            <li class="show_more">
                                <a onclick="showMoreBrands(this)" title="show More" alt="show More">Show
                                    More</a>
                            </li>
                        <?php } ?>
                    <?php }
                } ?>

                <?php if ($count > 4) { ?>
                    <li class="show_more show_less "><a onclick="showMoreBrands(this)" title="show More"
                                                        alt="show More">Show Less</a></li>
                <?php } ?>

            </ul>
        </div>
    </div>
</div>