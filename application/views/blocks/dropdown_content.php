
<?php $classifications = $this->ecommerce_model->getClassifications($cond=array(),6,0);?>



<?php 
$cond['sales']=1;
$classifications_sales = $this->ecommerce_model->getClassifications($cond);
?>
<?php $designers = $this->fct->getAll_limit_cond('designers','sort_order','asc',6,array());?>
<?php $designers_gallery = $this->fct->getAll('designers_gallery','sort_order');?>

<!--DESIGNERS-->
<div class="mainMenu_dropdown_content" id="mainMenu_dropdown_content_2">
    <div class="inner_mainMenu_dropdown_content">
    <div class="mainMenu_dropdown_content_left">
   
    <ul>
    <?php foreach($designers as $d){?>
    <li><a href="<?php echo route_to('products?designer='.$d['id_designers']);?>"><?php echo $d['title'];?></a></li>
    <?php } ?>

    </ul>
    </div>
    <?php if(!empty($designers_gallery)){?>
    <div class="mainMenu_dropdown_content_right">
    <div class="slick_pages mainMenu_dropdown_slick" id="mainMenu_dropdown_slick_2">
    <?php foreach($designers_gallery as $gal){?>
      <div class="item">
        <div class="innerItem"> <img src="<?php echo base_url();?>uploads/designers/gallery/<?php echo $gal['image'];?>" /> </div>
      </div>
    <?php } ?>  
      
      
      
    </div>
    </div>
    <?php } ?>
  </div>
  </div>

<!--CLASSIFICATIONS-->
<?php $j=2; foreach($classifications as $classification){$j++;
	$categories=$classification['categories'];
	$gallery=$classification['gallery'];
	?>
<?php if(!empty($categories)){ ?>
<div class="mainMenu_dropdown_content" id="mainMenu_dropdown_content_<?php echo $j;?>">
    <div class="inner_mainMenu_dropdown_content">
    <div class="mainMenu_dropdown_content_left">
   
    <ul>
    <?php foreach($categories as $cat){?>
    <li><a href="<?php echo route_to('products?classifications='.$classification['id_classifications'].'&category='.$cat['id_categories']);?>"><?php echo $cat['title'];?></a></li>
    <?php } ?>
    <li><a href="<?php echo route_to('products?classifications='.$classification['id_classifications']);?>">View All</a></li>

    </ul>
    </div>
    <?php if(!empty($gallery)){?>
    <div class="mainMenu_dropdown_content_right">
    <div class="slick_pages mainMenu_dropdown_slick" id="mainMenu_dropdown_slick_<?php echo $j;?>">
    <?php foreach($gallery as $gal){?>
      <div class="item">
        <div class="innerItem"> <img src="<?php echo base_url();?>uploads/classifications/gallery/<?php echo $gal['image'];?>" /> </div>
      </div>
    <?php } ?>  
 
    </div>
    </div>
    <?php } ?>
  </div>
  </div> 
  <?php } ?>
<?php } ?>


<!--SALES-->
<?php $j=$j+1;?>
<div class="mainMenu_dropdown_content sales_dropdown_content" id="mainMenu_dropdown_content_<?php echo $j;?>">
    <div class="inner_mainMenu_dropdown_content">
    <div class="mainMenu_dropdown_content_left">
   
    <ul>
    <?php foreach($classifications_sales as $s){?>
    <li><a href="<?php echo route_to('products?sales=1&classifications='.$s['id_classifications']);?>"><?php echo $s['title'];?></a></li>
    <?php } ?>

    </ul>
    </div>

  </div>
  </div>
