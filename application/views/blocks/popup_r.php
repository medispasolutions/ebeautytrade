<div class="shadow" id="shadow1" onclick="closePopup()"></div>
<div id="PopupContainer" class="<?php if(isset($popup_id)){ echo $popup_id; }?>">
 <div class="centered">
     <div class="content">
         <div class="close"><a onclick="closePopup()"> <i class="fa fa-fw"></i> </a></div>
         <div class="inner" id="PopupLoader">
            </div>
        </div>
    </div>
</div>