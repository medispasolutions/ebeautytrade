<?php
$categories=$this->fct->getAll('categories','sort_order');
$price_range=$this->fct->getAll('price_range','sort_order');
$sizes_search=$this->fct->getAll_cond('attribute_options','sort_order',array('id_attributes'=>1));
$colors_search=$this->fct->getAll_cond('attribute_options','sort_order',array('id_attributes'=>2));
?>

<div class="search-b">
<form method="get" action="<?php echo route_to('products');?>">
<label class="search-lbl">Search by</label>
<div class="select-search-bx">
<select class="select_box" name="category" id="category_s">
<option value="">Type</option>
<?php foreach($categories as $category){?>
<option value="<?php echo $category['title_url'] ;  ?>" <?php if(isset($_GET['category']) && $_GET['category']==$category['title_url'])echo "selected" ;?>>
<?php echo $category['title'] ;  ?>
</option>
<?php } ?>
</select>
</div>

<div class="select-search-bx">
<select class="select_box"  name="price_range" id="price_range_s">
<option value="">Price Range</option>
<?php foreach($price_range as $p){?>
<option value="<?php echo $p['id_price_range'] ;  ?>" <?php if(isset($_GET['price_range']) && $_GET['price_range']==$p['id_price_range'])echo "selected" ;?>>
<?php echo changeCurrency($p['from']).' - '.changeCurrency($p['to']);  ?>
</option>
<?php } ?>
</select>
</div>

<div class="select-search-bx">
<select class="select_box" name="color_s" id="color_s">
<option value="">Color</option>
<?php foreach($colors_search as $c){?>
<option  value="<?php echo $c['id_attribute_options'] ;  ?>" <?php if(isset($_GET['color_s']) && $_GET['color_s']==$c['id_attribute_options'])echo "selected" ;?>>
<?php echo $c['title'] ;  ?>
</option>
<?php } ?>
</select>
</div>

<div class="select-search-bx">
<select class="select_box"  name="size_s" id="size_s">
<option value="">Size</option>
<?php foreach($sizes_search as $s){?>
<option value="<?php echo $s['id_attribute_options'] ;  ?>" <?php if(isset($_GET['size_s']) && $_GET['size_s']==$s['id_attribute_options'])echo "selected" ;?>>
<?php echo $s['title'] ;  ?>
</option>
<?php } ?>
</select>
</div>

<button class="search-btn web_search"><img src="<?php echo base_url();?>front/img/icons/search.png" /></button>
<button class="search-btn mob_search">Search</button>
</form>
</div>