
<div class="toolbar">
<?php if($count>0){ ?>
<div class="sorter">
<?php $new_url=$current_url;?>
<!--AMOUNT-->

<p class="amount">
<?php if(!$search){?>
<strong><?php echo $count;?> Item(s)</strong>
<?php }else{ ?>
items <?php echo $items_from;?> to <?php echo $items_to;?> of <?php echo $count;?> total 
<?php } ?>
</p>


<!--SORT ORDER-->
<?php if(isset($_GET['sort'])) {
$new_url=getNewUrl($current_url,'sort='.$_GET['sort']);}?>
<div class="sort-by">
<label>Sort By</label>
<select  onchange="setLocation(this.value)" >
<option value="<?php echo $new_url.'&sort=newIn';?>"   <?php if(isset($_GET['sort']) && $_GET['sort']=="newIn") echo "selected";?> >New In </option>
<option value="<?php echo $new_url.'&sort=most_relevant';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="most_relevant") echo "selected";?> > Most Relevant </option>
<option value="<?php echo $new_url.'&sort=ascending';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="ascending") echo "selected";?> > Price: ascending  </option>
<option value="<?php echo $new_url.'&sort=descending';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="descending") echo "selected";?> > Price: descending </option>
</select>

<!--DIRECTION-->
<?php $new_url=$current_url;?>
<?php if(isset($_GET['dir'])) {
$new_url=getNewUrl($current_url,'dir='.$_GET['dir']);}?>
<?php if(isset($_GET['dir']) && $_GET['dir']=="desc"){?>
<a class="category-asc ic ic-arrow-down grid_m" title="Set Ascending Direction" href="<?php echo $new_url.'&dir=asc';?>" >
<i class="fa fa-arrow-up"></i>
</a>
<?php }else{?>
<a class="category-asc ic ic-arrow-down grid_m" title="Set Descending Direction"  href="<?php echo $new_url.'&dir=desc';?>" >
<i class="fa fa-arrow-down"></i>
</a>
<?php } ?>
</div>

<!--SHOW ITEM-->
<?php $new_url=$current_url;?>
<?php if(isset($_GET['show_items'])) {
$new_url=getNewUrl($current_url,'show_items='.$_GET['show_items']);}?>
<div class="limiter">
<label>Show</label>
<?php 
$show=30;
$show_items=0;
$k=ceil($count/$show);

?>
<select onchange="setLocation(this.value)">
<?php for($i=1;$i<=$k;$i++){
	$show_items=$show_items+$show;?>
<option <?php if(isset($_GET['show_items']) && $_GET['show_items']==$show_items) echo "selected";?>  value="<?php echo $new_url.'&show_items='.$show_items;?>"> <?php echo $show_items;?> </option>
<?php } ?>

</select>
<span class="per-page"> per page</span>
</div>


<!--VIEW AS-->
<?php $new_url=$current_url;?>
<?php if(isset($_GET['mode'])) {
$new_url=getNewUrl($current_url,'mode='.$_GET['mode']);}?>
<div class="view-mode">
<label>View as:</label>
<a class="grid_m <?php if((isset($_GET['mode']) && $_GET['mode']=="grid") || !isset($_GET['mode'])) echo "active"; ?>" title="Grid"  
<?php if((isset($_GET['mode']) && $_GET['mode']=="grid") || !isset($_GET['mode'])){}else{?> href="<?php echo $new_url.'&mode=grid';?>" <?php } ?>>
<i class="fa fa-th-large"></i></a>
<a class="grid_m  <?php if((isset($_GET['mode']) && $_GET['mode']=="list")) echo "active"; ?>" title="List" 
<?php if((isset($_GET['mode']) && $_GET['mode']=="list")) {}else{?>
href="<?php echo $new_url.'&mode=list';?>"
<?php } ?>><i class="fa fa-th-list"></i></a>
</div>
<div class="pagination-row">Page: <?php echo $this->pagination->create_links(); ?>  </div>
</div>
<?php }?>
</div>