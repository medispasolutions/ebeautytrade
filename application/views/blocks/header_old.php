
<?php if(checkUserIfLogin()){
	$user=$this->ecommerce_model->getUserInfo();
	$cartPrices=$this->ecommerce_model->getCheckoutAttr();
	$checklist_categories=$this->pages_fct->getUserCheckListCategories(array('id_user'=>$user['id_user'],'set_as_main'=>1),10,0);
$user_miles=$user['miles']-$cartPrices['total_redeem_miles'];

if($user['available_balance']>0){
$available_balance=" (".changeCurrency($user['available_balance'],true).")";  }else{
$available_balance =" (0,00)";	}

if($user_miles>0){
$available_miles =" (".$user_miles.")";}else{
$available_miles =" (0)";	}
	}?>

<?php $brands=$this->custom_fct->getBrands(array('set_as_featured'=>1,'status'=>1),8,0);?>
<?php 
		$cart_items = $this->cart->contents();
		if(empty($cart_items))
		$cart_items = $this->ecommerce_model->loadUserCart();
?>
<?php $cart_items_count=$this->cart->total_items(); ?>
<?php $currencies = $this->fct->getAll('currency_rates','sort_order');?>
<?php $guide_h = $this->fct->getAll('guide','sort_order');?>
<?php $cond['mainCategories']=1;?>
<?php $categoriesBrandTraining=$this->custom_fct->getCategoryBrandTraining($cond);?>
<?php 
$count_brands=$this->custom_fct->getBrands(array());
$list_brands=$this->custom_fct->getBrands(array('status'=>1),$count_brands,0);
?>
<?php /*?><?php $brands_letters = $this->ecommerce_model->getBrandsWithLetters();?><?php */?>
<?php $current_currency = $this->fct->getonerecord('currency_rates',array('currency_to'=>$this->session->userdata('currency'))); ?>
 <?php if(checkUserIfLogin()){$login=true;}else{$login=false;} ?>
 <div id="brands_init" style="display:none;">
<div class="brand_dropdown_slick" id="brands_slick">
<?php $b=0; foreach($brands as $brand_h){$b++;?>
<?php /*?><?php if(!empty($brand_h['logo'])){?><?php */?>
<div class="item">
<div class="innerItem">
 <?php if($b!=1){ ?>
<span class="border"></span> <?php } ?> 
<div class="imgTable"><a class="imgCell" href="<?php echo route_to('brands/details/'.$brand_h['id_brands']);?>">

<img title="<?php echo $brand_h['title'];?>" alt="<?php echo $brand_h['title'];?>"  src="<?php echo base_url();?>uploads/brands/<?php echo $brand_h['logo'];?>" />

</a></div>

</div>
</div>
<?php /*?><?php } ?><?php */?>
<?php } ?>

</div>
</div>
<header class="header">

<div class="row delivery-call ">
<div class="centered"><label><span>Free Delivery</span> +971 4 277 5920 / 5</label></div></div>
<div class="centered">

<div class="header-top">
<div class="logo"><a href="<?php echo site_url();?>" ><img src="<?php echo base_url();?>front/img/logo.png" title="Spamiles" alt="Spamiles" /></a></div>

<div class="header-top-right">

<ul class="ul-header-top">
<li>
<div class="search_mb">
<div  class="form-search menuBlg account-content-details" style="">
<form  method="get" action="<?php echo route_to('search');?>">
<div class="form-search-txt">
<input class="keywords" type="text" placeholder="Search by Code, Name or Keywords" name="keywords" value="">
<button class="button" type="submit" title="Search">
<a >GO</a>
</button>
</div>
</form>
</div>
</div>
</li>

<?php if($login){?>
<li class="web">
<a class="mainMenu-li-a" href="<?php echo route_to('user/credits');?>">My Wallet <?php echo $available_balance;?></a>
</li>

<li class="web" >
<a class="mainMenu-li-a" href="<?php echo route_to('user/miles');?>">My Miles <span id="remaining-miles"><?php echo $available_miles;?></span></a>
</li>
<?php } ?>
<li  class="web <?php if($this->router->class=="cart" && $this->router->method=="index") echo "active" ; ?>"><a id='cart_items_count'""  class="<?php if($this->router->class=="cart" && $this->router->method=="index") echo "active" ; ?>" href="<?php echo route_to('cart');?>"><i class="icon"><img src="<?php echo base_url();?>front/img/cart.png" /></i>My Cart
<?php if(!empty($cart_items_count)) {?> <span class="count" id="cart_items_count"><?php echo $cart_items_count ?></span><?php } ?></a></li>



<!--<li >
<a class="mainMenu-li-a" href="<?php echo route_to('contactus');?>">Contact Us</a>
</li>-->

<!--<li  class="checkout-h <?php if($this->router->class=="vouchers") echo "active" ; ?>"><a href="<?php echo route_to('vouchers');?>" >Vouchers</a></li>-->
<li><div class="currency_bx ">
      <div class="lbl_shdw_bx">
       <div class="lbl_shdw">
 
        <a class="current_currency" ><?php echo $current_currency['currency_to'] ;?> <span class="currency_arrow_dwn"></span></a>
        </div></div>
      <div class="currencies"> 
          <ul class="ul_currency">
          <?php foreach($currencies as $currency) {?>	
            <li class="ul_currency_li  <?php if($currency['currency_to'] == $this->session->userdata('currency')) { echo " active" ;} ?>" >
            <a href="<?php echo route_to('home/change_currency/'.$currency['currency_to']);?>"><?php echo $currency['title'] ;?>
            </a></li>
            <?php } ?>
            
          </ul>
        </div>
      </div></li>

      </ul>
</div>

</div>
<div class="header_bottom box-sizing">
<div class="mainMenu">
<ul class="ul_mainMenu no-delay menuBlg menu" >
<!--<li ><a class="mainMenu-li-a" href="<?php echo site_url();?>">Home</a></li>-->                                                                                                       
<li id="dropdown-3" class="dropdown active_dropdown">
<a class="mainMenu-li-a mainMenu-mob-li-a no-pointer <?php if($this->router->class=="brands" && $this->router->method=="details") echo "active" ; ?>">Shop by Brand <span class="opener">
<i class="fa fa-fw plus"></i>
<i class="fa fa-fw minus"></i>
</span></a>

<div id="sub-dropdown-3" class="sub-dropdown brand_dropdown">

<div class="brand_dropdown_cont">


<!--<div class="glossary_section">
<?php foreach($brands_letters as $brands_letter){?>
<div class="letter_block">
<span class="letter_title"><?php echo $brands_letter['letter'];?></span>
<?php  $brands=$brands_letter['brands'];?>
<?php if(!empty($brands)){?>
<ul class="ul_letters">
<?php foreach($brands as $brand_h){?>
<li><a class="<?php if($this->router->class=="brands" && $this->router->method=="details" && isset($brand) && $brand['id_brands']==$brand_h['id_brands']) echo "active" ; ?>" href="<?php echo route_to('brands/details/'.$brand_h['id_brands']);?>"><?php echo $brand_h['title'];?></a></li>
<?php } ?>
</ul>
<?php } ?>
</div>
<?php } ?>

</div>-->

<div class="featured_brands">
<?php 
$counter=count($list_brands);

$c_num=6;
$r_num=floor($counter/$c_num);

$remain=($r_num*$c_num)-$counter;

$bool_remain=false;
/*if($remain<0){
	$remain=-($remain);}*/
if($remain<0){
	$r_num++;
	$bool_remain=true;}
	
$remain=-($remain);	

$bool=true;
$i=0;?>
<ul class="ul_featured_brands">
<?php foreach($list_brands as $b){
	$i++;

	if($bool_remain==true && $remain<1){
			$r_num--;
			$i=1;
			$bool_remain=false;
			}
	
?>

<li class="featured_brand_bx">
<a  title="<?php echo $b['title'];?>" alt="<?php echo $b['title'];?>" href="<?php echo route_to('brands/details/'.$b['id_brands']);?>"><?php echo $b['title'];?></a></li>
<?php
if($i!=0 && $i%$r_num==0 && $i!=$counter){$bool=true;$remain--; ?>
	</ul>
    <ul class="ul_featured_brands">
<?php }} ?>

</ul>
</div>

<!--<div class="featured_brands">
<div class="innerFeaturedBrands">
<?php foreach($list_brands as $b){$i++;?>
<div class="featured_brand_bx">
<a  title="<?php echo $b['title'];?>" alt="<?php echo $b['title'];?>" href="<?php echo route_to('brands/details/'.$b['id_brands']);?>"><?php echo $b['title'];?></a>
</div>
<?php } ?>
</div>
</div>-->

<div class="brand_dropdown_slick_cont">


</div>
</div>
</div>
<!--<ul id="sub-dropdown-3" class="sub-dropdown" >
<?php foreach($brands as $brand){?> 
<li  class="dropdown-li">
<a class="dropdown-toggle" href="<?php echo route_to('brands/details/'.$brand['id_brands']);?>"><?php echo $brand['title'];?></a>
</li>
<?php } ?> </ul>--></li>
<li id="dropdown-5"  class="dropdown"><a class="mainMenu-li-a  mainMenu-mob-li-a no-pointer <?php if($this->router->class=="brands" && $this->router->method=="training") echo "active" ; ?>" >Brand Certified Training<span class="opener">
<i class="fa fa-fw plus"></i>
<i class="fa fa-fw minus"></i>
</span></a>

<ul id="sub-dropdown-5" class="sub-dropdown" >
<?php foreach($categoriesBrandTraining as $categoryTraining){?> 
<li  class="dropdown-li">
<a class="dropdown-toggle  <?php if($this->router->class=="brands" && $this->router->method=="training" && isset($category) && $categoryTraining['id_training_categories']==$category['id_categories']) echo "active" ; ?>" href="<?php echo route_to('brands/training/'.$categoryTraining['id_training_categories']);?>"><?php echo $categoryTraining['title'];?></a>
</li>

<?php } ?> 
<li  class="dropdown-li dropdow-li-view-all"><a class="dropdown-toggle" href="<?php echo route_to('brands/training');?>">View all</a></li></ul>
</li>



<li  id="dropdown-4" class="dropdown">
<a class="mainMenu-li-a mainMenu-mob-li-a no-pointer <?php if($this->router->class=="contactlist" || $this->router->class=="events") echo "active" ; ?>">Directory 
<span class="opener">
<i class="fa fa-fw plus"></i>
<i class="fa fa-fw minus"></i>
</span>
</a>

<ul id="sub-dropdown-4" class="sub-dropdown" >
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if($this->router->class=="events") echo "active" ; ?>" href="<?php echo route_to('events');?>">
Events

</a>
</li>
<?php foreach($guide_h as $g){?> 
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if($this->router->class=="contactlist" && isset($guide) && $g['id_guide']==$guide['id_guide']) echo "active" ; ?>" href="<?php echo route_to('contactlist/index/'.$g['id_guide']);?>">
<?php echo $g['title'];?></a>
</li>
<?php } ?> </ul>
</li>
<li >
<a class="mainMenu-li-a" href="<?php echo prep_url('http://spamiles.com/blog/');?>">Blog</a>
</li>
</ul>
<ul class="ul_mainMenu no-delay   mod menuBlg account">
<?php if($login){?>
<li id="dropdown-7" class="dropdown <?php if($this->router->class=="user" && $this->router->method=="checklist") echo "active" ; ?>">

<span class="ul_mainMenu_border_left"></span>

<a class="mainMenu-li-a mainMenu-mob-li-a tr-n <?php if($login){ echo "no-pointer";}?>" <?php if(!$login) {?>href="<?php echo route_to('user/checklist');?>" <?php } ?> >
<!--<i class="fa fa-fw"></i>-->My List <span class="opener">
<i class="fa fa-fw plus"></i>
<i class="fa fa-fw minus"></i>
</span></a>
<?php if($login){?>
<ul id="sub-dropdown-7" class="sub-dropdown" >
<li  class="dropdown-li <?php if(!empty($checklist_categories)){ echo "border-bottom";}?>">
<a class="dropdown-toggle <?php if($this->router->class=="user" && $this->router->method=="checklist") echo "active" ; ?>" href="<?php echo route_to('user/checklist');?>">
list all

</a>
</li>

<?php 

foreach($checklist_categories as $val){
	?>
<li  class="dropdown-li checklist-li">
<a class="dropdown-toggle" href="<?php echo route_to('user/checklist_categories/'.$val['id_checklist_categories']);?>">
<?php echo $val['title'];?>
</a>
</li>
<?php } ?>
 
<li  class="dropdown-li text-b  <?php if(!empty($checklist_categories)){ echo "border-top";}?>">
<a style="" class="dropdown-toggle popup_call_checklist <?php if($this->router->class=="user" && $this->router->method=="categories") echo "active" ; ?>" onclick="return false;" href="<?php echo route_to('user/category/add');?>">
[Add New Category]
</a>
</li>






 </ul>
 <?php } ?>
</li>
<?php } ?>

<?php if($login){?>

<li class="dropdown <?php if($this->router->class=="user" && $this->router->method=="index") echo "active" ; ?>" id="dropdown-6">
<a class="mainMenu-li-a mainMenu-mob-li-a no-pointer" ><!--<i class="fa fa-fw"></i>-->My Account <span class="opener">
<i class="fa fa-fw plus"></i>
<i class="fa fa-fw minus"></i>
</span></a>
<ul id="sub-dropdown-6" class="sub-dropdown" >


<li  class="dropdown-li">
<a class="dropdown-toggle <?php if($this->router->class=="user" && $this->router->method=="index") { echo "active" ; } ?>" href="<?php echo route_to('user');?>">
Dashboard</a>
</li>
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if($this->router->class=="user" && $this->router->method=="profile") { echo "active" ; } ?>" href="<?php echo route_to('user/profile');?>">
Profile</a>
</li>
<?php if(checkIfSupplier()){?>
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if(($this->router->class=="user" && $this->router->method=="brands") || ($this->router->class=="user" && $this->router->method=="brand")) { echo "active" ; } ?>" href="<?php echo route_to('user/brands');?>">
My Brands</a>
</li>
<?php } ?>

<li  class="dropdown-li">
<a class="dropdown-toggle <?php if(($this->router->class=="user" && $this->router->method=="addresses") || ($this->router->class=="user" && $this->router->method=="address")) { echo "active" ; } ?>" href="<?php echo route_to('user/addresses');?>">
Addresses</a>
</li>
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if($this->router->class=="user" && $this->router->method=="orders") { echo "active" ; } ?>" href="<?php echo route_to('user/orders');?>">
Orders</a>
</li>
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if($this->router->class=="user" && $this->router->method=="quotation") { echo "active" ; } ?>" href="<?php echo route_to('user/quotation');?>">
Quotations</a>
</li>
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if(($this->router->class=="user" && $this->router->method=="miles")) { echo "active" ; } ?>" href="<?php echo route_to('user/miles');?>">
My Miles</a>
</li>


<!--<li  class="dropdown-li">
<a class="dropdown-toggle <?php if(($this->router->class=="user" && $this->router->method=="vouchers")   || ($this->router->class=="user" && $this->router->method=="voucherDetails")) { echo "active" ; } ?>" href="<?php echo route_to('user/vouchers');?>">
My Vouchers</a>
</li>-->
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if(($this->router->class=="user" && $this->router->method=="credits")  || ($this->router->class=="user" && $this->router->method=="add_credits")) { echo "active" ; } ?>" href="<?php echo route_to('user/credits');?>">
My Wallet</a>
</li>
<?php if(checkIfSupplier()){?>
<li  class="dropdown-li">
<a class="dropdown-toggle <?php if(($this->router->class=="user" && $this->router->method=="dropfiles")  || ($this->router->class=="user" && $this->router->method=="dropfiles")) { echo "active" ; } ?>" href="<?php echo route_to('user/dropfiles');?>">
Drop Files</a>
</li>
<?php } ?>
 </ul>
</li>
<?php } ?>
<?php if($login){?>
<li ><a class="mainMenu-li-a" href="<?php echo route_to('user/logout');?>"><!--<i class="fa fa-fw"></i>-->Logout</a></li>

<li>  
<div class="welcome-message web" >
Welcome, <a class="mainMenu-li-a" href="<?php echo route_to('user');?>"> <?php echo $user['first_name'];?></a>
</div></li>
<?php }else{ ?>
<li  class="login-btn <?php if($this->router->class=="user" && $this->router->method=="login") echo "active" ; ?>">
<a class="mainMenu-li-a"  href="<?php echo route_to('user/login');?>"><!--<i class="fa fa-fw"></i>-->Login</a></li>
<li class="register-btn <?php if($this->router->class=="user" && $this->router->method=="register") echo "active" ; ?>"><a class="mainMenu-li-a" href="<?php echo route_to('user/register');?>"><!--<i class="fa fa-fw"></i>-->Join</a></li>

<?php } ?>
</ul>


</div>
</div>
</div>

<div class="mainMenu_mob">
<ul>
<li><a class="menu-toggle" id="mob_menu">
<i class="fa fa-fw bar"></i>
<span class="lbl">
Menu
</span>
</a></li>

<li><a class="menu-toggle" id="mob_form-search">
<i class="icon fa fa-fw"></i>
<span class="lbl">
Search
</span>
</a></li>
<li><a class="menu-toggle" id="mob_account"><i class="fa fa-fw"></i><span class="lbl">Account</span></a></li>

<li  class="<?php if($this->router->class=="cart" && $this->router->method=="index") echo "active" ; ?>"><a  class="<?php if($this->router->class=="cart" && $this->router->method=="index") echo "active" ; ?>" href="<?php echo route_to('cart');?>"><i class="icon fa fa-fw"></i>
<span class="lbl">My Cart</span>
<?php if(!empty($cart_items_count)) {?> <span class="count"><?php echo $cart_items_count ?></span><?php } ?></a></li>



<li><a href="<?php echo route_to('user/checklist');?>">
<i class="icon fa fa-fw"></i>
<span class="lbl">
My List
</span>
</a></li>

<li ><a class="menu-toggle" id="mob_navigation"><i class="fa fa-fw"></i> <span class="lbl">Navigation</span></a></li>
</ul>
</div>
<div class="menuMobCont" id="menuMobCont">

</div>
</header>