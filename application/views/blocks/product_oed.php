<?php

if (!empty($product['brand'])) {
    $title_img = $product['brand']['name'];
} else {
    $title_img = $product['title'];
}
$image = '';

if (isset($product['gallery']) && !empty($product['gallery']))

    $image = $product['gallery'][0];

$attributes = $product['attributes'];

$first_available_stock = array();

if (isset($product['first_available_stock']) && !empty($product['first_available_stock'])) {

    $first_available_stock = $product['first_available_stock'];

}

if (isset($first_available_stock) && !empty($first_available_stock)) {

    $quantity = $first_available_stock['quantity'];
    $product['miles'] = $first_available_stock['miles'];
    $list_price = $first_available_stock['list_price'];
    $price = $first_available_stock['price'];
    $sku = $first_available_stock['sku'];
    $retail_price = $first_available_stock['retail_price'];
    $discount = $first_available_stock['discount'];
    $discount_expiration = $first_available_stock['discount_expiration'];
    $stock_options = unSerializeStock($first_available_stock['combination']);
    $hide_price = $first_available_stock['hide_price'];
    $price_segments = $this->fct->getAll_cond("product_price_segments", 'sort_order', array("id_products" => $first_available_stock['id_products'], "id_stock" => $first_available_stock['id_products_stock']));
    //print '<pre>';print_r($stock_options);exit;

} else {

    $quantity = $product['quantity'];
    $list_price = $product['list_price'];
    $price = $product['price'];
    $sku = $product['sku'];
    $retail_price = $product['retail_price'];
    $discount = $product['discount'];
    $discount_expiration = $product['discount_expiration'];
    $hide_price = $product['hide_price'];
    $price_segments = $this->fct->getAll_cond("product_price_segments", 'sort_order', array("id_products" => $product['id_products']));
}

/////////////MULTI PRICES//////////

$multi_prices = "";

foreach ($price_segments as $segment) {
    $multi_prices .= '' . $segment['min_qty'] . ' for ' . strip_tags(changeCurrency($segment['price'], true)) . ' each &#13;';
}

?>


<div class="product <?php if (isset($trans_delay)) { ?>trans top-trans delay-<?php echo $trans_delay; ?><?php } ?>"
     id="product-list-<?php echo $product['id_products']; ?>">

    <?php if ($product['set_as_oed'] == 1) { ?>
        <span class="pro-img online-exclusive">Save <?php echo $product['discount']; ?> %</span>
    <?php } ?>

    <?php $product_url = route_to('products/details/' . $product['id_products']); ?>

    <?php if ($page == "miles" && checkUserIfLogin()) {
        $product_url = route_to('products/details/' . $product['id_products']) . '?miles=1';
    } ?>

    <div class="inner-product">
        <span class="product-img">
            <a href="<?php echo $product_url; ?>">
            <? $altt = character_limiter(strip_tags($product['brief']), 150); ?>
                <?php if (!empty($product['gallery'][0]['image'])) { ?>
                    <img class="base_image" title="<?php echo $altt; ?>" alt="<?php echo $altt; ?>"
                         src="<?php echo base_url(); ?>uploads/products/gallery/295x295/<?php echo $product['gallery'][0]['image']; ?>"/>
                <?php } else { ?>
                    <img class="base_image" title="<?php echo $altt; ?>" alt="<?php echo $altt; ?>"
                         src="<?php echo base_url(); ?>front/img/default_product.png"/>
                <?php } ?>
             </a>
        </span>

        <div class="product-content box-sizing">
            <div class="product-oed">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>
                            <a href="<?php echo $product_url; ?>">
                            <?php echo character_limiter(strip_tags($product['title']), 70); ?>
                            </a>
                        </h3>
                        <div class="oed-product-description">
                            <?php echo character_limiter(strip_tags($product['description']), 100); ?>
                        </div>
                        <p style="color:#a6a6a6;">Deal finishes on <?php echo 	$product['discount_expiration']; ?></p>
                        <a class="oed-button" href="<?php echo $product_url; ?>">BUY DEAL</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>