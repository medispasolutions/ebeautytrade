<?php $login=checkUserIfLogin();?>
<input type="hidden"  value="<?php echo $order_details['id_quotation'];?>" name="id" />
<input type="hidden"  value="<?php echo $order_details['rand'];?>" name="rand" />
            <div class="n-table" id="quotation_cart">
<?php 
$i=0;
$quotation_line_items = $order_details['quotation_line_items'];

if(!empty($quotation_line_items)) { ?>
            <table border="1" cellspacing="0" cellpadding="10" style="text-align:center;" class="shopping_cart_table data-table">
              <tr>
               <th class="product_cart_details" width="5%">#</th>
              <th class="product_cart_details" width="6%">REF</th>
              <th class="product_cart_details" width="6%">BARCODE</th>
              <th style="width:25%;">ITEM DESCRIPTION</th>
                <?php if($login){?>
              <th class="product_cart_details" width="10%">PRICE <?php echo $order_details['currency'];?></th>
              <?php } ?>
               <th  width="8%">QTY</th>
                 <?php if($login){?>
                <?php if($this->session->userdata('auto_login')==1) {?>
              <th class="product_cart_details" style="width:20% !important;">Discount</th>
              <?php } ?><?php } ?>
              
			  <?php if($login){?>
              <th width="15%">SUB-TOTAL  <?php echo $order_details['currency'];?></th>
              <?php } ?>
               <?php if($this->session->userdata('auto_login')==1) {?>
              <th></th>
              <?php } ?>
              </tr>
              <?php 
$quotation_line_items = $order_details['quotation_line_items'];
$net_price = 0;
$net_discount_price = 0;

foreach($quotation_line_items as $pro) {
	if($pro['discount_type']==1){
		$discount=100-($pro['price_currency']*100)/$pro['price_old_currency'];}else{
		$discount=$pro['discount'];}
		
		$pr=round($discount,0);
$discount=number_format($pr, 2, '.', ',');
	
	
$i++;
$options=unSerializeStock($pro['options_en']);
?>

<input type="hidden" name="ids_line_items[]" value="<?php echo $pro['id_quotation_line_items'];?>" />


              <tr id="quotation_line_item_<?php echo $pro['id_quotation_line_items'];?>" class="quotation_line_item">
               <td><?php echo $i; ?></td>
               <td><?php echo $pro['product']['sku']; ?></td>
               <td><?php echo $pro['product']['barcode']; ?></td>
                <td style="text-align:left;"><a href="<?php echo route_to('products/details/'.$pro['product']['title_url']);?>" class="product_name"><?php echo $pro['product']['title'];?></a>
                <dl class="item-options">
                  <?php if(!empty($options)) { 

		$i=0;
		$c = count($options);
		foreach($options as $opt) {?>
			  <dt><i class="fa fa-fw"></i><?php echo $opt; ?> </dt>
          
	
    <?php }}?></dl>
                  </td>
                    <?php if($login){?>
      		 <td >
			<?php echo '<span class="price new_price" itemprop="price">'.$pro['price_currency'].' '.$pro['currency'].'</span>'; ?>
          </td>
     <?php } ?>
                <td>
                 <?php if($this->session->userdata('auto_login')==1) {?>
              <input class="textbox qty updateQty_cart" maxlength="12" id="quantity_<?php echo $pro['id_quotation_line_items'];?>" title="Qty" size="4" name="product_quantity[]" value="<?php echo $pro['quantity']; ?>" >
              <?php }else{ ?>
              <?php echo $pro['quantity']; ?>
              <?php }?></td>
               <?php if($this->session->userdata('auto_login')==1) {?>
               <td>
             
              <input class="textbox qty updateQty_cart" style="width:50% !important;" id="discount_<?php echo $pro['id_quotation_line_items'];?>" title="Discount" size="4" name="discount[]" value="<?php echo $discount; ?>" >
              <input type="hidden" style="width:50% !important;" id="list_price_<?php echo $pro['id_quotation_line_items'];?>"  value="<?php echo $pro['price_old_currency']; ?>" >
			  <select name="discount_type[]" id="discount_type_<?php echo $pro['id_quotation_line_items'];?>" class="selectbox textbox qty"  style="width:46% !important; text-align:left;" onchange="checkDiscountType(this)">
              <option value="1" <?php if($pro['discount_type']==1) echo "selected='selected'";?> >%</option>
              <option value="2" <?php if($pro['discount_type']!=1) echo "selected='selected'";?> ><?php echo $pro['currency'];?></option>
              </select>
			 
          
              </td> <?php }?>
                <?php if($login){?>
                <td><?php echo $pro['total_price_currency'].' '.$pro['currency'] ;?></td>
                <?php } ?>
                 <?php if($this->session->userdata('auto_login')==1) {?>
                <td><a onclick="removeQuotationLineItem(<?php echo $pro['id_quotation_line_items'];?>,'<?php echo $order_details['rand'];?>')">Delete</a></td>
                <?php }?>
              </tr>
              <?php }?>
              </table>
              <?php }?>
              
              
<?php 
$i=0;
$quotation_line_items = $order_details['quotation_line_items_redeem_by_miles'];

if(!empty($quotation_line_items)) { ?>
            <table border="1" cellspacing="0" cellpadding="10" style="text-align:center;" class="shopping_cart_table data-table">
              <tr>
              <th class="product_cart_details" width="5%">#</th>
              <th class="product_cart_details" width="10%">REF</th>
              <th class="product_cart_details" width="12%">BARCODE</th>
              
              <th style="width:40%;">ITEM(S) REDEEMED BY MILES</th>
                <?php if($login){?>
              <th class="product_cart_details" width="10%">MILES</th>
              <?php } ?>
			  <th  width="8%">QTY</th>
                <?php if($login){?>
              <th width="15%">SUB-TOTAL</th>
              <?php } ?>
              </tr>
              <?php 

$net_price = 0;
$net_discount_price = 0;

foreach($quotation_line_items as $pro) {
$i++;
$options=unSerializeStock($pro['options_en']);
?>
              <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $pro['product']['sku']; ?></td>
               <td><?php echo $pro['product']['barcode']; ?></td>
                <td style="text-align:left;"><a href="<?php echo route_to('products/details/'.$pro['product']['title_url']);?>" class="product_name"><?php echo $pro['product']['title'];?></a>
                <dl class="item-options">
                  <?php if(!empty($options)) { 

		$i=0;
		$c = count($options);
		foreach($options as $opt) {?>
			  <dt><i class="fa fa-fw"></i><?php echo $opt; ?> </dt>
          
	
    <?php }}?></dl>
                  </td>
                    <?php if($login){?>
       <td><?php echo $pro['redeem_miles']/$pro['quantity']; ?></td>
       <?php } ?>
                <td><?php echo $pro['quantity']; ?></td>
                  <?php if($login){?>
                <td><?php echo $pro['redeem_miles'] ;?></td>
                <?php } ?>
              </tr>
              <?php }?>
              </table>
              <?php }?>
               <div id="loader-bx"></div>
      <div class="FormResult"> </div> 
            <?php if($login){?>    
       <table border="0" cellpadding="0" cellspacing="0" class="total-price-table">
        <?php /*?>            <?php if($userData['type']=="retail") { 
		$delivery_day=getDeliveryDay($quotation_line_items);
		if($delivery_day!=0){?>
                    <tr>
                      <td colspan="2"><span class="delivery_day">Order will be delivered within <?php echo $delivery_day;?> day(s). </span></td>
                    </tr>
                    <?php }}else{ ?>
                    <tr>
                      <td colspan="2"><span class="delivery_day"><?php echo lang('we_will_contact_us');?> </span></td>
                    </tr>
                    <?php } ?><?php */?>
                    <tr>
                      <td><span>Sub-Total: </span></td>
                      <td ><?php echo $order_details['sub_total'].' '.$order_details['currency']; ?></td>
                    </tr>
               
                    <tr>
                      <td><span>Discount: </span></td>
                      <td><?php echo $order_details['discount'].' '.$order_details['currency']; ?></td>
                    </tr>
              
                     <?php if($order_details['redeem_discount_miles_amount']>0){ ?>
                    <tr>
                      <td><span>Redeem Miles: </span></td>
                      <td><?php echo $order_details['redeem_miles'].' Miles'; ?></td>
                    </tr>
                    <?php } ?>
                    
                    
                    <tr class="grand_total last">
                      <td><strong>Total: </strong></td>
                      <td><?php echo $order_details['amount_currency'].' '.$order_details['currency']; ?></td>
                    </tr>
         
                  </table>
                  <?php } ?>
             
          </div> 
             
     