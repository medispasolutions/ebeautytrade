<?php 
$user_favorites="";
if(checkUserIfLogin()){
$user_favorites = $this->ecommerce_model->getUserFavorites($this->session->userdata('login_id'));}
$products_viewed = $this->ecommerce_model->RecentlyViewed("",5);


?>

<div class="block block-account">
<div class="block-title">
<strong>
<span>Account</span>
</strong>
</div>
<div class="block-content">
<ul>
<li class="<?php if($this->router->class=="user" && $this->router->method=="index") echo "active" ; ?>">
<a  href="<?php echo route_to('user');?>">Account Dashboard</a>
</li>
<li class="<?php if($this->router->class=="user" && $this->router->method=="profile") echo "active" ; ?>">
<a  href="<?php echo route_to('user/profile');?>">Account Information</a>
</li>
<li  class="<?php if($this->router->class=="user" && ($this->router->method=="addresses" || $this->router->method=="add_address" || $this->router->method=="address")) echo "active" ; ?>" >
<a href="<?php echo route_to('user/addresses');?>">Address Book</a>
</li>

<li class="<?php if($this->router->class=="user" && $this->router->method=="orders") echo "active" ; ?>">
<a  href="<?php echo route_to('user/orders');?>">My Orders</a>
</li>

<li class="<?php if($this->router->class=="user" && $this->router->method=="favorites") echo "active" ; ?>" >
<a  href="<?php echo route_to('user/favorites');?>">Wishlist</a>
</li>
</ul>
</div>
</div>

<?php if(!empty($user_favorites)){ ?>
<div class="block block-wishlist">
<div class="block-title">
<strong>
<span>
My Wishlist
<small>(<?php echo count($user_favorites);?>)</small>
</span>
</strong>
</div>
<div class="block-content">
<p class="block-subtitle">Last Added Items</p>
<ol id="wishlist-sidebar" class="mini-products-list">

<?php $j=0; foreach($user_favorites as $fav){$j++;
if($j<4){
$arra_p=array();
$arr_p['id_products']=$fav['id_products'];

$arr_p['list_price']=$fav['list_price'];
$arr_p['price']=$fav['price'];
$arr_p['qty']="";
$arr_p['discount_product']=$fav['discount'];
$arr_p['discount_product_expiration']=$fav['discount_expiration'];
$arr_p['withSegment']="";
 
	 $prices=$this->ecommerce_model->getPrices($arr_p);
	 $n_price=$prices['n_price'];
	 $o_price=$prices['o_price'];?>
<li class="item odd">
<a class="product-image" title="<?php echo $fav['title'] ;  ?>" href="<?php echo route_to('products/details/'.$fav['title_url']);?>">
<img alt="<?php echo $fav['title'] ;  ?>" src="<?php echo base_url();?>uploads/products/gallery/120x120/<?php echo $fav['gallery'][0]['image'] ;  ?>">
</a>
<div class="product-details">
<a class="btn-remove" onclick="return confirm('Are you sure you would like to remove this item from the wishlist?');" title="<?php echo $fav['title'] ;  ?>" href="<?php echo route_to('user/removeFavorite/'.$fav['id_products']);?>"><i class="fa fa-fw"></i></a>
<p class="product-name">
<a href="<?php echo route_to('products/details/'.$fav['title_url']);?>"><?php echo $fav['title'] ;  ?></a>
</p>
<div class="price-box">
<span id="product-price-2536-wishlist" class="regular-price">
  <?php if(!empty($o_price)){ ?>
            <span class="price old_price" itemprop="price"><?php echo changeCurrency($o_price);?></span> <br />
            <span class="price new_price" itemprop="price "><?php echo changeCurrency($n_price);?></span>
            <?php }else{ ?>
            <span class="price" itemprop="price"><?php echo changeCurrency($n_price);?></span>
  <?php } ?>
</span>
</div>
<a class="link-cart" onclick="$('#add_form_<?php echo $j;?>').submit();" >Add to Cart</a>
</div>

<form id="add_form_<?php echo $j;?>" action="<?php echo route_to('cart/add'); ?>" method="post">
        	<input  type="hidden" name="product_id" value="<?php echo $fav['id_products']; ?>" />
</form>

</li>
<?php }else{
	break;}} ?>
</ol>
<div class="actions"><a href="<?php echo route_to('user/favorites');?>">Go to Wishlist</a>
</div>
</div>
</div>
<?php } ?>

<?php if(!empty($products_viewed)){ ?>
<div class="block block-list block-viewed">
<div class="block-title">
<strong>
<span>Recently Viewed Products</span>
</strong>
</div>
<div class="block-content">
<ol id="recently-viewed-items">
<?php foreach($products_viewed as  $product_viewed){?>
<li class="item odd">
<p class="product-name">
<a href="<?php echo route_to('products/details/'.$product_viewed['title_url']);?>"><?php echo $product_viewed['title'];?></a>
</p>
</li>
<?php } ?>

</ol>
</div>
</div> 
<?php }?>

