<div id="afterRegistrationPopup">
    <div class="close"><a onclick="closePopup2()"> <i class="fa fa-fw"></i> </a></div>
    <div class="popup-content">
        <h5>YOUR PAYMENT HAS BEEN
            SUCCESSFULLY SUBMITTED</h5>
        <p>
            Check your email, get the login details to access your Admin Panel.
        </p>
    </div>
</div>