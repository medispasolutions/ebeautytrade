<div id="afterRegistrationPopup">
    <div class="close"><a onclick="closePopup2()"> <i class="fa fa-fw"></i></a></div>
    <div class="popup-content">
        <h5>Congratulations!</h5>
        <p>You are now a Seller Member on ebeautytrade.com<br/>Check the credentials sent to your email, Access the Admin Panel and start building your B2B Store today!</p>
    </div>
</div>