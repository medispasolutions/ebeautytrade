<div id="afterRegistrationPopup">
    <div class="close"><a onclick="closePopup2()"> <i class="fa fa-fw"></i> </a></div>
    <div class="popup-content">
        <h5>YOUR ORDER HAS BEEN
            SUCCESSFULLY SUBMITTED</h5>
        <p>
            You will be contacted by the supplier shortly
            to confirm your order and
            proceed with delivery
        </p>
    </div>
</div>