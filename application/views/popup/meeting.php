<div class="content meeting_popup_content">
         
         <div class="new-users">
         <div class="close"><a onclick="closePopup2()"> <i class="fa fa-fw"></i> </a></div>
      <div class="content">
  <label>Meeting Request</label>
        </div>
      <form id="meetingRequest" class="login-form" method="post" accept-charset="utf-8" action="<?php echo route_to('home/send_meeting') ;?>">
      
        <div class="form-item">
         <label>Full Name</label>
          <input class="textbox" type="text" name="name" placeholder="" >
          <div id="name-error" class="form-error"></div> </div>
        <div class="form-item">
         <label>E-mail Address</label>
          <input class="textbox" type="text" name="email" placeholder="" >
          <div id="email-error" class="form-error"></div> </div>
        <div class="form-item">
          
          <label>Contact Number</label>
          <input class="textbox mobile-m" type="text" name="phone" placeholder="" >
         <!--<input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:40px;" name="phone[0]"  >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:40px; margin-left:2px;" name="phone[1]" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:107px; margin-left:2px;" name="phone[2]">-->
          <div id="phone-error" class="form-error"></div>
          </div>
        <div class="form-item">
         <label>Inquiry</label>
          <textarea class="textarea" name="message" placeholder="I would like to schedule a meeting"></textarea>
          <div id="message-error" class="form-error"></div> </div>
        <div class="buttons-set ta-r">
            <div id="loader-bx"></div>
        <div class="FormResult"></div>
         <input type="submit" title="Send Request" value="Submit" class="btn">  
          </div>
         </form>
      </div>
        </div>
