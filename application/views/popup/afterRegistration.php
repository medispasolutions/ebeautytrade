<div id="afterRegistrationPopup">
    <div class="close"><a onclick="closePopup2()"> <i class="fa fa-fw"></i> </a></div>
    <div class="popup-content">
        <h5>Thank you for registering!</h5>
        <p>
            We will get back to you shortly<br>
            after review.
        </p>
    </div>
</div>