<div class="content meeting_popup_content register_popup">
         
         <div class="new-users">
          <div class="close"><a onclick="closePopup2()"> <i class="fa fa-fw"></i> </a></div>
 <div class="welcome_popup"><h3>Welcome to Spamiles!</h3></div>
 <div class="description register_popup_description">Register Here</div>
      <form id="registration" method="post" accept-charset="utf-8" action="<?php echo route_to('user/submit')?>" >
<div class="row_form">
<div class="form-item ">

<div class="roles_item">
<!--   <input type="radio" name="role_c" value="4" />Spa/Salon
   <input type="radio" name="role_c" value="5" />Supplier-->
<input type="hidden" name="role_c" value="4" />
<input id="id_role" type="hidden" value="4" name="id_role">
<input  type="hidden" value="1" name="popup">
</div>

 <span id="id_role-error" class="form-error"></span>
</div></div>
<div class="innerUserForm">
<!--ROW 1-->
<div class="row_form">
<div class="form-item">
          <label class="required" >Trade Name </label>
          <input class="textbox" type="text" name="trading_name" >
          <span id="trading_name-error" class="form-error"></span> </div>
               
</div>

<div class="row_form row_form_2_phone">
<div class="form-item">
          <label class="required" >First Name</label>
          <input class="textbox" type="text" name="first_name" >
          <span id="first_name-error" class="form-error"></span> </div>
          
<div class="form-item gutter-right">
          <label class="required" >Last Name</label>
          <input class="textbox" type="text" name="last_name" >
 <span id="last_name-error" class="form-error"></span> </div>
               
</div>

<!--Personal Information-->
<div class="row_form row_form_2_phone">
                    <script type="text/javascript" src="https://www.ebeautytrade.com/front/js/jquery-1.10.0.min.js"></script>
                    <script>
                    $(function () {
                        $('#email').bind("cut copy paste", function (e) {
                            e.preventDefault();
                        });
                        $('#confirm_email').bind("cut copy paste", function (e) {
                            e.preventDefault();
                        });
                    });
                    </script>
<div class="form-item">
          <label class="required">Email Address</label>
          <input class="textbox" type="text"  name="email" value="" id="email" >
   
            <input type="hidden" name="id_user" id="id_user" value="" />
  <input type="hidden" name="correlation_id" id="correlation_id" value="" />

          <span id="email-error" class="form-error"></span></div>
<div class="form-item gutter-right">
          <label class="required" >Confirm Email Address</label>
          <input class="textbox" type="text" name="confirm_email" id="confirm_email">
          <span id="confirm_email-error" class="form-error"></span></div>               
</div>

<div class="row_form row_form_2_phone">
<div class="form-item ">
          <label class="required" for="password">Desired Password </label>
          <input class="textbox" type="password" name="password" >
          <span id="password-error" class="form-error"></span> </div>
<div class="form-item gutter-right ">
          <label class="required" >Re-type Password </label>
          <input class="textbox" type="password" name="confirm_password" >
          <span id="confirm_password-error" class="form-error"></span> </div>               
</div>

<div class="row_form">
<div class="form-item">
          <label class="required" >Contact Number</label>
          <input class="textbox mobile-m" type="text" name="phone" id="contact_number" >
          <span id="phone-error" class="form-error"></span> </div>
               
</div>
<!--Personal Information-->
<div class="row_form row_form_2_phone">
<div class="form-item">
          <label class="required">City</label>
          <input class="textbox" type="text" name="city"  value=""  >
    <span id="name-error" class="form-error"></span></div>
<div class="form-item gutter-right">
          <label class="required">Country</label>
          <?php $countries=$this->fct->getAll('countries','title asc');?>
          <select name ="country" class="textbox selectbox">
          <option value="">-Select-</option>
          <?php foreach($countries as $val){?>
          <option value="<?php echo $val['id_countries'];?>"><?php echo $val['title'];?></option>
          <?php } ?>
          </select>
          <span id="country-error" class="form-error"></span> </div>               
</div>

<!--Private Policy-->
<!--<div class="row"><h2 class="legend">Privacy Policy</h2></div>-->

<div class="row_form">
<div class="form-item mod req_c agree_disclaimer">

<label >By clicking Submit, you agree to the <a class="popup_call" onclick="return false" href="<?php echo route_to('user/getPrivatePolicy/'.$private_policy['title_url']);?>">Privacy Policy</a> <br /> and to the <a class="popup_call" onclick="return false"  href="<?php echo route_to('user/getPrivatePolicy/'.$disclaimer['title_url']);?>">Terms of Use.</a></label>
<input type="hidden" name="agree" value="" />
<span id="agree-error" class="form-error"></span></div>
             
</div>






<!--<div class="row_form" style="margin-top:20px;">
<div class="form-item">
          <label class="required" for="password">Please type the letters below  </label>
          <span class="captchaImage"><?php echo $this->fct->createNewCaptcha(); ?></span>
		  <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" />
		<span id="captcha-error" class="form-error"></span> </div>               
</div>-->


  
  
  
<div class="buttons-set">
  <div id="loader-bx"></div>
      <div class="FormResult"> </div>
          <input class="btn" title="Create an Account" type="submit" value="Submit">
           </div>  
    
    </div></form>
      </div>
        </div>
