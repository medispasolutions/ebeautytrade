<section class="section contact-form">
    <div class="content">
    	<h2 class="title-header line">Contact Information</h2>
        <div class="left">
             <h2 class="sub-title"><?php echo $branches[0]['title']; ?></h2>
           <?php echo $branches[0]['description']; ?>
        </div>       
        <div class="right">
       <?php
$attributes = array('method'=>'post', 'class'=>'contactform','id'=>'ContactForm');
$action = route_to('contactus/send');
echo form_open($action, $attributes);
?>
             	<div class="form-item third">
               <input type="text" name="name" class="input_contact" placeholder="Name *" />
               <span class="form-error" id="name-error"></span>
               </div>
               <div class="form-item third">
                <input type="text"  name="email" class="input_contact"  placeholder="Email *" />
                <span class="form-error" id="email-error"></span>
                </div>
                 <div class="form-item third last right">
                <input type="text"  name="phone" class="input_contact"  placeholder="Phone" />
                <span class="form-error" id="phone-error"></span>
                   </div>
                   <div class="form-item full">
                <textarea class="textarea_contact" name="message" placeholder="Message *"></textarea>
                 <span class="form-error" id="message-error"></span>
                </div>
                <div class="form-item full">
                <input type="submit" value="Send"  class="btn_contact"/>
                <div class="FormResult"></div>
                </div>
             </form>
        </div>   
    </div>
</section>
<section class="section contact-branches">
    <div class="content">
        <h2 class="title-2">Our Branches</h2>
        <div class="left">
             <h2 class="sub-title"><?php echo $branches[1]['title']; ?></h2>
           <?php echo $branches[1]['description']; ?>
        </div>       
        <div class="right">
        
             <h2 class="sub-title"><?php echo $branches[2]['title']; ?></h2>
           <?php echo $branches[2]['description']; ?>
            
        </div>   
    </div>
</section>