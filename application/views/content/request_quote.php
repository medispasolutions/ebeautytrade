<section class="section contact-form">
    <div class="content">
    	<h2 class="title-header line">Request a quote</h2>
               
   
       <?php
$attributes = array('method'=>'post', 'class'=>'contactform','id'=>'requestQouteForm');
$action = route_to('cart/senRequest');
echo form_open($action, $attributes);
?>
			 <!--ROW ONE-->
             	<div class="form-item third">
               <input type="text" name="name" class="input_contact" placeholder="Name *" />
               <span class="form-error" id="name-error"></span>
               </div>
                <div class="form-item third">
                <input type="text"  name="email" class="input_contact"  placeholder="Email *" />
                <span class="form-error" id="email-error"></span>
                </div>
                <div class="form-item third last right">
                <input type="text"  name="phone" class="input_contact"  placeholder="Phone" />
                <span class="form-error" id="phone-error"></span>
                   </div>
                   
                   
             <!--ROW TWO-->          
                <div class="form-item third">
               <input type="text" name="products" class="input_contact" placeholder="Products *" />
               <span class="form-error" id="products-error"></span>
               </div>
                <div class="form-item third">
                <input type="text"  name="company" class="input_contact"  placeholder="Company *" />
                <span class="form-error" id="company-error"></span>
                </div>
                <div class="form-item third last right">
                <input type="text"  name="country_region" class="input_contact"  placeholder="COUNTRY / REGION" />
                <span class="form-error" id="country_region-error"></span>
                   </div>
                   
             <!--ROW THREE-->         
                <div class="form-item full">
                <textarea class="textarea_contact" name="message" placeholder="Message *"></textarea>
                 <span class="form-error" id="message-error"></span>
                </div>
                <div class="form-item full">
                <input type="submit" value="Send"  class="btn_contact"/>
                <div class="FormResult"></div>
                </div>
             </form>
      
    </div>
</section>
