<div class="innerContent testimonials">
<div class="page-title border blue">
<h1><?php echo $section['title'];?></h1>
</div>

<div class="testimonials_cont">
<?php foreach($info as $val ){?>
<div class="testigmonials_fig <?php if(empty($val['image'])){ echo " mod";}?>">
<div class="testimonials_tp">
<?php if(!empty($val['image'])){?>
<div clas="testimonials_img">
<div class="inner_testimonials_img">
<img src="<?php echo base_url();?>uploads/testimonials/200x180/<?php echo $val['image'];?>" />
</div>
</div>
<?php } ?>
<div class="testimonials_content">
<div class="inner_testimonials_content">
<span class="testimonials_name"><?php echo $val['name'];?></span>
<span class="testimonials_title"><?php echo $val['title'];?></span>
</div></div>
</div>
<div class="testimonials_description">
<div class="inner_testimonials_description">
<i class="first_c"><img src="<?php echo base_url();?>front/img/testimonials_description_qt_ft.png" /></i>
<?php echo $val['description'];?>
<span class="cls_des_lst"><i><img src="<?php echo base_url();?>front/img/testimonials_description_qt_lt.png" /></i></span>
</div></div>
</div>
<?php } ?>




</div>

</div>