<?php $this->load->view('blocks/booking_banner');?>
<section class="section booking-form-section">
  <div class="booking-form">
    <h2 class="title-header line">BOOK YOUR VISIT NOW</h2>
    </div>
    <?php
$attributes = array('method'=>'post', 'class'=>'contactform','id'=>'ContactForm');
$action = route_to('booking/send');
/*echo form_open($action, $attributes);*/

?>
<form action="<?php echo $action;?>" method='post' class="contactform" id="ContactForm">
<div class="booking-form">
    <div class="form-item third">
      <label><span>*</span> Business Name: </label>
      <input type="text" name="subject" class="input_booking" placeholder="" />
      <span class="form-error" id="subject-error"></span> </div>
    <div class="form-item third">
      <label><span>*</span> Email Address: </label>
      <input type="text"  name="email" class="input_booking"  placeholder="" />
      <span class="form-error" id="email-error"></span> </div>
    <div class="form-item third last right">
      <label><span>*</span> Full Name: </label>
      <input type="text"  name="name" class="input_booking"  placeholder="" />
      <span class="form-error" id="name-error"></span> </div>
    <div class="form-item third last right">
      <label><span>*</span> Mobile Number: </label>
      <input type="text"  name="phone" class="input_booking"  placeholder="" />
      <span class="form-error" id="phone-error"></span> </div>
      </div>
  
  <div class="select-schedule">
    <h4>SELECT YOUR SCHEDULE</h4>
    <div class="booking-form">
      <div class="form-item">
        <label class="select-date-lbl">Select Date:</label>
        <div class="calendar-blk">
          <div class="inner-calendar-blk"> 
            <!--<img src="<?php echo base_url();?>front/images/calendar-explore.png" />-->
            
            <div id="my-calendar"></div>
          </div>
        </div>
        <input type="hidden" value="" name="date" id="date" />
        <span class="form-error" id="date-error"></span>
      </div>
      <div class="date_time">
      <label class="select-date-lbl">Select Time Slot:</label>
       <div class="form-item">
       <select name="time_slot" class="textbox selectbox">
       <?php $time_slots=time_slots();?>
       <option class="" value="">-Select-</option>
       <?php foreach($time_slots as $key=>$val){?>
       <option class="tl" value="<?php echo $key;?>"><?php echo $val;?></option>
       <?php } ?></select>
        <span class="form-error" id="time_slot-error"></span>
        <!--<table>
          <tr>
            <th colspan="4">Select Time:</th>
          </tr>
          <tr >
            <td  class="call-time-lbl"> From: </td>
            <td class="call-time-field">
            <input type="text" value="" name="time_from" id="time_from" class="textbox timepicker1" />
            </td>
            <td  class="call-time-lbl"><label>To:</label></td>
            <td class="call-time-field">
           
            <input type="text" value="" name="time_to"  id="time_to" class="textbox timepicker1" />
           </td>
          </tr>
          
          <tr >
            <td  class="call-time-lbl"> </td>
            <td class="call-time-field">
       
                <span class="form-error" id="time_from-error"></span>
                </td>
            <td  class="call-time-lbl"></td>
            <td class="call-time-field">
            <span class="form-error" id="time_to-error"></span></td>
            </tr>
          
          
        </table>-->
        </div>
      </div>
    </div>
  </div>
  <div class="booking-s">
    <div class="form-item full">
    
       <div class="centered">  <div class="FormResult"></div><div id="loader-bx"></div></div>
        
      <input type="submit" value="PROCEED"  class="btn_booking btn"/>
 
    </div>
    <!--<p>We will contact you within 48 hours.</p>-->
    <p>Free Airport Pick up available for International Visitors</p>
  
  </div>
  </form>
</section>
