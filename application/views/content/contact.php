<section class="section contact-form">
    <div class="content">
        <div class="page-title border">
            <h1> Contact Us </h1>
        </div>
        <div class="contact_map">
            <iframe src="<?php echo $settings['google_map']; ?>"></iframe>
        </div>
        <div class="row">
            <div class="right">
                <?php
                $attributes = array('method' => 'post', 'class' => 'contactform', 'id' => 'ContactForm');
                $action = route_to('contactus/send');
                echo form_open($action, $attributes);
                ?>
                <h3>Contact form</h3>
                <div class="row_form">
                    <div class="form-item third">
                        <input type="text" name="name" class="textbox" placeholder="Name *"/>
                        <span class="form-error" id="name-error"></span></div>
                    <div class="form-item third">
                        <input type="email" name="email" class="textbox" placeholder="Email *"/>
                        <span class="form-error" id="email-error"></span></div>
                    <div class="form-item third last right">
                        <input type="text" name="phone" class="textbox" placeholder="Phone"/>
                        <span class="form-error" id="phone-error"></span></div>
                </div>
                <div class="row_form">
                    <div class="form-item full">
                        <textarea class="textarea" name="message" placeholder="Message *"></textarea>
                        <span class="form-error" id="message-error"></span></div>
                </div>
                <div class="form-item full">
                    <div id="loader-bx"></div>
                    <input type="submit" value="Send" class="btn"/>
                    <div class="FormResult"></div>
                </div>
                </form>
            </div>
            <div class="left">
                <div class="contact-info">
                    <h3>Contact Info</h3>
                    <div class="description"> <?php echo $settings['address']; ?></div>
                </div>
            </div>
        </div>
        <?php if (!empty($branches)) { ?>
            <div class="contact-branches">
                <?php foreach ($branches as $branch) { ?>
                    <div class="contact-branch">
                        <div class="contact-branch-map">
                            <iframe src="<?php echo $branch['map']; ?>"></iframe>
                        </div>
                        <div class="description"> <?php echo $branch['address']; ?></div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</section>