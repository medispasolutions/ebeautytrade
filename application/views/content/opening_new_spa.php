<div class="innerContent">

<div  class="page-title border">
<h1>Opening a New Spa?</h1>
</div>

<div class="spa_form">
<div class="description">Whether you are Opening a New Spa, Expanding, Franchising or simply re-branding and updating your menu or equipment, we shall help you take the right decisions, adapting to your planning and budgeting. Once you fill and submit the Form below, a member of our Consulting Group will communicate with you shortly.</div>
<form id="spaForm" class="login-form" method="post" accept-charset="utf-8" action="<?php echo route_to('opening_new_spa/send') ;?>">
<section class="spa_section"> 
<h2>Personal information</h2>
<div class="row_form">
<div class="form-item salutation ">
<label><input  type="radio" name="salutation" value="1"> Mrs.</label>
<label><input   type="radio" name="salutation" value="2"> Mr.</label> 
<label><input   type="radio" name="salutation" value="3"> Ms.</label> 
<div id="salutation-error" class="form-error"></div></div>
</div>
<div class="row_form">
        <div class="form-item col-3">
		  <label class="required">First Name<em>*</em></label>
          <input class="textbox" type="text" name="first_name">
          <div id="first_name-error" class="form-error"></div> </div>
        <div class="form-item col-3">
          <label class="required">Last Name<em>*</em></label>
          <input class="textbox" type="text" name="last_name">
          <div id="last_name-error" class="form-error"></div> </div>
          <div class="form-item col-3">
          <label class="required">Position<em>*</em></label>
          <input class="textbox" type="text" name="position">
          <div id="position-error" class="form-error"></div> </div>
          </div>
          
 <div class="row_form">
        <div class="form-item col-3">
		  <label class="required">E-mail<em>*</em></label>
          <input class="textbox" type="text" name="email">
          <div id="email-error" class="form-error"></div> </div>
        <div class="form-item col-3">
          <label class="required">Mobile<em>*</em></label>
          <input class="textbox mobile-m" type="text" name="mobile" id="mobile">
          <div id="mobile-error" class="form-error"></div> </div>
          <div class="form-item col-3">
          <label class="required">Phone</label>
          <input class="textbox mobile-m" type="text" name="phone">
          <div id="phone-error" class="form-error"></div> </div>
          </div>
          
</section>

<section class="spa_section">         
<h2>Company details (If available)</h2>
<div class="row_form">
        <div class="form-item col-3">
		  <label class="required">Trade Name</label>
          <input class="textbox" type="text" name="trade_name">
          <div id="first_name-error" class="form-error"></div> </div>
        <div class="form-item col-3">
          <label class="required">Website</label>
          <input class="textbox" type="text" name="website">
          <div id="website-error" class="form-error"></div> </div>
          <div class="form-item col-3">
          <label class="required">City</label>
          <input class="textbox" type="text" name="city">
          <div id="city-error" class="form-error"></div> </div>
          </div>          

		<div class="row_form">
        <div class="form-item col-3">
        <?php $countries=$this->fct->getAll('countries','title');?>
          <label class="required">Country</label>
          <select name ="id_countries" class="textbox selectbox">
          <option value="">-Select Country-</option>
          <?php foreach($countries as $val){?>
          <option value="<?php echo $val['id_countries'];?>"><?php echo $val['title'];?></option>
          <?php } ?>
          </select>
          <span id="business_type-error" class="form-error"></span> </div>
          <div class="form-item col-3">
          <label class="required">E-mail</label>
          <input class="textbox" type="text" name="company_email" >
          <div id="email_company-error" class="form-error"></div> </div>
          <div class="form-item col-3">
          <label class="required">Phone(s)</label>
          <input class="textbox" type="text" name="company_phone" >
          <div id="phone-error" class="form-error"></div> </div>
        
          
        </div>
        
        <div class="row_form">
        <div class="form-item">
    	  <label class="required">Address</label>
          <textarea class="textarea" name="address"></textarea>
          <div id="textarea-error" class="form-error"></div> </div>
         </div>
        

        
</section>  

<section class="spa_section mod">         
<h2>Project type </h2>
<div class="row_form">
<?php foreach($projects as $val){?>
   <label>
   <input type="checkbox" name="projects[]" value="<?php echo $val['id_projects_type'];?>" />
   <?php echo $val['title'];?>
   </label>
<?php } ?>
  </div>                  
</section> 

<section class="spa_section mod">         
<h2>Service(s) you may enquire</h2>
<div class="row_form">
<?php foreach($services as $val){?>
   <label>
   <input type="checkbox" name="services[]" value="<?php echo $val['id_services_enquiry'];?>" />
   <?php echo $val['title'];?>
   </label>
<?php } ?> 
        
          </div>                  
</section>

<section class="spa_section">         
<h2>Consultants</h2>
 <div class="row_form">
        <div class="form-item col-3">
          <input type="hidden" id="consultant" name="consultant">
          <div id="email-error" class="form-error"></div> </div></div>
<div class="row_form">
        
        <div class="consultants_cont">
        <div class="consultants_slick">
     <?php $consultants=$this->fct->getAll('consultants','sort_order');?>   
        <?php foreach($consultants as $val){ ?>
        <div class="item">
        <div class="innerItem">
        <div class="supplier">
  <div class="inner_supplier">
  <div class="supplier_blk"  id="consultants_<?php echo $val['id_consultants'];?>" >
<a class="remove_consultant hide" ><i class="fa fa-fw" aria-hidden="true" title="Remove"></i> </a>
  <span class="supplier_img">  <?php if(!empty($val['image'])){ ?>
  <?php if(!empty($val['link'])){ ?> <a href="<?php echo prep_url($val['link']);?>"> <?php } ?>
  <img src="<?php echo base_url();?>uploads/consultants/<?php echo $val['image'];?>" />
   <?php if(!empty($val['link'])){ ?></a><?php } ?> <?php } ?></span>

  <span class="supplier_name"><?php echo $val['name'];?></span>
  <label><?php echo $val['position'];?></label>

  <label><?php echo $val['phone'];?></label>
<div class="brand_logo_bx">
<?php if(!empty($val['logo'])){?>
<div class="imgTable"><div class="imgCell">
<img src="<?php echo base_url();?>uploads/consultants/<?php echo $val['logo'];?>" />
</div></div>
<?php } ?>
</div>
<div class="row ta-c">
<a class="popup_call read_more" onclick="return false" href="<?php echo route_to('opening_new_spa/getConsultant/'.$val['id_consultants']);?>" >Read More</a>
</div>
<div class="row consultants_btns" i>
<a class="btn consultants_btn" >Meeting Request</a>
</div>
<div class="meeting_request" id="consultants_request_<?php echo $val['id_consultants'];?>">
  <a class="close_meeting" onclick="unseletConsultant(<?php echo $val['id_consultants'];?>)">Close<!--<i class="fa fa-fw"></i>--></a>

        <input  type="hidden" name="consultants[]" value="<?php echo $val['id_consultants'];?>">
        
        <input  type="hidden" class="checked_input" name="checked[]" value="">
        
        <div class="form-item">
          <textarea class="textarea" name="message[]" placeholder="Inquiry"></textarea>
          <div id="message-error" class="form-error"></div> </div>
        
    
  </div>
</div>

  </div>
  </div>
        
        </div>
        </div>
        <?php } ?>
        
        </div>
        <div class="remove_btn"></div>
        </div>
          
          </div>          

</section>

<section class="spa_section mod"> 
<!--<div class="row_form">
        <div class="form-item">
    	  <textarea class="textarea" name="comments" placeholder="Comments"></textarea>
          <div id="comments-error" class="form-error"></div> </div>
         </div>-->
</section>
<div class="description">Submitting this form will entitle you for one hour Free Consulting Service with one of our Group Consultants.</div>
        <div class="buttons-set ta-r">
            <div id="loader-bx"></div>
        <div class="FormResult"></div>
        <div class="row ta-c" ><input type="submit" title="Submit a New Spa" value="SUBMIT" class="btn"></div>
          </div>
         </form>
</div>


</div>

<ul id="pagination" style="display:none;">
  <?php for($i=0;$i<$count;$i=$i+$show_items){
	 
		if(isset($_GET['per_page'])) {
		 $next=$_GET['per_page']+$show_items;
		  }else{
		$next=$offset+$show_items; }
	?>
  <li class="<?php if($i==$next) echo "next";?>"  ><a href="<?php echo route_to('question_and_answer?pagination=on&&per_page='.$i);?>"><?php echo  $next;?></a></li>
  <?php } ?>
</ul>

  <form id="meetingRequest" class="login-form" method="post" accept-charset="utf-8" action="<?php echo route_to('brands/send') ;?>"></form>