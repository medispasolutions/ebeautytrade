<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title><?php echo $title;?></title>
	<meta charset="utf-8">
    <!-- Favicon --> 
	<link rel="shortcut icon" href="<?php echo base_url(); ?>front/images/favicon.ico">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>front/css/style.css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-1.10.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.PrintArea.js"></script>
	<script type="text/javascript">
    	function calculateIDPos(){
			var WindowWidth = $(window).width();
			var RightPos = ((WindowWidth - 600) / 2) - 80;
			$('#printButton').css('right',RightPos); 
		}
		$(document).ready(function(){
			calculateIDPos();
			$('#printButton').click(function(){
			
				$('#printThis').printArea();
			});
		});
		$(window).bind("load",function(){
			calculateIDPos();
		});
    </script>
   
</head>
<body>
<input type="submit" style="position:absolute;top:0;" class="printButton btn button" id="printButton" value="<?php echo lang("print"); ?>" />
<div class="printing-container" id="printThis" style="width:770px; margin:0 auto;margin-top:40px;">

<?php 
$data['voucher']=$voucher;
?>
<?php $this->load->view('blocks/voucher_details',$data);?>

</div>
</body>
</html>















