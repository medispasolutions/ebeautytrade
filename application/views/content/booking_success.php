<?php $this->load->view('blocks/booking_banner');?>
<section class="section booking-success">
  <div class="img">
  <img src="<?php echo base_url();?>front/img/booking-success-img.png" />
  </div>
<div class="booking-success-content">
<h2>THANK YOU!</h2>
<p>We will contact you shortly to confirm your booking<br />
We look forward to welcoming you.
</p>

 

<h4>We are located at:</h4>
<?php echo $settings['address'];?>

<div class="booking-map">
<div class="contact_map">
      <iframe src="<?php echo $settings['google_map'];?>"></iframe>
    </div>
</div>
</div>

</section>
