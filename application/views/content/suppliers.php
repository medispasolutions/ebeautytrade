<div class="innerContent suppliers_listing">
    <div class="page-title border">
        <h1><?php echo lang('suppliers_title'); ?></h1>
    </div>

    <div class="suppliers">
        <?php foreach ($suppliers as $supplier) {
            $data['supplier'] = $supplier;
            $this->load->view('blocks/supplier', $data);
        } ?>
    </div>
    <div class="pagination-row"><?php echo $this->pagination->create_links(); ?></div>
</div>