<div class="innerContent">
<div class="page-title border">
<h1><?php echo $guide['title'];?></h1>
<!--<div class="sort_order">
<label>Sort by</label>
<select>
<option>Category</option>
</select>
</div>-->
</div>

<?php if(isset($home_bottom_ads_left) || isset($home_bottom_ads_right)){ ?>
  <?php 
$data['home_bottom_ads_left']=$home_bottom_ads_left;
$data['home_bottom_ads_right']=$home_bottom_ads_right;
$this->load->view('blocks/home-ads-bottom',$data);?>
  <?php } ?>

<?php if(!empty($contactus)){?>
<?php 
/*$data['ads_pages_top']=$advertisements;
$this->load->view('blocks/ads_pages_top',$data);*/?>
  <div class="figures contactlist">
  <?php  foreach($contactus as $contact){?>
    <div class="figure <?php if($contact['set_as_featured']==1) echo "featured_figure"; ?>">
      <div class="figure_content">
        <div class="figureDetails">
          <div class="row"> <div class="figure_title"> <?php echo $contact['title'];?></div>
            <label><span>Tel:</span> <?php echo $contact['phone'];?></label>
            <label><span>Email </span>: <a href="mailto:<?php echo $contact['email'];?>"><?php echo $contact['email'];?></a></label>
            <?php if(!empty($contact['website'])){?>
            <a target="_blank" class="figure_wesbite" <?php if(!empty($contact['website'])){?>href="<?php echo prep_url($contact['website']);?>"<?php }else{ ?>class="n-link"<?php } ?>><?php echo $contact['website'];?></a> 
            <?php } ?></div>
        </div>
         <?php if($contact['set_as_featured']==1  && !empty($contact['image'])){?> 
   <div class="figure_img">
        <span>
        <a  target="_blank" <?php if(!empty($contact['website'])){?>href="<?php echo prep_url($contact['website']);?>"<?php }else{ ?>class="n-link"<?php } ?>><img src="<?php echo base_url();?>uploads/contact_list/150x150/<?php echo $contact['image'];?>" /></a>
        </span>
         </div>
         <?php } ?>
      </div>
    </div>
    <?php }?>
    <?php }else{ ?>
    <div class="empty"><?php echo lang('no_data_found') ;?></div>
    <?php } ?>

  </div>
  
  <div class="pagination-row"><?php echo $this->pagination->create_links(); ?>  </div>
</div>
