<div class="innerContent">

<div  class="page-title border">
<h1>Question & Answer</h1>
</div>
<div class="question_and_answer_cont">
<?php foreach($answers as $val){?>
<div class="question_and_answer">
<div class="question_answer">
<div class="row">
<div class="ques_ans">
<span class="inner_ques_ans">
Q
</span>
</div>
<div class="question">
<div class="question_head"><h3><?php echo $val['name'];?></h3> <date><?php echo  date("F d,Y h:i:G", strtotime($val['created_date'])); ?></date></div>
<div class="question_content"><?php echo $val['question'];?></div>
</div>
</div>
<div class="row">
<div class="ques_ans">
<span class="inner_ques_ans">
A
</span>
</div>
<div class="answer">
<?php echo $val['answer'];?>
</div>
</div>
</div>

</div>
<?php } ?>
</div>
<div class="question_form">
<h2><b>Ask</b> a Question </h2>
<form id="questionForm" class="login-form" method="post" accept-charset="utf-8" action="<?php echo route_to('question_and_answer/question') ;?>">
<div class="row_form">
        <div class="form-item col-3">
          <input class="textbox" type="text" name="name" placeholder="Full Name" >
          <div id="name-error" class="form-error"></div> </div>
        <div class="form-item col-3">
          <input class="textbox" type="text" name="email" placeholder="E-mail" >
          <div id="email-error" class="form-error"></div> </div>
        <div class="form-item col-3">
          <input class="textbox" type="text" name="phone" placeholder="Phone" >
          <div id="phone-error" class="form-error"></div> </div>
          </div>
        <div class="form-item">
          <textarea class="textarea" name="question" placeholder="Question"></textarea>
          <div id="question-error" class="form-error"></div> </div>
        <div class="buttons-set ta-r">
            <div id="loader-bx"></div>
        <div class="FormResult"></div>
         <input type="submit" title="Send a question" value="Send" class="btn">  
          </div>
         </form>
</div>


</div>

<ul id="pagination" style="display:none;">
  <?php for($i=0;$i<$count;$i=$i+$show_items){
	 
		if(isset($_GET['per_page'])) {
		 $next=$_GET['per_page']+$show_items;
		  }else{
		$next=$offset+$show_items; }
	?>
  <li class="<?php if($i==$next) echo "next";?>"  ><a href="<?php echo route_to('question_and_answer?pagination=on&&per_page='.$i);?>"><?php echo  $next;?></a></li>
  <?php } ?>
</ul>