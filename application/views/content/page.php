<div class="innerContent" >
<div class="centered">

<?php echo $this->load->view('blocks/breadcrumbs',$breadcrumbs);?>
<div class="page-title">
<h1><?php echo $section['title'];?></h1>
</div>
<div class="description">
<?php if(!empty($section['description'])){?>
<?php echo $section['description'];?>
<?php }else{ ?>
<div class="empty">Coming Soon...</div>
<?php } ?>
</div>
</div>
</div>