<?php
$currentURL = current_url(); //http://myhost/main
$params   = $_SERVER['QUERY_STRING']; //my_id=1,3
$fullURL = $currentURL . '?' . $params;

$firstPart = strtok( $fullURL, '=' );
$allTheRest = strtok( '' );

?>
<div class="innerContent">
    <div class="row-breadcrumb-mrgn">
        <?php
        $data['breadcrumbs'] = $breadcrumbs;
        $this->load->view('blocks/breadcrumbs', $data); ?>

        <!--  <div  class="page-title">
    <h1><?php echo $selected_category['title']; ?></h1>
  </div>-->

        <div class="page-title border">
            <h1><?php echo $selected_category['title']; ?></h1>
        </div>
    </div>
                        <?php
                        $menu_categories=$this->fct->getAll_cond('categories','sort_order',array('id_parent'=>14,'section'=>categories_section(),'status'=>0));
                        $brands = $this->ecommerce_model->getBrandsByCategory($selected_category['id_categories']); ?>
                    <?php if (!empty($brands) && count($brands) > 1) { ?>
    <div class="row-breadcrumb-mrgn brand-details-breadcrumb" style="float:left;width:20%;margin:0 2% 0 0">
        <?php if (isset($_SERVER['HTTP_REFERER'])) { ?>
        <div class="searchPart">
            <h3 class="searchTitle">Refine Search</h3>
            <div class="reset-btns">
                <?php $previous_link = $_SERVER['HTTP_REFERER']; ?>
                <a style="display:none" class="back-link searchClear" href="<?php echo $previous_link; ?>">
                    <i class="fa fa-fw"></i> Back
                </a>
            </div><?php } ?>

            <?php $new_url = $current_url . '?brand=' . $this->input->get('brand'); ?>
            <div class="supplier_bx">
                <div class="supplier_brands">
                    <h3>Brands</h3>
                                <ul>
                            <?php foreach ($brands as $brand) { ?>
                                    <li class="supplier_brand">
                                        <input style="position: absolute;z-index: 0;" type="checkbox" name="ProductRange" value="<?php echo $brand['title']; ?>" <?php if($allTheRest == $brand['id_brands']){ ?> checked <?php } ?>>
                                        <a href="?brand=<?php echo $brand['id_brands']; ?>"><?php echo $brand['title']; ?></a>
                                    </li>
                            <?php } ?>
                                </ul>
                </div>
                <?php if($selected_category['id_categories'] == "14"){ ?>
                <div class="supplier_brands">
                    <h3>Category</h3>
                                <ul>
                            <?php foreach ($menu_categories as $menu_category) { ?>
                                    <li class="supplier_brand">
                                        <input style="position: absolute;z-index: 0;" type="checkbox" name="ProductRange" value="<?php echo $menu_category['title']; ?>" <?php if($allTheRest == $menu_category['title_url']){ ?> checked <?php } ?>>
                                        <a href="/<?php echo $menu_category['title_url']; ?>"><?php echo $menu_category['title']; ?></a>
                                    </li>
                            <?php } ?>
                                </ul>
                </div>
                <?php } ?>
<!--
                <div class="form-item autofomplete-form-item search_brand_keyword">
                    <input type="text" class="textbox onenter" name="keywords"
                           value="<?php echo $this->input->get('keywords'); ?>" id="keyowrd_autocomplete"
                           placeholder="Search by name, keyword or code"/></div>
                          -->
            </div>

            <!--<button class="searchBtn " type="submit">
            <i class="fa fa-search" aria-hidden="true"></i>
            </button>-->
            <?php if (isset($_GET['keywords'])) { ?>
                <a class="reset_btn"
                   href="<?php echo route_to('products/category/' . $selected_category['id_categories']); ?>">reset</a>
            <?php } ?>

            </form>
        </div>
    </div>
                    <?php } ?>
    <?php
    $select_category_img = "";
    $url_selected_category = "";
    if (isset($selected_category['image']) && !empty($selected_category['image'])) {
        $select_category_img = $selected_category['image'];
    }
    if (isset($selected_category) && !empty($selected_category)) {
        $url_selected_category = route_to('products/category/' . $selected_category['id_categories']);
    }

    ?>
    <?php if (!empty($sub_categories_cc)) { ?>
        <div class="sub_categories_list_bx <?php if (!empty($select_category_img)) {
            echo "mod";
        } ?>">
            <?php if (!empty($select_category_img)) { ?>
                <div class="sub_category_select_img">
                    <a alt="<?php echo $selected_category['title']; ?>"
                       title="<?php echo $selected_category['title']; ?>" href="<?php echo $url_selected_category; ?>">
                        <img src="<?php echo base_url(); ?>uploads/categories/295x295/<?php echo $select_category_img; ?>"/>
                    </a>
                    <a class="selected_category_h2" href="<?php echo $url_selected_category; ?>"
                       alt="<?php echo $selected_category['title']; ?>"
                       title="<?php echo $selected_category['title']; ?>"><?php echo $selected_category['title']; ?></a>
                </div>
            <?php } ?>
            <?php $this->load->view('blocks/sub_categories_list'); ?>
        </div>
    <?php } ?>

    <?php if ($selected_category['id_parent'] == 0) { ?>
        <div class="advertisements-cont">
            <div class="slick-advertisements">
                <?php foreach ($advertisements as $ads) {
                    ?>
                    <div class="item">
                        <div class="innerItem">
                            <img src="<?php echo base_url(); ?>uploads/banner_ads/<?php echo $ads['image']; ?>"/></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php /*?> <?php if(!empty($selected_category['image'])){ ?>
  <div class="category-img"> <img src="<?php echo base_url();?>uploads/categories/<?php echo $selected_category['image'];?>" /> </div>
  <?php } ?><?php */ ?>
    <?php if (!empty($selected_category['description']) && $selected_category['description'] != "") { ?>
        <div class="description"><?php echo $selected_category['description']; ?></div>
    <?php } ?>
    <?php if (!empty($sub_categories) && empty($all)) { ?>
        <div class="products-category-section categories-section  <?php if ($selected_category['id_parent'] != 0) {
            echo " mod";
        } ?>">

            <div class="products-category-cont products-container">

                <?php $j = 0;
                foreach ($sub_categories as $sub) {

                    $j++; ?>
                    <div class="product">
                        <div class="inner-product">

        <span class="product-img addImgEffect">
        <a href="<?php echo route_to('products/category/' . $sub['id_categories']); ?>">
          <?php if (!empty($sub['image'])) { ?>
              <img class="base_image" title="<?php echo $sub['title']; ?>" alt="<?php echo $sub['title']; ?>"
                   src="<?php echo base_url(); ?>uploads/categories/295x295/<?php echo $sub['image']; ?>"/>
          <?php } else { ?>
              <img class="base_image" title="<?php echo $sub['title']; ?>" alt="<?php echo $sub['title']; ?>"
                   src="<?php echo base_url(); ?>front/img/default_product.png"/>
          <?php } ?>
         </a>
        </span>

                            <div class="product-content box-sizing">
                                <span class="product-title"><a
                                            href="<?php echo route_to('products/category/' . $sub['id_categories']); ?>"><?php echo $sub['title']; ?></a></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    <?php } else { ?>

        <?php if (isset($_GET['brand']) || !empty($products)) { ?>
        <?php if (!empty($brands) && count($brands) > 1) { ?>
            <div style="width:78%;float:left" id="products_section"
                 class="products-category-section mod2<?php if ($selected_category['id_parent'] != 0 || !empty($all)) {
                     echo " mod2";
                 } ?>">
                <?php } else { ?>
                        <div style="width:100%;float:left" id="products_section"
                 class="products-category-section five-product mod2<?php if ($selected_category['id_parent'] != 0 || !empty($all)) {
                     echo " mod2";
                 } ?>">
                <?php } ?>

                <div class="products-category-cont products-container">

                    <?php
                    $data['products'] = $products;
                    $data['page'] = $page;
                    $this->load->view('blocks/products', $data);
                    ?>

                    <?php if ((isset($_GET['brand']) || isset($_GET['categories'])) && empty($products)) { ?>
                        <div class="empty bg">No products found</div>
                    <?php } ?>
                </div>
            </div>

            <div class="pagination-row"><?php echo $this->pagination->create_links(); ?></div>
        <?php } ?>
    <?php } ?>
</div>

<!--<ul id="pagination" style="display:none;">
  <?php for ($i = 0; $i < $count; $i = $i + $show_items) {

    if (isset($_GET['per_page'])) {
        $next = $_GET['per_page'] + $show_items;
    } else {
        $next = $offset + $show_items;
    }
    ?>
  <li class="<?php if ($i == $next) echo "next"; ?>"  ><a href="<?php echo route_to('products/category/' . $selected_category['id_categories'] . '?pagination=on&&per_page=' . $i); ?>"><?php echo $next; ?></a></li>
  <?php } ?>
</ul>-->