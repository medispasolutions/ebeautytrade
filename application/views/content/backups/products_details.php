<?php 
 $sizes="";
 $colors="";
 
 ///////////////////////////Attributes\\\\\\\\\\\\\\\\\\\
$attributes=$product['attributes'];
$gallery=$product['gallery'];
$brand=$product['brand'];
$informations=$product['informations'];
 ///////////////////////////PRICES\\\\\\\\\\\\\\\\\\\
	$first_available_stock = array();
	if(isset($product['first_available_stock']) && !empty($product['first_available_stock'])) {
		$first_available_stock = $product['first_available_stock'];
	}
	if(isset($first_available_stock) && !empty($first_available_stock)) {
		$quantity = $first_available_stock['quantity'];
		
		$list_price = $first_available_stock['list_price'];
		$price = $first_available_stock['price'];
		$retail_price = $first_available_stock['retail_price'];
		$discount = $first_available_stock['discount'];
		$discount_expiration = $first_available_stock['discount_expiration'];
		$stock_options = unSerializeStock($first_available_stock['combination']);
		$hide_price = $first_available_stock['hide_price'];
		$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$first_available_stock['id_products'],"id_stock"=>$first_available_stock['id_products_stock']));
//$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$product['id_products']));



	}
	else {
		$quantity = $product['quantity'];
		//echo $quantity;exit;
		$list_price = $product['list_price'];
		
		$retail_price = $product['retail_price'];
		$price = $product['price'];
		$discount = $product['discount'];
		$discount_expiration = $product['discount_expiration'];
		$hide_price = $product['hide_price'];
		$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$product['id_products']));
	}

?>

<div class="innerContent" >
  <?php 
$data['breadcrumbs']=$breadcrumbs;
$this->load->view('blocks/breadcrumbs',$data);?>
  <div class="page-title ">
    <?php if(isset($brand['title'])){?>
    <h1><?php echo $brand['title'];?></h1>
    <?php } ?>
    <a class="back-link" href="javascript: window.history.go(-1)"> <i class="fa fa-fw"></i> Back </a> </div>
  <!--GALLERY-->
  <?php if(!empty($gallery)){?>
  <div class="products_gallery">
    <div class="inner_products_gallery">
      <div class="load-bx">
        <div class="products_gallery_slick" >
          <?php foreach($gallery as $val){?>
          <div class="item" id="slide_<?php echo $val['id_gallery'];?>" onkeydown="zoomCall(<?php echo $val['id_gallery'];?>)"   onkeyup="zoomCall(<?php echo $val['id_gallery'];?>)"  onmouseout="zoomCall(<?php echo $val['id_gallery'];?>)" onmouseover="zoomCall(<?php echo $val['id_gallery'];?>)"  >
            <div class="innerItem"> 
              <!--<a rel="prettyPhoto[gallery1]" href="<?=base_url();?>uploads/products/gallery/<?php echo $val['image'] ; ?>"  class="zoom_icon popUpGallery"><div class="imgTable"><span class="imgCell"><i class="fa fa-search-plus" aria-hidden="true"></i>
<span class="zoom-lbl">Click To Enlarge</span></span></div></a>--> 
              <a href="#" > <img id="img_<?php echo $val['id_gallery'];?>" class="zoom-image"   data-image="<?php echo base_url() ;?>uploads/products/gallery/854x854/<?php echo $val['image'];?>" data-zoom-image="<?php echo base_url() ;?>uploads/products/gallery/<?php echo $val['image'];?>" src="<?php echo base_url() ;?>uploads/products/gallery/854x854/<?php echo $val['image'];?>" /> </a> </div>
          </div>
          <?php } ?>
        </div>
        <div class="products_thumb">
          <label class="text-mouseover" >Mouseover Image to Magnify.</label>
          <div class="click-to-enlarge"> <a id="popup_details" rel="prettyPhoto" href=""  class="popUpGallery">Open Larger Image</a> </div>
          <?php if(count($gallery)>1){?>
          <div class="inner_products_thumb">
            <div class="products_thumb_slick">
              <?php foreach($gallery as $val){?>
              <div class="item" id="thumb_<?php echo $val['id_gallery'];?>">
                <div class="innerItem"> <img src="<?php echo base_url() ;?>uploads/products/gallery/295x295/<?php echo $val['image'];?>" /> </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  
  <!--DETAILS-->
  <div class="products_details <?php if(empty($gallery)){ echo "row"; }?>">
    <form  action="<?php echo route_to('cart/add');?>" id="cart-form-details" <?php /*?>id="cart-formDetails-<?php echo $product['id_products'] ;?>"<?php */?> onsubmit="return validateForm()" method="post">
      <input type="hidden" name="product_id" id="id_product" value="<?php echo $product['id_products'] ;?>" />
      <?php if(isset($redeem)){?>
      <input type="hidden" name="redeem" id="redeem" value="<?php echo $redeem ;?>" />
      <?php } ?>
      <!--DETAILS 1-->
      
      <div  class="item-no">
        <label ><span class="lbl_t" style="display:block">Item No: </span><span  id="p-sku-<?php echo $product['id_products']; ?>"><?php echo $product['sku'];?></span></label>
      </div>
      <div class="products_details_1">
        <label><?php echo $product['title'] ;?></label>
        <?php if(!empty($product['brief'])){ ?>
        <!--<div class="description"><?php echo $product['brief'] ;?></div>-->
        <?php } ?>
      </div>
      
      <!--DETAILS 2-->
      <?php if($login ){?>
      <div class="retail_price_details" id="p-retail-price-<?php echo $product['id_products']; ?>" >
        <?php  if(!isset($redeem) && !empty($retail_price) && $retail_price>0){?>
        <span class="lbl_t" style="display:block"> <?php echo lang('suggester_retail_price');?>: </span><?php echo changeCurrency($retail_price);?>
        <?php } ?>
      </div>
      <?php } ?>
      <!--DETAILS 3-->
      <?php

 if(!empty($product['description'])){?>
      <div class="products_details_3  product_details_descrioption">
        <div class="inner_product_details_descrioption">
          <?php /*?><?php echo $product['description'];?><?php */?>
          <?php echo character_limiter(strip_tags($product['description']),290); ?> </div>
      </div>
      <a class="read_more">read more</a>
      <?php } ?>
    
 <?php 
 ///////////////////////////checkoptions\\\\\\\\\\\\\\\\\\\
 $bool_option=false;
 if(!empty($attributes)){
	
	foreach($attributes as $attr){
		if(!empty($attr['options'])){
			$bool_option=true;}}}?>
      <div class="row">
<?php /*?>        <?php

   if(isset($product['brand']['deliver_by_supplier']) && $product['brand']['deliver_by_supplier']==1){
	   $supplier_name=$this->fct->getonecell('user','trading_name',array('id_user'=>$product['brand']['id_user']));?>
        <div class="deliver_day mod"> <?php echo getDeliverText($supplier_name,$product['brand']['deliver_text']);?> </div>
        <?php } ?><?php */?>
        <?php if($login || checkProductGroup()){?>
        <!--OPTIONS-->
        <div  id="p-stock-<?php echo $product['id_products']; ?>"  class="stock-status">
          <?php if(false && !$bool_option && $product['quantity']<1 && empty($product['stock_status'])){?>
          <span class="not_available">Out of Stock</span>
          <?php } ?>
        </div>
      
	  <?php } ?>
      </div>
      <div class="row">
     <div class="content_details_left">  
      <?php if($login || checkProductGroup()){?> 
      <div class="products_details_2">

        <label >
        <?php if($login){?>
        <?php if(!isset($redeem)){?>
        <span class="lbl_t" style="display:block">Trade Price: </span>
        <div class="p-price" id="p-price-<?php echo $product['id_products']; ?>">
          <?php if($bool_option) { echo '<span class="price">based on selection</span>'; }else{?>
          <?php if($hide_price == 0) {?>
          <?php if(displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price,$discount_expiration,$price)) {?>
          <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span> <span class="price old_price" itemprop="price"><?php echo changeCurrency(displayWasCustomerPrice($list_price)); ?></span>
          <?php } else {?>
          <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span>
          <?php }?>
          <?php } }?>
        </div>
        <?php }else{ ?>
        <div class="p-price" id="p-price-<?php echo $product['id_products']; ?>">
          <?php if($bool_option) { echo '<span class="price">'.lang('miles_based_on_selection').'</span>'; }else{?>
          <?php if($hide_price == 0) {?>
          <span class="price new_price"><?php echo $product['miles'].' miles'; ?></span>
          <?php }?>
        </div>
        <?php } ?>
        <?php /*?> <?php if(!isset($redeem) && !empty($retail_price) && $retail_price>0 ){?>
<div class="products_details_2 retail_price_d" style="min-height:inherit;">
<div class="row"><label><span class="lbl_t" style="display:block"><?php echo lang('suggester_retail_price');?>: </span><?php echo changeCurrency($retail_price);?></label></div>
</div>
<?php } ?><?php */?>
        </label>
        <?php } ?>
        <?php } ?>
      </div>
      <?php } ?>
      <div class="container_attributes">
        <?php 
 ///////////////////////////Attributes\\\\\\\\\\\\\\\\\\\
$attributes=$product['attributes'];

foreach($attributes as $attr){
	$bool=true;
	if($attr['id_attributes']==1){
	$bool=false;
	$sizes=$attr['options'];
	if(!empty($sizes)){ ?>
        <div class="option_container">
          <div class="options_list">
            <dl class="product-options-dl">
              <dt id="color_option" class="product-options-dt">
                <label class="required"> Pick Size <em>*</em> </label>
              </dt>
              <dd class="product-options-dd">
                <div class="form-item mod" id="attr_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>">
                  <?php foreach($sizes as $size){
		 ;?>
                  <a class="op op_size" title="<?php echo $size['title'];?>" alt="<?php echo $size['title'];?>"  > <?php echo $size['title'];?>
                  <input type="hidden" value="<?php echo $size['id_attribute_options'];?>">
                  <input type="hidden" id="option_val_<?php echo $size['id_attribute_options'];?>"  value="<?php echo $size['title'];?>"/>
                  </a>
                  <?php  } ?>
                  <input type="hidden"  name="options[]"  id="attr_input_<?php echo $attr['id_attributes'];?>" class="req_input" />
                  <div id="attr-error-<?php echo $attr['id_attributes'];?>" class="form-error mod">The <?php echo $attr['title'];?> field is required.</div>
                </div>
              </dd>
            </dl>
          </div>
          <div  id="attr_select_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>" class="option_select">
            <dl class="product-options-dl">
              <dt class="product-options-dt" >
                <label class="required"> Your Selection {Size} </label>
              </dt>
              <dd class="product-options-dd">
                <div class="selected_option">
                  <div class="imgTable">
                    <div class="imgCell">
                      <div class="selected_option_val"></div>
                    </div>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
        </div>
        <?php }} ?>
        <?php if($attr['id_attributes']==2){
	$bool=false;
	$colors=$attr['options'];
	if(!empty($colors)){
		 ?>
        <div class="option_container">
          <div class="options_list">
            <dl class="product-options-dl">
              <dt id="color_option" class="product-options-dt">
                <label class="required"> Pick Color <em>*</em> </label>
              </dt>
              <dd class="product-options-dd">
                <div class="form-item mod" id="attr_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>">
                  <?php foreach($colors as $color){?>
                  <a class="op op_color " title="<?php echo $color['title'];?>" alt="<?php echo $color['title'];?>" style="background:#<?php echo $color['option'];?>" >
                  <input type="hidden" value="<?php echo $color['id_attribute_options'];?>">
                  <input type="hidden" id="option_val_<?php echo $color['id_attribute_options'];?>"  value="<?php echo $color['option'];?>"/>
                  </a>
                  <?php  } ?>
                  <input type="hidden"  name="options[]"  id="attr_input_<?php echo $attr['id_attributes'];?>"  class="req_input"/>
                  <div id="attr-error-<?php echo $attr['id_attributes'];?>" class="form-error mod">The <?php echo $attr['title'];?> field is required.</div>
                </div>
              </dd>
            </dl>
          </div>
          <div  id="attr_select_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>" class="option_select">
            <dl class="product-options-dl">
              <dt class="product-options-dt" >
                <label class="required"> Your Selection {Color} </label>
              </dt>
              <dd class="product-options-dd">
                <div class="selected_option">
                  <div class="imgTable">
                    <div class="imgCell">
                      <div class="selected_option_val"></div>
                    </div>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
        </div>
        <?php }}

if($bool){
	$bool=true;
	
	$options=$attr['options'];
	if(!empty($options)){	 ?>
        <div class="option_container">
          <div class="options_list">
            <dl class="product-options-dl">
              <dt id="color_option" class="product-options-dt">
                <label class="required"> <?php echo $attr['title'];?> <em>*</em> </label>
              </dt>
              <dd class="product-options-dd">
                <div class="form-item mod" id="attr_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>">
                  <?php foreach($options as $op){?>
                  <a class="op op_size" title="<?php echo $op['title'];?>" alt="<?php echo $op['title'];?>"  > <?php echo $op['title'];?>
                  <input type="hidden" value="<?php echo $op['id_attribute_options'];?>">
                  <input type="hidden" id="option_val_<?php echo $op['id_attribute_options'];?>"  value="<?php echo $op['title'];?>"/>
                  </a>
                  <?php  } ?>
                  <input type="hidden"  name="options[]"  id="attr_input_<?php echo $attr['id_attributes'];?>" class="req_input" />
                  <div id="attr-error-<?php echo $attr['id_attributes'];?>" class="form-error mod">The <?php echo $attr['title'];?> field is required.</div>
                </div>
              </dd>
            </dl>
          </div>
          <div  id="attr_select_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>" class="option_select">
            <dl class="product-options-dl">
              <dt class="product-options-dt" >
                <label class="required"> Your Selection {<?php echo $attr['title'];?>} </label>
              </dt>
              <dd class="product-options-dd">
                <div class="selected_option">
                  <div class="imgTable">
                    <div class="imgCell">
                      <div class="selected_option_val"></div>
                    </div>
                  </div>
                </div>
              </dd>
            </dl>
          </div>
        </div>
        <?php } ?>
        <?php }} ?>
      </div>

      
      <div class="action-products-details"> 
      <div  id="p-btn-<?php echo $product['id_products']; ?>" > 
  			<div class="segements"  id="r-PriceSegments-<?php echo $product['id_products']; ?>" >
            <?php if(!empty($price_segments) && $login) {

	 ?>
            <select id="qty_segements" class="combobox">
              <?php foreach($price_segments as $segment) {?>
              <?php $price_segments = ''.$segment['min_qty'].' for '.changeCurrency($segment['price'],true).' each'; ?>
              <option value="<?php echo $segment['min_qty']; ?>"><?php echo $price_segments; ?></option>
              <?php }?>
            </select>
            <?php }else{?>
            <input   class="textbox qty-txt" id="qty" type="text" name="qty"  value="1">
            <?php } ?>
          </div>
         
         <div class="product_details_btns">
          <?php if($login){
		  $login_txt="Add to Cart";
		   }else{
		 $login_txt="Please Login"; } ?>
          
          <a  class="btn-cart add_to_cart_btn popover_c" id="btn-cart-<?php echo $product['id_products'] ;?>" onclick="checkAccessCode(<?php echo $product['id_products'] ;?>)"  type="submit" value="+ Add to Cart"> Add to Cart
          <?php if(!$login){?>
          <span class="popover_m"><?php echo $login_txt;?></span>
          <?php } ?>
          </a>
          
         
        
          <?php if($login){?>
<div class="stock_list" id="stock_list"> 
	<span class="stock-list-m stock-list-heart"  id="stock-list-m1" <?php if(!empty($product['favorites']) && empty($attributes)){}else{ echo "style='display:none'" ;}?>><img alt="<?php echo lang('add_my_list');?>" title="<?php echo lang('add_my_list');?>" src="<?php echo base_url();?>front/img/heart.png" /></span> <a class="stock-list-m btn-cart btn-wishlist" id="stock-list-m2" <?php if(!empty($product['favorites']) && empty($attributes)){echo "style='display:none'";}?>  onclick="checkStockList(<?php echo $product['id_products'] ;?>)" title=" For easy re-ordering" alt=" For easy re-ordering" ><i class="fa fa-heart-o" aria-hidden="true"></i> Add to "Re-order List"</a> </div>
          <?php } ?>
          </div>
        </div>
        <div class="choosed_segement"></div>
      </div>
      
      <!--DETAILS 4-->
      <div class="products_details_4">
        <div id="p-miles-<?php echo $product['id_products'];?>" <?php if(isset($redeem)) echo "style='display:none'";?>>
          <?php 

if(isset($product['miles']) && $product['set_general_miles']==1 && $product['miles']>0 && !$bool_option){ ?>
          <div class="earned"> <i class="icon"> <img src="<?php echo base_url();?>front/img/miles.png"> </i>Earn <span class="miles-label"> <?php echo round($product['miles']);?></span> miles with this purchase </div>
          <?php } ?>
        </div>
        <?php if(!empty($product['datasheet'])){?>
        <a target="_blank" class="print_btn" href="<?php echo base_url();?>uploads/products/<?php echo $product["datasheet"];?>"><i class="fa fa-fw"></i>Printable Datasheet</a>
        <?php } ?>
        <?php if(!empty($product['catalog'])){?>
        <a target="_blank" class="print_btn" href="<?php echo prep_url($product['catalog']);?>"> <i class="fa fa-file-word-o" aria-hidden="true"></i> Catalog</a>
        <?php } ?>
      </div>
      
      </div>
      
      <?php $suppliers = $this->custom_fct->getSuppliers(array('list'=>1,'id_brands'=>$product['brand']['id_brands']),1,0);?>

	
    <?php
	$brand=$product['brand'];
	$current_url=current_url();

	 foreach($suppliers as $supplier){
$terms=getTerms($supplier);

	 $brands=$supplier['brands']; 
	 $count=count($brands);
	 $url_logo="";	
		
		?>
        
        </form>
      <div class="supplier_product_bx supplier_blk">
      
      <div class="supplier_product_img">
     <a class="addImgEffect"  title="<?php echo $supplier['trading_name'];?>" alt="<?php echo $supplier['trading_name'];?>" href="<?php echo route_to('brands/details/'.$brands[0]['id_brands']);?>">
  <?php if(!empty($supplier['logo'])){ 
$url_logo=base_url().'uploads/user/'.$supplier['logo'];
  }else{
	  if(isset($brands[0]['logo']) && !empty($brands[0]['logo'])){
$url_logo=base_url().'uploads/brands/112x122/'.$brands[0]['logo']; }
	  } ?>
      <?php if(!empty($url_logo)){?>
 <img alt="<?php echo $supplier['trading_name'];?>" title="<?php echo $supplier['trading_name'];?>" src="<?php echo $url_logo;?>" />
 <?php } ?>


 </a>
      </div>
      <div class="supplier_info">
      <ul>
      <?php foreach($terms as $term){?>
      <li>• <?php echo $term;?></li>
      <?php } ?>

      </ul>
      </div>
      
	<div class="row">
  <a class="btn supplier_phone" <?php if($login){?>onclick="showPhone(this,<?php echo $brand['id_brands'];?>, <?= $product['id_products'] ?>)"<?php }else{ ?>onclick="checkUrlIfLogin('<?php echo $current_url;?>')"  <?php } ?>>
  <span class="call_supplier">
  <i class="fa fa-phone" aria-hidden="true"></i> Call Supplier
  </span>
  <?php echo $brand['phone'];?>
  </a>
  <a class="btn" <?php if($login){?>onclick="meetingRequest()"<?php }else{ ?>onclick="checkUrlIfLogin('<?php echo $current_url;?>')"  <?php } ?>>
<i class="fa fa-envelope" aria-hidden="true"></i>
  <?php if(!empty($brand['button_text'])){?>
  <?php echo $brand['button_text'];?>
  <?php }else{ ?>
  <!-- Meeting Request-->
  Send Enquiry
  <?php } ?>
  </a>
  </div>
    
  <div class="meeting_request mod" style="z-index: 100">
  <a class="close_meeting" onclick="$('.meeting_request').slideUp('fast');">Close</a>
<form id="meetingRequest" class="login-form" method="post" accept-charset="utf-8" action="<?php echo route_to('brands/send')?>">
        <input  type="hidden" name="id_brands" value="<?php echo $brand['id_brands'];?>">
        <input  type="hidden" name="id_products" value="<?php echo $product['id_products'];?>">
<?php
$currentUserData = getCurrentUserData();
?>
        <div class="form-item">
          <input class="textbox" type="text" name="name" placeholder="Full Name" value="<?= $currentUserData ? $currentUserData['first_name']." ".$currentUserData['last_name'] : '' ?>">
          <div id="name-error" class="form-error"></div> </div>
        <div class="form-item">
          <input class="textbox" type="text" name="email" placeholder="E-mail" value="<?= $currentUserData['email'] ?>">
          <div id="email-error" class="form-error"></div> </div>
        <div class="form-item">
          
      
          <input class="textbox mobile-m" type="text" name="phone" placeholder="Phone" value="<?= $currentUserData['phone'] ?>">
        
          <div id="phone-error" class="form-error"></div>
          </div>
        <div class="form-item">
          <textarea class="textarea" name="message" placeholder="Inquiry"></textarea>
          <div id="message-error" class="form-error"></div> </div>

        <div class="form-item">
            <div class="g-recaptcha" id="enquiry-captcha" data-sitekey="6Le5kkMUAAAAAFyu9LqReVIG0Xi_EMLvbfVjAnft" data-size="compact"></div>
            <div id="g-recaptcha-response-error" class="form-error"></div>
        </div>

        <div class="buttons-set ta-r">
            <div id="loader-bx"></div>
        <div class="FormResult"></div>

         <input type="submit" title="Send Request" value="Send" class="btn">  
          </div>
         </form>
   </div>
      </div>
      <?php } ?>
      </div>
      
      <?php if(!empty($informations)){?>
      <div class="tabs_cont">
        <ul class="tabs">
          <?php foreach($informations as $info){?>
          <li><a href="#tab<?php echo $info['id_product_information'];?>"><?php echo $info['title'];?></a></li>
          <?php } ?>
        </ul>
        <div class="tab_container">
          <?php foreach($informations as $info){?>
          <div id="tab<?php echo $info['id_product_information'];?>" class="tab_content">
            <div class="description"> <?php echo $info['description'];?> </div>
            <?php if(!empty($info['video'])){?>
            <div class="product_info_video">
              <iframe src="https://www.youtube.com/embed/<?php echo getYoutubeCode($info['video']);?>"></iframe>
            </div>
            <?php } ?>
            <?php if(!empty($info['file'])){?>
            <div class="download_product_info"><a class="btn_download" download href="<?php echo base_url();?>uploads/product_information/<?php echo $info['file'];?>"><i class="fa fa-fw"></i>DOWNLOAD FILE</a></div>
            <?php } ?>
            <?php if(!empty($info['file2'])){?>
            <div class="download_product_info"><a class="btn_download" download href="<?php echo base_url();?>uploads/product_information/<?php echo $info['file2'];?>"><i class="fa fa-fw"></i>DOWNLOAD FILE</a></div>
            <?php } ?>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php } ?>
  

  </div>
</div>
<?php if(!empty($related_products )){?>
<div class="related_product">
  <div class="title-h2">
    <h2>Related products</h2>
  </div>
  <div class="row">
    <div class="related_products_slick_cont">
      <div class="related_products_slick">
        <?php $j=0; 
foreach($related_products as $product){
	

		 $image = '';
					 if(isset($product['gallery']) && !empty($product['gallery']))
					 $image = $product['gallery'][0];
					 $attributes = $product['attributes'];
					$first_available_stock = array();
					if(isset($product['first_available_stock']) && !empty($product['first_available_stock'])) {
						$first_available_stock = $product['first_available_stock'];
					}
					if(isset($first_available_stock) && !empty($first_available_stock)) {
						$quantity = $first_available_stock['quantity'];
						$list_price = $first_available_stock['list_price'];
						$price = $first_available_stock['price'];
						$sku = $first_available_stock['sku'];
						$retail_price = $first_available_stock['retail_price'];
						$discount = $first_available_stock['discount'];
						$discount_expiration = $first_available_stock['discount_expiration'];
						$stock_options = unSerializeStock($first_available_stock['combination']);
						$hide_price = $first_available_stock['hide_price'];
						$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$first_available_stock['id_products'],"id_stock"=>$first_available_stock['id_products_stock']));
						//print '<pre>';print_r($stock_options);exit;
					}
					else {
						$quantity = $product['quantity'];
						$list_price = $product['list_price'];
						$price = $product['price'];
						$sku = $product['sku'];
						$retail_price = $product['retail_price'];
						$discount = $product['discount'];
						$discount_expiration = $product['discount_expiration'];
						$hide_price = $product['hide_price'];
						$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$product['id_products']));
					}
			$j++; ?>
        <div class="product">
          <div class="inner-product">
            <?php if($product['set_as_new']==1) { ?>
            <span class="non-exportable-img"><img src="<?php echo base_url();?>front/img/new-arrivals.png" /></span>
            <?php } ?>
            <?php if($product['set_as_clearance']==1) { ?>
            <span class="non-exportable-img"><img src="<?php echo base_url();?>front/img/clearance.png" /></span>
            <?php } ?>
            <?php if($product['set_as_soon']==1) { ?>
            <span class="non-exportable-img"><img src="<?php echo base_url();?>front/img/soon.png" /></span>
            <?php } ?>
            <?php if($discount>0) { ?>
            <span class="non-exportable-img"><img src="<?php echo base_url();?>front/img/offers.png" /></span>
            <?php } ?>
            <?php if(isset($page) && $page=="miles" && !empty($product['redeem_miles']) && $product['set_as_redeemed_by_miles']==1){ ?>
            <div class="miles_title"> <?php echo $product['redeem_miles'].' Miles';?> </div>
            <?php } ?>
            <span class="product-img addImgEffect"> <a href="<?php echo route_to('products/details/'.$product['id_products']);?>">
            <?php if(!empty($product['gallery'][0]['image'])){?>
            <img  class="base_image" title="<?php echo $product['title'];?>"  alt="<?php echo $product['title'];?>"   src="<?php echo base_url();?>uploads/products/gallery/295x295/<?php echo $product['gallery'][0]['image'];?>" />
            <?php }else{?>
            <img  class="base_image" title="<?php echo $product['title'];?>"  alt="<?php echo $product['title'];?>"  src="<?php echo base_url();?>front/img/default_product.png" />
            <?php } ?>
            </a> </span>
            <div class="product-content box-sizing"> 
              <!--<span class="product-title"><a href="<?php echo route_to('products/details/'.$product['id_products']);?>"><?php echo $product['title'] ;?></a></span>-->
              
              <div class="product-description description mod">
                <?php /*?><?php echo character_limiter(strip_tags($product['brief']),68); ?><?php */?>
                <!--  <div class="row"><?php echo $product['brief_one'];?></div>
          <div class="row"><?php echo $product['brief_two'];?></div>-->
                <div class="row"><?php echo character_limiter(strip_tags($product['brief']),150); ?></div>
              </div>
              <!--<div class="row" style="margin-top:7px;"><label><strong>SKU:</strong> </span><?php echo $sku;?></label></div> -->
              
              <?php if(!$login){ ?>
              <div class="row retail_price">
                <?php if(!empty($retail_price) && $retail_price>0){?>
                <!--<label><strong><?php echo lang('suggester_retail_price');?>:</strong> </span><?php echo changeCurrency($retail_price);?></label>-->
                <?php } ?>
              </div>
              <?php } ?>
              <?php if($login){ ?>
              <?php 
 ///////////////////////////checkoptions\\\\\\\\\\\\\\\\\\\
 $bool_option=false;
/*if(!empty($attributes)){
	foreach($attributes as $attr){
		if(!empty($attr['options'])){
			$bool_option=true;}}}*/
			
if(!empty($first_available_stock)){
	$bool_option=true;}			
?>
              <div class="products_details_2">
                <?php if(isset($page) && $page=="miles" && !empty($product['redeem_miles']) && $product['set_as_redeemed_by_miles']==1){ ?>
                <label>
                <div class="p-price"> <span class="price new_price"><b>Miles: </b><?php echo $product['redeem_miles'];?></span> </div>
                </label>
                <?php }else{ ?>
                <label >
                <span class="lbl_t">Price: </span>
                <div class="p-price" >
                  <?php if($bool_option) { echo '<span class="price price_selected_option">'.lang('based_on_selection').'</span>'; }else{?>
                  <?php if($hide_price == 0) {?>
                  <?php if(displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price,$discount_expiration,$price)) {?>
                  <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span> <span class="price old_price" itemprop="price"><?php echo changeCurrency(displayWasCustomerPrice($list_price)); ?></span>
                  <?php } else {?>
                  <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span>
                  <?php }?>
                  <?php } }?>
                </div>
                </label>
                <?php } ?>
              </div>
              <form id="cart-form-<?php echo $product['id_products'] ;?>"  action="<?php echo site_url('cart/add');?>" style="display:none;"  method="post">
                <input type="hidden" name="product_id" value="<?php echo $product['id_products'] ;?>" />
              </form>
              <?php } ?>
              <div class="row ta-r">
                <?php if($product['set_as_redeemed_by_miles']==1){ ?>
                <span class="smily-face"><img  title="<?php echo  $product['redeem_miles'];?> miles" alt="<?php echo  $product['redeem_miles'];?> miles" src="<?php echo base_url();?>front/img/smily-face.png" /></span>
                <?php } ?>
                <?php if($login){
		  $login_txt="+ Add to Cart";
		   }else{
		 $login_txt="+ Add to Cart"; } ?>
                <a class="add_to_cart btn-cart" id="product-<?php echo $product['id_products'] ;?>">+ Add to Cart</a> </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php if(!empty($products_viewed)){
	
	
	?>
<div class="related_product products_viewed">
  <div class="title-h2">
    <h2>Recently Viewed</h2>
  </div>
  <div class="row load-bx">
    <div class="related_products_slick_cont arrows_slider_on">
      <div class="related_products_slick">
        <?php 
$j=0;

foreach($products_viewed as $product){$j++?>
        <?php 
	  $data['product']=$product;
	  $data['page']='details';
	  $data['j']=$j;
	  $data['login']=$login;
	  $this->load->view('blocks/product',$data);?>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
</div>
