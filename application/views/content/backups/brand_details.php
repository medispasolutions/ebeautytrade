<div class="innerContent <?= $brand['promote_to_front'] == 1 ? "brands-n-header" : "" ?>">
    <?php
    $data['breadcrumbs'] = $breadcrumbs;
    $this->load->view('blocks/breadcrumbs', $data); ?>

    <div class="page-title border">
        <h1><?php echo $brand['title']; ?></h1>
    </div>
    <?php if ($brand['promote_to_front'] != 1) { ?>
    <div class="row">
        <div class="brands_details">
            <div class="inner_brands_details">
                <div class="advertisements-cont brand-section">
                    <div class="inner-brand-section">
                        <?php foreach ($suppliers as $supplier) {
                            $data['supplier'] = $supplier;
                            $this->load->view('blocks/supplier', $data);
                        } ?>

                        <div class="advertsiement-slider">
                            <div class="slick-advertisements">
                                <?php for ($i = 0; $i < 5; $i++) { ?>
                                    <?php if (!empty($brand['slide_' . $i])) { ?>
                                        <div class="item">
                                            <div class="innerItem">
                                                <img src="<?php echo base_url(); ?>uploads/brands/735x290/<?php echo $brand['slide_' . $i]; ?>"/>
                                            </div>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                        </div>

                        <?php if (!empty($brand['name'])) { ?>
                            <div class="supplier_cont">
                                <div class="supplier">
                                    <a id="meetingRequest" href="#"></a>
                                    <div class="inner_supplier">
                                        <div class="supplier_blk">

<span class="supplier_img">  <?php if (!empty($brand['photo'])) { ?>
      <img src="<?php echo base_url(); ?>uploads/brands/<?php echo $brand['photo']; ?>"/>  <?php } ?></span>

                                            <span class="supplier_name"><?php echo $brand['name']; ?></span>
                                            <label><?php echo $brand['position']; ?></label>

                                            <?php /*?>  <?php if($login){ ?>
<label><?php echo $brand['phone'];?></label>
<?php } ?><?php */ ?>
                                            <div class="brand_logo_bx">
                                                <?php if (!empty($brand['logo'])) { ?>
                                                    <a <?php if (!empty($brand['website_url'])) { ?> target="_blank" href="<?php echo prep_url($brand['website_url']); ?>"<?php } ?>
                                                            class="brand_logo <?php if (empty($brand['website_url'])) {
                                                                echo "no-pointer";
                                                            } ?>"> <img
                                                                src="<?php echo base_url(); ?>uploads/brands/<?php echo $brand['logo']; ?>"/></a>
                                                <?php } ?></div>

                                            <div class="row">
                                                <a class="btn supplier_phone"
                                                   <?php if ($login){ ?>onclick="showPhone(this,<?php echo $brand['id_brands']; ?>)"
                                                   <?php }else{ ?>onclick="checkUrlIfLogin('<?php echo $current_url; ?>')" <?php } ?>>
<span class="call_supplier">
<i class="fa fa-phone" aria-hidden="true"></i> Call Supplier
</span>

                                                    <?php echo $brand['phone']; ?>
                                                </a>
                                                <a class="btn" <?php if ($login){ ?>onclick="meetingRequest()"
                                                   <?php }else{ ?>onclick="checkUrlIfLogin('<?php echo $current_url; ?>')" <?php } ?>>
                                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                                    <?php if (!empty($brand['button_text'])) { ?>
                                                        <?php echo $brand['button_text']; ?>
                                                    <?php } else { ?>
                                                        <!-- Meeting Request-->
                                                        Send Enquiry
                                                    <?php } ?>
                                                </a>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="meeting_request mod">

                                        <a class="close_meeting" onclick="$('.meeting_request').slideUp('fast');">Close
                                            <!--<i class="fa fa-fw"></i>--></a>
                                        <form id="meetingRequest2" class="login-form" method="post"
                                              accept-charset="utf-8"
                                              action="<?php echo route_to('brands/send'); ?>">
                                            <input type="hidden" name="id_brands"
                                                   value="<?php echo $brand['id_brands']; ?>">
                                            <?php
                                            $currentUserData = getCurrentUserData();
                                            ?>
                                            <div class="form-item">
                                                <input class="textbox" type="text" name="name"
                                                       placeholder="Full Name"
                                                       value="<?= $currentUserData ? $currentUserData['first_name'] . " " . $currentUserData['last_name'] : '' ?>">
                                                <div id="name-error" class="form-error"></div>
                                            </div>
                                            <div class="form-item">
                                                <input class="textbox" type="text" name="email" placeholder="E-mail"
                                                       value="<?= $currentUserData['email'] ?>">
                                                <div id="email-error" class="form-error"></div>
                                            </div>
                                            <div class="form-item">


                                                <input class="textbox mobile-m" type="text" name="phone"
                                                       placeholder="Phone" value="<?= $currentUserData['phone'] ?>">

                                                <div id="phone-error" class="form-error"></div>
                                            </div>
                                            <div class="form-item">
                                                <textarea class="textarea" name="message"
                                                          placeholder="Inquiry"></textarea>
                                                <div id="message-error" class="form-error"></div>
                                            </div>

                                            <div class="form-item">
                                                <div class="g-recaptcha" id="enquiry-captcha"
                                                     data-sitekey="6Le5kkMUAAAAAFyu9LqReVIG0Xi_EMLvbfVjAnft"
                                                     data-size="compact"></div>
                                                <div id="g-recaptcha-response-error" class="form-error"></div>
                                            </div>
                                            <div class="buttons-set ta-r">
                                                <div id="loader-bx"></div>
                                                <div class="FormResult"></div>
                                                <input type="submit" title="Send Request" value="Send" class="btn">
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        <?php } ?>
                    </div>
                    <div class="brand-section s-brand-section">
                        <div class="inner-brand-section">
                            <div class="advertsiement-slider <?php if (empty($brand['video'])) {
                                echo "mod";
                            } ?>">
                                <?php if (!empty($brand['overview'])) { ?>
                                    <span class="overview">Overview</span>
                                    <div class="description readmore"><?php echo $brand['overview']; ?></div>

                                    <?php if (!empty($brand['catalog'])) { ?>
                                        <div class="download_product_info"><a class="btn_download" target="_blank"
                                                                              href="<?php echo base_url(); ?>uploads/brands/<?php echo $brand['catalog']; ?>"><i
                                                        class="pdf-icon"><img
                                                            src="<?php echo base_url(); ?>front/img/PDF-Icon.png"/></i>Download
                                                Catalog</a></div>
                                    <?php } ?>

                                <?php } ?>
                            </div>

                            <?php if (!empty($brand['video'])) { ?>
                                <div class="supplier_video">

                                    <a class="popup_call supplier_video_player" onclick="return false"
                                       href="<?php echo route_to('brands/getVideo/' . $brand['id_brands']); ?>">
                                        <div class="imgTable">
                                            <div class="imgCell"><i class="fa fa-fw"></i></div>
                                        </div>
                                    </a>
                                    <div class="imgTable">
                                        <div class="imgCell">
                                            <img src="http://img.youtube.com/vi/<?php echo getYoutubeCode($brand['video']); ?>/0.jpg"/>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?php
    }
    //end promote to front
    ?>
    <?php if ((isset($products) && !empty($products)) || (isset($categories) && !empty($categories)) || isset($_GET['keywords']) || $this->input->get('categories') != "") { ?>
        <div id="products_section" class="products-category-section <?php if ($brand['promote_to_front'] == 1) {
            echo "mod";
        } ?>">
            <div class="title-h2">
                <!--<h2>Facial Products</h2>-->

                <!--      <?php if (isset($category['title'])) { ?>
	  <h2><?php echo $category['title']; ?></h2>
	  <?php } ?>-->

                <?php if (isset($parent_levels) && !empty($parent_levels)) { ?>
                    <?php /*?><ul class="breadcrumb-categories">
      <li><a href="<?php echo route_to('brands/details/'.$brand['id_brands']);?>"><b><?php echo $brand['title'];?></b></a></li>
      <?php  foreach($parent_levels as $val){$url=route_to('brands/details/'.$brand['id_brands']).'?category='.$val['id_categories'];?>
     <li>
     <?php if($category['id_categories']!=$val['id_categories']){?>
     <a href="<?php echo $url;?>">
     <?php } ?>
	 <?php echo $val['title'];?>
      <?php if($category['id_categories']!=$val['id_categories']){?>
     </a> <?php } ?>
     </li>
      <?php } ?>
      </ul><?php */ ?>
                <?php } ?>


                <!--<div class="sort_order_action">
      <input type="hidden" value="<?php echo $brand['id_brands']; ?>" id="id_brands" />
      <div class="sort_order search_brand_keyword">
      <input type="text"  class="textbox onenter" value="<?php echo $this->input->get('keywords'); ?>"  id="keyowrd_autocomplete"  placeholder="Search by keyword" />
      <form  id="form_search" action="<?php echo route_to('brands/details/' . $brand['id_brands']); ?>" style="display:none;">
      <input type="hidden" name="keywords" class="textbox" value="<?php echo $this->input->get('keywords'); ?>" id="keyowrds"  />
      <input type="hidden" name="sort"  value="<?php echo $this->input->get('sort'); ?>"  />
      </form>
      </div>
      <div class="sort_order">
        <label>Sort by</label>
        <?php $new_url = $current_url; ?>
       <select class="textbox selectbox"  onchange="setLocation(this.value)" >
 <option value="<?php echo $new_url . '&sort='; ?>" >Select</option>

<option value="<?php echo $new_url . '&sort=name'; ?>"  <?php if (isset($_GET['sort']) && $_GET['sort'] == "name") {
                    echo "selected";
                } ?> >Name</option>


<?php if ($login) { ?>
<option value="<?php echo $new_url . '&sort=descending'; ?>"  <?php if (isset($_GET['sort']) && $_GET['sort'] == "descending") {
                    echo "selected";
                } ?> > Price</option>
<?php } ?>

</select>
      </div>
      </div>-->

            </div>


            <!--//////////////////////////CATEGORIES PRODUCTS////////////////-->
            <!--<div class="refine-search" >
  <form method="get" action="<?php echo route_to('brands/details/' . $brand['id_brands']); ?>?pagination=1">
  <div class="row" id='search-brand'>
  
<?php $this->load->view('load/loadSearchBrand'); ?>
</div>

<button class="searchBtn" type="submit">
<i class="fa fa-search" aria-hidden="true"></i>
</button>
</form>
 </div>-->
            <div class="row-breadcrumb-mrgn brand-details-breadcrumb">


                <!--  <div  class="page-title">
    <h1><?php echo $selected_category['title']; ?></h1>
  </div>-->

                <div class="row">
                    <div class="reset-btns">

                        <?php if (isset($_SERVER['HTTP_REFERER'])) { ?>
                            <?php $previous_link = $_SERVER['HTTP_REFERER']; ?>
                            <a class="back-link" href="<?php echo $previous_link; ?>">
                                <i class="fa fa-fw"></i> Back
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="refine-search brand-search  <?php if (isset($_GET['keywords'])) {
                    echo "reset-search";
                } ?>">
                    <?php $new_url = $current_url; ?>
                    <?php if (!isset($category['id_categories'])) {
                        $category['id_categories'] = "";
                    } ?>

                    <input type="hidden" id="id_brands" value="<?php echo $brand['id_brands']; ?>"/>
                    <div class="inner-search-bx">

                        <!--    <label class="search_by"><?php if (isset($category['title'])) {
                            echo $category['title'];
                        } ?></label>
 -->
                        <div class="inner-refine-search">
                            <form method="get" action="<?php echo $new_url; ?>?pagination=1" id="formRefineSearch">
                                <input type="hidden" name="category" id="category"
                                       value="<?php echo $category['id_categories']; ?>"/>
                                <div class="row" id='search-brand'>


                                    <div class="form-item autofomplete-form-item search_brand_keyword">
                                        <input type="text" class="textbox onenter" name="keywords"
                                               value="<?php echo $this->input->get('keywords'); ?>"
                                               id="keyowrd_autocomplete" placeholder="Search by name, keyword or code"/>
                                    </div>
                                </div>

                                <!--<button class="searchBtn" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                </button>-->

                                <?php if (isset($_GET['keywords'])) { ?>
                                    <a class="reset_btn"
                                       href="<?php echo route_to('brands/details/' . $brand['id_brands']); ?>">reset</a>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <?php
            $selected_category = "";
            if (isset($category)) {
                $select_category = $category;
            }
            $select_category_img = "";
            if (isset($category['image']) && !empty($selected_category['image'])) {
                $select_category_img = $selected_category['image'];
            } ?>
            <?php if (!empty($sub_categories_cc)) { ?>
                <div class="sub_categories_list_bx <?php if (!empty($select_category_img)) {
                    echo "mod";
                } ?>">

                    <?php if (!empty($select_category_img)) { ?>
                        <a alt="<?php echo $selected_category['title']; ?>"
                           title="<?php echo $selected_category['title']; ?>" class="sub_category_select_img"
                           href="<?php echo route_to('products/category/' . $selected_category['id_categories']); ?> ">
                            <img src="<?php echo base_url(); ?>uploads/categories/295x295/<?php echo $select_category_img; ?>"/>
                        </a>
                    <?php } ?>

                    <?php $this->load->view('blocks/sub_categories_list'); ?>
                </div>
            <?php } ?>

            <!--//////////////////////////PRODUCTS////////////////-->
            <div class="products-category-cont products-container">

                <?php
                $data['products'] = $products;
                $data['page'] = '';
                $this->load->view('blocks/products', $data);
                ?>
                <?php if ((isset($_GET['keywords']) || isset($_GET['categories'])) && empty($products)) { ?>
                    <div class="empty">No products found</div>
                <?php } ?>
            </div>


        </div>
    <?php } ?>
</div>
<div class="pagination-row"><?php echo $this->pagination->create_links(); ?></div>
<!--<ul id="pagination" style="display:none;">
  <?php for ($i = 0; $i < $count; $i = $i + $show_items) {

    if (isset($_GET['per_page'])) {
        $next = $_GET['per_page'] + $show_items;
    } else {
        $next = $offset + $show_items;
    }
    ?>
  <li class="<?php if ($i == $next) {
        echo "next";
    } ?>"  ><a href="<?php echo route_to('brands/details/' . $brand['id_brands'] . '?pagination=on&&per_page=' . $i); ?>"><?php echo $next; ?></a></li>
  <?php } ?>
</ul>-->
