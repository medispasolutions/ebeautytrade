<div class="innerContent category-brands">
    <?php
    //SELECT * FROM `categories` WHERE `deleted` = 0 AND `id_parent` = 12
    $categoryBrands = $this->fct->getAll_limit_cond('categories', 'title', 'asc', 100, array('id_parent' => 14));
    ?>
    <div class="page-title border">
        <h1>Supply Room</h1>
        <div style="clear:both"></div>
    </div>
    <?php $k = 0;
    foreach ($categoryBrands as $brands) { ?>
        <?php if (!empty($brands['title'])) { ?>
            <div class="brand-section">
                <span class="imgCell">
                    <?php if (empty($brands['image'])) { ?>
                        <img src="<?php echo base_url(); ?>front/img/logo.png">
                    <?php } else { ?>
                        <img src="<?php echo base_url(); ?>uploads/categories/<?php echo $brands['image']; ?>">
                    <?php } ?>
                </span>
                <p><?php echo $brands['title']; ?></p>
            </div>
        <?php }
    } ?>
</div>