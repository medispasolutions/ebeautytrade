<div class="innerContent" >
  
    <div class="page-title border">
      <h1><?php echo $section['title'];?></h1>
    </div>
    <div class="std">
      <div class="widget widget-static-block">
        <?php foreach($info as $faq){?>
        <div id="faq_wrapper" class="faq_wrapper">
          <div class="tab_head " id="tab_head_<?php echo $faq['id_faq'];?>" data-role="title"><?php echo $faq['title'];?><span class="tab_icon">+</span></div>
          <div class="tab_body" data-role="content" > <?php echo $faq['description'];?> </div>
        </div>
        <?php } ?>
     
    </div>
  </div>
</div>
