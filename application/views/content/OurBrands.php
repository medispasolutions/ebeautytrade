<div class="innerContent suppliers_listing">

    <div class="page-title border">
        <h1>Shop by Brand</h1>
        <div style="clear:both"></div>
    </div>
    <?php
    $brands = $this->db->query("SELECT b.id_brands, b.title, b.logo, b.title_url FROM brands As b WHERE b.deleted = 0 AND b.status = 1 AND b.logo <> '' ORDER BY `b`.`title` ASC");
    $UsedCaract = "";
    $count = 0;
    foreach ($brands->result() as $row) {
        $url_logo = base_url() . 'uploads/brands/' . $row->logo;
        $FistCaract = $row->title[0];
        $count++ ;
        if (strtoupper($FistCaract) != strtoupper($UsedCaract)) {
            ?>
            <div style="clear:both"></div>
            <?php if($count != 1){ ?>
            <div class="line"></div>
            <?php } ?>
            <div class="brandCaract">
                <h3><?php echo $FistCaract; ?></h3>
            </div>
            <?php
        }
        ?>
        <div class="supplier_bx">
            <div class="inner_supplier_bx">
                <div class="supplier_logo">
                    <div class="imgTable">
                        <div class="imgCell">
                            <a class="addImgEffect" title="<?php echo $row->title; ?>"
                               alt="<?php echo $row->title; ?>" href="<?php echo route_to('brands/details/' . $row->id_brands); ?>">
                                <img alt="<?php echo $row->title; ?>" title="<?php echo $row->title; ?>"
                                     src="<?php echo $url_logo; ?>">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $UsedCaract = $row->title[0];
    } ?>
</div>