<section class="innerContent section about">
	<div class="content">
		<div class="page-title border"><h1><?php echo $page_title; ?></h1></div>

		<?php if(!empty($results)) {
			$cc = count($results);$i=0;
			foreach($results as $val){$i++;
				$text_1 = 'read more';
				$custom_des = '';
				$link = '';
				$img = '';
				list($id,$table,$text) = explode('s:',$val['id']);
				if($table=="products"){
					$val['image']=$this->fct->getonecell('products_gallery','image',array('id_products'=>$id));}
					switch($table) {
						case 'dynamic_pages':
						if($id == 1 || $id == 2 || $id == 3) {
							$link = route_to("aboutus");
						}
						if($id == 4) {
							$link = site_url();
						}
						break;
						case 'categories':
						$link = route_to('products/category/'.$id);
						if(!empty($val['image'])){
							$img = base_url().'uploads/categories/295x295/'.$val['image'];	}
							break;
							case 'categories_sub':
							$getcategories = $this->ecommerce_model->getCategoriesBySub($id);
							$custom_des = '';
							if(!empty($getcategories)) {
								foreach($getcategories as $ct) {
									$custom_des .= '<p><a href="'.route_to('products/index/'.$ct['title_url'.getUrlFieldLanguage()].'/'.$val['title_url'.getUrlFieldLanguage()]).'">'.$ct['title'.getFieldLanguage()].'</a></p>';
								}
							}
							$link = route_to("products");
							break;
							case 'products':
							$link = route_to("products/details/".$val['title_url'.getUrlFieldLanguage()]);
							if(!empty($val['image'])){
								$img = base_url().'uploads/products/gallery/295x295/'.$val['image'];	}
								break;
								case 'brands':
								$link = route_to("products/brand/".$val['title_url'.getUrlFieldLanguage()]);
								if(!empty($val['image'])){
									$img = base_url().'uploads/brands/295x295/'.$val['image'];	}
									break;
									case 'branches':
									$link = route_to("contactus");
									break;
								}
								$find = array($keywords,strtolower($keywords),strtoupper($keywords),ucfirst($keywords));
								$replace = array("<span class='keywordColor'>".$keywords."</span>","<span class='keywordColor'>".strtolower($keywords)."</span>","<span class='keywordColor'>".strtoupper($keywords)."</span>","<span class='keywordColor'>".ucfirst($keywords)."</span>");
								?>
								<?php if($link != '') {?>
									<article style ='display:none' class="search-result <?php if($i == $cc) echo ' last'; ?>">
										<?php if(!empty($img)){?>
											<a class="article_img" href="<?php echo $link;?>" title="<?php echo $val['title'];?>" alt="<?php echo $val['title'];?>">
												<img src="<?php echo $img;?>" />
											</a>
										<?php } ?>
										<div class="article_content <?php if(empty($img)){?> full-row <?php } ?>">
											<h3><a href="<?php echo $link; ?>"><?php echo str_replace($find,$replace,$val['title']); ?></a></h3>
											<?php if(isset($custom_des) && !empty($custom_des)) {?>
												<div class="description"><?php echo $custom_des; ?></div>
											<?php }?>
											<?php if(!empty($val['description'])) {?>
												<div class="description"><?php echo str_replace($find,$replace,character_limiter(strip_tags($val['description']),50)); ?></div>
											<?php }?>
											<a href="<?php echo $link; ?>" class="btn read-more"><?php echo $text_1 ;?></a>
										</div>
									</article>
									<a href="<?php echo $link; ?>">
									<div class="product" id='search_product'>
									
										<div class="inner-product">   
											<span class="product-img addImgEffect"> 
												
													<img src="<?php echo $img;?>" />			
											
											</span> 
											<div class="product-content box-sizing">
												<h3><?php echo str_replace($find,$replace,$val['title']); ?></h3>
												<span class="product-title"><?php echo str_replace($find,$replace,character_limiter(strip_tags($val['description']),180)); ?></span>
											</div>
										</div>
										
									</div>
									</a>
								<?php }?>
							<?php }?>
							<?php echo $this->pagination->create_links(); ?>
						<?php } else {?>
							<div class="empty">No results found...</div>
						<?php }?>









       <!-- <div class="desc">
           <?php //echo $intro_1['description']; ?>
       </div>-->
   </div>
</section>







