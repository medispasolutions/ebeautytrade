</div>
<div style="background:#ebecf0">
    <?php
    function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    $user_ip = getUserIP();
    $details = json_decode(file_get_contents("https://api.ipdata.co/{$user_ip}?api-key=test"));
    $user_country = $details->country_name;
    ?>
    <div class="landing-top">
        <div class="desktopVersion">
            <img src="/uploads/landing-page/main-banner.png" class="main-img">
            <div class="centered">
                <div class="main-top">
                    <div class="landing-img">
                        <a href="https://www.ebeautytrade.com/"><img src="/front/img/logo.png"></a>
                    </div>
                    <h1>BECOME A SELLER MEMBER <br>and connect with thousands of buyers</h1>
                    <p>We are a B2B Marketplace for the Professional Beauty Industry</p>
                    <p>We Boost and Expand your Brand into the Middle East market through Dubai</p>
                    <p>Our trade members are Spas, Salons, Beauty Clinics and Retailers</p>
                </div>
            </div>
        </div>
        <div class="mobileVersion">
            <div class="landing-img">
                <a href="https://www.ebeautytrade.com/"><img src="/front/img/logo.png"></a>
            </div>
            <img src="/uploads/landing-page/main-banner.png" class="main-img">
            <div class="centered">
                <div class="main-top">
                    <h1>BECOME A SELLER MEMBER <br>and connect with thousands of buyers</h1>
                    <p>We are a B2B Marketplace for the Professional Beauty Industry</p>
                    <p>We Boost and Expand your Brand into the Middle East market through Dubai</p>
                    <p>Our trade members are Spas, Salons, Beauty Clinics and Retailers</p>
                </div>
            </div>
        </div>
    </div>
    <div class="partner-section">
        <div class="container">
            <div class="row">
                <h3>Why Partner?</h3>
                <div class="col-md-3">
                    <img src="/uploads/landing-page/increase.png">
                    <h3>Increase your Exposure</h3>
                    <p>80% more traffic is going<br/>through multi-vendor platforms</p>
                </div>
                <div class="col-md-3">
                    <img src="/uploads/landing-page/expand.png">
                    <h3>Expand your Market</h3>
                    <p>Boost cross-border e-commerce<br/>linking the Store for easy conversion</p>
                </div>
                <div class="col-md-3">
                    <img src="/uploads/landing-page/leads.png">
                    <h3>Drive more Leads</h3>
                    <p>Collect details of potential<br/>buyers each time they visit your Store</p>
                </div>
                <div class="col-md-3">
                    <img src="/uploads/landing-page/receive.png">
                    <h3>Receive enquiries 24/7</h3>
                    <p>Straight to your inbox<br/>and Admin panel, all year long</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="manage-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>MANAGE YOUR BUSINESS FROM ADMIN PANEL</h3>
                    <h4>with own commercial and delivery terms</h4>
                    <img src="/uploads/landing-page/manage.jpg">
                </div>
            </div>
        </div>
    </div>
    <div class="arrow_box"></div>
    <div class="desktopVersion">
        <div class="dashboard-section">
            <div class="container">
                <div class="row">
                    <h3>ADMIN PANEL FEATURES</h3>
                    <div class="col-md-3">
                        <p><img src="/uploads/landing-page/check.png">Brand full profile</p>
                        <p><img src="/uploads/landing-page/check.png">Contact details per Brand</p>
                        <p><img src="/uploads/landing-page/check.png">Send Enquiry Form</p>
                        <p><img src="/uploads/landing-page/check.png">Google map</p>
                    </div>
                    <div class="col-md-3">
                        <p><img src="/uploads/landing-page/check.png">Unlimited product listing</p>
                        <p><img src="/uploads/landing-page/check.png">Temporary discounts</p>
                        <p><img src="/uploads/landing-page/check.png">Bulk pricing</p>
                        <p><img src="/uploads/landing-page/check.png">Promocode campaigns</p>
                    </div>
                    <div class="col-md-3">
                        <p><img src="/uploads/landing-page/check.png">Stock in / Out</p>
                        <p><img src="/uploads/landing-page/check.png">Set product as New</p>
                        <p><img src="/uploads/landing-page/check.png">Set product as Offer</p>
                        <p><img src="/uploads/landing-page/check.png">Set product as Clearance</p>
                    </div>
                    <div class="col-md-3">
                        <p><img src="/uploads/landing-page/check.png">Click analytics</p>
                        <p><img src="/uploads/landing-page/check.png">Data collection</p>
                        <p><img src="/uploads/landing-page/check.png">Order processing</p>
                        <p><img src="/uploads/landing-page/check.png">Online Payment</p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="desktopVersion">
        <div class="partners-section">
            <div class="container">
                <div class="row">
                    <h3>Some of our Valued Partners</h3>
                    <?php
                    for ($i = 1; $i <= 50; $i++) {
                        ?>
                        <div class="col-md-1">
                            <img src="/uploads/landing-page/<?= $i ?>.png">
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="additional-services-section">
        <div class="container">
            <div class="row">
                <h3>ADDITIONAL SERVICES</h3>
                <div class="col-md-2">
                    <img src="/uploads/landing-page/additional1.png">
                    <h3>Product Registration</h3>
                    <p>Make your Brand available in the UAE and ease Cross Border Trading</p>
                </div>
                <div class="col-md-2">
                    <img src="/uploads/landing-page/additional2.png">
                    <h3>Fulfillment Services</h3>
                    <p>Temporized Storage, Order Processing, Packing, Delivery and Payment Colletion</p>
                </div>
                <div class="col-md-2">
                    <img src="/uploads/landing-page/additional3.png">
                    <h3>Marketing Support</h3>
                    <p>Boost your Brand with email and digital campaigns targeting your audience</p>
                </div>
                <div class="col-md-2">
                    <img src="/uploads/landing-page/additional4.png">
                    <h3>Looking For Distributor</h3>
                    <p>Reach each and every distributor in the region through strategic networking</p>
                </div>
                <div class="col-md-2">
                    <img src="/uploads/landing-page/additional5.png">
                    <h3>Selling B2C Online</h3>
                    <p>Through e-commerce partners in UAE, GCC and India</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <script type="text/javascript" src="https://www.ebeautytrade.com/front/js/jquery-1.10.0.min.js"></script>
    <script type="text/javascript" src="https://www.ebeautytrade.com/front/js/new-landing-script.js"></script>
    <div class="brand-section">
        <div class="container">
            <div class="row becomeTop">
                <h3>BECOME A SELLER</h3>
                <h4>And connect with the Middle East Beauty industry</h4>
                <p style="margin: 10px 0 20px;"></p>
                <div class="col-md-2"></div>
                <div class="col-md-8 form-selected">
                    <div class="col-md-1"></div>
                    <div class="col-md-5 applicationForm">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="optradio" value="application"><span
                                    class="checkmark"></span>I'm a Supplier in the UAE
                        </label>
                    </div>
                    <div class="col-md-5 overseasFrom">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="optradio" value="overseas"><span
                                    class="checkmark"></span>I'm a Supplier Overseas
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 supplier-application" id="sapplication" style="">
                    <div class="row">
                        <div class="col-md-4 inner-logo">
                            <img src="/front/img/logo.png">
                        </div>
                        <div class="col-md-12 sellerTitle">
                            <h3>Seller Membership Application</h3>
                        </div>
                        <div class="col-md-12 backToBrand" style="display:none">
                            <p onclick="displaydiv('companyDetailsNext')">< Back to Brand(s) details</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="application-steps">
                            <div class="col-md-3">
                                <p id="step1" class="active" onclick="displaydiv('step1')"><span
                                            class="numberCircle">1</span>Contact person</p>
                            </div>
                            <div class="col-md-3">
                                <p id="step2" onclick="displaydiv('step2')"><span class="numberCircle">2</span>Company
                                    details</p>
                            </div>
                            <div class="col-md-3">
                                <p id="step3" onclick="displaydiv('step3')"><span class="numberCircle">3</span>Brand(s)
                                    details</p>
                            </div>
                            <div class="col-md-3">
                                <p id="step4" onclick="displaydiv('step4')"><span class="numberCircle">4</span>Select
                                    your
                                    plan</p>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                    <form action="https://ebeautytrade.com/Landing/send" accept-charset="utf-8" method="post"
                          class="contactform" id="ContactForm">
                        <div class="row" id="contact-person">
                            <div class="form-group">
                                <label for="first_name">First Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="first-name" id="first-name" required>
                            </div>
                            <div class="form-group">
                                <label for="last-name">Last Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="last-name" id="last-name" required>
                            </div>
                            <div class="form-group">
                                <label for="position">Position<span class="required">*</span></label>
                                <input type="text" class="form-control" name="position" id="position" required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone<span class="required">*</span></label>
                                <input type="tel" class="textbox mobile-m form-control" name="phone" id="phone"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email Address<span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" required>
                            </div>
                            <div class="form-group">
                                <label for="confemail">Confirm Email Address<span class="required">*</span></label>
                                <input name="emailConfirm" class="form-control" type="email" id="confemail"/>
                            </div>
                            <div class="click-button" onclick="displaydiv('contactperson')" style="margin-left: 48%;float: left;">Next</div>
                        </div>
                        <div class="row" id="company-details" style="display:none">
                            <div class="form-group">
                                <label for="company-name">Company Legal Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="company-name" id="company-name" required>
                            </div>
                            <div class="form-group">
                                <label for="registration-number">License or Tax Registration Number</label>
                                <input type="text" class="form-control" name="registration-number"
                                       id="registration-number">
                            </div>
                            <div class="form-group">
                                <label for="company-number">Company Address<span class="required">*</span></label>
                                <input type="text" class="form-control" name="company-address" id="company-address"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="company-number">Company Phone Number<span class="required">*</span></label>
                                <input type="tel" class="textbox mobile-m form-control" name="company-number"
                                       id="company-number" required>
                            </div>
                            <div class="form-group">
                                <label for="city">City<span class="required">*</span></label>
                                <input type="text" class="form-control" name="city" id="city" required>
                            </div>
                            <div class="form-group">
                                <label for="business-based">Where is, your business based?:</label>
                                <select name="business-based" id="business-based" class="form-control">
                                    <option value="" selected>choose...</option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia</option>
                                    <option value="BA">Bosnia and Herzegowina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="BN">Brunei Darussalam</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CD">Congo, The Democratic Republic of the</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CI">Cote d'Ivoire</option>
                                    <option value="HR">Croatia (Hrvatska)</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="TP">East Timor</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="FX">France, Metropolitan</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard and Mc Donald Islands</option>
                                    <option value="VA">Holy See (Vatican City State)</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran (Islamic Republic of)</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KP">Korea, Democratic People's Republic of</option>
                                    <option value="KR">Korea, Republic of</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Lao People's Democratic Republic</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libyan Arab Jamahiriya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macau</option>
                                    <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>

                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia, Federated States of</option>
                                    <option value="MD">Moldova, Republic of</option>

                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>

                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>

                                    <option value="NL">Netherlands</option>
                                    <option value="AN">Netherlands Antilles</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>

                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>

                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>

                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>

                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>

                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russian Federation</option>

                                    <option value="RW">Rwanda</option>

                                    <option value="KN">Saint Kitts and Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome and Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia (Slovak Republic)</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia and the South Sandwich Islands</option>

                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SH">St. Helena</option>
                                    <option value="PM">St. Pierre and Miquelon</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                    <option value="SZ">Swaziland</option>

                                    <option value="SE">Sweden</option>

                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syrian Arab Republic</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania, United Republic of</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TG">Togo</option>

                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>

                                    <option value="TT">Trinidad and Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks and Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>

                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE" selected>United Arab Emirates</option>

                                    <option value="GB">United Kingdom</option>
                                    <option value="US">United States</option>
                                    <option value="UM">United States Minor Outlying Islands</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>

                                    <option value="VU">Vanuatu</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Viet Nam</option>
                                    <option value="VG">Virgin Islands (British)</option>

                                    <option value="VI">Virgin Islands (U.S.)</option>
                                    <option value="WF">Wallis and Futuna Islands</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>

                                    <option value="YU">Yugoslavia</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="website">Website</label>
                                <input type="text" class="form-control" name="website" id="website">
                            </div>

                            <input type="hidden" class="form-control" name="userid" id="userid">

                            <div class="click-button" onclick="displaydiv('companyDetailsNext')">Next</div>
                            <div class="click-button" onclick="displaydiv('companyDetailsBack')">Back</div>
                        </div>
                        <div class="row" id="brands-details" style="display:none">
                            <div class="row">
                                <div class="col-md-12" style="padding: 0">
                                    <table class="table table-bordered" id="dynamic_field" style="border:none">
                                        <tr>
                                            <td>
                                                Brand Name<span class="required">*</span>
                                            </td>
                                            <td>
                                                Country of Origin<span class="required">*</span>
                                            </td>
                                            <td>
                                                Website
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" id="brand-name1"
                                                       name="brand-name1">
                                            </td>
                                            <td>
                                                <select name="country1" id="country1" class="form-control">
                                                    <option value=""><span class="flag-icon flag-icon-gr"></span>Select
                                                        Country
                                                    </option>
                                                    <option value="Afghanistan">Afghanistan</option>
                                                    <option value="Albania">Albania</option>
                                                    <option value="Algeria">Algeria</option>
                                                    <option value="American Samoa">American Samoa</option>
                                                    <option value="Andorra">Andorra</option>
                                                    <option value="Angola">Angola</option>
                                                    <option value="Anguilla">Anguilla</option>
                                                    <option value="Antartica">Antarctica</option>
                                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                    <option value="Argentina">Argentina</option>
                                                    <option value="Armenia">Armenia</option>
                                                    <option value="Aruba">Aruba</option>
                                                    <option value="Australia">Australia</option>
                                                    <option value="Austria">Austria</option>
                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                    <option value="Bahamas">Bahamas</option>
                                                    <option value="Bahrain">Bahrain</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                    <option value="Barbados">Barbados</option>
                                                    <option value="Belarus">Belarus</option>
                                                    <option value="Belgium">Belgium</option>
                                                    <option value="Belize">Belize</option>
                                                    <option value="Benin">Benin</option>
                                                    <option value="Bermuda">Bermuda</option>
                                                    <option value="Bhutan">Bhutan</option>
                                                    <option value="Bolivia">Bolivia</option>
                                                    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina
                                                    </option>
                                                    <option value="Botswana">Botswana</option>
                                                    <option value="Bouvet Island">Bouvet Island</option>
                                                    <option value="Brazil">Brazil</option>
                                                    <option value="British Indian Ocean Territory">British Indian Ocean
                                                        Territory
                                                    </option>
                                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                    <option value="Bulgaria">Bulgaria</option>
                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                    <option value="Burundi">Burundi</option>
                                                    <option value="Cambodia">Cambodia</option>
                                                    <option value="Cameroon">Cameroon</option>
                                                    <option value="Canada">Canada</option>
                                                    <option value="Cape Verde">Cape Verde</option>
                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                    <option value="Central African Republic">Central African Republic
                                                    </option>
                                                    <option value="Chad">Chad</option>
                                                    <option value="Chile">Chile</option>
                                                    <option value="China">China</option>
                                                    <option value="Christmas Island">Christmas Island</option>
                                                    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                    <option value="Colombia">Colombia</option>
                                                    <option value="Comoros">Comoros</option>
                                                    <option value="Congo">Congo</option>
                                                    <option value="Congo">Congo, the Democratic Republic of the</option>
                                                    <option value="Cook Islands">Cook Islands</option>
                                                    <option value="Costa Rica">Costa Rica</option>
                                                    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                    <option value="Croatia">Croatia (Hrvatska)</option>
                                                    <option value="Cuba">Cuba</option>
                                                    <option value="Cyprus">Cyprus</option>
                                                    <option value="Czech Republic">Czech Republic</option>
                                                    <option value="Denmark">Denmark</option>
                                                    <option value="Djibouti">Djibouti</option>
                                                    <option value="Dominica">Dominica</option>
                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                    <option value="East Timor">East Timor</option>
                                                    <option value="Ecuador">Ecuador</option>
                                                    <option value="Egypt">Egypt</option>
                                                    <option value="El Salvador">El Salvador</option>
                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                    <option value="Eritrea">Eritrea</option>
                                                    <option value="Estonia">Estonia</option>
                                                    <option value="Ethiopia">Ethiopia</option>
                                                    <option value="Falkland Islands">Falkland Islands (Malvinas)
                                                    </option>
                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                    <option value="Fiji">Fiji</option>
                                                    <option value="Finland">Finland</option>
                                                    <option value="France">France</option>
                                                    <option value="France Metropolitan">France, Metropolitan</option>
                                                    <option value="French Guiana">French Guiana</option>
                                                    <option value="French Polynesia">French Polynesia</option>
                                                    <option value="French Southern Territories">French Southern
                                                        Territories
                                                    </option>
                                                    <option value="Gabon">Gabon</option>
                                                    <option value="Gambia">Gambia</option>
                                                    <option value="Georgia">Georgia</option>
                                                    <option value="Germany">Germany</option>
                                                    <option value="Ghana">Ghana</option>
                                                    <option value="Gibraltar">Gibraltar</option>
                                                    <option value="Greece">Greece</option>
                                                    <option value="Greenland">Greenland</option>
                                                    <option value="Grenada">Grenada</option>
                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                    <option value="Guam">Guam</option>
                                                    <option value="Guatemala">Guatemala</option>
                                                    <option value="Guinea">Guinea</option>
                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                    <option value="Guyana">Guyana</option>
                                                    <option value="Haiti">Haiti</option>
                                                    <option value="Heard and McDonald Islands">Heard and Mc Donald
                                                        Islands
                                                    </option>
                                                    <option value="Holy See">Holy See (Vatican City State)</option>
                                                    <option value="Honduras">Honduras</option>
                                                    <option value="Hong Kong">Hong Kong</option>
                                                    <option value="Hungary">Hungary</option>
                                                    <option value="Iceland">Iceland</option>
                                                    <option value="India">India</option>
                                                    <option value="Indonesia">Indonesia</option>
                                                    <option value="Iran">Iran (Islamic Republic of)</option>
                                                    <option value="Iraq">Iraq</option>
                                                    <option value="Ireland">Ireland</option>
                                                    <option value="Israel">Israel</option>
                                                    <option value="Italy">Italy</option>
                                                    <option value="Jamaica">Jamaica</option>
                                                    <option value="Japan">Japan</option>
                                                    <option value="Jordan">Jordan</option>
                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                    <option value="Kenya">Kenya</option>
                                                    <option value="Kiribati">Kiribati</option>
                                                    <option value="Korea">Korea, Republic of</option>
                                                    <option value="Kuwait">Kuwait</option>
                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                    <option value="Lao">Lao People's Democratic Republic</option>
                                                    <option value="Latvia">Latvia</option>
                                                    <option value="Lebanon">Lebanon</option>
                                                    <option value="Lesotho">Lesotho</option>
                                                    <option value="Liberia">Liberia</option>
                                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya
                                                    </option>
                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                    <option value="Lithuania">Lithuania</option>
                                                    <option value="Luxembourg">Luxembourg</option>
                                                    <option value="Macau">Macau</option>
                                                    <option value="Macedonia">Macedonia, The Former Yugoslav Republic
                                                        of
                                                    </option>
                                                    <option value="Madagascar">Madagascar</option>
                                                    <option value="Malawi">Malawi</option>
                                                    <option value="Malaysia">Malaysia</option>
                                                    <option value="Maldives">Maldives</option>
                                                    <option value="Mali">Mali</option>
                                                    <option value="Malta">Malta</option>
                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                    <option value="Martinique">Martinique</option>
                                                    <option value="Mauritania">Mauritania</option>
                                                    <option value="Mauritius">Mauritius</option>
                                                    <option value="Mayotte">Mayotte</option>
                                                    <option value="Mexico">Mexico</option>
                                                    <option value="Micronesia">Micronesia, Federated States of</option>
                                                    <option value="Moldova">Moldova, Republic of</option>
                                                    <option value="Monaco">Monaco</option>
                                                    <option value="Mongolia">Mongolia</option>
                                                    <option value="Montserrat">Montserrat</option>
                                                    <option value="Morocco">Morocco</option>
                                                    <option value="Mozambique">Mozambique</option>
                                                    <option value="Myanmar">Myanmar</option>
                                                    <option value="Namibia">Namibia</option>
                                                    <option value="Nauru">Nauru</option>
                                                    <option value="Nepal">Nepal</option>
                                                    <option value="Netherlands">Netherlands</option>
                                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                    <option value="New Caledonia">New Caledonia</option>
                                                    <option value="New Zealand">New Zealand</option>
                                                    <option value="Nicaragua">Nicaragua</option>
                                                    <option value="Niger">Niger</option>
                                                    <option value="Nigeria">Nigeria</option>
                                                    <option value="Niue">Niue</option>
                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                    <option value="North Korea">North Korea</option>
                                                    <option value="Northern Mariana Islands">Northern Mariana Islands
                                                    </option>
                                                    <option value="Norway">Norway</option>
                                                    <option value="Oman">Oman</option>
                                                    <option value="Pakistan">Pakistan</option>
                                                    <option value="Palau">Palau</option>
                                                    <option value="Panama">Panama</option>
                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                    <option value="Paraguay">Paraguay</option>
                                                    <option value="Peru">Peru</option>
                                                    <option value="Philippines">Philippines</option>
                                                    <option value="Pitcairn">Pitcairn</option>
                                                    <option value="Poland">Poland</option>
                                                    <option value="Portugal">Portugal</option>
                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                    <option value="Qatar">Qatar</option>
                                                    <option value="Reunion">Reunion</option>
                                                    <option value="Romania">Romania</option>
                                                    <option value="Russia">Russian Federation</option>
                                                    <option value="Rwanda">Rwanda</option>
                                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                    <option value="Saint LUCIA">Saint LUCIA</option>
                                                    <option value="Saint Vincent">Saint Vincent and the Grenadines
                                                    </option>
                                                    <option value="Samoa">Samoa</option>
                                                    <option value="San Marino">San Marino</option>
                                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                    <option value="Senegal">Senegal</option>
                                                    <option value="Seychelles">Seychelles</option>
                                                    <option value="Sierra">Sierra Leone</option>
                                                    <option value="Singapore">Singapore</option>
                                                    <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                    <option value="Slovenia">Slovenia</option>
                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                    <option value="Somalia">Somalia</option>
                                                    <option value="South Africa">South Africa</option>
                                                    <option value="South Georgia">South Georgia and the South Sandwich
                                                        Islands
                                                    </option>
                                                    <option value="South Korea">South Korea</option>
                                                    <option value="South Sudan">South Sudan</option>
                                                    <option value="Span">Spain</option>
                                                    <option value="SriLanka">Sri Lanka</option>
                                                    <option value="St. Helena">St. Helena</option>
                                                    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon
                                                    </option>
                                                    <option value="Sudan">Sudan</option>
                                                    <option value="Suriname">Suriname</option>
                                                    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                    <option value="Swaziland">Swaziland</option>
                                                    <option value="Sweden">Sweden</option>
                                                    <option value="Switzerland">Switzerland</option>
                                                    <option value="Syria">Syrian Arab Republic</option>
                                                    <option value="Taiwan">Taiwan, Province of China</option>
                                                    <option value="Tajikistan">Tajikistan</option>
                                                    <option value="Tanzania">Tanzania, United Republic of</option>
                                                    <option value="Thailand">Thailand</option>
                                                    <option value="Togo">Togo</option>
                                                    <option value="Tokelau">Tokelau</option>
                                                    <option value="Tonga">Tonga</option>
                                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                    <option value="Tunisia">Tunisia</option>
                                                    <option value="Turkey">Turkey</option>
                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                    <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                    <option value="Tuvalu">Tuvalu</option>
                                                    <option value="Uganda">Uganda</option>
                                                    <option value="Ukraine">Ukraine</option>
                                                    <option value="United Arab Emirates" <?php if ($user_country == "United Arab Emirates" || $user_country == "UAE") { ?> selected <?php } ?>>
                                                        United Arab Emirates
                                                    </option>
                                                    <option value="United Kingdom">United Kingdom</option>
                                                    <option value="United States">United States</option>
                                                    <option value="United States Minor Outlying Islands">United States
                                                        Minor Outlying Islands
                                                    </option>
                                                    <option value="Uruguay">Uruguay</option>
                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                    <option value="Vanuatu">Vanuatu</option>
                                                    <option value="Venezuela">Venezuela</option>
                                                    <option value="Vietnam">Viet Nam</option>
                                                    <option value="Virgin Islands (British)">Virgin Islands (British)
                                                    </option>
                                                    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                    <option value="Wallis and Futana Islands">Wallis and Futuna
                                                        Islands
                                                    </option>
                                                    <option value="Western Sahara">Western Sahara</option>
                                                    <option value="Yemen">Yemen</option>
                                                    <option value="Yugoslavia">Yugoslavia</option>
                                                    <option value="Zambia">Zambia</option>
                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="brand-website1"
                                                       id="brand-website1">
                                            </td>
                                            <td><p><br/><input type="hidden" class="form-control" name="amountofbrands" id="amountofbrands" value="1"></p></td>
                                        </tr>
                                    </table>
                                    <div class="col-md-12 morebrand">
                                        <div class="click-button">
                                            <button type="button" name="add" id="add" class="btn btn-success">Add
                                                Brand
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <textarea type="text" class="form-control" id="brand-description"
                                              name="brand-description"
                                              placeholder="Please describe any special requirements you may have"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="click-button" onclick="displaydiv('brandsDetailsNext')">Select your plan</div>
                            <div class="click-button" onclick="displaydiv('brandsDetailsBack')">Back</div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                var i = 1;
                                $('#add').click(function () {
                                    i++;
                                    $('#dynamic_field').append('<tr id="row' + i + '">' +
                                        '<td><input type="text" class="form-control" id="brand-name' + i + '" name="brand-name' + i + '"></td>' +
                                        '<td><select name="country' + i + '" class="form-control">\n' +
                                        '<option value=""><span class="flag-icon flag-icon-gr"></span>Select Country</option>\n' +
                                        '<option value="Afghanistan">Afghanistan</option>\n' +
                                        '<option value="Albania">Albania</option>\n' +
                                        '<option value="Algeria">Algeria</option>\n' +
                                        '<option value="American Samoa">American Samoa</option>\n' +
                                        '<option value="Andorra">Andorra</option>\n' +
                                        '<option value="Angola">Angola</option>\n' +
                                        '<option value="Anguilla">Anguilla</option>\n' +
                                        '<option value="Antartica">Antarctica</option>\n' +
                                        '<option value="Antigua and Barbuda">Antigua and Barbuda</option>\n' +
                                        '<option value="Argentina">Argentina</option>\n' +
                                        '<option value="Armenia">Armenia</option>\n' +
                                        '<option value="Aruba">Aruba</option>\n' +
                                        '<option value="Australia">Australia</option>\n' +
                                        '<option value="Austria">Austria</option>\n' +
                                        '<option value="Azerbaijan">Azerbaijan</option>\n' +
                                        '<option value="Bahamas">Bahamas</option>\n' +
                                        '<option value="Bahrain">Bahrain</option>\n' +
                                        '<option value="Bangladesh">Bangladesh</option>\n' +
                                        '<option value="Barbados">Barbados</option>\n' +
                                        '<option value="Belarus">Belarus</option>\n' +
                                        '<option value="Belgium">Belgium</option>\n' +
                                        '<option value="Belize">Belize</option>\n' +
                                        '<option value="Benin">Benin</option>\n' +
                                        '<option value="Bermuda">Bermuda</option>\n' +
                                        '<option value="Bhutan">Bhutan</option>\n' +
                                        '<option value="Bolivia">Bolivia</option>\n' +
                                        '<option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>\n' +
                                        '<option value="Botswana">Botswana</option>\n' +
                                        '<option value="Bouvet Island">Bouvet Island</option>\n' +
                                        '<option value="Brazil">Brazil</option>\n' +
                                        '<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>\n' +
                                        '<option value="Brunei Darussalam">Brunei Darussalam</option>\n' +
                                        '<option value="Bulgaria">Bulgaria</option>\n' +
                                        '<option value="Burkina Faso">Burkina Faso</option>\n' +
                                        '<option value="Burundi">Burundi</option>\n' +
                                        '<option value="Cambodia">Cambodia</option>\n' +
                                        '<option value="Cameroon">Cameroon</option>\n' +
                                        '<option value="Canada">Canada</option>\n' +
                                        '<option value="Cape Verde">Cape Verde</option>\n' +
                                        '<option value="Cayman Islands">Cayman Islands</option>\n' +
                                        '<option value="Central African Republic">Central African Republic</option>\n' +
                                        '<option value="Chad">Chad</option>\n' +
                                        '<option value="Chile">Chile</option>\n' +
                                        '<option value="China">China</option>\n' +
                                        '<option value="Christmas Island">Christmas Island</option>\n' +
                                        '<option value="Cocos Islands">Cocos (Keeling) Islands</option>\n' +
                                        '<option value="Colombia">Colombia</option>\n' +
                                        '<option value="Comoros">Comoros</option>\n' +
                                        '<option value="Congo">Congo</option>\n' +
                                        '<option value="Congo">Congo, the Democratic Republic of the</option>\n' +
                                        '<option value="Cook Islands">Cook Islands</option>\n' +
                                        '<option value="Costa Rica">Costa Rica</option>\n' +
                                        '<option value="Cota D\'Ivoire">Cote d\'Ivoire</option>\n' +
                                        '<option value="Croatia">Croatia (Hrvatska)</option>\n' +
                                        '<option value="Cuba">Cuba</option>\n' +
                                        '<option value="Cyprus">Cyprus</option>\n' +
                                        '<option value="Czech Republic">Czech Republic</option>\n' +
                                        '<option value="Denmark">Denmark</option>\n' +
                                        '<option value="Djibouti">Djibouti</option>\n' +
                                        '<option value="Dominica">Dominica</option>\n' +
                                        '<option value="Dominican Republic">Dominican Republic</option>\n' +
                                        '<option value="East Timor">East Timor</option>\n' +
                                        '<option value="Ecuador">Ecuador</option>\n' +
                                        '<option value="Egypt">Egypt</option>\n' +
                                        '<option value="El Salvador">El Salvador</option>\n' +
                                        '<option value="Equatorial Guinea">Equatorial Guinea</option>\n' +
                                        '<option value="Eritrea">Eritrea</option>\n' +
                                        '<option value="Estonia">Estonia</option>\n' +
                                        '<option value="Ethiopia">Ethiopia</option>\n' +
                                        '<option value="Falkland Islands">Falkland Islands (Malvinas)</option>\n' +
                                        '<option value="Faroe Islands">Faroe Islands</option>\n' +
                                        '<option value="Fiji">Fiji</option>\n' +
                                        '<option value="Finland">Finland</option>\n' +
                                        '<option value="France">France</option>\n' +
                                        '<option value="France Metropolitan">France, Metropolitan</option>\n' +
                                        '<option value="French Guiana">French Guiana</option>\n' +
                                        '<option value="French Polynesia">French Polynesia</option>\n' +
                                        '<option value="French Southern Territories">French Southern Territories</option>\n' +
                                        '<option value="Gabon">Gabon</option>\n' +
                                        '<option value="Gambia">Gambia</option>\n' +
                                        '<option value="Georgia">Georgia</option>\n' +
                                        '<option value="Germany">Germany</option>\n' +
                                        '<option value="Ghana">Ghana</option>\n' +
                                        '<option value="Gibraltar">Gibraltar</option>\n' +
                                        '<option value="Greece">Greece</option>\n' +
                                        '<option value="Greenland">Greenland</option>\n' +
                                        '<option value="Grenada">Grenada</option>\n' +
                                        '<option value="Guadeloupe">Guadeloupe</option>\n' +
                                        '<option value="Guam">Guam</option>\n' +
                                        '<option value="Guatemala">Guatemala</option>\n' +
                                        '<option value="Guinea">Guinea</option>\n' +
                                        '<option value="Guinea-Bissau">Guinea-Bissau</option>\n' +
                                        '<option value="Guyana">Guyana</option>\n' +
                                        '<option value="Haiti">Haiti</option>\n' +
                                        '<option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>\n' +
                                        '<option value="Holy See">Holy See (Vatican City State)</option>\n' +
                                        '<option value="Honduras">Honduras</option>\n' +
                                        '<option value="Hong Kong">Hong Kong</option>\n' +
                                        '<option value="Hungary">Hungary</option>\n' +
                                        '<option value="Iceland">Iceland</option>\n' +
                                        '<option value="India">India</option>\n' +
                                        '<option value="Indonesia">Indonesia</option>\n' +
                                        '<option value="Iran">Iran (Islamic Republic of)</option>\n' +
                                        '<option value="Iraq">Iraq</option>\n' +
                                        '<option value="Ireland">Ireland</option>\n' +
                                        '<option value="Israel">Israel</option>\n' +
                                        '<option value="Italy">Italy</option>\n' +
                                        '<option value="Jamaica">Jamaica</option>\n' +
                                        '<option value="Japan">Japan</option>\n' +
                                        '<option value="Jordan">Jordan</option>\n' +
                                        '<option value="Kazakhstan">Kazakhstan</option>\n' +
                                        '<option value="Kenya">Kenya</option>\n' +
                                        '<option value="Kiribati">Kiribati</option>\n' +
                                        '<option value="Korea">Korea, Republic of</option>\n' +
                                        '<option value="Kuwait">Kuwait</option>\n' +
                                        '<option value="Kyrgyzstan">Kyrgyzstan</option>\n' +
                                        '<option value="Lao">Lao People\'s Democratic Republic</option>\n' +
                                        '<option value="Latvia">Latvia</option>\n' +
                                        '<option value="Lebanon">Lebanon</option>\n' +
                                        '<option value="Lesotho">Lesotho</option>\n' +
                                        '<option value="Liberia">Liberia</option>\n' +
                                        '<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>\n' +
                                        '<option value="Liechtenstein">Liechtenstein</option>\n' +
                                        '<option value="Lithuania">Lithuania</option>\n' +
                                        '<option value="Luxembourg">Luxembourg</option>\n' +
                                        '<option value="Macau">Macau</option>\n' +
                                        '<option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>\n' +
                                        '<option value="Madagascar">Madagascar</option>\n' +
                                        '<option value="Malawi">Malawi</option>\n' +
                                        '<option value="Malaysia">Malaysia</option>\n' +
                                        '<option value="Maldives">Maldives</option>\n' +
                                        '<option value="Mali">Mali</option>\n' +
                                        '<option value="Malta">Malta</option>\n' +
                                        '<option value="Marshall Islands">Marshall Islands</option>\n' +
                                        '<option value="Martinique">Martinique</option>\n' +
                                        '<option value="Mauritania">Mauritania</option>\n' +
                                        '<option value="Mauritius">Mauritius</option>\n' +
                                        '<option value="Mayotte">Mayotte</option>\n' +
                                        '<option value="Mexico">Mexico</option>\n' +
                                        '<option value="Micronesia">Micronesia, Federated States of</option>\n' +
                                        '<option value="Moldova">Moldova, Republic of</option>\n' +
                                        '<option value="Monaco">Monaco</option>\n' +
                                        '<option value="Mongolia">Mongolia</option>\n' +
                                        '<option value="Montserrat">Montserrat</option>\n' +
                                        '<option value="Morocco">Morocco</option>\n' +
                                        '<option value="Mozambique">Mozambique</option>\n' +
                                        '<option value="Myanmar">Myanmar</option>\n' +
                                        '<option value="Namibia">Namibia</option>\n' +
                                        '<option value="Nauru">Nauru</option>\n' +
                                        '<option value="Nepal">Nepal</option>\n' +
                                        '<option value="Netherlands">Netherlands</option>\n' +
                                        '<option value="Netherlands Antilles">Netherlands Antilles</option>\n' +
                                        '<option value="New Caledonia">New Caledonia</option>\n' +
                                        '<option value="New Zealand">New Zealand</option>\n' +
                                        '<option value="Nicaragua">Nicaragua</option>\n' +
                                        '<option value="Niger">Niger</option>\n' +
                                        '<option value="Nigeria">Nigeria</option>\n' +
                                        '<option value="Niue">Niue</option>\n' +
                                        '<option value="Norfolk Island">Norfolk Island</option>\n' +
                                        '<option value="North Korea">North Korea</option>\n' +
                                        '<option value="Northern Mariana Islands">Northern Mariana Islands</option>\n' +
                                        '<option value="Norway">Norway</option>\n' +
                                        '<option value="Oman">Oman</option>\n' +
                                        '<option value="Pakistan">Pakistan</option>\n' +
                                        '<option value="Palau">Palau</option>\n' +
                                        '<option value="Panama">Panama</option>\n' +
                                        '<option value="Papua New Guinea">Papua New Guinea</option>\n' +
                                        '<option value="Paraguay">Paraguay</option>\n' +
                                        '<option value="Peru">Peru</option>\n' +
                                        '<option value="Philippines">Philippines</option>\n' +
                                        '<option value="Pitcairn">Pitcairn</option>\n' +
                                        '<option value="Poland">Poland</option>\n' +
                                        '<option value="Portugal">Portugal</option>\n' +
                                        '<option value="Puerto Rico">Puerto Rico</option>\n' +
                                        '<option value="Qatar">Qatar</option>\n' +
                                        '<option value="Reunion">Reunion</option>\n' +
                                        '<option value="Romania">Romania</option>\n' +
                                        '<option value="Russia">Russian Federation</option>\n' +
                                        '<option value="Rwanda">Rwanda</option>\n' +
                                        '<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>\n' +
                                        '<option value="Saint LUCIA">Saint LUCIA</option>\n' +
                                        '<option value="Saint Vincent">Saint Vincent and the Grenadines</option>\n' +
                                        '<option value="Samoa">Samoa</option>\n' +
                                        '<option value="San Marino">San Marino</option>\n' +
                                        '<option value="Sao Tome and Principe">Sao Tome and Principe</option>\n' +
                                        '<option value="Saudi Arabia">Saudi Arabia</option>\n' +
                                        '<option value="Senegal">Senegal</option>\n' +
                                        '<option value="Seychelles">Seychelles</option>\n' +
                                        '<option value="Sierra">Sierra Leone</option>\n' +
                                        '<option value="Singapore">Singapore</option>\n' +
                                        '<option value="Slovakia">Slovakia (Slovak Republic)</option>\n' +
                                        '<option value="Slovenia">Slovenia</option>\n' +
                                        '<option value="Solomon Islands">Solomon Islands</option>\n' +
                                        '<option value="Somalia">Somalia</option>\n' +
                                        '<option value="South Africa">South Africa</option>\n' +
                                        '<option value="South Georgia">South Georgia and the South Sandwich Islands</option>\n' +
                                        '<option value="South Korea">South Korea</option>\n' +
                                        '<option value="South Sudan">South Sudan</option>\n' +
                                        '<option value="Span">Spain</option>\n' +
                                        '<option value="SriLanka">Sri Lanka</option>\n' +
                                        '<option value="St. Helena">St. Helena</option>\n' +
                                        '<option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>\n' +
                                        '<option value="Sudan">Sudan</option>\n' +
                                        '<option value="Suriname">Suriname</option>\n' +
                                        '<option value="Svalbard">Svalbard and Jan Mayen Islands</option>\n' +
                                        '<option value="Swaziland">Swaziland</option>\n' +
                                        '<option value="Sweden">Sweden</option>\n' +
                                        '<option value="Switzerland">Switzerland</option>\n' +
                                        '<option value="Syria">Syrian Arab Republic</option>\n' +
                                        '<option value="Taiwan">Taiwan, Province of China</option>\n' +
                                        '<option value="Tajikistan">Tajikistan</option>\n' +
                                        '<option value="Tanzania">Tanzania, United Republic of</option>\n' +
                                        '<option value="Thailand">Thailand</option>\n' +
                                        '<option value="Togo">Togo</option>\n' +
                                        '<option value="Tokelau">Tokelau</option>\n' +
                                        '<option value="Tonga">Tonga</option>\n' +
                                        '<option value="Trinidad and Tobago">Trinidad and Tobago</option>\n' +
                                        '<option value="Tunisia">Tunisia</option>\n' +
                                        '<option value="Turkey">Turkey</option>\n' +
                                        '<option value="Turkmenistan">Turkmenistan</option>\n' +
                                        '<option value="Turks and Caicos">Turks and Caicos Islands</option>\n' +
                                        '<option value="Tuvalu">Tuvalu</option>\n' +
                                        '<option value="Uganda">Uganda</option>\n' +
                                        '<option value="Ukraine">Ukraine</option>\n' +
                                        '<option value="United Arab Emirates">United Arab Emirates</option>\n' +
                                        '<option value="United Kingdom">United Kingdom</option>\n' +
                                        '<option value="United States">United States</option>\n' +
                                        '<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>\n' +
                                        '<option value="Uruguay">Uruguay</option>\n' +
                                        '<option value="Uzbekistan">Uzbekistan</option>\n' +
                                        '<option value="Vanuatu">Vanuatu</option>\n' +
                                        '<option value="Venezuela">Venezuela</option>\n' +
                                        '<option value="Vietnam">Viet Nam</option>\n' +
                                        '<option value="Virgin Islands (British)">Virgin Islands (British)</option>\n' +
                                        '<option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>\n' +
                                        '<option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>\n' +
                                        '<option value="Western Sahara">Western Sahara</option>\n' +
                                        '<option value="Yemen">Yemen</option>\n' +
                                        '<option value="Yugoslavia">Yugoslavia</option>\n' +
                                        '<option value="Zambia">Zambia</option>\n' +
                                        '<option value="Zimbabwe">Zimbabwe</option>\n' +
                                        '                                    </select>' +
                                        '<td><input type="text" class="form-control" name="brand-website' + i + '" id="brand-website"></td>' +
                                        '<td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">Delete</button><input type="hidden" class="form-control" name="amountofbrands" value="' + i + '"></td></tr>');
                                });

                                $(document).on('click', '.btn_remove', function () {
                                    var button_id = $(this).attr("id");
                                    $('#row' + button_id + '').remove();
                                });

                                /*$('#submit').click(function(){
                                $.ajax({
                                    url:"name.php",
                                    method:"POST",
                                    data:$('#add_name').serialize(),
                                    success:function(data)
                                    {
                                        alert(data);
                                        $('#add_name')[0].reset();
                                    }
                                });
                            });*/

                            });
                        </script>
                        <div class="row" id="bundle-details" style="display:none">
                            <?php $random = substr(md5(mt_rand()), 0, 7); ?>
                            <input type="hidden" name="random-number" value="<?php echo $random; ?>"/>
                            <input id="application-type" name="application-type" type="hidden" value="1">
                            <div class="desktopVersion">
                                <table class="table pricingTable sellerPlan">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr class="no-border">
                                        <th class="">
                                            <div class="maintext">SELLER PLANS<br/>For Suppliers in the UAE</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FREE</div>
                                            <p>
                                                Selling B2B, basic Online Store<br/>Fulfilled by Supplier
                                            </p>
                                        </th>
                                        <th>
                                            <div class="eyebrow">GOLD</div>
                                            <p>
                                                More Marketing, more Leads<br/>Fulfilled by Supplier
                                            </p>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FBE</div>
                                            <p>
                                                Selling B2B/B2C incl. logistics<br/>Fulfilled by eBeautyTrade
                                            </p>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="no-border pricing">
                                        <td>
                                            <p class="maintext">
                                                PRICING
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                STARTING /MO
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                STARTING /MO
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                STARTING /MO
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p class="bundle-price-month">
                                                0<sup>AED</sup>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-month">
                                                <span id="gold-price-month">129</span><sup>AED</sup>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-month">
                                                <span id="fbe-price-month">229</span><sup>AED</sup>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td><p>
                                                PAID YEARLY
                                            </p></td>
                                        <td><p>
                                                PAID YEARLY
                                            </p></td>
                                        <td><p>
                                                PAID YEARLY
                                            </p></td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p class="bundle-price-year">
                                                0<sup>AED</sup>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-year">
                                                <span id="gold-price-year">1 548</span><sup>AED</sup>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-year">
                                                <span id="fbe-price-year">2 748</span><sup>AED</sup>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td><p></p></td>
                                        <td><p></p></td>
                                        <td>
                                            <p style="font-weight: bold;">
                                                + 10% Commission <span data-toggle="tooltip" data-placement="right"
                                                                       title="Over net sales"><i
                                                            class="fa fa-info-circle"></i></span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p>
                                                <select name="uae-free-brand-number" id="freebrandnumber"
                                                        class="form-control">
                                                    <option value="">No of Brands</option>
                                                    <option value="free1">1 Brand</option>
                                                    <option value="free2">Up to 3 Brands</option>
                                                    <option value="free3">Up to 6 Brands</option>
                                                    <option value="free4">Unlimited Brands</option>
                                                </select>
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <select name="uae-gold-brand-number" id="goldbrandnumber"
                                                        class="form-control">
                                                    <option value="">No of Brands</option>
                                                    <option value="gold1">1 Brand</option>
                                                    <option value="gold2">Up to 3 Brands</option>
                                                    <option value="gold3">Up to 6 Brands</option>
                                                    <option value="gold4">Unlimited Brands</option>
                                                </select>
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <select name="uae-fbe-brand-number" id="fbebrandnumber"
                                                        class="form-control">
                                                    <option value="">No of Brands</option>
                                                    <option value="fbe1">1 Brand</option>
                                                    <option value="fbe2">2 Brands</option>
                                                    <option value="fbe3">3 Brands</option>
                                                    <option value="fbe4">4 Brands</option>
                                                </select>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p id="free-terms" style="display:none">
                                                <input style="margin:2px 4px 0 0" type="checkbox" name="agree"> I have
                                                read and accepted the Terms and Policy
                                            </p>
                                        </td>
                                        <td>
                                            <p id="gold-terms" style="display:none">
                                                <input style="margin:2px 4px 0 0" type="checkbox" name="agree"> I have
                                                read and accepted the Terms and Policy
                                            </p>
                                        </td>
                                        <td>
                                            <p id="fbe-terms" style="display:none">
                                                <input style="margin:2px 4px 0 0" type="checkbox" name="agree"> I have
                                                read and accepted the Terms and Policy
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <div class="radio-group">
                                                <button id="uae-free-button" style="display: table;margin: 10px auto 0;"
                                                        type="submit" class="btn btn-lg btn-default" disabled>Apply Now
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio-group">
                                                <button id="uae-gold-button" style="display: table;margin: 10px auto 0;"
                                                        type="submit" class="btn btn-lg btn-default" disabled>Apply Now
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio-group">
                                                <button id="uae-fbe-button" style="display: table;margin: 10px auto 0;"
                                                        type="submit" class="btn btn-lg btn-default" disabled>Apply Now
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">SELLING B2B through ebeautytrade.com</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FREE</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">GOLD</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FBE</div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="List unlimited items under the same Brand, including products, services, parts, accessories, training courses, etc.">
                                                Unlimited Listing per Brand</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                Admin Panel</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Receive enquiries and orders into your Admin Panel and registered E-mails in your Company and Brand(s) Profiles.">
                                                Receive orders straight to your inbox</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                Online Payment Gateway</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Share and keep updated your Operations and Sales team with your digital catalog. Ease your procedures and spend more time in upsales, marketing and customer service.">
                                                Multi-users for your Admin Panel</p>
                                        </td>
                                        <td><p></p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="You may use this option to make your Brand available only for potential Buyers who meet with your concept and criteria. The selected Buyers will need your Access Code for online ordering.">
                                                Access Code for authorized Buyers</p>
                                        </td>
                                        <td><p>

                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="See how many times Buyers are clicking on your Brands and Products.">
                                                Clicks Analytics on Brands and Products</p>
                                        </td>
                                        <td><p>

                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Collect contact details and drive more leads of Buyers when they click on your Brand(s), Products, Call Supplier and Send enquiry button(s).">
                                                Collect Buyers' details upon visiting you</p>
                                        </td>
                                        <td><p></p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Launch any email, printed or social media campaign  and track conversion rate by adding Promocode from your Admin Panel.">
                                                Promocode campaigns</p>
                                        </td>
                                        <td><p></p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">REGISTRATION (upon request)</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                Get your Product registered by us</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">LOGISTICS SERVICES</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Get Free Temporized Storage up to 700 units or 2 CBM per Brand. Additional storage is available at approx. US$ 40 per CBM /month.">
                                                Temporized Storage</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="We check your incoming orders upon notification, we confirm price, availability and  terms with the Buyer, we prepare and pack properly the order, then we proceed with the invoicing.">
                                                Order Processing / Invoicing</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Deliveries will be handled by selected couriers depending on weight and destination. Delivery terms and fees will be managed as per Supplier instructions.">
                                                Coordinating Deliveries / Payments</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Physical address is mostly needed for orders pick-up from E-store partners, B2B customers and eventual end-users.">
                                                Physical address for orders pick-up</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">SELLING B2C through partners</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p>Listing on UAE e-stores (upon request)</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Listing on GCC e-stores (upon request)</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Listing on India e-stores (upon request)</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable marketing-table">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">MARKETING SUPPORT included</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                               Welcoming E-mail shot per Brand
                                            </p>
                                        </td>
                                        <td><p>1</p></td>
                                        <td><p>1</p></td>
                                        <td><p>1</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Bespoke E-mail shot per Brand</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>1</p></td>
                                        <td><p>3</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Homepage featuring per Brand</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>1</p></td>
                                        <td><p>3</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Your logo sponsoring our E-Newsletter</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>1</p></td>
                                        <td><p>3</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mobileVersion" style="display:none">
                                <div class="maintext">SELLER PLANS<br/>For Suppliers in the UAE</div>
                                <div>
                                    <div id="accordion" class="panel-group">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a href="#panelFreeUae" class="accordion-toggle"
                                                       data-toggle="collapse" data-parent="#accordion">FREE</a>
                                                </h4>
                                                <h4 class="panel-title">
                                                    <a href="#panelgoldUae" class="accordion-toggle collapsed"
                                                       data-toggle="collapse"
                                                       data-parent="#accordion">GOLD</a>
                                                </h4>
                                                <h4 class="panel-title">
                                                    <a href="#panelfbeUae" class="accordion-toggle collapsed"
                                                       data-toggle="collapse"
                                                       data-parent="#accordion">FBE</a>
                                                </h4>
                                            </div>
                                            <div id="panelFreeUae" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <h3>FREE Plan</h3>
                                                    <p>Selling B2B, basic Online Store<br/>Fulfilled by
                                                        Supplier</p>
                                                    <p class="price-mobile">
                                                        PRICING Per Year
                                                        <select name="uae-free-brand-number"
                                                                id="freebrandnumber-mobile" class="form-control">
                                                            <option value="">Your Brand(s)</option>
                                                            <option value="free1">1 Brand</option>
                                                            <option value="free2">Up to 3 Brands</option>
                                                            <option value="free3">Up to 6 Brands</option>
                                                            <option value="free4">Unlimited Brands</option>
                                                        </select>
                                                    </p>
                                                    <div class="radio-group" style="clear:both">
                                                        <button id="uae-free-button-mobile"
                                                                style="display: table;margin: 10px auto 0;"
                                                                type="submit" class="btn btn-lg btn-default"
                                                                >Apply Now
                                                        </button>
                                                    </div>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">SELLING B2B through
                                                                    ebeautytrade.com
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="List unlimited items under the same Brand, including products, services, parts, accessories, training courses, etc.">
                                                                    Unlimited Listing per Brand <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                                    Admin Panel <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive enquiries and orders into your Admin Panel and registered E-mails in your Company and Brand(s) Profiles.">
                                                                    Receive orders straight to your inbox <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                                    Online Payment Gateway <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable marketing-table">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">MARKETING SUPPORT included</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><p data-toggle="tooltip" data-placement="right"
                                                                       title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                                                        Welcoming E-mail shot per Brand</p></td>
                                                                <td><p>1</p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div id="panelgoldUae" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <h3>GOLD Plan</h3>
                                                    <p>
                                                        More Marketing, more Leads<br/>Fulfilled by Supplier
                                                    </p>
                                               
                                                    <p class="price-mobile">
                                                        PRICING Per Year
                                                        <select name="uae-gold-brand-number"
                                                                id="goldbrandnumber-mobile" class="form-control">
                                                            <option value="">Your Brand(s)</option>
                                                            <option value="gold1">1 Brand 1 548 AED</option>
                                                            <option value="gold2">Up to 3 Brands 2 148 AED</option>
                                                            <option value="gold3">Up to 6 Brands 2 748 AED</option>
                                                            <option value="gold4">Unlimited Brands 3 588 AED</option>
                                                        </select>
                                                    </p>
                                                    <div class="radio-group">
                                                        <button id="uae-gold-button-mobile"
                                                                style="display: table;margin: 10px auto 0;"
                                                                type="submit" class="btn btn-lg btn-default"
                                                            >Apply Now
                                                        </button>
                                                    </div>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">SELLING B2B through
                                                                    ebeautytrade.com
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="List unlimited items under the same Brand, including products, services, parts, accessories, training courses, etc.">
                                                                    Unlimited Listing per Brand <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                                    Admin Panel <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive enquiries and orders into your Admin Panel and registered E-mails in your Company and Brand(s) Profiles.">
                                                                    Receive orders straight to your inbox <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                                    Online Payment Gateway <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Share and keep updated your Operations and Sales team with your digital catalog. Ease your procedures and spend more time in upsales, marketing and customer service.">
                                                                    Multi-users for your Admin Panel <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You may use this option to make your Brand available only for potential Buyers who meet with your concept and criteria. The selected Buyers will need your Access Code for online ordering.">
                                                                    Access Code for authorized Buyers <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="See how many times Buyers are clicking on your Brands and Products.">
                                                                    Clicks Analytics <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Collect contact details and drive more leads of Buyers when they click on your Brand(s), Products, Call Supplier and Send enquiry button(s).">
                                                                    Collect Buyers' details upon visiting you <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Launch any email, printed or social media campaign  and track conversion rate by adding Promocode from your Admin Panel.">
                                                                    Promocode campaigns <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable marketing-table">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">MARKETING SUPPORT included</div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                                                    Welcoming E-mail shot per Brand <i class="fa fa-info-circle"></i></p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Bespoke E-mail shot per Brand</p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Homepage featuring per Brand</p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Your logo sponsoring our E-Newsletter</p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div id="panelfbeUae" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <h3>FBE Plan</h3>
                                                    <p>
                                                        Selling B2B/B2C incl. logistics<br/>Fulfilled by
                                                        eBeautyTrade
                                                    </p>

                                                    <p class="price-mobile">
                                                        PRICING Per Year
                                                        <select name="uae-fbe-brand-number"
                                                                id="fbebrandnumber-mobile" class="form-control">
                                                            <option value="">Your Brand(s)</option>
                                                            <option value="fbe1">1 Brand 1 200 AED</option>
                                                            <option value="fbe2">2 Brands 4 548 AED</option>
                                                            <option value="fbe3">3 Brands 6 348 AED</option>
                                                            <option value="fbe4">4 Brands 8 148 AED</option>
                                                        </select>
                                                    </p>
                                                    <p style="width:100%">
                                                        + 10% Commission <span data-toggle="tooltip" data-placement="right" title="Over net sales"><i class="fa fa-info-circle"></i></span>
                                                    </p>
                                                    <div class="radio-group">
                                                        <button id="uae-fbe-button-mobile" style="display: table;margin: 10px auto 0;" type="submit" class="btn btn-lg btn-default">
                                                            Apply Now
                                                        </button>
                                                    </div>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">SELLING B2B through
                                                                    ebeautytrade.com
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="List unlimited items under the same Brand, including products, services, parts, accessories, training courses, etc.">
                                                                    Unlimited Listing per Brand <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                                    Admin Panel <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive enquiries and orders into your Admin Panel and registered E-mails in your Company and Brand(s) Profiles.">
                                                                    Receive orders straight to your inbox <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                                    Online Payment Gateway <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Share and keep updated your Operations and Sales team with your digital catalog. Ease your procedures and spend more time in upsales, marketing and customer service.">
                                                                    Multi-users for your Admin Panel <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You may use this option to make your Brand available only for potential Buyers who meet with your concept and criteria. The selected Buyers will need your Access Code for online ordering.">
                                                                    Access Code for authorized Buyers <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="See how many times Buyers are clicking on your Brands and Products.">
                                                                    Clicks Analytics <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Collect contact details and drive more leads of Buyers when they click on your Brand(s), Products, Call Supplier and Send enquiry button(s).">
                                                                    Collect Buyers' details upon visiting you <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Launch any email, printed or social media campaign  and track conversion rate by adding Promocode from your Admin Panel.">
                                                                    Promocode campaigns <i class="fa fa-info-circle"></i></p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th class="">
                                                                <div class="maintext">REGISTRATION (upon request)</div>
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                                    Get your Product registered by us</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>                                                    
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">LOGISTICS SERVICES</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Get Free Temporized Storage up to 700 units or 2 CBM per Brand. Additional storage is available at approx. US$ 40 per CBM /month.">
                                                                    Temporized Storage <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="We check your incoming orders upon notification, we confirm price, availability and  terms with the Buyer, we prepare and pack properly the order, then we proceed with the invoicing.">
                                                                    Order Processing / Invoicing <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Deliveries will be handled by selected couriers depending on weight and destination. Delivery terms and fees will be managed as per Supplier instructions.">
                                                                    Coordinating Deliveries / Payments <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Physical address is mostly needed for orders pick-up from E-store partners, B2B customers and eventual end-users.">
                                                                    Physical address for orders pick-up <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">SELLING B2C through partners</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p>Listing on UAE e-stores (upon request)</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Listing on GCC e-stores (upon request)</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Listing on India e-stores (upon request)</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable marketing-table">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">MARKETING SUPPORT included</div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                                                    Welcoming E-mail shot per Brand <i class="fa fa-info-circle"></i></p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Bespoke E-mail shot per Brand</p></td>
                                                            <td><p>3</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Homepage featuring per Brand</p></td>
                                                            <td><p>3</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Your logo sponsoring our E-Newsletter</p></td>
                                                            <td><p>3</p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 supplier-application supplier-overseas" id="soverseas" style="display:none">
                    <div class="row">
                        <div class="col-md-4 inner-logo">
                            <img src="/front/img/logo.png">
                        </div>
                        <div class="col-md-12 sellerTitle">
                            <h3>Seller Membership Application</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="application-steps">
                            <div class="col-md-3">
                                <p id="overseas_step1" class="active" onclick="displaydiv('step1')"><span
                                            class="numberCircle">1</span>Contact person</p>
                            </div>
                            <div class="col-md-3">
                                <p id="overseas_step2" onclick="displaydiv('step2')"><span class="numberCircle">2</span>Company
                                    details</p>
                            </div>
                            <div class="col-md-3">
                                <p id="overseas_step3" onclick="displaydiv('step3')"><span class="numberCircle">3</span>Brand(s)
                                    details</p>
                            </div>
                            <div class="col-md-3">
                                <p id="overseas_step4" onclick="displaydiv('step4')"><span class="numberCircle">4</span>Select
                                    your
                                    plan</p>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                    <form action="https://ebeautytrade.com/Landing/send" accept-charset="utf-8" method="post"
                          class="contactform" id="ContactForm">
                        <div class="row overseacontact-person" id="contact-person">
                            <div class="form-group">
                                <label for="overseas_first_name">First Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="first-name" id="overseas_first-name"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="overseas_last-name">Last Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="last-name" id="overseas_last-name"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="overseas_position">Position<span class="required">*</span></label>
                                <input type="text" class="form-control" name="position" id="overseas_position" required>
                            </div>
                            <div class="form-group">
                                <label for="overseas_phone">Phone<span class="required">*</span></label>
                                <input type="tel" class="textbox mobile-m form-control" name="phone" id="overseas_phone"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="overseas_email">Email Address<span class="required">*</span></label>
                                <input type="email" class="form-control" name="email" id="overseas_email" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Confirm Email Address<span class="required">*</span></label>
                                <input name="emailConfirm" class="form-control" type="email" id="overseas_confemail"/>
                            </div>
                            <input id="overseas_application-type" name="application-type" type="hidden" value="2">
                            <div class="click-button" onclick="displaydiv('Overseascontactperson')"  style="margin-left: 48%;float: left;">Next</div>
                        </div>
                        <div class="row overseacompany-details" id="company-details" style="display:none">
                            <div class="form-group">
                                <label for="company-name">Company Legal Name<span class="required">*</span></label>
                                <input type="text" class="form-control" name="company-name" id="overseas_company-name"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="registration-number">License or Tax Registration Number</label>
                                <input type="text" class="form-control" name="registration-number"
                                       id="overseas_registration-number">
                            </div>
                            <div class="form-group">
                                <label for="company-number">Company Address<span class="required">*</span></label>
                                <input type="text" class="form-control" name="company-address"
                                       id="overseas_company-address"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="company-number">Company Phone Number<span class="required">*</span></label>
                                <input type="tel" class="textbox mobile-m form-control" name="company-number"
                                       id="overseas_company-number"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="city">City<span class="required">*</span></label>
                                <input type="text" class="form-control" name="city" id="overseas_city" required>
                            </div>
                            <div class="form-group">
                                <label for="business-based">Country<span class="required">*</span></label>
                                <select name="business-based" id="overseas_business-based" class="form-control">
                                    <option value="" selected>choose...</option>
                                    <option value="AF">Afghanistan</option>

                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>

                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>

                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>

                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>

                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>

                                    <option value="BO">Bolivia</option>
                                    <option value="BA">Bosnia and Herzegowina</option>
                                    <option value="BW">Botswana</option>

                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="BN">Brunei Darussalam</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>

                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>

                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>

                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>

                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CD">Congo, The Democratic Republic of the</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>

                                    <option value="CI">Cote d'Ivoire</option>
                                    <option value="HR">Croatia (Hrvatska)</option>
                                    <option value="CU">Cuba</option>

                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>

                                    <option value="TP">East Timor</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>

                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands (Malvinas)</option>

                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>

                                    <option value="FR">France</option>
                                    <option value="FX">France, Metropolitan</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>

                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>

                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>

                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>

                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard and Mc Donald Islands</option>
                                    <option value="VA">Holy See (Vatican City State)</option>
                                    <option value="HN">Honduras</option>

                                    <option value="HK">Hong Kong</option>

                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran (Islamic Republic of)</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IL">Israel</option>

                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KP">Korea, Democratic People's Republic of</option>
                                    <option value="KR">Korea, Republic of</option>

                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Lao People's Democratic Republic</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libyan Arab Jamahiriya</option>

                                    <option value="LI">Liechtenstein</option>

                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macau</option>
                                    <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>

                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>

                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>

                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia, Federated States of</option>
                                    <option value="MD">Moldova, Republic of</option>

                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>

                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>

                                    <option value="NL">Netherlands</option>
                                    <option value="AN">Netherlands Antilles</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>

                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>

                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>

                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>

                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>

                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russian Federation</option>

                                    <option value="RW">Rwanda</option>

                                    <option value="KN">Saint Kitts and Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome and Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia (Slovak Republic)</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia and the South Sandwich Islands</option>

                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SH">St. Helena</option>
                                    <option value="PM">St. Pierre and Miquelon</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard and Jan Mayen Islands</option>
                                    <option value="SZ">Swaziland</option>

                                    <option value="SE">Sweden</option>

                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syrian Arab Republic</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania, United Republic of</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TG">Togo</option>

                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>

                                    <option value="TT">Trinidad and Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks and Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>

                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>

                                    <option value="GB">United Kingdom</option>
                                    <option value="US">United States</option>
                                    <option value="UM">United States Minor Outlying Islands</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>

                                    <option value="VU">Vanuatu</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Viet Nam</option>
                                    <option value="VG">Virgin Islands (British)</option>

                                    <option value="VI">Virgin Islands (U.S.)</option>
                                    <option value="WF">Wallis and Futuna Islands</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>

                                    <option value="YU">Yugoslavia</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="website">Website</label>
                                <input type="text" class="form-control" name="website" id="overseas_website">
                            </div>
                            
                            <input type="hidden" class="form-control" name="overseas_userid" id="overseas_userid">
                            
                            <div class="click-button" onclick="displaydiv('OverseascompanyDetailsNext')">Next</div>
                            <div class="click-button" onclick="displaydiv('OverseascompanyDetailsBack')">Back</div>
                        </div>
                        <div class="row overseabrands-details" id="brands-details" style="display:none">
                            <div class="row">
                                <div class="col-md-12" style="padding: 0">
                                    <table class="table table-bordered" id="Overseasdynamic_field" style="border:none">
                                        <tr>
                                            <td>
                                                Brand Name<span class="required">*</span>
                                            </td>
                                            <td>
                                                Country of Origin<span class="required">*</span>
                                            </td>
                                            <td>
                                                Website
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" id="overseas_brand-name1"
                                                       name="brand-name1">
                                            </td>
                                            <td>
                                                <select name="country1" id="overseas_country1" class="form-control">
                                                    <option value=""><span class="flag-icon flag-icon-gr"></span>Select
                                                        Country
                                                    </option>
                                                    <option value="Afghanistan">Afghanistan</option>
                                                    <option value="Albania">Albania</option>
                                                    <option value="Algeria">Algeria</option>
                                                    <option value="American Samoa">American Samoa</option>
                                                    <option value="Andorra">Andorra</option>
                                                    <option value="Angola">Angola</option>
                                                    <option value="Anguilla">Anguilla</option>
                                                    <option value="Antartica">Antarctica</option>
                                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                    <option value="Argentina">Argentina</option>
                                                    <option value="Armenia">Armenia</option>
                                                    <option value="Aruba">Aruba</option>
                                                    <option value="Australia">Australia</option>
                                                    <option value="Austria">Austria</option>
                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                    <option value="Bahamas">Bahamas</option>
                                                    <option value="Bahrain">Bahrain</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                    <option value="Barbados">Barbados</option>
                                                    <option value="Belarus">Belarus</option>
                                                    <option value="Belgium">Belgium</option>
                                                    <option value="Belize">Belize</option>
                                                    <option value="Benin">Benin</option>
                                                    <option value="Bermuda">Bermuda</option>
                                                    <option value="Bhutan">Bhutan</option>
                                                    <option value="Bolivia">Bolivia</option>
                                                    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina
                                                    </option>
                                                    <option value="Botswana">Botswana</option>
                                                    <option value="Bouvet Island">Bouvet Island</option>
                                                    <option value="Brazil">Brazil</option>
                                                    <option value="British Indian Ocean Territory">British Indian Ocean
                                                        Territory
                                                    </option>
                                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                    <option value="Bulgaria">Bulgaria</option>
                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                    <option value="Burundi">Burundi</option>
                                                    <option value="Cambodia">Cambodia</option>
                                                    <option value="Cameroon">Cameroon</option>
                                                    <option value="Canada">Canada</option>
                                                    <option value="Cape Verde">Cape Verde</option>
                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                    <option value="Central African Republic">Central African Republic
                                                    </option>
                                                    <option value="Chad">Chad</option>
                                                    <option value="Chile">Chile</option>
                                                    <option value="China">China</option>
                                                    <option value="Christmas Island">Christmas Island</option>
                                                    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                    <option value="Colombia">Colombia</option>
                                                    <option value="Comoros">Comoros</option>
                                                    <option value="Congo">Congo</option>
                                                    <option value="Congo">Congo, the Democratic Republic of the</option>
                                                    <option value="Cook Islands">Cook Islands</option>
                                                    <option value="Costa Rica">Costa Rica</option>
                                                    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                    <option value="Croatia">Croatia (Hrvatska)</option>
                                                    <option value="Cuba">Cuba</option>
                                                    <option value="Cyprus">Cyprus</option>
                                                    <option value="Czech Republic">Czech Republic</option>
                                                    <option value="Denmark">Denmark</option>
                                                    <option value="Djibouti">Djibouti</option>
                                                    <option value="Dominica">Dominica</option>
                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                    <option value="East Timor">East Timor</option>
                                                    <option value="Ecuador">Ecuador</option>
                                                    <option value="Egypt">Egypt</option>
                                                    <option value="El Salvador">El Salvador</option>
                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                    <option value="Eritrea">Eritrea</option>
                                                    <option value="Estonia">Estonia</option>
                                                    <option value="Ethiopia">Ethiopia</option>
                                                    <option value="Falkland Islands">Falkland Islands (Malvinas)
                                                    </option>
                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                    <option value="Fiji">Fiji</option>
                                                    <option value="Finland">Finland</option>
                                                    <option value="France">France</option>
                                                    <option value="France Metropolitan">France, Metropolitan</option>
                                                    <option value="French Guiana">French Guiana</option>
                                                    <option value="French Polynesia">French Polynesia</option>
                                                    <option value="French Southern Territories">French Southern
                                                        Territories
                                                    </option>
                                                    <option value="Gabon">Gabon</option>
                                                    <option value="Gambia">Gambia</option>
                                                    <option value="Georgia">Georgia</option>
                                                    <option value="Germany">Germany</option>
                                                    <option value="Ghana">Ghana</option>
                                                    <option value="Gibraltar">Gibraltar</option>
                                                    <option value="Greece">Greece</option>
                                                    <option value="Greenland">Greenland</option>
                                                    <option value="Grenada">Grenada</option>
                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                    <option value="Guam">Guam</option>
                                                    <option value="Guatemala">Guatemala</option>
                                                    <option value="Guinea">Guinea</option>
                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                    <option value="Guyana">Guyana</option>
                                                    <option value="Haiti">Haiti</option>
                                                    <option value="Heard and McDonald Islands">Heard and Mc Donald
                                                        Islands
                                                    </option>
                                                    <option value="Holy See">Holy See (Vatican City State)</option>
                                                    <option value="Honduras">Honduras</option>
                                                    <option value="Hong Kong">Hong Kong</option>
                                                    <option value="Hungary">Hungary</option>
                                                    <option value="Iceland">Iceland</option>
                                                    <option value="India">India</option>
                                                    <option value="Indonesia">Indonesia</option>
                                                    <option value="Iran">Iran (Islamic Republic of)</option>
                                                    <option value="Iraq">Iraq</option>
                                                    <option value="Ireland">Ireland</option>
                                                    <option value="Israel">Israel</option>
                                                    <option value="Italy">Italy</option>
                                                    <option value="Jamaica">Jamaica</option>
                                                    <option value="Japan">Japan</option>
                                                    <option value="Jordan">Jordan</option>
                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                    <option value="Kenya">Kenya</option>
                                                    <option value="Kiribati">Kiribati</option>
                                                    <option value="Korea">Korea, Republic of</option>
                                                    <option value="Kuwait">Kuwait</option>
                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                    <option value="Lao">Lao People's Democratic Republic</option>
                                                    <option value="Latvia">Latvia</option>
                                                    <option value="Lebanon">Lebanon</option>
                                                    <option value="Lesotho">Lesotho</option>
                                                    <option value="Liberia">Liberia</option>
                                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya
                                                    </option>
                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                    <option value="Lithuania">Lithuania</option>
                                                    <option value="Luxembourg">Luxembourg</option>
                                                    <option value="Macau">Macau</option>
                                                    <option value="Macedonia">Macedonia, The Former Yugoslav Republic of
                                                    </option>
                                                    <option value="Madagascar">Madagascar</option>
                                                    <option value="Malawi">Malawi</option>
                                                    <option value="Malaysia">Malaysia</option>
                                                    <option value="Maldives">Maldives</option>
                                                    <option value="Mali">Mali</option>
                                                    <option value="Malta">Malta</option>
                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                    <option value="Martinique">Martinique</option>
                                                    <option value="Mauritania">Mauritania</option>
                                                    <option value="Mauritius">Mauritius</option>
                                                    <option value="Mayotte">Mayotte</option>
                                                    <option value="Mexico">Mexico</option>
                                                    <option value="Micronesia">Micronesia, Federated States of</option>
                                                    <option value="Moldova">Moldova, Republic of</option>
                                                    <option value="Monaco">Monaco</option>
                                                    <option value="Mongolia">Mongolia</option>
                                                    <option value="Montserrat">Montserrat</option>
                                                    <option value="Morocco">Morocco</option>
                                                    <option value="Mozambique">Mozambique</option>
                                                    <option value="Myanmar">Myanmar</option>
                                                    <option value="Namibia">Namibia</option>
                                                    <option value="Nauru">Nauru</option>
                                                    <option value="Nepal">Nepal</option>
                                                    <option value="Netherlands">Netherlands</option>
                                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                    <option value="New Caledonia">New Caledonia</option>
                                                    <option value="New Zealand">New Zealand</option>
                                                    <option value="Nicaragua">Nicaragua</option>
                                                    <option value="Niger">Niger</option>
                                                    <option value="Nigeria">Nigeria</option>
                                                    <option value="Niue">Niue</option>
                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                    <option value="North Korea">North Korea</option>
                                                    <option value="Northern Mariana Islands">Northern Mariana Islands
                                                    </option>
                                                    <option value="Norway">Norway</option>
                                                    <option value="Oman">Oman</option>
                                                    <option value="Pakistan">Pakistan</option>
                                                    <option value="Palau">Palau</option>
                                                    <option value="Panama">Panama</option>
                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                    <option value="Paraguay">Paraguay</option>
                                                    <option value="Peru">Peru</option>
                                                    <option value="Philippines">Philippines</option>
                                                    <option value="Pitcairn">Pitcairn</option>
                                                    <option value="Poland">Poland</option>
                                                    <option value="Portugal">Portugal</option>
                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                    <option value="Qatar">Qatar</option>
                                                    <option value="Reunion">Reunion</option>
                                                    <option value="Romania">Romania</option>
                                                    <option value="Russia">Russian Federation</option>
                                                    <option value="Rwanda">Rwanda</option>
                                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                    <option value="Saint LUCIA">Saint LUCIA</option>
                                                    <option value="Saint Vincent">Saint Vincent and the Grenadines
                                                    </option>
                                                    <option value="Samoa">Samoa</option>
                                                    <option value="San Marino">San Marino</option>
                                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                    <option value="Senegal">Senegal</option>
                                                    <option value="Seychelles">Seychelles</option>
                                                    <option value="Sierra">Sierra Leone</option>
                                                    <option value="Singapore">Singapore</option>
                                                    <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                    <option value="Slovenia">Slovenia</option>
                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                    <option value="Somalia">Somalia</option>
                                                    <option value="South Africa">South Africa</option>
                                                    <option value="South Georgia">South Georgia and the South Sandwich
                                                        Islands
                                                    </option>
                                                    <option value="South Korea">South Korea</option>
                                                    <option value="South Sudan">South Sudan</option>
                                                    <option value="Span">Spain</option>
                                                    <option value="SriLanka">Sri Lanka</option>
                                                    <option value="St. Helena">St. Helena</option>
                                                    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon
                                                    </option>
                                                    <option value="Sudan">Sudan</option>
                                                    <option value="Suriname">Suriname</option>
                                                    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                    <option value="Swaziland">Swaziland</option>
                                                    <option value="Sweden">Sweden</option>
                                                    <option value="Switzerland">Switzerland</option>
                                                    <option value="Syria">Syrian Arab Republic</option>
                                                    <option value="Taiwan">Taiwan, Province of China</option>
                                                    <option value="Tajikistan">Tajikistan</option>
                                                    <option value="Tanzania">Tanzania, United Republic of</option>
                                                    <option value="Thailand">Thailand</option>
                                                    <option value="Togo">Togo</option>
                                                    <option value="Tokelau">Tokelau</option>
                                                    <option value="Tonga">Tonga</option>
                                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                    <option value="Tunisia">Tunisia</option>
                                                    <option value="Turkey">Turkey</option>
                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                    <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                    <option value="Tuvalu">Tuvalu</option>
                                                    <option value="Uganda">Uganda</option>
                                                    <option value="Ukraine">Ukraine</option>
                                                    <option value="United Arab Emirates" <?php if ($user_country == "United Arab Emirates" || $user_country == "UAE") { ?> selected <?php } ?>>
                                                        United Arab Emirates
                                                    </option>
                                                    <option value="United Kingdom">United Kingdom</option>
                                                    <option value="United States">United States</option>
                                                    <option value="United States Minor Outlying Islands">United States
                                                        Minor
                                                        Outlying
                                                        Islands
                                                    </option>
                                                    <option value="Uruguay">Uruguay</option>
                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                    <option value="Vanuatu">Vanuatu</option>
                                                    <option value="Venezuela">Venezuela</option>
                                                    <option value="Vietnam">Viet Nam</option>
                                                    <option value="Virgin Islands (British)">Virgin Islands (British)
                                                    </option>
                                                    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                    <option value="Wallis and Futana Islands">Wallis and Futuna Islands
                                                    </option>
                                                    <option value="Western Sahara">Western Sahara</option>
                                                    <option value="Yemen">Yemen</option>
                                                    <option value="Yugoslavia">Yugoslavia</option>
                                                    <option value="Zambia">Zambia</option>
                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" name="brand-website1"
                                                       id="brand-website1">
                                            </td>
                                            <td><br/><input type="hidden" class="form-control"
                                                            name="Overseasamountofbrands" id="Overseasamountofbrands" value="1"></td>
                                        </tr>
                                    </table>
                                    <div class="col-md-12 morebrand">
                                        <div class="click-button">
                                            <button type="button" name="add" id="Overseasadd" class="btn btn-success">
                                                Add Brand
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <textarea type="text" class="form-control" id="Overseasbrand-description"
                                              name="brand-description"
                                              placeholder="Please describe any special requirements you may have"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="click-button" onclick="displaydiv('OverseasbrandsDetailsNext')">Select your plan
                            </div>
                            <div class="click-button" onclick="displaydiv('OverseasbrandsDetailsBack')">Back</div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                var i = 1;
                                $('#Overseasadd').click(function () {
                                    i++;
                                    $('#Overseasdynamic_field').append('<tr id="row' + i + '">' +
                                        '<td><input type="text" class="form-control" id="brand-name' + i + '" name="brand-name' + i + '"></td>' +
                                        '<td><select name="country' + i + '" class="form-control">\n' +
                                        '<option value=""><span class="flag-icon flag-icon-gr"></span>Select Country</option>\n' +
                                        '<option value="Afghanistan">Afghanistan</option>\n' +
                                        '<option value="Albania">Albania</option>\n' +
                                        '<option value="Algeria">Algeria</option>\n' +
                                        '<option value="American Samoa">American Samoa</option>\n' +
                                        '<option value="Andorra">Andorra</option>\n' +
                                        '<option value="Angola">Angola</option>\n' +
                                        '<option value="Anguilla">Anguilla</option>\n' +
                                        '<option value="Antartica">Antarctica</option>\n' +
                                        '<option value="Antigua and Barbuda">Antigua and Barbuda</option>\n' +
                                        '<option value="Argentina">Argentina</option>\n' +
                                        '<option value="Armenia">Armenia</option>\n' +
                                        '<option value="Aruba">Aruba</option>\n' +
                                        '<option value="Australia">Australia</option>\n' +
                                        '<option value="Austria">Austria</option>\n' +
                                        '<option value="Azerbaijan">Azerbaijan</option>\n' +
                                        '<option value="Bahamas">Bahamas</option>\n' +
                                        '<option value="Bahrain">Bahrain</option>\n' +
                                        '<option value="Bangladesh">Bangladesh</option>\n' +
                                        '<option value="Barbados">Barbados</option>\n' +
                                        '<option value="Belarus">Belarus</option>\n' +
                                        '<option value="Belgium">Belgium</option>\n' +
                                        '<option value="Belize">Belize</option>\n' +
                                        '<option value="Benin">Benin</option>\n' +
                                        '<option value="Bermuda">Bermuda</option>\n' +
                                        '<option value="Bhutan">Bhutan</option>\n' +
                                        '<option value="Bolivia">Bolivia</option>\n' +
                                        '<option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>\n' +
                                        '<option value="Botswana">Botswana</option>\n' +
                                        '<option value="Bouvet Island">Bouvet Island</option>\n' +
                                        '<option value="Brazil">Brazil</option>\n' +
                                        '<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>\n' +
                                        '<option value="Brunei Darussalam">Brunei Darussalam</option>\n' +
                                        '<option value="Bulgaria">Bulgaria</option>\n' +
                                        '<option value="Burkina Faso">Burkina Faso</option>\n' +
                                        '<option value="Burundi">Burundi</option>\n' +
                                        '<option value="Cambodia">Cambodia</option>\n' +
                                        '<option value="Cameroon">Cameroon</option>\n' +
                                        '<option value="Canada">Canada</option>\n' +
                                        '<option value="Cape Verde">Cape Verde</option>\n' +
                                        '<option value="Cayman Islands">Cayman Islands</option>\n' +
                                        '<option value="Central African Republic">Central African Republic</option>\n' +
                                        '<option value="Chad">Chad</option>\n' +
                                        '<option value="Chile">Chile</option>\n' +
                                        '<option value="China">China</option>\n' +
                                        '<option value="Christmas Island">Christmas Island</option>\n' +
                                        '<option value="Cocos Islands">Cocos (Keeling) Islands</option>\n' +
                                        '<option value="Colombia">Colombia</option>\n' +
                                        '<option value="Comoros">Comoros</option>\n' +
                                        '<option value="Congo">Congo</option>\n' +
                                        '<option value="Congo">Congo, the Democratic Republic of the</option>\n' +
                                        '<option value="Cook Islands">Cook Islands</option>\n' +
                                        '<option value="Costa Rica">Costa Rica</option>\n' +
                                        '<option value="Cota D\'Ivoire">Cote d\'Ivoire</option>\n' +
                                        '<option value="Croatia">Croatia (Hrvatska)</option>\n' +
                                        '<option value="Cuba">Cuba</option>\n' +
                                        '<option value="Cyprus">Cyprus</option>\n' +
                                        '<option value="Czech Republic">Czech Republic</option>\n' +
                                        '<option value="Denmark">Denmark</option>\n' +
                                        '<option value="Djibouti">Djibouti</option>\n' +
                                        '<option value="Dominica">Dominica</option>\n' +
                                        '<option value="Dominican Republic">Dominican Republic</option>\n' +
                                        '<option value="East Timor">East Timor</option>\n' +
                                        '<option value="Ecuador">Ecuador</option>\n' +
                                        '<option value="Egypt">Egypt</option>\n' +
                                        '<option value="El Salvador">El Salvador</option>\n' +
                                        '<option value="Equatorial Guinea">Equatorial Guinea</option>\n' +
                                        '<option value="Eritrea">Eritrea</option>\n' +
                                        '<option value="Estonia">Estonia</option>\n' +
                                        '<option value="Ethiopia">Ethiopia</option>\n' +
                                        '<option value="Falkland Islands">Falkland Islands (Malvinas)</option>\n' +
                                        '<option value="Faroe Islands">Faroe Islands</option>\n' +
                                        '<option value="Fiji">Fiji</option>\n' +
                                        '<option value="Finland">Finland</option>\n' +
                                        '<option value="France">France</option>\n' +
                                        '<option value="France Metropolitan">France, Metropolitan</option>\n' +
                                        '<option value="French Guiana">French Guiana</option>\n' +
                                        '<option value="French Polynesia">French Polynesia</option>\n' +
                                        '<option value="French Southern Territories">French Southern Territories</option>\n' +
                                        '<option value="Gabon">Gabon</option>\n' +
                                        '<option value="Gambia">Gambia</option>\n' +
                                        '<option value="Georgia">Georgia</option>\n' +
                                        '<option value="Germany">Germany</option>\n' +
                                        '<option value="Ghana">Ghana</option>\n' +
                                        '<option value="Gibraltar">Gibraltar</option>\n' +
                                        '<option value="Greece">Greece</option>\n' +
                                        '<option value="Greenland">Greenland</option>\n' +
                                        '<option value="Grenada">Grenada</option>\n' +
                                        '<option value="Guadeloupe">Guadeloupe</option>\n' +
                                        '<option value="Guam">Guam</option>\n' +
                                        '<option value="Guatemala">Guatemala</option>\n' +
                                        '<option value="Guinea">Guinea</option>\n' +
                                        '<option value="Guinea-Bissau">Guinea-Bissau</option>\n' +
                                        '<option value="Guyana">Guyana</option>\n' +
                                        '<option value="Haiti">Haiti</option>\n' +
                                        '<option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>\n' +
                                        '<option value="Holy See">Holy See (Vatican City State)</option>\n' +
                                        '<option value="Honduras">Honduras</option>\n' +
                                        '<option value="Hong Kong">Hong Kong</option>\n' +
                                        '<option value="Hungary">Hungary</option>\n' +
                                        '<option value="Iceland">Iceland</option>\n' +
                                        '<option value="India">India</option>\n' +
                                        '<option value="Indonesia">Indonesia</option>\n' +
                                        '<option value="Iran">Iran (Islamic Republic of)</option>\n' +
                                        '<option value="Iraq">Iraq</option>\n' +
                                        '<option value="Ireland">Ireland</option>\n' +
                                        '<option value="Israel">Israel</option>\n' +
                                        '<option value="Italy">Italy</option>\n' +
                                        '<option value="Jamaica">Jamaica</option>\n' +
                                        '<option value="Japan">Japan</option>\n' +
                                        '<option value="Jordan">Jordan</option>\n' +
                                        '<option value="Kazakhstan">Kazakhstan</option>\n' +
                                        '<option value="Kenya">Kenya</option>\n' +
                                        '<option value="Kiribati">Kiribati</option>\n' +
                                        '<option value="Korea">Korea, Republic of</option>\n' +
                                        '<option value="Kuwait">Kuwait</option>\n' +
                                        '<option value="Kyrgyzstan">Kyrgyzstan</option>\n' +
                                        '<option value="Lao">Lao People\'s Democratic Republic</option>\n' +
                                        '<option value="Latvia">Latvia</option>\n' +
                                        '<option value="Lebanon">Lebanon</option>\n' +
                                        '<option value="Lesotho">Lesotho</option>\n' +
                                        '<option value="Liberia">Liberia</option>\n' +
                                        '<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>\n' +
                                        '<option value="Liechtenstein">Liechtenstein</option>\n' +
                                        '<option value="Lithuania">Lithuania</option>\n' +
                                        '<option value="Luxembourg">Luxembourg</option>\n' +
                                        '<option value="Macau">Macau</option>\n' +
                                        '<option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>\n' +
                                        '<option value="Madagascar">Madagascar</option>\n' +
                                        '<option value="Malawi">Malawi</option>\n' +
                                        '<option value="Malaysia">Malaysia</option>\n' +
                                        '<option value="Maldives">Maldives</option>\n' +
                                        '<option value="Mali">Mali</option>\n' +
                                        '<option value="Malta">Malta</option>\n' +
                                        '<option value="Marshall Islands">Marshall Islands</option>\n' +
                                        '<option value="Martinique">Martinique</option>\n' +
                                        '<option value="Mauritania">Mauritania</option>\n' +
                                        '<option value="Mauritius">Mauritius</option>\n' +
                                        '<option value="Mayotte">Mayotte</option>\n' +
                                        '<option value="Mexico">Mexico</option>\n' +
                                        '<option value="Micronesia">Micronesia, Federated States of</option>\n' +
                                        '<option value="Moldova">Moldova, Republic of</option>\n' +
                                        '<option value="Monaco">Monaco</option>\n' +
                                        '<option value="Mongolia">Mongolia</option>\n' +
                                        '<option value="Montserrat">Montserrat</option>\n' +
                                        '<option value="Morocco">Morocco</option>\n' +
                                        '<option value="Mozambique">Mozambique</option>\n' +
                                        '<option value="Myanmar">Myanmar</option>\n' +
                                        '<option value="Namibia">Namibia</option>\n' +
                                        '<option value="Nauru">Nauru</option>\n' +
                                        '<option value="Nepal">Nepal</option>\n' +
                                        '<option value="Netherlands">Netherlands</option>\n' +
                                        '<option value="Netherlands Antilles">Netherlands Antilles</option>\n' +
                                        '<option value="New Caledonia">New Caledonia</option>\n' +
                                        '<option value="New Zealand">New Zealand</option>\n' +
                                        '<option value="Nicaragua">Nicaragua</option>\n' +
                                        '<option value="Niger">Niger</option>\n' +
                                        '<option value="Nigeria">Nigeria</option>\n' +
                                        '<option value="Niue">Niue</option>\n' +
                                        '<option value="Norfolk Island">Norfolk Island</option>\n' +
                                        '<option value="North Korea">North Korea</option>\n' +
                                        '<option value="Northern Mariana Islands">Northern Mariana Islands</option>\n' +
                                        '<option value="Norway">Norway</option>\n' +
                                        '<option value="Oman">Oman</option>\n' +
                                        '<option value="Pakistan">Pakistan</option>\n' +
                                        '<option value="Palau">Palau</option>\n' +
                                        '<option value="Panama">Panama</option>\n' +
                                        '<option value="Papua New Guinea">Papua New Guinea</option>\n' +
                                        '<option value="Paraguay">Paraguay</option>\n' +
                                        '<option value="Peru">Peru</option>\n' +
                                        '<option value="Philippines">Philippines</option>\n' +
                                        '<option value="Pitcairn">Pitcairn</option>\n' +
                                        '<option value="Poland">Poland</option>\n' +
                                        '<option value="Portugal">Portugal</option>\n' +
                                        '<option value="Puerto Rico">Puerto Rico</option>\n' +
                                        '<option value="Qatar">Qatar</option>\n' +
                                        '<option value="Reunion">Reunion</option>\n' +
                                        '<option value="Romania">Romania</option>\n' +
                                        '<option value="Russia">Russian Federation</option>\n' +
                                        '<option value="Rwanda">Rwanda</option>\n' +
                                        '<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>\n' +
                                        '<option value="Saint LUCIA">Saint LUCIA</option>\n' +
                                        '<option value="Saint Vincent">Saint Vincent and the Grenadines</option>\n' +
                                        '<option value="Samoa">Samoa</option>\n' +
                                        '<option value="San Marino">San Marino</option>\n' +
                                        '<option value="Sao Tome and Principe">Sao Tome and Principe</option>\n' +
                                        '<option value="Saudi Arabia">Saudi Arabia</option>\n' +
                                        '<option value="Senegal">Senegal</option>\n' +
                                        '<option value="Seychelles">Seychelles</option>\n' +
                                        '<option value="Sierra">Sierra Leone</option>\n' +
                                        '<option value="Singapore">Singapore</option>\n' +
                                        '<option value="Slovakia">Slovakia (Slovak Republic)</option>\n' +
                                        '<option value="Slovenia">Slovenia</option>\n' +
                                        '<option value="Solomon Islands">Solomon Islands</option>\n' +
                                        '<option value="Somalia">Somalia</option>\n' +
                                        '<option value="South Africa">South Africa</option>\n' +
                                        '<option value="South Georgia">South Georgia and the South Sandwich Islands</option>\n' +
                                        '<option value="South Korea">South Korea</option>\n' +
                                        '<option value="South Sudan">South Sudan</option>\n' +
                                        '<option value="Span">Spain</option>\n' +
                                        '<option value="SriLanka">Sri Lanka</option>\n' +
                                        '<option value="St. Helena">St. Helena</option>\n' +
                                        '<option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>\n' +
                                        '<option value="Sudan">Sudan</option>\n' +
                                        '<option value="Suriname">Suriname</option>\n' +
                                        '<option value="Svalbard">Svalbard and Jan Mayen Islands</option>\n' +
                                        '<option value="Swaziland">Swaziland</option>\n' +
                                        '<option value="Sweden">Sweden</option>\n' +
                                        '<option value="Switzerland">Switzerland</option>\n' +
                                        '<option value="Syria">Syrian Arab Republic</option>\n' +
                                        '<option value="Taiwan">Taiwan, Province of China</option>\n' +
                                        '<option value="Tajikistan">Tajikistan</option>\n' +
                                        '<option value="Tanzania">Tanzania, United Republic of</option>\n' +
                                        '<option value="Thailand">Thailand</option>\n' +
                                        '<option value="Togo">Togo</option>\n' +
                                        '<option value="Tokelau">Tokelau</option>\n' +
                                        '<option value="Tonga">Tonga</option>\n' +
                                        '<option value="Trinidad and Tobago">Trinidad and Tobago</option>\n' +
                                        '<option value="Tunisia">Tunisia</option>\n' +
                                        '<option value="Turkey">Turkey</option>\n' +
                                        '<option value="Turkmenistan">Turkmenistan</option>\n' +
                                        '<option value="Turks and Caicos">Turks and Caicos Islands</option>\n' +
                                        '<option value="Tuvalu">Tuvalu</option>\n' +
                                        '<option value="Uganda">Uganda</option>\n' +
                                        '<option value="Ukraine">Ukraine</option>\n' +
                                        '<option value="United Arab Emirates">United Arab Emirates</option>\n' +
                                        '<option value="United Kingdom">United Kingdom</option>\n' +
                                        '<option value="United States">United States</option>\n' +
                                        '<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>\n' +
                                        '<option value="Uruguay">Uruguay</option>\n' +
                                        '<option value="Uzbekistan">Uzbekistan</option>\n' +
                                        '<option value="Vanuatu">Vanuatu</option>\n' +
                                        '<option value="Venezuela">Venezuela</option>\n' +
                                        '<option value="Vietnam">Viet Nam</option>\n' +
                                        '<option value="Virgin Islands (British)">Virgin Islands (British)</option>\n' +
                                        '<option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>\n' +
                                        '<option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>\n' +
                                        '<option value="Western Sahara">Western Sahara</option>\n' +
                                        '<option value="Yemen">Yemen</option>\n' +
                                        '<option value="Yugoslavia">Yugoslavia</option>\n' +
                                        '<option value="Zambia">Zambia</option>\n' +
                                        '<option value="Zimbabwe">Zimbabwe</option>\n' +
                                        '</select>' +
                                        '<td><input type="text" class="form-control" name="brand-website' + i + '" id="brand-website"></td>' +
                                        '<td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">Delete</button><input type="hidden" class="form-control" name="Overseasamountofbrands" value="' + i + '"></td></tr>');
                                });

                                $(document).on('click', '.btn_remove', function () {
                                    var button_id = $(this).attr("id");
                                    $('#row' + button_id + '').remove();
                                });

                                /*$('#submit').click(function(){
                                $.ajax({
                                    url:"name.php",
                                    method:"POST",
                                    data:$('#add_name').serialize(),
                                    success:function(data)
                                    {
                                        alert(data);
                                        $('#add_name')[0].reset();
                                    }
                                });
                            });*/

                            });
                        </script>
                        <div class="row overseabundle-details" id="bundle-details" style="display:none">
                            <?php $random = substr(md5(mt_rand()), 0, 7); ?>
                            <input type="hidden" name="random-number" id="overseas_random-number" value="<?php echo $random; ?>"/>
                            
                            <div class="desktopVersion">
                                <table class="table pricingTable sellerPlan">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr class="no-border">
                                        <th class="">
                                            <div class="maintext">SELLER PLANS<br/>For Suppliers overseas</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FREE</div>
                                            <p>
                                                Selling B2B, basic Online Store<br/>Shipping from based Country
                                            </p>
                                        </th>
                                        <th>
                                            <div class="eyebrow">GOLD</div>
                                            <p>
                                                More Marketing, more Leads<br/>Shipping from based Country
                                            </p>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FBE</div>
                                            <p>
                                                Selling B2B/B2C, incl. Logistics<br/>Fulfilled by eBeautyTrade
                                            </p>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="no-border pricing">
                                        <td><p>
                                                PRICING
                                            </p></td>
                                        <td><p>
                                                STARTING /MO
                                            </p></td>
                                        <td><p>
                                                STARTING /MO
                                            </p></td>
                                        <td><p>
                                                STARTING /MO
                                            </p></td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p class="bundle-price-month">
                                                0<sup>AED</sup>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-month">
                                                $ <span id="gold-price-month-overseas">79</span>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-month">
                                                $ <span id="fbe-price-month-overseas">100</span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td><p>
                                                PAID YEARLY
                                            </p></td>
                                        <td><p>
                                                PAID YEARLY
                                            </p></td>
                                        <td><p>
                                                PAID YEARLY
                                            </p></td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p class="bundle-price-year">
                                                0<sup>AED</sup>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-year">
                                                $ <span id="gold-price-year-overseas">948</span>
                                            </p>
                                        </td>
                                        <td>
                                            <p class="bundle-price-year">
                                                $ <span id="fbe-price-year-overseas">1 200</span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td><p></p></td>
                                        <td><p></p></td>
                                        <td>
                                            <p style="font-weight: bold;">
                                                + 10% Commission <span data-toggle="tooltip" data-placement="right"
                                                                       title="Over net sales"><i
                                                            class="fa fa-info-circle"></i></span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p>
                                                <select name="overseas-free-brand-number" id="freebrandnumberoverseas"
                                                        class="form-control">
                                                    <option value="">No of Brands</option>
                                                    <option value="free1-overseas">1 Brand</option>
                                                    <option value="free2-overseas">2 Brands</option>
                                                    <option value="free3-overseas">3 Brands</option>
                                                </select>
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <select name="overseas-gold-brand-number" id="goldbrandnumberoverseas"
                                                        class="form-control">
                                                    <option value="">No of Brands</option>
                                                    <option value="gold1-overseas">1 Brand</option>
                                                    <option value="gold2-overseas">2 Brands</option>
                                                    <option value="gold3-overseas">3 Brands</option>
                                                </select>
                                            </p>
                                        </td>
                                        <td>
                                            <p>
                                                <select name="overseas-free-brand-number" id="fbebrandnumberoverseas"
                                                        class="form-control">
                                                    <option value="">No of Brands</option>
                                                    <option value="fbe1-overseas">1 Brand</option>
                                                    <option value="fbe2-overseas">2 Brands</option>
                                                    <option value="fbe3-overseas">3 Brands</option>
                                                </select>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <p id="free-terms-overseas" style="display:none">
                                                <input style="margin:2px 4px 0 0" type="checkbox" name="agree"> I have
                                                read
                                                and accepted the Terms and Policy
                                            </p>
                                        </td>
                                        <td>
                                            <p id="gold-terms-overseas" style="display:none">
                                                <input style="margin:2px 4px 0 0" type="checkbox" name="agree"> I have
                                                read
                                                and accepted the Terms and Policy
                                            </p>
                                        </td>
                                        <td>
                                            <p id="fbe-terms-overseas" style="display:none">
                                                <input style="margin:2px 4px 0 0" type="checkbox" name="agree"> I have
                                                read
                                                and accepted the Terms and Policy
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="no-border">
                                        <td><p></p></td>
                                        <td>
                                            <div class="radio-group">
                                                <button id="overseas-free-button"
                                                        style="display: table;margin: 10px auto 0;" type="submit"
                                                        class="btn btn-lg btn-default" disabled>Apply Now
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio-group">
                                                <button id="overseas-gold-button"
                                                        style="display: table;margin: 10px auto 0;" type="submit"
                                                        class="btn btn-lg btn-default" disabled>Apply Now
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="radio-group">
                                                <button id="overseas-fbe-button"
                                                        style="display: table;margin: 10px auto 0;"
                                                        type="submit" class="btn btn-lg btn-default" disabled>Apply Now
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">SELLING B2B through ebeautytrade.com</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FREE</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">GOLD</div>
                                        </th>
                                        <th>
                                            <div class="eyebrow">FBE</div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="List unlimited items under the same Brand, including products, services, parts, accessories, training courses, etc.">
                                                Unlimited Listing per Brand</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                Admin Panel</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Receive enquiries and orders into your Admin Panel and registered E-mails in your Company and Brand(s) Profiles.">
                                                Receive orders straight to your inbox</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                Online Payment Gateway</p>
                                        </td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Share and keep updated your Operations and Sales team with your digital catalog. Ease your procedures and spend more time in upsales, marketing and customer service.">
                                                Multi-users for your Admin Panel</p>
                                        </td>
                                        <td><p></p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="You may use this option to make your Brand available only for potential Buyers who meet with your concept and criteria. The selected Buyers will need your Access Code for online ordering.">
                                                Access Code for authorized Buyers</p>
                                        </td>
                                        <td><p>

                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="See how many times Buyers are clicking on your Brands and Products.">
                                                Clicks Analytics on Brands and Products</p>
                                        </td>
                                        <td><p>

                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Collect contact details and drive more leads of Buyers when they click on your Brand(s), Products, Call Supplier and Send enquiry button(s).">
                                                Collect Buyers' details upon visiting you</p>
                                        </td>
                                        <td><p></p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p data-toggle="tooltip" data-placement="right"
                                               title="Launch any email, printed or social media campaign  and track conversion rate by adding Promocode from your Admin Panel.">
                                                Promocode campaigns</p>
                                        </td>
                                        <td><p></p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                        <td><p>
                                                <i class="fa fa-check"></i>
                                            </p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">REGISTRATION (upon request)</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                Get your Product registered by us</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    </tbody>
                                </table>                                                    
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                        <tr>
                                            <th colspan="2">
                                                <div class="maintext">LOGISTICS SERVICES</div>
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Get Free Temporized Storage up to 700 units or 2 CBM per Brand. Additional storage is available at approx. US$ 40 per CBM /month.">
                                                Temporized Storage</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Get Free Temporized Storage up to 700 units or 2 CBM per Brand. Additional storage is available at approx. US$ 40 per CBM /month.">
                                                Order Processing/Invoicing</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Deliveries will be handled by selected couriers depending on weight and destination. Delivery terms and fees will be managed as per Supplier instructions.">
                                                Coordinating Deliveries/Payments</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="Physical address is mostly needed for orders pick-up from E-store partners, B2B customers and eventual end-users.">
                                                Physical address for orders pick-up</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">SELLING B2C through partners</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p>Listing on e-store partners in UAE</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Listing on e-store partners in GCC</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Listing on e-store partners in India</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable" style="display:none">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th>
                                            <div class="maintext">LOOKING FOR A DISTRIBUTOR</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p>B2B campaign & Meeting Set-up</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                        <td><p><i class="fa fa-check"></i></p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table pricingTable marketing-table">
                                    <thead class="pricingGrid__plansHeader">
                                    <tr>
                                        <th class="">
                                            <div class="maintext">MARKETING SUPPORT INCLUDED</div>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><p data-toggle="tooltip" data-placement="right"
                                               title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                                Welcoming E-mail shot per Brand</p></td>
                                        <td><p>1</p></td>
                                        <td><p>1</p></td>
                                        <td><p>1</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Bespoke E-mail campaign shot per Brand</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>1</p></td>
                                        <td><p>3</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Homepage featuring per Brand</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>1</p></td>
                                        <td><p>3</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>Your logo sponsoring our E-Newsletter</p></td>
                                        <td><p>&nbsp;</p></td>
                                        <td><p>1</p></td>
                                        <td><p>3</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="mobileVersion" style="display:none">
                                <div class="maintext">SELLER PLANS<br/>For Suppliers in overseas</div>
                                <div>
                                    <div id="accordion-2a" class="panel-group">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a href="#panelfreeOverseas" class="accordion-toggle"
                                                       data-toggle="collapse" data-parent="#accordion-2a">FREE</a>
                                                </h4>
                                                <h4 class="panel-title">
                                                    <a href="#panelgoldOverseas" class="accordion-toggle collapsed"
                                                       data-toggle="collapse"
                                                       data-parent="#accordion-2a">GOLD</a>
                                                </h4>
                                                <h4 class="panel-title">
                                                    <a href="#panelfbeOverseas" class="accordion-toggle collapsed"
                                                       data-toggle="collapse"
                                                       data-parent="#accordion-2a">FBE</a>
                                                </h4>
                                            </div>
                                            <div id="panelfreeOverseas" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <h3>FREE Plan</h3>
                                                    <p>
                                                        Selling B2B, basic Online Store<br/>Shipping from
                                                        based Country
                                                    </p>
                                                    <p class="price-mobile">
                                                        PRICING Per Year
                                                        <select name="overseas-free-brand-number"
                                                                id="freebrandnumberoverseas-mobile"
                                                                class="form-control">
                                                            <option value="">No of Brands</option>
                                                            <option value="free1-overseas">1 Brand</option>
                                                            <option value="free2-overseas">2 Brands</option>
                                                            <option value="free3-overseas">3 Brands</option>
                                                        </select>
                                                    </p>
                                                    <div class="radio-group">
                                                        <button id="overseas-free-button-mobile"
                                                                style="display: table;margin: 10px auto 0;"
                                                                type="submit"
                                                                class="btn btn-lg btn-default">
                                                            Apply Now
                                                        </button>
                                                    </div>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">SELLING B2B through eBeautyTrade
                                                                    </div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="You will be able to list unlimited products and services belonging to the same Brand. This may include parts and accessories, supplies, training manuales and courses, etc.">
                                                                    Unlimited Listing per Brand <i class="fa fa-info-circle"></i>
                                                                </p></td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                                    Admin Panel <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You will be receiving the orders to your E-mail registered in the Brand Profile, as well into your Buyers table in the Admin Panel, where all your orders will be listed for view and processing.">
                                                                    Receive orders straight into your inbox <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                                    Online Payment Gateway <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable marketing-table">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">MARKETING SUPPORT INCLUDED</div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                                                    Welcoming E-mail shot per Brand <i class="fa fa-info-circle"></i></p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div id="panelgoldOverseas" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <h3>GOLD Plan</h3>
                                                    <p>
                                                        More Marketing, more Leads<br/>Shipping from based
                                                        Country<br/>Looking for
                                                        Distributor option
                                                    </p>
                                                    <p class="price-mobile">
                                                        PRICING Per Year
                                                        <select name="overseas-gold-brand-number"
                                                                id="goldbrandnumberoverseas-mobile"
                                                                class="form-control">
                                                            <option value="">No of Brands</option>
                                                            <option value="gold1-overseas">1 Brand 948$</option>
                                                            <option value="gold2-overseas">2 Brands 1 548$</option>
                                                            <option value="gold3-overseas">3 Brands 2 748$</option>
                                                        </select>
                                                    </p>
                                                    <div class="radio-group">
                                                        <button id="overseas-gold-button-mobile"
                                                                style="display: table;margin: 10px auto 0;"
                                                                type="submit"
                                                                class="btn btn-lg btn-default">
                                                            Apply Now
                                                        </button>
                                                    </div>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">SELLING B2B through eBeautyTrade</div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="You will be able to list unlimited products and services belonging to the same Brand. This may include parts and accessories, supplies, training manuales and courses, etc.">
                                                                    Unlimited Listing per Brand <i class="fa fa-info-circle"></i>
                                                                </p></td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                                    Admin Panel <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You will be receiving the orders to your E-mail registered in the Brand Profile, as well into your Buyers table in the Admin Panel, where all your orders will be listed for view and processing.">
                                                                    Receive orders straight into your inbox <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                                    Online Payment Gateway <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Share and keep updated your Operations and Sales team with your digital catalog. Ease your procedures and spend more time in upsales, marketing and customer service.">
                                                                    Multi-users for your Admin Panel <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <i class="fa fa-check"></i>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You may use this option to make your Brand available only for potential Buyers who meet with your concept and criteria. The selected Buyers will need your Access Code for online ordering.">
                                                                    Access Code for authorized Buyers <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <i class="fa fa-check"></i>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="See how many times Buyers are clicking on your Brands and Products.">
                                                                    Clicks Analytics <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Collect contact details and drive more leads of Buyers when they click on your Brand(s), Products, Call Supplier and Send enquiry button(s).">
                                                                    Collect Buyers' details upon clicks <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <i class="fa fa-check"></i>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Launch any email, printed or social media campaign  and track conversion rate by adding Promocode from your Admin Panel.">
                                                                    Promocode campaigns <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th class="">
                                                                <div class="maintext">REGISTRATION (upon request)</div>
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                                    Get your Product registered by us</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>                                                    
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">LOGISTICS SERVICES</div>
                                                                </th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                                    Product Registration (upon request) <i class="fa fa-info-circle"></i></p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable" style="display:none">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">LOOKING FOR A DISTRIBUTOR</div>
                                                                </th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><p>B2B campaign & Meeting Set-up</p></td>
                                                                <td><p><i class="fa fa-check"></i></p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable marketing-table">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">MARKETING SUPPORT INCLUDED</div>
                                                                </th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                                                    Welcoming E-mail shot per Brand <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Bespoke E-mail campaign shot per Brand</p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Homepage featuring per Brand</p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Your logo sponsoring our E-Newsletter</p></td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel">
                                            <div id="panelfbeOverseas" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <h3>FBE Plan</h3>
                                                    <p>
                                                        Selling B2B/B2C, incl. Logistics<br/>Fulfilled by
                                                        eBeautyTrade
                                                    </p>
                                                    <p class="price-mobile">
                                                        PRICING Per Year
                                                        <select name="overseas-free-brand-number"
                                                                id="fbebrandnumberoverseas-mobile"
                                                                class="form-control">
                                                            <option value="">No of Brands</option>
                                                            <option value="fbe1-overseas">1 Brand 1 200$</option>
                                                            <option value="fbe2-overseas">2 Brands 2 400$</option>
                                                            <option value="fbe3-overseas">3 Brands 3 600$</option>
                                                        </select>
                                                    </p>
                                                    <p style="width:100%">
                                                        + 10% Commission <span data-toggle="tooltip" data-placement="right" title="Over net sales"><i class="fa fa-info-circle"></i></span>
                                                    </p>
                                                    <div class="radio-group">
                                                        <button id="overseas-fbe-button-mobile"
                                                                style="display: table;margin: 10px auto 0;"
                                                                type="submit" class="btn btn-lg btn-default"
                                                            >Apply Now
                                                        </button>
                                                    </div>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">SELLING B2B through eBeautyTrade
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You will be able to list unlimited products and services belonging to the same Brand. This may include parts and accessories, supplies, training manuales and courses, etc.">
                                                                    Unlimited Listing per Brand <i class="fa fa-info-circle"></i>
                                                                </p></td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Manage your Business with own terms and conditions. Publish and unpublish products at your convenience, keep updated your digital Catalog with prices, stock availability, novelties and offers 24/7.">
                                                                    Admin Panel <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You will be receiving the orders to your E-mail registered in the Brand Profile, as well into your Buyers table in the Admin Panel, where all your orders will be listed for view and processing.">
                                                                    Receive orders straight into your inbox <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Receive online payment and view your  balance in Financials on your Dashboard. Request payment anytime allowing 2 to 3 working days for bank transfer to your registered account.">
                                                                    Online Payment Gateway <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Share and keep updated your Operations and Sales team with your digital catalog. Ease your procedures and spend more time in upsales, marketing and customer service.">
                                                                    Multi-users for your Admin Panel <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <i class="fa fa-check"></i>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="You may use this option to make your Brand available only for potential Buyers who meet with your concept and criteria. The selected Buyers will need your Access Code for online ordering.">
                                                                    Access Code for authorized Buyers <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <i class="fa fa-check"></i>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="See how many times Buyers are clicking on your Brands and Products.">
                                                                    Clicks Analytics <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Collect contact details and drive more leads of Buyers when they click on your Brand(s), Products, Call Supplier and Send enquiry button(s).">
                                                                    Collect Buyers' details upon clicks <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td>
                                                                <p>
                                                                    <i class="fa fa-check"></i>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Launch any email, printed or social media campaign  and track conversion rate by adding Promocode from your Admin Panel.">
                                                                    Promocode campaigns <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>
                                                                    <i class="fa fa-check"></i>
                                                                </p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th class="">
                                                                <div class="maintext">REGISTRATION (upon request)</div>
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                                    Get your Product registered by us</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>                                                    
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">LOGISTICS SERVICES</div>
                                                                </th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p data-toggle="tooltip" data-placement="right"
                                                                   title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                                    Product Registration (upon request) <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Get Free Temporized Storage up to 700 units or 2 CBM per Brand. Additional storage is available at approx. US$ 40 per CBM /month.">
                                                                    Temporized Storage <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Should your product need registration in the UAE, we will apply on your behalf. The process may take from 1 to 3 weeks until product is listed and approved. Application service fee is US$ 10.00 per SKU (Government fees not included).">
                                                                    Order Processing/Invoicing <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Deliveries will be handled by selected couriers depending on weight and destination. Delivery terms and fees will be managed as per Supplier instructions.">
                                                                    Coordinating Deliveries/Payments <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="Physical address is mostly needed for orders pick-up from E-store partners, B2B customers and eventual end-users.">
                                                                    Physical address for orders pick-up <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">SELLING B2C through partners</div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><p>Listing on e-store partners in UAE</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Listing on e-store partners in GCC</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Listing on e-store partners in India</p></td>
                                                            <td><p><i class="fa fa-check"></i></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable" style="display:none">
                                                        <thead class="pricingGrid__plansHeader">
                                                        <tr>
                                                            <th colspan="2">
                                                                <div class="maintext">LOOKING FOR A DISTRIBUTOR</div>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><p>B2B campaign & Meeting Set-up</p></td>
                                                                <td><p><i class="fa fa-check"></i></p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table pricingTable marketing-table">
                                                        <thead class="pricingGrid__plansHeader">
                                                            <tr>
                                                                <th colspan="2">
                                                                    <div class="maintext">MARKETING SUPPORT INCLUDED</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p data-toggle="tooltip" data-placement="right"
                                                                   title="E-Shot to be sent to selected target audience, linked to your B2B Store">
                                                                    Welcoming E-mail shot per Brand <i class="fa fa-info-circle"></i>
                                                                </p>
                                                            </td>
                                                            <td><p>1</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Bespoke E-mail campaign shot per Brand</p></td>
                                                            <td><p>3</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Homepage featuring per Brand</p></td>
                                                            <td><p>3</p></td>
                                                        </tr>
                                                        <tr>
                                                            <td><p>Your logo sponsoring our E-Newsletter</p></td>
                                                            <td><p>3</p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="centered">