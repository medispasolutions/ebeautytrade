<div class="innerContent" >

<?php 
$count_brands=$this->custom_fct->getBrands(array());
$list_brands=$this->custom_fct->getBrands(array('status'=>1),$count_brands,0);

$like=array();
$cond=array();
$categories = $this->fct->getAll_cond('categories','sort_order asc',array('id_parent'=>0));
?>
<?php $guide_h = $this->fct->getAll('guide','sort_order');?>

<?php 
$cond=array();
$cond['mainCategories']=1;?>
<?php $categoriesBrandTraining=$this->custom_fct->getCategoryBrandTraining($cond);?>

<?php $about_spamiles=$this->fct->getAll('about_spamiles','sort_order'); ?>
<?php $customer_service=$this->fct->getAll('customer_service','sort_order'); ?>

<?php echo $this->load->view('blocks/breadcrumbs',$breadcrumbs);?>
<div class="page-title">
<h1><?php echo $section['title'];?></h1>
</div>
<ul class="sitemap">

<li><a title="Home" href="<?php echo site_url();?>">Home</a></li>
<li><a title="<?php echo lang('suppliers_title');?>" href="<?php echo route_to('suppliers');?>"><?php echo lang('suppliers_title');?></a></li>
<li><a title=" About EBeautyTrade" class="no-pointer"> About EBeautyTrade</a>
<ul class="sub-list list-inline">
<?php foreach($about_spamiles as $val){ ?>
<li>
<a title="<?php echo $val['title'];?>" alt="<?php echo $val['title'];?>" href="<?php echo route_to('pages/about_spamiles/'.$val['id_about_spamiles']);?>"><?php echo $val['title'];?></a>
</li>
<?php } ?>

</ul>
</li>


<li><a title="Shop By Brand" class="no-pointer">Shop By Brand</a>
<ul class="sub-list list-inline">
<?php foreach($list_brands as $val){ ?>
<li>
<a title="<?php echo $val['title'];?>" alt="<?php echo $val['title'];?>" href="<?php echo route_to('brands/details/'.$val['id_brands']);?>"><?php echo $val['title'];?></a>
</li>
<?php } ?>

</ul>
</li>



<li><a title="Brand Certified Training" class="no-pointer">Brand Certified Training</a>
<ul class="sub-list list-inline">

<?php foreach($categoriesBrandTraining as $val){ ?>
<li>
<a title="<?php echo $val['title'];?>" alt="<?php echo $val['title'];?>" href="<?php echo route_to('brands/training/'.$val['id_training_categories']);?>"><?php echo $val['title'];?></a>
</li>
<?php } ?>

</ul>
</li>
<li><a title="Categoris" class="no-pointer" >Categoris</a>
<ul class="sub-list list-inline">
<?php foreach($categories as $val){ ?>
<li>
<a title="<?php echo $val['title'];?>" alt="<?php echo $val['title'];?>" href="<?php echo route_to('products/category/'.$val['id_categories']);?>"><?php echo $val['title'];?></a>
</li>
<?php } ?>

</ul>
</li>


<?php
/*
?>
<li><a title="Directory" class="no-pointer" >Directory</a>
<ul class="sub-list list-inline">
<li>
<a title="Events" alt="Events"  href="<?php echo route_to('events');?>">Events</a>
</li>
<?php foreach($guide_h as $val){ ?>
<li>
<a title="<?php echo $val['title'];?>" alt="<?php echo $val['title'];?>" href="<?php echo route_to('contactlist/index/'.$val['id_guide']);?>"><?php echo $val['title'];?></a>
</li>
<?php } ?>

</ul>
</li>
<?php
*/
?>

<li><a title="Customer Service" class="no-pointer">Customer Service</a>
<ul class="sub-list list-inline">
<?php foreach($customer_service as $val){ ?>
<li>
<a title="<?php echo $val['title'];?>" alt="<?php echo $val['title'];?>" href="<?php echo route_to('pages/customer_service/'.$val['title_url']);?>"><?php echo $val['title'];?></a>
</li>
<?php } ?>

</ul>
</li>

<li><a href="<?php echo route_to('contactus');?>">Contact Us</a></li>



</ul>

</div>