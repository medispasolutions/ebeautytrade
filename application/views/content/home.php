<?php $categories_bottom = $this->fct->getAll_limit_cond('categories','sort_order','asc',24,array('id_parent'=>0,'status'=>0,'position'=>'top','section'=>categories_section())); ?>
<div class="innerContent no-padd-btm" style='padding:0px!important'>
    <div class="centered" style='padding:0px!important;width:100%'>
        <div class="menu-categories-bottom">
            <ul>
                <?php foreach ($categories_bottom as $val) {
                    $url_main_category = route_to('products/category/' . $val['id_categories']); ?>
                    <li>
                        <a title="<?php echo $val['title']; ?>" alt="<?php echo $val['title']; ?>"
                           href="<?php echo $url_main_category; ?>" class="inner-menu-categories-btm"
                           style="background-image:url(<?php echo base_url(); ?>uploads/categories/295x295/<?php echo $val['image']; ?>)">
                            <div class="menu-categories-btm-content">
                                <div class="imgTable">
                                    <div class="imgCell">
                                        <?php echo $val['title']; ?>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <?php if (!$login) {
        $middle_banner = $this->fct->getAll('middle_banner', 'sort_order');
        ?>
        <div class="booking-home">
            <div class="centered">
                <h2>For Spas, Salons & Beauty Clinics Only</h2>
                <ul class="booking-home-ul">
                    <?php $i = 0;
                    $k = 0;
                    foreach ($middle_banner as $val) {
                        $i++; ?>
                        <?php if ($i > 1) {
                            $k++; ?>
                            <li class="booking_arrow <?php if ($k % 2 == 0) {
                                echo "bottom";
                            } ?>">

                            </li>
                        <?php } ?>
                        <li class="booking-home-ul-li">
                            <?php if (!empty($val['link'])){ ?>
                            <a href="<?php echo prep_url($val['link']); ?>">
                                <?php } ?>
                                <div class="booking-head">
                                    <div class="imgTable">
                                        <div class="imgCell">
                                            <span class="booking_num"><?php echo $i; ?></span>
                                            <h4>
                                                <?php echo $val['title']; ?>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="booking-home-content">
                                    <?php echo $val['brief']; ?></div>
                                <?php if (!empty($val['link'])){ ?>
                            </a>
                        <?php } ?>
                        </li>
                    <?php } ?>
                </ul>


                <!--<div class="visit-our-showroom">
<h5>Visit us for Testing & Sampling</h5>
<a href="<?php echo route_to('booking'); ?>">
Book Now
</a>
</div>-->
            </div>
        </div>
    <?php } ?>

    <?php /*?><?php if(!empty($ads_steps)){?>
<div class="ads_step" style="display:none;">
<div class="ads_step_slider">
<?php foreach($ads_steps as $val){?>
<div class="item">
<div class="innerItem" style="background-image:url(<?php echo base_url();?>uploads/banner_ads/<?php echo $val['image'];?>);">

</div>
</div>
<?php } ?>
</div>
</div>
<?php } ?><?php */ ?>
    <div class="row feature-brand-products-cont">
        <div class="centered">
            <?php if (!empty($featured_brands)) { ?>
                <div class="featured_brands_home">
                    <div class="title-h2 mod">
                        <h2>Featured Brands</h2>
                    </div>
                    <div class="load-bx">
                        <div class="featured_brands_home_logos arrows_slider_on">
                            <div class="slider_exceed_width">
                                <div class="featured_brands_slick">
                                    <?php $i = 0;
                                    foreach ($featured_brands as $val) {
                                        $i++;
                                        $url_m = route_to('brands/details/' . $val['id_brands']);
                                        $url_m2 = $url_m . '#meetingRequest'; ?>
                                        <?php if (!empty($val['logo'])) { ?>
                                            <div class="item">
                                                <div class="featured_brands_home_logo">
                                                    <div class="inner_featured_brands_home_logo ">
                                                        <?php $brand = $val; ?>
                                                        <div class="supplier">
                                                            <div class="inner_supplier">
                                                                <div class="supplier_blk">

                                                                    <div class="supplier_img">
                                                                        <div class="imgTable">
                                                                            <div class="imgCell">
                                                                                <?php if (!empty($brand['photo'])) { ?>
                                                                                    <img src="<?php echo base_url(); ?>uploads/brands/<?php echo $brand['photo']; ?>"/>  <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <span class="supplier_name"><?php echo $brand['name']; ?></span>
                                                                    <!--<label><?php echo $brand['position']; ?></label>-->

                                                                    <?php /*?> <?php if($login){ ?><label><?php echo $brand['phone'];?></label><?php } ?><?php */ ?>
                                                                    <!--<div class="brand_logo_bx">
                                                                    <?php if (!empty($brand['logo'])) { ?>
                                                                    <a  <?php if (!empty($brand['website_url'])) { ?> target="_blank" href="<?php echo prep_url($brand['website_url']); ?>"<?php } ?> class="brand_logo <?php if (empty($brand['website_url'])) {
                                                                        echo "no-pointer";
                                                                    } ?>"> <img src="<?php echo base_url(); ?>uploads/brands/<?php echo $brand['logo']; ?>" /></a>
                                                                    <?php } ?></div>-->

                                                                    <?php if ($login) {
                                                                        $user = $this->ecommerce_model->getUserInfo();
                                                                        $rolesUser = $user['id_roles'];
                                                                        $userID = $user['id_user'];
                                                                        $productUserID = $product['brand']['id_user'];
                                                                    }?>
                                                                    <?php if (($rolesUser == '5' && $userID == $productUserID) || $login) { ?>
                                                                    <div class="row">
                                                                        <a class="btn supplier_phone"
                                                                           <?php if ($login){ ?>onclick="showPhone(this,<?php echo $brand['id_brands']; ?>)"
                                                                           <?php }else{ ?>onclick="checkUrlIfLogin('<?php echo site_url(''); ?>')" <?php } ?>><span class="call_supplier"><i class="fa fa-phone" aria-hidden="true"></i> Call Supplier</span>
                                                                            <?php echo $brand['phone']; ?>
                                                                        </a>
                                                                        <a class="btn"
                                                                           <?php if ($login){ ?>href="<?php echo $url_m2; ?>"
                                                                           <?php }else{ ?>onclick="checkUrlIfLogin('<?php echo site_url(''); ?>')" <?php } ?>>
                                                                            <i class="fa fa-envelope"
                                                                               aria-hidden="true"></i>
                                                                            <?php if (!empty($brand['button_text'])) { ?>
                                                                                <?php echo $brand['button_text']; ?>
                                                                            <?php } else { ?>
                                                                                <!-- Meeting Request-->
                                                                                Send Enquiry
                                                                            <?php } ?>
                                                                        </a>
                                                                    </div>
                                                                    <?php } else { ?>
                                                                        <div class="row">
                                                                            <a class="btn supplier_phone"
                                                                           <?php if ($login){ ?>onclick="showPhone(this,<?php echo $brand['id_brands']; ?>)"
                                                                           <?php }else{ ?>onclick="checkUrlIfLogin('<?php echo site_url(''); ?>')" <?php } ?>>
                                                                            <span class="call_supplier">
                                                                            <i class="fa fa-phone" aria-hidden="true"></i> Call Supplier
                                                                            </span>
                                                                            <?php echo $brand['phone']; ?>
                                                                            </a>
                                                                            <a class="btn"
                                                                               <?php if ($login){ ?>href="<?php echo $url_m2; ?>"
                                                                               <?php }else{ ?>onclick="checkUrlIfLogin('<?php echo site_url(''); ?>')" <?php } ?>>
                                                                                <i class="fa fa-envelope"
                                                                                   aria-hidden="true"></i>
                                                                                <?php if (!empty($brand['button_text'])) { ?>
                                                                                    <?php echo $brand['button_text']; ?>
                                                                                <?php } else { ?>
                                                                                    <!-- Meeting Request-->
                                                                                    Send Enquiry
                                                                                <?php } ?>
                                                                            </a>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <span class="featured_brand_logo_bg"></span>
                                                        <a title="<?php echo $val['title']; ?>"
                                                           alt="<?php echo $val['title']; ?>" class="brand-logo"
                                                           href="<?php echo $url_m; ?>">
                                                            <div class="imgTable">
                                                                <div class="imgCell">
                                                                    <img src="<?php echo base_url(); ?>uploads/brands/112x122/<?php echo $val['logo']; ?>"/>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>


                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if (!empty($featured)) { ?>
                <div class="products_home">
                    <div class="title-h2 mod">
                        <h2>Featured Products</h2>
                    </div>
                    <div class="load-bx">
                        <div id="products_section" class="products-category-cont products-container arrows_slider_on">
                            <div class="slider_exceed_width">
                                <div class="home_slider">

                                    <?php $j = 0;
                                    foreach ($featured as $product) {
                                        $j++;
                                        ?>
                                        <div class="item">
                                            <?php
                                            $data['product'] = $product;
                                            $data['page'] = 'home';
                                            $data['j'] = $j++;
                                            $data['login'] = $login;
                                            $this->load->view('blocks/product', $data); ?>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>
            
            <?php if (!empty($special_products)) { ?>
            <div class="products_home_oed">
                <div class="title-h2 mod">
                    <h2>Online Exclusive Deals</h2>
                    <p>Valid for first-time Buyers only - limited quantity</p>
                </div>
                <div class="load-bx">
                    <style>
                        .carousel {width: 100%;margin: 0px auto;}
                        .slick-slide {margin: 10px;}
                        .slick-slide img {width: 100%;border: 2px solid #fff;}
                    </style>
                    <div class="carousel slide">
                        <?php $j = 0;
                        foreach ($special_products as $product) {
                            $j++;
                            ?>
                            <div class="item">
                                <?php
                                $data['product'] = $product;
                                $data['page'] = 'home';
                                $data['j'] = $j++;
                                $data['login'] = $login;
                                $this->load->view('blocks/product_oed', $data); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            
            <?php if (!empty($blogs) && !empty($featuredblogs)){ ?>
                <div class="blogs_home" id='blog_sec' style="display:block">
                    <div class="title-h2 mod">
                        <h2 style="padding:20px 0">News And Tips</h2>
                    </div>
                    <div class="load-bx">
                        <div class="blogs">
                            <div class="col-md-6 one-blogs">
                            <div id="myCarousel" class="carousel news" data-ride="carousel">
                                    <!-- Wrapper for slides -->

                            <?php $j = 0;
                            foreach ($blogs as $val) {
                                $j++;
                                $link = $val['link'];
                                ?>
                                <div class="item">
                                    <div class="blogs_img">
                                        <a href="<?php echo $link; ?>" class="imgTable" target="_blank">
                                            <div class="imgCell">
                                                <img src="<?php echo $val['image']; ?>"
                                                     alt="<?php echo $val['title']; ?>"
                                                     title="<?php echo $val['title']; ?>"/>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="blogs_title">
                                        <a href="<?php echo $link; ?>" target="_blank">
                                            <p>
                                                <?php echo $val['formatted_date']; ?>
                                            <p class="news-category">
                                                <?php echo $val['category']; ?>
                                            </p>
                                            </p>
                                            <div class="news-title">
                                                <?php echo $val['title']; ?>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            
                            </div>
                            </div>
                            
                            <div class="col-md-6 four-blogs">
                            <?php $j = 0;
                            foreach ($featuredblogs as $val) {
                                $j++;
                                $link = $val['link'];
                                ?>
                                <div class="col-md-6">
                                <div class="blogs_img">
                                    <a href="<?php echo $link; ?>" class="imgTable" target="_blank">
                                        <div class="imgCell">
                                            <img src="<?php echo $val['image']; ?>"
                                                 alt="<?php echo $val['title']; ?>"
                                                 title="<?php echo $val['title']; ?>"/>
                                        </div>
                                    </a>
                                </div>
                                    <div class="blogs_title">
                                        <a href="<?php echo $link; ?>" target="_blank">
                                            <p>
                                                <?php echo $val['formatted_date']; ?>
                                            <p class="news-category">
                                                <?php echo $val['category']; ?>
                                            </p>
                                            </p>
                                            <div class="news-title">
                                                <?php echo $val['title']; ?>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            <?php /* if (!empty($blogs)){ ?>
            <div class="products_home" id='blog_sec'>
                <div class="title-h2 mod">
                    <h2>Blog</h2>
                </div>
                <div class="load-bx">
                    <div class="blogs">

                        <?php $j = 0;
                        foreach ($blogs as $val) {
                            $j++;
                            $link = $val['link'];
                            ?>
                            <div class="blogs_item">
                                <div class="blogs_img">
                                    <a href="<?php echo $link; ?>" class="imgTable">
                                        <div class="imgCell">
                                            <img src="<?php echo $val['image']; ?>" alt="<?php echo $val['title']; ?>"
                                                 title="<?php echo $val['title']; ?>"/>
                                        </div>
                                    </a>
                                </div>
                                <div class="blogs_title">
                                    <a href="<?php echo $link; ?>">
                                        <?php echo $val['title']; ?>
                                    </a>
                                </div>
                                <div class="blog_readmore"><a href="<?php echo $link; ?>">Read More</a></div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <?php } ?>
            </div>
            <?php /*?><?php if(!empty($special_products)){?>
<div class="products_home">
    <div class="title-h2 mod">
      <h2>Special Products</h2>
    </div>
    <div id="products_section" class="products-category-cont products-container arrows_slider_on">
    <div class="slider_exceed_width">
    <div class="home_slider">

<?php $j=0; foreach($special_products as $product){	$j++;
	?>
    <div class="item">
      <?php
	  $data['product']=$product;
	  $data['page']='home';
	  $data['j']=$j++;
	  $data['login']=$login;
	  $this->load->view('blocks/product',$data);?>
    </div>
      <?php } ?>

</div>
</div>
 </div>

  </div>
<?php } ?><?php */ ?>
        </div>
    </div>
</div>