<div class="innerContent">
  <div class="page-title border">
    <h1>Events</h1>
    <div class="sort_order">
      <label>Sort by</label>
      <select class="textbox selectbox"  onchange="setLocation(this.value)" >
       <!-- <option value="<?php echo $new_url.'&sort=';?>"  >Select</option>-->
        <option value="<?php echo $new_url.'&sort=date';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="date") echo "selected";?> >Date</option>
        <option value="<?php echo $new_url.'&sort=name';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="name") echo "selected";?> >Name</option>
        <option value="<?php echo $new_url.'&sort=country';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="country") echo "selected";?> >Country</option>
      </select>
    </div>
  </div>
  
<?php /*?>  <?php if(isset($events)){ ?>
  <?php 
$data['ads_pages_top']=$advertisements;
$this->load->view('blocks/ads_pages_top',$data);?>
  <?php } ?><?php */?>
  
    <?php if(isset($home_bottom_ads_left) || isset($home_bottom_ads_right)){ ?>
  <?php 
$data['home_bottom_ads_left']=$home_bottom_ads_left;
$data['home_bottom_ads_right']=$home_bottom_ads_right;
/*$this->load->view('blocks/home-ads-bottom',$data);*/?>
  <?php } ?>

  <div class="figures events_list">
    <?php if(isset($list_as_country)){
	
	$countries=$events;

	 foreach($countries as $country){
	
		
		$events_c=$country['events'];
			?>
    <?php if(!empty($events_c)){ 
		
	
		?>
    <div class="row">
    <h3><?php echo $country['title'];?></h3>
    <?php  foreach($events_c as $event){
	?>
    <div class="figure <?php if($event['set_as_featured']==1) echo "featured_figure"; ?>">
      <div class="figure_content">
        <div class="figureDetails">
          <div class="row">
       
            <div class="figure_title"><a target="_blank" <?php if(!empty($event['website'])){?>href="<?php echo prep_url($event['website']);?>"<?php }else{ ?>class="n-link"<?php } ?>><?php echo $event['title'];?></a></div>
            <label>
      <?php /*?>        <?php $date=$event['date'];?>
              <?php if($date!="" && $date!="0000-00-00"){?>
              <span><?php echo  date("F d,Y", strtotime($date)); ?></span>
              <?php } ?><?php */?>
               <?php if(!empty( $event['date_label'])){?>
                <span><?php echo  $event['date_label'];?></span><?php } ?></a>
            </label>
            <div class="description"> <?php echo $event['description'];?></div>
            <?php if(!empty($event['website'])){?>
            <a class="figure_wesbite"  target="_blank" href="<?php echo prep_url($event['website']);?>"><?php echo $event['website'];?></a>
            <?php } ?>
          </div>
        </div>
        <?php if($event['set_as_featured']==1 && !empty($event['image'])){?>
        <div class="figure_img"> <span> <a  target="_blank" <?php if(!empty($event['website'])){?>href="<?php echo prep_url($event['website']);?>"<?php }else{ ?>class="n-link"<?php } ?>><img title="<?php echo $event['title'];?>" alt="<?php echo $event['title'];?>" src="<?php echo base_url();?>uploads/events/300x240/<?php echo $event['image'];?>" /></a> </span> </div>
        <?php } ?>
      </div>
    </div>
    </div>
    <?php }}}}else{ ?>
    <?php foreach($events as $event){
?>
    <div class="figure <?php if($event['set_as_featured']==1) echo "featured_figure"; ?>">
      <div class="figure_content">
        <div class="figureDetails">
          <div class="row">
            <div class="figure_title"><a target="_blank" <?php if(!empty($event['website'])){?>href="<?php echo prep_url($event['website']);?>"<?php }else{ ?>class="n-link"<?php } ?>><?php echo $event['title'];?></a></div>
            <label>
              <?php $countries=$event['countries'];?>
              <?php $k=0;$countries_txt=""; 
			if(!empty($countries)){
			
				foreach($countries as $country){$k++;
				
			$com=",";
			if($k==1){$com="";}
			$countries_txt .=$com.$country['title'];
			}}?>
              <?php echo $countries_txt;?> </label>
            <!--<label>
              <?php $date=$event['date'];?>
              <?php if($date!="" && $date!="0000-00-00"){?>
              <span><?php echo  date("F d,Y", strtotime($date)); ?></span></a>
              <?php } ?>
            </label>-->
            <label>
            <?php if(!empty($event['date_label'])){?>
                <span><?php echo  $event['date_label'];?></span><?php } ?></label>
            <div class="description"> <?php echo $event['description'];?></div>
            <?php if(!empty($event['website'])){?>
            <a class="figure_wesbite"  target="_blank" href="<?php echo prep_url($event['website']);?>"><?php echo $event['website'];?></a>
            <?php } ?>
            
          </div>
        </div>
        <?php if($event['set_as_featured']==1 && !empty($event['image'])){?>
        <div class="figure_img"> <span> <a  target="_blank" <?php if(!empty($event['website'])){?>href="<?php echo prep_url($event['website']);?>"<?php }else{ ?>class="n-link"<?php } ?>><img title="<?php echo $event['title'];?>" alt="<?php echo $event['title'];?>" src="<?php echo base_url();?>uploads/events/300x240/<?php echo $event['image'];?>" /></a> </span> </div>
        <?php } ?>
      </div>
    </div>
    <?php }	} ?>
  </div>
  <div class="pagination-row"><?php echo $this->pagination->create_links(); ?> </div>
</div>
