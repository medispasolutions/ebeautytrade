<div class="innerContent">
<div class="products-section">
    <div class="page-title border">
      <h1><?php echo $title;?></h1>
      <div class="sort_order">
        <label>Sort by</label>
        <?php $new_url=$current_url;?>
        <select class="textbox selectbox"  onchange="setLocation(this.value)" >
 <option value="<?php echo $new_url.'&sort=';?>" >Select</option>       

<option value="<?php echo $new_url.'&sort=name';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="name") echo "selected";?> >Name</option>
<?php if($page!="miles"){?>
<option value="<?php echo $new_url.'&sort=category';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="category") echo "selected";?> > Category</option>
<?php } ?>
<?php if($login){?>
<option value="<?php echo $new_url.'&sort=descending';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="descending") echo "selected";?> > Price</option>
<?php } ?>
<!--<option value="<?php echo $new_url.'&sort=ascending';?>"  <?php if(isset($_GET['sort']) && $_GET['sort']=="ascending") echo "selected";?> > Price: ascending  </option>-->
</select>
      </div>
    </div>
    <div id="products_section" class="products-category-cont products-container">
    <?php  if(!empty($products)){ 
$data['products']=$products;
$data['page']=$page;
$this->load->view('blocks/products',$data);
 }else{?>
<div class="empty">No products found.</div>
      <?php } ?>
      

      
    </div>
  </div></div>
  
  <div class="pagination-row"><?php echo $this->pagination->create_links(); ?></div>