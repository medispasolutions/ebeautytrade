<div class="innerContent" >
<div class="page-title border">
<h1><?php echo $section['title'];?></h1>
</div>
<?php if(!empty($section['description']) || !empty($section['image'])){?>

<div class="description">
<?php if(!empty($section['image'])){?>
<div class="img_table">
<img src="<?php echo base_url();?>uploads/about_spamiles/<?php echo $section['image'];?> " />
</div>
<?php } ?>
<?php if(!empty($section['description'])){?>
<?php echo $section['description'];?>
<?php } ?>
</div>
<?php }else{ ?>
<div class="empty">Coming Soon...</div>
<?php } ?>
</div>