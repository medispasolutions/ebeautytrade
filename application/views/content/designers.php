<div class="innerContent" >
  <div class="centered">
    <?php $this->load->view('blocks/breadcrumbs',$breadcrumbs);?>
    <div class="page-title category-title">
      <h1>Designers</h1>
    </div>
    <div class="glossary_head">
      <ul>
        <li class="go-to">GO TO:</li>
        <?php foreach($designers as $designer) {?>
        <li> <a id="letter_<?php echo $designer['letter'];?>" href="#<?php echo $designer['letter'];?>"><?php echo $designer['letter'];?></a> </li>
        <?php } ?>

      </ul>
    </div>
    
    <div class="glossary_body">
   
<div class="glossary_section">
  <?php 
$counter=count($designers);
$j=0;$n=6;  foreach($designers as $designer) { 
if(!empty($designer['designers'])){$j++;
$letter_designers=$designer['designers'];
	?>
<div class="letter_block" id="letter_block_<?php echo $designer['letter'];?>">
<a name="<?php echo $designer['letter'];?>"></a>
<span class="letter_title"><?php echo $designer['letter'];?></span>
<ul>
<?php foreach($letter_designers as $val){?>
<li>
<a class="designer_link"  title="<?php echo $val['title'];?>" href="<?php echo route_to('products?designer='.$val['id_designers']);?>"><?php echo $val['title'];?></a>
</li>
<?php }?>

</ul>
</div>
<?php }else{
	}

if($j%$n==0 && $j!=$counter ){
	/*if($n==5){
		$n=6;}else{
			$n=5;}*/?>
	</div>
<div class="glossary_section">
	<?php }} ?>
</div>
</div>
  </div>
</div>
