<div class="innerContent">
  <?php 
$data['breadcrumbs']=$breadcrumbs;
$this->load->view('blocks/breadcrumbs',$data);?>
<div  class="page-title border">
<h1><?php if(isset($training)){echo $training['title'];}else{echo "Brand Certified Training";}?></h1>
</div>
<?php foreach($products as $product){
	 /*if(!empty($product['training_image']) &&  !empty($product['training_title']) ){*/
	?>
<div class="product-grid">
<?php if(isset($product['training_image']) && !empty($product['training_image'])){?>
<div class="product-grid-img">
<div class="inner-product-grid-img">
 <?php if(!empty($product['datasheet'])){?>
 <a  target="_blank" download href="<?php echo base_url();?>uploads/products/<?php echo $product['datasheet'];?>">
 <?php } ?>
<img title="<?php echo $product['training_title'];?>" alt="<?php echo $product['training_title'];?>" src="<?php echo base_url();?>uploads/brand_certified_training/300x228/<?php echo $product['training_image'];?>" />
 <?php if(!empty($product['datasheet'])){?>
 </a><?php } ?>
</div>
</div>
<?php } ?>
<div class="product-grid-content">
<div class="row">
<div class="product-grid-header ">
 <?php if(!empty($product['duration'])){?>
<span class="date"><?php echo $product['duration'];?></span>
<?php } ?>
<div class="title">

<?php echo $product['training_title'];?>
</div>
 </div>
<div class="product-grid-description description">
<?php echo $product['training_description'];?></div>
</div></div>
<div class="product-grid-related">
<div class="inner-product-grid-related">
<div class="product-grid-related-img">
   <a  title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>"  href="<?php echo route_to('products/details/'.$product['title_url']);?>">
          <?php if(!empty($product['gallery'][0]['image'])){?>
            <img    src="<?php echo base_url();?>uploads/products/gallery/295x295/<?php echo $product['gallery'][0]['image'];?>" />  	 <?php }else{ ?>
				<img    src="<?php echo base_url();?>front/img/default_product.png" /> 
			<?php	}?>
   </a>
<span class="product-grid-related-brand">
<img  src="<?php echo base_url();?>uploads/brands/<?php echo $product['brands_image'];?>" />
</span>
</div>
<div class="title-related-product"><a title="<?php echo $product['title'];?>" alt="<?php echo $product['title'];?>"  href="<?php echo route_to('products/details/'.$product['id_products']);?>"><?php echo $product['title'];?></a></div>

</div>
</div>
</div>
<?php /*}*/} ?>

<div class="pagination-row"><?php echo $this->pagination->create_links(); ?>  </div>
</div>