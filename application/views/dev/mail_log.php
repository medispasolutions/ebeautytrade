<h1 class="d-flex align-content-center justify-content-between">
    <span>Mail log</span>
    <a class="btn btn-danger" href="/dev/maillog/clear" role="button">Clear log</a>
</h1>
<?php
if (empty($log)):
?>
<div class="alert alert-info mt-5" role="alert">
    No emails in log - be sure to put <code>MAILER_MOCKING = 1</code> in your .env file
</div>
<?php
else:
?>
<div id="accordion" role="tablist" aria-multiselectable="true" class="mt-5">
    <?php
    $counter = 0;
    foreach ($log as $i => $mail):
        $counter++;
    ?>
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <p style="cursor: pointer" class="mb-1 d-flex align-content-center justify-content-between" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $i ?>" aria-expanded="true" aria-controls="collapseOne">
                <span>
                    #<?= $counter ?> (<?= $mail['timeago'] ?>)
                </span>
                <span>
                    to: <?= $mail['to'] ?>
                </span>
                <span>
                    from: <?= $mail['from'].($mail['from_name'] ? " ({$mail['from_name']})" : "") ?>
                </span>
            </p>
            <p style="cursor: pointer" class="mb-0" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?= $i ?>" aria-expanded="true" aria-controls="collapseOne">
                <strong><?= $mail['subject'] ?></strong>
            </p>
        </div>
        <div id="collapse-<?= $i ?>" class="collapse<?= $counter == 1 ? ' show' : '' ?>" role="tabpanel" aria-labelledby="headingOne">
            <iframe style="width:100%;height:600px;border:0;" src="/dev/maillog/getMailContent/<?= $i ?>"></iframe>
        </div>
    </div>
    <?php
    endforeach;
    ?>
</div>
<?php
endif;
?>