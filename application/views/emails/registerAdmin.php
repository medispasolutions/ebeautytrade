
<div style="background:#9ce1fc;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td valign="top" align="center" style="padding:20px 0 20px 0">
<table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #e0e0e0;color:#404040;font-size:14px">
<tbody>
<tr>
<td valign="top">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>

</tbody>
</table>
</td>
</tr>
<tr style="text-align:center;" ><td><a target="_blank" href="<?php echo site_url();?>"><img style="width:150px;" src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>
<tr>
<td valign="top">
<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Dear <?php echo $admindata['website_title'];?></h1>
New user registered on <?php  echo $user['created_date']; ?>
<?php $role=$this->fct->getonerecord('roles',array('id_roles'=>$user['id_roles']));?>

<p><strong>Role : </strong><?php echo lang($role['title']); ?></p>
<?php if(!empty($user['trading_name'])){?>
<p><strong>Trade Name : </strong><?php echo $user['trading_name'] ;?></p>
<?php } ?>
<?php if(!empty($user['trading_licence'])){?>
<p><strong><a download href="<?php echo base_url();?>uploads/user/<?php echo $user['trading_licence']; ?>">Download Trading License</a></strong></p>
<?php } ?>

<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">Personal Information</p>
<?php if(!empty($user['salutation'])){?>
<p><strong>Salutation : </strong><?php echo $user['salutation'] ;?></p>
<?php } ?>
<p><strong>First Name : </strong><?php echo $user['first_name'] ;?></p>
<p><strong>Last Name : </strong><?php echo $user['last_name'] ;?></p>
<p><strong>E-mail : </strong><?php echo $user['email'] ;?></p>

<?php if(!empty($user['position'])){?>
<p><strong>Position : </strong><?php echo $user['position'] ;?></p>
<?php } ?>

<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">Address</p>

<?php if(!empty($user['id_countries'])){?>
<?php $country=$this->fct->getonerecord('countries',array('id_countries'=>$user['id_countries'])); ?>
<p><strong>Country : </strong><?php echo $country['title'] ;?></p>
<?php } ?>


<?php if(!empty($user['city'])){?>
<p><strong>City : </strong><?php echo $user['city'] ;?></p>
<?php } ?>

<?php if(!empty($user['address_1'])){?>
<p><strong>Address 1 : </strong><?php echo $user['address_1'] ;?></p>
<?php } ?>
<?php if(!empty($user['address_2'])){?>
<p><strong>Address 2 : </strong><?php echo $user['address_2'] ;?></p>
<?php } ?>

<?php if(!empty($user['phone'])){?>
<p><strong>Phone : </strong><?php echo $user['phone'] ;?></p>
<?php } ?>
<?php if(!empty($user['fax'])){?>
<p><strong>Fax : </strong><?php echo $user['fax'] ;?></p>
<?php } ?>
<?php if(!empty($user['mobile'])){?>
<p><strong>Mobile : </strong><?php echo $user['mobile'] ;?></p>
<?php } ?>




<?php if(!empty($user['business_type'])){?>
<p style="border:1px dashed #9ce1fc;"></p>
<?php $business_type=$this->fct->getonerecord('business_type',array('id_business_type'=>$user['business_type'])); ?>
<p><strong>Business Type : </strong><?php echo $business_type['title'] ;?></p>
<?php } ?>
<?php if(!empty($user['hear_about_us'])){?>
<?php $hear_about_us=$this->fct->getonerecord('how_did_you_hear_about_us',array('id_how_did_you_hear_about_us'=>$user['hear_about_us'])); ?>
<p><strong>Hear About Us : </strong><?php echo $hear_about_us['title'] ;?></p>
<?php } ?>

<?php $selected_categories = $this->users_categories_m->select_user_categories($user['id_user']);?>
<?php if(!empty($selected_categories)){?>
<?php $i=0;
foreach($selected_categories as $val) {  $i++;
 $category=$this->fct->getonerecord('categories',array('id_categories'=>$val));
if($i==1){$txt=$category['title'];}else{$txt .=",".$category['title'];}
}?>
<p><strong>Product/Service Categories: </strong><?php echo $txt ;?></p>
<?php } ?>




<?php if(!empty($user['comments'])){?>
<p style="border:1px dashed #9ce1fc;"></p>
<p><strong>Comments : </strong><?php echo $user['comments'] ;?></p>
<?php } ?>



<p></p>
<p></p>
</td>
</tr>
<tr>
<td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
<center>
<p style="font-size:11px;margin:0">
 <?php  $link = site_url('back_office/users/edit/'.$user['id_user']); ?>  
	<p><a style="background:#9ce1fc; padding: 6px 12px;color:white; text-decoration:none;" href="<?php echo $link?>">Click To View</a></p>
</p>
</center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</div>
