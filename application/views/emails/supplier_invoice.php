<?php $color="#D9FFFF";?>


<table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
      <td valign="top" align="center">
      <table width="600" cellspacing="0" cellpadding="10" border="0" bgcolor="#FFFFFF" style="border:1px solid #e0e0e0">
          <tbody>
            <tr>
              <td valign="top" style="text-align:center;padding: 0;"><a target="_blank" href="<?php echo site_url();?>" style="display:inline-block;"> <img  border="0" style="margin-bottom:10px;width:150px;" alt="Spamiles" src="<?php echo base_url();?>front/img/logo.png"> </a></td>
            </tr>
            <tr>
              <td valign="top" style="padding-left: 0 20px;"><h1 style="padding:1px 0;margin:2px 0;">Invoice#<?php echo $orderData['id_invoices'];?><?php  if(!isset($print)) echo '-'.$orderData["status"]; ?></h1>
                <p style=" clear: both;
    color: #ee001c;
    margin: 10px 0;">Order Date: <?php echo  date("F d,Y g:i a", strtotime($orderData['created_date'])); ?></p></td>
            </tr>
            
   		    <tr><td style="padding:3px 9px; font-size:14px;"><table>
        		   <tr>
                      <td  ><b>Total Amount</b> </td>
                      <td  >: <span><?php echo $orderData['total_amount'].' '.$orderData['currency']; ?></span></td>
                  </tr>
                  
                  <tr>
                   <td><b>Total Amount credits</b> </td>
                   <td>: <span><?php echo $orderData['total_amount_credits'].' '.$orderData['currency']; ?></span></td>
                  </tr>
                  <tr>
                      <td ><b>Shpping Charge</b> </td>
                      <td >: <span><?php echo $orderData['shipping_charge'].' '.$orderData['currency']; ?></span></td>
                  </tr>
                   <tr>
                      <td > <b>Net Price</b> </td>
                      <td >: <span><?php echo $orderData['net_price'].' '.$orderData['currency']; ?></span></td>
                  </tr>
                   </table></td></tr>
                   
            <tr>
               <td>
<table cellspacing="0" cellpadding="0" width="300" border="0" style="border:1px solid #eaeaea;">
<?php 
$brands = $info['brands'];
foreach($brands as $brand) {
$line_items=$brand['line_items'];  ?>

					 <tr><td colspan="11" style="text-align:center;"><h3><?php echo $brand['title'];?></h3></td></tr>

					 <tr>
                      <!--<th bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Order ID</th>-->
                      <th bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Product</th>
                      <th bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px">SKU</th>
                      <th bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px"><?php echo lang('barcode');?></th>
                          <th bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px"><?php echo lang('shelf_location');?></th>
                     
                      <th bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Date</th>
                      <th bgcolor="<?php echo $color;?>" style="font-size:13px;padding:3px 9px;text-align:center">Price(<?php echo $orderData['currency']; ?>)</th>
                      <th bgcolor="<?php echo $color;?>"  style="font-size:13px;padding:3px 9px;text-align:center;"><?php echo lang('quantity');?></th>
                      <th bgcolor="<?php echo $color;?>"  style="font-size:13px;padding:3px 9px;text-align:center"><?php echo lang('return_quantity');?></th>
                  <!--    <th bgcolor="<?php echo $color;?>" style="font-size:13px;padding:3px 9px;text-align:center"><?php echo lang('paid_m'); ?></th>
                      <th bgcolor="<?php echo $color;?>"  style="font-size:13px;padding:3px 9px;text-align:center"><?php echo lang('consignment_m'); ?></th>-->
                   	 <th bgcolor="<?php echo $color;?>"  style="font-size:13px;padding:3px 9px;text-align:center"><?php echo lang('credit_cart');?>(<?php echo $orderData['currency']; ?>)</th>
                    <th bgcolor="<?php echo $color;?>"  style="font-size:13px;padding:3px 9px;text-align:center"><?php echo lang('total_price');?>(<?php echo $orderData['currency']; ?>)</th>
                    </tr>
                   
                    
                    
<?php if(!empty($line_items)) {?>
<?php 
$net_price = 0;
$net_discount_price = 0;
foreach($line_items as $pro) {
	

	$options=unSerializeStock($pro['options']);

?>
               
                    <tr>
                      <!--<td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">#<?php echo $pro['id_orders']; ?></td>-->
                      <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><strong style="font-size:11px"><?php echo $pro['product']['title'];?></strong>
                  
        <?php if(!empty($options)) { 
		$j=0;
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="";}else{$com=", ";}?>
                          <?php echo $com.$opt; ?>
                          <?php }}?>
                       </td>
                      <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['sku']; ?></td>
                      
                       <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['product']['barcode']; ?></td>
                       
                        <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['product']['location_code']; ?></td>
                      <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo  date("F d,Y", strtotime($pro['orders_created_date'])); ?></td>
                      <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['price'].' '.$orderData['currency']; ?></td>
                      
                      <td valign="top" align="center" style="text-align:center;font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['quantity']; ?></td>
                      <td valign="top" align="center" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;text-align:center"><?php echo $pro['return_quantity']; ?></td>
                     <!-- <td valign="top" align="right" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;text-align:center"><?php echo $pro['qty_sold_by_admin']; ?></td>
                   
                  	 <td valign="top" align="right" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;text-align:center"><?php echo $pro['qty_payable']; ?></td>-->
                  	 <td valign="top" align="right" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;text-align:center"><?php echo $pro['credit_cart'].' '.$orderData['currency']; ?></td>
                   	<td valign="top" align="right" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc;text-align:center"><?php echo $pro['total_price'].' '.$orderData['currency']; ?></td>
                    </tr>
            <?php }} ?>
                  
  <?php } ?> 
           
                  
                </table>
               </td>
            </tr>
            <tr>
              <td bgcolor="<?php echo $color;?>" align="center" style="background:<?php echo $color;?>;text-align:center"><center>
                  <p style="font-size:12px;margin:0"> Thank you, <strong><a style="color:black; text-decoration:none;" target="_blank" href="<?php echo site_url();?>">Spamiles</a></strong> </p>
                </center></td>
            </tr>
          </tbody>
        </table>
      </td>
   </tr>
  </tbody>
</table>

