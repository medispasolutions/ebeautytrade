<?php $login=checkUserIfLogin();?>
<table border="0" cellpadding="0" cellspacing="5" width="100%" style="border:none;">
  <?php $color="#D9FFFF";?>
  <?php $website_info=$this->fct->getonerow('settings',array('id_settings'=>1)); ?>
  <!-- //////////////////////HEAD/////////////////////-->
  <?php if(!isset($print)){?>
  <?php /*?><tr>
                <td valign="top" style="text-align:center;padding: 0;">
                <table  width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:12px;"><tr><td style="text-align:center;"><a target="_blank" href="<?php echo site_url();?>" style="display:inline-block;"> <img  border="0" style="margin-bottom:10px;width:150px;" alt="ebeautytrade" src="<?php echo base_url();?>front/img/logo.png"> </a></td>
                <!--<td style="text-align:right;">Control No. <span style="color:#ff0000;">2018</span></td>--></tr></table>
          
                
                </td>
              </tr><?php */?>
  <?php } ?>
  
  <tr>
                <td valign="top" style="text-align:center;padding: 0;">
                <table  width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:16px;"><tr><td style="text-align:left;"><a target="_blank" href="<?php echo site_url();?>" style="display:inline-block;"> <img  border="0" style="margin-bottom:10px;width:150px;" alt="ebeautytrade" src="<?php echo base_url();?>front/img/logo.png"> </a></td>
                <td style="text-align:right; font-weight:600; padding-bottom:10px;"><?php echo $website_info['address'];?></td></tr></table>
          </td>
              </tr>
  <!-- //////////////////////TITLE/////////////////////-->
  <tr>
    <td valign="top" style="text-align:center;padding: 0;"><h1 style="font-size:16px; font-weight:600;margin:2px 0;">Quotation No. <?php echo $orderData['id_quotation'];?></h1></td>
  </tr>
  
  <!-- //////////////////////ADDRESS//////////////////-->
  <tr>
    <td style="padding-top:5px;padding-bottom:10px;"><table width="100%" cellspacing="0" cellpadding="0" border="0" >
      </table></td>
  </tr>
  
  <!--//////////////////////ITEMS//////////////////-->
  <?php $total_units=0;?>
  <tr>
    <td><table width="100%" cellspacing="0" cellpadding="0" border="0" style="vertical-align:middle;">
        <?php if(empty($orderData['quotation_line_items']) && !empty($orderData['quotation_line_items_redeem_by_miles'])){?>
        <tr>
          <?php $style="border:1px solid black;border-left:none;padding:5px 1px;font-size:16px;vertical-align:middle;text-align:center";?>
          <th <?php echo 'style="'.$style.';border-left:1px solid black;"';?> width="5%" >#</th>
          <th  <?php echo 'style="'.$style.'"';?> width="10%">REF</th>
          <th  <?php echo 'style="'.$style.'"';?> width="12%">BARCODE</th>
          <th <?php echo 'style="'.$style.'"';?> width="40%">ITEM DESCRIPTION</th>
          <?php if($login){?>
          <th <?php echo 'style="'.$style.'"';?> width="10%"> PRICE <?php echo $orderData['currency'];?></th>
          <?php } ?>
          <th <?php echo 'style="'.$style.'"';?> width="8%">QTY</th>
          <?php if($login){?>
          <th <?php echo 'style="'.$style.'"';?> width="15%">SUB-TOTAL <br />
            <?php echo $orderData['currency'];?> </th>
            <?php } ?>
        </tr>
        <?php } ?>
        <?php if(!empty($orderData['quotation_line_items'])){?>
        <tr>
          <?php $style="border:1px solid black;border-left:none;padding:5px 1px;font-size:16px;vertical-align:middle;text-align:center";?>
          <th <?php echo 'style="'.$style.';border-left:1px solid black;"';?> width="5%" >#</th>
          <th  <?php echo 'style="'.$style.'"';?> width="10%">REF</th>
          <th  <?php echo 'style="'.$style.'"';?> width="12%">BARCODE</th>
          <th <?php echo 'style="'.$style.'"';?> width="40%">ITEM DESCRIPTION</th>
          <?php if($login){?>
          <th <?php echo 'style="'.$style.'"';?> width="10%"> PRICE <?php echo $orderData['currency'];?></th>
          <?php } ?>
          <th <?php echo 'style="'.$style.'"';?> width="8%">QTY</th>
          <?php if($login){?>
          <th <?php echo 'style="'.$style.'"';?> width="15%">SUB-TOTAL <br />
            <?php echo $orderData['currency'];?> </th>
            <?php } ?>
        </tr>
        <?php } ?>
        <?php $style="padding:1px 3px;;text-align:center;font-size:16px;";?>
        <?php $total_quantity=0;?>
        <?php $quotation_line_items=$orderData['quotation_line_items'];?>
        <?php $i=0;if(!empty($quotation_line_items)){?>
        <?php foreach($quotation_line_items as $pro){
		$total_quantity=$total_quantity+$pro['quantity'];
			 ?>
        <?php  $options=unSerializeStock($pro['options_en']);$i++; ?>
        <tr>
          <td <?php echo 'style="'.$style.';text-align:center;"';?>><?php echo $i;?></td>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['product']['sku']; ?></td>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['product']['barcode']; ?></td>
          <td <?php echo 'style="'.$style.'text-align:left;"';?>><?php echo $pro['product']['title'];?>
            <?php if(!empty($options)) { 
		$j=0;
		
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="<br>";}else{$com=", ";}?>
            <?php echo $com.$opt; ?>
            <?php }}?></td>
            <?php if($login){?>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['price_currency']; ?></td>
          <?php } ?>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['quantity']; ?></td>
          <?php if($login){?>
          <td <?php echo 'style="'.$style.'"';?>><?php  echo $pro['total_price_currency'].' '.$pro['currency'];?></td>
          <?php } ?>
        </tr>
        <?php } ?>
        <?php }?>
        
        <!--////////////////Redeemed By Miles///////////////-->
        <?php $quotation_line_items=$orderData['quotation_line_items_redeem_by_miles'];?>
        <?php $i=0;if(!empty($quotation_line_items)){?>
        <?php $style="padding:1px 1px;;text-align:center;";?>
        <?php if(!empty($orderData['quotation_line_items'])){?>
        <tr>
          <?php $style="padding:3px 1px;font-weight:bold;text-align:center;font-size:16px;";?>
          <td <?php echo 'style="'.$style.'"';?> >#</td>
          <td  <?php echo 'style="'.$style.'"';?> >REF</td>
          <td  <?php echo 'style="'.$style.'"';?> >BARCODE</td>
          <td <?php echo 'style="'.$style.'"';?> >ITEM(S) REDEEMED BY MILES</td>
          <?php if($login){?>
          <td <?php echo 'style="'.$style.'"';?>>MILES</td>
          <?php } ?>
          <td <?php echo 'style="'.$style.'"';?> >QTY</td>
          <?php if($login){?>
          <td <?php echo 'style="'.$style.'"';?>>SUB-TOTAL</td>
          <?php } ?>
        </tr>
        <?php } ?>
        <?php $style="padding:3px 1px;text-align:center;font-size:4px;";?>
        <?php foreach($quotation_line_items as $pro){
		$total_quantity=$total_quantity+$pro['quantity'];
			 ?>
        <?php  $options=unSerializeStock($pro['options_en']);$i++; ?>
        <tr>
          <td <?php echo 'style="'.$style.'"';?> style="text-align:center;"><?php echo $i;?></td>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['product']['sku']; ?></td>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['product']['barcode']; ?></td>
          <td <?php echo 'style="'.$style.'text-align:left;"';?>><?php echo $pro['product']['title'];?>
            <?php if(!empty($options)) { 
		$j=0;
		
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="<br>";}else{$com=", ";}?>
            <?php echo $com.$opt; ?>
            <?php }}?></td>
             <?php if($login){?>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['redeem_miles']/$pro['quantity']; ?></td>
          <?php } ?>
          <td <?php echo 'style="'.$style.'"';?>><?php echo $pro['quantity']; ?></td>
           <?php if($login){?>
          <td <?php echo 'style="'.$style.'"';?>><?php  echo $pro['redeem_miles'];?></td>
          <?php } ?>
        </tr>
        <?php } ?>
        <?php }?> 
		<?php /*?>
        <tr>
         <td  <?php if($login){?>colspan="5"<?php }else{ ?> colspan="4" <?php } ?> style="text-align:center"></td>  
  </tr><?php */?>
  </table></td></tr>
  <!--//////////////////////COMMENTS//////////////////-->
  <tr>
    <td >
     <?php if($login){?>
    <table width="100%">
        <tr>
          <td style="padding-left:0; width:70%"></td>
          <!--/////////TOTAL///////////-->
          <?php $style_1=" text-align:right;font-size:16px;";$style_2=" border-bottom:1px solid #d4d4d4;font-size:16px;";?>
          <td  style="padding-top:17px;padding-right:0;font-size:11px; width:30%" ><table   width="100%" cellpadding="2" cellspacing="2" style="text-align:right;font-size:11px;">
              <tr>
                <td  <?php echo 'style="'.$style_1.'"';?>>Sub-Total</td>
                <td <?php echo 'style="'.$style_2.'"';?>><?php echo $orderData['sub_total'].' '.$orderData['currency']; ?></td>
              </tr>
              <tr>
                <td <?php echo 'style="'.$style_1.'"';?>>Discount</td>
                <td <?php echo 'style="'.$style_2.'"';?> ><?php echo $orderData['discount'].' '.$orderData['currency']; ?></td>
              </tr>
              <?php if($orderData['redeem_discount_miles_amount']>0){ ?>
              <tr>
                <td <?php echo 'style="'.$style_1.'"';?>>Redeem Miles</td>
                <td <?php echo 'style="'.$style_2.'"';?> ><?php echo $orderData['redeem_miles'].' Miles'; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td <?php echo 'style="'.$style_1.'"';?>><b>TOTAL AED</b></td>
                <td <?php echo 'style="'.$style_2.'"';?>  ><b><?php echo $orderData['amount_currency'].' '.$orderData['currency']; ?></b></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td style="text-align:right;font-size:12px; text-transform:capitalize;" colspan="2"><?php echo convert_number_to_words($orderData['amount_currency']);?> <?php echo $orderData['currency'];?></td>
        </tr>
        <tr>
          <td style="text-align:right; font-size:12px;"  colspan="2"></td>
        </tr>
      </table>
      <?php } ?>
      </td>
  </tr>
  
  <!--//////////////////////Company Brief//////////////////-->
  <tr>
    <td  style="text-align:center;font-size:12px; padding-top:10px; font-weight:600;">Medispa Solutions General Trading LLC | Office 1202 Regal Tower Business Bay Dubai UAE <br />
      POBox 487874 | +971 4 277 5920 / 25 | info@ebeautytrade.com</td>
  </tr>
  <!--///////////////////////////////////////////////-->
</table>
