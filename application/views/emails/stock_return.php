<?php $color="#D9FFFF";?>

<table width="100%" cellspacing="0" cellpadding="0" border="0"  style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
  <tbody>
    <tr>
      <td valign="top" align="center" style="padding:20px 0 20px 0"><table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="#FFFFFF" style="border:1px solid #e0e0e0">

          <tbody>
            <tr>
              <td valign="top" style="text-align:center;padding: 0;"><a target="_blank" href="<?php echo site_url();?>" style="display:inline-block;"> <img  border="0" style="margin-bottom:10px;width:150px;" alt="Spamiles" src="<?php echo base_url();?>front/img/logo.png"> </a></td>
            </tr>
            <tr>
              <td valign="top" style="padding-left: 0 20px;padding-bottom:0;"><h1 style="padding:1px 0;margin:2px 0;">
           
              <?php  if($orderData["id_role"] == 5) { $order_name="D-ORDER#"; }else{ $order_name="D-RQ#";} ?>
              <?php if($orderData["return"] == 1) { $order_name="R-ORDER#";} ?>
              <?php echo $order_name;?> <?php echo $orderData['id_delivery_order'];?>
              <?php if($orderData["return"] != 1) { ?>
              -<span style="color:rgb(37, 176, 229);"><?php if($orderData['type'] == '2') echo lang('consignment_m'); ?>
					 <?php if($orderData['type'] == '1') echo lang('paid_m'); ?>
                     <?php } ?></span></h1>
                <p style=" clear: both;
    color: #ee001c;
    margin: 10px 0;">Order Date: <?php echo  date("F d,Y g:i a", strtotime($orderData['created_date'])); ?></p></td>
            </tr>
           <tr><td style="padding-top:0;">
            <table><tr> <td>Customer Name</td><td> : </td><td><?php echo $orderData['user']['name'];?></td></tr>
            <tr>
              <td>Status</td><td> : </td>
<td>
<? if($orderData["status"] == '2') {?>
<?php echo lang('pending'); ?></span>
<?php } ?>
<?php  if($orderData["status"] == '1') {?>
<?php echo lang('approved'); ?></span>
<?php }?>
<?php  if($orderData["status"] == '3') {?>
<?php echo lang('canceled'); ?></span>
<?php }?>
<?php  if($orderData["status"] == '4') {?>
<?php echo lang('in_progress'); ?>
<?php }?></td>
            </tr>
            <?php if(isset($hide_price)){}else{ ?>
            <? if($orderData["type"] == '1') {?>
            <tr> <td>Type</td><td> : </td><td><?php echo lang('paid_m');?></td></tr>
            <?php }?>
            <? if($orderData["type"] == '2') {?>
            <tr> <td>Type</td><td> : </td><td><?php echo lang('consignment_m');?></td></tr>
            <?php }?> <?php }?>
            
            </table></td>
             
            
<?php $brands_line_items=$orderData['line_items'];?>

<?php if(!empty($brands_line_items)) {?>
<?php foreach($brands_line_items as $brand) { ?>
            <tr> <td style="text-align:center;color:#333; padding:0;"><h2><?php echo $brand['title'];?></h2></td></tr>
            <tr>
              <td style="padding-top:0;">
          
             <table <?php if(isset($hide_price)){  echo 'width="75%"  align="center"' ; }else{ ?> width="650"  <?php } ?> cellspacing="0" cellpadding="0" border="0" style="border:1px solid #eaeaea">
                  <thead>
                    <tr>
                    <th width="10%" bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Order ID</th>
                      <th width="30%" bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Product Name</th>
                      <th width="20%"bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px">Sku</th>
                    
<th width="10%" bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px">Remaining Quantity</th>
<th width="10%" bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px">Return Quantity</th>
                     
                  	</tr>
                  </thead>
                  <?php 
$line_items=$orderData['line_items'];
if(!empty($line_items)) {?>
                  <?php 


foreach($line_items as $pro) {

  
		$remain_quantity=$pro['quantity']-$pro['return_quantity']-$pro['sold_quantity'];
		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];	
		$options=unSerializeStock($pro['stock']['combination']); 
			 }else{
		$sku=$pro['product']['sku'];
		$options="";}
		  ?>

<?php if($pro['delivery_order']["id_role"] == 5) { $order_name="D-ORDER#"; }else{ $order_name="D-RQ#";}   ?>
                  <tbody bgcolor="#F6F6F6">
                    <tr>
            <td  valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $order_name.$pro['id_orders'];?></td>
  <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['product']['title'];?>
        <br />    <?php
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
                      </td>
                      <td valign="top" align="center" style="text-align:center;font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $sku;?></td>
              
        <td valign="top" align="center" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc">    <?php echo $pro['quantity'];?></td>
               <td valign="top" align="center" style="text-align:center;font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"> <?php echo $remain_quantity;?></td>
               
               <td valign="top" align="center" style="text-align:center;font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php  echo  $pro['return_quantity_stock']; ?></td>
                      

                    </tr>
                  </tbody>
                  <?php }} ?>

                </table>
              
               </td>
            </tr>
            
              <?php } } ?>
            <tr>
              <td bgcolor="<?php echo $color;?>" align="center" style="background:<?php echo $color;?>;text-align:center"><center>
                  <p style="font-size:12px;margin:0"> Thank you, <strong><a style="color:black; text-decoration:none;" target="_blank" href="<?php echo site_url();?>">Spamiles</a></strong> </p>
                </center></td>
            </tr>
          </tbody>
      </table>
        </td>
    </tr>
  </tbody>
</table>

