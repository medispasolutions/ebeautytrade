
<div style="background:#9ce1fc;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td valign="top" align="center" style="padding:20px 0 20px 0">
<table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #e0e0e0;color:#404040;font-size:14px">
<tbody>
<tr>
<td valign="top">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>

</tbody>
</table>
</td>
</tr>
<tr style="text-align:center;" ><td><a target="_blank" href="<?php echo site_url();?>"><img style="width:150px;" src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>
<tr>
<?php $name=$user_credits['first_name'].' '.$user_credits['last_name'];?>
<td valign="top">
<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Dear <?php echo $name;?>,</h1>
New Petty Cash have been added to  your account on <?php  echo date("F d,Y g:i a", strtotime($user_credits['created_date'])) ; ?>.


<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">Details</p>
<p style="font-size:18px;margin: 18px 0;padding: 2px 0;color:#25b0e5;">
<strong>Amount : </strong><?php echo $user_credits['amount'] ;?> <?php echo $user_credits['currency'] ;?>
</p>
<p><strong>First Name : </strong><?php echo $user_credits['first_name'] ;?></p>
<p><strong>Last Name : </strong><?php echo $user_credits['last_name'] ;?></p>
<p><strong>E-mail : </strong><?php echo $user_credits['email'] ;?></p>
<p><strong>Street One : </strong><?php echo $user_credits['street_one'] ;?></p>
<?php if($user_credits['street_two']){ ?>
<p><strong>Street Two: </strong><?php echo $user_credits['street_two'] ;?></p>
<?php } ?>
<p><strong>City: </strong><?php echo $user_credits['city'] ;?></p>
<p><strong>State: </strong><?php echo $user_credits['state'] ;?></p>
<?php if($user_credits['postal_code']){ ?>
<p><strong>Zip/Postal Code: </strong><?php echo $user_credits['postal_code'] ;?></p>
<?php } ?>
<p><strong>Country: </strong><?php echo $user_credits['country']['title'] ;?></p>
<?php if($user_credits['telephone']){ ?>
<p><strong>Telephone : </strong><?php echo $user_credits['telephone'] ;?></p>
<?php } ?>
<?php if($user_credits['fax']){ ?>
<p><strong>fax : </strong><?php echo $user_credits['fax'] ;?></p>
<?php } ?>

<p></p>
<p></p>
</td>
</tr>
<tr>
<td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
<center>
<p style="font-size:12px;margin:0">
Thank you ,
<strong><a style="color:black;" target="_blank" href="<?php echo site_url();?>"><?php echo $admindata['website_title'];?></a></strong>
</p>
</center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</div>
