
<div style="background:#9ce1fc;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td valign="top" align="center" style="padding:20px 0 20px 0">
<table width="700" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #e0e0e0">
<tbody>
<tr>
<td valign="top">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>

</tbody>
</table>
</td>
</tr>
<tr style="text-align:center;" ><td><a target="_blank" href="<?php echo site_url();?>"><img style="width:150px;" src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>
<tr>
<td valign="top">
<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Dear <?php echo $user['first_name'];?> <?php echo $user['surname'];?></h1>

	<p>A request to reset the password for your account has been made at <?php echo $adminname ;?>.</p>
    <p>You may now log in by clicking this link or copying and pasting it to your browser:<br /><a href="<?php echo $reset_link ;?>">
	<?php echo $reset_link ;?></a></p>
       <p>This link can only be used once to log in and will lead you to a page where you can set your password. It expires after five days and nothing will happen if it\'s not used.</p>



<p></p>
<p></p>
</td>
</tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</div>
