
<div style="background:#9ce1fc;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td valign="top" align="center" style="padding:20px 0 20px 0">
<table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #e0e0e0;color:#404040;font-size:14px">
<tbody>
<tr>
<td valign="top">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>

</tbody>
</table>
</td>
</tr>
<tr>
<td style="text-align: center;">
<?php $data['admindata']=$admindata; ?>
<?php $this->load->view('emails/social_media',$data); ?>
</td></tr>
<tr style="text-align:center;" ><td><a target="_blank" href="<?php echo site_url();?>"><img style="width:150px;" src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>

<tr>
<td valign="top">
<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Dear <?php echo $admindata['website'];?>,</h1>
<p style="font-size:12px;line-height:16px;margin:0 0 16px 0">
The customer:  <?php echo $user['name'];?> - <?php echo $this->fct->getonecell('roles','title',array('id_roles'=>$user['id_roles']));?><br /> completed his profile.
<br />
<a href="<?php echo prep_url($link);?>"><?php echo $link;?></a>
<p></p>
</td>
</tr>
<tr>
<td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
<center>
<p style="font-size:11px;margin:0">
<?php  $link = site_url('back_office/users/edit/'.$user['id_user']); ?>  
<p><a style="background:#9ce1fc; padding: 6px 12px;color:white; text-decoration:none;" href="<?php echo $link?>">Click To View</a></p>
</p>
</center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</div>
