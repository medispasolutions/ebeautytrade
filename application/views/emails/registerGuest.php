
<div style="background:#9ce1fc;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td valign="top" align="center" style="padding:20px 0 20px 0">
<table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #e0e0e0;color:#404040;font-size:14px">
<tbody>
<tr>
<td valign="top">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>

</tbody>
</table>
</td>
</tr>
<tr><td style="text-align: center;">
<?php  if(!empty($admindata['facebook'])) {?>
<a style="text-decoration:none;" target="_blank" href="<?php echo prep_url($admindata['facebook']);?>">
<img  src="<?php echo base_url();?>front/img/icons/facebook.png">
</a>
<?php } ?>
<?php  if(!empty($admindata['youtube'])) {?>
<a style="text-decoration:none;" target="_blank" href="<?php echo prep_url($admindata['youtube']);?>">
<img  src="<?php echo base_url();?>front/img/icons/youtube.png">
</a>
<?php } ?>
<?php  if(!empty($admindata['instagram'])) {?>
<a style="text-decoration:none;" target="_blank" href="<?php echo prep_url($admindata['instagram']);?>">
<img  src="<?php echo base_url();?>front/img/icons/instagram.png">
</a>
<?php } ?>

<?php  if(!empty($admindata['twitter'])) {?>
<a style="text-decoration:none;" target="_blank" href="<?php echo prep_url($admindata['twitter']);?>">
<img  src="<?php echo base_url();?>front/img/icons/twitter.png">
</a>
<?php } ?>
<?php  if(!empty($admindata['google_plus'])) {?>
<a style="text-decoration:none;" target="_blank" href="<?php echo prep_url($admindata['google_plus']);?>">
<img  src="<?php echo base_url();?>front/img/icons/google-plus.png">
</a>
<?php } ?>
</td></tr>
<tr style="text-align:center;" ><td><a target="_blank" href="<?php echo site_url();?>"><img style="width:150px;" src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>

<tr>
<td valign="top">
<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Dear <?php echo $user['name'];?>,</h1>
<p style="font-size:12px;line-height:16px;margin:0 0 16px 0">

<p>
A site administrator at <?php echo $admindata['website_title'];?> has created an account for you. You may now log in by clicking this link or copying and pasting it to your browser:
</p>
<?php echo route_to('user/setPassword/'.$user['id_user'].'/'.$user['correlation_id']);?>
<p>
This link can only be used once to log in and will lead you to a page where you can set your password.
</p>
<p>After setting your password, you will be able to log in at <?php echo route_to('user/login');?> in the future using:
</p><p>
email: <?php echo $user['email'];?><br />

password: Your password</p>
<p></p><p></p>
<?php echo $admindata['website_title'];?> team
<p></p>
</td>
</tr>
<tr>
<td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
<center>
<p style="font-size:12px;margin:0">
Thank you ,
<strong><a style="color:black;" target="_blank" href="<?php echo site_url();?>"><?php echo $admindata['website_title'];?></a></strong>
</p>
</center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</div>
