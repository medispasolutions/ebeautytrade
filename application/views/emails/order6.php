<?php $color="#D9FFFF";?>

<div style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
      <td valign="top" align="center" style="padding:20px 0 20px 0"><table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="#FFFFFF" style="border:1px solid #e0e0e0">
          <tbody>
            <tr>
              <td valign="top" style="text-align:center;padding: 0;"><a target="_blank" href="<?php echo site_url();?>" style="display:inline-block;"> <img  border="0" style="margin-bottom:10px;width:150px;" alt="Spamiles" src="<?php echo base_url();?>front/img/logo.png"> </a></td>
            </tr>
            <tr>
              <td valign="top" style="padding-left: 0 20px;"><h1 style="padding:1px 0;margin:2px 0;">Order#<?php echo $orderData['id_orders'];?></h1>
                <p style=" clear: both;
    color: #ee001c;
    margin: 10px 0;">Order Date: <?php echo  date("F d,Y g:i a", strtotime($orderData['created_date'])); ?></p></td>
            </tr>
            <tr>
            <tr>
            <tr>
              <td><table width="650" cellspacing="0" cellpadding="0" border="0">
                  <thead>
                    <tr>
                      <th width="325" bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Billing Information:</th>
                      <th width="10"></th>
                      <th width="325" bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Payment Method:</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php $billing=$orderData['billing'];?>
                      <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea"><?php echo $billing['name'];?> <br>
                      <?php echo $orderData['user']['email'];?> <br>
                        <?php if(!empty($billing['company']) && $billing['company']!=0){?>
                        <?php echo $billing['company'];?> <br>
                        <?php }?>
                        <?php echo $billing['street_one'];?> <br>
                        <?php if(!empty($billing['street_two']) && $billing['street_two']!=0){?>
                        <?php echo $billing['street_two'];?> <br>
                        <?php }?>
                        <?php echo $billing['city'] ?>,<?php echo $billing['state'] ?>
                        <?php if(!empty($billing['postal_code']) && $billing['postal_code']!=0){?>
                        ,<?php echo $billing['postal_code'] ?>
                        <?php }?>
                        <br>
                        <?php echo $billing['country']['title'] ?> <br>
                        T: <?php echo $billing['phone'] ?> 
                        <?php if(!empty($billing['fax']) && $billing['fax']!=0){?>
                       <br> F: <?php echo $billing['fax'] ?>
                        <?php } ?>
                         <?php if(!empty($billing['mobile']) && $billing['mobile']!=0){?>
                        <br> M: <?php echo $billing['mobile'] ?>
                        <?php } ?>
                        </td>
                      <td></td>
                      <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea"><?php echo $this->ecommerce_model->getPaymentMethod($orderData['payment_method']);?></td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <table width="650" cellspacing="0" cellpadding="0" border="0">
                  <thead>
                    <tr>
                      <th width="325" bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Shipping Information:</th>
                      <th width="10"></th>
                      <th width="325" bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:5px 9px 6px 9px;line-height:1em">Shipping Charges:</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php $shipping=$orderData['delivery'];?>
                      <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea"><?php if(!empty($shipping['branch_name'])){?>
                        <?php echo $shipping['branch_name'];?> <br>
                        <?php }?>
                        <?php echo $shipping['name'];?> <br>
                        <?php if(isset($shipping['company']) && $shipping['company']!=0) {?>
                        <?php echo $shipping['company'];?> <br>
                        <?php } ?>
                        <?php echo $shipping['street_one'];?> <br>
                        <?php if(!empty($shipping['street_two']) && $shipping['street_two']!=0){?>
                        <?php echo $shipping['street_two'];?> <br>
                        <?php }?>
                        <?php echo $shipping['city'] ?>,<?php echo $shipping['state'] ?>
                        <?php if(isset($shipping['postal_code']) && $shipping['postal_code']!=0) {?>
                        , <?php echo $shipping['postal_code'] ?>
                        <?php } ?>
                        <br>
                        <?php echo $shipping['country']['title'] ?> <br>
                        T: <?php echo $shipping['phone'] ?> 
                        <?php if(!empty($shipping['fax']) && $shipping['fax']!=0){?>
                       <br> F: <?php echo $shipping['fax'] ?>
                        <?php } ?>
                        <?php if(!empty($shipping['mobile']) && $shipping['mobile']!=0){?>
                        <br> M: <?php echo $shipping['mobile'] ?>
                        <?php } ?>
                        </td>
                      <td></td>
                      <td valign="top" style="font-size:12px;padding:7px 9px 9px 9px;border-left:1px solid #eaeaea;border-bottom:1px solid #eaeaea;border-right:1px solid #eaeaea"><?php if($orderData['shipping_charge_currency']>0){
					echo $orderData['shipping_charge_currency'].' '.$orderData['currency'];
					}else{ echo "Free Shipping " ;}?></td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <table width="650" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #eaeaea;border-collapse: collapse; border-spacing: 0;">
                  <thead>
                    <tr>
                      <th bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px; width:125px;">Item</th>
                      <th bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Sku</th>
                      <th bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Barcode</th>
                      <th bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px">Redeem Miles</th>
                      <th bgcolor="<?php echo $color;?>" align="left" style="font-size:13px;padding:3px 9px">Price</th>
                      <th bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px">Qty</th>
                      <th bgcolor="<?php echo $color;?>" align="center" style="font-size:13px;padding:3px 9px; text-align:center;">Subtotal</th>
                    </tr>
                  </thead>
                  <?php 
$line_items = $orderData['line_items'];
if(!empty($line_items)) {?>
                  <?php 


$net_price = 0;
$net_discount_price = 0;
foreach($line_items as $pro) {
	$options=unSerializeStock($pro['options_en']);

?>
                  <tbody bgcolor="#F6F6F6">
                    <tr>
                      <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><strong style="font-size:11px"><?php echo $pro['product']['title'];?></strong>
                        <dl class="item-options">
                          <?php if(!empty($options)) { 

		$j=0;
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="";}else{$com=", ";}?>
                          <?php echo $com.$opt; ?>
                          <?php }}?>
                        </dl></td>
                      <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['product']['sku']; ?></td>
                       <td valign="top" align="left" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['product']['barcode']; ?></td>
                      <td valign="top" align="center" style="text-align:center;font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php if($pro['redeem']==1) {echo "<b><i>Redeem</i></b>";}else{ echo "-" ;} ?></td>
                      <td valign="top" align="center" style="text-align:left;font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['price_currency'].' '.$pro['currency']; ?></td>
                      <td valign="top" align="center" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php echo $pro['quantity']; ?></td>
                      <td valign="top" align="center" style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc"><?php  echo $pro['total_price_currency'].' '.$pro['currency'];?></td>
                    </tr>
                  </tbody>
                  <?php }} ?>
                  <tbody>
                  </tbody>
                  <tbody>
                    <tr>
                      <td align="right" style="padding:3px 9px" colspan="6"> Subtotal: </td>
                      <td align="left" style="padding:3px 9px" colspan="2"><span><?php echo $orderData['sub_total'].' '.$orderData['currency']; ?></span></td>
                    </tr>
                    <!--<tr>
<td align="right" style="padding:3px 9px" colspan="3"> Shipping & Handling </td>
<td align="right" style="padding:3px 9px">
<span>AED0.00</span>
</td>
</tr>-->
                    <?php if($orderData['discount']>0){?>
                    <tr>
                      <td align="right" style="padding:3px 9px" colspan="6"> Discount: </td>
                      <td align="left" style="padding:3px 9px" colspan="2"><span><?php echo $orderData['discount'].' '.$orderData['currency']; ?></span></td>
                    </tr>
                    <?php } ?>
                    <?php if($orderData['redeem_discount_miles_amount']>0){ ?>
                    <tr>
                      <td align="right" style="padding:3px 9px" colspan="6">Redeem Miles   : </td>
                      <td align="left" style="padding:3px 9px" colspan="2"><?php echo $orderData['redeem_miles'].' Miles'; ?></td>
                    </tr>
                    <?php } ?>
                    <?php if(isset($orderData['shipping_charge_currency']) && $orderData['shipping_charge_currency']>0){?>
                    <tr>
                      <td align="right" style="padding:3px 9px" colspan="6"> Shipping Charge: </td>
                      <td align="left" style="padding:3px 9px" colspan="2"><span><?php echo $orderData['shipping_charge_currency'].' '.$orderData['currency']; ?></span></td>
                    </tr>
                    <?php } ?>
                    <tr>
                      <td align="right" style="padding:3px 9px" colspan="6"><strong>Net Price:</strong></td>
                      <td align="left" style="padding:3px 9px" colspan="2"><strong> <span><?php echo $orderData['amount_currency'].' '.$orderData['currency']; ?></span> </strong></td>
                    </tr>
                  </tbody>
                </table>
                <p style="font-size:12px;margin:0 0 10px 0"></p></td>
            </tr>
            <tr>
              <td bgcolor="<?php echo $color;?>" align="center" style="background:<?php echo $color;?>;text-align:center"><center>
                  <p style="font-size:12px;margin:0"> Thank you, <strong><a style="color:black; text-decoration:none;" target="_blank" href="<?php echo site_url();?>">Spamiles</a></strong> </p>
                </center></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</div>
