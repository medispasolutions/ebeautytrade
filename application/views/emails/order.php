<?php $color = "#D9FFFF"; ?>
<?php $website_info = $this->fct->getonerow('settings', array('id_settings' => 1));
$supplier = array();
if (!empty($orderData['id_supplier']) && $orderData['id_supplier'] != 0)
    $supplier = $this->ecommerce_model->getUserInfo($orderData['id_supplier']);
?>
<div style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
    <table width="100%" cellspacing="0" cellpadding="0" border="0">

        <!-- //////////////////////HEAD/////////////////////-->
        <?php if (!isset($print)) { ?>
            <tr>
                <td valign="top" style="text-align:center;padding: 0;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-size:12px;">
                        <tr>
                            <td style="text-align:left;">
                                <a target="_blank" href="<?php echo site_url(); ?>" style="display:inline-block;"> 
                                    <img border="0" style="margin-bottom:10px;width:150px;" alt="ebeautytrade" src="<?php echo base_url(); ?>front/img/logo.png">
                                </a>
                            </td>
                            <td style="text-align:right; font-weight:600; padding-bottom:10px;"><?php echo $website_info['address']; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php } ?>
        <?php if (isset($id_auto_reply) && $id_auto_reply == 1 && ($orderData['status'] == "completed" || $orderData['status'] == "paid")) { ?>
            <!-- //////////////////////Terms + status/////////////////////-->
            <tr>
                <td valign="top" style="text-align:left;padding: 0;">
                    <?php if (empty($supplier)) {
                        $supplier['trading_name'] = "Spmiles";
                    } ?>
                    Your order no. <?php echo $orderData['id_orders']; ?> has been confirmed
                    by <?php echo $supplier['trading_name']; ?>
                    <?php $terms = getTerms($orderData); ?>

                    <?php if (empty($terms)) {
                        echo ".";
                    } ?>
                    <?php if (!empty($terms)) { ?>
                        under the followin terms.
                        <table width="100%" cellspacing="0" cellpadding="0" border="0"
                               style="font-size:12px;color:#000000; padding-top:10px;">
                            <?php foreach ($terms as $val) { ?>
                                <tr>
                                    <td>• <?php echo $val; ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>

        <!-- //////////////////////TITLE/////////////////////-->
        <tr>
            <td valign="top" style="text-align:center;padding: 0; border-bottom:1px solid #002060;">
                <h1 style="font-size:15px; font-weight:600;margin:2px 0;">Sales Order
                    No. <?php echo $orderData['id_orders']; ?></h1></td>
        </tr>

        <tr>
            <td style="padding-top:5px;padding-bottom:10px;">

                <table width="50%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <?php $padding = '1px 2px'; ?>

                        <td style="width:50%; vertical-align:top; padding-left:10px;">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                               style="font-size:12px;">
                                            <tr>
                                                <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>Customer
                                                        Name: </b></td>
                                                <td colspan="3"><?php echo $orderData['user']['name']; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>E-mail: </b>
                                                </td>
                                                <td colspan="3"><?php echo $orderData['user']['email']; ?></td>
                                            </tr>
                                            <tr>
                                                <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>Phone : </b>
                                                </td>
                                                <td colspan="3"><?php echo $orderData['user']['phone']; ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

        <!-- //////////////////////ADDRESS//////////////////-->
        <?php if (isset($orderData['billing']['branch_name'])) { ?>
            <tr>
                <td style="padding-top:5px;padding-bottom:10px;">

                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <?php $padding = '1px 2px'; ?>
                            <!--//BILLING ADDRESS-->
                            <td style="width:50%; vertical-align:top; padding-right:10px;">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                   style="font-size:12px;">
                                                <tr>
                                                    <th colspan="4"
                                                        style="padding:<?php echo $padding; ?>;text-align:left; background:white; padding-bottom:5px;">
                                                        Order: <?php echo date("F d,Y g:i A", strtotime($orderData['created_date'])); ?>
                                                        <br/></th>
                                                </tr>
                                                <?php $billing = $orderData['billing']; ?>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;"><b>Billing
                                                            address: </b></td>
                                                    <td colspan="3"><?php /*?><?php echo $orderData['user']['email'];?><?php */ ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Trade Name:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $orderData['user']['email']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Trade Name:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $orderData['user']['trading_name']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Contact Name:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $orderData['user']['name']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Address 1:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $billing['street_one']; ?>
                                                    </td>
                                                </tr>
                                                <?php if (!empty($billing['street_two']) && $billing['street_two'] != 0) { ?>
                                                    <tr>
                                                        <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                                <!--Address 2:--> </b><!--</td><td colspan="3">--></td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td colspan="4">
                                                        <table width="100%" border="0" cellpadding="0"
                                                               style="text-align:left; font-size:12px;">
                                                            <tr>
                                                                <td style="padding:<?php echo $padding; ?>;width:15%">
                                                                    <b>City: </b></td>
                                                                <td style="width:30%;"><?php echo $billing['city'] ?></td>
                                                                <td style="padding-left:3px;width:15%"><b>Country:</b>
                                                                </td>
                                                                <td style="width:40%;"> <?php echo $billing['country']['title'] ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td colspan="4">
                                                        <table width="100%" border="0" cellpadding="0"
                                                               style="text-align:left; font-size:12px;">
                                                            <tr>
                                                                <td style="padding:<?php echo $padding; ?>; width:15%">
                                                                    <b>Phone: </b></td>
                                                                <td style="width:30%;"><?php echo cleanPhone($billing['phone']); ?></td>


                                                                <td style="padding-left:3px; width:15%;">  <?php if (!empty($billing['mobile']) && $billing['mobile'] != 0) { ?>
                                                                        <b>Mobile:</b> <?php } ?></td>
                                                                <td style="width:30%;"><?php if (!empty($billing['mobile']) && $billing['mobile'] != 0) { ?><?php echo cleanPhone($billing['mobile']); ?><?php } ?></td>

                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <!--//DELIVERY ADDRESS-->
                            <td style="width:50%; vertical-align:top; padding-left:10px;">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                   style="font-size:12px;">
                                                <tr>
                                                    <th colspan="4"
                                                        style="padding:<?php echo $padding; ?>;text-align:left; background:white; padding-bottom:5px;">
                                                        Payment
                                                        Method: <?php echo $this->ecommerce_model->getPaymentMethod($orderData['payment_method']); ?></th>
                                                </tr>
                                                <?php $delivery = $orderData['delivery']; ?>

                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>Shipping
                                                            Address: </b></td>
                                                    <td colspan="3"><?php /*?><?php echo $orderData['user']['email'];?><?php */ ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Email Name:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $orderData['user']['email']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Trade Name:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $orderData['user']['trading_name']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Contact Name:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $orderData['user']['name']; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                            <!--Address 1:--> </b>
                                                        <!--</td><td colspan="3">--><?php echo $delivery['street_one']; ?>
                                                    </td>
                                                </tr>
                                                <?php if (!empty($delivery['street_two']) && $delivery['street_two'] != 0) { ?>
                                                    <tr>
                                                        <td style="padding:<?php echo $padding; ?>;" colspan="4"><b>
                                                                <!--Address 2:--> </b><!--</td><td colspan="3">--></td>
                                                    </tr>
                                                <?php } ?>

                                                <tr>
                                                    <td colspan="4">
                                                        <table width="100%" border="0" cellpadding="0"
                                                               style="text-align:left;font-size:12px;">
                                                            <tr>
                                                                <td style="padding:<?php echo $padding; ?>;width:15%">
                                                                    <b>City: </b></td>
                                                                <td style="width:30%;"><?php echo $delivery['city'] ?></td>
                                                                <td style="padding-left:3px;width:15%"><b>Country:</b>
                                                                </td>
                                                                <td style="width:40%;"> <?php echo $delivery['country']['title'] ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <table width="100%" border="0" cellpadding="0"
                                                               style="text-align:left;font-size:12px;">
                                                            <tr>
                                                                <td style="padding:<?php echo $padding; ?>; width:15%">
                                                                    <b>Phone: </b></td>
                                                                <td style="width:30%;"><?php echo cleanPhone($delivery['phone']) ?></td>


                                                                <td style="padding-left:3px; width:15%;">  <?php if (!empty($delivery['mobile']) && $delivery['mobile'] != 0) { ?>
                                                                        <b>Mobile:</b> <?php } ?></td>
                                                                <td style="width:30%;"><?php if (!empty($delivery['mobile']) && $delivery['mobile'] != 0) { ?><?php echo cleanPhone($delivery['mobile']); ?><?php } ?></td>
                                                            </tr>
                                                            <?php if ($delivery['pickup'] == 1) { ?>
                                                                <tr>
                                                                    <td colspan="4"
                                                                        style="padding:<?php echo $padding; ?>;color:red;font-size:16; padding-bottom:5px; text-align:left;"><?php echo lang('pickup'); ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php } ?>

        <!--//////////////////////ITEMS//////////////////-->
        <?php $total_units = 0; ?>
        <tr>
            <td>

                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <?php $total_quantity = 0; ?>
                    <?php
                    $line_items_brands = $orderData['line_items_brands'];
                    foreach ($line_items_brands as $line_items_brand) {
                        ?>
                        <?php if ((!empty($line_items_brand['id_brands']) || (empty($line_items_brand['id_brands']) && count($line_items_brands) > 1))) { ?>
                            <tr>
                                <td colspan="7" style="text-align:left;padding: 0; border-bottom:1px solid #002060;">
                                    <h2 style="font-size:13px; font-weight:600;margin:2px 0;"><?php echo $line_items_brand['deliver_by']; ?></h2>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="height:10px;"></td>
                            </tr>
                        <?php } ?>
                        <?php if (empty($line_items_brand['line_items']) && !empty($line_items_brand['line_items_redeem_by_miles'])) { ?>
                            <tr>
                                <?php $style = "border:1px solid black;border-left:none;padding:5px 1px;font-size:12px;"; ?>
                                <th <?php echo 'style="' . $style . ';border-left:1px solid black;"'; ?> width="5%">#
                                </th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="10%">REF</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="12%">BARCODE</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="40%">ITEM(S) REDEEMED BY REWARD
                                    PRODUCTS
                                </th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="10%"> MILES</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="8%">QTY</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="15%">SUB-TOTAL</th>
                            </tr>
                        <?php } ?>
                        <?php if (!empty($line_items_brand['line_items'])) { ?>
                            <tr>
                                <?php $style = "border:1px solid black;border-left:none;padding:5px 1px;font-size:12px;"; ?>
                                <th <?php echo 'style="' . $style . ';border-left:1px solid black;"'; ?> width="5%">#
                                </th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="10%">REF</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="12%">BARCODE</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="40%">PRODUCT DESCRIPTION</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="10%">
                                    PRICE <?php echo $orderData['currency']; ?></th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="8%">QTY</th>
                                <th <?php echo 'style="' . $style . '"'; ?> width="15%">SUB-TOTAL
                                    <br/> <?php echo $orderData['currency']; ?> </th>
                            </tr>
                        <?php } ?>
                        <?php $style = "padding:1px 3px;;text-align:center;font-size:12px;"; ?>

                        <?php $line_items = $line_items_brand['line_items']; ?>
                        <?php $i = 0;
                        if (!empty($line_items)) {
                            ?>
                            <?php foreach ($line_items as $pro) {
                                $total_quantity = $total_quantity + $pro['quantity'];
                                ?>
                                <?php $options = unSerializeStock($pro['options_en']);
                                $i++; ?>
                                <tr>
                                    <td <?php echo 'style="' . $style . '"'; ?>
                                            style="text-align:center;"><?php echo $i; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['product']['sku']; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['product']['barcode']; ?></td>
                                    <td <?php echo 'style="' . $style . 'text-align:left;"'; ?>><?php echo $pro['product']['title']; ?><?php if (!empty($options)) {
                                            $j = 0;

                                            $c = count($options);
                                            foreach ($options as $opt) {
                                                $j++;
                                                if ($j == 1) {
                                                    $com = "<br>";
                                                } else {
                                                    $com = ", ";
                                                } ?>
                                                <?php echo $com . $opt; ?>
                                            <?php }
                                        } ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['price_currency']; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['quantity']; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['total_price_currency'] . ' ' . $pro['currency']; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>

                        <!--////////////////Redeemed By Miles///////////////-->
                        <?php
                        $line_items = array();
                        if (isset($line_items_brand['line_items_redeem_by_miles'])) {
                            ?>
                            <?php $line_items = $line_items_brand['line_items_redeem_by_miles']; ?>
                        <?php } ?>
                        <?php $i = 0;
                        if (!empty($line_items)) {
                            ?>
                            <?php $style = "padding:1px 1px;;text-align:center;"; ?>

                            <?php if (!empty($line_items_brand['line_items'])) { ?>
                                <tr>
                                    <?php $style = "padding:3px 1px;font-weight:bold;text-align:center;font-size:12px;padding-top:10px;"; ?>
                                    <td <?php echo 'style="' . $style . '"'; ?> >#</td>
                                    <td <?php echo 'style="' . $style . '"'; ?> ></td>
                                    <td <?php echo 'style="' . $style . '"'; ?> ></td>
                                    <td <?php echo 'style="' . $style . '"'; ?> >ITEM(S) REDEEMED BY REWARD PRODUCTS
                                    </td>
                                    <td <?php echo 'style="' . $style . '"'; ?>>MILES</td>
                                    <td <?php echo 'style="' . $style . '"'; ?> ></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>>SUB-TOTAL</td>
                                </tr>
                            <?php } ?>
                            <?php $style = "padding:3px 2px;text-align:center;font-size:12px;font-style:italic;"; ?>
                            <?php foreach ($line_items as $pro) {
                                $total_quantity = $total_quantity + $pro['quantity'];
                                ?>
                                <?php $options = unSerializeStock($pro['options_en']);
                                $i++; ?>

                                <tr>
                                    <td <?php echo 'style="' . $style . '"'; ?>
                                            style="text-align:center;"><?php echo $i; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['product']['sku']; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['product']['barcode']; ?></td>
                                    <td <?php echo 'style="' . $style . 'text-align:left;"'; ?>><?php echo $pro['product']['title']; ?><?php if (!empty($options)) {
                                            $j = 0;

                                            $c = count($options);
                                            foreach ($options as $opt) {
                                                $j++;
                                                if ($j == 1) {
                                                    $com = "<br>";
                                                } else {
                                                    $com = ", ";
                                                } ?>
                                                <?php echo $com . $opt; ?>
                                            <?php }
                                        } ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['redeem_miles']; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['quantity']; ?></td>
                                    <td <?php echo 'style="' . $style . '"'; ?>><?php echo $pro['redeem_miles'] * $pro['quantity']; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <tr>
                        <td colspan="5" style="text-align:right; padding-right:5px;font-size:12px;">Total Units</td>
                        <td style="text-align:center; border:1px solid black;font-size:12px;"><?php echo $total_quantity; ?></td>
                    </tr>
                </table>
            </td>
        </tr>

        <!--//////////////////////COMMENTS//////////////////-->
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td style="padding-left:0; width:70%">
                            <table width="100%" style="font-size: 11px;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><b>Special Instuction</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="float:left;width:95%; padding:5px 2%;min-height:50px; border:1px solid black;">
                                            <?php if (!empty($orderData['comments'])) { ?><?php echo $orderData['comments']; ?><?php } ?>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </td>
                        <!--/////////TOTAL///////////-->
                        <?php $style_1 = " text-align:right;";
                        $style_2 = " border-bottom:1px solid #d4d4d4;"; ?>

                        <td style="padding-top:17px;padding-right:0;font-size:11px;">
                            <?php if (isset($supplier_e) && $supplier_e == 1) {
                            } else { ?>
                                <table width="100%" cellpadding="2" cellspacing="2"
                                       style="text-align:right;font-size:11px;">
                                    <tr>
                                        <td <?php echo 'style="' . $style_1 . '"'; ?>>Sub-Total</td>
                                        <td <?php echo 'style="' . $style_2 . '"'; ?>><?php echo $orderData['total_price_currency'] . ' ' . $orderData['currency']; ?></td>
                                    </tr>
                                    <?php if (!empty($orderData['discount']) && $orderData['discount'] > 0) { ?>
                                        <tr>
                                            <td <?php echo 'style="' . $style_1 . '"'; ?>>Discount</td>
                                            <td <?php echo 'style="' . $style_2 . '"'; ?> ><?php echo $orderData['discount'] . ' ' . $orderData['currency']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if (!empty($orderData['shipping_charge_currency']) && $orderData['shipping_charge_currency'] > 0) { ?>
                                        <tr>
                                            <td <?php echo 'style="' . $style_1 . '"'; ?>>Shipping</td>
                                            <td <?php echo 'style="' . $style_2 . '"'; ?> ><?php echo $orderData['shipping_charge_currency'] . ' ' . $orderData['currency']; ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php if ($orderData['redeem_discount_miles_amount'] > 0) { ?>
                                        <tr>
                                            <td <?php echo 'style="' . $style_1 . '"'; ?>>Redeemed Miles</td>
                                            <td <?php echo 'style="' . $style_2 . '"'; ?> ><?php echo $orderData['redeem_miles'] . ' Miles'; ?></td>
                                        </tr>
                                    <?php } ?>

<?php

$q = "SELECT u.delivery_shipping_from FROM user AS u INNER JOIN products AS p ON p.id_user = u.id_user WHERE p.title LIKE '" . $pro['product']['title']. "'";
$query = $this->db->query($q);
foreach ($query->result() as $row){
    $deliveryaddress = $row->shipping;
}
$amout = $orderData['amount_currency'];
if($deliveryaddress = 'United Arab Emirates'){
?>
                                    <tr>
                                        <td <?php echo 'style="' . $style_1 . '"'; ?>><b>TAX 5%</b></td>
                                        <td <?php echo 'style="' . $style_2 . '"'; ?> >
                                            <b><?php echo $amout*0.05; ?></b>
                                        </td>
                                    </tr>
<?php
      $amout = ($amout*0.05) + $amout;
} else {
    $amout = $orderData['amount_currency'];
}?>
                                    <tr>
                                        <td <?php echo 'style="' . $style_1 . '"'; ?>><b>TOTAL AED</b></td>
                                        <td <?php echo 'style="' . $style_2 . '"'; ?> >
                                            <b><?php echo $orderData['amount_currency'] . ' ' . $orderData['currency']; ?></b>
                                        </td>
                                    </tr>
                                </table>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;font-size:12px; text-transform:capitalize;" colspan="2">
                            <?php if (isset($supplier_e) && $supplier_e == 1) {
                            } else { ?>
                                <?php echo convert_number_to_words($orderData['amount_currency']); ?><?php echo $orderData['currency']; ?>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td style="text-align:right; font-size:12px;" colspan="2">Thank you for your Business</td>
                    </tr>
                </table>
            </td>
        </tr>

        <!--//////////////////////Company Brief//////////////////-->
        <!--<tr>
        <td style="text-align:center;font-size:12px; padding-top:10px; font-weight:600;">Medispa Solutions General Trading LLC | Office 1202 Regal Tower Business Bay Dubai UAE <br /> POBox 487874 | +971 4 277 5920 / 25 | info@ebeautytrade.com</td>
        </tr>-->

        <!--///////////////////////////////////////////////-->

    </table>
</div>
