<?php $color="#ffffff";?>
<?php $site_info=$this->fct->getonerow('settings',array('id_settings'=>1));?>
<table width="100%" cellspacing="0" cellpadding="0" border="0"  style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:11px;margin:0;padding:0;border-bottom:1px solid #e0e0e0">
  <tbody>
    <tr>
      <td valign="top" align="center" style="padding:20px 0 0 0"><table width="100%" cellspacing="0" cellpadding="10" border="0" bgcolor="#FFFFFF" >

          <tbody>
          <!--////////////////////HEAD//////////////-->
            <tr>
            <table cellspacing="0" width="100%" cellpadding="0" style="padding:0;margin:0;" >
            <tr>
            <td style="text-align:center;padding: 0; vertical-align:bottom;width:35%;text-align:left; font-weight:600; vertical-align:top">
            <table>
            <tr><td style="font-size: 11px;" >
         <?php echo $site_info['address'];?>

            </td></tr>
            </table>
            </td>
            <?php if(!isset($print)){?>
            <td valign="middle" style="text-align:center;padding: 0; vertical-align:middle"><a target="_blank" href="<?php echo site_url();?>" style="display:inline-block;"> <img  border="0" style="margin-bottom:10px;width:150px;margin:0 40px;" alt="Spamiles" src="<?php echo base_url();?>front/img/logo.png"> </a></td>
            <?php } ?>
            <td valign="top" style="text-align:center;padding: 0;width:35%;text-align:left; font-weight:600;">
            <table   style="border:1px solid black;font-size: 11px;" width="100%" cellspacing="0" cellpadding="3" border="1" >
            <tr><td style="border:none;font-size: 11px;">
 <span style="font-size:13px;margin-bottom:2px; display:inline-block;"><?php echo $orderData['user']['trading_name'];?></span><br />
<?php echo $orderData['user']['address_1'];?><br />
<?php if(!empty($orderData['user']['phone'])){ ?>
Tel : <?php echo $orderData['user']['phone'];?>
<br />
<?php } ?>
<?php if(!empty($orderData['user']['fax'])){ ?>
Fax: <?php echo $orderData['user']['fax'];?>
<?php } ?>

<?php if(!empty($orderData['user']['website'])){ ?>
<?php echo $orderData['user']['website'];?>
<?php } ?>
            </td></tr>
            </table>
            </td></tr>
            </table>
              
            </tr>
            
         <!--////////////////////TITLE//////////////-->
         <tr><td style="text-align:center; text-transform:uppercase; "><h1 style="font-weight:normal;border-bottom:1px solid black;margin: 5px 0;font-size:20px; padding:0;"><?php echo $section_name;?></h1></td></tr>
         
         <!--////////////////////INFO//////////////-->
         <tr>
         <td style="padding:0; font-weight:600;">
         <table style="font-size: 11px;" >
         <tr>
         <td style="padding:0;width:48%; vertical-align:top;">
         <table>
         <tr>
         <td style="padding:0;padding-bottom:5px;font-size: 11px;">
          Order No : <?php if(!empty($orderData['id_invoice'])){?>
		 <?php echo $orderData['id_invoice'];?>
         <?php }else{ ?>
          <?php echo $orderData['id_delivery_order'];?>
         <?php } ?><br />
        
          Order Date: <?php echo  date("d.m.Y", strtotime($orderData['created_date'])); ?> <br />
          <?php if(!empty($orderData['delivery_date'])){?>
          Delivery Date: <?php echo  date("d.m.Y", strtotime($orderData['delivery_date'])); ?> <br />
          <?php  } ?>
          Prepared By: 
		 
         </td>
		 </tr>
         <tr>
         
         
         
         
         
         </tr>
         <tr>
    
         
         
         
         </tr>
         </table>
         </td>
         <td style="width:48%;"><table>
         <tr>
         <td style="">
    	<?php echo $orderData['order_conditions'];?>
         </td>
           </tr>
         
         
         </table></td>
         </tr>
         </table>
         </td>
         </tr>
         
         <!--////////////////////ORDER DETAILS//////////////-->
            
           <tr>
             
            
<?php $brands_line_items=$orderData['line_items'];?>
<?php $total_qty=0;?>
<?php if(!empty($brands_line_items)) {?>
<?php foreach($brands_line_items as $brand) { ?>
            <tr> <td style="text-align:center;color:#333; padding:0;"><h2 style="font-weight:normal;font-size:16px;margin-top:0;margin-bottom:5px;"><?php echo $brand['title'];?></h2></td></tr>
            <tr>
              <td style="padding-top:0;">
          
             <table <?php if(isset($hide_price)){  echo 'width="75%"  align="center"' ; }else{ ?> width="100%"  <?php } ?> cellspacing="0" cellpadding="0" border="0" >
                  <thead>
                    <tr>
                    <th  align="center" style="background:#c0c0c0;color:black; width:200px;font-size:13px;padding:5px 9px;text-align:left;">REF / SKU</th>
<!--                     <th  align="center" style="background:#c0c0c0;color:black; width:200px;font-size:13px;padding:5px 9px;text-align:left;">Barcode</th>
                     
                      <th  align="center" style="background:#c0c0c0;color:black; width:200px;font-size:13px;padding:5px 9px;text-align:left;">Shelf Location</th>-->
                    
                      <th width="35%"  style="background:#c0c0c0;color:black;font-size:13px;padding:3px 9px;">Description</th>
                      
                    
                      <th  style="text-align:center;width:70px;font-size:13px;padding:3px 9px;background:#c0c0c0;color:black;">Qty</th>
                      <?php if(isset($hide_price)){}else{ ?>
    
                      <th width="20%" bgcolor="<?php echo $color;?>" align="center" style="font-size:13px; text-align:right;padding:3px 9px;background:#c0c0c0;color:black;">Unit Price</th>
                      <?php } ?>
                      
         <?php if(isset($hide_price)){}else{ ?>
                  <th width="20%" bgcolor="<?php echo $color;?>" align="center" style="font-size:13px; text-align:right;padding:3px 9px;background:#c0c0c0;color:black;">Amount</th>
                      <?php } ?>
                     
                  	</tr>
                  </thead>
          <?php 
$line_items = $brand['line_items'];
if(!empty($line_items)) {?>
                  <?php 


foreach($line_items as $pro) {
$qty=$pro['quantity'];
$total_qty=$total_qty+$qty;
$shelf_location=$pro['product']['location_code'];

		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];
		$barcode=$pro['stock']['barcode'];
			
		$options=unSerializeStock($pro['stock']['combination']); 
		}else{
			
		$sku=$pro['product']['sku'];
		$barcode=$pro['product']['barcode'];
		$options="";}

?>
                  <tbody bgcolor="#F6F6F6">
                    <tr>
          			  <td valign="top" align="center" style="text-align:left;font-size:11px;padding:5px;"><?php echo $sku;?></td>
                      <td valign="top" align="left" style="font-size:11px; text-align:left;padding:5px;"><?php echo $pro['product']['title'];?> <br />
       <?php
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
                      </td>
<!--                      <td valign="top" align="center" style="text-align:left;font-size:12px;padding:5px;border-bottom:1px dotted #cccccc"><?php echo $barcode;?></td>
                      
                       <td valign="top" align="center" style="text-align:left;font-size:12px;padding:5px;border-bottom:1px dotted #cccccc"><?php echo $shelf_location;?></td>-->
                      
                      <td valign="top" align="center" style="text-align:center;font-size:11px;padding:5px"><?php echo $qty;?></td>
                     <?php if(isset($hide_price)){}else{ ?>
  					  <td valign="top" style="text-align:right;font-size:11px;padding:5px"><?php echo roundPrice($pro['cost']);?></td> 
                     <?php } ?>
                     
                    <?php if(isset($hide_price)){}else{ ?>
					<td valign="top" align="center" style="text-align:right;font-size:11px;padding:5px">
					<?php echo $orderData['currency'];?> <?php echo roundPrice($qty*$pro['cost']);?></td> 
                    <?php } ?>
                      

                    </tr>
                  </tbody>
                  <?php }} ?>
                  </table></td></tr>
          <?php } } ?>
          
           <tr><td style="text-align:right; font-size:15px; padding-right:0;">
           <table width="100%" style="font-size: 12px;" >
       <tr><td style="text-align:center;">
           Total Units :<span style="border:1px solid black;width:50px;display:inline-block;text-align:center;"><?php echo $total_qty;?></span>
           </td></tr>
           </table></td></tr>
           <tr>
           <td>
           <table width="100%">
           <tr>
           <td style="padding-left:0; width:70%">
           <table width="100%" style="font-size: 11px;" >
           <tr><td><b>Comments</b></td></tr>
           <tr><td><div style="float:left;width:95%;height:65px; border:1px solid black;"></div></td></tr>
           </table>
           </td>
           <td  style="padding-top:17px;padding-right:0;font-size:11px;" >
           <table   width="100%" cellpadding="2" cellspacing="2" style="text-align:right;border:1px solid black;font-size:11px;">
           <tr><td  style="border-bottom:1px solid black; border-right:1px solid black;">Sub-Total</td><td  style="border-bottom:1px solid black;"><?php echo $orderData['total_cost'];?><?php echo $orderData['currency'];?></td></tr>
           <tr><td  style="border-bottom:1px solid black; border-right:1px solid black;">Discount</td><td style="border-bottom:1px solid black;"></td></tr>
           <tr><td  style="border-right:1px solid black;"><b>TOTAL</b></td><td  ><b><?php echo $orderData['total_cost'];?> <?php echo $orderData['currency'];?></b></td></tr>
           </table>
           </td>
           </tr>
           </table>
           </td>
           </tr>
           
           <tr><td style="color:#ff0000; padding-bottom:5px; padding-top:10px; font-size:14px;"><b>Orginal</b></td></tr>
           
            <!--<tr>
              <td bgcolor="<?php echo $color;?>" align="center" style="background:<?php echo $color;?>;text-align:center"><center>
                  <p style="font-size:12px;margin:0"> Thank you, <strong><a style="color:black; text-decoration:none;" target="_blank" href="<?php echo site_url();?>">Spamiles</a></strong> </p>
                </center></td>
            </tr>-->
          </tbody>
      </table>
        </td>
    </tr>
    
    <!--////////////////////SIGNATURE//////////////-->
    
  </tbody>
</table>

