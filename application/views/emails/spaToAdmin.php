
<div style="background:#9ce1fc;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td valign="top" align="center" style="padding:20px 0 20px 0">
<table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #e0e0e0;color:#404040;font-size:14px">
<tbody>
<tr>
<td valign="top">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>

</tbody>
</table>
</td>
</tr>
<tr style="text-align:center;" ><td><a target="_blank" href="<?php echo site_url();?>"><img style="width:150px;" src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>
<tr>
<td valign="top">
<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Dear <?php echo $admindata['website_title'];?></h1>
New message from  <?php  echo $data['name'] ?> on <?php  echo $data['created_date']; ?> 
<?php if(!empty($data['id_consultants'])){
$consultant_name=$this->fct->getonecell('consultants','name',array('id_consultants'=>$data['id_consultants'])); ?>
<p><strong>The consultant: </strong><?php echo $consultant_name ;?></p>
<?php } ?>
<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">PERSONAL INFORMATION</p>

<p><strong>First Name : </strong><?php echo $data['first_name'] ;?></p>
<p><strong>Last Name : </strong><?php echo $data['last_name'] ;?></p>
<p><strong>Position : </strong><?php echo $data['position'] ;?></p>
<p><strong>E-mail : </strong><?php echo $data['email'] ;?></p>
<p><strong>Mobile : </strong><?php echo $data['mobile'] ;?></p>
<p><strong>Phone : </strong><?php echo $data['phone'] ;?></p>
<?php if(!empty($data['trade_name']) || !empty($data['website']) || !empty($data['company_phone']) || !empty($data['city']) || !empty($data['id_countries']) || !empty($data['company_email']) || !empty($data['address'])){?>
<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">COMPANY DETAILS (If available)</p>
<?php if(!empty($data['trade_name'])){?>
<p><strong>Trade Name : </strong><?php echo $data['trade_name'] ;?></p>
<?php } ?>
<?php if(!empty($data['website'])){?>
<p><strong>Website : </strong><?php echo $data['website'] ;?></p>
<?php } ?>
<?php if(!empty($data['company_phone'])){?>
<p><strong>Company Name : </strong><?php echo $data['company_phone'] ;?></p>
<?php } ?>
<?php if(!empty($data['city'])){?>
<p><strong>City : </strong><?php echo $data['city'] ;?></p>
<?php } ?>
<?php if(!empty($data['id_countries'])){?>
<p>
<strong>Country : </strong><?php echo $this->fct->getonecell('countries','title',array('id_countries'=>$data['id_countries'])); ;?></p>
<?php } ?>
<?php if(!empty($data['company_email'])){?>
<p><strong>Company E-mail : </strong><?php echo $data['company_email'] ;?></p>
<?php } ?>
<?php if(!empty($data['address'])){?>
<p><strong>Address : </strong><?php echo $data['address'] ;?></p>
<?php } ?>
<?php } ?>
<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">PROJECT TYPE</p>
<?php 
$text="";
$projects = $this->fct->select_spa_projects($data['id_opening_a_new_spa']);
$j=0;foreach($projects as $val){$j++;
$title=$this->fct->getonecell('projects_type','title',array('id_projects_type'=>$val));
if($j==1){
	$text = $title;
	}else{
	$text .= ', '.$title;	
}}
echo $text;	
	?>
  
  
<?php $services = $this->fct->select_spa_services($data['id_opening_a_new_spa']); ?>   
<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">SERVICE(S) YOU MAY ENQUIRE</p>
<?php 
$text="";
$j=0;foreach($services as $val){$j++;
$title=$this->fct->getonecell('services_enquiry','title',array('id_services_enquiry'=>$val));
if($j==1){
	$text = $title;
	}else{
	$text .= ', '.$title;	
}}
echo $text;	
?> 

<?php $consultants = $this->fct->select_spa_cosnultants($data['id_opening_a_new_spa']); ?>   
<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">Consultants Meeting Request</p>
<?php 
$text="";
$j=0;foreach($consultants as $val){$j++;
	echo $val['name'].' : '.$val['message'].'<br>';
}

?> 

<?php if(!empty($data['comments'])){?>
<p><strong>Comments : </strong><?php echo $data['comments'] ;?></p>
<?php } ?>
    
<p></p>
<p></p>
</td>
</tr>
<tr>
<td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
<center>
<p style="font-size:11px;margin:0">
 <?php  $link = site_url('back_office/opening_a_new_spa/edit/'.$data['id_opening_a_new_spa']); ?>  
	<p><a style="background:#9ce1fc; padding: 6px 12px;color:white; text-decoration:none;" href="<?php echo $link?>">Click To View</a></p>
</p>
</center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</div>
