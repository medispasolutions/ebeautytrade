<?php

if (!function_exists('_user_mail_header')) {
    function _user_mail_header($title)
    {
        return '<h1 style="color: #25ade3; font-size: 18px; font-weight: normal;">' . $title . '</h1>';
    }
}

if (!function_exists('_user_mail_small_font')) {
    function _user_mail_small_font($content)
    {
        return '<p style="font-size: 11px; color: black">' . $content . '</p>';
    }
}

if (!function_exists('_user_mail_link')) {
    function _user_mail_link($href, $name)
    {
        return '<a href="'.$href.'" style="color: #25ade3; text-decoration: none">' . $name . '</a>';
    }
}



if (!function_exists('_user_mail_asset_url')) {
    function _user_mail_asset_url($path)
    {
        return site_url().'public/shop/emails/'.$path;
    }
}
