<?php
include('_user_macros.php'); ?>

<?= _user_mail_header("Dear admin,") ?>

<?php $randomRequest = $request['random_number']; ?>

<?php 
    $planSelected = $request['plan_selected'];
    if($planSelected == "free1-overseas"){
        $dataplan = "Free Plan";
        $brandNumber = "1 Brand";
    } else if($planSelected == "free2-overseas"){
        $dataplan = "Free Plan";
        $brandNumber = "2 Brands";
    } else if($planSelected == "free3-overseas"){
        $dataplan = "Free Plan";
        $brandNumber = "3 Brands ";
    } else if($planSelected == "gold1-overseas"){
        $dataplan = "Gold Plan";
        $brandNumber = "1 Brand";
    } else if($planSelected == "gold2-overseas"){
        $dataplan = "Gold Plan";
        $brandNumber = "2 Brands";
    } else if($planSelected == "gold3-overseas"){
        $dataplan = "Gold Plan";
        $brandNumber = "3 Brands ";
    } else if($planSelected == "fbe1-overseas"){
        $dataplan = "FBE Plan";
        $brandNumber = "1 Brand";
    } else if($planSelected == "fbe2-overseas"){
        $dataplan = "FBE Plan";
        $brandNumber = "2 Brand";
    } else if($planSelected == "fbe3-overseas"){
        $dataplan = "FBE Plan";
        $brandNumber = "3 Brand";
    }
?>

<p>
    A new member Submitted to our Landing page As a Overseas Supplier:<br/>
    Name: <?php echo $request['first_name']; ?><br/>
    Email: <?php echo $request['email']; ?><br/>
    Plan Selected: <?php echo $dataplan; ?><br/>
    Brand number selected: <?php echo $brandNumber; ?>
</p>

<p>
    <br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>