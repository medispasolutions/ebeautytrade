<?php
include('_user_macros.php'); ?>

<?= _user_mail_header('Dear Customer,') ?>
<p>
    A request to reset the password for your personal account has been received.<br>
    To reset your password, please click on the link below (or copy and paste the URL into your browser).
</p>

<p><?= _user_mail_link($reset_link, $reset_link) ?></p>

<p>
    If you have any questions, please feel free to contact us <?= _user_mail_link("mailto:customerservice@ebeautytrade.com", "customerservice@ebeautytrade.com") ?>
</p>

<p>
    <br><br><br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>
