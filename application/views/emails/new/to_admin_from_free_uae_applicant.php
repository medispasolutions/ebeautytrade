<?php
include('_user_macros.php'); ?>

<?= _user_mail_header("Dear admin,") ?>

<?php $randomRequest = $request['random_number']; ?>

<?php 
    $planSelected = $request['plan_selected'];
    if($planSelected == "free1"){
        $dataplan = "Free Plan";
        $brandNumber = "1 Brand";
    } else if($planSelected == "free2"){
        $dataplan = "Free Plan";
        $brandNumber = "Up to 3 Brands";
    } else if($planSelected == "free3"){
        $dataplan = "Free Plan";
        $brandNumber = "Up to 6 Brands ";
    } else if($planSelected == "free4"){
        $dataplan = "Free Plan";
        $brandNumber = "Unlimited Brands";
    } else if($planSelected == "gold1"){
        $dataplan = "Gold Plan";
        $brandNumber = "1 Brand";
    } else if($planSelected == "gold2"){
        $dataplan = "Gold Plan";
        $brandNumber = "Up to 3 Brands";
    } else if($planSelected == "gold3"){
        $dataplan = "Gold Plan";
        $brandNumber = "Up to 6 Brands ";
    } else if($planSelected == "gold4"){
        $dataplan = "Gold Plan";
        $brandNumber = "Unlimited Brands";
    } else if($planSelected == "fbe1"){
        $dataplan = "FBE Plan";
        $brandNumber = "1 Brand";
    } else if($planSelected == "fbe2"){
        $dataplan = "FBE Plan";
        $brandNumber = "2 Brand";
    } else if($planSelected == "fbe3"){
        $dataplan = "FBE Plan";
        $brandNumber = "3 Brand";
    } else if($planSelected == "fbe4"){
        $dataplan = "FBE Plan";
        $brandNumber = "4 Brand";
    }
?>

<p>
    A new member Submitted to our Landing page As a UAE Supplier:<br/>
    Name: <?php echo $request['first_name']; ?><br/>
    Email: <?php echo $request['email']; ?><br/>
    Plan Selected: <?php echo $dataplan; ?><br/>
    Brand number selected: <?php echo $brandNumber; ?>
</p>

<p>
    <br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>