<?php
include('_user_macros.php'); ?>

<?= _user_mail_header('Successfully submitted!') ?>
<p>
    Please find your order details below:
</p>

<p>
    <strong>Order ID: </strong><?= $orderData['id_orders'] ?><br>
    <strong>Customer Name: </strong><?= $orderData['user']['name'] ?><br>
    <strong>Created date: </strong><?= $orderData['created_date'] ?><br>
</p>
<br>
<table class="order" align="center" width="100%" cellpadding="4" cellspacing="0" style="border: 0; background: #fff; width: 100%">
    <tr>
        <th style="text-align:left" align="left">PRODUCT</th>
        <th style="text-align:center" align="center">PRICE AED</th>
        <th style="text-align:center" align="center">QTY</th>
        <th style="text-align:center" align="center">SUB-TOTAL AED</th>
    </tr>
<?php
$line_items_brands = $orderData['line_items_brands'];
$total = 0;
foreach($orderData['line_items_brands'] as $line_items_brand):
    foreach($line_items_brand['line_items'] as $product):
        $total += $product['total_price_currency'] ?>
    <tr>
        <td style="text-align:left" align="left"><?= $product['product']['title'] ?></td>
        <td style="text-align:center" align="center"><?= $product['price_currency'] ?></td>
        <td style="text-align:center" align="center"><?= $product['quantity'] ?></td>
        <td style="text-align:center" align="center"><?= $product['total_price_currency'] ?></td>
    </tr>
<?php
    endforeach;
endforeach; ?>
<?php
$q = "SELECT u.delivery_shipping_from FROM user AS u INNER JOIN products AS p ON p.id_user = u.id_user WHERE p.title LIKE '" . $product['product']['title'] . "'";
$query = $this->db->query($q);
foreach ($query->result() as $row){
    $deliveryaddress = $row->shipping;
}
if($deliveryaddress = 'United Arab Emirates'){
?>
    <tr>
        <td colspan="3" style="text-align:right" align="right"><i>5% TAX</i></td>
        <td style="text-align:center" align="center">
            <?php $total_tax = $total * 0.05; echo $total_tax; 
                $total = ($total * 0.05) + $total;
            ?>
        </td>
    </tr>
<?php } else {
    $total = number_format($total, 2, '.', ' ');
} ?>
    <tr>
        <td colspan="3" style="text-align:right" align="right"><i>TOTAL AED</i></td>
        <td style="text-align:center" align="center"><?= $total; ?></td>
    </tr>
</table>
<br><br>
<p>
    The selected supplier(s) will contact you to confirm items availability, delivery and conditions.
    <br><br>
    Thank you for making spamiles<sup>PRO</sup> your beauty marketplace of choice!<br>
    We hope you are enjoying your experience with us.
</p>

<p>
    <br><br><br>
    Best regards,<br>
    spamiles Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>
