<?php
include('_user_macros.php'); ?>

<p><strong>Dear <?php echo $request['first_name']; ?>,</strong> </p>

<p>
    Thank you for applying for a Seller Membership on eBeautyTrade ME.<br/>
    We will revert back to you shortly after review.
<p><br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>