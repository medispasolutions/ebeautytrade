<?php
include('_user_macros.php'); ?>

<?= _user_mail_header('Registration successfully submitted!') ?>
<p>You successfully submitted your registration request on EBeautyTrade.</p>

<p>Your enquiry is under review by our Team and we will get back to you shortly.</p>

<p>
    <br><br><br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>
