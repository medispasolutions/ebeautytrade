<?php
include('_user_macros.php'); ?>

<?= _user_mail_header("Dear {$request['first_name']},") ?>

<p>Kindly find below you credential to access Ebeautytrade backend:</p>
<p>
    URL: https://www.ebeautytrade.com/back_office/ <br/>
    Email: <?php echo $request['email']; ?> <br/>
    Password: <?php echo $request['encrypt_pass']; ?>
</p>

<p>
    <br><br><br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>
