<?php
include('_user_macros.php'); ?>

<?= _user_mail_header('Welcome to EBeautyTrade<sup>PRO</sup>!') ?>
<p>
    We are pleased to confirm you that your profile is now active and you have full access to the functionalities reserved to our Members!
</p>

<p>
    Creating your account with us allows you to:
</p>
<ul>
    <li>Access authentic wholesale prices of 70+ professional Brands<br><br></li>
    <li>Order anytime, anywhere, at your convenience<br><br></li>
    <li>Order from Multiple Suppliers at one place<br><br></li>
    <li>Replenish your stock easily using My Re-Order list<br><br></li>
    <li>Keep your purchases records for better analytics<br><br></li>
</ul>

<p>
    If you have any questions about your account, or any requirements related to your EBeautyTrade experience, feel free to contact us.
    <br><br>
    Our Team is here to help you!
</p>

<p>
    <br><br><br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>
