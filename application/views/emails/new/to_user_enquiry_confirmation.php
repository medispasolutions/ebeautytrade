<?php
include('_user_macros.php'); ?>

<?= _user_mail_header("Dear {$request['name']},") ?>

<?= $replyMessage['message'] ?>

<p>
    <br><br><br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>