<?php
include('_user_macros.php'); ?>
<html>
<head>
    <style>
        p {
            color: #959595;
        }

        table.order {
        }

        table.order th, table.order td {
            color: #959595;
            font-size: 13px;
        }
    </style>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
    <title>E-MAIL</title>
</head>
<body style="background: #fff; font-family: Arial, Verdana; margin-top:0; margin-bottom:0; padding:0;">

<table align="center" cellpadding="0" cellspacing="0" style="border: 0; background: #fff; width: 600px !important;">
    <tr>
        <td colspan="3" style="font-size: 11px; font-family: Arial; text-transform: uppercase;">
            <img src="<?= _user_mail_asset_url('img/logo.png') ?>"/><br>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="width: 480px; color: #969696; padding-top: 50px; font-size: 13px;">
            <?= $content ?>
        </td>
        <td style="width: 60px;">&nbsp;</td>
    </tr>
</table>
<table align="center" cellpadding="0" cellspacing="0"
       style="border: 0; background: #959595; width: 100%; margin-top: 20px;">
    <tr>
        <td style="width: 100%; height: 45px; background: #959595;">
            <table align="center" cellpadding="0" cellspacing="0"
                   style="border: 0;background: #fff; width: 600px !important;">
                <tr>
                    <td colspan="3"
                        style="background: #959595; font-size: 11px; font-family: Arial; color: #fff;text-align: center; padding-top: 20px; padding-bottom: 20px;">
                        <p style="color: white">Copyright &copy; <?= date("Y") ?><br>Medispa Solutions GT LLC | Level 14, Boulevard Plaza Tower One Downtown, | Dubai, UAE<br>All rights reserved</p>
                        <p style="color: white"><span style="text-decoration: underline;">Please add <a
                                        href="mailto: marketing@ebeautytrade.com">info@ebeautytrade.com</a> to your address book</span>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>