<?php
include('_user_macros.php'); ?>

<?= _user_mail_header("Dear admin,") ?>

<?php $randomRequest = $request['random_number']; ?>

<p>We welcome you on-board!</p>
<p>A new overseas applicant has applied to your system.</p>

<?php 
    $query = $this->db->query("SELECT * from supplier_application WHERE random_number like '" . $randomRequest. "' limit 1");
    foreach ($query->result() as $row)
    {
        $landingPlan = $row->plan_selected;
        $suplId = $row->id_supplier_application;
    }
    if($landingPlan == "5"){
        $type = "Fulfilled By Supplier";
    } else if($landingPlan == "6"){
        $type = "Fulfilled By Sponsor";
    }
?>

<p>His Plan: <?php echo $type; ?></p>
<p>To check his details <a href="https://www.ebeautytrade.com/back_office/new_applicant/edit/<?php echo $suplId; ?>">click here</a></p>

<p>
    <br><br>
    Best regards,<br>
    EBeautyTrade Team
</p>

<br><br><br>
<center>
    <?= _user_mail_small_font('***This is an automatically generated email. Please do not reply.***') ?>
</center>