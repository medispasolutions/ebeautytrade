
<div style="background:#9ce1fc;font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;margin:0;padding:0">
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td valign="top" align="center" style="padding:20px 0 20px 0">
<table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #e0e0e0;color:#404040;font-size:14px">
<tbody>
<tr>
<td valign="top">

<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tbody>

</tbody>
</table>
</td>
</tr>
<tr>
<td style="text-align: center;">
<?php $data['admindata']=$admindata; ?>
<?php $this->load->view('emails/social_media',$data); ?>
</td></tr>
<tr style="text-align:center;" ><td><a target="_blank" href="<?php echo site_url();?>"><img style="width:150px;" src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>

<tr>
<td valign="top">
<h1 style="font-size:22px;font-weight:normal;line-height:22px;margin:0 0 11px 0">Dear <?php echo $user['name'];?>,</h1>
<p style="font-size:12px;line-height:16px;margin:0 0 16px 0">

<?php echo $replymessage['message'];?>
<p style="border-bottom:1px solid #9ce1fc;font-size:24px;margin: 18px 0;padding: 2px 0;">Details</p>

<p><strong>Name : </strong><?php echo $brand['title'] ;?></p>
<p><strong>Logo : </strong><a href="<?= base_url(); ?>uploads/brands/<?= $brand['logo']; ?>" target="_blank"  > show logo </a></p>
<?php if($brand['video']){ ?>
<p><strong>Youtube : </strong><?php echo $brand['video'] ;?></p>
<?php } ?>
<p><strong>Catalog : </strong><a href="<?= base_url(); ?>uploads/brands/<?= $brand['catalog']; ?>" target="_blank"  > show catalog </a></p>
<?php if($brand['url']){ ?>
<p><strong>Website Link : </strong><?php echo $brand['url'] ;?></p>
<?php } ?>
<?php if($brand['overview']){ ?>
<p><strong>Overview : </strong><?php echo $brand['overview'] ;?></p>
<?php } ?>

<p></p>
</td>
</tr>
<tr>
<td bgcolor="#EAEAEA" align="center" style="background:#eaeaea;text-align:center">
<center>
<p style="font-size:11px;margin:0">
 <?php  $link = site_url('user/brand/'.$brand['id_brands']); ?>  
	<p><a style="background:#9ce1fc; padding: 6px 12px;color:white; text-decoration:none;" href="<?php echo $link?>">Click To View</a></p>
</p>
</center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</div>
