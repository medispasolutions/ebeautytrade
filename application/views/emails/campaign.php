<?php 
$count_brands=$this->custom_fct->getBrands(array());
$list_brands=$this->custom_fct->getBrands(array('status'=>1,'logo'=>1),$count_brands,0);
?>
<table width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td valign="top" align="center" >
<table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="f5f8fa" style="color:#404040;font-size:14px">
<tbody>
<tr>
<td valign="top" style="padding:0;">
<table width="100%"  cellspacing="0" cellpadding="10" border="0" style="font-family:Arial, Helvetica, sans-serif;letter-spacing:1.3;text-align:center;"  >
<tr>
<td style="text-align:center;"><a href="<?php echo site_url('');?>"><img src="<?php echo base_url();?>front/img/logo.png" /></a></td></tr>

<tr>
<td style="color:#313337; font-size:20px;padding-bottom:5px;">EXPOSE YOUR BRAND <b>ONLINE</b></td>
</tr>

<tr>
<td style="color:#75797a; font-size:12px;letter-spacing:1.1;padding-top:0;padding-bottom:10px;">Exlusively for Spas, Salons & beauty Clinics</td>
</tr>

<tr><td style="text-align:center;padding-top:0;"><img src="<?php echo base_url();?>front/img/campaign/laptop.png" /></td></tr>
<tr>
<td style="color:#4e4f52; font-size:20px;">EBeautyTrade is the first Online B2B Store in the Middle East </td></tr>
<tr><td style="color:#24aee5;padding:3px;font-size:18px;">Why Partner with EBeautyTrade?</td></tr>
<tr><td style="color:#85888c;padding:2px;font-size:14px;">Increase your Online Ranking</td></tr>
<tr><td style="color:#85888c;padding:2px;font-size:14px;">Get more leads through Business Meeting Setup</td></tr>
<tr><td style="color:#85888c;padding:2px;font-size:14px;">Display your Testers & Catalogs in our Dubai Showroom</td></tr>
<tr><td style="text-align:center;"><img src="<?php echo base_url();?>front/img/campaign/yes_like.png" /></td></tr>

</table>
</td>
</tr>
<tr>
<td style="color:#52bce9;font-size:16px;font-family:Arial, Helvetica, sans-serif;padding-bottom:0;padding-left:15px;">Our Esteemed Partners</td>
</tr>
<tr>
<td>
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<?php $i=0;
$counter=count($list_brands); 
foreach($list_brands as $val){
	$i++;?>
<td><a style="width:90%;margin:5px 5%;display:inline-block;height:100px;border:3px solid #e4e4e4;background-color:white;" href="<?php echo route_to('brands/details/'.$val['id_brands']);?>">
<table width="100%" height="100%"><tr><td style="vertical-align:middle;text-align:center;">
<img style="max-height:95px;max-width:90px;"  src="<?php echo base_url();?>uploads/brands/112x122/<?php echo $val['logo'];?>" alt="<?php echo $val['title'];?>" title="<?php echo $val['title'];?>" />
</td></tr></table></a></td>

<?php if($i%6==0){?>
	</tr><tr>
	<?php }?>
<?php } ?>

</tr>
</table>
</td></tr>


<tr>
<td style="text-align:right;font-style:italic;color:#52bce9;font-size:16px;font-family:Arial, Helvetica, sans-serif;padding-top:0;padding-right:15px;">...stay tuned for more!</td>
</tr>

<tr>
<td align="center" height="70" valign="top">
<table width="300px"cellspacing="0" cellpadding="10" border="0"   align="center"><tr><td style="border:1px solid #f7d077;font-family:Arial, Helvetica, sans-serif;border-radius:3px;font-size:16px;color:#1d1f23;text-align:center;">Online B2C Store very soon!</td></tr></table>
</td>
</tr>

<tr>
          <td style="text-align:center;font-size:13px;letter-spacing:1.3; background-color:#24aee5; padding-top:10px; font-weight:600;font-family:Arial, Helvetica, sans-serif;color:#fafafa;">Medispa Solutions GT LLC | Office 308, Al Attar Business Avenue, Al Barsha 1, | Dubai, UAE <br /> P: +971 4 277 5920 | E:<a href="mailto:info@ebeautytrade.com" style="color:#fafafa;text-decoration:underline">info@ebeautytrade.com</a></td>
          </tr>





</tbody>
</table>
</td>
</tr>
</tbody>
</table>

