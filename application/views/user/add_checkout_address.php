<?php
if (isset($address) && !empty($address)) {
    $buttonLabel = lang('update_profile');
    $formAction = site_url('user/submitAddress');
    $pTitle = lang('user_account_update_profile');
    $pStar = '';
    $formID = 'checkOutAddressForm';
    $menuBlock = "user/account_menu";
    $update = true;
    $title = 'Edit Address';
} else {
    $update = false;
    $address['first_name'] = $userData['first_name'];
    $address['last_name'] = $userData['last_name'];
    $address['company'] = '';
    $address['phone'] = '';
    $address['mobile'] = '';
    $address['fax'] = '';
    $address['street_one'] = '';
    $address['street_two'] = '';
    $address['state'] = '';
    $address['city'] = '';
    $address['country'] = '';
    $address['id_countries'] = '';
    $address['postal_code'] = '';
    $address['billing'] = '';
    $address['shipping'] = '';

    $buttonLabel = lang('register');
    $pTitle = lang('create_an_account');
    $formAction = route_to('user/submitAddress');
    $pStar = ' *';
    $formID = 'checkOutAddressForm';
    $menuBlock = "user/user_menu";
    $title = 'Add New Address';
}


?>

<div class="row account-create add_address">
    <div class="tabs_cont mod">


        <div class="tab_container dashboard ">
            <div class="tab_content">
                <div class="col-main grid12-9 grid-col2-main no-gutter dashboard">
                    <form id="<?php echo $formID; ?>" method="post" accept-charset="utf-8"
                          action="<?php echo $formAction; ?>">
                        <input type="hidden" name="checkout" value="1"/>

                        <?php
                        if (isset($address['id_users_addresses']) && !empty($address['id_users_addresses'])) { ?>
                            <input class="textbox" type="hidden" name="id_users_addresses"
                                   value="<?php echo $address['id_users_addresses']; ?>">
                        <?php } ?>
                        <div class="my-account ">
                            <div class="add_new_address">
                                <h2><?php echo $title; ?></h2>


                            </div>
                            <div class="box-account box-info">
                                <div class="box-head">
                                    <h2>Contact Information</h2>
                                </div>

                                <!--ROW 1-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required"> First Name <em>*</em> </label>
                                        <input class="textbox" type="text" name="first_name"
                                               value="<?php echo $address['first_name']; ?>">
                                        <span id="first_name-error" class="form-error"></span></div>
                                    <div class="form-item gutter-right">
                                        <label class="required"> Last Name <em>*</em> </label>
                                        <input class="textbox" type="text" name="last_name"
                                               value="<?php echo $address['last_name']; ?>">
                                        <span id="last_name-error" class="form-error"></span></div>
                                </div>

                                <!--ROW 2-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required">Company </label>
                                        <input class="textbox" type="text" name="company"
                                               value="<?php echo $address['company']; ?>">
                                        <span id="company-error" class="form-error"></span></div>

                                    <div class="form-item gutter-right">
                                        <label class="required" for="fax">Fax </label>
                                        <!--
                   <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="fax[0]" value="<?php echo getPhoneField($address['fax'], 0); ?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="fax[1]" value="<?php echo getPhoneField($address['fax'], 1); ?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="fax[2]" value="<?php echo getPhoneField($address['fax'], 2); ?>" >-->

                                        <input class="textbox " type="text" name="fax"
                                               value="<?php echo cleanPhone($address['fax']); ?>">
                                        <span id="fax-error" class="form-error"></span></div>
                                </div>

                                <!--ROW 3-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required">Telephone <em>*</em> </label>

                                        <input class="textbox mobile-m" type="tel" name="phone"
                                               value="<?php echo cleanPhone($address['phone']); ?>">
                                        <!--                  <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="phone[0]" value="<?php echo getPhoneField($address['phone'], 0); ?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="phone[1]" value="<?php echo getPhoneField($address['phone'], 1); ?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="phone[2]" value="<?php echo getPhoneField($address['phone'], 2); ?>"> -->
                                        <span id="phone-error" class="form-error"></span></div>
                                    <div class="form-item gutter-right">
                                        <label class="required" for="fax">Mobile Number</label>
                                        <input class="textbox mobile-m" type="tel" name="mobile"
                                               value="<?php echo cleanPhone($address['mobile']); ?>">
                                        <!--            <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="mobile[0]" value="<?php echo getPhoneField($address['mobile'], 0); ?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="mobile[1]" value="<?php echo getPhoneField($address['mobile'], 1); ?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="mobile[2]" value="<?php echo getPhoneField($address['mobile'], 2); ?>" >-->
                                        <span id="mobile_number-error" class="form-error"></span></div>
                                </div>


                            </div>
                        </div>
                        <div class="box-account box-info address_info">
                            <div class="box-head">
                                <h2>Address</h2>
                            </div>

                            <!--ROW 4-->
                            <div class="row_form">
                                <div class="form-item gutter-right">
                                    <label class="required">Street Address <em>*</em> </label>
                                    <input class="textbox" type="text" name="street_one"
                                           value="<?php echo $address['street_one']; ?>">
                                    <span id="street_one-error" class="form-error"></span>
                                    <input class="textbox gutter-top" type="text" name="street_two"
                                           value="<?php echo $address['street_two']; ?>"></div>
                            </div>

                            <!--ROW 5-->
                            <div class="row_form">
                                <div class="form-item gutter-right">
                                    <label class="required"> City <em>*</em> </label>
                                    <input class="textbox" type="text" name="city"
                                           value="<?php echo $address['city']; ?>">
                                    <span id="city-error" class="form-error"></span></div>
                                <div class="form-item gutter-right">
                                    <label class="required"> State/Province </label>
                                    <input class="textbox" type="text" name="state"
                                           value="<?php echo $address['state']; ?>">
                                    <span id="state-error" class="form-error"></span></div>
                            </div>


                            <!--ROW 6-->
                            <div class="row_form">
                                <div class="form-item gutter-right">
                                    <label class="required"> Zip/Postal Code </label>
                                    <input class="textbox" type="text" name="postal_code"
                                           value="<?php echo $address['postal_code']; ?>">
                                    <span id="postal_code-error" class="form-error"></span></div>
                                <div class="form-item gutter-right">
                                    <label class="required">Country <em>*</em> </label>
                                    <select id="country" class="textbox selectbox" name="id_countries" title="Country">
                                        <option value="">--Select Country--</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id_countries']; ?>" <?php if ($country['id_countries'] == $address['id_countries']) echo " selected"; ?>><?php echo $country['title']; ?></option>
                                        <?php } ?>

                                    </select>
                                    <span id="id_countries-error" class="form-error"></span></div>
                            </div>

                            <!--ROW 7-->
                            <div class="row_form gutter-bottom-non" style="display:none;">
                                <div class="form-item mod">
                                    <input class="checkbox validation-passed"
                                           type="checkbox" <?php if ($formType == "billing") echo " checked"; ?>
                                           title="Use as billing address" name="billing">
                                    <label>Use as my default billing address</label>
                                    <span id="billing-error" class="form-error"></span></div>
                            </div>

                            <!--ROW 8-->
                            <div class="row_form" style="display:none">
                                <div class="form-item mod">
                                    <input class="checkbox validation-passed"
                                           type="checkbox" <?php if ($formType == "shipping") echo " checked"; ?>
                                           title="Use as shipping address" name="shipping">
                                    <label>Use as my default shipping address</label>
                                    <span id="shipping-error" class="form-error"></span></div>
                            </div>

                            <!--ROW 9-->
                            <!--<div class="row_form" >
<div class="form-item gutter-right">
          <label class="required" for="password">Please type the letters below <em>*</em> </label>
          <span class="captchaImage"><?php echo $this->fct->createNewCaptcha(); ?></span>
		  <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" />
		<span id="captcha-error" class="form-error"></span> </div>               
</div>-->

                            <label class="lbl_required mod">* Required Fields</label>
                            <div class="buttons-set">
                                <div id="loader-bx"></div>
                                <div class="FormResult"></div>
                                <input class="btn" type="submit" value="Save" title="Save" style="float:right;">
                            </div>
                        </div>

                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<script>
    if ($("#checkOutAddressForm").length > 0)
        $("#checkOutAddressForm").dowformvalidate();

    if ($('.mobile-m').length > 0) {
        var baseurl = $('#baseurl').val();
        $(".mobile-m").intlTelInput({
            utilsScript: baseurl + "front/lib/libphonenumber/build/utils.js", defaultCountry: "auto",
        });
    }
</script>



