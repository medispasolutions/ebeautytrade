<?php


	$formAction = site_url('user/updatenewsletter');


?>

<div class="innerContent account-create">
  <div class="centered">
    <div class="row">
      
      <div class="col-main grid12-9 grid-col2-main no-gutter dashboard">
             <form id="NewsletterForm" method="post" accept-charset="utf-8" action="<?php echo $formAction ;?>" >
        <div class="my-account ">
          <div class="page-title">
            <h1>Newsletter Subscription</h1>
          </div>
          <div class="box-account box-info">
            <div class="box-head">
              <h2>Newsletter Subscription</h2>
            </div>
          
 
              <!--ROW 5-->
              <div class="row_form">
                <div class="form-item mod">
                  <input  class="checkbox validation-passed" type="checkbox"  <?php if(!empty($userData['newsletter'])) echo " checked";?> value="1" title="General Subscription" name="is_subscribed">
                  <label>General Subscription</label>
                  <span id="is_subscribed-error" class="form-error"></span></div>
              </div>
              
        <div class="row_form">
<div class="form-item">
          <label class="required" for="password">Please type the letters below <em>*</em> </label>
          <span class="captchaImage"><?php echo $this->fct->createNewCaptcha(); ?></span>
		  <input type="text"  class="input-text" id="captcha" name="captcha" value="" style="text-transform:none" />
		<span id="captcha-error" class="form-error"></span> </div>               
</div>
       
          </div>
        </div>
        
        
            <label class="lbl_required mod"></label>
            <div class="buttons-set">
              
              <div class="FormResult"> </div>
               <div id="loader-bx"></div>
              <input class="button" title="Create an Account" type="submit" value="Save">
             
              <a class="back-link" href="<?php echo route_to('user');?>" > <small>« </small> Back</a> </div>
        </form>
      </div>
      <div class="col-left sidebar grid12-3 grid-col2-sidebar">
        <?php $this->load->view('blocks/account');?>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
