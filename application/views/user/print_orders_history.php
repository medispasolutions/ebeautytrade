<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title>Orders History</title>
	<meta charset="utf-8">
    <!-- Favicon --> 
	<link rel="shortcut icon" href="<?php echo base_url(); ?>front/images/favicon.ico">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>front/css/style.css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-1.10.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.PrintArea.js"></script>
	<script type="text/javascript">
    	function calculateIDPos(){
			var WindowWidth = $(window).width();
			var RightPos = ((WindowWidth - 600) / 2) - 80;
			$('#printButton').css('right',RightPos); 
		}
		$(document).ready(function(){
			calculateIDPos();
			$('#printButton').click(function(){
				$('#printThis').printArea();
			});
		});
		$(window).bind("load",function(){
			calculateIDPos();
		});
    </script>
   
</head>
<body>
<input type="submit" style="position:absolute;" class="printButton btn button" id="printButton" value="<?php echo lang("print"); ?>" />
<div class="printing-container" id="printThis" style="width:1140px; margin:0 auto">
<?php 
	$section = 'checkout';
?>

<h1 class="h1_title">Orders History</h1>

<table border="1" cellpadding="0" cellspacing="0" class="shopping_cart_table">
<tr>
    <th style="width:115px;text-align:center;"><?php echo lang('order_id'); ?></th>
    <th>Date Added</th>
    <th style="width:115px;text-align:center;">Count Items</th>
    <th>Total</th>
    <th style="text-align:center;"><?php echo lang('status'); ?></th>
    
    
</tr>
<?php if(!empty($user_orders)) {?>
<?php foreach($user_orders as $order) {
/*	echo "<pre>";
	print_r($order);exit;*/
$order_details=$this->fct->getOrder($order['id_orders']);
$line_items = $order_details['line_items'];
$count_items=count($line_items);
$i=0;
$item_title="";
$item_cod="";
foreach($line_items as $pro){$i++;
if($i==1){
	$cm="";}else{$cm=",";}
$item_cod=$item_cod.$cm.$pro['product']['sku'];
$item_title=$item_title.$cm.$pro['product']['title'];
	}


$created_date=date("j-F-Y",strtotime($order['created_date']));  


?>
<tr>
    <td style="text-align:center;"><?php echo $order['id_orders']; ?></td>
    <td><?php echo $created_date; ?></td>
    <td style="text-align:center"><?php echo $count_items; ?></td>
    <td><?php echo changeCurrency($order['amount']); ?></td>
    <td style="text-align:center;">
	<?php 
	$status=$order['status'];
	if($status=="pending") {
	echo '<span class="status-lbl pending">pending</span>';	
		} ?>
        
    <?php if($status=="paid") {
	echo '<span class="status-lbl paid">'.lang($status).'</span>';	
		} ?> 
          
    <?php if($status=="completed") {
	echo '<span class="status-lbl completed">'.lang($status).'</span>';	
		} ?>  
    
    <?php if($status=="canceled") {
	echo '<span class="status-lbl canceled">'.lang($status).'</span>';	
		} ?>         
	</td>
    
</tr>
<?php }?>
<?php } else {?>
<tr>
<td colspan="5" align="center">
<?php echo lang('no_orders'); ?>
</td>
</tr>
<?php }?>
</table>
</div>
</body>
</html>















