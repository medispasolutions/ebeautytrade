<div class="row">
  <div class="tabs_cont mod">
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <div class="tab_container dashboard my_credits">
      <div  class="tab_content">
        <div class="my-account">
        <div class="row add_new_address">
<a class="btn" type="button" title="Add New Address" href="<?php echo route_to('user/add_credits');?>">Add to My Wallet</a>
</div>
          <div class="box-account box-info">
            <div class="col2-set">
              <div class="box">
                <div class="box-content">
                  <div class="col-md-3">
                 <div class="available_balance">
                 <h3>Available Balance</h3>
                 <?php $currencies_arr=array(); ?>
                <?php if(!empty($currencies)){?>
                 <?php foreach($currencies as $currency){
					if(!in_array($currency['currency'],$currencies_arr)) {
					array_push($currencies_arr,$currency['currency']); ?>
<label><?php echo changeCurrency($userData['available_balance'],true,$currency['currency'],$currency['default_currency']); ?></label>
				 <?php } }?>
                 <?php } else{?>
                 0 <?php echo $this->config->item('default_currency');?>
                 <?php } ?>
                 </div>
                  </div>
                  <div class="col-md-9">
                    <div class="row">
            <div class="shopping_cart_cont">
             <div class="table-b">
            <div class="n-table">
              <table border="1" cellpadding="0" cellspacing="0" class="shopping_cart_table">
                <tr>
                <th>Order Id</th>
                  <th>Date Added</th>
                  <th>Currency</th>
         			<th style="width:70px;text-align:center;">Amount</th>
                </tr>
                 <?php 
		if(!empty($credits)) {
		foreach($credits as $val){$sgn="";
		list($id,$table,$text) = explode('s:',$val['id']);
			switch($table) {
	
						case 'credits':
							$sgn = "+";
							break;
						case 'orders':
							$sgn = "-";
							break;
		
				}
				;?>
        <tr class="from_<?php echo $table;?> credits_item">
       			   <td>
                   <?php if($table=="orders"){?>
                     <a href="<?php echo route_to('user/orders/'.$id.'/'.$val['rand']);?>">#<?php echo $id;?></a>
                     <?php }else{ ?>-<?php }?></td>
                  <td><?php echo  date("F d,Y g:i a", strtotime($val['created_date'])); ?></td>
                  <td><?php echo  $val['currency']; ?></td>
         			<td style="width:70px;text-align:center;"><span class="amount_credits">
					<?php if( $val['amount']>0){?>
					<?php echo $sgn;?>
                    <?php } ?> <?php echo  $val['amount']; ?></span></td>
                </tr>
        <?php }}else {?>
        <tr><td colspan="6"><div class="empty">No credits...</div></td></tr>
        
        <?php }?>
                </table>
                </div>   </div>
            </div>
            
          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<ul id="pagination" style="display:none;">
  <?php for($i=0;$i<$count;$i=$i+$show_items){
	 
		if(isset($_GET['per_page'])) {
		 $next=$_GET['per_page']+$show_items;
		  }else{
		$next=$offset+$show_items; }
	?>
  <li class="<?php if($i==$next) echo "next";?>"  ><a href="<?php echo route_to('user/credits?pagination=on&&per_page='.$i);?>"><?php echo  $next;?></a></li>
  <?php } ?>
</ul>
