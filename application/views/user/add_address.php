<?php
if (isset($address) && !empty($address)) {
    $buttonLabel = lang('update_profile');
    $formAction = site_url('user/submitAddress');
    $pTitle = lang('user_account_update_profile');
    $pStar = '';
    $formID = 'updateAddress';
    $menuBlock = "user/account_menu";
    $update = true;
    $title = 'Edit Address';
} else {
    $update = false;
    $address['first_name'] = $userData['first_name'];
    $address['last_name'] = $userData['last_name'];
    $address['company'] = '';
    $address['phone'] = '';
    $address['mobile'] = '';
    $address['fax'] = '';
    $address['street_one'] = '';
    $address['street_two'] = '';
    $address['state'] = '';
    $address['city'] = '';
    $address['country'] = '';
    $address['id_countries'] = '';
    $address['postal_code'] = '';
    $address['billing'] = '';
    $address['shipping'] = '';

    $buttonLabel = lang('register');
    $pTitle = lang('create_an_account');
    $formAction = route_to('user/submitAddress');
    $pStar = ' *';
    $formID = 'newAddressForm';
    $menuBlock = "user/user_menu";
    $title = 'Add New Address';
}
?>

<div class="row account-create add_address">
    <div class="tabs_cont mod">
        <?php $this->load->view('blocks/dashboard_tabs'); ?>
        <div class="tab_container dashboard ">
            <div class="tab_content">
                <div class="col-main grid12-9 grid-col2-main no-gutter dashboard">
                    <form id="<?php echo $formID; ?>" method="post" accept-charset="utf-8"
                          action="<?php echo $formAction; ?>">
                        <?php
                        if (isset($address['id_users_addresses']) && !empty($address['id_users_addresses'])) { ?>
                            <input class="textbox" type="hidden" name="id_users_addresses"
                                   value="<?php echo $address['id_users_addresses']; ?>">
                        <?php } ?>
                        <div class="my-account ">
                            <div class="add_new_address">
                                <h2>Address Book(<?php echo $title; ?>)</h2>
                                <a class="back-link" href="<?php echo route_to('user/addresses'); ?>">
                                    <i class="fa fa-fw"></i>
                                    Back
                                </a>
                            </div>
                            <div class="box-account box-info">
                                <div class="box-head">
                                    <h2>Contact Information</h2>
                                </div>
                                <!--ROW 1-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required"> First Name <em>*</em> </label>
                                        <input class="textbox" type="text" name="first_name"
                                               value="<?php echo $address['first_name']; ?>">
                                        <span id="first_name-error" class="form-error"></span></div>
                                    <div class="form-item gutter-right">
                                        <label class="required"> Last Name <em>*</em> </label>
                                        <input class="textbox" type="text" name="last_name"
                                               value="<?php echo $address['last_name']; ?>">
                                        <span id="last_name-error" class="form-error"></span>
                                    </div>
                                </div>
                                <!--ROW 2-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required">Company </label>
                                        <input class="textbox" type="text" name="company"
                                               value="<?php echo $address['company']; ?>">
                                        <span id="company-error" class="form-error"></span></div>

                                    <div class="form-item gutter-right">
                                        <label class="required" for="fax">Fax </label>
                                        <input class="textbox" type="text" name="fax"
                                               value="<?php echo $address['fax']; ?>">
                                        <span id="fax-error" class="form-error"></span>
                                    </div>
                                </div>
                                <!--ROW 3-->
                                <div class="row_form">
                                    <div class="form-item gutter-right">
                                        <label class="required">Telephone <em>*</em> </label>
                                        <input class="textbox mobile-m" type="tel" name="phone"
                                               value="<?php echo cleanPhone($address['phone']); ?>">
                                        <span id="phone-error" class="form-error"></span></div>
                                    <div class="form-item gutter-right">
                                        <label class="required" for="fax">Mobile Number</label>
                                        <input class="textbox mobile-m" type="tel" name="mobile"
                                               value="<?php echo cleanPhone($address['mobile']); ?>">
                                        <span id="mobile_number-error" class="form-error"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-account box-info address_info">
                            <div class="box-head">
                                <h2>Address</h2>
                            </div>
                            <!--ROW 4-->
                            <div class="row_form">
                                <div class="form-item gutter-right">
                                    <label class="required">Street Address <em>*</em> </label>
                                    <input class="textbox" type="text" name="street_one"
                                           value="<?php echo $address['street_one']; ?>">
                                    <span id="street_one-error" class="form-error"></span>
                                    <input class="textbox gutter-top" type="text" name="street_two"
                                           value="<?php echo $address['street_two']; ?>">
                                </div>
                            </div>
                            <!--ROW 5-->
                            <div class="row_form">
                                <div class="form-item gutter-right">
                                    <label class="required"> City <em>*</em> </label>
                                    <input class="textbox" type="text" name="city"
                                           value="<?php echo $address['city']; ?>">
                                    <span id="city-error" class="form-error"></span></div>
                                <div class="form-item gutter-right">
                                    <label class="required"> State/Province </label>
                                    <input class="textbox" type="text" name="state"
                                           value="<?php echo $address['state']; ?>">
                                    <span id="state-error" class="form-error"></span>
                                </div>
                            </div>
                            <!--ROW 6-->
                            <div class="row_form">
                                <div class="form-item gutter-right">
                                    <label class="required"> Zip/Postal Code </label>
                                    <input class="textbox" type="text" name="postal_code"
                                           value="<?php echo $address['postal_code']; ?>">
                                    <span id="postal_code-error" class="form-error"></span></div>
                                <div class="form-item gutter-right">
                                    <label class="required">Country <em>*</em> </label>
                                    <select id="country" class="textbox selectbox" name="id_countries" title="Country">
                                        <option value="">--Select Country--</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id_countries']; ?>" <?php if ($country['id_countries'] == $address['id_countries']) echo " selected"; ?>><?php echo $country['title']; ?></option>
                                        <?php } ?>

                                    </select>
                                    <span id="id_countries-error" class="form-error"></span>
                                </div>
                            </div>
                            <!--ROW 7-->
                            <div class="row_form gutter-bottom-non">
                                <div class="form-item mod">
                                    <input class="checkbox validation-passed"
                                           type="checkbox" <?php if ($address['billing'] == "1") echo " checked"; ?>
                                           title="Use as billing address" name="billing">
                                    <label>Use as my default billing address</label>
                                    <span id="billing-error" class="form-error"></span>
                                </div>
                            </div>
                            <!--ROW 8-->
                            <div class="row_form">
                                <div class="form-item mod">
                                    <input class="checkbox validation-passed"
                                           type="checkbox" <?php if ($address['shipping'] == "1") echo " checked"; ?>
                                           title="Use as shipping address" name="shipping">
                                    <label>Use as my default shipping address</label>
                                    <span id="shipping-error" class="form-error"></span>
                                </div>
                            </div>

                            <!--ROW 9-->
                            <!--<div class="row_form" >
                                <div class="form-item gutter-right">
                                   <label class="required" for="password">Please type the letters below <em>*</em> </label>
                                   <span class="captchaImage"><?php echo $this->fct->createNewCaptcha(); ?></span>
                                   <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" />
                                   <span id="captcha-error" class="form-error"></span> </div>               
                            </div>-->
                        </div>
                        <label class="lbl_required mod">* Required Fields</label>
                        <div class="buttons-set">
                            <div id="loader-bx"></div>
                            <div class="FormResult"></div>
                            <input class="btn" style="float: right;" type="submit" value="<?php echo lang('submit_information'); ?>"
                                   title="Create an Account">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>