<div class="innerContent">
<div class="centered">
 <div class="page-title category-title">
<h1><?php echo lang('compare_products'); ?></h1>
</div>

<div class="shopping_cart_cont compare_products_table">
<table border="0" cellpadding="0" cellspacing="0" style="background: #efefef none repeat scroll 0 0;
    float: left;
    width: 100%;">
<?php if(!empty($user_compare_products)) {?>
<tr>
<?php 

$th_width=100/count($user_compare_products);
foreach($user_compare_products as $pro) {
	?>
    <th style="   background: black;
    color: #fff;
    width:<?php echo $th_width.'%' ?>;
    border:none;
    font-size: 14px;
    font-weight: 700;
    padding: 16px 19px;
    border-right:1px solid #efefef;
    text-transform: uppercase;
    font-family: Oxygen,sans-serif;"><a style="color:white;" href="<?php echo route_to('products/details/'.$pro['title_url'.getUrlFieldLanguage()]); ?>"><?php echo $pro['title'.getFieldLanguage()]; ?></a></th>
    <?php }?>
</tr>
<tr>
<?php 
$i=0;
foreach($user_compare_products as $pro) {
	$i++;

/*	echo "<pre>";
	print_r($pro);exit;*/
	?>
    <td style="background: #fff none repeat scroll 0 0;
    text-align:center;
    border:none;
    border-right:1px solid #efefef;
    vertical-align: middle;
    ">
	<div style="
    text-align: center;
    ">
	<?php if(!empty($pro['gallery'])) {?>
        	<a href="<?php echo route_to('products/details/'.$pro['title_url'.getUrlFieldLanguage()]); ?>">
            <img align="top"  src="<?php echo base_url(); ?>uploads/products/gallery/500x500/<?php echo $pro['gallery'][0]['image']; ?>" title="<?php echo $pro['title'.getFieldLanguage()]; ?>" alt="<?php echo $pro['title'.getFieldLanguage()]; ?>" />
            </a>
        <?php }else{?>
           	<a href="<?php echo route_to('products/details/'.$pro['title_url'.getUrlFieldLanguage()]); ?>">
            <img align="top" style="max-width:150px;"   src="<?php echo base_url(); ?>front/img/product.jpg" title="<?php echo $pro['title'.getFieldLanguage()]; ?>" alt="<?php echo $pro['title'.getFieldLanguage()]; ?>" />
            </a>
        <?php } ?>
        </div></td>
 <?php }?>
</tr>
<?php if($this->session->userdata('login_id') != '') {?>
<tr>
<?php 
foreach($user_compare_products as $pro) {

	?>
    <td style=" color: #707070;
    font-size: 13px;
    font-weight: 400;
    border:none;
    border-right:1px solid #efefef;
    padding: 16px 19px; ">
<?php 
$arra_p=array();
$arr_p['id_products']=$pro['id_products'];
if($this->session->userdata('account_type')=="wholesale"){
$arr_p['list_price']=$pro['wholesale_price'];
}else{
$arr_p['list_price']=$pro['list_price'];
	}
$arr_p['price']=$pro['price'];
$arr_p['qty']="";
$arr_p['discount_product']=$pro['discount'];
$arr_p['discount_product_expiration']=$pro['discount_expiration'];
$arr_p['withSegment']="";
	 $prices=$this->ecommerce_model->getPrices($arr_p);
	 $n_price=$prices['n_price'];
	 $o_price=$prices['o_price'];
?>
<?php
$size="";
$color="";
 if(isset($pro['options']) && !empty($pro['options'])) {
$options = explode ( ',' , $pro['options']);
$size=$this->ecommerce_model->getAttributeOption(1,$options,$pro['id_products']); 
$color=$this->ecommerce_model->getAttributeOption(2,$options,$pro['id_products']);  }else{
		$options="";
		}

	 ?>


<div class="row"><span style="color:black;margin-right:5px;float:left;margin-bottom:10px;">Size: </span>
 <?php  if(!empty($size)){ echo $size['title']; }else{ echo "No";} ?></div>


<div class="row"><span style="color:black;margin-top:4px;float:left;margin-right:5px;margin-bottom:10px;">Color: </span> <?php  if(!empty($color)){ echo $color['title']; }else{ echo "No";} ?>
</div>




<span style="color:black;margin-right:5px;">Price: </span>
<?php if(!empty($o_price)) { ?>
<span class="old_price">  <?php echo changeCurrency($o_price);?> </span>
<?php } ?>
<?php echo changeCurrency($n_price);?>
	</td>
    <?php }?>
</tr>
<?php } else {?>
<tr><td colspan="4"> <a class="r-Button more" style="width:98%" href="<?php echo route_to('user/login'); ?>">To view price: <?php echo lang('login_register'); ?></a></td></tr>
<?php }?>
<tr>
<?php 
foreach($user_compare_products as $pro) {
	?>
    <td style="   background: #fff none repeat scroll 0 0;
    color: #707070;
    font-size: 13px;
    font-weight: 400;
    border:none;
    border-right:1px solid #efefef;
    padding: 16px 19px;
    font-family: Open Sans;">
     <?php if(!empty($pro['washing_instructions'])) { ?>
	<span style="display: block; padding: 8px 0;">Description:</span>
	<?php echo $pro['washing_instructions']; ?>
    <span 
    style="max-height: 110px;
    overflow: hidden;
    padding: 8px 0;
    display: block;"></span>
    <?php } ?> </td>
    <?php }?>
</tr>
<tr>
<?php 
foreach($user_compare_products as $pro) {
	?>
    <td style=" background: #f5f5f5 none repeat scroll 0 0;
 	border:none;
    padding: 6px 19px;
    border-right:1px solid #efefef;
    width: calc(100% - 38px);"><a href="<?php echo route_to('user/removecompareProducts/'.$pro['id_products']); ?>"><img style="width:35px;" src="<?php echo base_url(); ?>front/img/red-x.png" /></a></td>
    <?php }?>
</tr>
<?php } else {?>
<tr>
<td colspan="5" align="center" style="padding:35px 0;">
<?php echo lang('no_compare'); ?>
</td>
</tr>
<?php }?>
</table>
</div>


<?php if(!empty($user_compare_products)) {?>
<div class="btns_cont">
<!--<a class="btn fr" href="<?php echo route_to('products/export_to_pdf');?>">export to pdf</a>-->

<a class="btn button fr"  href="<?php echo route_to('products');?>">Add More</a>
</div>
<?php } ?>
</div></div>
