<script>
function delete_address(id){
window.location="<?= route_to('user/deleteAddress'); ?>/"+id;
return false;
}</script>



<div class="row">
  <div class="tabs_cont mod">

  <?php $this->load->view('blocks/dashboard_tabs');?>
  
  <div class="tab_container dashboard">
    <div  class="tab_content">
    <div class="my-account dashboard">
          <div class="row add_new_address"><a class="btn" href="<?php echo route_to('user/address');?>" title="Add New Address" type="button">Add New Address</a></div>
          <div class="col2-set addresses-list">
            <div class="col-1 addresses-primary">
              <h2>Default Addresses</h2>
              <ol>
              
              <?php 
			  
			  
			$bool=true;
			  $i=0;foreach($default_addresses as $default_address){$i++;
			  if(!empty($default_address)){
				  $bool=false;
				  if($default_address['billing']==1 && $i==1){
					  $default_billing_address='Default Billing Address';
					  $change_billing_address='Change Billing Address';
					  }else{
					  $default_billing_address='Default Shipping Address';
					  $change_billing_address='Change Shipping Address';  }?>
                <li class="item">
                  <h3><?php echo $default_billing_address;?></h3>
                  <address>
                  <?php echo $default_address['name'];?><br>
                   <?php if(!empty($default_address['company']) && $default_address['company']!=0){?>
                  <?php echo $default_address['company'];?> <br>
                  <?php }?>
                  <?php echo $default_address['street_one'];?> <br>
                        <?php if(!empty($default_address['street_two']) && $default_address['street_two']!=0){?>
                  <?php echo $default_address['street_two'];?> <br>
                    <?php }?>
                  <?php echo $default_address['city'];?>
                  <?php if(!empty($default_address['state']) && $default_address['state']!=0) echo ",".$default_address['state'];?>
                   <?php if(!empty($default_address['postal_code']) && $default_address['postal_code']!=0) echo ",".$default_address['postal_code'].'';?>
                  <br> <?php echo $default_address['countries_title'];?> <br>
                  T: <?php echo $default_address['phone'];?> 
                  <?php if(!empty($default_address['fax'])) echo "<br>F: ".$default_address['state']?>
                  <?php if(!empty($default_address['mobile'])) echo "<br>M: ".$default_address['mobile']?>
                  </address>
                  <p> <a href="<?php echo route_to('user/address/'.$default_address['id_users_addresses']);?>"><?php echo $change_billing_address;?></a> </p>
                </li>
                <?php }}
				if($bool){?>
                <li class="item empty">
<p>You have no additional address entries in your address book.</p>
</li>
				<?php } ?>
              </ol>
            </div>
            
            <div class="col-2 addresses-additional">
<h2>Additional Address Entries</h2>
<ol>
<?php if(!empty($additional_addresses)){
?>
<?php foreach($additional_addresses as $val){?>
<li class="item">
<?php echo $val['name'];?><br>
  <?php if(!empty($val['company']) && $val['company']!=0){?>
<?php echo $val['company'];?> <br>
<?php } ?>
<?php echo $val['street_one'];?> <br>
  <?php if(!empty($val['street_two']) && $val['street_two']!=0){?>
<?php echo $val['street_two'];?> <br>
<?php } ?>
<?php echo $val['city'];?>
<?php if(!empty($val['state'])  && $val['state']!=0) echo ",".$val['state'];?>
<?php if(!empty($val['postal_code'])  && $val['postal_code']!=0) echo ",".$val['postal_code'].'';?>
<br> <?php echo $val['countries_title'];?> <br>
 T: <?php echo $val['phone'];?> 
 <?php if(!empty($val['fax'])) echo "<br>F: ".$val['state']?>
 <?php if(!empty($val['mobile'])) echo "<br>M:".$val['mobile']?>
 
 <p>
<a href="<?php echo route_to('user/address/'.$val['id_users_addresses']);?>">Edit Address</a>
<span class="separator">|</span>
<a class="link-remove" onclick="if(confirm('Are you sure you want delete this Address ?')){ delete_address('<?=$val["id_users_addresses"];?>'); }" >Delete Address</a>
</p>
</li>
<?php } ?>
<?php }else{ ?>
<li class="item empty">
<p>You have no additional address entries in your address book.</p>
</li>	
<?php } ?>  
</ol>
</div>
          </div>
          
          
          
          
        </div>
    </div>
    

  </div>
</div>
</div>

