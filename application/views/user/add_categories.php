<?php


if(isset($category) && !empty($category)) {
	$buttonLabel = lang('update');
	$formAction = site_url('user/submitCategory');
	$pTitle = lang('user_account_update_profile');
	$pStar = '';
	$formID = 'updatePopUpForm';
	$menuBlock = "user/account_menu";
	$update=true;
	$title='Edit Category';
}
else {
	$update=false;

	$category['title'] = '';

	
	$buttonLabel = lang('submit');
	$pTitle = lang('create_an_account');
	$formAction = route_to('user/submitCategory');
	$pStar = ' *';
	$formID = 'updatePopUpForm';
	$menuBlock = "user/user_menu";
	$title='Add New Category';
}


?>
<div class="innerContent">
<input type="hidden" id="popup_cls" value="add_category" />
<div class="page-title border"><h1><?php echo $title;?> </h1><!--<a class="back-link"  onclick="javascript:history.go(-1)" > <i class="fa fa-fw"></i> Back </a>--></div>
<?php if(isset($category['id_checklist_categories']) && !empty($category['id_checklist_categories'])) {?>
<!--<a class="btn fr" type="button" title="Selected Products"  href="<?php echo route_to('user/checklist_categories/'.$category['id_checklist_categories']); ?>">Selected Products</a>-->
<?php } ?>


          <form id="<?php echo $formID;?>" method="post" accept-charset="utf-8" action="<?php echo $formAction ;?>" >
       
            <?php
if(isset($category['id_checklist_categories']) && !empty($category['id_checklist_categories'])) {?>
            <input class="textbox" type="hidden" name="id" value="<?php echo $category['id_checklist_categories'];?>" >
            <?php } ?>

            <div class="my-account ">

              <!--ROW 1-->
              <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Name <em>*</em></label>
                  <input class="textbox" type="text" name="title" value="<?php echo $category['title'];?>" >
                  <span id="title-error" class="form-error"></span> </div>
                
              </div>
              
  
              <!--<label class="lbl_required mod">* Required Fields</label>-->
              <div class="buttons-set">
                <div id="loader-bx"></div>
                <div class="FormResult"> </div>
                <input class="btn" type="submit" value="Save" title="Save">
              </div>
            </div>
          </form>
          </div>
