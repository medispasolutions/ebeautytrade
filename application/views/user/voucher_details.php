<?php 
$data['voucher']=$voucher;
?>

<div class="row">
  <div class="tabs_cont mod">
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <div class="tab_container dashboard my_vouchers">
      <div  class="tab_content">
        <div class="my-account">
        <div class="row ta-r" style="margin-bottom:30px;">
<a class="btn" target="_blank" href="<?php echo route_to('vouchers/printVoucher/'.$voucher['id_users_vouchers'].'/'.$voucher['rand']);?>"> <i class="fa fa-fw"></i> Print Voucher</a>
<a class="btn"href="<?php echo route_to('user/vouchers');?>"><i class="fa fa-fw"></i> Back</a></div>
 <?php $this->load->view('blocks/voucher_details',$data);?>
        </div>
      </div>
    </div>
  </div>
</div>

