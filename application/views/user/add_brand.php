<?php

$size_arr_file=getMaxSize('file');
$size_arr_image=getMaxSize('image');
if(isset($brand) && !empty($brand)) {
	$buttonLabel = lang('update');
	$formAction = site_url('user/submitBrand');
	$pTitle = lang('user_account_update_profile');
	$pStar = '';
	$formID = 'updateBrand';
	$menuBlock = "user/account_menu";
	$update=true;
	$title='Edit Brand';
}
else {
	$update=false;

	$brand['title'] = '';
	$brand['logo'] = '';
	$brand['overview'] = '';
	$brand['access_code'] = '';
    $brand['image'] = '';
	$brand['catalog_link'] = '';
	$brand['catalog'] = '';
	$brand['overview'] = '';
	$brand['url'] = '';
	$brand['video'] = '';
	$brand['access_Code'] = '';
	$brand['name'] = '';
	$brand['email'] = '';
	$brand['photo'] = '';
	$brand['mobile'] = '';
	$brand['country_code'] = '';
	$brand['city_code'] = '';
	$brand['number'] = '';
	$brand['position'] = '';
	$brand['slide_1'] = '';
	$brand['slide_2'] = '';
	$brand['slide_3'] = '';
	$brand['slide_4'] = '';
	
	$brand['phone'] = '';
	
	$buttonLabel = lang('submit');
	$pTitle = lang('create_an_account');
	$formAction = route_to('user/submitBrand');
	$pStar = ' *';
	$formID = 'brandForm';
	$menuBlock = "user/user_menu";
	$title='Add New Brand';
}


?>

<div class="row account-create add_address">
  <div class="tabs_cont mod">
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <div class="tab_container dashboard ">
      <div  class="tab_content">
        <div class="col-main grid12-9 grid-col2-main no-gutter dashboard">
          <form id="<?php echo $formID;?>" method="post" accept-charset="utf-8" action="<?php echo $formAction ;?>" >
       
            <?php
if(isset($brand['id_brands']) && !empty($brand['id_brands'])) {?>
            <input class="textbox" type="hidden" name="id_brands" value="<?php echo $brand['id_brands'];?>" >
            <?php } ?>
            <div class="my-account ">
              <div class="add_new_address">
                <h2><?php echo $title;?></h2>
                <a class="back-link"  href="<?php echo route_to('user/brands');?>" > <i class="fa fa-fw"></i> Back </a> </div>
              
              <!--ROW 1-->
              <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Name <em>*</em></label>
                  <input class="textbox" type="text" name="title" value="<?php echo $brand['title'];?>" >
                  <span id="title-error" class="form-error"></span> </div>
                <div class="form-item gutter-right req_c">
                  <label class="required" >Logo (Max Size <?php echo $size_arr_image['size_mega'] ;?>Mb)<em>*</em></label>
                  <input class="textbox input-file" type="file" name="logo" >
                   <input  type="hidden" name="logo_f" value="<?php echo $brand['logo'];?>" >
                  <?
if($brand['logo'] != ""){?>
                  <span id="logo_<?php echo $brand["id_brands"]; ?>"> <a class="popUpGallery" rel="prettyPhoto" href="<?= base_url(); ?>uploads/brands/<?= $brand['logo']; ?>" target="_blank"  > show Logo </a> &nbsp;&nbsp;&nbsp; <a class="cur" onclick='delete_file_m("brands","logo","<?= $brand['logo']; ?>",<?php echo $brand["id_brands"]; ?>)' > <img src="<?= base_url(); ?>images/delete.png"  /></a></span>
                  <? } else {  } ?>
                  <span id="logo_f-error" class="form-error"></span> </div>
              </div>
              
              <!--ROW 2-->
              <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Youtube Link</label>
                  <input class="textbox" type="text" name="video" value="<?php echo $brand['video'];?>" >
                  <span id="video-error" class="form-error"></span> </div>
                <div class="form-item gutter-right req_c">
                  <label class="required" >Catalog (Max Size <?php echo $size_arr_file['size_mega'] ;?>Mb)<em>*</em></label>
                  <input class="textbox input-file" type="file" name="catalog" >
                  <input  type="hidden" name="catalog_f" value="<?php echo $brand['catalog'];?>" >
                  <?
if($brand['catalog'] != ""){?>
                  <span id="catalog_<?php echo $brand["id_brands"]; ?>"> <a href="<?= base_url(); ?>uploads/brands/<?= $brand['catalog']; ?>" target="_blank"  > show catalog </a> &nbsp;&nbsp;&nbsp; <a class="cur" onclick='delete_file_m("brands","catalog","<?= $brand['catalog']; ?>",<?php echo $brand["id_brands"]; ?>)' > <img src="<?= base_url(); ?>images/delete.png"  /></a></span>
                  <? } else {  } ?>
                  <span id="catalog_f-error" class="form-error"></span> </div>
              </div>
              
               <!--ROW 3-->
              <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Access Code</label>
                  <input class="textbox" type="text" name="access_code" value="<?php echo $brand['access_code'];?>" >
                  <span id="access_code-error" class="form-error"></span> </div>
                  
                <div class="form-item gutter-right">
                  <label class="required" >Website Link <em>*</em></label>
                  <input class="textbox" type="text" name="url" value="<?php echo $brand['url'];?>" >
                  <span id="url-error" class="form-error"></span> </div>  
                
              </div>
              
              <!--ROW4-->
              <div class="row_form">
                <div class="form-item">
                  <label class="required">Overview  <small class="blue">(Max <?php echo getLimitText('overview');?> Characters)</small><em>*</em></label>
                  <textarea class="textarea"  onKeyDown="limitText(this.form.overview,0,<?php echo getLimitText('overview');?>);" onKeyUp="limitText(this.form.overview,0,<?php echo getLimitText('overview');?>);" name="overview"><?php echo $brand['overview'];?></textarea>
                  <span id="overview-error" class="form-error"></span> </div>
              </div>

               <!--Contact Person-->
              <div class="box-account box-info address_info">
                <div class="box-head">
                  <h2>Contact Person</h2>
                </div>
                
             <!--ROW 5-->
               <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Name <em>*</em></label>
                  <input class="textbox" type="text" name="name" value="<?php echo $brand['name'];?>" >
                  <span id="name-error" class="form-error"></span> </div>
                <div class="form-item gutter-right">
                  <label class="required" >Photo (Max Size <?php echo $size_arr_image['size_mega'] ;?>Mb)</label>
                  <input class="textbox input-file" type="file" name="photo" >
                  <input  type="hidden" name="photo_f" value="<?php echo $brand['photo'];?>" >
                  <?
if($brand['photo'] != ""){?>
                  <span id="photo_<?php echo $brand["id_brands"]; ?>"> <a class="popUpGallery" rel="prettyPhoto" href="<?= base_url(); ?>uploads/brands/<?= $brand['photo']; ?>" target="_blank"  > show photo </a> &nbsp;&nbsp;&nbsp; <a class="cur" onclick='delete_file_m("brands","photo","<?= $brand['photo']; ?>",<?php echo $brand["id_brands"]; ?>)' > <img src="<?= base_url(); ?>images/delete.png"  /></a></span>
                  <? } else {  } ?>
                  <span id="photo_f-error" class="form-error"></span> </div>
              </div>
              
              <!--ROW 6-->
              <div class="row_form">
                
                <div class="form-item">
                  <label class="required" >E-mail <em>*</em></label>
                  <input class="textbox" type="text" name="email" value="<?php echo $brand['email'];?>" >
                  <span id="email-error" class="form-error"></span> </div>
              </div>
              
             <!--ROW 7-->
             <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Position <em>*</em></label>
                  <input class="textbox" type="text" name="position" value="<?php echo $brand['position'];?>" >
                  <span id="position-error" class="form-error"></span> </div>
                <div class="form-item gutter-right">
               
                  <label class="required" >Phone <em>*</em></label>
                  <input class="textbox mobile-m" type="text" name="phone" value="<?php echo cleanPhone($brand['phone']);?>" >
                 <!-- <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="phone[0]" value="<?php echo getPhoneField($brand['phone'],0);?>" >
                  <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="phone[1]" value="<?php echo getPhoneField($brand['phone'],1);?>" >
                  <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="phone[2]" value="<?php echo getPhoneField($brand['phone'],2);?>" >-->
                  <span id="phone-error" class="form-error"></span> </div>
              </div>
              
              
              </div>
              
              <div class="box-head">
                <h2>Shop by brand sliders (Width:850px,Height:160px)</h2>
              </div>
              <div class="form-item gutter-right">
              <input class="textbox input-file" type="hidden" name="slide" >
              <span id="slide-error" class="form-error"></span></div>
              <!--ROW 8-->
              <div class="row_form">
                <?php for($i=1;$i<5;$i++){
				$image="slide_".$i; ?>
                <div class="form-item gutter-right req_c">
                  <label class="required" >Upload Slider <?php echo $i;?> (Max Size <?php echo $size_arr_image['size_mega'] ;?>Mb)</label>
                  <input class="textbox input-file" type="file" name="<?php echo $image;?>" >
                  <input class="textbox input-file" type="hidden" name="<?php echo $image.'_f';?>" value="<?php echo $brand[$image]; ?>" >
                  <?
if($brand[$image] != ""){?>
                  <span id="slide_<?php echo $i;?>_<?php echo $brand["id_brands"]; ?>"> <a class="popUpGallery" rel="prettyPhoto[slider]" href="<?= base_url(); ?>uploads/brands/<?= $brand[$image]; ?>" target="_blank"  > show file </a> &nbsp;&nbsp;&nbsp; <a class="cur" onclick='delete_file_m("brands","slide_<?php echo $i;?>","<?php echo $brand[$image]; ?>",<?php echo $brand["id_brands"]; ?>)' > <img src="<?= base_url(); ?>images/delete.png"  /></a></span>
                  <? } else {  } ?>
                  
                  <span id="<?php echo $image.'_f';?>-error" class="form-error"></span> </div>
                <?php  if($i%2==0 && $i!=4){?>
              </div>
              <div class="row_form">
                <?php }} ?>
              </div>
              
              <!--ROW 9-->
              <!--<div class="row_form" >
                <div class="form-item gutter-right">
                  <label class="required" for="password">Please type the letters below <em>*</em> </label>
                  <span class="captchaImage"><?php echo $this->fct->createNewCaptcha(); ?></span>
                  <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" />
                  <span id="captcha-error" class="form-error"></span> </div>
              </div>-->
              <label class="lbl_required mod">* Required Fields</label>
              <div class="buttons-set">
                <div id="loader-bx"></div>
                <div class="FormResult"> </div>
                <input class="btn" type="submit" value="<?php echo lang('submit_information');?>" title="Create an Account">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
