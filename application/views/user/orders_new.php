<div class="innerContent">
  <div class="centered">
    <div class="row">
    <div class="col-left sidebar grid12-3 grid-col2-sidebar no-gutter">
<?php $this->load->view('blocks/account');?>
    </div>
    
   
    <div class="col-main grid12-9 grid-col2-main no-gutter">
        <div class="my-account">
          <div class="page-title">
            <h1>My Orders</h1>
          </div>
          <p>You have placed no orders.</p>
        <div class="buttons-set">
<p class="back-link">
<a href="<?php echo route_to('user');?>"><small>« </small>
Back</a>
</p>
</div>
        </div>
      </div>
      
  </div>  
  </div>
</div>
