<div class="row account-create add_address">
  <div class="tabs_cont mod">

  <?php $this->load->view('blocks/dashboard_tabs');?>
  
  <div class="tab_container dashboard ">
    <div  class="tab_content">
<section class="innerContent">

   
        <div class="my-account dashboard">
          <div class="page-title">
        
            
            <!--<a class="link-print"  href="<?php echo route_to('user/print_orders_history');?>" target="_blank"><i class="fa fa-fw"></i> Print Order</a>--> </div>
          <div class="row">
            <div class="shopping_cart_cont">
              <div class="table-b">
            <div class="n-table">
              <table border="1" cellpadding="0" cellspacing="0" class="shopping_cart_table">
                <tr>
                  <th style="width:115px;text-align:center;"><?php echo lang('order_id'); ?></th>
                  <th>Date Added</th>
                  <th style="width:115px;text-align:center;">Count Items</th>
                  <th>Total</th>
                  <th style="text-align:center;"><?php echo lang('status'); ?></th>
                  <th style="width:70px;text-align:center;"><?php echo lang('order_operations'); ?></th>
                </tr>
                <?php if(!empty($user_orders)) {?>
                <?php foreach($user_orders as $order) {
/*	echo "<pre>";
	print_r($order);exit;*/
$order_details=$this->fct->getOrder($order['id_orders'],$order['rand']);
$line_items = $order_details['line_items'];
$count_items=count($line_items);
$i=0;
$item_title="";
$item_cod="";
$count_items=0;
foreach($line_items as $pro){$i++;
if($i==1){
	$cm="";}else{$cm=",";}
	
$item_cod=$item_cod.$cm.$pro['product']['sku'];
$item_title=$item_title.$cm.$pro['product']['title'];
$count_items=$count_items+$pro['quantity'];
	}
$created_date=date("F d,Y g:i A",strtotime($order['created_date']));?>

<?php if(isset($pro['product']['id_products'])){?>
                <tr>
                  <td style="text-align:center;"><?php echo $order['id_orders']; ?></td>
                  <td><?php echo $created_date; ?></td>
                  <td style="text-align:center"><?php echo $count_items; ?></td>
                  <td><?php echo  $order['amount_currency'].' '.$order['currency']; ?></td>
                  <td style="text-align:center;" class="status_order"><?php 
	$status=$order['status'];
	if($status=="pending") {
	echo '<span class="label-status label-pending">pending</span>';	
		} ?>
                    <?php if($status=="paid") {
	echo '<span class="label-status label-success">'.lang($status).'</span>';	
		} ?>
                    <?php if($status=="completed") {
	echo '<span class="label-status label-success">'.lang($status).'</span>';	
		} ?>
                    <?php if($status=="canceled") {
	echo '<span class="label-status label-important">'.lang($status).'</span>';	
		} ?></td>
                  <td style="text-align:center;"><a class="btn" href="<?php echo route_to('user/orders/'.$order['id_orders'].'/'.$order['rand']); ?>"> 
                    <!-- <img width="55px" src="<?php echo base_url();?>front/img/new-view-button-hi.png" />--> 
                    View</a></td>
                </tr>
                <?php } ?>
                <?php }?>
                <?php } else {?>
                <tr>
                  <td colspan="7" align="center"><?php echo lang('no_orders'); ?></td>
                </tr>
                <?php }?>
              </table>
              </div> </div>
            </div>
            
          </div>
        </div>
   

</section>
</div></div></div></div>
