
<div class="row account-create add_address">
  <div class="tabs_cont mod">
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <div class="tab_container dashboard ">
      <div  class="tab_content">
        <section class="innerContent">
          <div class="my-account dashboard">
            <div class="innerContent">
              <div class="miles">
                
                <div class="products-category-cont products-container">
                <?php if(!empty($vouchers)){?>
                  <?php foreach($vouchers as $voucher){ ?>
                  <div class="product">
                    <div class="inner-product">
                      <div class="miles_title"> <?php echo $voucher['miles']; ?>  Miles </div>
                      <div class="product-img">
                        <div class="imgTable">
                          <div class="imgCell">
                          <a href="<?php echo route_to('user/voucherDetails/'.$voucher['id_users_vouchers'].'/'.$voucher['rand']);?>">
                            <?php if(!empty($voucher['image'])){?>
                            <img  src="<?php echo base_url();?>uploads/users_vouchers/295x295/<?php echo $voucher['image'];?>" />
                            <?php  }else{ ?>
                            <img  src="<?php echo base_url();?>front/img/default_product.png" />
                            <?php } ?>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="product-content box-sizing"> <span class="product-title">
					   <a href="<?php echo route_to('user/voucherDetails/'.$voucher['id_users_vouchers'].'/'.$voucher['rand']);?>"><?php echo $voucher['title'];?></a></span>
                        <!--<div class="product-description description mod"> <?php echo $voucher['description'];?></div>-->
                        <div class="row ta-c"> </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                  <?php }else{ ?>
                  <div class="empty">No Vouchers</div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>
