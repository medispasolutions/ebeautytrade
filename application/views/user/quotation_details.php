
<div class="row account-create add_address order_details">
  <div class="tabs_cont mod">
    <?php if($login){?>
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <?php } ?>
    <div class="tab_container dashboard ">
      <div  class="tab_content">
      
        <div class="my-account dashboard">
       <?php if($login){
		   $url_back=route_to('user/quotation');
		   }else{
			$url_back=route_to('cart');   } ?>
       <div class="row" style="margin-bottom:15px;"> <a class="back-link" href="<?php echo $url_back;?>">
<i class="fa fa-fw"></i>
Back
</a></div>
		
        <div class="add_new_address">
        
<h2>Quotation#<?php echo $order_details['id_quotation'];?></h2>





<a class="link-print btn" target="_blank"   href="<?php echo route_to('user/quotation_print/'.$order_details['id_quotation'].'/'.$order_details['rand']);?>">
<i class="fa fa-fw"></i>
 Print/Preview
</a>

<a class="link-print btn popup_call_checklist" style="margin-right:10px;"  href="<?php echo route_to('user/quotation_email/'.$order_details['id_quotation'].'/'.$order_details['rand']);?>"  onclick="return false;">
<i class="fa fa-fw"></i>
Send By Email
</a>

<a class="link-print btn" style="margin-right:10px;" href="<?php echo route_to('cart/download_pdf/'.$order_details['id_quotation'].'/'.$order_details['rand']);?>">
<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
Download As PDF
</a>
<?php if($login){?>
<a class="link-print btn" style="margin-right:10px;" href="<?php echo route_to('cart/export_to_shopping/'.$order_details['id_quotation'].'/'.$order_details['rand']);?>">
<i class="fa fa-sign-out" aria-hidden="true"></i>
 Export To Shopping
</a>
<?php } ?>
</div>
          
<p class="order-date">Quotation Created Date: <?php echo  date("F d,Y", strtotime($order_details['created_date'])); ?></p>
          
          <!--ORDER SHIPPING -->
          
          
          <!--ORDER BILLING -->
          
          
          <!--<table border="0" cellpadding="0" cellspacing="0" class="total-price-table">
     <tr>
<td colspan="2">
<span><a class="print invoice_btn" target="_blank" href="<?php echo route_to('user/invoice/'.$order_details['id_quotation']); ?>"><?php echo lang('invoice'); ?></a></span></td>

</tr>
<tr>
<td>
<span><?php echo lang('order_id'); ?>: </span></td>
<td><?php echo $order_details['id_quotation']; ?></td>
</tr>
<tr>
<td>
<span>Date : </span></td>
<td><?php echo $order_details['created_date']; ?></td>
</tr>
<tr>
<td>
<span><?php echo lang('status'); ?> : </span></td>
<td><?php echo lang($order_details['status']); ?></td>
</tr>

<?php if($order_details['status'] == 'paid') {?>
<tr>
<td>
<span><?php echo lang('payment_date'); ?> : </span></td>
<td><?php echo $order_details['payment_date']; ?></td>
</tr>
<?php }?>


<?php if(!empty($order_details['comments'])) {?>
<tr>
<td>
<span>Comments : </span></td>
<td><?php echo $order_details['comments']; ?></td>
</tr>
<?php }?>

</table>-->
          
          <div class="shopping_cart_cont">
          <div class="table-b">
           
          <form id="update_cart_form" method="post" accept-charset="utf-8" action="<?php echo route_to('user/update_quotation');?>">
         <div class="row" id="shopping-cart-cont">
         	<?php $this->load->view('blocks/quotation');?>
         </div>
          </form>
           
          </div>  </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
