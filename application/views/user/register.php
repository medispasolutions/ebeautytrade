<?php
if(isset($userData) && !empty($userData)) {
	$buttonLabel = lang('update_profile');
	$formAction = site_url('user/updateProfile');
	$pTitle = lang('user_account_update_profile');
	$pStar = '';
	$formID = 'updateProfile';
	$menuBlock = "user/account_menu";
	$update=true;
}
else {
	$update=false;
	$userData['first_name'] = '';
	$userData['last_name'] = '';
	$userData['email'] = '';
	$userData['phone'] = '';
	$userData['company_name'] = '';
	$userData['company_email'] = '';
	$userData['company_website'] = '';
	$userData['mobile'] = '';
	$userData['gender'] = '';
	$userData['username'] = '';
	$userData['website'] = '';
	$userData['id_countries'] = '';
	$userData['state'] = '';
	$userData['city'] = '';
	$userData['country'] = '';
	$userData['address'] = '';
	$buttonLabel = lang('register');
	$pTitle = lang('create_an_account');
	$formAction = route_to('user/submit');
	$pStar = ' *';
	$formID = 'registration';
	$menuBlock = "user/user_menu";
}


?>



<div class="innerContent account-create account-register-mod">
<div class="page-title border">
      <h1><i class="fa fa-fw"></i>Create an Account</h1>
</div>
<div class="row">
<div class="col-register-2">
<ol class="opc">
<li id="opc-email" class="opc-step">
<div class="step-title">
<span class="number">1</span>
<h2>Enter Your Email & Country</h2>
<a class="edit_register" onclick="edit_register()">Edit</a>
</div>
<div class="new-users">
      
       <form id="add_email" method="post" accept-charset="utf-8" action="<?php echo route_to('user/add_email');?>" >
        
        <div class="form-item ">
          <label class="required" >Email <em>*</em> </label>
          <input class="textbox" type="text" name="email" >
          <span id="email-error" class="form-error"></span> </div>
          
          <div class="form-item">
          <label class="required" for="City">City<em>*</em></label>
          <input class="textbox" type="text" name="city" >
          <span id="city-error" class="form-error"></span></div>
          
          <div class="form-item ">
          <label class="required" >Country <em>*</em> </label>
          <select name="id_countries" class="textbox selectbox">
          <option value="">-Select Country-</option>
          <?php foreach($countries as $country){?>
          <option <?php if($country['id_countries']==253) echo "selected"; ?> value="<?php echo $country['id_countries'];?>"><?php echo $country['title'];?></option>
          <?php } ?>
          </select>
          <span id="id_countries-error" class="form-error"></span> </div>
        
        
        <label class="lbl_required">* Required Fields</label>
        <div class="buttons-set">
            <div id="loader-bx"></div>
        <div class="FormResult"></div>
         <input type="submit" title="submit" value="Proceed" class="btn">  
         </div>
         </form>
      </div>
</li>
<li id="opc-register" class="opc-step">
<div class="step-title">
<span class="number">2</span>
<h2>Account Registration</h2>
</div>
<div class="new-users">
<div class="tabs_cont">


 
    <div  class="register_form">
<!--<div class="content">
Should you not have an existing business and you are planning to open a new one, kindly <a href="<?php echo route_to('opening_new_spa');?>">click here</a>.
</div>-->
  <form id="<?php echo $formID;?>" method="post" accept-charset="utf-8" action="<?php echo $formAction ;?>" >
  <div class="row">

 

<div class="roles_item">
<!--   <input type="radio" name="role_c" value="4" />Spa/Salon
   <input type="radio" name="role_c" value="5" />Supplier-->
  
<input type="hidden" name="role_c" value="4" />
<input id="id_role" type="hidden" value="4" name="id_role">

</div>


<div class="innerUserForm">
<!--ROW 1-->
<div class="row_form">
<div class="form-item">
          <label class="required" >Business Name<em>*</em> </label>
          <input class="textbox" type="text" name="trading_name" >
          <span id="trading_name-error" class="form-error"></span> </div>
               
</div>

<div class="row_form">
<div class="form-item ">
          <label class="required" ><?php echo lang('license_number'); ?></label>
          <input class="textbox" type="text" name="license_number" >
          <span id="license_number-error" class="form-error"></span> </div>  
                
</div>

<div class="row_form">
<!--<div class="form-item gutter-right">
          <label class="required" for="Salutation" name="salutation">Salutation</label>
          <select name="salutation" class="textbox selectbox">
          <option>-Select-</option>
           <option value="Mr">Mr</option>
           <option value="Mrs">Mrs</option>
           <option value="Ms">Ms</option>
       
          </select>
          <span id="salutation-error" class="form-error"></span> </div>-->
<div class="form-item gutter-right">
          <label class="required" >First Name<em>*</em> </label>
          <input class="textbox" type="text" name="first_name" >
          <span id="first_name-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >Last Name<em>*</em> </label>
          <input class="textbox" type="text" name="last_name" >
          <span id="last_name-error" class="form-error"></span> </div>  
          
          
                       
</div>

<div class="row_form">
<div class="form-item ">
          <label class="required" ><?php echo lang('position'); ?></label>
          <input class="textbox" type="text" name="position" >
          <span id="position-error" class="form-error"></span> </div>  
                
</div>



<div class="row_form">
<div class="form-item">
<label class="required" for="Phone">Phone<em>*</em></label>
<input class="textbox mobile-m" type="text" name="phone" >
<span id="phone-error" class="form-error"></span></div>                 
</div>

<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" for="password">Desired Password<em>*</em> </label>
          <input class="textbox" type="password" name="password" >
          <span id="password-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >Re-type Password<em>*</em> </label>
          <input class="textbox" type="password" name="confirm_password" >
          <span id="confirm_password-error" class="form-error"></span> </div>               
</div>




<div class="row_form">
<div class="form-item gutter-right" style="display:none;">
          <label class="required">Email<em>*</em></label>
          <input class="textbox email_register" type="text"  value="" disabled="disabled" >
          <input class="email_register" type="hidden"  name="email" value=""  >
            <input type="hidden" name="id_user" id="id_user" value="" />
  <input type="hidden" name="correlation_id" id="correlation_id" value="" />

          <span id="email-error" class="form-error"></span></div>
<div class="form-item">
          <label class="required" >Confirm Email<em>*</em></label>
          <input class="textbox" type="text" name="confirm_email" >
          <span id="confirm_email-error" class="form-error"></span></div>               
</div>

<!--Private Policy-->
<!--<div class="row"><h2 class="legend">Privacy Policy</h2></div>-->


<!--<div class="review_reg">
<a  class="popup_call" onclick="return false" href="<?php echo route_to('user/getPrivatePolicy/'.$private_policy['title_url']);?>"><i class="fa fa-fw"></i>Review privacy policy</a>         
</div>
<div class="review_reg">
<a class="popup_call" onclick="return false"  href="<?php echo route_to('user/getPrivatePolicy/'.$disclaimer['title_url']);?>"><i class="fa fa-fw"></i>Review Legal Disclaimer</a>         
</div>-->


<!--<div class="row_form" style="margin-top:20px;">
<div class="form-item">
          <label class="required" for="password">Please type the letters below <em>*</em> </label>
          <span class="captchaImage"></span>
		  <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" />
		<span id="captcha-error" class="form-error"></span> </div>               
</div>-->

    
    <!--<div class="form-item captcha-form"><div class="g-recaptcha" data-sitekey="6LepIiYTAAAAAGWLCgFjPrgQnryqWDNXoKG2x-1J"></div><div id="g-recaptcha-response-error" class="form-error"></div></div>-->
    </div>
    
    
    </div> 
    
    <div class="row">

<div class="row_form">
<div class="form-item mod req_c agree_disclaimer">
<input id="agree_c" class="checkbox validation-passed" type="checkbox" value="1" title="Sign Up for Newsletter" name="agree_c">
<label >Yes, I have read and understood the <a class="popup_call" onclick="return false" href="<?php echo route_to('user/getPrivatePolicy/'.$private_policy['title_url']);?>">Privacy Policy</a> and agree to the <a class="popup_call" onclick="return false"  href="<?php echo route_to('user/getPrivatePolicy/'.$disclaimer['title_url']);?>">Terms of Use.</a></label>
<input type="hidden" name="agree" value="" />
<span id="agree-error" class="form-error"></span></div>
             
</div>

<div class="row_form">
<div class="form-item mod">
<input id="is_subscribed" class="checkbox validation-passed" type="checkbox" value="1" title="Sign Up for Newsletter" name="is_subscribed">
<label >  Sign Up for Newsletter</label>
<span id="is_subscribed-error" class="form-error"></span></div>
               
</div>

<!--<div class="form-item">

<div class="fw">
<img class="siimage" id="siimage" src="<?=base_url()?>captcha/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left" width="224" height="60" />
<span class="captchatitle">
<a onclick="document.getElementById('siimage').src = '<?=base_url()?>captcha/securimage_show.php?sid=' + Math.random(); this.blur(); return false"  tabindex="-1" style="border-style: none;"  title="Refresh Image" >
<img  src="<?=base_url()?>captcha/images/refresh.png" alt="Reload Image" onclick="this.blur()" align="bottom" border="0" /></a> 
</span>
</div>

  <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" placeholder="<?php echo lang('please_type_the_letters_below');?>" />
		<span id="captcha-error" class="form-error"></span>
</div>-->

<label class="lbl_required mod">* Required Fields</label>
 
<div class="buttons-set">
  <div id="loader-bx"></div>
      <div class="FormResult"> </div>
          <input class="btn" title="Create an Account" type="submit" value="Submit">
           </div>  </div>
    </form>
    </div>
    
    

    

</div>
</div>
</li></ol>
</div>
<div class="col-register-2 right">

<?php  if(!empty($ads_register)){  ?>
<div class="register-ads box-sizing" >
  <div class="slick-ads-register">
  <?php 
foreach($ads_register as $ads){ ?>
  <div class="item">
  <div class="innerItem">
    <?php if(!empty($ads['link'])){?>
  <a href="<?php echo prep_url($ads['link']);?>" target="<?php echo $ads['target'];?>">
  <?php } ?>
  <img src="<?php echo base_url();?>uploads/banner_ads/<?php echo $ads['image'];?>" />
  
   <?php if(!empty($ads['link'])){?></a><?php } ?>
  </div>
  </div>

  

  <?php } ?>
  </div>
  </div>
  <?php } ?>
</div>
</div>

  

</div>
