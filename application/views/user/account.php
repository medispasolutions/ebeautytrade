<div class="row">
  <div class="tabs_cont mod">
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <div class="tab_container dashboard">
      <div  class="tab_content">
        <div class="my-account">
          <div class="box-account box-info">
            <div class="box-head">
              <h2>Account Information</h2>
            </div>
            <div class="col2-set">
              <div class="col-1">
                <div class="box">
                  <div class="box-title">
                    <h3>Contact Information</h3>
                    <a href="<?php echo route_to('user/profile');?>"><span class="web">Edit Profile</span><i class="fa fa-fw"></i></a> </div>
                  <div class="box-content">
                    <p> <?php echo $user['name'];?> <br>
                      <?php echo $user['email'];?> <br>
                      <a  href="<?php echo route_to('user/profile/changepass#Edit');?>">Change Password</a> </p>
                  </div>
                </div>
              </div>
              <div class="col-2">
               <div class="box">
                <div class="box-title">
                    <h3>Available Balance</h3>
                    <a href="<?php echo route_to('user/credits');?>"><span class="web">Manage My Wallet</span><i class="fa fa-fw"></i></a> </div>
                <?php $currencies_arr=array(); ?>
                <p>
                <div class="box-content">
                  <?php if(!empty($currencies)){?>
                  <?php foreach($currencies as $currency){
					if(!in_array($currency['currency'],$currencies_arr)) {
						 array_push($currencies_arr,$currency['currency']);
						   ?>
                  <label class="currency-label"><?php echo changeCurrency($user['available_balance'],true,$currency['currency'],$currency['default_currency']); ?></label>
                  <?php } }?>
                  <?php } else{?>
                  0 <?php echo $this->config->item('default_currency');?>
                  <?php } ?>
                  <br />
                   <a  href="<?php echo route_to('user/add_credits');?>">Add to My Wallet</a>
                  </p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col2-set">
              <div class="box">
                <div class="box-title">
                  <h3>Address Book</h3>
                  <a  href="<?php echo route_to('user/addresses');?>"><span class="web">Manage Addresses</span><i class="fa fa-fw"></i></a> </div>
                <div class="box-content">
                  <div class="col-1">
                    <h4>Default Billing Address</h4>
                    <address>
                    <?php if(isset($billing) && !empty($billing)){ ?>
                    <?php echo $billing['name'];?><br>
                    <?php if(!empty($billing['company']) && $billing['company']!=0){?>
                    <?php echo $billing['company'];?> <br>
                    <?php } ?>
                    <?php echo $billing['street_one'];?> <br>
                    <?php if(!empty($billing['street_two']) && $billing['street_two']!=0){?>
                    <?php echo $billing['street_two'];?> <br>
                    <?php } ?>
                    <?php echo $billing['city'];?>
                    <?php if(!empty($billing['state']) && $billing['state']!=0) echo ",".$billing['state'];?>
                    <?php if(!empty($billing['postal_code']) && $billing['postal_code']!=0) echo ",".$billing['postal_code'].'';?>
                    <br>
                    <?php echo $billing['countries_title'];?> <br>
                    T: <?php echo $billing['phone'];?>
                    <?php if(!empty($billing['fax'])) echo "<br>F:".$billing['state']?>
                    <br>
                    <a href="<?php echo route_to('user/address/'.$billing['id_users_addresses']);?>">Edit Address</a>
                    <?php }else{ ?>
                    You have not set a default billing address.
                    <?php } ?>
                    <br>
                    </address>
                  </div>
                  <div class="col-2">
                    <h4>Default Shipping Address</h4>
                    <address>
                    <?php if(isset($shipping) && !empty($shipping)){ ?>
                    <?php echo $shipping['name'];?><br>
                    <?php if(!empty($shipping['company']) && $shipping['company']!=0){?>
                    <?php echo $shipping['company'];?> <br>
                    <?php } ?>
                    <?php echo $shipping['street_one'];?> <br>
                    <?php if(!empty($shipping['street_two']) && $shipping['street_two']!=0){?>
                    <?php echo $shipping['street_two'];?> <br>
                    <?php } ?>
                    <?php echo $shipping['city'];?>
                    <?php if(!empty($shipping['state']) && $shipping['state']!=0) echo ",".$shipping['state'];?>
                    <?php if(!empty($shipping['postal_code']) && $shipping['postal_code']!=0) echo ",".$shipping['postal_code'].'';?>
                    <br>
                    <?php echo $shipping['countries_title'];?> <br>
                    T: <?php echo $shipping['phone'];?>
                    <?php if(!empty($shipping['fax'])) echo "<br>F:".$shipping['state']?>
                    <br>
                    <a href="<?php echo route_to('user/address/'.$shipping['id_users_addresses']);?>">Edit Address</a>
                    <?php }else{ ?>
                    You have not set a default shipping address.
                    <?php } ?>
                    <br>
                    </address>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
