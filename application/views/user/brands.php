<?php 
$size_arr_file=getMaxSize('file');
$size_arr_image=getMaxSize('image');
?>

<div class="row account-create add_address">
  <div class="tabs_cont mod">

  <?php $this->load->view('blocks/dashboard_tabs');?>
  
  <div class="tab_container dashboard list_brands">
    <div  class="tab_content">
<section class="innerContent">

   
        <div class="my-account dashboard">
          <div class="page-title">
        
            <a class="btn fr" type="button" title="Add New Address" href="<?php echo route_to('user/brand');?>">Add Brand</a>
           </div>
          <div class="row">
            <div class="shopping_cart_cont">
            <div class="table-b">
            <div class="n-table">
            
              <table border="1" cellpadding="0" cellspacing="0" class="shopping_cart_table">
                <tr>
             
                  <th>Name</th>
                  <th>Date Added</th>
                  <th>Logo</th>
          
                  <th style="text-align:center;"><?php echo lang('status'); ?></th>
                  <th style="width:70px;text-align:center;"><?php echo lang('order_operations'); ?></th>
                </tr>
                <?php if(!empty($brands)) {?>
                <?php foreach($brands as $brand) {

$created_date=date("j-F-Y",strtotime($brand['created_date']));?>

                <tr>
             
                  <td><?php echo $brand['title']; ?></td>
                  <td><?php echo $created_date; ?></td>
                  <td>
                  <?php if(!empty($brand['logo'])){?>
                  <img style="max-height:30px;" src="<?php echo base_url();?>uploads/brands/<?php echo $brand['logo'];?>" />
                  <?php }else{ ?>
                  -
                  <?php } ?>
                  </td>
                  <td style="text-align:center"><?php echo $this->custom_fct->getStatus($brand['status']);?></td>
                  
                  
                  <td style="text-align:center;">
                  <a  href="<?php echo route_to('user/brand/'.$brand['id_brands']); ?>"> 
                 <i class="fa fa-fw"></i></a>
                    <!--<a onclick="if(confirm('Are you sure you want delete this brand ?')){ delete_row('brands','<?=$brand["id_brands"];?>'); }" > 
                    <i class="fa fa-fw"></i></a>--></td>
                    
                </tr>
        
                <?php }?>
                <?php } else {?>
                <tr>
                  <td colspan="7" align="center">No Brands</td>
                </tr>
                <?php }?>
              </table>
              </div>
              </div>
            </div>
            
          </div>
        </div>
   

</section>
</div></div></div></div>
