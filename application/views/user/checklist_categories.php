<script>
function removeCategory4(){
window.location="<?= route_to('user/removeCategory/'.$info_section['id_checklist_categories']); ?>";
return false;
}</script>
<?php $hide=""; if(empty($user_favorites)){ $hide="hide";}?>

<div class="innerContent stocklist">
  <div class="page-title border">
    <h1 class="tr-n">My List</h1>
    <a class="back-link"  href="<?php echo route_to('user/checklist');?>" > <i class="fa fa-fw"></i> Back </a> </div>
  <div class="checklist_title"><?php echo $info_section['title'];?> <a alt="Edit" tiltle="Edit" class="popup_call_checklist" onclick="return false;"  href="<?php echo route_to('user/category/'.$info_section['id_checklist_categories']);?>" > [edit name] </a></div>
  <div class="stocklist-btns">
    <ul >
      <!--   <li class="pull-right"><a class="bulk-options-a btn">Bulk Options <i class="fa fa-angle-down" ></i></a>
   <ul class="bulk-options-ul">
   <li><a onclick="performAction('add-to-cart')" class="btn-c">Add To Cart</a></li>
   <li><a onclick="performAction('remove-all-list')" class="btn-c">Remove All</a></li>

   </ul></li>   -->
      <li class="pull-right bulk-option <?php echo $hide;?>"><a class="btn add-products" onclick="submitBulkOption()">Perform Action</a></li>
      <li class="pull-right bulk-option <?php echo $hide;?>">
        <select id="bulk_option" class="textbox selectbox" >
          <option value="">Bulk Options</option>
          <option value="add-to-cart">Add To Cart</option>
          <option value="remove-all-list">Remove Selected</option>
        </select>
      </li>
      <!--<li> <a  class="btn add-products popup_call_checklist" onclick="return false;" href="<?php echo route_to('user/checklist_products/'.$info_section['id_checklist_categories']);?>" >Add Products</a></li>-->
      <li > <a  class="btn add-products" onclick="if(confirm('Are you sure you want to remove this category ?')){ removeCategory4(); }" href="#">Remove Category</a></li>
    </ul>
  </div>
  <?php $this->load->view('user/favorites_cart');?>
</div>
