<div id="loginAndRegistrationPage" class="mrg-top-25">

    <h3>Forgot your password?</h3>
    <p class="section-lead">Not to worry! Enter your e-mail address below and our Customer Service will e-mail you instructions for setting up a new password.</p>

    <form id="PasswordForm" method="post" accept-charset="utf-8" action="<?php echo route_to('user/password_sendrequest') ;?>" class="mrg-top-25">
        <div class="narrow">
            <input type="email" placeholder="Type here your e-mail address" name="email">
            <div id="email-error" class="form-error"></div>

            <input type="submit" value="<?php echo lang('password_reminder_button');?>" class="mrg-top-25">
            <div class="FormResult"></div>
        </div>
    </form>

</div>