


<div class="innerContent account-login forgot_password">
    <div class="page-title border">
    <?php if(isset($setpassword)){?>
      <h1>My Account : Set Password</h1>
      <?php }else{ ?>
      <h1>My Account : Forgot Password</h1> 
      <?php } ?>
      <a class="back-link" href="<?php echo route_to('user/login');?>">
<i class="fa fa-fw"></i>
Back
</a>
    </div>

      
      <div class="new-users">
      
       <form id="UpdatePasswordForm" action="<?php echo site_url('user/update_new_password') ?>" method="post">
         <input type="hidden" id="id_user" name="id_user" value="<?php echo $id_user; ?>" />
        <div class="form-item ">
          <label class="required"> Password <em>*</em> </label>
          <input class="textbox" type="password" name="password" >
          <span id="password-error" class="form-error"></span> </div>
          
          <div class="form-item ">
          <label class="required" > Confirm Password <em>*</em> </label>
          <input class="textbox" type="password" name="confirm_password" >
          <span id="confirm_password-error" class="form-error"></span> </div>
        <!--<div class="form-item">
          <label class="required" for="email"> Please type the letters below <em>*</em> </label>
          <input class="textbox" type="text" name="country" >
          <span id="country-error" class="form-error"></span> </div>-->
        <label class="lbl_required">* Required Fields</label>
        <div class="buttons-set">
         <div id="loader-bx"></div>
        <div class="FormResult"></div>
        <?php
		$btn_name=lang('update_password'); 
		if(isset($setpassword)){
		$btn_name='Proceed';}
		?>
          <input type="submit" class="btn"  value="<?php echo $btn_name; ?>"> 
           </div>
           </form>
      </div>
  

</div>