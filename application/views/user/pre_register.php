<?php
$size_arr_file=getMaxSize('file');
$size_arr_image=getMaxSize('image');
if(isset($userData) && !empty($userData)) {
	$buttonLabel = lang('update_profile');
	$formAction = site_url('user/update_pre_register');
	$pTitle = lang('user_account_update_profile');
	$pStar = '';
	$formID = 'updateProfile';
	$menuBlock = "user/account_menu";
	$update=true;
}
else {
	$update=false;
$userData=array('name'=>'','email'=>'','phone'=>'','address'=>'','username'=>'','password'=>'','id_roles' => '','status' => '','id_discount_groups' => '');
$userData['company_name'] = "";
$userData['id_countries'] = "";
$userData['state'] = "";
$userData['username'] = "";
$userData['comments'] = "";
$userData['city'] = "";
$userData['trading_name'] = "";
$userData['trading_licence'] = "";
$userData['address_1'] = "";
$userData['address_2'] = "";
$userData['salutation'] = "";
$userData['mobile'] = "";
$userData['fax'] = "";
$userData['type'] = "";
$userData['position'] = "";
$userData['first_name'] = "";
$userData['last_name'] = "";
$userData['website'] = "";
$userData['gender'] = "";
$userData['guest'] = "";
$userData['id_supplier_info'] = "";
	$userData['address'] = '';
	$buttonLabel = lang('register');
	$pTitle = lang('create_an_account');
	$formAction = route_to('user/update_pre_register');
	$pStar = ' *';
	$formID = 'registration';
	$menuBlock = "user/user_menu";
}


?>

<div class="page-title border">
<h1>Pre-registration</h1>

</div>
<div class="row">
  <div class=" mod">
  
<div class=" dashboard account-create">

    <div class="row">
 <form id="<?php echo $formID;?>" method="post" enctype="multipart/form-data" action="<?php echo $formAction ;?>" >
<div class="row_form">
<div class="form-item gutter-right">


  <input type="hidden" name="id_role" id="id_role" value="<?php echo $userData['id_roles'];?>" />
 <span id="id_role-error" class="form-error"></span>
</div></div>
<!--ROW 1-->
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >Trading Name<em>*</em>  </label>
          <input class="textbox" type="text" name="trading_name" value="<?php echo $userData['trading_name'];?>"  >
          <span id="trading_name-error" class="form-error"></span> 
         </div>
<div class="form-item gutter-right req_c">
          <label class="required" >Upload Trading Licence here<em>*</em></label>
          <input class="textbox input-file " type="file" name="trading_licence" >
          <input class="textbox input-file" type="hidden" name="trading_licence_f" value="<?php echo $userData["trading_licence"]; ?>" >
          <?
if($userData["trading_licence"] != ""){?>	<span id="trading_licence_<?php echo $userData["id_user"]; ?>"> 
<a href="<?= base_url(); ?>uploads/user/<?= $userData["trading_licence"]; ?>" target="_blank"  >
show file
</a>
&nbsp;&nbsp;&nbsp;
<a class="cur" onclick='removeFile("user","trading_licence","<?php echo $userData["trading_licence"]; ?>",<?php echo $userData["id_user"]; ?>)' >
<img src="<?= base_url(); ?>images/delete.png"  /></a></span>
<? } else {  } ?>
          <span id="trading_licence-error" class="form-error"></span>
          <span id="trading_licence_f-error" class="form-error"></span> </div>               
</div>

<div class="row_form">

<div class="form-item ">
          <label class="required" >Website</label>
          <input class="textbox" type="text" name="website" value="<?php echo $userData['website'];?>" >
          <span id="website-error" class="form-error"></span></div>               
</div>

<h2 class="legend">Personal Information</h2>
<!--Personal Information-->
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" for="Salutation" >Salutation</label>
          <select name="salutation" class="textbox selectbox">
 <option value=""  >-Select Salutation-</option>
<option value="Mr" <? if(set_value('salutation',$userData["salutation"]) == 'Mr') echo 'selected="selected"'; ?> >Mr</option>
<option value="Mrs" <? if(set_value('salutation',$userData["salutation"]) == 'Mrs') echo 'selected="selected"'; ?> >Mrs</option>
<option value="Ms" <? if(set_value('salutation',$info["salutation"]) == 'Ms') echo 'selected="selected"'; ?> >Ms</option>
<option value="Ms" <? if(set_value('salutation',$info["salutation"]) == 'Miss') echo 'selected="selected"'; ?> >Miss</option>
          </select>
          <span id="first_name-error" class="form-error"></span> </div></div>
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >First Name<em>*</em> </label>
          <input class="textbox" type="text" name="first_name" value="<?php echo $userData['first_name'];?>" >
          <span id="first_name-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >Last Name<em>*</em> </label>
          <input class="textbox" type="text" name="last_name" value="<?php echo $userData['last_name'];?>" >
          <span id="last_name-error" class="form-error"></span> </div>               
</div>
<div class="row_form">
<!--<div class="form-item gutter-right">
          <label class="required" >Username<em>*</em></label>
          <input class="textbox" type="text" name="username" value="<?php echo $userData['username'];?>" >
          <span id="username-error" class="form-error"></span></div>-->
<div class="form-item">
          <label class="required" >Position</label>
          <input class="textbox" type="text" name="position"  value="<?php echo $userData['position'];?>" >
          <span id="last_name-error" class="form-error"></span> </div>               
</div>


<div class="row_form">
<div class="form-item gutter-right">
          <label class="required">Email<em>*</em></label>
          <input class="textbox" type="text"   name="email"  value="<?php echo $userData['email'];?>">
<span id="email-error" class="form-error"></span></div>               
</div>

<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" for="password">Desired Password<em>*</em> </label>
          <input class="textbox" type="password" name="password" >
          <span id="password-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >Retype Password<em>*</em> </label>
          <input class="textbox" type="password" name="confirm_password" >
          <span id="confirm_password-error" class="form-error"></span> </div>               
</div>

<!--Address-->
<h2 class="legend">Address</h2>
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >Address 1<em>*</em></label>
          <input class="textbox" type="text" name="address_1" value="<?php echo $userData['address_1'];?>" >
          <span id="address_1-error" class="form-error"></span></div>
<div class="form-item gutter-right">
          <label class="required" >Address 2</label>
          <input class="textbox" type="text" name="address_2"  value="<?php echo $userData['address_2'];?>" >
          <span id="address_2-error" class="form-error"></span> </div>               
</div>
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" for="City">City<em>*</em></label>
          <input class="textbox" type="text" name="city" value="<?php echo $userData['city'];?>"  >
          <span id="city-error" class="form-error"></span></div>
<div class="form-item gutter-right">
          
         <!--<input class="textbox" type="text" name="phone" value="<?php echo $userData['phone'];?>"  >-->
          
         <label class="required" >Phone <em>*</em></label>
       
                  <!--<input class="textbox" type="text" name="mobile" value="<?php echo $brand['mobile'];?>" >-->
         <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="phone[0]" value="<?php echo getPhoneField($userData['phone'],0);?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="phone[1]" value="<?php echo getPhoneField($userData['phone'],1);?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="phone[2]" value="<?php echo getPhoneField($userData['phone'],2);?>" >
                  <span id="number-error" class="form-error"></span>
          <span id="phone-error" class="form-error"></span></div>               
</div>
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >Fax</label>
          <!--<input class="textbox" type="text" name="fax" value="<?php echo $userData['fax'];?>"  >-->
           <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="fax[0]" value="<?php echo getPhoneField($userData['fax'],0);?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="fax[1]" value="<?php echo getPhoneField($userData['fax'],1);?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="fax[2]" value="<?php echo getPhoneField($userData['fax'],2);?>" >
          <span id="fax-error" class="form-error"></span></div>
<div class="form-item gutter-right">
          <label class="required">Mobile</label>
          <!--<input class="textbox" type="text" name="mobile" value="<?php echo $userData['mobile'];?>"  >-->
                     <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="mobile[0]" value="<?php echo getPhoneField($userData['mobile'],0);?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="mobile[1]" value="<?php echo getPhoneField($userData['mobile'],1);?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="mobile[2]" value="<?php echo getPhoneField($userData['mobile'],2);?>" >
          <span id="mobile-error" class="form-error"></span></div>               
</div>


<!--Choose Password-->




<!--OTHER-->
<? 

$categories = $this->fct->getAll_cond('categories','sort_order',array('id_parent'=>0));	

$selected_categories = array();

if(isset($userData['id_roles']) && $userData['id_roles']==5) {	
$selected_categories = $this->users_categories_m->select_user_categories($userData['id_user']);
}

if(isset($val_categories)) {
 $selected_categories = $val_categories;
}

?>
<hr class="row_form_hr" />
<input type="hidden" name="token" value="<?php echo $userData['token'];?>" />
<input type="hidden" name="id_user" value="<?php echo $userData['id_user'];?>" />
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required">Business Type<em>*</em> </label>
          <select name ="business_type" class="textbox selectbox">
          <option value="">-Select Business Type-</option>
          <?php foreach($business_type as $val){?>
          <option <?php if($val['id_business_type']==$userData['business_type']){ echo "selected";}?> value="<?php echo $val['id_business_type'];?>"><?php echo $val['title'];?></option>
          <?php } ?>
          </select>
          <span id="business_type-error" class="form-error"></span> </div>   
<!--<div class="form-item gutter-right">
          <label class="required">How Diy You Hear About Us?<em>*</em> </label>
          <select name ="hear_about_us" class="textbox selectbox">
           <option value="">-Select-</option>
          <?php foreach($hear_about_us as $val){?>
          <option <?php if($val['id_how_did_you_hear_about_us']==$userData['hear_about_us']){ echo "selected";}?> value="<?php echo $val['id_how_did_you_hear_about_us'];?>"><?php echo $val['title'];?></option>
          <?php } ?>
          </select>
          <span id="hear_about_us-error" class="form-error"></span> </div>-->  
          <?php if(checkIfSupplier()){ ?>
          <div class="form-item gutter-right">
<label class="required">Product/Service Categories<em>*</em> (Please check all that apply) </label>
<dl class="dropdown"> 
  
    <dt>
    <a class="textbox selectbox">
    
      <span class="hida" <?php if(!empty($selected_categories)){ echo 'style="display: none;"'; } ?>>Select</span>  
    
      <p class="multiSel">
       <?php if(!empty($selected_categories)){?>
       <?php $i=0;foreach($categories as $category){
		   if(in_array($category['id_categories'],$selected_categories)){$i++;
		 echo '<span title="'.$category['title'].',">'.$category['title'].',</span>';?> 
		 <?php }} ?><?php } ?></p> 
    
    </a>
    </dt>
  
    <dd>
        <div class="mutliSelect">
        
            <ul>
 <?php foreach($categories as $category){
				$cl = '';

if(in_array($category['id_categories'],$selected_categories)) {$cl = 'checked="checked"';
/*$categories_id.=$category['id_categories'];*/
}?>
                <li>
                    <input type="checkbox" <?php echo $cl  ;?>  name="categories[<?php echo $category['id_categories'];?>]" value="<?php echo $category['title'];?>" /><?php echo $category['title'];?></li>
                    <?php } ?>
               
            </ul>
        </div>
    </dd>

</dl>
<input type="hidden" name="category" /></dl>
 <span id="category-error" class="form-error"></span> </div>  
 			<?php } ?>        
</div>

<hr class="row_form_hr" />
<div class="row_form">
<div class="form-item">
          <label class="required">Comments</label>
          <textarea class="textarea" name="comments"><?php echo $userData['comments'];?></textarea>
          <span id="comments-error" class="form-error"></span> </div>   
</div>

<div class="row_form">
<div class="form-item mod">
<input id="is_subscribed" class="checkbox validation-passed" type="checkbox" value="1" <?php if($userData['is_subscribed']==1) echo "checked"; ?> title="Sign Up for Newsletter" name="is_subscribed">
<label >  Sign Up for Newsletter</label>
<span id="is_subscribed-error" class="form-error"></span></div>
               
</div>
<div class="row_form" style="margin-top:20px;">
<div class="form-item">
          <label class="required" for="password">Please type the letters below <em>*</em> </label>
          <span class="captchaImage"><?php echo $this->fct->createNewCaptcha(); ?></span>
		  <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" />
		<span id="captcha-error" class="form-error"></span> </div>               
</div>
<label class="lbl_required mod">* Required Fields</label>
  
<div class="buttons-set">
  <div id="loader-bx"></div>
      <div class="FormResult"> </div>
          <input class="btn" title="Create an Account" type="submit" value="<?php echo lang('submit_information');?>">
           </div>  
    
 </form>

    </div>
  </div></div>

