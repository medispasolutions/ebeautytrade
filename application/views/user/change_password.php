<section class="innerContent">
    <div class="centered">

		<h1 class="h1_title">Change your password</h1>
<div class="edit_password">
<?php
$attributes = array('method'=>'post', 'class'=>'reg-form', 'id'=>'UpdatePasswordForm');
$action = route_to('user/update_password');
echo form_open($action, $attributes);
?>

<div class="row-form row_update_password">
<input type="hidden" value="<?php echo $this->session->userdata('login_id') ;?>" name="id_user" />
<!--Column 1-->
<div class="col-form-3 first">
<div class="row">
<table class="form-item-table" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td>
<label class="form-label">Old Password:</label>
</td>
<td >
<div class="form-item">
<input type="password" name="old_password" class="textbox"  /><span class="form-error" id="old_password-error"></span>
</div>
</td>
</tr>
</table>
</div>  
</div>  

<!--Column 2-->
<div class="col-form-3">
<div class="row">
<table class="form-item-table" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td>
<label class="form-label">New Password:</label>
</td>
<td >
<div class="form-item">
<input type="password" name="new_password" class="textbox"  /><span class="form-error" id="new_password-error"></span>
</div>
</td>
</tr>
</table>
</div>  
</div>

<!--Column 3-->
<div class="col-form-3">
<div class="row">
<table class="form-item-table" width="100%" cellpadding="0" cellspacing="0">
<tr>
<td>
<label class="form-label">Retype New Password:</label>
</td>
<td >
<div class="form-item">
<input type="password" name="confirm_new_password" class="textbox"  /><span class="form-error" id="confirm_new_password-error"></span>
</div>
</td>
</tr>
</table>
</div>  
</div>     
                 
</div>
         
<div class="row">
  <input type="submit" value="Change"  class="btn right"/>
  <a   class="back" onclick="javascript:history.go(-1)" >Back</a>
<div class="FormResult"></div>
  </div>
        </form>
        
       </div> 

    	
    </div>
</section>