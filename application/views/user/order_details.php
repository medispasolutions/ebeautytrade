<?php $login=checkUserIFLogin();?>
<div class="row account-create add_address order_details  <?php if(!$login){ echo "order_details"; }?>">
  <div class="tabs_cont mod">
  <?php if($login){ ?>
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <?php } ?>
    <div class="tab_container dashboard ">
      <div  class="tab_content">
      
        <div class="my-account dashboard">
       <?php if($login){?>
       <div class="row" style="margin-bottom:15px;"> <a class="back-link" href="<?php echo route_to('user/orders');?>">
<i class="fa fa-fw"></i>
Back
</a></div>
		<?php } ?>
     
        <div class="add_new_address">
<h2>Order#<?php echo $order_details['id_orders'];?> - <?php echo $order_details['status'];?></h2>

<a class="link-print btn" target="_blank" href="<?php echo route_to('user/invoice/'.$order_details['id_orders'].'/'.$order_details['rand']);?>">
<i class="fa fa-fw"></i>
View Invoice
</a>
</div>
          
          <p class="order-date">Date: <?php echo  date("F d,Y g:i A", strtotime($order_details['created_date'])); ?></p>
          
          <!--ORDER SHIPPING -->
            <?php if(isset($order_details['delivery']['branch_name'])){?>
          <div class="col2-set order-info-box">
        
            <div class="col-1">
              <div class="box">
                <div class="box-title">
                  <h2>Shipping Address</h2>
                </div>
                <div class="box-content">
                  <?php $shipping=$order_details['delivery'];
				  ?>
                  <address>
                 <?php if($shipping['pickup']==1){?>
                <div style="color:red;font-size:16; padding-bottom:5px;"> <?php echo lang('pickup');?></div>
                 <?php } ?>
				<?php if(!empty($shipping['branch_name'])){?>
				<?php echo $shipping['branch_name'];?>
				<br>
				<?php }?>

                  
                  <?php echo $shipping['name'];?> <br>
                   <?php echo $order_details['user']['email'];?> <br>
                  <?php if(!empty($shipping['company']) && $shipping['company']!=0){?>
                  <?php echo $shipping['company'];?> <br>
                  <?php }?>
                  <?php echo $shipping['street_one'];?> <br>
                  <?php if(!empty($shipping['street_two'])){?>
                  <?php echo $shipping['street_two'];?> <br>
                  <?php }?>
                  <?php echo $shipping['city'] ?>,<?php echo $shipping['state'] ?>
                  <?php if(!empty($shipping['postal_code']) && $shipping['postal_code']!=0){?>
                  ,<?php echo $shipping['postal_code'] ?>
                  <?php } ?>
                  <br>
                  <?php echo $shipping['country']['title'] ?> <br>
                  T: <?php echo cleanPhone($shipping['phone']); ?>
                  <?php if(!empty($shipping['fax']) && $shipping['fax']!=0){?>
                  <br> F: <?php echo cleanPhone($shipping['fax']); ?>
                  <?php } ?>
                  
                  <?php if(!empty($shipping['mobile']) && $shipping['mobile']!=0){?>
                 <br> M: <?php echo cleanPhone($shipping['mobile']); ?>
                  <?php } ?> 
                  </address>
                </div>
              </div>
            </div>
        
            <div class="col-2">
              <div class="box">
                <div class="box-title">
                  <h2>Shipping Charges</h2>
                </div>
                <div class="box-content"> 
                <?php if($order_details['shipping_charge_currency']>0){
					echo $order_details['shipping_charge_currency'].' '.$order_details['currency'];
					}else{ echo "Free Shipping " ;}?>
                 </div>
              </div>
            </div>
          </div>
              <?php } ?>
          <!--ORDER BILLING -->
          <?php if(isset($order_details['billing']['branch_name'])){?>
          <div class="col2-set order-info-box">
         
            <div class="col-1">
              <div class="box">
                <div class="box-title">
                  <h2>Billing Address</h2>
                </div>
                <div class="box-content">
                  <?php $billing=$order_details['billing'];?>
                  <address>
                  <?php echo $billing['name'];?> <br>
              
                  <?php if(!empty($billing['company']) && $billing['company']!=0){?>
                  <?php echo $billing['company'];?> <br>
                  <?php }?>
                  <?php echo $billing['street_one'];?> <br>
                  <?php if(!empty($billing['street_two']) && $billing['street_two']!=0){?>
                  <?php echo $billing['street_two'];?> <br>
                  <?php }?>
                  <?php echo $billing['city'] ?>,<?php echo $billing['state'] ?>
                  <?php if(!empty($billing['postal_code']) && $billing['postal_code']!=0){?>
                  ,<?php echo $billing['postal_code'] ?>
                  <?php }?>
                  <br>
                  <?php echo $billing['country']['title'] ?> <br>
                  T: <?php echo cleanPhone($billing['phone']); ?> 
                  <?php if(!empty($billing['fax']) && $billing['fax']!=0){?>
                  <br>F: <?php echo cleanPhone($billing['fax'] );?>
                  <?php } ?>
                  <?php if(!empty($billing['mobile']) && $billing['mobile']!=0){?>
                 <br> M: <?php echo cleanPhone($billing['mobile']); ?>
                  <?php } ?> 
                  
                  </address>
                </div>
              </div>
            </div>
       
            <div class="col-2">
              <div class="box">
                <div class="box-title">
                  <h2>Payment Method</h2>
                </div>
                <div class="box-content">
               
                <?php echo $this->ecommerce_model->getPaymentMethod($order_details['payment_method']);?>
                </div>
              </div>
            </div>
          </div>
               <?php } ?>
          <!--<table border="0" cellpadding="0" cellspacing="0" class="total-price-table">
     <tr>
<td colspan="2">
<span><a class="print invoice_btn" target="_blank" href="<?php echo route_to('user/invoice/'.$order_details['id_orders']); ?>"><?php echo lang('invoice'); ?></a></span></td>

</tr>
<tr>
<td>
<span><?php echo lang('order_id'); ?>: </span></td>
<td><?php echo $order_details['id_orders']; ?></td>
</tr>
<tr>
<td>
<span>Date : </span></td>
<td><?php echo $order_details['created_date']; ?></td>
</tr>
<tr>
<td>
<span><?php echo lang('status'); ?> : </span></td>
<td><?php echo lang($order_details['status']); ?></td>
</tr>

<?php if($order_details['status'] == 'paid') {?>
<tr>
<td>
<span><?php echo lang('payment_date'); ?> : </span></td>
<td><?php echo $order_details['payment_date']; ?></td>
</tr>
<?php }?>


<?php if(!empty($order_details['comments'])) {?>
<tr>
<td>
<span>Comments : </span></td>
<td><?php echo $order_details['comments']; ?></td>
</tr>
<?php }?>

</table>-->
          
          <div class="shopping_cart_cont">
          <div class="table-b">
            <div class="n-table">
<?php 
		$line_items_brands = $order_details['line_items_brands'];
		
		

		
	
foreach($line_items_brands as $line_items_brand){?>
<?php /*?><?php if((!empty($line_items_brand['id_brands']) || (empty($line_items_brand['id_brands']) && count($line_items_brands)>1))){?>
<div class="cart-line-items-brand"><?php echo $line_items_brand['deliver_by'];?></div>
<?php } ?><?php */?>
<div class="cart-line-items-brand"><?php echo $line_items_brand['deliver_by'];?></div>
<?php 
$i=0;
$line_items = $line_items_brand['line_items'];

if(!empty($line_items)) { ?>
            <table border="1" cellspacing="0" cellpadding="10" style="text-align:center;" class="shopping_cart_table data-table">
              <tr>
               <th class="product_cart_details" width="5%">#</th>
              <th class="product_cart_details" width="10%">REF</th>
              <th class="product_cart_details" width="12%">BARCODE</th>
              <th style="width:40%;">PRODUCT DESCRIPTION</th>
              <th class="product_cart_details" width="10%">PRICE <?php echo $order_details['currency'];?></th>
			  <th  width="8%">QTY</th>
              <th width="15%">SUB-TOTAL  <?php echo $order_details['currency'];?></th>
              </tr>
              <?php 
$line_items = $line_items_brand['line_items'];
$net_price = 0;
$net_discount_price = 0;

foreach($line_items as $pro) {
$i++;
$options=unSerializeStock($pro['options_en']);
?>
              <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $pro['product']['sku']; ?></td>
               <td><?php echo $pro['product']['barcode']; ?></td>
                <td style="text-align:left;"><a href="<?php echo route_to('products/details/'.$pro['product']['title_url']);?>" class="product_name"><?php echo $pro['product']['title'];?></a>
                <dl class="item-options">
                  <?php if(!empty($options)) { 

		$i=0;
		$c = count($options);
		foreach($options as $opt) {?>
			  <dt><i class="fa fa-fw"></i><?php echo $opt; ?> </dt>
          
	
    <?php }}?></dl>
                  </td>
       <td><?php echo '<span class="price new_price" itemprop="price">'.$pro['price_currency'].' '.$pro['currency'].'</span>'; ?></td>
                <td><?php echo $pro['quantity']; ?></td>
                <td><?php echo $pro['total_price_currency'].' '.$pro['currency'] ;?></td>
              </tr>
              <?php }?>
              </table>
              <?php }?>
              
              
<?php 
$i=0;
$line_items = $line_items_brand['line_items_redeem_by_miles'];

if(!empty($line_items)) { ?>
            <table border="1" cellspacing="0" cellpadding="10" style="text-align:center;" class="shopping_cart_table data-table">
              <tr>
              <th class="product_cart_details" width="5%">#</th>
              <th class="product_cart_details" width="10%">REF</th>
              <th class="product_cart_details" width="12%">BARCODE</th>
              <th style="width:40%;">ITEM(S) REDEEMED BY REWARD PRODUCTS</th>
              <th class="product_cart_details" width="10%">MILES</th>
			  <th  width="8%">QTY</th>
              <th width="15%">SUB-TOTAL</th>
              </tr>
              <?php 

$net_price = 0;
$net_discount_price = 0;

foreach($line_items as $pro) {
$i++;
$options=unSerializeStock($pro['options_en']);
?>
              <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $pro['product']['sku']; ?></td>
               <td><?php echo $pro['product']['barcode']; ?></td>
                <td style="text-align:left;"><a href="<?php echo route_to('products/details/'.$pro['product']['title_url']);?>" class="product_name"><?php echo $pro['product']['title'];?></a>
                <dl class="item-options">
                  <?php if(!empty($options)) { 

		$i=0;
		$c = count($options);
		foreach($options as $opt) {?>
			  <dt><i class="fa fa-fw"></i><?php echo $opt; ?> </dt>
          
	
    <?php }}?></dl>
                  </td>
       <td><?php echo $pro['redeem_miles']; ?></td>
                <td><?php echo $pro['quantity']; ?></td>
                <td><?php echo $pro['redeem_miles']*$pro['quantity'] ;?></td>
              </tr>
              <?php }?>
              </table>
              <?php }?><?php }?>
                  
                  <table border="0" cellpadding="0" cellspacing="0" class="total-price-table">
        <?php /*?>            <?php if($userData['type']=="retail") { 
		$delivery_day=getDeliveryDay($line_items);
		if($delivery_day!=0){?>
                    <tr>
                      <td colspan="2"><span class="delivery_day">Order will be delivered within <?php echo $delivery_day;?> day(s). </span></td>
                    </tr>
                    <?php }}else{ ?>
                    <tr>
                      <td colspan="2"><span class="delivery_day"><?php echo lang('we_will_contact_us');?> </span></td>
                    </tr>
                    <?php } ?><?php */?>
<!--                    <tr>
                      <td><span>Sub-Total: </span></td>
                      <td ><?php echo $order_details['total_price_currency'].' '.$order_details['currency']; ?></td>
                    </tr>-->
               
               <?php if(!empty($order_details['discount']) && $order_details['discount']>0 ){?>
                    <tr>
                      <td><span>Discount: </span></td>
                      <td><?php echo $order_details['discount'].' '.$order_details['currency']; ?></td>
                    </tr>
                    <?php } ?>
              
                     <?php if($order_details['redeem_discount_miles_amount']>0){ ?>
                    <tr>
                      <td><span>Redeemed Miles: </span></td>
                      <td><?php echo $order_details['redeem_miles'].' Miles'; ?></td>
                    </tr>
                    <?php } ?>
                    
                    <!--<tr>
                      <td><span>Shipping: </span></td>
                      <td><?php if($order_details['shipping_charge_currency']>0){
					echo $order_details['shipping_charge_currency'].' '.$order_details['currency'];
					}else{ echo "0.00" ;}?></td>
                    </tr>-->
                    <tr class="grand_total last">
                      <td><strong>Total Price: </strong></td>
                      <td><?php echo $order_details['amount_currency'].' '.$order_details['currency']; ?></td>
                    </tr>
         
                  </table>
             
          </div>  </div>  </div>
          
        </div>
      </div>
    </div>
  </div>
</div>
