<?php $user=$this->ecommerce_model->getUserInfo();?>
<div class="row account-create add_address">
  <div class="tabs_cont mod">
    <?php $this->load->view('blocks/dashboard_tabs');?>
    <div class="tab_container dashboard  miles">
      <div  class="tab_content">
        <section class="innerContent">
        <!--<div class="box-head">
              <h2>My Miles</h2>
            </div>-->
          <div class="col2-set">
              <div class="col-1">
                <div class="box">
                  <div class="box-title">
                    <h3>Available Miles</h3>
                     </div>
                  <div class="box-content">
                  <?php if($userData['miles']<0){
					 $userData['miles']=0;}?>
                   <?php echo $userData['miles'];?> Miles
                  </div>
              
                
                </div>
              </div>
              <?php $pending_miles=$this->custom_fct->getUserPendingMiles($user['id_user']); ?>
              <div class="col-2">
               <div class="box">
                <div class="box">
                  <div class="box-title">
                    <h3 title="Miles will be available when payment is processed">Pending Miles</h3>
                     </div>
                  <div class="box-content" title="Miles will be available when payment is processed">
                   <?php echo $pending_miles;?> Miles
                  </div>
                  
           
                  
                </div>

                </div>
              </div>
            </div>
        </section>
      </div>
    </div>
  </div>
</div>
