<div id="loginAndRegistrationPage" class="container">
    <h1>Welcome to E-Beauty Trade</h1>
    <p class="lead">Your B2B Marketplace for Spas, Salons, Beauty Clinics and Retailers</p>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <a name="login"></a>
            <h3 class="login-heading">Existing Member</h3>
            <p class="section-lead">If you have already registered, please login here with your email address and password.</p>

            <form id="login" method="post" accept-charset="utf-8" action="<?php echo route_to('user/validate') ;?>" class="mrg-top-25">
                <div class="narrow">
                    <input type="email" placeholder="E-mail address" name="email">
                    <div id="email-error" class="form-error"></div>

                    <input type="password" placeholder="Password" name="password">
                    <div id="password-error" class="form-error"></div>
                    <p class="blue-link">
                        <a href="<?php echo route_to('user/password');?>">Forgot Your Password?</a>
                    </p>
                    <input type="submit" value="<?php echo lang('login');?>" class="mrg-top-25">
                    <div class="FormResult"></div>

                    <p class="black-link text-center">
                        New Customer?
                        <a class="underlined" href="#register">Register here</a>
                    </p>
                </div>
            </form>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 left-border-realted">
            <h3 class="login-heading"><a name="register"></a>Create an Account</h3>
            <P>Sign up to access authentic wholesale prices and connect with suppliers directly.</P>
            <h4>Business Details</h4>

            <form id="registration" method="post" accept-charset="utf-8" action="<?php echo route_to('user/submit') ;?>" class="mrg-top-15">
                <div class="narrow">
                    <input type="text" placeholder="Business name as per Trade License*" name="trading_name" >
                    <div id="trading_name-error" class="form-error"></div>

                    <input type="text" placeholder="Business Registration number or VAT ID number*" name="trade_license_no">
                    <div id="trading_license_no-error" class="form-error"></div>

                    <div class="bissiness-realtes">
                        <select name="b_type">
                            <option value="0">Business Type*</option>
                            <option  value="1">Beauty Clinic</option>
                            <option  value="2">Beauty Salon</option>
                            <option  value="3">Day Spa</option>
                            <option  value="4">Fitness Center</option>
                            <option  value="5">Hotel/Resort Spa</option>
                            <option  value="6">Massage/Wellness Center</option>
                            <option  value="7">School</option>
                            <option  value="8">Slimming Center</option>
                            <option  value="9">Suppliers and Others</option>
                            <option  value="11">Gents</option>
                            <option  value="12">Retailer</option>
                        </select>
                        <div id="b_type-error" class="form-error"></div>
                    </div>
                    <div class="bissiness-realtes">
                        <?php
                            $q = "SELECT `countries`.`id_countries`, `countries`.`title` FROM `countries`";
                            $query = $this->db->query($q);
                        ?>
                        <select name="country" >
                            <option value="">Select your Country*</option>
                        <?php
                            foreach ($query->result() as $row){
                        ?>
                            <option value = "<?php echo $row->id_countries; ?>"><?php echo $row->title; ?></option>
                        <?php } ?>
                        </select>
                        <div id="country1-error" class="form-error"></div>
                    </div>
                    
                    <input type="text" placeholder="Phone*" name="phone" id='demo' class="mobile-m" autocomplete="on">
                    <div id="phone-error" class="form-error"></div>

                    <h4>Authorized Person</h4>

                    <input type="text" placeholder="First Name*" name="first_name">
                    <div id="first_name-error" class="form-error"></div>

                    <input type="text" placeholder="Family Name*" name="family_name">
                    <div id="family_name-error" class="form-error"></div>

                    <input type="text" placeholder="Position*" name="position" >
                    <div id="position-error" class="form-error"></div>

                    <h4>Login Details</h4>
                    <script type="text/javascript" src="https://www.ebeautytrade.com/front/js/jquery-1.10.0.min.js"></script>
                    <script>
                    $(function () {
                        $('#email').bind("cut copy paste", function (e) {
                            e.preventDefault();
                        });
                        $('#confirm_email').bind("cut copy paste", function (e) {
                            e.preventDefault();
                        });
                    });
                    </script>
                    <input type="email" placeholder="E-mail address*" name="email" id="email">
                    <div id="email-error" class="form-error"></div>

                    <input type="email" placeholder="Confirm E-mail address*" name="confirm_email" id="confirm_email">
                    <div id="confirm_email-error" class="form-error"></div>

                    <input type="password" placeholder="Desired Password*" name="password">
                    <div id="password-error" class="form-error"></div>

                    <input type="password" placeholder="Confirm Password*" name="con_password">
                    <div id="con_password-error" class="form-error"></div>
                    <p style="font-size: 12px !important; margin: -10px 0 10px !important;"><i>*required fields</i></p>

                </div>

                <p>This is a trade only marketplace. By submitting the information above you admit that you are legally authorized to act on behalf of your Company.</p>

                <label class="mrg-top-25">
                    <input id="agree" class="checkbox validation-passed" type="checkbox" value="1" name="agree">
                    Yes, I have read and understood the <a class="popup_call" onclick="return false" href="<?php echo route_to('user/getPrivatePolicy/'.$private_policy['title_url']);?>">Privacy Policy</a> and I agree to the <a class="popup_call" onclick="return false"  href="<?php echo route_to('user/getPrivatePolicy/'.$disclaimer['title_url']);?>">Terms of Use.</a>
                </label>
                <div id="agree-error" class="form-error"></div>

                <label>
                    <input id="is_subscribed" class="checkbox validation-passed" type="checkbox" value="1" name="is_subscribed">
                    I wish to receive periodical newsletters from E-Beauty Trade<sup></sup>.
                </label>
                <div id="is_subscribed-error" class="form-error"></div>
                <div class="FormResult"></div>
                <div class="narrow">
                    <input type="submit" value="<?php echo lang('register');?>" class="mrg-top-15">

                    <p class="black-link text-center mrg-top-25">
                        Already registered?
                        <a class="underlined" href="#login">Login here</a>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<!--<script src="intlTelInput.js"></script>-->
<script type="text/javascript">
    $(document).ready(function(){
        //country phone code drop down
      //  $("#demo").intlTelInput();  
        //end phone drop down
        //ip lookup
        $("#demo").intlTelInput({
          defaultCountry: "auto",
          geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
          
        });

        $('#result').on('click', 'li', function () {
            var country = $(this).text();
            var country1 = $(this).attr("id-countries");
            if (country != 'No Country Found'){
                $("#country_drop_down").val(country);
                $("#country").val(country1);
            }
            $('#result').hide();
        });


        $('#country_drop_down').keyup(function(e) {

            var title=$("#country_drop_down").val();

            if(title!=""){
                $.ajax({
                    type:"post",
                    url:"https://www.ebeautytrade.com/user/country_dropdown",
                    data:"title="+title,
                    success:function(data){
                        $('#result').show();
                        $("#result").html(data);
                        $("#country_drop_down").val("");
                    }
                });
            }
        });
    });
</script>