
<div class="row">
  <div class="tabs_cont mod">
  <?php $this->load->view('blocks/dashboard_tabs');?>
<div class="tab_container dashboard account-create">

    <div  class="tab_content">
    <div class="row">
    <div class="add_new_address">
<h2>Add to My Wallet</h2>
<a class="back-link" href="<?php echo route_to('user/credits');?>">
<i class="fa fa-fw"></i>
Back
</a>
</div>

 <form id="creditsForm" method="post" accept-charset="utf-8" action="<?php echo route_to('user/submit_credits');?>" >
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" ><strong>Amount(<?php echo $this->session->userdata('currency')?>)</strong><em>*</em> </label>
          
          <?php $petty_cash=$this->fct->getAll('petty_cash','sort_order asc');?>
          <select name ="amount" class="textbox selectbox">
          <?php foreach($petty_cash as $val){?>
        <option value="<?php echo changeCurrency($val['title'],false);?>"><?php echo changeCurrency($val['title']);?></option>
          <?php } ?>
          </select>
          
          <span id="amount-error" class="form-error"></span> </div>
               
</div>
<!--ROW 1-->
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >First Name<em>*</em> </label>
          <input class="textbox" type="text" name="first_name" value="<?php echo $userData['first_name'];?>"  >
          <span id="first_name-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >Last Name<em>*</em> </label>
          <input class="textbox" type="text" name="last_name" value="<?php echo $userData['last_name'];?>" >
          <span id="last_name-error" class="form-error"></span> </div>               
</div>


<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >E-mail<em>*</em> </label>
          <input class="textbox" type="text" name="email" value="<?php echo $userData['email'];?>" >
          <span id="email-error" class="form-error"></span> </div>  
          
 <div class="form-item gutter-right">
          <label class="required" >Street<em>*</em> </label>
          <input class="textbox" type="text" name="street_one" value=""  >
         <span id="street_one-error" class="form-error"></span>
	<input class="textbox" type="text" style="margin-top:5px;" name="street_two" value=""> </div>             
</div>

<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >City<em>*</em> </label>
          <input class="textbox" type="text" name="city" value="<?php echo $userData['city'];?>"  >
          <span id="city-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >State<em>*</em> </label>
          <input class="textbox" type="text" name="state" value="<?php echo $userData['state'];?>" >
          <span id="state-error" class="form-error"></span> </div>               
</div>
<?php /*echo "<pre>";
print_r($userData);exit; */?>
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >Zip/Postal Code</label>
          <input class="textbox" type="text" name="postal_code" value=""  >
          <span id="postal_code-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >Country<em>*</em> </label>
          <select name ="country" class="textbox selectbox">
           <option value="">-Select-</option>
          <?php foreach($countries as $country){?>
          <option <?php if($userData['id_countries']==$country['id_countries']) echo "selected"; ?> value="<?php echo $country['id_countries'];?>"><?php echo $country['title'];?></option>
          <?php } ?>
          </select>
          <span id="country-error" class="form-error"></span> </div>               
</div>



<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" >Telephone</label>
                <!--  <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="phone[0]" value="<?php echo getPhoneField($userData['phone'],0);?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="phone[1]" value="<?php echo getPhoneField($userData['phone'],1);?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="phone[2]" value="<?php echo getPhoneField($userData['phone'],2);?>" >-->
           <input class="textbox mobile-m" type="tel" name="phone" value="<?php echo cleanPhone($userData['phone']);?>" >
          <span id="phone-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" >Fax</label>
           <!--  <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="fax[0]" value="<?php echo getPhoneField($userData['fax'],0);?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="fax[1]" value="<?php echo getPhoneField($userData['fax'],1);?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="fax[2]" value="<?php echo getPhoneField($userData['fax'],2);?>" >-->
          <input class="textbox mobile-m" type="tel" name="fax" value="<?php echo cleanPhone($userData['fax']);?>" >
          <span id="fax-error" class="form-error"></span> </div>               
</div>

<label class="lbl_required mod">* Required Fields</label>

<div class="buttons-set">
  <div id="loader-bx"></div>
      <div class="FormResult"> </div>
          <input class="btn" title="Create an Account" type="submit" value="<?php echo lang('submit_information');?>">
           </div>  
    
 </form>
 <div id="credits_message"></div>
 </div>
    </div>
  </div></div>

