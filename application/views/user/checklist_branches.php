
<div class="innerContent">
<div class="page-title border"><h1>Stock-list Branches </h1><a class="back-link"  href="<?php echo route_to('user/checklist');?>" > <i class="fa fa-fw"></i> Back </a></div>
<div class="my-account dashboard">
          <div class="page-title">
<?php 
$data['breadcrumbs']=$breadcrumbs;
$this->load->view('blocks/breadcrumbs',$data);?>
<?php if(!isset($my_branch) || empty($info)){?>
            <a class="btn fr" type="button" title="Add New Branch" href="<?php echo route_to('user/branch/my-branch');?>">Add Branch</a> <?php } ?>
           </div>
          <div class="row">
            <div class="shopping_cart_cont">
            <div class="table-b">
            <div class="n-table">
  
      
      
              <?php if(!empty($info)){
			  foreach($info as $branch){ ?>
              <div class="branch-cont">
             <!-- <h2 class="h2_branch"><?php echo $branch['title'];?></h2>-->
              <table border="1" cellpadding="0" cellspacing="0" class="shopping_cart_table" width="100%">
           
          <tr><th  colspan="7"><?php echo $branch['title'];?>
    <!--    <a class="popup_call_checklist edit_branch" onclick="return false" href="<?php echo route_to('user/checklist/branches/'.$branch['id_checklist_branches']); ?>">Selected Products</a>--> 
       <a class="edit_branch" onclick="if(confirm('Are you sure you want delete this brand ?')){ delete_row('checklist_branches','<?php echo $branch['id_checklist_branches'];?>'); }" tiltle="Delete" alt="Delete">
<i class="fa fa-fw"></i>
Delete
</a>  

       <a href="<?php echo route_to('user/branch/'.$branch['id_checklist_branches']); ?>" class="edit_branch" tiltle="Edit" alt="Edit">
<i class="fa fa-fw"></i>
Edit
</a>

</th> </tr>
                <!--////////////////////////////CATEGORIES/////////////////-->
     
               
                  <?php if(isset($branch['category']) && !empty($branch['category'])){
					  $categories=$branch['category']; ?> 
        <tr class="tr-prdocust-category">
          		<td style="width:30% !important; text-align:left;" colspan="2">Product</td>
                <td style="text-align:center;">1 Month Quantity</td>
                <td width="130px" style="text-align:center;">Quantity To Order</td>
                <td width="80"><?php echo lang('suggester_retail_price');?></td>
                <td width="80">Unit Price</td>
              </tr>
				  <?php foreach($categories as $cat){
					  $info_section= $cat?>
                        <!--////////////////////////////products/////////////////-->
                        <!--<tr class="tr-category-title"><td style="text-align:center;" colspan="7"><h2 class="category-branch-title"><?php echo $cat['title'];?></h2></td> </tr>-->
					 <?php  $selected_section=$cat['selected_checklist_categories'];
			
					  if(!empty($selected_section)){ ?>
                      
					 
        
          
              <?php 
if(!empty($user_favorites)){ ?>
            <!--  <tr class="tr-prdocust-category">
          		<td style="width:30% !important;" colspan="2">Product</td>
                <td style="text-align:center;">1 Month Quantity</td>
                <td width="130px" style="text-align:center;">Quantity To Order</td>
                <td width="80"><?php echo lang('suggester_retail_price');?></td>
                <td width="80">Unit Price</td>
              </tr>-->
          
              <?php 

$i=0;foreach($user_favorites as $fav) {
	
	$first_available_stock=$fav['product_stock'];
	if(isset($first_available_stock) && !empty($first_available_stock)) {
		$quantity = $first_available_stock['quantity'];
		
		$list_price = $first_available_stock['list_price'];
		$price = $first_available_stock['price'];
		$retail_price = $first_available_stock['retail_price'];
		$discount = $first_available_stock['discount'];
		$discount_expiration = $first_available_stock['discount_expiration'];
		$stock_options = unSerializeStock($first_available_stock['combination']);
		$hide_price = $first_available_stock['hide_price'];
		$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$first_available_stock['id_products'],"id_stock"=>$first_available_stock['id_products_stock']));
	}
	else {
		$quantity = $fav['quantity'];
		//echo $quantity;exit;
		$list_price = $fav['list_price'];
		$retail_price = $fav['retail_price'];
		$price = $fav['price'];
		$discount = $fav['discount'];
		$discount_expiration = $fav['discount_expiration'];
		$hide_price = $fav['hide_price'];
		$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$fav['id_products']));
	}

?>



              <?php $selected=false; ?>
        
                <?php
$cl = '';
$selected=false;
$checklist_section=array();
$table="checklist_categories";
if(in_array($fav['id_favorites'],$selected_section)){ 
$cl = 'checked="checked"';
$selected=true;
$cond['id_favorites']=$fav['id_favorites'];
$cond['id_user']=$user['id_user'];
$cond['id_'.$table]=$info_section['id_'.$table];
$checklist_section=$this->fct->getonerecord('user_'.$table,$cond);

if($checklist_section['qty']>0){
	$fav['qty']=$checklist_section['qty'];}
	
if($checklist_section['qty_month']>0){
	$fav['qty_month']=$checklist_section['qty_month'];}	
}

 ?>
 

<?php if($selected){?>
              <tr id="item_<?php echo $fav['id_favorites'];?>" class="first last odd">
             
                <td style="border-right:none;">
                <?php if(isset($fav['gallery'][0]['image'])){?>
                <a class="product-image" title="<?php echo $fav['title'] ;  ?>" href="<?php echo route_to('products/details/'.$fav['id_products']);?>"> <img  alt="<?php echo $fav['title'] ;  ?>" src="<?php echo base_url();?>uploads/products/gallery/120x120/<?php echo $fav['gallery'][0]['image'] ;  ?>" /> </a>
                <?php }else{ ?>
                <img src="<?php echo base_url();?>front/img/default_product.png" />
                <?php } ?>
       		   </td>
                
<td >
                <h3 class="product-name"> 
                <a title="<?php echo $fav['title'] ;  ?>" href="<?php echo route_to('products/details/'.$fav['id_products']);?>">
				<?php echo $fav['title'] ;?></a> </h3>
                  
                  <?php 
if(isset($fav['product_attributes_options']) && !empty($fav['product_attributes_options'])) {
	
	$product_options=$fav['product_attributes_options'];?>
                  <div class="truncated">
                    <div class="truncated_full_value">
                      <div class="item-options mod">
                        <dl>
                          <?php foreach($product_options as $option){ ?>
                          <dt><?php echo $option['attribute_name'];?></dt>
                          <dd><?php echo $option['option_title']; ?> </dd>
                          <?php } ?>
                        </dl>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                
                </td>
                  
                  <td style="text-align:center;">
  
   <?php echo $fav['qty_month']; ?>
    
    <label class="qty-label" >Reference only</label>
    </td>
           
    <td style="text-align:center;">
   <?php echo $fav['qty']; ?></td>
   <td>
    <div class="row retail_price">
<?php if(!empty($retail_price) && $retail_price>0){?>
<label><strong><?php echo lang('suggester_retail_price');?>:</strong> </span><?php echo changeCurrency($retail_price);?></label>
<?php }else{ echo "-"; }?>
</div>
    </td>
    <td>
    <div class="price-box"> <span  class="regular-price">
                      <?php if($hide_price == 0) {?>
                      <?php if(displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price,$discount_expiration,$price)) {		                        ?>
                      <span class="price old_price" itemprop="price"><?php echo changeCurrency(displayWasCustomerPrice($list_price)); ?></span> <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span>
                      <?php } else {?>
                      <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span>
                      <?php }?>
                      <?php } ?>
     </span> </div>
    </td>	  

              </tr>
              <?php } ?><?php } ?>
              <?php } else {?>
               <tr class="">
                <td colspan="5" style="text-align:center;"><?php echo lang('no_favorites'); ?></td>
              </tr>
              <?php }?>
         
                      
                      <?php }else{ ?>
                       <!--<tr><td style="text-align:center" colspan="7"><div class="empty">No products found</div></td></tr>-->
                      <?php } ?>
					 <?php }}else{ ?>
                
                      <tr><td style="text-align:center" colspan="7"><div class="empty">No products found</div></td></tr>
                  
                  <?php } ?>
                
	
                 
          
                        </table>
             </div>
                <?php } ?>
              <?php }else{ ?>
         
  <div class="cart-empty"> 
You have no branches in your Stock-List. Please click at <a href="<?php echo route_to('user/branch/my-branch');?>">add branch</a> button to add new branch.
  </div>
<?php /*?>    <?php $brands=$this->custom_fct->getBrands(array('set_as_featured'=>1,'status'=>1),8,0);?>
  <div class="brand_dropdown_slick_cont">
<div class="brand_dropdown_slick">
<?php $b=0; foreach($brands as $brand_h){$b++;?>
<?php if(!empty($brand_h['logo'])){?>
<div class="item">
<div class="innerItem">
 <?php if($b!=1){ ?>
<span class="border"></span> <?php } ?> 
<div class="imgTable"><a class="imgCell" href="<?php echo route_to('brands/details/'.$brand_h['id_brands']);?>">
<span>
<img title="<?php echo $brand_h['title'];?>" alt="<?php echo $brand_h['title'];?>"  src="<?php echo base_url();?>uploads/brands/<?php echo $brand_h['logo'];?>" />
</span>
</a></div>

</div>
</div>
<?php } ?>
<?php } ?>

</div>

</div><?php */?>

 <?php } ?>
               
      
              </div>
            </div>
          </div>
            
          </div>
        </div>
   </div>

