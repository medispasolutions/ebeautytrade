
<div class="innerContent">
<div class="page-title border"><h1>Stock-list Branches </h1><a class="back-link"  href="<?php echo route_to('user/checklist');?>" > <i class="fa fa-fw"></i> Back </a></div>
<div class="my-account dashboard">
          <div class="page-title">
<?php 
$data['breadcrumbs']=$breadcrumbs;
$this->load->view('blocks/breadcrumbs',$data);?>
            <a class="btn fr" type="button" title="Add New Branch" href="<?php echo route_to('user/branch');?>">Add Branches</a>
           </div>
          <div class="row">
            <div class="shopping_cart_cont">
            <div class="n-table">
              <table border="1" cellpadding="0" cellspacing="0" class="shopping_cart_table">
                <tr>
           
                  <th>Branch Name</th>
                  <th>Date Added</th>
	
                  <th style="width:30%;text-align:center;"><?php echo lang('order_operations'); ?></th>
                </tr>
                <?php if(!empty($info)) {?>
                <?php foreach($info as $val) {

$created_date=date("j-F-Y",strtotime($val['created_date']));

?>

                <tr>
             
                  <td  style="text-align:left;"><?php echo $val['title']; ?></td>
                  <td><?php echo $created_date; ?></td>
                  
                  
                  
                  
                  <td style="text-align:center;">
                  <a class="btn_manage_checklist"  href="<?php echo route_to('user/checklist/branches/'.$val['id_checklist_branches']); ?>"> 
                 Selected Products</a>
                  <a alt="Edit" tiltle="Edit"  href="<?php echo route_to('user/branch/'.$val['id_checklist_branches']); ?>"> 
                 <i class="fa fa-fw"></i>Edit</a>
                    <a alt="Delete" tiltle="Delete" onclick="if(confirm('Are you sure you want delete this branch ?')){ delete_row('checklist_branches','<?=$val["id_checklist_branches"];?>'); }" > 
                    <i class="fa fa-fw"></i>Delete</a></td>
                    
                </tr>
        
                <?php }?>
                <?php } else {?>
                <tr>
                  <td colspan="7" align="center">No Branches</td>
                </tr>
                <?php }?>
              </table>
              </div>
            </div>
            
          </div>
        </div>
   </div>

