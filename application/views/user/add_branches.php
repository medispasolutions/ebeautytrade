<script>
function removeRow(section,field,id)
{

	if(confirm('Are you sure you want to delete this record ?')) {
		
		$('#section_repeat_'+id).html('<tr><td colspan="5">Loading...</td></tr>');
		
		var url = "<?php echo site_url(); ?>user/delete_row/"+section+"/"+id;
		$.post(url,{section : section,field : field,id : id},function(data){
		
			  $('#section_repeat_'+id).remove(); 
			  var i=0;
			    $('.categories-section tr').each(function(){i++;
					var obj=$(this);
					obj.find('td:first-child').html(i);
					});
			  
		});
	}
	else {
		return false;
	}
}

</script>

<?php


if(isset($branch) && !empty($branch)) {
	
		$buttonLabel = lang('update');
	$formAction = site_url('user/submitBranch');
	
	$pStar = '';
	$formID = 'updateBranch';
	$menuBlock = "user/account_menu";
	$update=true;
	$title='Edit Branch';
}
else {
	$update=false;
	$branch['first_name'] =$userData['first_name'];
	$branch['last_name'] =$userData['last_name'];
	$branch['company'] = '';
	$branch['phone'] = '';

	$branch['fax'] = '';
	$branch['street_one'] = '';
	$branch['branch_name'] = '';
	$branch['id_checklist_branches'] = '';
	$branch['street_two'] = '';
	$branch['state'] = '';
	$branch['city'] = '';
	$branch['country'] = '';
	$branch['id_countries'] = '';
	$branch['postal_code'] = '';
	$branch['billing'] = '';
	$branch['shipping'] = '';
	
	$buttonLabel = lang('submit');
	
	$formAction = route_to('user/submitBranch');
	$pStar = ' *';
	$formID = 'branchForm';
	$menuBlock = "user/user_menu";
	$title='Add New Branch';
}


?>

<?php




?>

<div class="innerContent add_branch account-create">
<div class="page-title border"><h1><?php echo $title;?> </h1><a class="back-link"  href="<?php echo route_to('user/branches');?>" > <i class="fa fa-fw"></i> Back </a></div>
<?php /*?><?php if(isset($branch['id_checklist_branches']) && !empty($branch['id_checklist_branches'])) {?>
<a class="btn fr" type="button" title="Selected Products"  href="<?php echo route_to('user/checklist/branches/'.$branch['id_checklist_branches']); ?>">Selected Products</a>
<?php } ?><?php */?>
<?php 
$data['breadcrumbs']=$breadcrumbs;
$this->load->view('blocks/breadcrumbs',$data);?>

          <form id="<?php echo $formID;?>" method="post" accept-charset="utf-8" action="<?php echo $formAction ;?>" >
          <input type="hidden" name="my_branch" value="<?php echo $my_branch;?>" />
             
<?php
if(isset($branch['id_users_addresses']) && !empty($branch['id_users_addresses'])) {?>
 <input class="textbox" type="hidden" name="id_users_addresses" value="<?php echo $branch['id_users_addresses'];?>" >
 <?php } ?>
        <div class="my-account">
          
          <div class="box-account box-info">
            <div class="box-head">
              <h2>Contact Information</h2>
            </div>
          
              <!--ROW 1-->
              <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" > First Name <em>*</em> </label>
                  <input class="textbox" type="text" name="first_name" value="<?php echo $branch['first_name'];?>" >
                  <span id="first_name-error" class="form-error"></span> </div>
                <div class="form-item gutter-right">
                  <label class="required" > Last Name <em>*</em> </label>
                  <input class="textbox" type="text" name="last_name" value="<?php echo $branch['last_name'];?>" >
                  <span id="last_name-error" class="form-error"></span> </div>
              </div>
              
              <!--ROW 2-->
              <div class="row_form">
              <div class="form-item  gutter-right">
                  <label class="required" >Branch Name <em>*</em></label>
                  <input class="textbox" type="text" name="branch_name" value="<?php echo $branch['branch_name'];?>" >
                  <span id="branch_name-error" class="form-error"></span> </div>
              
                <div class="form-item  gutter-right">
                  <label class="required" >Company </label>
                  <input class="textbox" type="text" name="company" value="<?php echo $branch['company'];?>" >
                  <span id="company-error" class="form-error"></span> </div>
              </div>

              <!--ROW 3-->
              <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Telephone <em>*</em> </label>

         <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="phone[0]" value="<?php echo getPhoneField($branch['phone'],0);?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="phone[1]" value="<?php echo getPhoneField($branch['phone'],1);?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="phone[2]" value="<?php echo getPhoneField($branch['phone'],2);?>" >
                  
                  <span id="phone-error" class="form-error"></span> </div>
                <div class="form-item gutter-right">
                  <label class="required" for="fax">Fax </label>
             <input class="textbox phone_1" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code"  style="width:50px; margin-left:2px;" name="fax[0]" value="<?php echo getPhoneField($branch['fax'],0);?>" >
         <input class="textbox phone_2" type="text" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Area Code"  style="width:50px; margin-left:2px;" name="fax[1]" value="<?php echo getPhoneField($branch['fax'],1);?>" >
         <input class="textbox phone_3" type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number"  style="width:150px; margin-left:2px;" name="fax[2]" value="<?php echo getPhoneField($branch['fax'],2);?>" >
                  <span id="fax-error" class="form-error"></span> </div>
                    
              </div>

        
       
          </div>
        </div>
        <div class="box-account box-info address_info">
          <div class="box-head">
            <h2>Address</h2>
          </div>
            
            <!--ROW 4-->
            <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" >Street Address <em>*</em> </label>
                  <input class="textbox" type="text" name="street_one" value="<?php echo $branch['street_one'];?>" >
                  <span id="street_one-error" class="form-error"></span> 
                  <input class="textbox gutter-top" type="text" name="street_two" value="<?php echo $branch['street_two'];?>" ></div>
              </div>
            
            <!--ROW 5-->
            <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" > City <em>*</em> </label>
                  <input class="textbox" type="text" name="city" value="<?php echo $branch['city'];?>" >
                  <span id="city-error" class="form-error"></span> </div>
                <div class="form-item gutter-right">
                  <label class="required" > State/Province </label>
                  <input class="textbox" type="text" name="state" value="<?php echo $branch['state'];?>" >
                  <span id="state-error" class="form-error"></span> </div>
              </div>
           
        
            <!--ROW 6-->
            <div class="row_form">
                <div class="form-item gutter-right">
                  <label class="required" > Zip/Postal Code </label>
                  <input class="textbox" type="text" name="postal_code" value="<?php echo $branch['postal_code'];?>" >
                  <span id="postal_code-error" class="form-error"></span> </div>
                <div class="form-item gutter-right">
                  <label class="required" >Country <em>*</em> </label>
           <select id="country" class="textbox selectbox" name="id_countries" title="Country">
<option value="">--Select Country--</option>
<?php foreach($countries as $country){?>
<option value="<?php echo $country['id_countries'];?>"  <?php if($country['id_countries']==$branch['id_countries']) echo " selected";?>><?php echo $country['title'];?></option>
<?php } ?>

</select>
                  <span id="id_countries-error" class="form-error"></span> </div>
              </div>
 <!--Categories--> 
           <div class="my-account" >
          
          <div class="box-account box-info">
            <div class="box-head">
              <h2>Categories</h2>
            </div>
          <div class="categories-section">
          <div class="inner-categories-section">
              <!--ROW 1-->
              <table class="table-categories input_fields_wrap" cellpadding="5" cellspacing="5">
              <?php $i=0; if(isset($categories) && !empty($categories)){ ?>
              <?php foreach($categories as $val) { $i++;?>
              <tr id="section_repeat_<?php echo $val["id_checklist_categories"];?>"><td style="width:40px;"><?php echo $i;?></td>
              <td><div class="form-item">
                  <input class="textbox" type="text" name="categories[<?php echo $val["id_checklist_categories"];?>]" value="<?=$val['title'];?>">
                  <span id="first_name-error" class="form-error"></span> </div>
                  <a class="popup_call_checklist selected_products" onclick="return false" href="<?php echo route_to('user/checklist_categories/'.$val["id_checklist_categories"]); ?>">Selected Products</a></td><td>
                  <a  tiltle="Delete" alt="Delete" onclick='removeRow("checklist_categories","id_checklist_categories","<?php echo $val["id_checklist_categories"]; ?>")'><img src="<?php echo base_url();?>front/img/delete.png" /></a></td></tr>
                  <?php } ?> <?php }else{ $i++;?>
                  <?php $rand=rand().'a';?>
                  <tr id="section_repeat_<?php echo $rand;?>"><td style="width:40px;"><?php echo $i;?></td>
              <td><div class="form-item">
                  <input class="textbox" type="text" name="categories[<?php echo $rand;?>]" value="">
                  <span id="first_name-error" class="form-error"></span> </div></td><td><a tiltle="Delete" alt="Delete"  onclick='removeRow("checklist_categories","id_checklist_categories","<?php echo $rand; ?>")'><img src="<?php echo base_url();?>front/img/delete.png" /></a></td></tr>
                  <?php }?>
                  
              </table>
              <a class="add_more add_field_button">[+Add More]</a>
              </div>
              </div>
              
              
              
         
              

        
       
          </div>
        </div> 
              
            <!--ROW 7--> 
            <div class="row_form" >
<div class="form-item gutter-right">
          <label class="required" for="password">Please type the letters below <em>*</em> </label>
          <span class="captchaImage"><?php echo $this->fct->createNewCaptcha(); ?></span>
		  <input type="text"  class="textbox" id="captcha" name="captcha" value="" style="text-transform:none" />
		<span id="captcha-error" class="form-error"></span> </div>               
</div>
              
              
        
        </div>
        
        
        
      <label class="lbl_required mod">* Required Fields</label>
<div class="buttons-set">
<div id="loader-bx"></div>
<div class="FormResult"> </div>
<input class="btn" type="submit" value="<?php echo lang('submit_information');?>" title="Create an Account">
</div>
        </form>
          </div>
