<div id="loginAndRegistrationPage" class="container">
    <h1>Welcome to E-Beauty Trade</h1>
    <p class="lead">Your B2B Marketplace for Spas, Salons, Beauty Clinics and Retailers</p>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <a name="login"></a>
            <h3 class="login-heading">Existing Member</h3>
            <p class="section-lead">If you have already registered, please login here with your email address and password.</p>
        
            <form id="login" method="post" accept-charset="utf-8" action="<?php echo route_to('user/validate') ;?>" class="mrg-top-25">
                <div class="narrow">
                    <input type="email" placeholder="E-mail address" name="email">
                    <div id="email-error" class="form-error"></div>
        
                    <input type="password" placeholder="Password" name="password">
                    <div id="password-error" class="form-error"></div>
        
                    <p class="blue-link">
                        <a href="<?php echo route_to('user/password');?>">Forgot Your Password?</a>
                    </p>
        
                    
        
                    <input type="submit" value="<?php echo lang('login');?>" class="mrg-top-25">
                    <div class="FormResult"></div>

                    <p class="black-link text-center">
                        New Customer?
                        <a class="underlined" href="#register">Register here</a>
                    </p>
                </div>
            </form>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 left-border-realted">
            <h3 class="login-heading"><a name="register"></a>Create an Account</h3>
            <P>Sign up to access authentic wholesale prices and connect with suppliers directly.</P>
            <h4>Business Details</h4>

            <form id="registration" method="post" accept-charset="utf-8" action="<?php echo route_to('user/submit') ;?>" class="mrg-top-15">
                <div class="narrow">
                    <input type="text" placeholder="Business name as per Trade License" name="trading_name">
                    <div id="trading_name-error" class="form-error"></div>
										
										<input type="text" placeholder="Trade License No." name="trade_license_no">
                    <div id="trading_license_no-error" class="form-error"></div>

                    <div class="bissiness-realtes">
                        <select name="b_type">
                            <option value="0">Business Type</option>
							<option value="Day Spa">Day Spa</option>
                            <option value="Beauty Salon">Beauty Salon</option>
							<option value="Hair Salon">Hair Salon</option>
							<option value="Nail Salon">Nail Salon</option>
                            <option value="Gents Salon">Gents Salon</option>
                            <option value="Beauty Clinic">Beauty Clinic</option>
							<option value="Retail Store">Retail Store</option>
							<option value="Others">Others</option>
							
						</select>
                    </div>
                                    <input type="text" placeholder="Address" name="address">
                    <div id="address-error" class="form-error"></div>
                                        
                                        <input type="text" placeholder="City" name="city">
                    <div id="city-error" class="form-error"></div>
                                        
                    <input type="text" id="country_drop_down" placeholder="Country" name="country1">
                    <ul id="result" class="result"></ul>
                    <div id="Country-error" class="form-error"></div>
                    <input type="hidden" id="country" placeholder="Country" name="country">

                    <input type="text" placeholder="Phone" name="phone" class="mobile-m">
					<div id="phone-error" class="form-error"></div>

                     

                    <h4>Authorized Person</h4>
                                        
                    <input type="text" placeholder="First Name" name="first_name">
                    <div id="first_name-error" class="form-error"></div>
										
										<input type="text" placeholder="Family Name" name="family_name">
                    <div id="last_name-error" class="form-error"></div>
										
						
					
										<input type="text" placeholder="Position" name="position" >
                    <div id="phone-error" class="form-error"></div>

                    <h4>Login Details</h4>
        
                    <input type="email" placeholder="E-mail address" name="email">
                    <div id="email-error" class="form-error"></div>
										
										<input type="email" placeholder="Confirm E-mail address" name="confirm_email">
                    <div id="confirm_email-error" class="form-error"></div>
        
                    <input type="password" placeholder="Desired Password" name="password">
                    <div id="password-error" class="form-error"></div>

                    <input type="password" placeholder="Confirm Password" name="con_password">
                    <div id="con_password-error" class="form-error"></div>
        
        
                    
                </div>
        
                <p>This is a trade only marketplace. By submitting the information above you admit that you are legally authorized to act on behalf of your Company.</p>
        
                <label class="mrg-top-25">
                    <input id="agree" class="checkbox validation-passed" type="checkbox" value="1" name="agree">
                    Yes, I have read and understood the <a class="popup_call" onclick="return false" href="<?php echo route_to('user/getPrivatePolicy/'.$private_policy['title_url']);?>">Privacy Policy</a> and I agree to the <a class="popup_call" onclick="return false"  href="<?php echo route_to('user/getPrivatePolicy/'.$disclaimer['title_url']);?>">Terms of Use.</a>
                </label>
                <div id="agree-error" class="form-error"></div>
        
                <label>
                    <input id="is_subscribed" class="checkbox validation-passed" type="checkbox" value="1" name="is_subscribed">
                    I wish to receive periodical newsletters from E-Beauty Trade<sup></sup>.
                </label>
                <div id="is_subscribed-error" class="form-error"></div>
                <div class="FormResult"></div>
                <div class="narrow">
                    <input type="submit" value="<?php echo lang('register');?>" class="mrg-top-15">
        
                    <p class="black-link text-center mrg-top-25">
                        Already registered?
                        <a class="underlined" href="#login">Login here</a>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#result').on('click', 'li', function () {
            var country = $(this).text();
            var country1 = $(this).attr("id-countries");
            if (country != 'No Country Found'){
                $("#country_drop_down").val(country);
                $("#country").val(country1);
            }
                $('#result').hide();
        });


        $('#country_drop_down').keyup(function(e) {

           var title=$("#country_drop_down").val();

            if(title!=""){

                $.ajax({
                    type:"post",
                    url:"<?php echo route_to('countrydropdown') ;?>",
                    data:"title="+title,
                    success:function(data){
                        $('#result').show();
                        $("#result").html(data);
                        $("#country_drop_down").val("");
                    }
                });
            }
        });





    });
</script>