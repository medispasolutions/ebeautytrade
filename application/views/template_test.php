<?php 
	$meta_description = '';
	$meta_keywords = '';
	$url = "http" . (($_SERVER['SERVER_PORT']==443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	if(!isset($seo)) {
		$seo = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>1));
	}
	if($this->session->userdata('currency') == "") {
		$this->session->set_userdata('currency',$this->config->item("default_currency"));
	}
?>
<!DOCTYPE html>
<html>
<head>

<!-- Google Tag Manager -->

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-NC5DXCK');</script>

<!-- End Google Tag Manager -->

<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="HandheldFriendly" content="true">-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if(isset($seo['meta_title']) && !empty($seo['meta_title'])) {?>
<title><?php echo $seo['meta_title']; ?></title>
<?php } else {?>
<title>Spamiles</title>
<?php }?>
<?php if(isset($seo['meta_description']) && !empty($seo['meta_description'])) {?>
<meta name="description" content="<?php echo $seo['meta_description']?>" />
<?php }?>
<?php if(isset($seo['meta_keywords']) && !empty($seo['meta_keywords'])) {?>
<meta name="keywords" content="<?php echo $seo['meta_keywords']?>" />
<?php }?>

<?php if(isset($og_image)) {?>
<meta property="og:image" content="<?php echo $og_image; ?>" /> 
<?php } else {?>
<meta property="og:image" content="<?php echo base_url()?>front/img/logo.png" /> 
<?php }?>

<meta property="og:url" content="<?php echo $url?>">

<link rel="shortcut icon" href="<?php echo base_url(); ?>front/img/favicon.ico" />
<!--<link rel='stylesheet' id=''  href="<?php echo base_url(); ?>front/css/bootstrap.min.css" type='text/css' media='all' />-->

<link rel='stylesheet'  href="<?php echo base_url(); ?>front/css/jquery-ui.css" type='text/css' media='all' />
<?php if($this->router->class=="user"){ ?>
<link rel='stylesheet' id=''  href="<?php echo base_url(); ?>front/css/multi-select.css" type='text/css' media='all' />
<?php } ?>
<link rel='stylesheet' id=''  href="<?php echo base_url(); ?>front/css/default.css" type='text/css' media='all' />
<link rel='stylesheet' id=''  href="<?php echo base_url(); ?>front/css/popup_r.css" type='text/css' media='all' />
<link href="<?php echo base_url(); ?>front/slick/slick.css" rel="stylesheet">

<?php if($this->router->class=="products" && $this->router->method="details"){ ?>
<link rel='stylesheet'  href="<?php echo base_url(); ?>front/css/transitions.css" type='text/css' media='all' />
<link rel='stylesheet' id='style-css-2'  href="<?php echo base_url(); ?>front/prettyphoto/css/prettyPhoto.css" type='text/css' media='all' />
<link rel='stylesheet' id='style-css-3'  href="<?php echo base_url(); ?>front/css/animate.css" type='text/css' media='all' />
<?php } ?>
<link rel='stylesheet' id=''  href="<?php echo base_url(); ?>front/awesome/css/font-awesome.css" type='text/css' media='all' />
<link rel='stylesheet' id='style-css-4'  href="<?php echo base_url(); ?>front/css/intlTelInput.css" type='text/css' media='all' />
<link rel='stylesheet' id=''  href="<?php echo base_url(); ?>front/css/jquery.mCustomScrollbar.css" type='text/css' media='all' />

<link rel='stylesheet' id='style-css-5'  href="<?php echo base_url(); ?>front/css/style.css" type='text/css' media='all' />
<link rel='stylesheet' id='style-css-5'  href="<?php echo base_url(); ?>front/css/style_new.css" type='text/css' media='all' />
<link rel='stylesheet'  href="<?php echo base_url(); ?>front/css/responsive.css" type='text/css' media='all' />

<?php echo $_styles; // default region for the css files ?>


</head>
<body lang="<?php echo $this->lang->lang(); ?>" class="page_<?php echo $this->router->class;?>">
<!-- Google Tag Manager (noscript) -->

<!--<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NC5DXCK"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>-->

<!-- End Google Tag Manager (noscript) -->
<!--<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78835717-1', 'auto');
  ga('send', 'pageview');

</script>-->





<?php if(!$this->session->userdata('session_id')) {$this->session->set_userdata('session_id',rand());}?>

<input type="hidden" id="siteurl" value="<?php echo site_url(); ?>" />
<input type="hidden" id="baseurl" value="<?php echo base_url(); ?>" />
<!--<div style="width:100%; float:left; padding:10px 0 0 0; color:red; font-size:15px; font-weight:bold; text-align:center">Website on Demo, Professional Online Store will be active very soon..</div>-->

<!--<div style="width:100%; float:left; padding:10px 0 0 0; color:red; font-size:15px; font-weight:bold; text-align:center">Going Live on August, 8 2016 / <span data-countdown="<?php echo changeToCountDownDate('2016-08-08'); ?>"></span></div>-->
<?php echo $header; ?>

<div class="row">
<div class="centered">

<ul class="messages">
<?php if($this->session->flashdata('success_message')) {?>
<li class="success-msg r-fullSide">
<ul>
<li>
<span><?php echo $this->session->flashdata('success_message');?></span>
</li>
</ul>
</li>
<?php } ?>
<?php if($this->session->flashdata('error_message')) {?>
<li class="error-msg r-fullSide">
<ul>
<li>
<span><?php echo $this->session->flashdata('error_message');?></span>
</li>
</ul>
</li>
<?php } ?>

<?php if($this->session->flashdata('notice_message')) {?>
<li class="notice-msg">
<ul>
<li>
<span><?php echo $this->session->flashdata('notice_message');?></span>
</li>
</ul>
</li>
<?php } ?>


</ul>
</div></div>



 <?php if(isset($success_messages) && !empty($success_messages)) {?>
 <div class="r-fullSide messages">
<div class="centered">
<div class="msgcontact">
<div class="success">
<?php 
echo $success_messages;   ?>
</div>
</div>
</div>
</div>
<?php
} 
 ?>
<?php if(isset($error_messages) && !empty($error_messages)) {?>
<div class="r-fullSide messages">
<div class="centered">
<div class="msgcontact">
<div class="error_msg">
<?php 
echo $error_messages;   ?>
</div>
</div>
</div>
</div>
<?php
} 
 ?>
 <?php echo $banner; ?>
 <?php if($this->router->class!="home"){?>
 <div class="row ">
   <div class="centered"> 
   <?php } ?>

          <?php if(!empty($quarter_left_sideBar)){ ?> 
    <div class="quarterLeftSideBar"> 
   <?php echo $quarter_left_sideBar; ?>
   </div>
   <?php } ?>
    <?php if(!empty($quarter_left_sideBar)){ ?> 
    <div class="middleSide"> 
    <div class="innerMiddleSide">
   <?php echo $content; ?>
   </div></div>
   <?php }else{ ?>
  <?php echo $content; ?>
   <?php } ?>

<?php if($this->router->class!="home"){?>
   </div></div>   <?php } ?>

<?php echo $bottom; ?> 
<?php echo $footer; ?>

<!--<div class="r-Shadow" onclick="closePopup()"></div>
<div id="r-PopupContainer">
	<div class="container">
    	<div class="content">
        	<div class="inner" id="r-PopupLoader">
            </div>
        </div>
    </div>
</div>-->


<?php $this->load->view('blocks/popup_r');?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-1.11.0.min.js"></script>
<?php if(($this->router->class=="products" && $this->router->method=="details") || $this->router->class=="brands"){ ?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-ui.js"></script>
<?php } ?>

<?php if($this->router->class=="user" && ($this->router->method="profile" || $this->router->method="register")){ ?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/profile.js"></script>
<?php } ?>
<script src="<?php echo base_url(); ?>front/custom/jquery.dow.form.validate.js"></script>
<script src="<?php echo base_url(); ?>front/custom/forms_calls.js"></script>
<script src="<?php echo base_url(); ?>front/slick/slick.js"></script>
<script src="<?php echo base_url(); ?>front/slick/calls.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/combobox.js"></script>
<?php if($this->router->class=="products" && $this->router->method=="details"){ ?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.zoom.js"></script>
<script src="<?php echo base_url(); ?>front/prettyphoto/js/jquery.prettyPhoto.js"></script>
<?php } ?>
<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/popup_js.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.zoom.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.mCustomScrollbar.concat.min.js"></script>
<?php if($this->router->class=="user"){ ?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-ias.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/multi-select.js"></script>
<?php } ?>
<?php if($this->router->class=="brands"){ ?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/readmore.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.dow.resize.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>front/js/intlTelInput.js"></script>
<?php if($this->router->class!="home"){ ?>
<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.countdown.min.js"></script>
<?php } ?>

<?php echo $_scripts; //  default region for the js files ?>

<script type="text/javascript" src="<?php echo base_url(); ?>front/js/myscript.js"></script>



<!--End of Tawk.to Script-->




<!--<script>
	if($('[data-countdown]').length>0){
		$('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
    $this.countdown(finalDate, function(event) {
     $this.html(event.strftime('%D <?php echo lang('days'); ?> %H:%M:%S'));
   });
 });}</script>-->


<script type="text/javascript">
_linkedin_data_partner_id = "32873";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>


<!--Start of Tawk.to Script-->
<!--<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/57d009da60af0d7233adc865/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>-->
</body>
</html>