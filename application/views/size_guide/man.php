<div>
 <div id="fb-root"></div>
<div class="page-title">
<h1>Mens's size guide</h1>
</div>
<div class="std"><div id="size-chart" class="popup-size-measure">
<ul>
<li class="open">The following charts will help you match the designer's size scheme for this product (indicated in the dropdown) to your usual size.
<ul class="content" style="display: block;">
<li>
<div id="tabBody1" class="tabContent tabBody size-conversion-body">
<div id="size_conversion">
<h1>Blazers chart</h1>
<table class="three-column"><thead><tr>
<th colspan="3" class="style3">Formal Jackets (Suit Jacket &amp; Blazers)<br>
Waistcoats</th></tr><tr><th class="style3"></th><th class="style4" colspan="2">
To fit chest Size</th></tr><tr><th class="style1"><strong>Size</strong></th><th>Inches</th>
<th class="style2">CM</th></tr></thead><tbody><tr><td class="style1">
<strong>32"</strong></td><td>32</td><td class="style2">81</td></tr><tr>
<td class="style1"><strong>34"</strong></td><td>34</td><td class="style2">86</td></tr><tr>
<td class="style1"><strong>36"</strong></td><td>36</td><td class="style2">91</td></tr><tr>
<td class="style1"><strong>38"</strong></td><td>38</td><td class="style2">96</td></tr><tr>
<td class="style1"><strong>40"</strong></td><td>40</td><td class="style2">101</td></tr><tr>
<td class="style1"><strong>42"</strong></td><td>42</td><td class="style2">106</td></tr><tr>
<td class="style1"><strong>44"</strong></td><td>44</td><td class="style2">111</td></tr><tr>
<td class="style1"><strong>46"</strong></td><td>46</td><td class="style2">116</td></tr></tbody></table>
<h1>Jackets and Coats chart</h1>
<table class="three-column"><thead><tr><th><strong>Size</strong></th><th colspan="2"><strong>
To fit Chest Size</strong></th></tr><tr><th></th><th class="style3">Inches</th>
<th class="style2">CM</th></tr></thead><tbody><tr><td><strong>XXXS</strong></td>
<td class="style3">
30-32</td><td class="style2">76-81</td></tr><tr><td><strong>XXS</strong></td>
<td class="style3">32-34</td><td class="style2">81-86</td></tr><tr><td><strong>XS</strong></td>
<td class="style3">34-36</td><td class="style2">86-91</td></tr><tr><td><strong>S</strong></td>
<td class="style3">36-38</td><td class="style2">91-96</td></tr><tr><td><strong>M</strong></td>
<td class="style3">38-40</td><td class="style2">96-101</td></tr><tr><td><strong>L</strong></td>
<td class="style3">40-42</td><td class="style2">101-106</td></tr><tr><td><strong>XL</strong></td>
<td class="style3">42-44</td><td class="style2">106-111</td></tr><tr><td><strong>XXL</strong></td>
<td class="style3">44-46</td><td class="style2">111-116</td></tr><tr><td><strong>XXXL</strong></td>
<td class="style3">
46-48</td><td class="style2">116-121</td></tr></tbody></table>
<h1>Belts chart</h1>
<table class="three-column">
<thead>
<tr>
<th class="style5"><strong>BELT SIZE&nbsp; </strong></th>
<th class="style6" colspan="2"><strong>CASUAL / FORMAL<br>
TROUSER SIZE </strong></th>
</tr>
</thead>
<tbody>
<tr>
<td class="style1"><strong>&nbsp;XXS-XS</strong></td>
<td class="style4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 26"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 28"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 29"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 30"</td>
</tr>
<tr>
<td class="style1"><strong>&nbsp;S-M</strong></td>
<td class="style4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 31"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 32"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 33"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 34"&nbsp;</td>
</tr>
<tr>
<td class="style1"><strong>&nbsp;L-XXL</strong></td>
<td class="style4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
36"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 38"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 40"</td>
</tr>
</tbody>
</table>
<h1>Caps and Hats chart</h1>
<table class="three-column">
<thead>
<tr>
<th class="style3">*Hats<br>
To fit head
<br>
Circumference</th>
<th class="style2">CM</th>
<th class="style1">INCHES</th>
</tr>
</thead>
<tbody>
<tr>
<td class="style3"><strong>S/M</strong></td>
<td class="style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 57</td>
<td class="style1">
<span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 22</span><sup><span class="style10" style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA">1</span></sup><span class="style9" style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA">⁄<sub class="style10">4</sub></span> </td>
</tr>
<tr>
<td class="style4"><strong>M/L</strong></td>
<td class="style5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 59</td>
<td class="style6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 23</td>
</tr>
<tr>
<td class="style4"><b>ONE SIZE</b></td>
<td class="style5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 58</td>
<td class="style6">
<span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;22</span><sup><span class="style10" style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA">5</span></sup><span class="style9" style="font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;color:black;mso-ansi-language:EN-US;mso-fareast-language:
EN-US;mso-bidi-language:AR-SA">⁄<sub class="style10">8</sub></span></td>
</tr>
</tbody>
</table>
<h1>Jeans & Trousers chart</h1>
<table class="three-column"><thead><tr><th><strong>Size</strong></th><th colspan="2"><strong>
To fit Waist Size </strong></th></tr><tr><th></th><th>Inches</th><th>CM</th></tr></thead><tbody><tr><td><strong>26"</strong></td><td>26</td><td>66</td></tr><tr><td><strong>28"</strong></td><td>28</td><td>71</td></tr><tr><td>
<strong>29"</strong></td><td>29</td><td>73.5</td></tr><tr><td><strong>30"</strong></td><td>30</td><td>76</td></tr><tr><td><strong>31"</strong></td><td>31</td><td>78.5</td></tr><tr><td><strong>32"</strong></td><td>32</td><td>81</td></tr><tr><td><strong>33"</strong></td><td>33</td><td>83.5</td></tr><tr><td><strong>34"</strong></td><td>34</td><td>86</td></tr><tr><td><strong>36"</strong></td><td>36</td><td>91</td></tr><tr><td><strong>38"</strong></td><td>38</td><td>96</td></tr><tr><td>
<strong>40''</strong></td><td>40</td><td>101</td></tr></tbody></table>
<h1>Shirts chart</h1>
<table class="three-column"><thead><tr><th><strong>Size</strong></th><th colspan="2">
To fit Chest
<br>
Size</th><th class="style1" colspan="2">To fit Neck Size</th></tr><tr><th></th>
<th class="style44">Inches</th>
<th class="style23">CM</th><th>Inches</th><th class="style4">CM</th></tr></thead><tbody><tr><td>
<strong>XXXS</strong></td><td class="style44">30-32</td><td class="style23">76-81</td>
<td class="style3">14</td><td class="style4">36</td></tr><tr><td><strong>XXS</strong></td>
<td class="style44">32-34</td>
<td class="style23">81-86</td><td class="style3">14.5</td><td class="style4">37.5</td></tr><tr><td><strong>XS</strong></td>
<td class="style44">34-36</td>
<td class="style23">86-91</td><td class="style3">15</td><td class="style4">38.5</td></tr><tr><td><strong>S</strong></td>
<td class="style44">36-38</td>
<td class="style23">91-96</td><td class="style3">15.5</td><td class="style4">39.5</td></tr><tr><td><strong>M</strong></td>
<td class="style44">38-40</td>
<td class="style23">96-101</td><td class="style3">16</td><td class="style4">41.5</td></tr><tr><td><strong>L</strong></td>
<td class="style44">40-42</td>
<td class="style23">101-106</td><td class="style3">17</td><td class="style4">43.5</td></tr><tr><td><strong>XL</strong></td>
<td class="style44">42-44</td>
<td class="style23">106-111</td><td class="style3">17.5</td><td class="style4">
45.5</td></tr><tr><td><strong>XXL</strong></td><td class="style44">44-46</td><td class="style23">111-116</td>
<td class="style3">18.5</td><td class="style4">47.5</td></tr><tr><td>
<strong>XXXL</strong></td><td class="style44">46-48</td><td class="style23">116-121</td>
<td class="style3">19.5</td><td class="style4">49.5</td></tr></tbody></table>
<h1>Footwear chart</h1>
<table class="four-column"><thead><tr><th><strong>UK Size</strong></th><th><strong>EU Size</strong></th><th><strong>US Size</strong></th><th><strong>Foot Length (mm)</strong></th></tr></thead><tbody><tr><td>
3</td><td>35.5</td><td>4</td><td>220</td></tr><tr><td>4</td><td>37</td><td>5</td><td>
229</td></tr><tr><td>5</td><td>38</td><td>6</td><td>237</td></tr><tr><td>6</td><td>
39</td><td>7</td><td>246</td></tr><tr><td>7</td><td>40.5</td><td>8</td><td>254</td></tr><tr><td>
7.5</td><td>41</td><td>8.5</td><td>258</td></tr><tr><td>8</td><td>42</td><td>9</td><td>262</td></tr><tr><td>
8.5</td><td>42.5</td><td>9.5</td><td>266</td></tr><tr><td>9</td><td>43</td><td>10</td><td>271</td></tr><tr><td>
9.5</td><td>44</td><td>10.5</td><td>275</td></tr><tr><td>10</td><td>44.5</td><td>11</td><td>279</td></tr><tr><td>
10.5</td><td>45</td><td>11.5</td><td>283</td></tr><tr><td>11</td><td>46</td><td>12</td><td>288</td></tr><tr><td>12</td><td>47</td><td>13</td><td>296</td></tr><tr><td>13</td><td>48</td><td>14</td><td>305</td></tr><tr><td>
14</td><td>49.5</td><td>15</td><td>314</td></tr></tbody></table>
<h1>Shorts & swimwear chart</h1>
<table class="three-column"><thead><tr><th><strong>Size</strong></th><th colspan="2">
To fit waist Size</th></tr><tr><th></th><th>Inches</th><th>CM</th></tr></thead><tbody><tr><td><strong>26"</strong></td><td>26</td><td>66</td></tr><tr><td><strong>28"</strong></td><td>28</td><td>71</td></tr><tr><td>
<strong>29"</strong></td><td>29</td><td>73.5</td></tr><tr><td><strong>30"</strong></td><td>30</td><td>76</td></tr><tr><td><strong>31"</strong></td><td>31</td><td>78.5</td></tr><tr><td><strong>32"</strong></td><td>32</td><td>81</td></tr><tr><td><strong>33"</strong></td><td>33</td><td>83.5</td></tr><tr><td><strong>34"</strong></td><td>34</td><td>86</td></tr><tr><td><strong>36"</strong></td><td>36</td><td>91</td></tr><tr><td><strong>38"</strong></td><td>38</td><td>96</td></tr>
<tr>
<td><strong>40''</strong></td><td>40</td><td>101</td>
</tr>
</tbody></table>
</div>
</div>
</li>
</ul>
</li>
</ul>
</div></div>

Powered By <a href="http://www.dowgroup.com" target="_blank">Dow Group</a><br/> </div>

