
<div>
 
<div id="fb-root"></div>

<div class="std"><div id="size-chart" class="popup-size-measure">
<ul>
<li class="open">The following charts will help you match the designer's size scheme for this product (indicated in the dropdown) to your usual size.
<ul class="content" style="display: block;">
<li>
<div id="tabBody1" class="tabContent tabBody size-conversion-body">
<div id="size_conversion">
<h1>Unisex Sizing</h1>
<table cellspacing="1" cellpadding="2" border="0">
<tbody><tr>
<td><div align="left">Size</div></td>
<td colspan="2"><div align="center">X-Small</div></td>
<td colspan="2"><div align="center">Small</div></td>
<td colspan="2"><div align="center">Medium</div></td>
<td colspan="2"><div align="center">Large</div></td>
<td colspan="2"><div align="center">X-Large</div></td>
<td colspan="2"><div align="center">XX-Large</div></td>
</tr>
<tr>
<td width="130"><div align="left">Your Measurements</div></td>
<td width="40"><div align="center">inch</div></td>
<td width="30"><div align="center">cm</div></td>
<td width="40"><div align="center">inch</div></td>
<td width="30"><div align="center">cm</div></td>
<td width="40"><div align="center">inch</div></td>
<td width="30"><div align="center">cm</div></td>
<td width="40"><div align="center">inch</div></td>
<td width="30"><div align="center">cm</div></td>
<td width="40"><div align="center">inch</div></td>
<td width="30"><div align="center">cm</div></td>
<td width="40"><div align="center">inch</div></td>
<td width="30"><div align="center">cm</div></td>
</tr>
<tr>
<td><div align="left">Chest</div></td>
<td><div align="center">34</div></td>
<td><div align="center">86</div></td>
<td><div align="center">37</div></td>
<td><div align="center">94</div></td>
<td><div align="center">40</div></td>
<td><div align="center">102</div></td>
<td><div align="center">43</div></td>
<td><div align="center">109</div></td>
<td><div align="center">46</div></td>
<td><div align="center">117</div></td>
<td><div align="center">50</div></td>
<td><div align="center">127</div></td>
</tr>
<tr>
<td><div align="left">Waist</div></td>
<td><div align="center">27</div></td>
<td><div align="center">69</div></td>
<td><div align="center">30</div></td>
<td><div align="center">76</div></td>
<td><div align="center">33</div></td>
<td><div align="center">84</div></td>
<td><div align="center">36</div></td>
<td><div align="center">91</div></td>
<td><div align="center">39</div></td>
<td><div align="center">99</div></td>
<td><div align="center">43</div></td>
<td><div align="center">109</div></td>
</tr>
<tr>
<td><div align="left">Hip</div></td>
<td><div align="center">36</div></td>
<td><div align="center">89</div></td>
<td><div align="center">38</div></td>
<td><div align="center">94</div></td>
<td><div align="center">40</div></td>
<td><div align="center">102</div></td>
<td><div align="center">43</div></td>
<td><div align="center">109</div></td>
<td><div align="center">46</div></td>
<td><div align="center">117</div></td>
<td><div align="center">50</div></td>
<td><div align="center">127</div></td>
</tr>
<tr>
<td><div align="left">Sleeve</div></td>
<td><div align="center">30 1/2</div></td>
<td><div align="center">77</div></td>
<td><div align="center">32 1/4</div></td>
<td><div align="center">82</div></td>
<td><div align="center">34</div></td>
<td><div align="center">86</div></td>
<td><div align="center">35</div></td>
<td><div align="center">89</div></td>
<td><div align="center">36</div></td>
<td><div align="center">91</div></td>
<td><div align="center">37</div></td>
<td><div align="center">94</div></td>
</tr>
</tbody></table>
</div>
</div>
</li>
</ul>
</li>
</ul>
</div></div>

Powered By <a href="http://www.dowgroup.com" target="_blank">Dow Group</a><br/> </div>

