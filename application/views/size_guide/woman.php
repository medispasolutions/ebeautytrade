
<div class="page-title">
<h1>Women's size guide</h1>
</div>
<div class="std"><div id="size-chart" class="popup-size-measure">
<ul>
<li class="open">The following charts will help you match the designer's size scheme for this product (indicated in the dropdown) to your usual size.
<ul class="content" style="display: block;">
<li>
<div id="tabBody1" class="tabContent tabBody size-conversion-body">
<div id="size_conversion">
<h1>Ready to wear size chart</h1>
<table cellspacing="0" cellpadding="0">
<tbody>
<tr class="alt"><th>&nbsp;</th>
<th>XXS</th>
<th>XXS - XS</th>
<th>XS - S</th>
<th>S - M</th>
<th>M - L</th>
<th>L - XL</th>
<th>XL - XXL</th>
<th>XXL - XXXL</th>
<th>XXXL</th>
</tr>
<tr class="alt"><th class="country">US</th>
<td>00</td>
<td>0</td>
<td>2 - 4</td>
<td>4 - 6</td>
<td>8</td>
<td>10</td>
<td>12</td>
<td>14</td>
<td>16</td>
</tr>
<tr><th class="country">UK</th>
<td>4</td>
<td>6</td>
<td>8</td>
<td>10</td>
<td>12</td>
<td>14</td>
<td>16</td>
<td>18</td>
<td>20</td>
</tr>
<tr class="alt"><th class="country">Italy</th>
<td>36</td>
<td>38</td>
<td>40</td>
<td>42</td>
<td>44</td>
<td>46</td>
<td>48</td>
<td>50</td>
<td>52</td>
</tr>
<tr><th class="country">France</th>
<td>32</td>
<td>34</td>
<td>36</td>
<td>38</td>
<td>40</td>
<td>42</td>
<td>44</td>
<td>46</td>
<td>48</td>
</tr>
<tr class="alt"><th class="country">Denmark</th>
<td>30</td>
<td>32</td>
<td>34</td>
<td>36</td>
<td>38</td>
<td>40</td>
<td>42</td>
<td>44</td>
<td>46</td>
</tr>
<tr><th class="country">Russia</th>
<td>38</td>
<td>40</td>
<td>42</td>
<td>44</td>
<td>46</td>
<td>48</td>
<td>50</td>
<td>52</td>
<td>54</td>
</tr>
<tr class="alt"><th class="country">Germany</th>
<td>30</td>
<td>32</td>
<td>34</td>
<td>36</td>
<td>38</td>
<td>40</td>
<td>42</td>
<td>44</td>
<td>46</td>
</tr>
<tr><th class="country">Australia</th>
<td>4</td>
<td>6</td>
<td>8</td>
<td>10</td>
<td>12</td>
<td>14</td>
<td>16</td>
<td>18</td>
<td>20</td>
</tr>
<tr class="alt"><th class="country">Japan</th>
<td>3</td>
<td>5</td>
<td>7</td>
<td>9</td>
<td>11</td>
<td>13</td>
<td>15</td>
<td>17</td>
<td>19</td>
</tr>
<tr><th class="country">Jeans</th>
<td>23</td>
<td>24 - 25</td>
<td>26 - 27</td>
<td>27 - 28</td>
<td>29 - 30</td>
<td>31 - 32</td>
<td>32 - 33</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<h1>Shoe size chart</h1>
<table cellspacing="0" cellpadding="0">
<tbody>
<tr class="alt"><th class="country">US</th>
<td>3.5</td>
<td>4</td>
<td>4.5</td>
<td>5</td>
<td>5.5</td>
<td>6</td>
<td>6.5</td>
<td>7</td>
<td>7.5</td>
<td>8</td>
<td>8.5</td>
<td>9</td>
<td>9.5</td>
<td>10</td>
<td>10.5</td>
<td>11</td>
<td>11.5</td>
</tr>
<tr><th class="country">UK</th>
<td>1</td>
<td>1.5</td>
<td>2</td>
<td>2.5</td>
<td>3</td>
<td>3.5</td>
<td>4</td>
<td>4.5</td>
<td>5</td>
<td>5.5</td>
<td>6</td>
<td>6.5</td>
<td>7</td>
<td>7.5</td>
<td>8</td>
<td>8.5</td>
<td>9</td>
</tr>
<tr class="alt"><th class="country">Italy/Europe</th>
<td>34</td>
<td>34.5</td>
<td>35</td>
<td>35.5</td>
<td>36</td>
<td>36.5</td>
<td>37</td>
<td>37.5</td>
<td>38</td>
<td>38.5</td>
<td>39</td>
<td>39.5</td>
<td>40</td>
<td>40.5</td>
<td>41</td>
<td>41.5</td>
<td>42</td>
</tr>
<tr><th class="country">France</th>
<td>35</td>
<td>35.5</td>
<td>36</td>
<td>36.5</td>
<td>37</td>
<td>37.5</td>
<td>38</td>
<td>38.5</td>
<td>39</td>
<td>39.5</td>
<td>40</td>
<td>40.5</td>
<td>41</td>
<td>41.5</td>
<td>42</td>
<td>42.5</td>
<td>43</td>
</tr>
</tbody>
</table>
<h1>Ring size chart</h1>
<table cellspacing="0" cellpadding="0">
<tbody>
<tr class="alt"><th class="country">US</th>
<td>4</td>
<td>4&frac14;</td>
<td>4&frac12;</td>
<td>4&frac34;</td>
<td>5</td>
<td>5&frac14;</td>
<td>5&frac12;</td>
<td>5&frac34;</td>
<td>6</td>
<td>6&frac14;</td>
<td>6&frac12;</td>
<td>6&frac34;</td>
<td>7</td>
<td>7&frac14;</td>
<td>7&frac12;</td>
<td>7&frac34;</td>
<td>8</td>
<td>8&frac14;</td>
<td>8&frac12;</td>
<td>8&frac34;</td>
<td>9</td>
</tr>
<tr><th class="country">UK</th>
<td>H&frac12;</td>
<td>I</td>
<td>I&frac12;</td>
<td>J</td>
<td>J&frac12;</td>
<td>K</td>
<td>K&frac12;</td>
<td>L</td>
<td>L&frac12;</td>
<td>M</td>
<td>M&frac12;</td>
<td>N</td>
<td>N&frac12;</td>
<td>O</td>
<td>O&frac12;</td>
<td>P</td>
<td>P&frac12;</td>
<td>Q</td>
<td>Q&frac12;</td>
<td>R</td>
<td>R&frac12;</td>
</tr>
<tr class="alt"><th class="country">Italy</th>
<td>8</td>
<td>8/9</td>
<td>9</td>
<td>10</td>
<td>10/11</td>
<td>11</td>
<td>12</td>
<td>12/13</td>
<td>13</td>
<td>14</td>
<td>14/15</td>
<td>15</td>
<td>15/16</td>
<td>16</td>
<td>17</td>
<td>17/18</td>
<td>18</td>
<td>18/19</td>
<td>19</td>
<td>19/20</td>
<td>20</td>
</tr>
<tr><th class="country" style="bgcolor: red;">France</th>
<td>46</td>
<td>47</td>
<td>48</td>
<td>49</td>
<td>49.5</td>
<td>50</td>
<td>50.5</td>
<td>51</td>
<td>52</td>
<td>53</td>
<td>53.5</td>
<td>54</td>
<td>54.5</td>
<td>55</td>
<td>55.5</td>
<td>56</td>
<td>57</td>
<td>58</td>
<td>58.5</td>
<td>59</td>
<td>60</td>
</tr>
</tbody>
</table>
<h1>Glove size chart</h1>
<table cellspacing="0" cellpadding="0">
<tbody>
<tr class="alt"><th class="country">Ladies</th>
<th>XS</th>
<th>S</th>
<th>M</th>
<th>L</th>
<th>XL</th>
</tr>
<tr><th class="country">Size "</th>
<td>6</td>
<td>6.5</td>
<td>7/7.5</td>
<td>8</td>
<td>8.5/9</td>
</tr>
</tbody>
</table>
<h1>Belt size chart</h1>
<table cellspacing="0" cellpadding="0">
<tbody>
<tr class="alt"><th>&nbsp;</th>
<th>XXS</th>
<th>XXS - XS</th>
<th>XS - S</th>
<th>S - M</th>
<th>M - L</th>
<th>L - XL</th>
<th>XL - XXL</th>
<th>XXL - XXXL</th>
<th>XXXL</th>
</tr>
<tr class="alt"><th>US</th>
<td>00</td>
<td>0</td>
<td>2 - 4</td>
<td>4 - 6</td>
<td>8</td>
<td>10</td>
<td>12</td>
<td>14</td>
<td>16</td>
</tr>
<tr><th class="country">UK</th>
<td>4</td>
<td>6</td>
<td>8</td>
<td>10</td>
<td>12</td>
<td>14</td>
<td>16</td>
<td>18</td>
<td>20</td>
</tr>
<tr class="alt"><th class="country">Italy</th>
<td>36</td>
<td>38</td>
<td>40</td>
<td>42</td>
<td>44</td>
<td>46</td>
<td>48</td>
<td>50</td>
<td>52</td>
</tr>
<tr><th class="country">Europe</th>
<td>65</td>
<td>70</td>
<td>75</td>
<td>80</td>
<td>85</td>
<td>90</td>
<td>95</td>
<td>100</td>
<td>105</td>
</tr>
</tbody>
</table>
<h1>Hat size chart</h1>
<table cellspacing="0" cellpadding="0">
<tbody>
<tr class="alt"><th>&nbsp;</th>
<th>XS</th>
<th>XS</th>
<th>S</th>
<th>S</th>
<th>M</th>
<th>M</th>
<th>L</th>
<th>L</th>
<th>XL</th>
<th>XL</th>
</tr>
<tr class="alt"><th class="country">US</th>
<td>6&frac12;</td>
<td>6⅝</td>
<td>6&frac34;</td>
<td>6⅞</td>
<td>7</td>
<td>7⅛</td>
<td>7&frac14;</td>
<td>7⅜</td>
<td>7&frac12;</td>
<td>7⅝</td>
</tr>
<tr><th class="country">UK</th>
<td>6⅜</td>
<td>6&frac12;</td>
<td>6⅝</td>
<td>6&frac34;</td>
<td>6⅞</td>
<td>7</td>
<td>7⅛</td>
<td>7&frac14;</td>
<td>7⅜</td>
<td>7&frac12;</td>
</tr>
<tr class="alt"><th class="country">Europe</th>
<td>52</td>
<td>53</td>
<td>54</td>
<td>55</td>
<td>56</td>
<td>57</td>
<td>58</td>
<td>59</td>
<td>60</td>
<td>61</td>
</tr>
</tbody>
</table>
</div>
</div>
</li>
</ul>
</li>
</ul>
</div></div>

Powered By <a href="http://www.dowgroup.com" target="_blank">Dow Group</a><br/> </div>
</body>
</html>
