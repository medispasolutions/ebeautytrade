<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/consultants/'.$this->session->userdata("back_link"),'<b>List consultants</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/consultants/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/consultants/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["image"] = "";
$info["brief"] = "";
$info["phone"] = "";
$info["link"] = "";
$info["name"] = "";
$info["logo"] = "";
$info["company_name"] = "";
$info["position"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>


<div class="row-fluid">
<div class="span6">
<?php
//NAME SECTION.
echo form_label('<b>NAME</b>', 'NAME');
echo form_input(array('name' => 'name', 'value' => set_value("name",$info["name"]),'class' =>'span' ));
echo form_error("name","<span class='text-error'>","</span>");
echo br();?>
</div>
<div class="span6">

<label><b>Phone</b><em>*</em></label>
<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="phone[0]" value="<?php echo getPhoneField($info['phone'],0);?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="phone[1]" value="<?php echo getPhoneField($info['phone'],1);?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="phone[2]" value="<?php echo getPhoneField($info['phone'],2);?>" />
<?= form_error('phone','<span class="text-error">','</span>'); ?>

</div></div>


<div class="row-fluid">
<div class="span6">
<?php
//IMAGE SECTION.
echo form_label('<b>IMAGE</b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_consultants"].'">'.anchor('uploads/consultants/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("consultants","image","<?php echo $info["image"]; ?>",<?php echo $info["id_consultants"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
?>
</div><div class="span6">
<?php 
//LOGO SECTION.
echo form_label('<b>LOGO</b>', 'LOGO');
echo form_upload(array("name" => "logo", "class" => "input-large"));
echo "<span >";
if($info["logo"] != ""){ 
echo '<span id="logo_'.$info["id_consultants"].'">'.anchor('uploads/consultants/'.$info["logo"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("consultants","logo","<?php echo $info["logo"]; ?>",<?php echo $info["id_consultants"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("logo","<span class='text-error'>","</span>");
echo br();
?>
</div></div>

<div class="row-fluid">
<div class="span6">
<?php 

//COMPANY NAME SECTION.
echo form_label('<b>COMPANY NAME</b>', 'COMPANY NAME');
echo form_input(array('name' => 'company_name', 'value' => set_value("company_name",$info["company_name"]),'class' =>'span' ));
echo form_error("company_name","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<?php
//POSITION SECTION.
echo form_label('<b>POSITION</b>', 'POSITION');
echo form_input(array('name' => 'position', 'value' => set_value("position",$info["position"]),'class' =>'span' ));
echo form_error("position","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>

<div class="row-fluid">

<?php 

//COMPANY NAME SECTION.
echo form_label('<b>LINK</b>', 'LINK');
echo form_input(array('name' => 'link', 'value' => set_value("link",$info["link"]),'class' =>'span' ));
echo form_error("link","<span class='text-error'>","</span>");
echo br();
?>

</div>
<?php


//BRIEF SECTION.
echo form_label('<b>BRIEF</b>', 'BRIEF');
echo form_textarea(array("name" => "brief", "value" => set_value("brief",$info["brief"]),"class" =>"ckeditor","id" => "brief", "rows" => 15, "cols" =>100 ));
echo form_error("brief","<span class='text-error'>","</span>");
echo br();

echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>