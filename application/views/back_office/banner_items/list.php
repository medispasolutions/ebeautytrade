<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
function delete_row(id){
	<?php if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {?>
		window.location="<?= site_url('back_office/banner_items/delete/'); ?>"+id+"?id_banner=<?php echo $_GET['id_banner']; ?>";
	<?php } else {?>
		window.location="<?= site_url('back_office/banner_items/delete'); ?>/"+id;
	<?php }?>

return false;
}

$(function(){
$("#table-1").tableDnD({
onDrop: function(table, row) {
var ser=$.tableDnD.serialize();
$("#result").load("<?= site_url('back_office/banner_items/sorted'); ?>"+'?'+ser);
}
});



$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<?php if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {
	
	$banner = $this->fct->getonerow('banner',array('id_banner'=>$_GET['id_banner']));
	?>

<li>
<a href="<?php echo site_url('back_office/banner'); ?>" data-original-title="">
<b>List of banners</b>
</a>
<span class="divider">/</span>
</li>
<li>

<li>
<a href="<?php echo site_url('back_office/banner/edit/'.$banner['id_banner']); ?>" data-original-title="">
<b><?php echo $banner['title']; ?></b>
</a>
<span class="divider">/</span>
</li>
<li class="active">List of items</li>


<?php } else {?>

<li class="active"><?= $title; ?></li>
<?php }?>
<? if ($this->acl->has_permission('banner_items','add')){ ?>
<li class="pull-right">
<?php if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {?>
<a href="<?= site_url('back_office/banner_items/add').'?id_banner='.$_GET['id_banner']; ?>" id="topLink" class="btn btn-info top_btn" title="">Add banner items</a>
<?php } else {?>
<a href="<?= site_url('back_office/banner_items/add'); ?>" id="topLink" class="btn btn-info top_btn" title="">Add banner items</a>
<?php }?>

</li><? } ?>
</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {
	echo form_open_multipart('back_office/banner_items/delete_all?id_banner='.$_GET['id_banner'], $attributes); 
}
else {
	echo form_open_multipart('back_office/banner_items/delete_all', $attributes); 
}
?>  	

<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="3" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<td width="2%">&nbsp;</td>
<th>
TITLE
</th>
<th style="text-align:center;" width="250">ACTION</th></tr>

</thead><tfoot><tr>
<td class="col-chk"><input type="checkbox" id="checkAllAuto" /></td>
<td colspan="2">
<? if ($this->acl->has_permission('banner_items','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>
<tbody>
<? 
if(count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>
<tr id="<?=$val["id_banner_items"]; ?>">
<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_banner_items"] ; ?>" /></td>
<td class="title_search">
<? echo $val["title"]; ?>
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('banner_items','edit')){ ?>
<?php if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {?>
<a href="<?= site_url('back_office/banner_items/edit/'.$val["id_banner_items"]);?>?id_banner=<?php echo $_GET['id_banner']; ?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>
<?php } else {?>
<a href="<?= site_url('back_office/banner_items/edit/'.$val["id_banner_items"]);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>
<?php }?>

<? } ?>
<? if ($this->acl->has_permission('banner_items','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_banner_items"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="3" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">

<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 