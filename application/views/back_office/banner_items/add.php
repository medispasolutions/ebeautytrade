<link rel="stylesheet" href="<?php echo base_url(); ?>js/colorpicker/css/colorpicker.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/eye.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/utils.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/layout.js?ver=1.0.2"></script>
<script type="text/javascript">
$(document).ready(function(){
	
$('#colorpickerField1').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val(hex);
		$(el).ColorPickerHide();
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});
$('#TypeChange').change(function(){
	var type = $(this).val();
	if(type == 'image') {
		$('#TextBox').slideUp('fast');
		$('#ImageBox').slideDown('fast');
	}
	else if(type == 'text'){
		$('#ImageBox').slideUp('fast');
		$('#TextBox').slideDown('fast');
	}
	else {
		$('#ImageBox').slideUp('fast');
		$('#TextBox').slideUp('fast');
	}
});
	
});
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?php if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {
	
	$banner = $this->fct->getonerow('banner',array('id_banner'=>$_GET['id_banner']));
	?>
<ul class="breadcrumb">
<li>
<a href="<?php echo site_url('back_office/banner'); ?>" data-original-title="">
<b>List of banners</b>
</a>
<span class="divider">/</span>
</li>
<li>

<li>
<a href="<?php echo site_url('back_office/banner/edit/'.$banner['id_banner']); ?>" data-original-title="">
<b><?php echo $banner['title']; ?></b>
</a>
<span class="divider">/</span>
</li>
<li>
<a href="<?php echo site_url('back_office/banner_items').'?id_banner='.$banner['id_banner']; ?>" data-original-title="">
<b>List of items</b>
</a>
<span class="divider">/</span>
</li>
<?php if(isset($id)) {?>
<li class="active">Edit banner items</li>
<?php } else {?>
<li class="active">Add banner items</li>
<?php }?>

</ul>
<?php } else {?>
<?
$ul = array(
            anchor('back_office/banner_items/'.$this->session->userdata("back_link"),'<b>List banner items</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$attributes = array('class' => 'middle-forms');
if(isset($_GET['id_banner']) && !empty($_GET['id_banner']))
echo form_open_multipart('back_office/banner_items/submit?id_banner='.$_GET['id_banner'], $attributes); 
else
echo form_open_multipart('back_office/banner_items/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["banner"] = "";$info["type"] = "";$info["link"] = "";$info["image"] = "";$info["text"] = "";$info["color"] = "";$info["font_size"] = "";$info["font_weight"] = "";$info["top"] = "";$info["left"] = "";$info["zindex"] = "";$info["slide_in_direction"] = "";$info["slide_out_direction"] = "";$info["easingin"] = "";$info["easingout"] = "";$info["duration_in"] = "";$info["duration_out"] = "";$info["delay_in"] = "";
$info["title"] = "";
$info["meta_title"] = "";
$info["meta_description"] = "";
$info["meta_keywords"] = "";
$info["title_url"] = "";
$info["rotate"] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title", "value" => set_value("title",$info["title"]),"class" =>"span" ));
echo form_error("title","<span class='text-error'>","</span>");
echo br();
if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {
	echo form_hidden('banner',$_GET['id_banner']);
}
else {
	//BANNER&nbsp;<em>*</em>: SECTION.
echo form_label('<b>BANNER&nbsp;<em>*</em>:</b>', 'BANNER&nbsp;<em>*</em>:');
$items = $this->fct->getAll("banner","sort_order"); 
echo '<select name="banner"  class="span">';
echo '<option value="" > - select banner - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_banner"]; ?>" <? if(isset($id)){  if($info["id_banner"] == $valll["id_banner"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("banner","<span class='text-error'>","</span>");
echo br();
}

//LINK SECTION.
echo form_label('<b>LINK</b> (ex: http://www.dowgroup.com)', 'LINK');
echo form_input(array('name' => 'link', 'value' => set_value("link",$info["link"]),'class' =>'span' ));
echo form_error("link","<span class='text-error'>","</span>");
echo br();
//TYPE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>TYPE&nbsp;<em>*</em>:</b>', 'TYPE&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
                  'image' => "Image",
                  'text' => "Text",
				  'readmore' => "View Details Button",
                );
echo form_dropdown("type", $options,set_value("type",$info["type"]), 'class="span" id="TypeChange"');
echo form_error("type","<span class='text-error'>","</span>");
echo br();
?>
<div id="ImageBox" style=" <?php if(set_value('type',$info['type']) == 'image') {?>display:block<?php } else {?>display:none<?php }?>">
<?php
//IMAGE SECTION.
echo form_label('<b>IMAGE</b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo anchor('uploads/banner_items/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);
echo anchor("back_office/banner_items/delete_image/image/".$info["image"]."/".$info["id_banner_items"], img('images/delete.png'),array('class' => 'cur','onClick' => "return confirm('Are you sure you want to delete this image ?')" ));
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div id="TextBox" style=" <?php if(set_value('type',$info['type']) == 'text') {?>display:block<?php } else {?>display:none<?php }?>">
<?php
//TEXT SECTION.
echo form_label('<b>TEXT</b>', 'TEXT');
echo form_textarea(array('name' => 'text', 'value' => set_value("text",$info["text"]),'class' =>'span ckeditor','id' =>'desc' ));
echo form_error("text","<span class='text-error'>","</span>");
echo br();
//COLOR SECTION.
/*echo form_label('<b>COLOR</b>', 'COLOR');
echo form_input(array('name' => 'color', 'value' => set_value("color",$info["color"]),'class' =>'span','id'=>'colorpickerField1' ));
echo form_error("color","<span class='text-error'>","</span>");
echo br();
//FONT SIZE SECTION.
echo form_label('<b>FONT SIZE</b> (integer)', 'FONT SIZE');
echo form_input(array('name' => 'font_size', 'value' => set_value("font_size",$info["font_size"]),'class' =>'span' ));
echo form_error("font_size","<span class='text-error'>","</span>");
echo br();
//FONT WEIGHT SECTION.
echo form_label('<b>FONT WEIGHT</b>', 'FONT WEIGHT');
$options = array(
                  "" => " - select option - ",
                  'bold' => "Bold",
                  'normal' => "Normal",
                );
echo form_dropdown("font_weight", $options,set_value("font_weight",$info["font_weight"]), 'class="span"');
echo form_error("font_weight","<span class='text-error'>","</span>");
echo br();*/
?>
</div>
<?php
//TOP&nbsp;<em>*</em>: SECTION.
echo form_label('<b>TOP&nbsp;<em>*</em>:</b> (integer)', 'TOP&nbsp;<em>*</em>:');
echo form_input(array('name' => 'top', 'value' => set_value("top",$info["top"]),'class' =>'span' ));
echo form_error("top","<span class='text-error'>","</span>");
echo br();
//LEFT&nbsp;<em>*</em>: SECTION.
echo form_label('<b>LEFT&nbsp;<em>*</em>:</b> (integer)', 'LEFT&nbsp;<em>*</em>:');
echo form_input(array('name' => 'left', 'value' => set_value("left",$info["left"]),'class' =>'span' ));
echo form_error("left","<span class='text-error'>","</span>");
echo br();
//ZINDEX&nbsp;<em>*</em>: SECTION.
echo form_label('<b>ZINDEX&nbsp;<em>*</em>:</b> (integer)', 'ZINDEX&nbsp;<em>*</em>:');
echo form_input(array('name' => 'zindex', 'value' => set_value("zindex",$info["zindex"]),'class' =>'span' ));
echo form_error("zindex","<span class='text-error'>","</span>");
echo br();
//SLIDE IN DIRECTION&nbsp;<em>*</em>: SECTION.
/*echo form_label('<b>SLIDE IN DIRECTION&nbsp;<em>*</em>:</b>', 'SLIDE IN DIRECTION&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
				  'fade' => "Fade",
                  'top' => "Top",
				  'bottom' => "Bottom",
				  'left' => "Left",
				  'right' => "Right",
                );
echo form_dropdown("slide_in_direction", $options,set_value("slide_in_direction",$info["slide_in_direction"]), 'class="span"');
echo form_error("slide_in_direction","<span class='text-error'>","</span>");
echo br();
//SLIDE OUT DIRECTION&nbsp;<em>*</em>: SECTION.
echo form_label('<b>SLIDE OUT DIRECTION&nbsp;<em>*</em>:</b>', 'SLIDE OUT DIRECTION&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
				  'fade' => "Fade",
                  'top' => "Top",
				  'bottom' => "Bottom",
				  'left' => "Left",
				  'right' => "Right",
                );
echo form_dropdown("slide_out_direction", $options,set_value("slide_out_direction",$info["slide_out_direction"]), 'class="span"');
echo form_error("slide_out_direction","<span class='text-error'>","</span>");
echo br();
//TYPE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>Enable Rotate</b>', 'Enable Rotate&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("rotate", $options,set_value("rotate",$info["rotate"]), 'class="span"');
echo form_error("rotate","<span class='text-error'>","</span>");
echo br();
//EASINGIN&nbsp;<em>*</em>: SECTION.
echo form_label('<b>EASINGIN&nbsp;<em>*</em>:</b>', 'EASINGIN&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
                  'linear' => "linear",
                  'swing' => "swing",
				  'easeInQuad' => "easeInQuad",
				  'easeOutQuad' => "easeOutQuad",
				  'easeInOutQuad' => "easeInOutQuad",
				  'easeInCubic' => "easeInCubic",
				  'easeOutCubic' => "easeOutCubic",
				  'easeInOutCubic' => "easeInOutCubic",
				  'easeInQuart' => "easeInQuart",
				  'easeOutQuart' => "easeOutQuart",
				  'easeInOutQuart' => "easeInOutQuart",
				  'easeInQuint' => "easeInQuint",
				  'easeOutQuint' => "easeOutQuint",
				  'easeInOutQuint' => "easeInOutQuint",
				  'easeInExpo' => "easeInExpo",
				  'easeOutExpo' => "easeOutExpo",
				  'easeInOutExpo' => "easeInOutExpo",
				  'easeInSine' => "easeInSine",
				  'easeOutSine' => "easeOutSine",
				  'easeInOutSine' => "easeInOutSine",
				  'easeInCirc' => "easeInCirc",
				  'easeOutCirc' => "easeOutCirc",
				  'easeInOutCirc' => "easeInOutCirc",
				  'easeInElastic' => "easeInElastic",
				  'easeOutElastic' => "easeOutElastic",
				  'easeInOutElastic' => "easeInOutElastic",
				  'easeInBack' => "easeInBack",
				  'easeOutBack' => "easeOutBack",
				  'easeInOutBack' => "easeInOutBack",
				  'easeInBounce' => "easeInBounce",
				  'easeOutBounce' => "easeOutBounce",
				  'easeInOutBounce' => "easeInOutBounce",
                );
echo form_dropdown("easingin", $options,set_value("easingin",$info["easingin"]), 'class="span"');
echo form_error("easingin","<span class='text-error'>","</span>");
echo br();
//EASINGOUT&nbsp;<em>*</em>: SECTION.
echo form_label('<b>EASINGOUT&nbsp;<em>*</em>:</b>', 'EASINGOUT&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
                  'linear' => "linear",
                  'swing' => "swing",
				  'easeInQuad' => "easeInQuad",
				  'easeOutQuad' => "easeOutQuad",
				  'easeInOutQuad' => "easeInOutQuad",
				  'easeInCubic' => "easeInCubic",
				  'easeOutCubic' => "easeOutCubic",
				  'easeInOutCubic' => "easeInOutCubic",
				  'easeInQuart' => "easeInQuart",
				  'easeOutQuart' => "easeOutQuart",
				  'easeInOutQuart' => "easeInOutQuart",
				  'easeInQuint' => "easeInQuint",
				  'easeOutQuint' => "easeOutQuint",
				  'easeInOutQuint' => "easeInOutQuint",
				  'easeInExpo' => "easeInExpo",
				  'easeOutExpo' => "easeOutExpo",
				  'easeInOutExpo' => "easeInOutExpo",
				  'easeInSine' => "easeInSine",
				  'easeOutSine' => "easeOutSine",
				  'easeInOutSine' => "easeInOutSine",
				  'easeInCirc' => "easeInCirc",
				  'easeOutCirc' => "easeOutCirc",
				  'easeInOutCirc' => "easeInOutCirc",
				  'easeInElastic' => "easeInElastic",
				  'easeOutElastic' => "easeOutElastic",
				  'easeInOutElastic' => "easeInOutElastic",
				  'easeInBack' => "easeInBack",
				  'easeOutBack' => "easeOutBack",
				  'easeInOutBack' => "easeInOutBack",
				  'easeInBounce' => "easeInBounce",
				  'easeOutBounce' => "easeOutBounce",
				  'easeInOutBounce' => "easeInOutBounce",
                );
echo form_dropdown("easingout", $options,set_value("easingout",$info["easingout"]), 'class="span"');
echo form_error("easingout","<span class='text-error'>","</span>");
echo br();
//DURATION IN&nbsp;<em>*</em>: SECTION.
echo form_label('<b>DURATION IN&nbsp;<em>*</em>:</b> (integer)', 'DURATION IN&nbsp;<em>*</em>:');
echo form_input(array('name' => 'duration_in', 'value' => set_value("duration_in",$info["duration_in"]),'class' =>'span' ));
echo form_error("duration_in","<span class='text-error'>","</span>");
echo br();
//DURATION OUT&nbsp;<em>*</em>: SECTION.
echo form_label('<b>DURATION OUT&nbsp;<em>*</em>:</b> (integer)', 'DURATION OUT&nbsp;<em>*</em>:');
echo form_input(array('name' => 'duration_out', 'value' => set_value("duration_out",$info["duration_out"]),'class' =>'span' ));
echo form_error("duration_out","<span class='text-error'>","</span>");
echo br();
//DELAY IN&nbsp;<em>*</em>: SECTION.
echo form_label('<b>DELAY IN&nbsp;<em>*</em>:</b> (integer)', 'DELAY IN&nbsp;<em>*</em>:');
echo form_input(array('name' => 'delay_in', 'value' => set_value("delay_in",$info["delay_in"]),'class' =>'span' ));
echo form_error("delay_in","<span class='text-error'>","</span>");
echo br();
echo br();
*/if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>