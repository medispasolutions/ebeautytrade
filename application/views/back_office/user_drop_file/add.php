<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >

<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(anchor('back_office/users','<b>List Users</b>').'<span class="divider">/</span>',
            anchor('back_office/users/edit/'.$info['id_user'],'<b>Edit User('.$info['name'].')</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/supplier_info/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>

</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/user_drop_file/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["file_1"] = "";
$info["file_2"] = "";

}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}
?>

<input type="hidden" name="id_user"  value="<?php echo set_value("id_user",$info["id_user"]);?>" />
<input type="hidden" name="id"  value="<?php echo set_value("id",$user_drop_file["id_user_drop_file"]);?>" />
<?php
//FILE 1 SECTION.
echo "<label>";
echo form_label('<b>FILE 1</b>', 'FILE 1');

echo form_upload(array("name" => "file_1", "class" => "input-large"));
echo "<span >";
if($user_drop_file["file_1"] != ""){ 
echo '<span id="file_1_'.$user_drop_file["id_user_drop_file"].'">';
echo '<a download href="site_url(uploads/user_drop_file/'.$user_drop_file["file_1"].')" >Download File</a>';
echo nbs(3);?>
<a class="cur" onclick='removeFile("user_drop_file","file_1","<?php echo $user_drop_file["file_1"]; ?>",<?php echo $user_drop_file["id_user_drop_file"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo "</label>";
echo "<label>";
echo form_error("file_1","<span class='text-error'>","</span>");
echo br();
//FILE 2 SECTION.
echo form_label('<b>FILE 2</b>', 'FILE 2');
echo form_upload(array("name" => "file_2", "class" => "input-large"));
echo "<span >";
if($user_drop_file["file_2"] != ""){ 
echo '<a download href="site_url(uploads/user_drop_file/'.$user_drop_file["file_2"].')" >Download File</a>';
echo nbs(3);?>
<a class="cur" onclick='removeFile("user_drop_file","file_2","<?php echo $user_drop_file["file_2"]; ?>",<?php echo $user_drop_file["id_user_drop_file"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo "</label>";
echo form_error("file_2","<span class='text-error'>","</span>");
echo br();

if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>