<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/categories/'.$this->session->userdata("back_link"),'<b>List categories</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
			if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/categories/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/categories/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["id_parent"] = "";
$info["image"] = "";
$info["pdf"] = "";
$info["title".$lang] = "";
$info["description".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//CATEGORY&nbsp;<em>*</em>: SECTION.
echo form_label('<b>CATEGORY&nbsp;<em>*</em>:</b>', 'CATEGORY&nbsp;<em>*</em>:');
$items = $this->fct->getAll_cond("categories","sort_order",array('id_parent'=>0)); 
echo '<select name="category"  class="span">';
echo '<option value="" > - main category - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_categories"]; ?>" <? if(isset($id)){  if($info["id_parent"] == $valll["id_categories"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("category","<span class='text-error'>","</span>");
echo br();
echo br();




//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
echo br();



//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();

//IMAGE SECTION.
echo form_label('<b>IMAGE</b> (not translated)', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_categories"].'">'.anchor('uploads/categories/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);
echo '<a class=\'cur\' onclick=\'removeFile("categories","image","'.$info["image"].'",'.$info["id_categories"].')\'><img src="'.base_url().'images/delete.png" /></a>'.'</span>';
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
echo br();

echo form_label('<b>PDF</b> (not translated)', 'PDF');
echo form_upload(array("name" => "pdf", "class" => "input-large"));
echo "<span >";
if($info["pdf"] != ""){ 
echo '<span id="pdf_'.$info["id_categories"].'">'.anchor('uploads/categories/'.$info["pdf"],'show file',array("class" => 'blue','target'=>'_blank'));
echo nbs(3);
echo '<a class=\'cur\' onclick=\'removeFile("categories","pdf","'.$info["pdf"].'",'.$info["id_categories"].')\'><img src="'.base_url().'images/delete.png" /></a>'.'</span>';
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo form_error("pdf","<span class='text-error'>","</span>");
echo br();
echo br();

//DESCRIPTION SECTION.
echo form_label('<b>DESCRIPTION</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"ckeditor","id" => "description".$lang, "rows" => 15, "cols" =>100 ));
echo form_error("description".$lang,"<span class='text-error'>","</span>");
echo br();

if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>