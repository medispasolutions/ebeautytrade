<link rel="stylesheet" href="<?php echo base_url(); ?>js/colorpicker/css/colorpicker.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/eye.js"></script>
<?php if(isset($attribute['id_attributes']) && $attribute['id_attributes']==2) { ?>
<script type='text/javascript'>
$(document).ready(function(){
 //$('.colorpicker').each(function(){
  //alert("aaaaa");
  //var id = this.id;
  //$("#"+id).spectrum({ preferredFormat: "hex",showInput:true });
  $('.fillColor').ColorPicker({
   onSubmit: function(hsb, hex, rgb, el) {
    $(el).val(hex);
    $(el).ColorPickerHide();
   },
   onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
   }
  })
  .bind('keyup', function(){
   $(this).ColorPickerSetColor(this.value);
  });
 //});
 //$(".colorpicker").spectrum({ preferredFormat: "hex",showInput:true });
});
</script>
<?php } ?>

<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/utils.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/layout.js?ver=1.0.2"></script>
<div class="container-fluid">

<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?php if(isset($attribute) && !empty($attribute)) {?>
<ul class="breadcrumb">
<li>
<a href="<?= site_url('back_office/attributes'); ?>" title="">List Attributes</a>
</li><span class="divider">/</span>
<li>
<a href="<?= site_url('back_office/attributes/edit/'.$attribute['id_attributes']); ?>" title="">Edit <?php echo $attribute['title']; ?></a>
</li><span class="divider">/</span>
<li>
<a href="<?= site_url('back_office/attribute_options?id_attribute='.$attribute['id_attributes']); ?>" title="">Options</a>
</li><span class="divider">/</span>
<?php if(isset($id)) {?>
<?php if($this->router->method == 'view') {?>
<li class="active">View Option</li>
<?php } else {?>
<li class="active">Edit Option</li>
<?php }?>

<?php }else {?>
<li class="active">Add Option </li>
<?php }?>
</ul>
<?php } else {?>
<?
$ul = array(
            anchor('back_office/attribute_options/'.$this->session->userdata("back_link"),'<b>List attribute options</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
			/*if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/videos/'.$this->translation_lib->getDefaultRowLangID("videos",$id,TRUE)).'" class="btn btn-info top_btn">Translate</a>');
			}*/

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
<?php }?>
</div>
<div class="hundred pull-left">   
<?
$attributes = array('class' => 'middle-forms');
if(isset($_GET['id_attribute']))
echo form_open_multipart('back_office/attribute_options/submit?id_attribute='.$_GET['id_attribute'], $attributes); 
else
echo form_open_multipart('back_office/attribute_options/submit', $attributes); 

echo form_open_multipart('back_office/attribute_options/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["attribute"] = "";
$info["title"] = "";
$info["option"] = "";
/*$info["meta_title"] = "";
$info["meta_description"] = "";
$info["meta_keywords"] = "";
$info["title_url"] = "";*/
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
/*if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}*/
//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title", "value" => set_value("title",$info["title"]),"class" =>"span" ));
echo form_error("title","<span class='text-error'>","</span>");
echo br();
 if(isset($attribute['id_attributes']) && $attribute['id_attributes']==2) {
//TITLE SECTION.
echo form_label('<b>Option&nbsp;<em>*</em>:</b>', 'Option');
echo form_input(array("name" => "option", "value" => set_value("option",$info["option"]),"class" =>"span fillColor","id"=>$attribute['id_attributes'] ));
echo form_error("option","<span class='text-error'>","</span>");
echo br();
 } 

/*echo form_label('<b>Title&nbsp;<em>*</em>:</b> (English)', 'Title');
echo form_input(array("name" => "title_en", "value" => set_value("title_en",$info["title_en"]),"class" =>"span" ));
echo form_error("title_en","<span class='text-error'>","</span>");
echo br();*/
//PAGE TITLE SECTION.
/*echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title", "value" => set_value("meta_title",$info["meta_title"]),"class" =>"span" ));
echo form_error("meta_title","<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url", "value" => set_value("title_url",$info["title_url"]),"class" =>"span" ));
echo form_error("title_url","<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description", "value" => set_value("meta_description",$info["meta_description"]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description","<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords", "value" => set_value("meta_keywords",$info["meta_keywords"]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords","<span class='text-error'>","</span>");
echo br();*/

//ATTRIBUTE&nbsp;<em>*</em>: SECTION.
if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute'])) {
	echo form_hidden('attribute',$_GET['id_attribute']);
}
else {
	echo form_label('<b>ATTRIBUTE&nbsp;<em>*</em>:</b>', 'ATTRIBUTE&nbsp;<em>*</em>:');
$items = $this->fct->getAll("attributes","sort_order"); 
echo '<select name="attribute"  class="span">';
echo '<option value="" > - select attributes - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_attributes"]; ?>" <? if(isset($id)){  if($info["id_attributes"] == $valll["id_attributes"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("attribute","<span class='text-error'>","</span>");
echo br();
echo br();
}

if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>