<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
<script src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate3.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>
</head>

<body>
<div class="container-fluid">
<div class="row-fluid">


<div class="span10-fluid" >
<!--<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li>
<span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/purchases?id_product='.$product['id_products']); ?>" >List Of Purchases</a></li>
<span class="divider">/</span>
<li class="active">List of purchases</li>

</ul>-->
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms','id'=>'priceSegements');
echo form_open_multipart('back_office/product_price_segments/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["product"] = "";
$info["min_qty"] = "";
$info["price"] = "";
$info["discount"] = "";
$info["discount_expiration"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
/*echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();*/
/*//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();*/

//PRODUCT&nbsp;<em>*</em>: SECTION.
if($id_products != '') {
	echo form_hidden("id_product",$id_products);
	echo form_hidden("product",$id_products);
}
else {
echo form_label('<b>PRODUCT&nbsp;<em>*</em>:</b>', 'PRODUCT&nbsp;<em>*</em>:');
$items = $this->fct->getAll("products","sort_order"); 
echo '<select name="product'.'"  class="span">';
echo '<option value="" > - select products - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_products"]; ?>" <? if(isset($id)){  if($info["id_products"] == $valll["id_products"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("product","<span class='text-error'>","</span>");
echo br();
}
//MIN QTY&nbsp;<em>*</em>: SECTION.
echo form_label('<b>QTY&nbsp;<em>*</em>:</b>', 'MIN QTY&nbsp;<em>*</em>:');

	echo form_label("please specify the minimum quantity of this offer, should be in integer","help_text",array("class"=>"yellow"));
	echo form_input(array('name' => 'min_qty', 'value' => set_value("min_qty",$info["min_qty"]),'class' =>'span' ));
echo form_error("min_qty","<span class='text-error'>","</span>");
echo br();
//DISCOUNT&nbsp;<em>*</em>: SECTION.
/*echo form_label('<b>DISCOUNT&nbsp;<em>*</em>:</b>', 'DISCOUNT&nbsp;<em>*</em>:');

	echo form_label("should be integer (%)","help_text",array("class"=>"yellow"));
	echo form_input(array('name' => 'discount', 'value' => set_value("discount",$info["discount"]),'class' =>'span' ));
echo form_error("discount","<span class='text-error'>","</span>");
echo br();
//DISCOUNT EXPIRATION SECTION.
echo form_label('<b>DISCOUNT EXPIRATION</b>', 'DISCOUNT EXPIRATION');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'discount_expiration', 'value' => set_value("discount_expiration",$this->fct->date_out_formate($info["discount_expiration"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("discount_expiration","<span class='text-error'>","</span>");
echo br();
echo br();
*/
//PRICE SECTION.

echo form_label('<b>PRICE</b>', 'PRICE');
echo form_label("Should be Decimal format 10,2","help_text",array("class"=>"yellow"));
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span' ));
echo form_error("price","<span class='text-error'>","</span>");
echo br();
echo br();
?>
         <div id="loader-bx"></div>
        <div class="FormResult"></div>
<?php
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>     
</div>
</div>

<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?></body>