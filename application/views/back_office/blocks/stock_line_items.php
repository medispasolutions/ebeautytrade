
<?php $bool=false;if(!empty($order_details['user'])){
	$bool=true;}?>
    <?php if($bool){?>
    <br />
<label><b>Stock Return ID:</b><?php echo $order_details['id_stock_return']; ?></label>
<label><b><?php echo lang('trading_name');?>:</b> <?php echo $order_details['user']['trading_name']; ?></label>

<label><b>Created Date:</b> <?php echo  date("F d,Y h:i A", strtotime($order_details['created_date'])); ?></label>

 <?php if($order_details['status']==1){ ?>
<label><b>Status:</b> <span class="label label-success">Approved</span></label>
 <?php } ?>
 
 <?php if($order_details['type']==1){ ?>
<label><b>Type :</b> <?php echo lang('paid_m'); ?></label>
<?php } ?> 
<?php if($order_details['type']==2){ ?>
<label><b>Type :</b> <?php echo lang('consignment_m'); ?></label>
 <?php } ?> 
<?php echo form_error("qty",'<div class="alert alert-error">','</div>');?>
<?php } ?>

<?php 
$line_items=$order_details['line_items'];
?>
<?php if(!empty($line_items)){?>
<table class="table table-striped order-details-table"  style="margin-top:20px;">
          <tr>
          <th width="28%">Delivery Note / Purchase Invoice #</th>
            <th width="20%">Product Name</th>
            <th width="15%">Type</th>
            <th width="10%">Sku</th>
            <th width="12%" style="text-align:center;">Order Quantity</th>
             <!--<th width="15%" style="text-align:center;">Remaining Quantity</th>-->
            <th width="20%" style="text-align:center;">Return Quantity</th>
 <th></th>
     
          </tr>
 
          
     <?php foreach($line_items as $pro){
$remain_quantity=$pro['quantity']-$pro['return_quantity']-$pro['sold_quantity'];
		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];	
		$options=unSerializeStock($pro['stock']['combination']); 
			 }else{
		$sku=$pro['product']['sku'];
		$options="";}
		  ?>
		

<tr class="order-table-row">

	
      <td class="order-id">
      <?php if($pro['type_order']==1){
	$id_invoice=$pro["id_invoice"];
	$section="invoices";
	$link=route_to('back_office/inventory/view_delivery_request/invoices/'.$pro['id_orders'].'/'.$pro['order_rand']);
	}else{
	$id_invoice=$pro["id_orders"];
	$section="delivery_note";
	$link=route_to('back_office/inventory/view_delivery_request/delivery_note/'.$pro['id_orders'].'/'.$pro['order_rand']);
	}?>
<b><a href="<?php echo $link;?>" target="_blank"><?php echo '#'.$id_invoice;?></a></b></td>
        <td>
		 <input type="hidden" name="ids[]" value="<?php echo $pro['id_stock_return_line_items'];?>" />
		<?php echo $pro['product']['title'];?>
        <br />    <?php
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
        </td>
        <td>
        <? 
if($pro["invoice"] == '1') {?>
<span class="label label-success"><?php echo lang('paid_m'); ?></span>
<?php } else {?>
<span class="label label-important"><?php echo lang('consignment_m'); ?></span>
<?php }?></td>
        <td><?php echo $sku;?></td>
          <td style="text-align:center;">
          
<input type="hidden" id="remain_quantity_<?php echo $pro['id_stock_return_line_items'];?>" value="<?php echo $remain_quantity;?>" />
         <?php echo $remain_quantity;?>
    	</td>
          <?php /*?><td style="text-align:center;">
      <?php echo $remain_quantity;?>
    	</td><?php */?>
        <td style="text-align:center;">
   
         <?php if($order_details['status']!=1){?>
    <input class="span qty checklist_qty return_qty" maxlength="12" title="Qty" size="4"  id="qty_<?php echo $pro['id_stock_return_line_items'];?>" name="qty[]" onkeyup="check_qty_request(<?php echo $pro['id_stock_return_line_items'];?>)" style="width:50px;text-align:center;"  value="<?php echo $pro['return_quantity_stock']; ?>"  >
     <?php }else{ 
	 echo  $pro['return_quantity_stock'];
		  }?>      
         </td>
         
         <td>
          <?php if($order_details['status']!=1 ){ ?>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2(<?php echo $pro['id_stock_return_line_items'];?>,<?php echo $order_details['id_stock_return'];?>,<?php echo $order_details['rand'];?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>

</a>
<?php } ?>
         </td>
            

        </tr>
			 
	 <?php } ?> 

       
        </table>
        <?php } ?>