<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
<script src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script src="<?= base_url(); ?>js/custom.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate2.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>

<script type="text/javascript" >
function check_qty_request(i){

var qty_request = $('#qty_'+i).val();
var qty_remain = $('#remain_quantity_'+i).val();

if(!isNumber(qty_request)){
alert('Invalid Quantity.');
$('#qty_'+i).val(0);
return false;
}
if(parseFloat(qty_request) > parseFloat(qty_remain)){
alert('The Quantity you asked for are more than the available in the stock.');
$('#qty_'+i).val(0);
return false;
}
/*var total = 0;
$('.qty_request').each(function(){
total = parseFloat(total) + parseFloat($(this).val());
});
if(total > qty_item_requested){
alert('The Quantity you asked for are more than the requested quantity.');
$('#qty_request'+i).val(0);
return false;
}*/
}
</script>
</head>

<body>
<div class="container-fluid">
<div class="row-fluid">


<div class="span10-fluid" >
<!--<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li>
<span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/purchases?id_product='.$product['id_products']); ?>" >List Of Purchases</a></li>
<span class="divider">/</span>
<li class="active">List of purchases</li>

</ul>-->
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms','id'=>'addToStockOrder');
echo form_open_multipart('back_office/inventory/insertStockOrder', $attributes); 


if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
} ?>
<?php if(!empty($order_details['line_items'])) {

	 ?>
 <input type="hidden" name="id_delivery_order" value="<?php echo $order_details['id_delivery_order'];?>" />
 <input type="hidden" name="id_stock_return" value="<?php echo $id_stock_return;?>" />
 <input type="hidden" value="<?php echo $order_details['user']['id_user'];?>" name="id_user" id="id_user" />
  <input type="hidden" value="<?php echo $order_details['id_role'];?>" name="id_role" id="id_role" />
  <input type="hidden" value="1" name="return" />
  
  
<label><b><? 
if($order_details["invoice"] == '1') {?>
<?php echo lang('paid_m'); ?>
<?php } else {?>
<?php echo lang('consignment_m'); ?>
<?php }?>: </b> #<?php echo $order_details['id_delivery_order'];?></label>
<label><b><?php echo lang('trading_name');?>: </b><?php echo $order_details['user']['trading_name'];?></label>
<label><b><?php echo lang('created_date');?>: </b><?php echo  date("F d,Y h:i A", strtotime($order_details['created_date'])); ?></label>
<label><b><?php echo lang('type');?>: </b><? 
if($order_details["invoice"] == '1') {?>
<span class="label label-success"><?php echo lang('paid_m'); ?></span>
<?php } else {?>
<span class="label label-important"><?php echo lang('consignment_m'); ?></span>
<?php }?></label>

<?php $brands_line_items=$order_details['line_items'];?>

<?php if(!empty($brands_line_items)) {?>
<?php foreach($brands_line_items as $brand) {
 $line_items=$brand['line_items'];?> 


<table class="table table-striped order-details-table">
  <tr><td colspan="8" style="font-size:25px; background:#CCC; color:white; text-align:center;"><?php echo $brand['title'];?></td></tr>
          <tr>
           <th width="2%"> </th>
            <th width="15%">Product Name</th>
            <th width="10%">Sku</th>
            
            <th width="5%" style="text-align:center;">Qty</th>
            <th width="15%"  style="text-align:center;">Return Quantity</th>
          </tr>
 
          
     <?php foreach($line_items as $pro){
	
$remain_quantity=$pro['quantity']-$pro['return_quantity']-$pro['sold_quantity'];
		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];	
		$options=unSerializeStock($pro['stock']['combination']); 
			 }else{
		$sku=$pro['product']['sku'];
		$options="";}
		  ?>
		
	
<tr class="order-table-row">
    
    <td>
    <input type="checkbox" class="checklist-chkbx"  name="checklist[]" value="<?php echo $pro['id_delivery_order_line_items'];?>" ></td>
        <td>
		 <input type="hidden" name="ids[]" value="<?php echo $pro['id_delivery_order_line_items'];?>" />
		<?php echo $pro['product']['title'];?>
        <br />    <?php 
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
        </td>
        <td><?php echo $sku;?></td>
        
        <td style="text-align:center;">
     <?php echo $remain_quantity;?>
        </td>
        <td style="text-align:center;">
        <input type="hidden" id="remain_quantity_<?php echo $pro['id_delivery_order_line_items'];?>" value="<?php echo $remain_quantity;?>" />
        <input class="span qty checklist_qty return_qty" maxlength="12" title="Qty" size="4"  id="qty_<?php echo $pro['id_delivery_order_line_items'];?>" name="qty[]" onkeyup="check_qty_request(<?php echo $pro['id_delivery_order_line_items'];?>)" style="width:60px;text-align:center;"  value="0"     disabled=disabled >
        </td>


        <!--<td>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2(<?php echo $rand;?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>
Remove
</a></td>-->

        </tr>
			 
	 <?php } ?> 

       
        </table>
     <?php } ?>
     
     <table width="100%"><tr><td colspan="10">
    <div id="loader-bx"></div>
   <div class="FormResult"></div>
   <p class="pull-right" >
<input type="submit" name="submit" value="Add To Stock Return" class="btn btn-primary">
</p></td></tr></table>
<?php }}else{?>
<table width="100%"><tr><td><div class="alert alert-error" style="text-align:center;">
No available stock to return 
</div></td></tr></table>
<?php } ?>

<?php

echo form_fieldset_close();
echo form_close();
?>
</div>     
</div>
</div>

<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?></body>