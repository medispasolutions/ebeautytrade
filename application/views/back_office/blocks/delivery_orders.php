<table class="table table-striped" id="table-1">
<tfoot>
</tfoot>
<tbody>
<tr>

<th width="300">
Delivery Note / Purchase Invoice
</th>
<th>Type</th>
<th>Date</th>
<th><?php echo lang('trading_name');?></th>
<th style="width:200px;"></th>
</tr>

<? 
if(isset($info) && !empty($info)){
$i=0;
foreach($info as $val){
	

$i++; 
?>
<tr id="<?=$val["id_delivery_order"]; ?>">


<td class="title_search">
<?php if($val['invoice']==1){
	$id_invoice=$val["id_invoice"];
	$section="invoices";
	$link=route_to('back_office/inventory/view_delivery_request/invoices/'.$val['id_delivery_order'].'/'.$val['rand']);
	}else{
	$id_invoice=$val["id_delivery_order"];
	$section="delivery_note";
	$link=route_to('back_office/inventory/view_delivery_request/delivery_note/'.$val['id_delivery_order'].'/'.$val['rand']);
	}?>
<b><a href="<?php echo $link;?>" target="_blank"><?php echo '#'.$id_invoice;?></a></b>
</td>

<td><? 
if($val["invoice"] == '1') {?>
<span class="label label-success"><?php echo lang('paid_m'); ?></span>
<?php } else {?>
<span class="label label-important"><?php echo lang('consignment_m'); ?></span>
<?php }?></td>

<td >
<?php echo  date("F d,Y h:i A", strtotime($val['created_date'])); ?>
</td>
<td >
<? echo $val["trading_name"]; ?>
</td>
<?php $export_section="";?>
<?php if($section=="stock"){
$export_section='delivery_note';
	} ?>
    
<?php if($section=="lpo"){
$export_section='invoices';
	} ?> 
    
<?php if($section=="delivery_note"){
$export_section='invoices';
	} ?>  



<td>
<a  class="pull-right fancyClick3" href="<?php echo site_url('back_office/inventory/getOrdersLineItems/'.$val['id_delivery_order'].'/'.$id_stock_return);?>" style="margin-top:5px;margin-right:5px;text-transform:uppercase;" data-original-title="">[select Products]</a>
</td>





</tr>

<?php  }} else { ?>
<tr class='odd'><td colspan="10" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>