<?php if(!empty($order_details['line_items'])) { ?>

<?php $brands_line_items=$order_details['line_items'];?>

<?php if(!empty($brands_line_items)) {?>
<?php foreach($brands_line_items as $brand) {
 $line_items=$brand['line_items'];?> 
<table class="table table-striped order-details-table">
  <tr><td colspan="8" style="font-size:25px; background:#CCC; color:white; text-align:center;"><?php echo $brand['title'];?></td></tr>
          <tr>
         <th> </th>
            <th width="30%">Product Name</th>
            <th width="20%">Sku</th>
            <th width="20%">Qty</th>
            <th width="20%"  style="text-align:center;">Return Quantity</th>
            <th width="20%" >Type</th>

          </tr>
 
          
     <?php foreach($line_items as $pro){
	
$remain_quantity=$pro['quantity']-$pro['return_quantity']-$pro['sold_quantity'];
		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];	
		$options=unSerializeStock($pro['stock']['combination']); 
			 }else{
		$sku=$pro['product']['sku'];
		$options="";}
		  ?>
		
	
<tr class="order-table-row">
    
    <td>
    <input type="checkbox" class="checklist-chkbx"  name="checklist[]" value="<?php echo $pro['id_delivery_order_line_items'];?>" ></td>
        <td>
		 <input type="hidden" name="ids[]" value="<?php echo $pro['id_delivery_order_line_items'];?>" />
		<?php echo $pro['product']['title'];?>
        <br />    <?php 
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
        </td>
        <td><?php echo $sku;?></td>
        <td>
     <?php echo $remain_quantity;?>
        </td>
        <td style="text-align:center;">
        <input type="hidden" id="remain_quantity_<?php echo $pro['id_delivery_order_line_items'];?>" value="<?php echo $remain_quantity;?>" />
        <input class="span qty checklist_qty return_qty" maxlength="12" title="Qty" size="4"  id="qty_<?php echo $pro['id_delivery_order_line_items'];?>" name="qty[]" onkeyup="check_qty_request(<?php echo $pro['id_delivery_order_line_items'];?>)" style="width:50px;text-align:center;"  value="0"     disabled=disabled >
        </td>

<td><? 
if($pro["invoice"] == '1') {?>
<span class="label label-success"><?php echo lang('paid_m'); ?></span>
<?php } else {?>
<span class="label label-important"><?php echo lang('consignment_m'); ?></span>
<?php }?></td>
        <!--<td>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2(<?php echo $rand;?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>
Remove
</a></td>-->

        </tr>
			 
	 <?php } ?> 

       
        </table>
     <?php } ?>
     
     <table width="100%"><tr><td colspan="10">
    <div id="loader-bx"></div>
   <div class="FormResult"></div>
   <p class="pull-right" >
<input type="submit" name="submit" value="Add" class="btn btn-primary">
</p></td></tr></table>
<?php }}else{?>
<table width="100%"><tr><td>No available stock to return</td></tr></table>
<?php } ?>