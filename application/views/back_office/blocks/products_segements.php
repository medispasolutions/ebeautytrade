
 <?php if(!empty($segements)) { ?>   
<div class="row-fluid">
<table class="table">
<tr id=""><!--<th width="500">Title</th>--><th>Min Qty</th><th>Price(<?php echo $this->config->item('default_currency');?>)</th><th></th></tr>
<?php foreach($segements as $segement){?>
<tr id="segement_<?php echo $segement['id_product_price_segments'];?>"><!--<td>


Get <?php echo $segement['min_qty'];?> for <?php echo $segement['price'];?> <?php echo $this->config->item('default_currency');?> only</td>--><td><?php echo $segement['min_qty'];?></td><td><?php echo $segement['price'];?></td>
<td><? if ($this->acl->has_permission('product_price_segments','edit')){ ?>
<a href="<?= site_url('back_office/product_price_segments/edit/'.$segement["id_product_price_segments"].'?id_product='.$segement["id_products"]);?>" class="table-edit-link fancyClick3" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>
<? } ?>
<? if ($this->acl->has_permission('product_price_segments','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this segment ?')){ delete_segement('<?=$segement["id_product_price_segments"];?>','<?=$segement["id_products"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?> </td></tr>
<?php } ?>
</table>

</div>
<?php }else{ ?>
<div class="empty">No segments found</div>
<?php }?>
