<?
$cond = array('position' => 2);
$menu_left = getAll_cond('content_type', 'sort_order', $cond);

$menu_left_seg1 = $this->uri->segment(2);
$menu_left_seg2 = $this->uri->segment(3);
?>
<div class="accordion" id="accordion2">
    <?
    $user_id = $this->session->userdata('user_id');
    $i = 0;

    foreach ($menu_left as $val) {
        $i++;

        if (strpos(file_get_contents("./application/config/acl.php"), str_replace(" ", "_", $val["name"])) !== false) {
            if ($this->acl->has_permission(str_replace(" ", "_", $val["name"]), 'index') || $this->acl->has_permission(str_replace(" ", "_", $val["name"]), 'add')) {
                ?>
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2"
                           href="#collapse_<?= $i; ?>" style="text-transform:capitalize">
                            <?php if (!empty($val["new_name"])) {
                                $section_name = str_replace("_", " ", $val["new_name"]);
                            } else {
                                $section_name = str_replace("_", " ", $val["name"]);
                            } ?>
                            <?php echo $section_name; ?>
                        </a>
                    </div>
                    <?php $table_m = $menu_left_seg1; ?>
                    <?php if ($menu_left_seg1 == "products" || $menu_left_seg1 == "miles" || $menu_left_seg1 == "categories" || $menu_left_seg1 == "attributes" || $menu_left_seg1 == "designers") {
                        $table_m = 'products';
                    } else {
                    }
                    ?>
                    <?php if ($menu_left_seg1 == "guide" || $menu_left_seg1 == "events" || $menu_left_seg1 == "contact_list") {
                        $table_m = 'guide';
                    } ?>

                    <?php if ($menu_left_seg1 == "task" || $menu_left_seg1 == "categories_task") {
                        $table_m = 'task';
                    } ?>

                    <?php if ($menu_left_seg1 == "categories") {
                        $table_m = 'categories';
                    } ?>

                    <?php if ($menu_left_seg1 == "brand_certified_training" || $menu_left_seg1 == "training_categories") {
                        $table_m = 'brand_certified_training';
                    } ?>

                    <?php if ($menu_left_seg1 == "footer" || $menu_left_seg1 == "about_spamiles" || $menu_left_seg1 == "customer_service") {
                        $table_m = 'footer';
                    } ?>

                    <div id="collapse_<?= $i; ?>"
                         class="accordion-body collapse <? if ($menu_left_seg1 == str_replace(" ", "_", $val["name"]) || ($val["id_content"] == 25 && $table_m == "products") || ($val["id_content"] == 129 && $table_m == "guide") || ($val["id_content"] == 146 && $table_m == "footer")) echo "in"; ?>">
                        <div class="accordion-inner">
                            <ul class="nav nav-list" style="padding-left:0;padding-right:0;">
                                <?php
                                $section_arr = array(25, 129, 146, 123, 197, 198, 132);
                                if ($val["id_content"] == 25) {
                                    ?>
                                    <li class="<? if (($menu_left_seg1 == str_replace(" ", "_", $val["name"]) && $menu_left_seg2 != "add") || $menu_left_seg1 == str_replace(" ", "_", $val["name"]) && $menu_left_seg2 == "add") echo "active"; ?>">
                                        <a href="<?= site_url('back_office/products'); ?>"><i class="icon-th-list"></i>Manage
                                            Products</a></li>
                                    <?php /*?><? if ($this->acl->has_permission('categories','index')){ ?>
                                        <li class="<? if($menu_left_seg1 == 'categories') echo "active"; ?>"><a href="<?= site_url('back_office/categories'); ?>"><i class="icon-th-list"></i>Manage Categories</a></li>
                                        <?php } ?><?php */
                                    ?>
                                    <? if ($this->acl->has_permission('attributes', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'attributes') echo "active"; ?>"><a
                                                    href="<?= site_url('back_office/attributes'); ?>"><i
                                                        class="icon-th-list"></i>Manage Attributes</a></li>
                                    <?php } ?>
                                <?php }

                                if ($val["id_content"] == 198 || $val["id_content"] == 197) {
                                    ?>
                                    <li class="<? if ($menu_left_seg1 == 'categories_task') echo "active"; ?>"><a
                                                href="<?= site_url('back_office/categories_task'); ?>"><i
                                                    class="icon-th-list"></i>Manage Categories</a></li>
                                    <? if ($this->acl->has_permission('task', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'task') echo "active"; ?>"><a
                                                    href="<?= site_url('back_office/task'); ?>"><i
                                                        class="icon-th-list"></i>Manage Tasks</a></li>
                                    <?php } ?>
                                <?php }

                                if ($val["id_content"] == 132) {
                                    ?>
                                    <li class="<? if ($this->input->get('section') == 'b2b') echo "active"; ?>"><a
                                                href="<?= site_url('back_office/categories'); ?>?section=b2b">Manage
                                            B2B</a></li>

                                    <li class="<? if ($this->input->get('section') == 'b2c') echo "active"; ?>"><a
                                                href="<?= site_url('back_office/categories'); ?>?section=b2c">Manage
                                            B2C</a></li>
                                <?php }

                                if ($val["id_content"] == 123) {
                                    ?>
                                    <? if ($this->acl->has_permission('brand_certified_training', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'brand_certified_training') echo "active"; ?>">
                                            <a href="<?= site_url('back_office/brand_certified_training'); ?>"><i
                                                        class="icon-th-list"></i>List All</a></li>
                                    <?php } ?>
                                    <? if ($this->acl->has_permission('training_categories', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'training_categories') echo "active"; ?>">
                                            <a href="<?= site_url('back_office/training_categories'); ?>"><i
                                                        class="icon-th-list"></i>Training Categories</a></li>
                                    <?php } ?>
                                <?php }

                                if ($val["id_content"] == 123) {
                                    ?>
                                    <? if ($this->acl->has_permission('brand_certified_training', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'brand_certified_training') echo "active"; ?>">
                                            <a href="<?= site_url('back_office/brand_certified_training'); ?>"><i
                                                        class="icon-th-list"></i>List All</a></li>
                                    <?php } ?>
                                    <? if ($this->acl->has_permission('training_categories', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'training_categories') echo "active"; ?>">
                                            <a href="<?= site_url('back_office/training_categories'); ?>"><i class="icon-th-list"></i>Training Categories</a>
                                        </li>
                                    <?php } ?>
                                <?php }

                                if ($val["id_content"] == 129) {
                                    ?>
                                    <li class=""><a href="<?= site_url('back_office/events'); ?>" target="_blank"><i class="icon-th-list"></i>Events</a></li>
                                    <li class=""><a href="<?= site_url('back_office/spa-consultants'); ?>" target="_blank"><i class="icon-th-list"></i>Spa consultants</a></li>
                                    <li class=""><a href="<?= site_url('back_office/spa-software'); ?>" target="_blank"><i class="icon-th-list"></i>Spa software</a></li>
                                <?php }
                                
                                if ($val["id_content"] == 146) {
                                    ?>
                                    <? if ($this->acl->has_permission('customer_service', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'customer_service') echo "active"; ?>">
                                            <a href="<?= site_url('back_office/customer_service'); ?>"><i class="icon-th-list"></i>Customer Service</a>
                                        </li>
                                    <?php } ?>
                                    <? if ($this->acl->has_permission('about_spamiles', 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == 'about_spamiles') echo "active"; ?>">
                                            <a href="<?= site_url('back_office/about_spamiles'); ?>"><i class="icon-th-list"></i>About Spamiles</a>
                                        </li>
                                    <?php } ?>
                                <?php }

                                if (!in_array($val["id_content"], $section_arr)) {
                                    ?>
                                    <? if ($this->acl->has_permission(str_replace(" ", "_", $val["name"]), 'index')) { ?>
                                        <li class="<? if ($menu_left_seg1 == str_replace(" ", "_", $val["name"]) && $menu_left_seg2 != "add") echo "active"; ?>">
                                            <a href="<?= site_url('back_office/' . str_replace(" ", "_", $val["name"])); ?>"><i class="icon-th-list"></i> List All <? // $val["name"]; ?></a>
                                        </li>
                                    <? }
                                    if ($this->acl->has_permission(str_replace(" ", "_", $val["name"]), 'add')) {
                                        ?>
                                        <li class="<? if ($menu_left_seg1 == str_replace(" ", "_", $val["name"]) && $menu_left_seg2 == "add") echo "active"; ?>">
                                            <a href="<?= site_url('back_office/' . str_replace(" ", "_", $val["name"]) . '/add'); ?>"><i class="icon-plus-sign"></i> Add New <? // $val["name"]; ?></a>
                                        </li>
                                    <? } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?
            }
        }
    }
?>
</div>