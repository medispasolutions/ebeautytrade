<script>
    $(function () {
        $('#searchbox').click(function () {
            window.find();
        });
    });
</script>

<div id="header" style="margin:0 auto;">
    <div id="top" class="row">
        <div class="span8">
            <h2 class="logo">
                <?
                $cond = array('id_settings' => 1);
                $title_site = getonerow('settings', $cond);
                $seg = $this->uri->segment(2);
                $newsletter_emails = $this->fct->getAll_orderdate('newsletter');
                ?>
                <a href="<?= site_url('back_office/home/dashboard'); ?>" class="decoration"> <img
                            src="<?= base_url(); ?>uploads/website/<?= $title_site["image"]; ?>"/> <i>
                        <?php /*?><?= $title_site["website_title"]; ?><?php */ ?>
                    </i></a>
            </h2>
        </div>
        <div class="span7 pull-right" style="text-align:right; margin-right:10px;">
            <p id="userbox"><i>Hello</i> <strong>
                    <?= $this->session->userdata('user_name'); ?>
                </strong> &nbsp;| &nbsp; <i class="icon-edit"></i> <a href="<?= site_url('back_office/profile'); ?>">Edit
                    Profile</a>
                &nbsp;| &nbsp; <a href="<?= site_url(); ?>">Visit Website</a> &nbsp;| &nbsp
                <? if ($this->acl->has_permission('events', 'add')) { ?>
                    <i class="icon-wrench"></i> <a
                            href="<?= site_url('back_office/settings'); ?>">Settings</a> &nbsp;| &nbsp
                <? } ?>

                <i class="icon-off"></i> <a href="<?= site_url('back_office/home/logout'); ?>"> Logout</a> <br/>
                <small><b>Last Login:</b>
                    <?= $this->session->userdata('login_date'); ?>
                </small>
                <?php

                if ($this->session->userdata('role_access') == "supplier_admin" && $this->session->userdata('current_id_user') != "") {
                    $user = $this->ecommerce_model->getUserInfo($this->session->userdata('current_id_user'));
                    ?><br/>
                    <a href="<?= site_url('user/auto_login/' . $user["id_user"] . '/' . $user['correlation_id']); ?>?role_access=admin"><<
                        Back To Admin</a>
                <?php } ?>
            </p>
        </div>
        <div class="span6 pull-right" style="text-align:right; margin-right:10px;">
            <?php $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            /*$ar_link = str_replace('/en/','/ar/',$url);
            $en_link = str_replace('/ar/','/en/',$url);*/
            $ar_link = anchor($this->lang->switch_uri('ar'), 'AR');
            $en_link = anchor($this->lang->switch_uri('en'), 'EN');
            ?>
            <!-- <a href="<?php //echo $en_link; ?>">EN</a> &nbsp;| &nbsp <a href="<?php //echo $ar_link; ?>">AR</a>-->
            <!-- <?php //echo $ar_link; ?> &nbsp;| &nbsp <?php //echo $en_link; ?>-->
        </div>
    </div>
    <div>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container"><a class="brand">
                        <?= $title_site["website_title"]; ?>
                    </a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <?php if ($this->acl->has_permission('orders', 'index')) { ?>
                                <li <? if ($seg == "home") echo 'class="active"'; ?>><a
                                            href="<?= site_url('back_office/home/dashboard'); ?>"><i
                                                class="icon-home"></i> Dashboard</a></li>
                            <?php } ?>
                            <!-- User Section -->
                            <?
                            if ($this->acl->has_permission('users', 'index') || $this->acl->has_permission('users', 'add')) {
                                ?>
                                <li class="dropdown <? if ($seg == "users") echo 'active'; ?>"><a href="#"
                                                                                                  class="dropdown-toggle menu-mod-a"
                                                                                                  data-toggle="dropdown">
                                        <i class="icon-user"></i> Users <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <? if ($this->acl->has_permission('users', 'index')) { ?>
                                            <li><a href="<?= site_url('back_office/users/add'); ?>">Add User</a></li>
                                        <? }
                                        if ($this->acl->has_permission('users', 'index')) { ?>
                                            <li><a href="<?= site_url('back_office/users'); ?>">List Users</a></li>
                                        <? } ?>

                                        <? if ($this->acl->has_permission('business_type', 'index')) { ?>
                                            <li><a href="<?= site_url('back_office/business_type'); ?>">Business
                                                    Type</a></li>
                                        <? } ?>

                                        <? if ($this->acl->has_permission('business_type', 'index')) { ?>
                                            <li><a href="<?= site_url('back_office/users_areas'); ?>">Areas</a></li>
                                        <? } ?>

                                        <? if ($this->acl->has_permission('account_manager', 'index')) { ?>
                                            <li><a href="<?= site_url('back_office/account_manager'); ?>">Account
                                                    Manager</a></li>
                                        <? } ?>

                                        <? if ($this->acl->has_permission('how_did_you_hear_about_us', 'index')) { ?>
                                            <!--<li><a href="<?= site_url('back_office/how_did_you_hear_about_us'); ?>" >Hear About Us</a></li>-->
                                        <? } ?>
                                    </ul>
                                </li>
                            <? } ?>

                            <?php /*?> <? if ($this->acl->has_permission('units','index')){ ?>
                                <li <? if($seg == "units") echo 'class="active"'; ?>><a href="<?= site_url('back_office/units'); ?>"> <i class="icon-units"></i> Units</a></li>
                                <?
                            }?><?php */ ?>

                            <?
                            if ($this->acl->has_permission('discount_groups', 'index')) { ?>
                                <!--<li <? if ($seg == "discount_groups") echo 'class="active"'; ?>><a href="<?= site_url('back_office/discount_groups'); ?>"> <i class="icon-user"></i> Discount Groups</a></li>-->
                                <?
                            } ?>



                            <?php
                            if ($this->acl->has_permission('currency_rates', 'index')) { ?>
                                <li <? if ($seg == "currency_rates") echo 'class="active"'; ?>><a
                                            href="<?= site_url('back_office/currency_rates'); ?>"> <i
                                                class="icon-currency"></i>Currencies</a></li>
                                <?
                            } ?>

                            <?php
                            if ($this->acl->has_permission('orders', 'index')) {
                                ?>
                                <li <? if ($seg == "newsletter") echo 'class="active"'; ?>><a class="menu-mod-a"
                                                                                              href="<?= site_url('back_office/newsletter'); ?>"
                                                                                              title="<?= count($newsletter_emails); ?> subscribers">
                                        <i class="icon-messages"></i>NewsLetter
                                        <? if (count($newsletter_emails) > 0) { ?>
                                            <span class="notification red"><?= count($newsletter_emails); ?></span>
                                        <?php } ?></a></li>
                                <?
                            } ?>

                            <?php
                            if ($this->acl->has_permission('orders', 'index')) {
                                /*$new_message=$this->fct->getAll_cond('orders','sort_order',array('readed'=>0));
                                
                                $count_messages=count($new_message);*/

                                $cond3['orders.readed'] = 0;
                                $count_messages = $this->ecommerce_model->getOrders($cond3);
                                ?>
                                <li <? if ($seg == "orders") echo 'class="active"'; ?>><a class="menu-mod-a"
                                                                                          href="<?= site_url('back_office/orders'); ?>">
                                        <i class="icon-orders"></i>Orders
                                        <? if ($count_messages > 0) { ?>
                                            <span class="notification red"><?= $count_messages; ?></span>
                                        <?php } ?></a></li>
                                <?
                            } ?>

                            <?php if ($this->acl->has_permission('products', 'index')) {
                                $cond2['readed'] = 0;
                                $count_messages = $this->fct->getMessages($cond2);


                                $cc = $this->fct->getMessages(array());
                                $new_message = $this->fct->getMessages(array(), $count_messages, 0);

                                ?>
                                <li <? if ($seg == "messages") echo 'class="active"'; ?>><a class="menu-mod-a"
                                                                                            href="<?= site_url('back_office/messages'); ?>">
                                        <i class="icon-messages"></i>Messages
                                        <? if ($count_messages > 0) { ?>
                                            <span class="notification red"><?= $count_messages; ?></span>
                                        <?php } ?></a></li>
                                <?
                            } ?>

                            <?php /*?>    <?php 
if ($this->acl->has_permission('sales_history','index')){ ?>
<li <? if($seg == "sales_history") echo 'class="active"'; ?>><a href="<?= site_url('back_office/sales_history'); ?>"> 
<i class="icon-sales_history"></i>Sales History</a></li>
<?
}?><?php */ ?>

                            <?php if ($this->acl->has_permission('products', 'index')) { ?>
                                <li <? if ($seg == "inventory") echo 'class="active"'; ?>><a
                                            href="<?= site_url('back_office/inventory'); ?>">
                                        <i class="icon-inventory"></i>Inventory</a></li>
                                <?
                            } ?>


                            <!--    <?php
                            if ($this->acl->has_permission('sales_history', 'index')) { ?>
              <li <? if ($seg == "sales_history") echo 'class="active"'; ?>><a href="<?= site_url('back_office/sales_history'); ?>"> <i class="icon-sales_history"></i> Sales History</a></li>
              <?
                            } ?>-->

                            <!-- <li <? //if($seg == "pages") echo 'class="active"'; ?>><a href="<? //= base_url(); ?>back_office/pages"> <i class="icon-book"></i> Views</a></li>-->

                            <?php
                            $this->db->select('*');
                            $this->db->where('status ', '2');
                            $this->db->where('deleted ', '0');
                            $q = $this->db->get('products');
                            $count = $q->result();
                            ?>
                            <li><a class="menu-mod-a"
                                   href="<?= site_url('back_office/products?status=2&report_submit=Filter'); ?>"> <i
                                            class="icon-book"></i> Under Review Product
                                    <? if (count($count) > 0) { ?>
                                        <span class="notification red"><?= count($count); ?></span>
                                    <?php } ?></a></li>
                            </li>
                            
                            <?php
                            $this->db->select('*');
                            $this->db->where('status ', '2');
                            $this->db->where('deleted ', '0');
                            $q = $this->db->get('brands');
                            $countBrand = $q->result();
                            ?>
                            
                            <li><a class="menu-mod-a"
                                   href="<?= site_url('back_office/brands?brand_name=&brand=&supplier=&status=2&set_as_featured='); ?>"> <i
                                            class="icon-book"></i> Under Review Brands
                                    <? if (count($countBrand) > 0) { ?>
                                        <span class="notification red"><?= count($countBrand); ?></span>
                                    <?php } ?></a></li>
                            </li>
                            
                            <?
                            if ($this->acl->has_permission('roles', 'index')) { ?>
                                <li <? if ($seg == "roles") echo 'class="active"'; ?>><a
                                            href="<?= site_url('back_office/roles'); ?>"> <i class="icon-lock"></i>
                                        Manage Permissions</a></li>
                                <?
                            }
                            if ($this->acl->has_permission('recycle', 'index')) { ?>
                                <li <? if ($seg == "recycle") echo 'class="active"'; ?>><a
                                            href="<?= site_url('back_office/recycle'); ?>"> <i class="icon-trash"></i>
                                        Recycle Bin</a></li>
                            <? } ?>
                            <? if ($this->session->userdata('level') == 3) { ?>
                                <li class="dropdown <? if ($seg == "control" || $seg == "install") echo 'active'; ?>"><a
                                            href="#" class="dropdown-toggle" data-toggle="dropdown"> <i
                                                class="icon-globe"></i> Super Management<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?= site_url('back_office/control'); ?>">Manage content type</a>
                                        </li>
                                    </ul>
                                </li>
                            <? } ?>
                            <?php if ($this->config->item("language_module") && $this->session->userdata('level') == 3) { ?>
                                <li <? if ($seg == "languages") echo 'class="active"'; ?>><a
                                            href="<?= site_url('back_office/languages'); ?>"> <i class="icon-lock"></i>
                                        Manage Languages</a></li>
                            <? } ?>

                        </ul>
                        <? if ($seg != "home") { ?>
                            <a href="javascript: history.go(-1)" class="btn pull-right" style="margin-right:10px"> <i
                                        class=" icon-arrow-left"></i> BACK</a>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearFix">&nbsp;</span></div>
<? error_reporting(E_ALL); ?>
<!-- end of #header -->