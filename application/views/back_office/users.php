<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
    function delete_row(db_name) {
        var url = '<?= site_url('back_office/users/checkUser'); ?>/' + db_name;
        $.post(url, {id_user: db_name}, function (data) {

            if (data == 1) {
                window.location = '<?= site_url('back_office/users/delete'); ?>/' + db_name;
            } else {
                alert('Srry! you can not delete the row.');
            }
        });

        return false;

    }

    $(function () {
        $('#table-1').tableDnD({
            onDrop: function (table, row) {
                var ser = $.tableDnD.serialize();
                $('#result').load('<?= site_url('back_office/users/sorted'); ?>' + '?' + ser);
            }
        });
        $("#show_items").change(function () {

            var val = $(this).val();
            $("#result").html(val);
            $("#show_items").submit();
        });
    });
</script>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <? $this->load->view('back_office/includes/left_box'); ?>
        </div>

        <div class="span10 cont_h">
            <div class="span10-fluid">
                <ul class="breadcrumb">
                    <li><?= $title; ?></li>
                    <li class="pull-right">
                        <a href="<?= site_url('back_office/users/add'); ?>" class="btn btn-info top_btn">Create New
                            User</a>
                    </li>
                </ul>
            </div>
            <div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">
                <div class="row-fluid sortable">
                    <div class="box span12">
                        <div class="box-header well">
                            <h2><i class="icon-search"></i> Filter by</h2>
                            <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i
                                            class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i
                                            class="icon-remove"></i></a></div>
                        </div>
                        <div class="box-content">
                            <div class="sortable row-fluid">
                                <form method="get" action="<?= site_url("back_office/users") ?>">
                                    <div class="row-fluid">
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">User ID<br/><input
                                                    type="text" id="user_id" name="user_id"
                                                    value="<? if (isset($_GET['user_id'])) {
                                                        echo $_GET['user_id'];
                                                    } ?>"/></div>

                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">
                                            Name<br/><input type="text" id="name" name="name"
                                                            value="<? if (isset($_GET['name'])) {
                                                                echo $_GET['name'];
                                                            } ?>"/></div>

                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">User
                                            Email<br/><input type="text" id="user_email" name="user_email"
                                                             value="<? if (isset($_GET['user_email'])) {
                                                                 echo $_GET['user_email'];
                                                             } ?>"/></div>


                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Trade
                                            Name<br/><input type="text" id="trading_name" name="trading_name"
                                                            value="<? if (isset($_GET['trading_name'])) {
                                                                echo $_GET['trading_name'];
                                                            } ?>"/></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Person
                                            Name<br/><input type="text" id="person_name" name="person_name"
                                                            value="<? if (isset($_GET['person_name'])) {
                                                                echo $_GET['person_name'];
                                                            } ?>"/></div>
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Country<br/>
                                            <select name="country" id="country">
                                                <option value="">- All -</option>
                                                <?php foreach ($countries as $country) { ?>
                                                    <option value="<?php echo $country['id_countries']; ?>" <?php if (isset($_GET['country']) && $_GET['country'] == $country['id_countries']) echo 'selected="selected"'; ?>><?php echo $country['title']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Role<br/>
                                            <?php
                                            if ($this->session->userdata('level') != 3) {
                                                $roles = $this->fct->getAll_cond('roles', 'title', array('id_roles !=' => 3));
                                            } else {
                                                $roles = $this->fct->getAll('roles', 'title');
                                            }
                                            ?>
                                            <select name="role" id="role">
                                                <option value="">- All -</option>
                                                <?php foreach ($roles as $role) {
                                                    $cl = '';
                                                    if (isset($_GET['role']) && $_GET['role'] == $role['id_roles'])
                                                        $cl = 'selected="selected"';

                                                    ?>
                                                    <option value="<?php echo $role['id_roles']; ?>" <?php echo $cl; ?>><?php echo lang($role['title']); ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Discount
                                            Group<br/>
                                            <?php
                                            $discount_groups = $this->fct->getAll('discount_groups', 'title');
                                            ?>
                                            <select name="discount_group" id="discount_group">
                                                <option value="">- All -</option>
                                                <?php
                                                $cl = '';
                                                if (isset($_GET['discount_group']) && $_GET['discount_group'] == "not_assigned")
                                                    $cl = 'selected="selected"';
                                                ?>
                                                <option value="not_assigned" <?php echo $cl; ?>> - not assigned -
                                                </option>
                                                <?php foreach ($discount_groups as $grp) {
                                                    $cl = '';
                                                    if (isset($_GET['discount_group']) && $_GET['discount_group'] == $grp['id_discount_groups'])
                                                        $cl = 'selected="selected"';

                                                    ?>
                                                    <option value="<?php echo $grp['id_discount_groups']; ?>" <?php echo $cl; ?>><?php echo $grp['title']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row-fluid">

                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">First
                                            Step<br/>
                                            <select name="first_step" id="first_step">

                                                <option value="0" <?php if (isset($_GET['first_step']) && $_GET['first_step'] == "0") echo 'selected="selected"'; ?>>
                                                    InActive
                                                </option>
                                                <option value="1" <?php if (isset($_GET['first_step']) && $_GET['first_step'] == "1") echo 'selected="selected"'; ?>>
                                                    Active
                                                </option>
                                            </select>
                                        </div>

                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Status<br/>
                                            <select name="status" id="status">
                                                <option value="">- All -</option>

                                                <option value="blocked" <?php if (isset($_GET['status']) && $_GET['status'] == "blocked") echo 'selected="selected"'; ?>>
                                                    Blocked
                                                </option>
                                                <option value="active" <?php if (isset($_GET['status']) && $_GET['status'] == "active") echo 'selected="selected"'; ?>>
                                                    Active
                                                </option>
                                                <option value="under_review" <?php if (isset($_GET['status']) && $_GET['status'] == "under_review") echo 'selected="selected"'; ?>>
                                                    Under Review
                                                </option>
                                                <option value="incomplete" <?php if (isset($_GET['status']) && $_GET['status'] == "incomplete") echo 'selected="selected"'; ?>>
                                                    Incomplete
                                                </option>
                                            </select>
                                        </div>

                                        <!--Account Manager -->
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">
                                            <label><?php echo lang('account_manager'); ?></label>
                                            <?php $account_manager = $this->fct->getAll('account_manager', 'title'); ?>

                                            <select name="account_manager" id="account_manager">
                                                <option value="">-Select <?php echo lang('account_manager'); ?>-
                                                </option>
                                                <?php


                                                foreach ($account_manager as $val) {
                                                    $cl = '';
                                                    if (isset($_GET['account_manager']) && $_GET['account_manager'] == $val['id_account_manager'])
                                                        $cl = 'selected="selected"';
                                                    ?>
                                                    <option value="<?php echo $val['id_account_manager']; ?>" <?php echo $cl; ?>><?php echo $val['title']; ?></option>
                                                <?php } ?>
                                            </select></div>

                                        <!--Areas-->
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">
                                            <label><?php echo lang('areas'); ?></label>
                                            <?php
                                            $cond3 = array();
                                            $users_areas = $this->fct->getAll_cond('users_areas', 'title asc', $cond3);
                                            ?>

                                            <select name="id_users_areas" id="users_areas">
                                                <option value="">-Select <?php echo lang('area'); ?>-</option>
                                                <?php
                                                foreach ($users_areas as $val) {
                                                    $cl = '';
                                                    if (isset($_GET['id_users_areas']) && $_GET['id_users_areas'] == $val['id_users_areas'])
                                                        $cl = 'selected="selected"';
                                                    ?>
                                                    <option value="<?php echo $val['id_users_areas']; ?>" <?php echo $cl; ?>><?php echo $val['title']; ?></option>
                                                <?php } ?>
                                            </select></div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="row-fluid">
                                            <div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">
                                                City<br/><input type="text" id="city" name="city"
                                                                value="<? if (isset($_GET['city'])) {
                                                                    echo $_GET['city'];
                                                                } ?>"/></div>
                                        </div>
                                        <div class="fl" style="margin:0 15px 0 0;width: 230px; float:right;">
                                            <input type="submit" class="btn btn-primary" value="Filter"/>
                                            <input type="submit" class="btn btn-primary" name="report_submit" value="export"/>
                                            <br/>
                                            <input type="submit" class="btn btn-primary" name="report_submit" value="subscribers" style="margin: 5px 0 0;"/>
                                            <input type="submit" class="btn btn-primary" name="report_submit" value="supplier" style="margin: 5px 0 0;"/>
                                        </div>
                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hundred pull-left">
                <div id="result"></div>

                <form action="<?= site_url('back_office/users/delete_all'); ?>" method="post" name="list_form">
                    <table class="table table-striped" id="table-1">
                        <thead>
                        <? if ($this->session->userdata('success_message')){ ?>
                        <tr>
                            <td colspan="14" align="center" style="text-align:center">
                                <div class="alert alert-success">
                                    <?= $this->session->userdata('success_message'); ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <? } ?>
                            <? if ($this->session->userdata('error_message')) { ?>
                                <div class="alert alert-error">
                                    <?= $this->session->userdata('error_message'); ?>
                                </div>
                            <? } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>Role</td>
                            <td>Full Name</td>
                            <!--<td>Person Name</td>-->
                            <td>Trade Name</td>
                            <td>Email</td>
                            <td>Country</td>
                            <td width="100px">Created Date</td>
                            <td>Status</td>
                            <td style="text-align:center;width:216px;display:table">Action</td>
                        </tr>
                        </thead>
                        <tfoot>

                        <tr>
                            <td class="col-chk"><input type="checkbox" id="checkAllAuto"/></td>
                            <td colspan="8">
                                <div class="pull-right">
                                    <select class="form-select" name="check_option">
                                        <option value="option1">Bulk Options</option>
                                        <option value="delete_all">Delete All</option>
                                    </select>
                                    <a onclick="document.forms['list_form'].submit();"
                                       class="btn btn-primary btn_mrg"><span>perform action</span></a></div>
                            </td>
                        </tr>

                        </tfoot>
                        <tbody>
                        <?
                        if (count($info) > 0) {
                            $i = 0;
                            foreach ($info as $val) {
                                $i++;
                                ($i % 2 == 0) ? $odd = '' : $odd = 'odd';
                                ?>
                                <tr class="<?= $odd; ?>" id="<?= $val["id_user"]; ?>">
                                    <td class="col-chk"><input type="checkbox" name="cehcklist[]"
                                                               value="<?= $val["id_user"]; ?>"/></td>
                                    <?php
                                    $cl = 'success';
                                    if ($val['id_roles'] == 2) $cl = 'important';
                                    if ($val['id_roles'] == 4) $cl = 'warning';
                                    if ($val['id_roles'] == 5) $cl = 'info';
                                    if ($val['id_roles'] == 6) $cl = 'inverse';
                                    ?>
                                    <td class="" width="">
                                        <span class="label label-<?php echo $cl ?>">
		                                    <?php echo lang($this->fct->getonecell('roles', 'title', array('id_roles' => $val['id_roles']))); ?>
		                                </span>
		                            </td>
                                    <td class="" width=""><?= $val["name"]; ?></td>
                                    <!--<td class="" width=""><?= $val["person_name"]; ?></td>-->
                                    <td class="" width=""><?= $val["trading_name"]; ?></td>
                                    <td class="" width=""><?= $val["email"]; ?></td>
                                    <td class=""
                                        width=""><?php echo $this->fct->getonecell('countries', 'title', array('id_countries' => $val["id_countries"])); ?></td>
                                    <td class="" width=""><?= $val["created_date"]; ?></td>
                                    <td class="" width=""><? if ($val['status'] == 1) {
                                            echo '<span class="label label-success">Active</span>';
                                        } else {
                                            if ($val['status'] == 2) {
                                                echo '<span class="label label-important">Under Review</span>';
                                            } else {
                                                if ($val['status'] == 3) {
                                                    echo '<span class="label label-important">Incomplete</span>';
                                                } else {
                                                    echo '<span class="label label-important">Blocked</span>';
                                                }
                                            }
                                        } ?></td>
                                    <td class="" style="text-align:center;">
                                        <a href="<?= site_url('back_office/tracing'); ?>"
                                           class="table-edit-link cur cur">
                                            <i class="icon-search"></i>Audit Log</a>
                                        <span class="hidden"> | </span>
                                        <a href="<?= site_url('user/auto_login/' . $val["id_user"] . '/' . $val['correlation_id']); ?>"
                                           target="_parent" class="table-edit-link cur cur">
                                            <i class="icon-off"></i> Auto Login</a> <br/>
                                        <?php if ($val['id_roles'] == 5) { ?>
                                            <a href="<?= site_url('user/auto_login/' . $val["id_user"] . '/' . $val['correlation_id']); ?>?role_access=supplier_admin"
                                               class="table-edit-link cur cur">
                                                <i class="icon-off"></i> Auto login to supplier admin</a>
                                            <br/>
                                        <?php } ?>
                                        <a href="<?= site_url('back_office/users/edit/' . $val["id_user"]); ?>"
                                           class="table-edit-link cur cur">
                                            <i class="icon-edit"></i> Edit</a>
                                        <span class="hidden"> | </span>
                                        <a onclick="if(confirm('Are you sure you want delete this user ?')){ delete_row('<?= $val["id_user"]; ?>'); }"
                                           class="table-delete-link cur">
                                            <i class="icon-remove-sign"></i> Delete</a></td>
                                </tr>
                            <? }
                        } else {
                            echo '<tr class="odd"><td colspan="12" style="text-align:center;">No records available . </td></tr>';
                        } ?>
                        </tbody>
                    </table>
                </form>
                <div class="pagination_container">
                    <div class="span2 pull-left">
                        <? $search_array = array("25", "100", "200", "500", "1000", "All"); ?>
                        <form action="<?= site_url("back_office/users"); ?>" method="post" id="show_items">
                            Show Items&nbsp;
                            <select name="show_items" class="input-mini">
                                <? for ($i = 0; $i < count($search_array); $i++) { ?>
                                    <option value="<?= $search_array[$i]; ?>" <? if ($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
                                <? } ?>
                            </select>
                        </form>
                    </div>
                    <? echo $this->pagination->create_links(); ?>
                </div>
            </div><!-- end of div.box -->

        </div>
    </div>
    <? $this->session->unset_userdata('success_message'); ?>
    <? $this->session->unset_userdata('error_message'); ?> 