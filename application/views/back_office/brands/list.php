<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
    function delete_row(id) {
        window.location = "<?= site_url("back_office/brands/delete"); ?>/" + id;
        return false;
    }

    $(function () {
        $("#table-1").tableDnD({
            onDrop: function (table, row) {
                var ser = $.tableDnD.serialize();
                $("#result").load("<?= site_url("back_office/brands/sorted"); ?>" + "?" + ser);
            }
        });

        $("#match2 input[name='search']").live('keyup', function (e) {
            e.preventDefault();
            var id = this.id;
            $('#match2 tbody tr').css('display', 'none');
            var searchtxt = $.trim($(this).val());
            var bigsearchtxt = searchtxt.toUpperCase();
            var smallsearchtxt = searchtxt.toLowerCase();
            var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                return letter.toUpperCase();
            });
            if (searchtxt == "") {
                $('#match2 tbody tr').css('display', "");
            } else {
                $('#match2 tbody tr td.' + id + ':contains("' + searchtxt + '")').parent().css('display', "");
                $('#match2 tbody tr td.' + id + ':contains("' + bigsearchtxt + '")').parent().css('display', "");
                $('#match2 tbody tr td.' + id + ':contains("' + fbigsearchtxt + '")').parent().css('display', "");
                $('#match2 tbody tr td.' + id + ':contains("' + smallsearchtxt + '")').parent().css('display', "");
            }
        });

        $("#show_items").change(function () {
            var val = $(this).val();
            $("#result").html(val);
            $("#show_items").submit();
        });

    });
</script><?php
$lang = "";
if ($this->config->item("language_module")) {
    $lang = getFieldLanguage($this->lang->lang());
}
$sego = $this->uri->segment(4);
$gallery_status = "0";
?>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <? $this->load->view("back_office/includes/left_box"); ?>
        </div>

        <div class="span10">
            <div class="span10-fluid">
                <?
                if (isset($breadcrumbs)) {
                    echo $breadcrumbs;
                } else {
                    $ul = array(

                        $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
                    );
                    if ($this->config->item("language_module") && isset($id)) {
                        $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="' . site_url('back_office/translation/section/supplier_info/' . $id) . '" class="btn btn-info top_btn">Translate</a>');

                    }
                    $ul['add_brands'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="' . site_url('back_office/brands/add') . '" class="btn btn-info top_btn">Add Brand</a>');

                    $ul_attributes = array(
                        'class' => 'breadcrumb'
                    );
                    echo ul($ul, $ul_attributes);
                }
                ?>
            </div>
            <?php if (!checkIfSupplier_admin()) { ?>

                <div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">


                    <div class="row-fluid sortable">
                        <div class="box span12">
                            <div class="box-header well">
                                <h2><i class="icon-search"></i> Filter by</h2>
                                <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i
                                                class="icon-chevron-up"></i></a><a href="#"
                                                                                   class="btn btn-close btn-round"><i
                                                class="icon-remove"></i></a></div>
                            </div>
                            <div class="box-content">
                                <div class="sortable row-fluid">
                                    <form method="get" action="<?= site_url("back_office/brands") ?>">
                                        <!--Product Name-->
                                        <!--<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;">Brand Name<br /><input type="text" id="brand_name" name="brand_name" value="<? if (isset($_GET['brand_name'])) {
                                            echo $_GET['brand_name'];
                                        } ?>" /></div>-->
                                        <!--Brand Name-->
                                        <div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;">Brand name<br />
                                        <input type="text" id="brand_name" name="brand_name" value="<? if(isset($_GET['brand_name'])) { echo $_GET['brand_name']; }?>" />
                                        </div>
                                        
                                        <div class="fl"
                                             style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;"><?php echo 'Brand'; ?>
                                            <br/>
                                            <?php

                                            $brands = $this->fct->getAll('brands', 'title'); ?>
                                            <select name="brand" id="brand">
                                                <option value="">- All -</option>
                                                <?php foreach ($brands as $val) {
                                                    $cl = '';
                                                    if (isset($_GET['brand']) && $_GET['brand'] == $val['id_brands'])
                                                        $cl = 'selected="selected"';
                                                    ?>
                                                    <option value="<?php echo $val['id_brands']; ?>" <?php echo $cl; ?>><?php echo $val['title']; ?></option>
                                                <?php } ?>
                                            </select></div>
                                        <div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;"><?php echo lang('trading_name'); ?>
                                            <br/>
                                            <?php
                                            $cond['user.status'] = 1;
                                            $cond['user.completed'] = 1;
                                            $cond['user.id_roles'] = 5;
                                            $users = $this->fct->getAll_cond('user', 'trading_name', $cond); ?>
                                            <select name="supplier" id="supplier">
                                                <option value="">- All -</option>
                                                <?php foreach ($users as $val) {
                                                    $cl = '';
                                                    if (isset($_GET['supplier']) && $_GET['supplier'] == $val['id_user'])
                                                        $cl = 'selected="selected"';
                                                    ?>
                                                    <option value="<?php echo $val['id_user']; ?>" <?php echo $cl; ?>><?php echo $val['trading_name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <!--STATUS-->
                                        <div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Status<br/>
                                            <select name="status" id="status">
                                                <option <?php if ($this->input->get('status') == "") echo 'selected="selected"'; ?>
                                                        value="">-All-
                                                </option>
                                                <option <?php if ($this->input->get('status') == "1") echo 'selected="selected"'; ?>
                                                        value="1">Published
                                                </option>
                                                <option <?php if ($this->input->get('status') == "0") echo 'selected="selected"'; ?>
                                                        value="0">Unpublished
                                                </option>
                                                <option <?php if ($this->input->get('status') == "2") echo 'selected="selected"'; ?>
                                                        value="2">Under Review
                                                </option>
                                            </select>
                                        </div>
                                        <!-- Feature -->
                                        <div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Set as featured<br/>
                                            <select name="set_as_featured" id="set_as_featured">
                                                <option <?php if ($this->input->get('set_as_featured') == "") echo 'selected="selected"'; ?>
                                                        value="">-All-
                                                </option>
                                                <option <?php if ($this->input->get('set_as_featured') == "0") echo 'selected="selected"'; ?>
                                                        value="0">no
                                                </option>
                                                <option <?php if ($this->input->get('set_as_featured') == "1") echo 'selected="selected"'; ?>
                                                        value="1">yes
                                                </option>
                                            </select>
                                        </div>
                                        <div class="fl" style="margin:21px 0 0 0; float:left">
                                            <input type="submit" class="btn btn-primary" value="Filter"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <div class="hundred pull-left" id="match2">
                <div id="result"></div>
                <?
                $attributes = array('name' => 'list_form');
                echo form_open_multipart('back_office/brands/delete_all', $attributes);
                ?>
                <table class="table table-striped" id="table-1">
                    <thead>
                    <? if ($this->session->userdata("success_message")){ ?>
                    <tr>
                        <td colspan="8" align="center" style="text-align:center">
                            <div class="alert alert-success">
                                <?= $this->session->userdata("success_message"); ?>
                            </div>
                        </td>
                    <tr>
                        <? } ?>
                    <tr>
                        <td width="2%">&nbsp;</td>
                        <th>
                            Brand Name
                        </th>
                        <th>
                            <?php echo lang('trading_name'); ?>
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Set as featured
                        </th>
                        <th style="text-align:center;" width="250">ACTION</th>
                    </tr>

                    </thead>
                    <tbody>
                    <?
                    if (count($info) > 0) {
                        $i = 0;
                        foreach ($info as $val) {
                            /*echo "<pre>";
                            print_r($info);exit;*/
                            $i++;;
                            ?>
                            <tr id="<?= $val["id_brands"]; ?>">
                                <td class="col-chk"></td>
                                <td class="title_search">
                                    <? echo $val["title" . $lang]; ?>
                                    <br/>
                                    Created Date:<?php echo $val['created_date']; ?>
                                </td>
                                <td>

                                    <?php $supplier_name = $this->fct->getonecell('user', 'trading_name', array('id_user' => $val['id_user'])); ?>
                                    <?php
                                    if (!empty($supplier_name)) {
                                        echo $supplier_name;
                                    } else {
                                        echo "-";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <? if ($val["status"] == 1) {
                                        echo "<span class=' label label-success' >Published</span>";
                                    } else {
                                        if ($val['status'] == 2) {
                                            echo "<span class=' label label-important ' >Under Review</span>";
                                        } else {
                                            echo "<span class=' label label-important ' >Unpublished</span>";
                                        }
                                    }

                                    ?>
                                </td>
                                <td>
                                    <? if ($val["set_as_featured"] == 1)
                                        echo "<span class=' label label-success' >Active</span>";
                                    else
                                        echo "<span class=' label label-important ' >InActive</span>"; ?>
                                </td>

                                <td style="text-align:center">
                                    <? if ($this->acl->has_permission('brands', 'edit')) { ?>

                                        <a href="<?= site_url('back_office/brands/edit/' . $val["id_brands"]); ?>"
                                           class="table-edit-link" title="Edit">
                                            <i class="icon-edit"></i> Edit</a>
                                    <? } ?>
                                    <? if ($this->acl->has_permission('brands', 'delete')) { ?>
                                        <span class="hidden"> | </span>
                                        <a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?= $val["id_brands"]; ?>'); }"
                                           class="table-delete-link cur" title="Delete">
                                            <i class="icon-remove-sign"></i> Delete</a>
                                    <? } ?>
                                </td>
                            </tr>
                        <? }
                    } else { ?>
                        <tr class='odd'>
                            <td colspan="8" style='text-align:center;'>No records available .</td>
                        </tr>
                    <? } ?>
                    </tbody>
                </table>
                <? echo form_close(); ?>
                <div class="pagination_container">
                    <div class="span2 pull-left">
                        <? $search_array = array("25", "100", "200", "500", "1000", "All"); ?>
                        <form action="<?= site_url("back_office/brands"); ?>" method="post" id="show_items">
                            Show Items&nbsp;
                            <select name="show_items" class="input-mini">
                                <? for ($i = 0; $i < count($search_array); $i++) { ?>
                                    <option value="<?= $search_array[$i]; ?>" <? if ($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
                                <? } ?>
                            </select>
                        </form>
                    </div>
                    <? echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message");
?> 