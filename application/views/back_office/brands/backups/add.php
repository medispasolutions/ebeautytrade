<script>
$(document).ready(function(){
	$('select[name="supplier"]').change(function(){
		
		var value=$(this).val();
		var text=$('select[name="supplier"] option:selected').text();
		if(value==""){
			text="supplier";}
			$('#supplier_name').html(text);
	

    	});});
</script>

<?php $size_arr=getMaxSize('file');?>
<?php 
$cond_brand=array();
$user=$this->ecommerce_model->getUserInfo();

$cond_brand['id_roles']=5;
$cond_brand['completed']=1;
$cond_brand['guest']=0;
$users=$this->fct->getAll_cond('user','id_user',$cond_brand);

?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {

	echo $breadcrumbs;
}
else {
		if($this->session->userdata("back_link")!=""){
		$back_link=$this->session->userdata("back_link");
		}else{
   	   $back_link=site_url("back_office/brands");}
$ul = array('<a href="'.$back_link.'" class=""><b>List Brands</b></a><span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/brands/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			    
			}
			$ul['add_brands'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/banner/add').'" class="btn btn-info top_btn">Add Brand</a>');

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/brands/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["logo"] = "";
$info["button_text"] = "Meeting Request";
$info["overview"] = "";
$info["image"] = "";
$info["video"] = "";
$info["set_as_featured"] = "";
$info["deliver_text"] = "";
$info["slide_1"] = "";
$info["slide_2"] = "";
$info["slide_3"] = "";
$info["slide_4"] = "";
$info["photo"] = "";
$info["position"] = "";
$info["name"] = "";
$info["country_code"] = "";
$info["city_code"] = "";
$info["number"] = "";
$info["mobile"] = "";
$info["email"] = "";
$info["catalog"] = "";
$info["status"] = "";
if(!isset($info['phone'])){	
$info['phone'] = "";
}

$info["catalog_link"] = "";
$info["url"] = "";
$info["access_code"] = "";
$info["promote_to_front"] = "";
$info["id_user"]=0;
$info["expire_date"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>
<div class="row-fluid">
<div class="span6">
<?php
//TITLE SECTION.

echo form_label('<b>Brand Name&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("id"=>"title","name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span","onKeyDown"=>"limitText(this.form.title,0,".getLimitText('title').");","onKeyUp"=>"limitText(this.form.title,0,".getLimitText('title').");" ));
echo form_error("title","<span class='text-error'>","</span>");
echo br();
?>
</div>

<?php if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo(); ?>
	<input type="hidden" name="supplier" value="<?php echo $user['id_user'];?>" />
<?php }else{?>
<div class="span6">
<label><b><?php echo lang('trading_name');?>*</b></label>
<select name="supplier" >
<option value="">-Select-</option>
<?php foreach($users as $val){?>
<option value="<?php echo $val['id_user'];?>" <?php if($val['id_user']==$info["id_user"] || $val['id_user']==set_value("supplier",$info["id_user"]) ) echo "selected";?>><?php echo $val['trading_name'];?></option>
<?php } ?>
</select>
<?php echo form_error("supplier","<span class='text-error'>","</span>");?>
</div>
<?php } ?></div>


<div class="row-fluid">
<div class="span6">
<?php
//LOGO SECTION.
echo form_label('<b>LOGO <small class="blue">(Max Size '.$size_arr['size_mega'].'Mb)</small></b>', 'LOGO');
echo form_upload(array("name" => "logo", "class" => "input-large"));
echo "<span >";
if($info["logo"] != ""){ 
echo '<span id="logo_'.$info["id_brands"].'">'.anchor('uploads/brands/'.$info["logo"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("brands","logo","<?php echo $info["logo"]; ?>",<?php echo $info["id_brands"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo "<div></div>";
echo form_error("logo","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<?php 
//Catalog SECTION.
echo form_label('<b>CATALOG </b>', 'CATALOG');
echo form_upload(array("name" => "catalog", "class" => "input-large"));
echo "<span >";
if($info["catalog"] != ""){ 
echo '<span id="catalog_'.$info["id_brands"].'">'.anchor('uploads/brands/'.$info["catalog"],'show catalog',array("class" => 'blue',"target" => '_blank'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("brands","catalog","<?php echo $info["catalog"]; ?>",<?php echo $info["id_brands"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Catalog Available</span>"; } 
echo "</span>";
echo "<div></div>";
echo form_error("catalog","<span class='text-error'>","</span>");
echo br();
?>
</div></div>
<?php 
//OVERVIEW SECTION.
echo form_label('<b>OVERVIEW</b></b>', 'OVERVIEW');
echo form_textarea(array("name" => "overview".$lang, "value" => set_value("overview".$lang,$info["overview".$lang]),"class" =>"span","rows" =>6, "cols" =>100));
echo form_error("overview","<span class='text-error'>","</span>");
echo br();
//IMAGE SECTION.
/*echo form_label('<b>IMAGE</b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_brands"].'">'.anchor('uploads/brands/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("brands","image","<?php echo $info["image"]; ?>",<?php echo $info["id_brands"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();*/

?>
<div class="row-fluid">
<div class="span6">
<?php
//VIDEO SECTION.
echo form_label('<b>YOUTUBE<small class="blue" style="text-transform:none"> (ex:https://www.youtube.com/watch?v=C0DPdy98e4c)</small></b>', 'YOUTUBE');
echo form_input(array('name' => 'video', 'value' => set_value("video",$info["video"]),'class' =>'span' ));
echo form_error("video","<span class='text-error'>","</span>");
echo br();
?>
</div>


<div class="span6">

<?php 

//URL SECTION.
echo form_label('<b>Access Code</b>', 'Access Code');
echo form_input(array('name' => 'access_code', 'value' => set_value("access_code",$info["access_code"]),'class' =>'span' ));

echo form_error("access_code","<span class='text-error'>","</span>");
echo br();

?>


</div>

</div>
<?php if(!checkIfSupplier_admin()){?>
<div class="row-fluid">
<div class="span6">
<?php

//SET AS FEATURED SECTION.
echo form_label('<b>SET AS FEATURED</b>', 'SET AS FEATURED');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("set_as_featured", $options,set_value("set_as_featured",$info["set_as_featured"]), 'class="span"');
echo form_error("set_as_featured","<span class='text-error'>","</span>");
echo br();
?>
</div>

<div class="span6">
<?php
echo form_label('<b>STATUS</b>', 'STATUS');
$options = array(
                  "" => " - select option - ",
                  1 => "Published",
                  0 => "Unpublished",
				  2 => "Under Review",
                );
echo form_dropdown("status", $options,set_value("status",$info["status"]), 'class="span"');
echo form_error("status","<span class='text-error'>","</span>");
echo br();
?>
</div>


</div>  <?php } ?>
<div class="row-fluid">


<div class="span6">
<?php 

//URL SECTION.
echo form_label('<b>WEBSITE <small class="blue" style="text-transform:none">(ex:http://www.spamiles.com)</small></b>', 'WEBSITE URL');
echo form_input(array('name' => 'url', 'value' => set_value("url",$info["url"]),'class' =>'span' ));
echo form_error("url","<span class='text-error'>","</span>");
echo br();

?>
</div>
<div class="span6">
<?php 
echo form_label('<b>Display without header</b>', 'Display without header  ');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("promote_to_front", $options,set_value("promote_to_front",$info["promote_to_front"]), 'class="span"');
echo form_error("promote_to_front","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<?php 

 echo form_fieldset("Shop by brand sliders  (Width:850px,Height:160px)");?>
<div class="row-fluid">

<?php for($i=1;$i<5;$i++){ ?>

<div class="span6">
<label class="field-title" style="margin-top:10px;"><b>Slide <?php echo $i;?> <small class="blue">(Max Size <?php echo $size_arr['size_mega'];?>Mb)</small>:</b></label>
<?php

echo form_upload(array("name" => "slide_".$i, "class" => "input-large"));
echo "<span >";
if($info["slide_".$i] != ""){ 
echo '<span id="slide_'.$i.'_'.$info["id_brands"].'">'.anchor('uploads/brands/'.$info["slide_".$i],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("brands","slide_<?php echo $i;?>","<?php echo $info["slide_".$i]; ?>",<?php echo $info["id_brands"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No  Image Available</span>"; } 
echo "</span>";
echo "<div></div>";
echo form_error("slide_".$i,"<span class='text-error'>","</span>");
echo br();
?>
</div>
<?php 
if($i%2==0 && $i!=4){?>
</div>
<div class="row-fluid">
<?php }} ?>
</div>

<?php echo form_fieldset_close();

echo br();
echo form_fieldset("Contact Person Info");
?>
<div class="row-fluid">
<div class="span6">
<?php
//NAME SECTION.
echo form_label('<b>NAME</b>', 'NAME');
echo form_input(array('name' => 'name', 'value' => set_value("name",$info["name"]),'class' =>'span' ));
echo form_error("name","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<?php 
echo form_label('<b>POSITION</b>', 'POSITION');
echo form_input(array('name' => 'position', 'value' => set_value("position",$info["position"]),'class' =>'span' ));
echo form_error("position","<span class='text-error'>","</span>");
echo br();
?>
</div></div>


<div class="row-fluid">
<div class="span6">
<?php 

//EMAIL SECTION.
echo form_label('<b>EMAIL</b>', 'EMAIL');
echo form_input(array('name' => 'email', 'value' => set_value("email",$info["email"]),'class' =>'span' ));
echo form_error("email","<span class='text-error'>","</span>");
echo br();
?>

</div>


<div class="span6">

<label><b>Phone</b><em>*</em></label>
<label><input type="text" class="input-xlarge" name="phone" value="<?= set_value('phone',$info["phone"]); ?>" /></label>
<!--<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="phone[0]" value="<?php echo getPhoneField($info['phone'],0);?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="phone[1]" value="<?php echo getPhoneField($info['phone'],1);?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="phone[2]" value="<?php echo getPhoneField($info['phone'],2);?>" />-->
<?= form_error('phone','<span class="text-error">','</span>'); ?>
<?php 
echo br();
echo form_error("country_code","<span class='text-error'>","</span>");

echo form_error("city_code","<span class='text-error'>","</span>");

echo form_error("number","<span class='text-error'>","</span>"); ?>
</div></div>
<div class="row-fluid">
<div class="span6">
<?php
//TITLE SECTION.

echo form_label('<b>Button Text&nbsp;:</b>', 'Button Text');
echo form_input(array("id"=>"button_text","name" => "button_text".$lang, "value" => set_value("button_text".$lang,$info["button_text".$lang]),"class" =>"span"));
echo form_error("button_text","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">

<?php
//PHOTO SECTION.
echo form_label('<b>PHOTO (WIDTH:300PX,HEIGHT:228PX)<small class="blue">(Max Size '.$size_arr['size_mega'].'Mb)</small></b></b>', 'PHOTO');
echo form_upload(array("name" => "photo", "class" => "input-large"));
echo "<span >";
if($info["photo"] != ""){ 
echo '<span id="photo_'.$info["id_brands"].'">'.anchor('uploads/brands/'.$info["photo"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("brands","photo","<?php echo $info["photo"]; ?>",<?php echo $info["id_brands"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo "<div></div>";
echo form_error("photo","<span class='text-error'>","</span>"); ?></div>
</div>
<div class="row-fluid">
<div class="span6">

<?php 
//URL SECTION.
echo form_label('<b>Deliver Text<small class="blue" style="text-transform: none;">(ex: Delivered by {supplier name} )</small></b>', 'Deliver Text');
echo form_input(array('name' => 'deliver_text', 'value' => set_value("access_code",$info["deliver_text"]),'class' =>'span' ));
echo form_error("deliver_text","<span class='text-error'>","</span>");

?>


</div></div>
<?php

if(!empty($info['id_user'])){
	$supplier_name=$this->fct->getonecell('user','trading_name',array('id_user'=>$info['id_user']));}else{
	$supplier_name="supplier";}
if(isset($deliver_by_supplier)){}else{
if(isset($info['deliver_by_supplier']))
$deliver_by_supplier = $info['deliver_by_supplier'];}
?>
<label ><input type="checkbox" id="deliver_by_supplier" name="deliver_by_supplier" <?php if(isset($deliver_by_supplier) && $deliver_by_supplier == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Deliver by <span id="supplier_name"><?php echo $supplier_name;?></span></b></label>
<?php
//MOBILE SECTION.
/*echo form_label('<b>MOBILE</b>', 'MOBILE');
echo form_input(array('name' => 'mobile', 'value' => set_value("mobile",$info["mobile"]),'class' =>'span' ));
echo form_error("mobile","<span class='text-error'>","</span>");
echo br();*/


echo br();
 if(!checkIfSupplier_admin()){
echo form_fieldset("Meta Tags");
//PAGE TITLE SECTION.
?>
<div class="row-fluid">
<div class="span6">
<?php
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max '.getLimitText('page_title').' Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span","onKeyDown"=>"limitText(this.form.meta_title,0,".getLimitText('page_title').");","onKeyUp"=>"limitText(this.form.meta_title,0,".getLimitText('page_title').");" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();?>
</div>
<div class="span6">
<?php 
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<div class="row-fluid">
<div class="span6">
<?php
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max '.getLimitText('meta_description').' Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100,"onKeyDown"=>"limitText(this.form.meta_description,0,".getLimitText('meta_description').");","onKeyUp"=>"limitText(this.form.meta_description,0,".getLimitText('meta_description').");"));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<?php 
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max '.getLimitText('meta_description').' Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100,"onKeyDown"=>"limitText(this.form.meta_keywords,0,".getLimitText('meta_description').");","onKeyUp"=>"limitText(this.form.meta_keywords,0,".getLimitText('meta_description').");" ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<?php
echo form_fieldset_close();}
?>
<?php if(!checkIfSupplier_admin()){ ?>
<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Client</label>
<?php } ?>
<?php

//PROMOTE TO FRONT SECTION.

echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}

echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>