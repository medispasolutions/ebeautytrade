<?php
$lang = "";
if ($this->config->item("language_module")) {
    $lang = getFieldLanguage($this->lang->lang());
}
?>

<script type="text/javascript">
    /*$(document).ready(function(){
        
    $('#RoleChange').change(function(){
        var role = $(this).val();
        if(role == 5 || role==4) {
            $('.new-roles').slideDown('fast');
            if(role==5){
                $('.supplier').show();
                }else{
                    $('.supplier').hide();}
            }
        else {
            
            $('.new-roles').slideUp('fast');
        }
    });
        
    });*/

    $(document).ready(function () {
        $('#RoleChange').change(function () {
            var val = $(this).val();
            if (($('#trading_name').is(":visible") && $(this).prop('checked') == false) || (val != 5 && val != 4)) {
                $('#trading_name').stop(true, true).slideUp('fast');
            } else {
                $('#trading_name').stop(true, true).slideDown('fast');
            }
        });
    });
</script>
<script>
    $(function () {
        $('input[name=variable]:radio').change(function () {
            var val = $(this).val();
            if (val == "exist") {
                $('#db_names').css('display', 'block');
            } else {
                $('#db_names').css('display', 'none');
            }
        });
    });
</script>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <? $this->load->view('back_office/includes/left_box'); ?>
        </div>

        <div class="span10 cont_h">
            <div class="span10-fluid">
                <ul class="breadcrumb">
                    <li><a href="<?= site_url('back_office/users'); ?>">List Users</a> <span class="divider">/</span>
                    </li>
                    <li><?= $title; ?></li>
                    <?php if (isset($id)) { ?>

                        <?php if ($info['id_roles'] == 5) { ?>
                            <!--<li class="pull-right">
<a id="topLink" class="btn btn-info top_btn" href="<?= site_url('back_office/brands/index/' . $id); ?>" data-original-title="">Brands</a>
</li>-->
                            <li class="pull-right">
                            <a id="topLink" class="btn btn-info top_btn"
                               href="<?= site_url('back_office/supplier_info/edit/' . $id); ?>" data-original-title="">Supplier
                                Info</a>
                            </li><?php } ?>

                        <?php if ($info['id_roles'] == 4) { ?>
                            <li class="pull-right">
                                <a id="topLink" class="btn btn-info top_btn"
                                   href="<?= site_url('back_office/supplier_info/edit/' . $id); ?>"
                                   data-original-title="">
                                    <?php echo lang('store_owner'); ?> Info</a>
                            </li>

                        <?php } ?>
                        <?php if ($info['id_roles'] != 7) { ?>
                            <li class="pull-right">
                                <a id="topLink" class="btn btn-info top_btn"
                                   href="<?= site_url('back_office/user_drop_file/edit/' . $id); ?>"
                                   data-original-title="">Drop Files</a>
                            </li>
                        <?php } ?>
                    <?php } ?>

                </ul>
            </div>
            <div class="hundred pull-left">
                <form action="<?= site_url('back_office/users/submit'); ?>" method="post" class="middle-forms"
                      enctype="multipart/form-data">
                    <p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em>
                        <?
                        $countries = $this->fct->getAll('countries', 'title');
                        if (isset($id)) {
                            echo '<input type="hidden" name="id" value="' . $id . '" />';
                        } else {

                            $info['name'] = "";
                            $info['email'] = "";
                            $info['id_account_manager'] = "";
                            $info['address'] = "";
                            $info['username'] = "";
                            $info['password'] = "";
                            $info['id_roles'] = "";
                            $info['status'] = "";
                            $info['license_number'] = "";
                            $info['id_discount_groups'] = "";
                            $info['id_users_areas'] = "";
                            $info['company_name'] = "";
                            $info['id_countries'] = "";
                            $info['state'] = "";

                            $info['comments'] = "";
                            $info['city'] = "";
                            $info['trading_name'] = "";
                            $info['trading_licence'] = "";
                            $info['address_1'] = "";
                            $info['address_2'] = "";
                            $info['salutation'] = "";
                            $info['banner_image'] = "";
                            if (!isset($info['mobile'])) {
                                $info['mobile'] = "";
                            }
                            if (!isset($info['fax'])) {
                                $info['fax'] = "";
                            }

                            if (!isset($info['phone'])) {
                                $info['phone'] = "";
                            }


                            $info['available_balance'] = "";
                            $info['type'] = "";
                            $info['miles'] = "";
                            $info['position'] = "";
                            $info['first_name'] = "";
                            $info['last_name'] = "";
                            $info['gender'] = "";
                            $info['id_brands'] = "";
                            $info['guest'] = "";
                            $info['id_supplier_info'] = "";
                            $info["completed"] = 0;
                        }
                        $info['thumb'] = "";
                        $info['thumb'] = "";
                        ?>

                        <? if ($this->session->userdata('success_message')){ ?>
                    <div class="alert alert-success">
                        <?= $this->session->userdata('success_message'); ?>
                    </div>
                    <? } ?>
                    <? if ($this->session->userdata('error_message')) { ?>
                        <div class="alert alert-error">
                            <?= $this->session->userdata('error_message'); ?>
                        </div>
                    <? } ?>
                    </p>
                    <fieldset>


                        <?php if (isset($id)) {
                            ; ?>

                            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <?php } ?>
                        <!--<label class="field-title">Full Name <em>*</em>:</label> <label>
<input type="text" class="input-xxlarge" name="name" value="<? //= set_value('name',$info["name"]); ?>" />
<? //= form_error('name','<span class="text-error">','</span>'); ?>
</label>-->
                        <?php if (!empty($info['type'])) { ?>
                            <label class="field-title"><b>Account Type </b> : <?php echo $info['type']; ?></label>
                        <?php } ?>

                        <!--<label class="field-title">Name <em>*</em>:</label> <label>
<input type="text" class="input-xxlarge" name="name" value="<?= set_value('name', $info["name"]); ?>" />
<?= form_error('name', '<span class="text-error">', '</span>'); ?>
</label>-->
                        <input type="hidden" class="input-xlarge" name="account_type"
                               value="<?= set_value('account_type', $info["type"]); ?>"/>
                        <!--<label class="field-title">Username<em>*</em>:</label>
<label><input type="text" class="input-xlarge" name="username" value="<?= set_value('username', $info["username"]); ?>" />
<?= form_error('username', '<span class="text-error">', '</span>'); ?>
</label>-->


                        <?php echo form_fieldset("Personal Information"); ?>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">First Name<em>*</em>:</label>
                                <label><input type="text" class="input-xlarge" name="first_name"
                                              value="<?= set_value('first_name', $info["first_name"]); ?>"/>
                                    <?= form_error('first_name', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Last Name<em>*</em>:</label>
                                <label><input type="text" class="input-xlarge" name="last_name"
                                              value="<?= set_value('last_name', $info["last_name"]); ?>"/>
                                    <?= form_error('last_name', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label>Salutation: (DISABLED, TO BE DELETED)</label>
                                <label>
                                    <select name="salutation" class="input-xxlarge" disabled>
                                        <option value="">-Select Salutation-</option>
                                        <option value="Mr" <? if (set_value('salutation', $info["salutation"]) == 'Mr') echo 'selected="selected"'; ?> >
                                            Mr
                                        </option>
                                        <option value="Mrs" <? if (set_value('salutation', $info["salutation"]) == 'Mrs') echo 'selected="selected"'; ?> >
                                            Mrs
                                        </option>
                                        <option value="Ms" <? if (set_value('salutation', $info["salutation"]) == 'Ms') echo 'selected="selected"'; ?> >
                                            Ms
                                        </option>
                                        <option value="Miss" <? if (set_value('salutation', $info["salutation"]) == 'Miss') echo 'selected="selected"'; ?> >
                                            Miss
                                        </option
                                        >
                                    </select>
                                    <?= form_error('salutation', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Position: </label> <label>
                                    <input type="text" class="input-xxlarge" name="position"
                                           value="<?= set_value('position', $info["position"]); ?>"/>
                                    <?= form_error('position', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Email<em>*</em>:</label>
                                <label><input type="text" class="input-xlarge" name="email"
                                              value="<?= set_value('email', $info["email"]); ?>"/>
                                    <?= form_error('email', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>

                            <div class="span5">
                                <label class="field-title">License Number:</label>
                                <label><input type="text" class="input-xlarge" name="license_number"
                                              value="<?= set_value('license_number', $info["license_number"]); ?>"/>
                                    <?= form_error('license_number', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <!--
                            <div class="span5">
                                <label class="field-title">Username:</label>
                                <label><input type="text" class="input-xlarge" name="username" value="<?= set_value('username', $info["username"]); ?>" />
                                <?= form_error('username', '<span class="text-error">', '</span>'); ?>
                                </label>
                                </div>
                            -->
                        </div>
                        <?php if($info["id_user_type"]){ ?>
                            <?php if($info["id_user_type"] == "2"){ ?>
                            <div class="row-fluid">
                                <div class="span5">
                                    <label class="field-title">User Account Type:</label>
                                    <label>
                                        Free Member
                                    </label>
                                </div>
                            </div>
                            <?php } else if($info["id_user_type"] == "3") { ?>
                            <div class="row-fluid">
                                <div class="span5">
                                    <label class="field-title">User Account Type:</label>
                                    <label>
                                        Gold Member
                                    </label>
                                </div>
                                <div class="span5">
                                    <label class="field-title">Expiry Date:</label>
                                    <label>
                                         <?php echo $info["expire_date"]; ?>
                                    </label>
                                </div>
                            </div>
                            <?php } else { ?>
                            
                            <?php } ?>
                        <?php } ?>
                        <!--<div class="row-fluid">
                        <label><input type="checkbox" name="is_subscribed" value="1" style="margin-top:0" />&nbsp;&nbsp;Sign Up for Newsletter</label>
                        </div>-->
                        <?php echo form_fieldset_close(); ?>
                        <?php echo form_fieldset("Address"); ?>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Address 1<em>*</em>:</label>
                                <label><textarea type="text" class="input-xlarge" name="address_1"
                                                 style="height: 50px"><?= set_value('address_1', $info["address_1"]); ?></textarea>
                                    <?= form_error('address_1', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Address 2: (DISABLED, TO BE DELETED)</label>
                                <label><input disabled type="text" class="input-xlarge" name="address_2"
                                              value="<?= set_value('address_2', $info["address_2"]); ?>"/>
                                    <?= form_error('address_2', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">City:</label>
                                <label><input type="text" class="input-xlarge" name="city"
                                              value="<?= set_value('city', $info["city"]); ?>"/>
                                    <?= form_error('city', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Phone<em>*</em>:</label>
                                <label><input type="text" class="input-xlarge" name="phone"
                                              value="<?= set_value('phone', $info["phone"]); ?>"/>
                                    <label>
                                        <!--<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="phone[0]" value="<?php echo getPhoneField($info['phone'], 0); ?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="phone[1]" value="<?php echo getPhoneField($info['phone'], 1); ?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="phone[2]" value="<?php echo getPhoneField($info['phone'], 2); ?>" />-->
                                        <?= form_error('phone', '<span class="text-error">', '</span>'); ?>
                                    </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Fax:</label>
                                <label><input type="text" class="input-xlarge" name="fax"
                                              value="<?= set_value('fax', $info["fax"]); ?>"/>
                                    <!--<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="fax[0]" value="<?php echo getPhoneField($info['fax'], 0); ?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="fax[1]" value="<?php echo getPhoneField($info['fax'], 1); ?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="fax[2]" value="<?php echo getPhoneField($info['fax'], 2); ?>" />-->
                                    <?= form_error('fax', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Mobile:</label>

                                <label><input type="text" class="input-xlarge" name="mobile"
                                              value="<?= set_value('mobile', $info["mobile"]); ?>"/>
                                    <!--<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="mobile[0]" value="<?php echo getPhoneField($info['mobile'], 0); ?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="mobile[1]" value="<?php echo getPhoneField($info['mobile'], 1); ?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="mobile[2]" value="<?php echo getPhoneField($info['mobile'], 2); ?>" />-->
                                    <?= form_error('mobile', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Country<em>*</em>:</label>
                                <label>
                                    <select name="id_countries" class="input-xxlarge">
                                        <option value="">-Select Country-</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?php echo $country['id_countries']; ?>" <? if (set_value('id_countries', $info["id_countries"]) == $country['id_countries']) echo 'selected="selected"'; ?> ><?php echo $country['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?= form_error('id_countries', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>


                        </div>


                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Account Manager:</label>
                                <label>
                                    <?php $account_manager = $this->fct->getAll('account_manager', 'title'); ?>
                                    <select name="id_account_manager" class="input-xxlarge">
                                        <option value="">-Select Account Manager-</option>
                                        <?php foreach ($account_manager as $val) { ?>
                                            <option value="<?php echo $val['id_account_manager']; ?>" <? if (set_value('id_account_manager', $info["id_account_manager"]) == $val['id_account_manager']) echo 'selected="selected"'; ?> ><?php echo $val['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?= form_error('id_account_manager', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>

                            <div class="span5">
                                <?php $areas = $this->fct->getAll('users_areas', 'title'); ?>
                                <label class="field-title">Areas: </label>
                                <label>
                                    <select name="id_users_areas" class="input-xxlarge">
                                        <option value="">-Select Area-</option>
                                        <?php foreach ($areas as $val) { ?>
                                            <option value="<?php echo $val['id_users_areas']; ?>" <? if (set_value('id_users_areas', $info["id_users_areas"]) == $val['id_users_areas']) echo 'selected="selected"'; ?> ><?php echo $val['title']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?= form_error('id_users_areas', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>

                        </div>

                        <?php echo form_fieldset_close(); ?>

                        <?php

                        echo br();

                        echo form_fieldset("Password"); ?>
                        <div class="row-fluid">
                            <div class="span5">
                                <input type="hidden" name="guest" value="<?php echo $info['guest']; ?>"/>
                                <label class="field-title">Password <?php if (!isset($id)) { ?><em>*</em>:<?php } ?>
                                </label>
                                <label><input type="password" class="input-xlarge" name="password"
                                              value="<?= set_value('password'); ?>"/>
                                    <?= form_error('password', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Confirm Password<?php if (!isset($id)) { ?>
                                        <em>*</em>:<?php } ?></label>
                                <label><input type="password" class="input-xlarge" name="confirm_password"
                                              value="<?= set_value('confirm_password'); ?>"/>
                                    <?= form_error('confirm_password', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <?php echo form_fieldset_close(); ?>
                        <br/>

                        <br/>
                        <?php echo form_fieldset(" "); ?>
                        <div class="row-fluid">
                            <div class="span5">
                                <label>Level:</label>
                                <label>
                                    <select name="roles" id="RoleChange" class="input-xxlarge">
                                        <option value=""> - Select Level -</option>
                                        <? foreach ($roles as $val) {
                                            if (($val["id_roles"] == 3) && $this->session->userdata('level') == 0) { // Do nothing 
                                            } elseif ($val["id_roles"] == 3 && $this->session->userdata('level') == 2) { // Do nothing 
                                            } else {
                                                ?>
                                                <option value="<?= $val["id_roles"]; ?>" <? if (set_value('roles', $info["id_roles"]) == $val["id_roles"]) echo 'selected="selected"'; ?> ><?php echo lang($val['title']); ?></option>
                                            <? }
                                        } ?>
                                    </select>
                                    <?= form_error('roles', '<span class="text-error">', '</span>'); ?>
                                </label>
                                <div class="row-fluid"
                                     id="trading_name" <?php if ($info['id_roles'] != 4 && $info['id_roles'] != 5) echo 'style="display:none;"'; ?>>

                                    <label class="field-title">Trade Name<em>*</em>:</label>
                                    <label><input type="text" class="input-xlarge" name="trading_name" id="trading_name"
                                                  value="<?= set_value('trading_name', $info["trading_name"]); ?>"/>
                                        <?= form_error('trading_name', '<span class="text-error">', '</span>'); ?>
                                    </label>

                                </div>
                            </div>
                            <div class="span5">
                                <label>Status:</label>
                                <label>
                                    <select name="status" class="input-xxlarge">
                                        <option value="0" <? if (set_value('status', $info["status"]) == 0) echo 'selected="selected"'; ?> >
                                            Blocked
                                        </option>
                                        <option value="1" <? if (set_value('status', $info["status"]) == 1) echo 'selected="selected"'; ?> >
                                            Active
                                        </option>
                                        <option value="2" <? if (set_value('status', $info["status"]) == 2) echo 'selected="selected"'; ?> >
                                            Under Review
                                        </option>
                                        <option value="3" <? if (set_value('status', $info["status"]) == 3) echo 'selected="selected"'; ?> >
                                            Incomplete
                                        </option>
                                    </select>
                                    <?= form_error('status', '<span class="text-error">', '</span>'); ?>
                                </label>

                                <label class="field-title">Fulfilled by:</label>
                                <?php if($info["fulfilled_by"] != ""){ ?>
                                <label>
                                    <input type="text" class="input-xlarge" name="fulfilled_by"
                                              value="<?= set_value('fulfilled_by', $info["fulfilled_by"]); ?>"/>
                                </label>
                                <?php } else { ?>
                                <label>
                                    <input type="text" class="input-xlarge" name="fulfilled_by"
                                              value="">
                                </label>
                                <?php } ?>

                                <?php
                                if (isset($info['featured_supplier']) && !isset($featured_supplier))
                                    $featured_supplier = $info['featured_supplier'];
                                ?>
        
                                <label>
                                    <input type="checkbox" name="featured_supplier" <?php if (isset($featured_supplier) && $featured_supplier == 1) { ?> checked="checked"<?php } ?> value="1" style="margin:0"/>&nbsp;&nbsp;Set as Menu Featured
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label>Completed:</label>
                                <label>
                                    <select name="completed" class="input-xxlarge">
                                        <option value="1" <? if (set_value('completed', $info["completed"]) == 1) echo 'selected="selected"'; ?> >
                                            Completed
                                        </option>
                                        <option value="0" <? if (set_value('completed', $info["completed"]) == 0) echo 'selected="selected"'; ?> >
                                            UnCompleted
                                        </option>

                                    </select>
                                    <?= form_error('completed', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>

                        </div>


                        <!--<div class="row-fluid">

<div class="span5">
<label class="field-title">Miles:</label>
<label><input type="text" class="input-xlarge" name="miles" value="<?= set_value('miles', $info["miles"]); ?>" />
<?= form_error('miles', '<span class="text-error">', '</span>'); ?>
</label>
</div>

<div class="span5">
<label class="field-title">Avalable Balance(<?php echo $this->config->item('default_currency'); ?>):</label>
<label><input type="text" class="input-xlarge" name="available_balance" value="<?= set_value('available_balance', $info["available_balance"]); ?>" />
<?= form_error('available_balance', '<span class="text-error">', '</span>'); ?>
</label>
</div>


</div>-->

                        <?php if (isset($id)) { ?>
                            <label><input type="checkbox" name="inform_client" value="1" style="margin-top:0"/>&nbsp;&nbsp;Inform
                                Client</label>
                            <?php /*?><?php }else{ ?><?php */ ?><?php } ?>
                        <?php
                        if (isset($info['pre_registration']) && !isset($pre_registration))
                            $pre_registration = $info['pre_registration'];
                        ?>

                        <label><input type="checkbox"
                                      name="pre_registration" <?php if (isset($pre_registration) && $pre_registration == 1) { ?> checked="checked"<?php } ?>
                                      value="1" style="margin:0"/>&nbsp;&nbsp;Set as pre-registration</label>


                    </fieldset>
                    <p class="pull-right">
                        <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
                        <input type="submit" name="submit" value="Save and Continue" class="btn btn-primary"/>
                    </p>
                </form>
            </div>


        </div><!-- end of div#mid-col -->

        <!-- end of div#right-col -->
        <span class="clearFix">&nbsp;</span>
    </div>
    <? $this->session->unset_userdata('success_message'); ?>
    <? $this->session->unset_userdata('error_message'); ?>
    <?php $size_arr = getMaxSize('file'); ?>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <? $this->load->view("back_office/includes/left_box"); ?>
            </div>
            <div class="span10">
                <div class="span10-fluid">
                    <?
                    if (isset($breadcrumbs)) {
                        echo $breadcrumbs;
                    } else {
                        if(!empty($info['id_user'])){
                        $ul = array(anchor('back_office/users', '<b>List Users</b>') . '<span class="divider">/</span>',
                            anchor('back_office/users/edit/' . $info['id_user'], '<b>Edit User(' . $info['name'] . ')</b>') . '<span class="divider">/</span>',
                            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
                        );
                        } else {
                        $ul = array(anchor('back_office/users', '<b>List Users</b>') . '<span class="divider">/</span>',
                            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
                        );
                        }
                        
                        if ($this->config->item("language_module") && isset($id)) {
                            $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="' . site_url('back_office/translation/section/supplier_info/' . $id) . '" class="btn btn-info top_btn">Translate</a>');
                        }
                        $ul_attributes = array(
                            'class' => 'breadcrumb'
                        );
                        echo ul($ul, $ul_attributes);
                    }
                    ?>
                </div>
                <div class="hundred pull-left">
                    <?
                    $lang = "";
                    if ($this->config->item("language_module")) {
                        $lang = getFieldLanguage($this->lang->lang());
                    }
                    $attributes = array('class' => 'middle-forms');
                    echo form_open_multipart('back_office/supplier_info/submit', $attributes);
                    echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
                    if (isset($id)) {

                        echo form_hidden('id', $id);
                    } else {
                        $info["name"] = "";
                        $info["mobile"] = "";
                        $info["image"] = "";
                        $info["logo"] = "";
                        $info["video"] = "";
                        $info["position"] = "";
                        $info["banner_image"] = "";
                        $info["set_as_featured"] = "";
                        $info["website"] = "";
                        $info["title" . $lang] = "";
                        $info["meta_title" . $lang] = "";
                        $info["meta_description" . $lang] = "";
                        $info["meta_keywords" . $lang] = "";
                        $info["title_url" . $lang] = "";
                    }
                    if ($this->session->userdata("success_message")) {
                        echo '<div class="alert alert-success">';
                        echo $this->session->userdata("success_message");
                        echo '</div>';
                    }
                    if ($this->session->userdata("error_message")) {
                        echo '<div class="alert alert-error">';
                        echo $this->session->userdata("error_message");
                        echo '</div>';
                    }

                    echo form_fieldset("");
                    if ($this->config->item("language_module")) {
                        $data['info'] = $info;
                        $this->load->view('back_office/translation/add_view_language', $data);
                    }

                    ?>

                    <?php echo form_fieldset(" "); ?>
                    <!--
<div class="row-fluid">

<div class="span5">
<label class="field-title"><strong>Miles :</strong> <?php echo $info['miles']; ?></label>
</div>
</div>
<div class="row-fluid">
<div class="span5">
<label class="field-title"><strong>Available Balance :</strong> <?php echo $info['available_balance']; ?> <?php echo $this->config->item('default_currency'); ?></label>
</div>
</div>
-->
                    <div class="row-fluid">
                        <?php if(!empty($info['id_user'])){ ?>
                        <div class="span5">
                            <input type="hidden" name="id_user" value="<?= set_value('id_user', $info["id_user"]); ?>"/>
                            <label class="field-title">Trading Name<em>*</em>:</label>
                            <label><input type="text" class="input-xlarge" name="trading_name"
                                          value="<?= set_value('trading_name', $info["trading_name"]); ?>"/>
                                <?= form_error('trading_name', '<span class="text-error">', '</span>'); ?>
                            </label>
                        </div>
                        <?php } ?>
                        <div class="span5">

                            <label class="field-title">Trading License:</label>
                            <div class="form-item input_fields_wrap2">
                                <?php
                                if ($info['trading_licence']): ?>
                                    <a href="/uploads/user/<?= $info["trading_licence"]; ?>" target="_blank">show
                                        file</a>
                                    <br>
                                <?php
                                endif;
                                ?>
                                Upload file: <input class="textbox input-file" type="file" name="trading_licence"
                                                    id="trade">
                            </div>
                            <span id="trading_licence_f-error" class="form-error"></span>

                        </div>
                        <br/>
                    </div>
                    <div class="row-fluid">

                        <div class="span5">

                            <label class="field-title">Website: (DISABLED, TO BE DELETED)</label>
                            <label><input disabled type="text" class="input-xlarge" name="website"
                                          value="<?= set_value('website', $info["website"]); ?>"/>
                                <?= form_error('website', '<span class="text-error">', '</span>'); ?>
                            </label>
                        </div>

                        <div class="span5">
                            <label class="field-title">Business Type<em>*</em>:</label>
                            <label>
                                <?php $business_type = $this->fct->getAll("business_type", "title"); ?>
                                <select name="business_type" class="input-xxlarge">
                                    <option value="">-Select Business Type-</option>
                                    <?php foreach ($business_type as $val) { ?>
                                        <option <?php if ($val['id_business_type'] == $info['business_type']) {
                                            echo "selected";
                                        } ?> value="<?php echo $val['id_business_type']; ?>"><?php echo $val['title']; ?></option>
                                    <?php } ?>
                                </select>
                                <?= form_error('business_type', '<span class="text-error">', '</span>'); ?>
                            </label>
                        </div>

                        <!--<div class="span5">
<label class="field-title">How Diy You Hear About Us?<em>*</em>:</label>
<label><?php $hear_about_us = $this->fct->getAll("how_did_you_hear_about_us", "title"); ?>
          <select name ="hear_about_us" class="input-xxlarge">
           <option value="">-Select-</option>
          <?php foreach ($hear_about_us as $val) { ?>
          <option <?php if ($val['id_how_did_you_hear_about_us'] == $info['hear_about_us']) {
                            echo "selected";
                        } ?> value="<?php echo $val['id_how_did_you_hear_about_us']; ?>"><?php echo $val['title']; ?></option>
          <?php } ?>
          </select>
<?= form_error('hear_about_us', '<span class="text-error">', '</span>'); ?>
</label>
</div>-->

                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <?php
                            //LOGO SECTION.
                            echo form_label('<b>LOGO <small class="blue">(Max Size ' . $size_arr['size_mega'] . 'Mb)</small></b>', 'LOGO');
                            echo form_upload(array("name" => "logo", "class" => "input-large"));
                            echo "<span >";
                            if ($info["logo"] != "") {
                                echo '<span id="logo_' . $info["id_user"] . '">' . anchor('uploads/user/' . $info["logo"], 'show image', array("class" => 'blue gallery'));
                                echo nbs(3); ?>
                                <a class="cur"
                                   onclick='removeFile("users","logo","<?php echo $info["logo"]; ?>",<?php echo $info["id_user"]; ?>)'><img
                                            src="<?php echo base_url() ?>images/delete.png"/></a></span>
                                <?php
                            } else {
                                echo "<span class='blue'>No Image Available</span>";
                            }
                            echo "</span>";
                            echo "<div></div>";
                            echo form_error("logo", "<span class='text-error'>", "</span>");

                            ?>
                            <?php echo br(); ?>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <?php if ($info['id_roles'] == 20) { ?>
                            <div class="span5 ">
                                <label class="field-title">Product/service Categories<em>*</em>:</label>
                                <label>
                                    <?

                                    $categories = $this->fct->getAll_cond('categories', 'sort_order', array('id_parent' => 0));

                                    $selected_categories = array();
                                    if (isset($info['id_user'])) {
                                        $selected_categories = $this->users_categories_m->select_user_categories($info['id_user']);
                                    }

                                    if (isset($val_categories)) {
                                        $selected_categories = $val_categories;
                                    }
                                    ?>
                                    <select multiple="multiple" style="height:200px" name="categories[]"
                                            class="input-xxlarge">
                                        <?
                                        foreach ($categories as $valll) {
                                            $cl = '';

                                            if (in_array($valll['id_categories'], $selected_categories)) $cl = 'selected="selected"';
                                            ?>
                                            <?php /*?><?php echo $this->custom_fct->printsubcategories($valll,$selected_categories,0); ?><?php */
                                            ?>
                                            <option value="<?php echo $valll['id_categories']; ?>" <?php echo $cl; ?>><?php echo $valll['title']; ?></option>
                                            <?
                                        } ?>
                                    </select>
                                    <input type="hidden" name="validate_categories" value="1"/>
                                    <!--<a href="#myModal" onclick="popup_categories();" data-toggle="modal" >[add more]</a>-->

                                </label>
                            </div>
                        <?php } ?>

                        <div class="span5">
                            <label class="field-title">Comments</label>
                            <label>
                                <textarea class="span" rows="3" cols="100"
                                          name="comments"><?php echo $info['comments']; ?></textarea>
                            </label>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span5 ">
                            <?php


                            if (isset($info['display_in_menu']))
                                $display_in_menu = $info['display_in_menu'];
                            ?>
                            <label><input type="checkbox" id="display_in_menu"
                                          name="display_in_menu" <?php if (isset($display_in_menu) && $display_in_menu == 1) { ?> checked="checked"<?php } ?>
                                          value="1" style="margin:0"/> <b>Display in menu </b></label>

                            <?php echo br(); ?>

                        </div>
                    </div>
                    
                    <fieldset>
                        <legend>Terms</legend>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="span4">
                                    <label class="field-title">DELIVERY TIME:</label>
                                    <label>
                                        <select name="delivery_time" class="input-xxlarge">
                                            <option value="">-Select-</option>
                                            <option <?php if ("1-3-days" == $info['delivery_time']) {
                                                echo "selected";
                                            } ?> value="1-3-days">1-3 days
                                            </option>
                                            <option <?php if ("3-5-weeks" == $info['delivery_time']) {
                                                echo "selected";
                                            } ?> value="3-5-weeks">3-5 weeks
                                            </option>
                                            <option <?php if ("other" == $info['delivery_time']) {
                                                echo "selected";
                                            } ?> value="other">other..
                                            </option>
                                        </select>

                                    </label>
                                </div>
                                <div class="span3">
                                    <label class="field-title">DELIVERY OTHER DURATION:</label>
                                    <label>
                                        <?php if(!empty($info['id_user'])){ ?>
                                        <input type="text" class="input-large" name="delivery_time_period"
                                               value="<?= set_value('delivery_time_period', $info["delivery_time_period"]); ?>">
                                        <?php } else { ?>
                                        <input type="text" class="input-large" name="delivery_time_period"
                                               value="">
                                        <?php } ?>
                                    </label>
                                </div>
                                <div class="span3">
                                    <label class="field-title">DELIVERY OTHER DURATION TYPE:</label>
                                    <label>
                                        <select name="delivery_time_period_type" class="input-xxlarge">
                                            <option value="">N/A</option>
                                            <option <?php if ("days" == $info['delivery_time_period_type']) {
                                                echo "selected";
                                            } ?> value="days">days
                                            </option>
                                            <option <?php if ("weeks" == $info['delivery_time_period_type']) {
                                                echo "selected";
                                            } ?> value="weeks">weeks
                                            </option>
                                        </select>

                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">DELIVERY FEE: </label>
                                <label>
                                    <select name="delivery_fee" class="input-xxlarge">
                                        <option value="">-Select-</option>
                                        <option <?php if ("free" == $info['delivery_fee']) {
                                            echo "selected";
                                        } ?> value="free">free
                                        </option>
                                        <option <?php if ("courier-price" == $info['delivery_fee']) {
                                            echo "selected";
                                        } ?> value="courier-price">as per the courier rate
                                        </option>
                                        <option <?php if ("free-on-bigger-orders" == $info['delivery_fee']) {
                                            echo "selected";
                                        } ?> value="free-on-bigger-orders">free on orders &gt;:
                                        </option>
                                        <option <?php if ("fixed-fee" == $info['delivery_fee']) {
                                            echo "selected";
                                        } ?> value="fixed-fee">fixed fee:
                                        </option>
                                    </select>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">DELIVERY FEE PRICE OR AMOUNT OF ORDER FOR FREE: </label>
                                <label>
                                    <?php if(!empty($info['id_user'])){ ?>
                                        <input type="text" class="input-xlarge" name="delivery_fee_price"
                                                  value="<?= set_value('delivery_fee_price', $info["delivery_fee_price"]); ?>">
                                    <?php } else { ?>
                                        <input type="text" class="input-xlarge" name="delivery_fee_price"
                                              value="">
                                    <?php } ?>
                                </label>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">MIN. ORDER: (DISABLED, TO BE DELETED)</label>
                                <label>
                                    <?php if(!empty($info['id_user'])){ ?>
                                        <input disabled type="text" class="input-xlarge" name="min_order"
                                              value="<?= set_value('min_order', $info["min_order"]); ?>">
                                    <?php } else { ?>
                                        <input disabled type="text" class="input-xlarge" name="min_order"
                                              value="">
                                    <?php } ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">CREDIT TERMS: </label>

                                <label>
                                    <select name="credit_terms" class="input-xxlarge">
                                        <option value="">-Select-</option>
                                        <option <?php if ("consult-with-supplier" == $info['credit_terms']) {
                                            echo "selected";
                                        } ?> value="consult-with-supplier">Consult with Supplier
                                        </option>
                                        <option <?php if ("do-not-publish" == $info['credit_terms']) {
                                            echo "selected";
                                        } ?> value="do-not-publish">Do not publish
                                        </option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    
                    <?php echo form_fieldset_close(); ?>
                    <?php
                    echo br();
                    if ($this->uri->segment(3) != "view") {
                        echo '<p class="pull-right">';
                        echo form_submit(array('name' => 'submit', 'value' => 'Save Changes', 'class' => 'btn btn-primary'));
                        echo '</p>';
                    }
                    echo form_fieldset_close();
                    echo form_close();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?
    $this->session->unset_userdata("success_message");
    $this->session->unset_userdata("error_message");
    ?>
    
    <script>
        $(document).ready(function (e) {

            var baseurl = $('#baseurl').val();

            var max_fields = 50; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap2"); //Fields wrapper
            var add_button = $(".add_field_button2"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function (e) {
                var cc = $('.remove_d').length;
                var rand = Math.floor((Math.random() * 1000) + 1) + "a";//on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><input  type="hidden" name="trading_license[]" value="' + rand + '" ><input class="textbox input-file" type="file" name="trading_license_' + rand + '"  id="trade_' + rand + '"><a class="remove_field remove_d" id="remove_' + rand + '" alt="Delete" tiltle="Delete">Remove</a></div>'); //add input box
                }

                if (cc > 4) {
                    add_button.hide();
                }
            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                var id = $(this).attr('id').replace('remove_', '');
                var cc = $('.remove_d').length;
                $('#trade_' + id).parent('div').remove();
                x--;
                if (cc > 4) {
                } else {
                    add_button.show();
                }
            })
        });
    </script>