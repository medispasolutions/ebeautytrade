
<script type="text/javascript">

function SelectedCategory(id,title)
{
	var url="<?php echo site_url('back_office/training_categories/edit/')?>/"+id ;
if($('#selelect_btn_'+id).length>0){

$('#selelect_btn_'+id).remove();
$('#multi_select_categories option[value="'+id+'"]').attr('selected',false);

}else{
	
var btn_select="<div class='btn btn_select' id='selelect_btn_"+id+"'><a href='"+url+"' >"+title+"</a><span class='notification red remove_selected' onclick='SelectedCategory("+id+")'>x</span></div>";	
$('.select_categories').append(btn_select);

	}
}
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/events/'.$this->session->userdata("back_link"),'<b>List events</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/events/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/events/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["countries"] = "";
$info["description"] = "";
$info["website"] = "";
$info["set_as_featured"] = "";
$info["image"] = "";
$info["date"] = "";
$info["date_label"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Event Name&nbsp;<em>*</em>:</b>', 'Event Name');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();

?>


<div class="row-fluid">
<div class="span6">
<label class="field-title"><strong>Countries<em>*</em></strong>:</label>
<label>
<? 

$countries =$this->fct->getAll('countries','sort_order');	

$selected_countries = array();
if(isset($info['id_events'])) {


$selected_countries = $this->custom_fct->select_event_countries($info['id_events']);

}


if(isset($val_countries)) {
 $selected_countries = $val_countries;
}
?>
<select multiple="multiple" style="height:200px" id="multi_select_categories" name="countries[]" class="input-xxlarge">
<? 
foreach($countries as $valll){ 
$cl = '';
if(in_array($valll['id_countries'],$selected_countries)) $cl = 'selected="selected"';
?>
<?php /*?><?php echo $this->custom_fct->printsubcountries($valll,$selected_countries,0); ?><?php */?>
<option  value="<?php echo $valll['id_countries'] ;?>" <?php echo $cl  ;?>><?php echo $valll['title'];?></option>
<? 
} ?>
</select>
<label></label>
<input type="hidden" name="validate_countries" value="1" />
<!--<a href="#myModal" onclick="popup_countries();" data-toggle="modal" >[add more]</a>-->
<?php echo form_error("countries","<span class='text-error'>","</span>");?> 
</label></div>
<div class="span6">

<?php echo br(); ?>
<div class="select_categories">
<?php if(isset($selected_countries) && !empty($selected_countries)) { ?>

<?php foreach($selected_countries as $cat_id){
	$category_selected=$this->fct->getonerecord('countries',array('id_countries'=>$cat_id));
	echo "<div class='btn btn_select' id='selelect_btn_".$cat_id."'><a>".$category_selected['title']."</a><span class='notification red remove_selected'
	 onclick='SelectedCategory(".$cat_id.")' >x</span></div>";
	?>
	<?php } ?>
	<?php } ?>
   
</div>
</div></div>


<?php
//DATE SECTION.
echo form_label('<b>DATE<em>*</em></b>', 'DATE');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'date', 'value' => set_value("date",$this->fct->date_out_formate($info["date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("date","<span class='text-error'>","</span>");

//DATE LABEL  SECTION.
echo form_label('<b>Date(label)<em>*</em>:</b>', 'DATE(LABEL)');
echo form_input(array("name" => "date_label".$lang, "value" => set_value("date_label".$lang,$info["date_label".$lang]),"class" =>"span" ));
echo form_error("date_label".$lang,"<span class='text-error'>","</span>");
echo br();

//IMAGE SECTION.
echo form_label('<b>IMAGE</b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_events"].'">'.anchor('uploads/events/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("events","image","<?php echo $info["image"]; ?>",<?php echo $info["id_events"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
echo br();


//META DESCRIPTION SECTION.
echo form_label('<b>DESCRIPTION&nbsp;<small class="blue">(Max 300 Characters)</small>:</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("description".$lang,"<span class='text-error'>","</span>");
echo br();
//WEBSITE SECTION.
echo form_label('<b>WEBSITE</b>', 'WEBSITE');
echo form_input(array('name' => 'website', 'value' => set_value("website",$info["website"]),'class' =>'span' ));
echo form_error("website","<span class='text-error'>","</span>");
echo br();
//SET AS FEATURED SECTION.
echo form_label('<b>SET AS FEATURED</b>', 'SET AS FEATURED');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("set_as_featured", $options,set_value("set_as_featured",$info["set_as_featured"]), 'class="span"');
echo form_error("set_as_featured","<span class='text-error'>","</span>");
echo br();

echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>