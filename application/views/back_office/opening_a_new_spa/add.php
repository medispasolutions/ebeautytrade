
<script type="text/javascript">
$(document).ready(function(e) {

	});
function SelectedCategory(id,title)
{

var url="<?php echo site_url('back_office/projects_type/edit')?>/"+id ;

if($('.projects #selelect_btn_'+id).length>0){
$('.projects #selelect_btn_'+id).remove();

$('#multi_select_projects option[value="'+id+'"]').attr('selected',false);
}else{
var btn_selet="<div class='btn btn_select' id='selelect_btn_"+id+"'><a href='"+url+"' >"+title+"</a><span class='notification red remove_selected' onclick='SelectedCategory("+id+")'>x</span></div>";	
$('.projects .select_categories').append(btn_selet);
	}
}


function SelectedCategory2(id,title)
{

var url="<?php echo site_url('back_office/services_enquiry/edit')?>/"+id ;

if($('.services #selelect_btn_'+id).length>0){
$('.services #selelect_btn_'+id).remove();

$('#multi_select_services option[value="'+id+'"]').attr('selected',false);
}else{
var btn_selet="<div class='btn btn_select' id='selelect_btn_"+id+"'><a href='"+url+"' >"+title+"</a><span class='notification red remove_selected' onclick='SelectedCategory2("+id+")'>x</span></div>";	
$('.services .select_categories').append(btn_selet);
	}
}
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/opening_a_new_spa/'.$this->session->userdata("back_link"),'<b>List opening a new spa</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/opening_a_new_spa/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/opening_a_new_spa/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["name"] = "";
$info["first_name"] = "";
$info["last_name"] = "";
$info["position"] = "";
$info["email"] = "";
$info["mobile"] = "";
$info["phone"] = "";
$info["trade_name"] = "";
$info["website"] = "";
$info["address"] = "";
$info["city"] = "";
$info["country"] = "";
$info["company_email"] = "";
$info["company_phone"] = "";
$info["salutation"] = "";
$info["comments"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}


echo br();
?>
<?php if(!empty($info['id_consultants'])){
	$consultant_name=$this->fct->getonecell('consultants','name',array('id_consultants'=>$info['id_consultants'])); ?>
<h4>The consultant: <?php echo $consultant_name; ?></h4>
<?php } ?>
<?php echo form_fieldset("PERSONAL INFORMATION");?>
<label>
<input  type="radio" name="salutation" value="1" <?php if($info["salutation"]==1) echo "checked";?>> Mrs. 
<input   type="radio" name="salutation" value="2" <?php if($info["salutation"]==2) echo "checked";?>> Mr.</label>
<input   type="radio" name="salutation" value="3" <?php if($info["salutation"]==3) echo "checked";?>> Ms.</label>
<div class="row-fluid">
<div class="span4">
<?php 
//FIRST NAME SECTION.
echo form_label('<b>FIRST NAME</b>', 'FIRST NAME');
echo form_input(array('name' => 'first_name', 'value' => set_value("first_name",$info["first_name"]),'class' =>'span' ));
echo form_error("first_name","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//LAST NAME SECTION.
echo form_label('<b>LAST NAME</b>', 'LAST NAME');
echo form_input(array('name' => 'last_name', 'value' => set_value("last_name",$info["last_name"]),'class' =>'span' ));
echo form_error("last_name","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//POSITION SECTION.
echo form_label('<b>POSITION</b>', 'POSITION');
echo form_input(array('name' => 'position', 'value' => set_value("position",$info["position"]),'class' =>'span' ));
echo form_error("position","<span class='text-error'>","</span>");
echo br();

?>
</div>
</div>
<div class="row-fluid">
<div class="span4">
<?php
//EMAIL SECTION.
echo form_label('<b>EMAIL</b>', 'EMAIL');
echo form_input(array('name' => 'email', 'value' => set_value("email",$info["email"]),'class' =>'span' ));
echo form_error("email","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//MOBILE SECTION.
echo form_label('<b>MOBILE</b>', 'MOBILE');
echo form_input(array('name' => 'mobile', 'value' => set_value("mobile",$info["mobile"]),'class' =>'span' ));
echo form_error("mobile","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//PHONE SECTION.
echo form_label('<b>PHONE</b>', 'PHONE');
echo form_input(array('name' => 'phone', 'value' => set_value("phone",$info["phone"]),'class' =>'span' ));
echo form_error("phone","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>

<?php echo form_fieldset("COMPANY DETAILS (If available)");?>
<div class="row-fluid">
<div class="span4">
<?php
//TRADE NAME SECTION.
echo form_label('<b>TRADE NAME</b>', 'TRADE NAME');
echo form_input(array('name' => 'trade_name', 'value' => set_value("trade_name",$info["trade_name"]),'class' =>'span' ));
echo form_error("trade_name","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//WEBSITE SECTION.
echo form_label('<b>WEBSITE</b>', 'WEBSITE');
echo form_input(array('name' => 'website', 'value' => set_value("website",$info["website"]),'class' =>'span' ));
echo form_error("website","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//ADDRESS SECTION.

echo form_label('<b>COMPANY PHONE</b>', 'COMPANY PHONE');
echo form_input(array('name' => 'company_phone', 'value' => set_value("company_phone",$info["company_phone"]),'class' =>'span' ));
echo form_error("company_phone","<span class='text-error'>","</span>");
echo br();


?>
</div>
</div>
<div class="row-fluid">
<div class="span4">

<?php
//CITY SECTION.
echo form_label('<b>CITY</b>', 'CITY');
echo form_input(array('name' => 'city', 'value' => set_value("city",$info["city"]),'class' =>'span' ));
echo form_error("city","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//COUNTRY SECTION.
echo form_label('<b>COUNTRY</b>', 'COUNTRY');
$items = $this->fct->getAll("countries","sort_order"); 
echo '<select name="country'.'"  class="span">';
echo '<option value="" > - select countries - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_countries"]; ?>" <? if(isset($id)){  if($info["id_countries"] == $valll["id_countries"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("country","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span4">
<?php
//COMPANY EMAIL SECTION.
echo form_label('<b>COMPANY EMAIL</b>', 'COMPANY EMAIL');
echo form_input(array('name' => 'company_email', 'value' => set_value("company_email",$info["company_email"]),'class' =>'span' ));
echo form_error("company_email","<span class='text-error'>","</span>");
?>

</div>
</div>
<?php

//COMMENTS SECTION.
echo form_label('<b>ADDRESS&nbsp;:</b>', 'ADDRESS');
echo form_textarea(array("name" => "address".$lang, "value" => set_value("address".$lang,$info["address".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("address".$lang,"<span class='text-error'>","</span>");
echo br();
?>
<?php echo form_fieldset("PROJECT TYPE");?>
<div class="row-fluid projects">
<div class="span6">
<label>
<? 

$projects = $this->fct->getAll('projects_type','sort_order');	

$selected_options = array();
if(isset($id)) {
$selected_options = $this->fct->select_spa_projects($id);
}
if(isset($val_projects)) {
 $selected_options = $val_projects;
}
?>
<select multiple="multiple" style="height:200px" name="projects[]"  id="multi_select_projects"class="input-xxlarge">

<? 


foreach($projects as $valll){ 
$cl = '';

if(in_array($valll['id_projects_type'],$selected_options)) $cl = 'selected="selected"';
?>
<?php /*?><?php echo $this->custom_fct->printsubcategories($valll,$selected_categories,0); ?><?php */?>
<option  value="<?php echo $valll['id_projects_type'] ;?>" <?php echo $cl  ;?>><?php echo $valll['title'];?></option>
<? 
} ?>
</select>
<input type="hidden" name="validate_projects" value="1" />
<!--<a href="#myModal" onclick="popup_categories();" data-toggle="modal" >[add more]</a>-->

</label>
</div>
<div class="span6">

<?php echo br(); ?>
<div class="select_categories">
<?php if(isset($selected_options) && !empty($selected_options)) { ?>

<?php foreach($selected_options as $cat_id){
	$category_selected=$this->fct->getonerecord('projects_type',array('id_projects_type'=>$cat_id));
 ?>
	<div class='btn btn_select' id='selelect_btn_<?php echo $cat_id ;?>'><a href="<?php echo site_url('back_office/projects_type/edit/'.$cat_id);?>"><?php echo $category_selected['title'];?></a><span class='notification red remove_selected' onclick="SelectedCategory('<?php echo $cat_id;?>','<?php echo $category_selected['title'];?>')" >x</span></div>

	<?php } ?>
	<?php } ?>
   
</div>
</div>
</div>

<?php echo form_fieldset("SERVICE(S) YOU MAY ENQUIRE");?>
<div class="row-fluid services">
<div class="span6">
<label>
<? 

$services = $this->fct->getAll('services_enquiry','sort_order');	

$selected_options = array();
if(isset($id)) {
$selected_options = $this->fct->select_spa_services($id);
}
if(isset($val_services)) {
 $selected_options = $val_services;
}
?>
<select multiple="multiple" style="height:200px" name="services[]"  id="multi_select_services"class="input-xxlarge">

<? 


foreach($services as $valll){ 
$cl = '';

if(in_array($valll['id_services_enquiry'],$selected_options)) $cl = 'selected="selected"';
?>
<?php /*?><?php echo $this->custom_fct->printsubcategories($valll,$selected_categories,0); ?><?php */?>
<option  value="<?php echo $valll['id_services_enquiry'] ;?>" <?php echo $cl  ;?>><?php echo $valll['title'];?></option>
<? 
} ?>
</select>
<input type="hidden" name="validate_projects" value="1" />
<!--<a href="#myModal" onclick="popup_categories();" data-toggle="modal" >[add more]</a>-->

</label>
</div>
<div class="span6">

<?php echo br(); ?>
<div class="select_categories">

<?php if(isset($selected_options) && !empty($selected_options)) { ?>

<?php foreach($selected_options as $cat_id){
	$category_selected=$this->fct->getonerecord('services_enquiry',array('id_services_enquiry'=>$cat_id));
 ?>
	<div class='btn btn_select' id='selelect_btn_<?php echo $cat_id ;?>'><a href="<?php echo site_url('back_office/services_enquiry/edit/'.$cat_id);?>"><?php echo $category_selected['title'];?></a><span class='notification red remove_selected' onclick="SelectedCategory2('<?php echo $cat_id;?>','<?php echo $category_selected['title'];?>')" >x</span></div>

	<?php } ?>
	<?php } ?>
   
</div>
</div>
</div>
<?php $i=0;if(isset($id)) {?>
<?php $consultants = $this->fct->select_spa_cosnultants($id); ?>  
<?php if(!empty($consultants)){
	$counter=count($consultants);?>
    <br />
<div class="row-fluid">
<?php foreach($consultants as $val){$i++;?>
<div class="span4">

<?php
//CITY SECTION.
echo form_label('<b>'.$val['name'].'</b>', $val['name']);
echo $val['message'];
echo br();
?>
</div>

<?php if($i%3==0 && $i!=$counter){
?>
</div>
<div class="row-fluid">
<?php }}?>
</div>
<?php } ?>
<br />
<?php } ?>
<?php

//COMMENTS SECTION.
echo form_label('<b>COMMENTS&nbsp;:</b>', 'COMMENTS');
echo form_textarea(array("name" => "comments".$lang, "value" => set_value("comments".$lang,$info["comments".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("comments".$lang,"<span class='text-error'>","</span>");
echo br();
//COMPANY PHONE SECTION.

?>

<?php

echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>