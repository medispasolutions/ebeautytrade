<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/invoices/'.$this->session->userdata("back_link"),'<b>List invoices</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/invoices/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/invoices/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["default_currency"] = "";
$info["status"] = "";
$info["total_amount_credits"] = "";
$info["total_customer_charge"] = "";
$info["id_user"] = "";
$info["currency"] = "";
$info["total_amount"] = "";
$info["total_amount_currency"] = "";
$info["shipping_charge"] = "";
$info["net_price"] = "";
$info["discount"] = "";
$info["from_date"] = "";
$info["to_date"] = "";
$info["rand"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();

//DEFAULT CURRENCY SECTION.
echo form_label('<b>DEFAULT CURRENCY</b>', 'DEFAULT CURRENCY');
echo form_input(array('name' => 'default_currency', 'value' => set_value("default_currency",$info["default_currency"]),'class' =>'span' ));
echo form_error("default_currency","<span class='text-error'>","</span>");
echo br();
//STATUS SECTION.
echo form_label('<b>STATUS</b>', 'STATUS');
echo form_input(array('name' => 'status', 'value' => set_value("status",$info["status"]),'class' =>'span' ));
echo form_error("status","<span class='text-error'>","</span>");
echo br();
//TOTAL AMOUNT CREDITS SECTION.
echo form_label('<b>TOTAL AMOUNT CREDITS</b>', 'TOTAL AMOUNT CREDITS');
echo form_input(array('name' => 'total_amount_credits', 'value' => set_value("total_amount_credits",$info["total_amount_credits"]),'class' =>'span' ));
echo form_error("total_amount_credits","<span class='text-error'>","</span>");
echo br();
//TOTAL CUSTOMER CHARGE SECTION.
echo form_label('<b>TOTAL CUSTOMER CHARGE</b>', 'TOTAL CUSTOMER CHARGE');
echo form_input(array('name' => 'total_customer_charge', 'value' => set_value("total_customer_charge",$info["total_customer_charge"]),'class' =>'span' ));
echo form_error("total_customer_charge","<span class='text-error'>","</span>");
echo br();
//ID USER SECTION.
echo form_label('<b>ID USER</b>', 'ID USER');
echo form_input(array('name' => 'id_user', 'value' => set_value("id_user",$info["id_user"]),'class' =>'span' ));
echo form_error("id_user","<span class='text-error'>","</span>");
echo br();
//CURRENCY SECTION.
echo form_label('<b>CURRENCY</b>', 'CURRENCY');
echo form_input(array('name' => 'currency', 'value' => set_value("currency",$info["currency"]),'class' =>'span' ));
echo form_error("currency","<span class='text-error'>","</span>");
echo br();
//TOTAL AMOUNT SECTION.
echo form_label('<b>TOTAL AMOUNT</b>', 'TOTAL AMOUNT');
echo form_input(array('name' => 'total_amount', 'value' => set_value("total_amount",$info["total_amount"]),'class' =>'span' ));
echo form_error("total_amount","<span class='text-error'>","</span>");
echo br();
//TOTAL AMOUNT CURRENCY SECTION.
echo form_label('<b>TOTAL AMOUNT CURRENCY</b>', 'TOTAL AMOUNT CURRENCY');
echo form_input(array('name' => 'total_amount_currency', 'value' => set_value("total_amount_currency",$info["total_amount_currency"]),'class' =>'span' ));
echo form_error("total_amount_currency","<span class='text-error'>","</span>");
echo br();
//SHIPPING CHARGE SECTION.
echo form_label('<b>SHIPPING CHARGE</b>', 'SHIPPING CHARGE');
echo form_input(array('name' => 'shipping_charge', 'value' => set_value("shipping_charge",$info["shipping_charge"]),'class' =>'span' ));
echo form_error("shipping_charge","<span class='text-error'>","</span>");
echo br();
//NET PRICE SECTION.
echo form_label('<b>NET PRICE</b>', 'NET PRICE');
echo form_input(array('name' => 'net_price', 'value' => set_value("net_price",$info["net_price"]),'class' =>'span' ));
echo form_error("net_price","<span class='text-error'>","</span>");
echo br();
//DISCOUNT SECTION.
echo form_label('<b>DISCOUNT</b>', 'DISCOUNT');
echo form_input(array('name' => 'discount', 'value' => set_value("discount",$info["discount"]),'class' =>'span' ));
echo form_error("discount","<span class='text-error'>","</span>");
echo br();
//FROM DATE SECTION.
echo form_label('<b>FROM DATE</b>', 'FROM DATE');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'from_date', 'value' => set_value("from_date",$this->fct->date_out_formate($info["from_date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("from_date","<span class='text-error'>","</span>");
echo br();
//TO DATE SECTION.
echo form_label('<b>TO DATE</b>', 'TO DATE');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'to_date', 'value' => set_value("to_date",$this->fct->date_out_formate($info["to_date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("to_date","<span class='text-error'>","</span>");
echo br();
//RAND SECTION.
echo form_label('<b>RAND</b>', 'RAND');
echo form_input(array('name' => 'rand', 'value' => set_value("rand",$info["rand"]),'class' =>'span' ));
echo form_error("rand","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>