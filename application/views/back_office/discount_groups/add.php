<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/discount_groups/'.$this->session->userdata("back_link"),'<b>List discount groups</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/discount_groups/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/discount_groups/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["discount"] = "";
$info["expiry_date"] = "";
$info["code"] = "";
$info["account_type"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
/*//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();
*/
//DISCOUNT&nbsp;<em>*</em>: SECTION.
echo form_label('<b>DISCOUNT&nbsp;<em>*</em>:</b>', 'DISCOUNT&nbsp;<em>*</em>:');

	echo form_label("Should be integer in %, maximum allowed digits is 3","help_text",array("class"=>"yellow"));
	echo form_input(array('name' => 'discount', 'value' => set_value("discount",$info["discount"]),'class' =>'span' ));
echo form_error("discount","<span class='text-error'>","</span>");
echo br();
//EXPIRY DATE SECTION.
echo form_label('<b>EXPIRY DATE</b>', 'EXPIRY DATE');

	echo form_label("Please leave empty if discount do not have expiry date","help_text",array("class"=>"yellow"));
	echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'expiry_date', 'value' => set_value("expiry_date",$this->fct->date_out_formate($info["expiry_date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("expiry_date","<span class='text-error'>","</span>");
echo br();
echo br();
echo form_label('<b>ACCOUNT TYPE</b>','<b>ACCOUNT TYPE</b>');
$lang_select = $this->lang->lang();
if(isset($lang)) {
	$lang_select = $lang;
}
$options = array();
$options[''] = '--Select Type--';
$options['retail'] = 'Retail';
$options['wholesale'] = 'Wholesale';
?>
<label>
<?php
echo form_dropdown('account_type',$options,set_value('account_type',$info['account_type'])); 
echo form_error("account_type","<span class='text-error'>","</span>");
echo br();
echo br();

//CODE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>CODE&nbsp;<em>*</em>:</b>', 'CODE&nbsp;<em>*</em>:');

	echo form_label("150 characters allowed","help_text",array("class"=>"yellow"));
	echo form_input(array('name' => 'code', 'value' => set_value("code",$info["code"]),'class' =>'span' ));
echo form_error("code","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>