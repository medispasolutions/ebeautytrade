<?php 
$bool=false;
if(count($order_details['group_products_line_items'])>0){$bool=true;}
?>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/project.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/project.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate2.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>

<script type="text/javascript" >
$(document).ready(function(){
$('#brands').change(function(){
	var id_supplier="";
		if($('#supplier')){
		var id_supplier=$('#supplier').val();}
		
	var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	
	getBrands(val);
		
	});
		
	
	if($('#supplier').length>0){
	
		var baseurl=$('#baseurl').val();
		var val=$('#supplier').val();
	
$.post(baseurl+'back_office/group_products/getBrands',{id_supplier:val},function(data){
var Parsedata = JSON.parse(data);
$('.product_stock').html(Parsedata.html2);
loadSearch();
});
		}
	$('#supplier').change(function(){
		var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	$('#supplier').val(val);
	$('#title_bx').hide();
		$('.empty').show();

		$('.order-details-table .form-controls').hide();
	$('.order-details-table').find('.table').parent().parent().remove()
	if(val==""){/*$('#suppliers_brands').html('');*/
	$('.product_stock').html('');}
$('#suppliers_brands').html('<div class="loader"></div>');	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('#suppliers_brands').html(Parsedata.html);


$('.product_stock').html(Parsedata.html2);

$('#brands').change(function(){
	
	var obj=$(this);
	var val =obj.val();
	
getBrands(val);
	});
		
		
		
		
    	});

});});
		
</script>
<script type="text/javascript" >
$(function(){
$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});</script>
<script type="text/javascript" >

function remove2(id_line_item,id_order,rand){
window.location="<?= site_url('back_office/group_products/remove_product'); ?>/"+id_line_item+"/"+id_order+"/"+rand;
return false;
}

function getBrands(id_brand){

	var id_supplier="";
		if($('#supplier')){
		var id_supplier=$('#supplier').val();}
		
	var baseurl=$('#baseurl').val();

		$('.product_stock').html('');
		$('#title_bx input').val('');
		$('#title_bx').show();
		$('.product_stock').html('');
		$('#loader_1').html('<div class="loader"></div>');
		$.post(baseurl+'back_office/inventory/getStockReachedThreshold',{id_brand:id_brand,id_supplier:id_supplier},function(data){
	$('#loader_1').html('');
	var Parsedata = JSON.parse(data);
$('.product_stock').html(Parsedata.html);
loadSearch();
	});	}
	
function loadSearch(){

	$("#match2 input[name='search']").live('keyup', function(e){

e.preventDefault();
var id =this.id;
$('#match2 tbody tr.tr_m').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr.tr_m').css('display',"");	
} else {
$('#match2 tbody tr.tr_m td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});}
	
$(document).ready(function(){
});

function backToForm(){
	$('#add-more').show();
		$('#addNewProducts').slideUp();
		$('.section-details').slideDown();
		scroll_To('.section-details',20);}
	function addNewProducts(){
		$('#add-more').hide();
	
		$('#addNewProducts').slideDown();
		$('.section-details').slideUp();
		/*scroll_To('#addNewProducts_form',20)*/;
		
		}</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" id="match2">
<div class="hundred pull-left">



<div class="span10-fluid" >
<?
$ul = array(
anchor('back_office/group_products','<b>List Group Products</b>').'<span class="divider">/</span>',
 $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
            );
/*			if($bool){
$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/group_products/print_group_products/'.$order_details['id_orders'].'/'.$order_details['rand']).'" target="_blank" class="btn btn-info top_btn">print / preview</a>');
}*/
/*$ul['new_order'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/add_group_products').'" target="_blank" class="btn btn-info top_btn">Add New Order</a>');*/
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<table class="table table-striped" id="table-1">

<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:left">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:left; background:none !important;">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
</tr>
<? } ?>
</table>
<div class="keyone-left">
	<?php $this->load->view("back_office/group_products/products-menu"); ?>
</div>


<div class="keyone-right">
<div class="row-fluid realEstateTabContainer" id="realEstateTabContainer-3" >
 <?php $this->load->view("back_office/group_products/gallery/add_gallery"); ?>

</div>
        
 		<div class="row-fluid realEstateTabContainer" id="realEstateTabContainer-2" <?php if(isset($tab) && $tab==2) echo 'style="display:block"';?>> 
        <?php echo form_fieldset('Product Details');?>
        <?php $this->load->view('back_office/products/add_content');?>
        <?php echo form_fieldset_close();?>
        </div>
        
        <div class="row-fluid realEstateTabContainer" id="realEstateTabContainer-1" <?php if(!isset($tab) || (isset($tab) && $tab==1)) echo 'style="display:block"';?>> 
 <?php $this->load->view('back_office/group_products/group_products');?>
 </div>
 
 
 
        </div>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>