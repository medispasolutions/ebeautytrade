<?php
$content_type = 'products';
$id_gallery = $id;

?>


<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>js/plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/jquery-ui.min.js"></script>
<!-- production -->
<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
<script type="text/javascript">
// Initialize the widget when the DOM is ready
$(function() {
	var uploaderObj = $("#uploader").plupload({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : '<?php echo site_url('back_office/group_products/upload_gal_image/'.$content_type.'/'.$id_gallery); ?>'+'/'+$("input[name='type']").val(),
		// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 20,
		
	//	chunk_size: '1mb',

		// Resize images on clientside if we can
		/*resize : {
			width : 200, 
			height : 200, 
			quality : 90,
			crop: true // crop to exact dimensions
		},*/
		
		filters : {
			// Maximum file size
			//max_file_size : '1000mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Images", extensions : "jpg,gif,png,jpeg"},
				//{title : "Zip files", extensions : "zip"},
				//{title : "Files", extensions : "pdf,doc,docx"}
			]
		},

		// Rename files by clicking on their titles
		rename: false,
		
		// Sort files
		sortable: false,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,

		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},

		// Flash settings
		flash_swf_url : '<?php echo base_url(); ?>js/plupload/js/Moxie.swf',
		// Silverlight settings
		silverlight_xap_url : '<?php echo base_url(); ?>js/plupload/js/Moxie.xap',
		// Called when all files are either uploaded or failed

		init: {
			UploadComplete: function(up, files) {
				$('#uploader_filelist').html('');
			   $('#ajax_gallery').html("Loading...");
				var url = '<?=site_url('back_office/group_products/loadGallery')?>';
				$.post(url,{content_type:"<?php echo $content_type; ?>",id : <?php echo $id_gallery; ?>, type : $("input[name='type']").val()},function(data){
					$('#ajax_gallery').html(data);
					if($('.masterkeyImg').length > 0) {
						$('.masterkeyImg').each(function(){
							var Obj = $(this);
							var ObjDataLs = Obj.attr("data-ls");
							var ObjDataLs = JSON.parse(ObjDataLs);
							var ObjSrc = ObjDataLs.src;
							Obj.attr("src",ObjSrc);
							Obj.removeAttr("data-ls");
						});
					}
				});
			}
		}
	});
});
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span12" >
<div class="hundred pull-left">  

<fieldset>
<legend>Images</legend>
<div id="uploader">
		<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
	</div>
</fieldset>
<p class="pull-left">

<input type="hidden" name="content_type" value="<?= $content_type; ?>"  />
<input type="hidden" name="id_gallery" value="<?= $id_gallery; ?>"  />
<input type="hidden" name="type" value="image"  />
<div id="ajax_gallery">
    <?php
		$data['content_type'] = $content_type;
		$data['id'] = $id_gallery;
		$data['gallery']=$this->site_model->getFileGallery($content_type,$id_gallery,array("image","masterkey"));
		$this->load->view('back_office/group_products/gallery/ajax_gallery',$data);
		?>
</div>
</div> 
</div>
</div>
</div>