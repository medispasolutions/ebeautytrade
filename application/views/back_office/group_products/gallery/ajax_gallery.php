<?php
$rand = rand();
//print '<pre>'; print_r($gallery);exit;
?>
<style type="text/css">
#sortable_<?php echo $rand; ?> {
 list-style-type: none;
 margin: 0;
 padding: 0;
 width: 100%;
}
#sortable_<?php echo $rand; ?> li {
 margin: 0 3px 3px 3px;
 padding: 5px;
 float:left;
 position:relative;
 margin-bottom:10px;
 cursor:move

}
#sortable_<?php echo $rand; ?> li span {
 position: absolute;
 top:-13px;
 cursor:pointer;
}
.remove_gallery {
 float:left;
 text-align:center
}
.remove_gallery a {
 color:#000;
}
.remove_gallery a:hover {
 color:#EB832A;
}
.reload_gallery {
 float:left;
 width:100%;
}
</style>
<script>
$(document).ready(function(){

$('#sortable_<?php echo $rand; ?>').sortable({
update: function(event, ui) {
var newOrder = $(this).sortable('toArray').toString();
var site_url = '<?=site_url('back_office/group_products/sort_order_gallery')?>';
$.post(site_url,{content_type: "<?php echo $content_type; ?>",newOrder : newOrder},function(data){
$('#result_<?php echo $rand; ?>').html(data);
});
}
});
$( "#sortable_<?php echo $rand; ?>" ).disableSelection();	
})

function deletegal(id){
	var id_image=id;
	
var site_url = '<?=site_url('back_office/control/delete_gal_image')?>';
$.post(site_url,{content_type: "<?php echo $content_type; ?>", id_image : id_image,id:<?php echo $id; ?>},function(data){
	$('#'+id).remove();
});	
}

</script>

<div class="reload_gallery" style="margin-top:20px">
<ul id="sortable_<?php echo $rand; ?>">
<? $i=0; foreach($gallery as $val){$i++;?>
<li id="<?=$val['id_gallery']?>"  class="ui-state-default<? if($i%6==0){?> last <? }?>">
<?php if($val['type'] == "image") {?>
<img src="<?=base_url()?>uploads/<?php echo $content_type; ?>/gallery/120x120/<?=$val['image']?>" />
<?php }elseif($val['type'] == "masterkey"){?>
<img src="" class="masterkeyImg" data-ls='{"src":"<?php echo $val['image']; ?>"}' width="120" height="120" />
<?php } else {?>
<label><a href="<?=base_url()?>uploads/<?php echo $content_type; ?>/gallery/<?=$val['image']?>" target="_blank"><?php echo $val['image']; ?></a></label>
<?php }?>
<span class="remove_gallery">
<a onclick="if(confirm('Are you sure?')) {deletegal(<?=$val['id_gallery']?>)}"><img src="<?=base_url()?>images/remove_gallery.png" /></a></span>
</li>
<? }?>
</ul>
</div>
<div id="result_<?php echo $rand; ?>"></div>