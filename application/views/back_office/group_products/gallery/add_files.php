<?php
$content_type = $this->router->class;
$id_gallery = $id;
?>
<!--<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>js/plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />-->
<!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>-->
<!--<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/jquery-ui.min.js"></script>-->
<!-- production -->
<!--<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/plupload.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"></script>-->
<!-- debug WWW
<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/moxie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/plupload.dev.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/plupload/js/jquery.ui.plupload/jquery.ui.plupload.js"></script>
-->
<script type="text/javascript">
// Initialize the widget when the DOM is ready
$(function() {
	var uploaderObj = $("#uploader1").plupload({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : '<?php echo site_url('back_office/group_products/upload_gal_image/'.$content_type.'/'.$id_gallery); ?>'+'/'+$("input[name='type1']").val(),

		// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 20,
		
	//	chunk_size: '1mb',

		// Resize images on clientside if we can
		/*resize : {
			width : 200, 
			height : 200, 
			quality : 90,
			crop: true // crop to exact dimensions
		},*/
		
		filters : {
			// Maximum file size
			//max_file_size : '1000mb',
			// Specify what files to browse for
			mime_types: [
				//{title : "Image files", extensions : "jpg,gif,png"},
				//{title : "Zip files", extensions : "zip"},
				{title : "Files", extensions : "pdf,doc,docx"}
			]
		},

		// Rename files by clicking on their titles
		rename: false,
		
		// Sort files
		sortable: false,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,

		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},

		// Flash settings
		flash_swf_url : '<?php echo base_url(); ?>js/plupload/js/Moxie.swf',
		// Silverlight settings
		silverlight_xap_url : '<?php echo base_url(); ?>js/plupload/js/Moxie.xap',
		// Called when all files are either uploaded or failed

		init: {
			UploadComplete: function(up, files) {
				$('#uploader1_filelist').html('');
			   $('#ajax_gallery1').html("Loading...");
				var url = '<?=site_url('back_office/group_products/loadGallery')?>';
				$.post(url,{content_type:"<?php echo $content_type; ?>",id : <?php echo $id_gallery; ?>, type : $("input[name='type1']").val()},function(data){
				$('#ajax_gallery1').html(data);});
			}
		}
	});
});
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span12" >
<div class="hundred pull-left">  

<fieldset>
<legend>Documents</legend>
<div id="uploader1">
		<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
	</div>
</fieldset>
<p class="pull-left">
<input type="hidden" name="content_type" value="<?= $content_type; ?>"  />
<input type="hidden" name="id_gallery" value="<?= $id_gallery; ?>"  />
<input type="hidden" name="type1" value="file"  />
<div id="ajax_gallery1">
    <?php
		$data['content_type'] = $content_type;
		$data['id'] = $id_gallery;
		$data['gallery']=$this->fct->getAll_cond($content_type.'_gallery','sort_order',array('id_'.$content_type=>$id_gallery,"type"=>"file"));
		$this->load->view('back_office/group_products/gallery/ajax_gallery',$data);
		?>
</div>
</div> 
</div>
</div>
</div>