
<?php 
$bool=false;
if(count($order_details['group_products_line_items'])>0){$bool=true;}
?>

<?php echo form_fieldset('Product kits');?>
  


<?php
if(isset($errors)){
	echo $errors;}?>
  
<form id="SetPaidForm" action="<?php echo site_url('back_office/group_products/submit'); ?>" method="post">


<input type="hidden" name="id_orders" value="<?php echo $order_details['id_orders']; ?>" />
<input type="hidden" name="rand" value="<?php echo $order_details['rand']; ?>" />

  <?php if($bool){?>


<?php $brands_line_items=$order_details['group_products_line_items'];

?>

<?php if(!empty($brands_line_items)) {?>
<?php foreach($brands_line_items as $brand) {
		
 $group_products_line_items=$brand['line_items'];?> 
<table class="table table-striped order-details-table">
  <tr><td colspan="7" style="font-size:25px; background:#CCC; color:white; text-align:center;"><?php echo $brand['title'];?></td></tr>
          <tr>
          
            <th width="20%">Product Name</th>
            <th width="15%">Sku</th>
           <!-- <th width="15%" style="text-align:center;">Price(<?php echo $order_details['currency'];?>)</th>-->
            <th width="15%" style="text-align:center;">Quantity</th>
            
             <?php if($order_details['status']==1){ ?>
<!--              <th width="10%" style="text-align:center;">Return Quantity</th>
              <th width="10%" style="text-align:center;">Sold Quantity</th>-->
			 <?php } ?>

     <th  width="10%"></th>
          </tr>
          
 
          
     <?php foreach($group_products_line_items as $pro){
$pro['type']=="product";		

		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];	
		$options=unSerializeStock($pro['stock']['combination']); 
			 }else{
		$sku=$pro['product']['sku'];
		$options="";}
		  ?>
		
	
<tr class="order-table-row">
    
    
        <td>
		 <input type="hidden" name="ids[]" value="<?php echo $pro['id_group_products_line_items'];?>" />
		<?php echo $pro['product']['title'];?>
        <br />    <?php
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
        </td>
        <td><?php echo $sku;?></td>
      
  <!--<td style="text-align:center;">
       <?php if($order_details['status']!='paid' && $order_details['status']!='completed'){?> 
	    <input type="text"  name="price[]"  style="width:50px;" value="<?php echo set_value("price",$pro["price"]);?>" >
		<?php }else{ echo $pro['price']; }?></td>-->
        <td style="text-align:center;">
       <?php if($order_details['status']!=1 && empty($order_details['exported_order'])){?> 
	    <input type="text"  name="qty[]"  style="width:50px;" value="<?php echo set_value("qty",$pro["quantity"]);?>" >
		<?php }else{ echo $pro['quantity']; }?></td>
        
 

        <td style="text-align:center;">
       <?php if($order_details['status']!=1 && (empty($order_details['exported_order']) || $order_details['invoice']==1) ){ ?>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2(<?php echo $pro['id_group_products_line_items'];?>,<?php echo $pro['id_orders'];?>,<?php echo $order_details['rand'];?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>

</a>
<?php } ?></td>

        <!--<td>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2(<?php echo $rand;?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>
Remove
</a></td>-->
        </tr>
			 
	 <?php } ?> 


       
        </table>
     <?php } ?>
<?php }?>
<?php if($bool){?>
<div class="row-fluid">
 <?php if($order_details['status']!=1 && (empty($order_details['exported_order']) || $order_details['invoice']==1) ){ ?>
<div class="pull-left">
<a class="btn btn-primary" id="add-more" onclick="addNewProducts()"> + Add More Products</a></div>
<?php } ?>

</div>
<?php } ?>

<div class="section-details">

<?php echo br(); ?>
<div class="row-fluid">
<div class="span3" style="margin-left:0;">
<label>
<label><b>Qauntity</b></label>
<input type="text" name="quantity_kits" value="<?php echo $order_details['quantity'];?>" />
</div>
</div>

<div class="pull-right">

<input type="submit" name="submit" value="Save Changes" class="btn btn-primary" id="SetPaid" />

</div>

</div>

        
 
 <?php }?> </form>
 
 
<div class="row-fluid sortable" id="addNewProducts" style="margin-top:20px; <?php if($bool){?>display:none<?php } ?>">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-info-sign"></i> Add Product(s)</h2>
              <?php if($bool){?>
              <div class="box-icon"><a  onclick="backToForm()" class="btn  btn-round"><i class="icon-remove"></i></a></div>
              <?php } ?>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form  method="post" enctype="multipart/form-data" id="addNewProducts_form" action="<?php echo site_url('back_office/group_products/insertNewProducts');?>" >

             <input type="hidden" name="id_orders" value="<?php echo $order_details['id_orders'];?>" />
             <input type="hidden" name="rand" value="<?php echo $order_details['rand'];?>" />

  <div class="span3 form-item">
<label>Trading Name *</label>
<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name asc',$cond3);
?>

<select name="id_user" id="supplier"  >
<option value="">-Select trading name-</option>
<?php 
foreach($suppliers as $val){?>
<option value="<?php echo $val['id_user'];?>" ><?php echo $val['trading_name'];?></option>
<?php } ?>
</select>
 <div id="id_user-error" class="form-error"></div>

</div>    
     
              
<div class="span3" id="suppliers_brands">
<label>Brand </label>
<?php 
$brands=array();
if(isset($order_details['user']) && !empty($order_details['user'])){
$brands = $this->fct->getAll_cond('brands','title asc',array('id_user'=>$order_details['id_user']));
 } ?>


<select name='brands' id="brands">
<option value="">-Select All-</option>
<?php 
foreach($brands as $val){?>
<option value="<?php echo $val['id_brands'];?>"><?php echo $val['title'];?></option>
<?php } ?>
</select>

</div>


<div class="product_stock"></div>

<div id="loader_1"></div>
 
</form>
              </div>
            </div>
          </div>
        </div>
<?php echo form_fieldset_close();?>
