<?php 
	$lang = "";
	if($this->config->item("language_module")) {
		$lang = getFieldLanguage($this->lang->lang());
	}
?>
<?php

echo form_fieldset("Specifications");
//PAGE TITLE SECTION.
?>

<table class="table table-striped" id="table-1">
<tfoot>
</tfoot>
<tbody>
<? 
$product_specifications=$info['specifications'];


$select_arr=array();
$specifications=$this->fct->getAll('specifications','sort_order asc');
foreach($product_specifications as $s){
	array_push($select_arr,$s['id_specifications']);
	}
	
	

if(isset($specifications) && !empty($specifications)){
$i=0;
foreach($specifications as $val){
$i++; 
?>
<tr id="<?=$val["id_specifications"]; ?>">
<td style="position:relative;width: 21px;">
<?php /*?><?php if($val["list_price"]>$val['price']){?>
<img style="position:absolute;top:0;left:0;" src="<?php echo base_url();?>front/img/salelogo1.png" />
<?php } ?><?php */?></td>
<td class="col-chk"><input <?php if(in_array($val['id_specifications'],$select_arr)) echo "checked";?> type="checkbox" name="specifications[]" value="<?= $val["id_specifications"] ; ?>" /></td>

<td class="title_search">
<? echo $val["title".$lang]; ?><br />

</td>

<td class="sku_search">
<? $value=$this->fct->getonecell('products_specifications','value',array('id_specifications'=>$val['id_specifications'],'id_products'=>$id)); 
?>
<?php 

if(empty($value)) {$value=$val["default_value"];}?>

<input type="text" value="<?php echo $value;?>" name="quantity_specifications[<?= $val["id_specifications"] ; ?>]" />
</td>






</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="10" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>
<?php
echo form_fieldset_close();
?>
