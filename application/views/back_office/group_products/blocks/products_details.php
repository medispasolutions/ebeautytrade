<?php 
	$lang = "";
	if($this->config->item("language_module")) {
		$lang = getFieldLanguage($this->lang->lang());
	}
?>

<?php echo form_fieldset("Product Details"); ?>
<?php if(isset($id)){ ?>
<label><strong>REMAINING  QUANTITY: </strong><span id="remanin_quantity"><?php echo $info['quantity'];?></span></label>
<br />
<?php } ?>

<?php

/*echo form_label('<b>Quantity&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "quantity", "value" => set_value("quantity",$info["quantity"]),"class" =>"span" ));
echo form_error("quantity","<span class='text-error'>","</span>");
echo br();*/
?>

<div class="row-fluid">
<div class="span6">
<?php
//TITLE SECTION.
echo form_label('<b>Product Name&nbsp;<small class="blue">(Max '.getLimitText('title').' Characters)</small><em>*</em></b>', 'Product Name');
echo form_input(array("id"=>"title","name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span","onKeyDown"=>"limitText(this.form.title,0,".getLimitText('title').");","onKeyUp"=>"limitText(this.form.title,0,".getLimitText('title').");" ));
echo form_error("title","<span class='text-error'>","</span>");
echo br();

?>
</div>

<div class="span6">
<?php
echo form_label('<b>BRANDS</b> ', 'BRANDS');

$cond=array();
if(checkIfSupplier_admin()){
$user=$this->ecommerce_model->getUserInfo();
$cond['id_user']=$user['id_user'];
}

$brands = $this->fct->getAll_cond('brands','sort_order',$cond);
$options = array();
$options[""] = '--Select Brands--';
foreach($brands as $brand) {
	$options[$brand['id_brands']] = $brand['title'];
}

echo form_dropdown('id_brands',$options,set_value('id_brands',$info['id_brands'])); 
echo form_error("id_brands","<span class='text-error'>","</span>");
echo br();

?>
</div>

</div>

<div class="row-fluid">

<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>SKU&nbsp;<em>*</em></b>  ', 'SKU&nbsp;<em>*</em>');
echo form_input(array('name' => 'sku', 'value' => set_value("sku",$info["sku"]),'class' =>'span' ));
echo form_error("sku","<span class='text-error'>","</span>");
echo br();
?>


</div>
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>BARCODE&nbsp;</b>  ', 'BARCODE&nbsp;<em>*</em>:');
echo form_input(array('name' => 'barcode', 'value' => set_value("barcode",$info["barcode"]),'class' =>'span' ));
echo form_error("barcode","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>

<div class="row-fluid">
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>Threshold <small class="blue">(Min Quantity to receive stock notification)</small>&nbsp;</b>  ', 'Threshold&nbsp;');
echo form_input(array('name' => 'threshold', 'value' => set_value("sku",$info["threshold"]),'class' =>'span' ));
echo form_error("threshold","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<?php
echo form_label('<b>Classifications</b> ', 'Classifications');

$cond=array();

$classifications = $this->fct->getAll_cond('classifications','sort_order',$cond);
$options = array();
$options[""] = '--Select Classifications--';
foreach($classifications as $classification) {
	$options[$classification['id_classifications']] = $classification['title'];
}

echo form_dropdown('id_classifications',$options,set_value('id_classifications',$info['id_classifications'])); 
echo form_error("id_classifications","<span class='text-error'>","</span>");
echo br();

?>
</div>

</div>




<div class="row-fluid">
<div class="span6">
<?php
//TITLE SECTION.
/*echo form_label('<b>Brief(Line one) <small class="blue">(Max 35 Characters)</small>&nbsp;:</b>', 'Brief(Line one)');
echo form_input(array("id"=>"brief_one",'maxlength'=>35,"name" => "brief_one".$lang, "value" => set_value("brief_one".$lang,$info["brief_one".$lang]),"class" =>"span"));
echo form_error("brief_one","<span class='text-error'>","</span>");
echo br();
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>Brief(Line Two) <small class="blue">(Max 35 Characters)</small>&nbsp;:</b>  ', 'Brief(Line Two)&nbsp;<em>*</em>:');
echo form_input(array('name' => 'brief_two','maxlength'=>35, 'value' => set_value("brief_two",$info["brief_two"]),'class' =>'span' ));
echo form_error("brief_two","<span class='text-error'>","</span>");
echo br();*/
?>
<?php
//META DESCRIPTION SECTION.
echo form_label('<b>BRIEF&nbsp;<small class="blue">(Max 115 Characters)</small></b>', 'BRIEF');
echo form_textarea(array("name" => "brief".$lang,"style" =>"height:54px;", "value" => set_value("brief".$lang,$info["brief".$lang]),"class" =>"span","rows" => 4, "cols" =>100,"onKeyDown"=>"limitText(this.form.brief,0,115);","onKeyUp"=>"limitText(this.form.brief,0,115);"));
echo form_error("brief".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>


<div class="span6">

<!--//QUANTITY SECTION.-->

<?php 

//BACKGROUND SECTION.
echo "<label>";
echo form_label('<b>PRINTABLE DATASHEET <small class="blue">(Max Size '.$size_arr['size_mega'].'Mb)</small></b>', 'DATASHEET');
echo form_upload(array("name" => "datasheet", "class" => "input-large"));
echo "<span >";
if($info["datasheet"] != ""){ 
echo '<span id="datasheet_'.$info["id_products"].'">' ;?>
<a target="_blank" href="<?php echo base_url();?>uploads/products/<?php echo $info["datasheet"];?>">Show File</a>
<?php 
echo nbs(3);?>
<a class="cur" onclick='removeFile("products","datasheet","<?php echo $info["datasheet"]; ?>",<?php echo $info["id_products"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo "</label>";
echo form_error("datasheet","<span class='text-error'>","</span>");
echo br();

?></div>
</div>

<div class="row-fluid">
<div class="span6">
<?php
//TITLE SECTION.
echo form_label('<b>Delivery Day</b>', 'Delivery Day');
echo form_input(array("id"=>"delivery_day","name" => "delivery_day".$lang, "value" => set_value("delivery_day".$lang,$info["delivery_day".$lang]),"class" =>"span"));
echo form_error("delivery_day","<span class='text-error'>","</span>");
echo br();

?>
</div>

<div class="span6" style="margin-top:25px;">
<?php
if(isset($exclude_from_shipping_calculation)){}else{

if(isset($info['exclude_from_shipping_calculation']))
$exclude_from_shipping_calculation = $info['exclude_from_shipping_calculation'];}

?>
<label><input type="checkbox" id="exclude_from_shipping_calculation" name="exclude_from_shipping_calculation" <?php if(isset($exclude_from_shipping_calculation) && $exclude_from_shipping_calculation == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Exclude from shipping calculation</b></label>

</div>

</div>
<div class="row-fluid">
<div class="span6">
<?php 
$selected_options=array();
if(isset($id)) {
$selected_options = $this->fct->select_product_categories($id);

}
if(isset($h_selected_options)){
$selected_options =$h_selected_options ;	}
//CATEGORY SECTION.
echo form_label('<b>CATEGORIES</b>', 'CATEGORIES');
$items = $this->custom_fct->getCategoriesPages(0,array(),array(),12000,0);

//print '<pre>'; print_r($items); exit; 
$categories = $items;
echo '<select name="categories[]"  class="span" multiple="multiple" id="multi_select_categories" style="height:220px;"  onchange="getSelectedOptions(this)">';
/*echo '<option value=""  > - set as parent - </option>';*/
$s_id = "";
if(isset($id) && isset($info['id_parent'])) {
$s_id = $info['id_parent'];
}
echo displayLevels($items,0,$s_id,$selected_options);
echo "</select>";
echo form_error("categories","<span class='text-error'>","</span>");
?>


</label>
</div>
<div class="span6">

<?php echo br(); ?>
<div class="select_categories">
<?php if(isset($selected_options) && !empty($selected_options)) { ?>

<?php foreach($selected_options as $cat_id){
	$category_selected=$this->fct->getonerecord('categories',array('id_categories'=>$cat_id));
	echo "<div class='btn btn_select' id='selelect_btn_".$cat_id."'><a href=".site_url('back_office/categories/edit/'.$cat_id).">".$category_selected['title']."</a><span class='notification red remove_selected'
	 onclick='SelectedCategory(".$cat_id.")' >x</span></div>";
	?>
	<?php } ?>
	<?php } ?>
   
</div>
</div>
</div>

<!--<a href="#myModal" onclick="popup_categories();" data-toggle="modal" >[add more]</a>-->


<?php 
echo br();
echo form_label('<b>DESCRIPTION </b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"ckeditor","id" => "description".$lang, "rows" => 15, "cols" =>100 ));
echo br();
echo form_error("description","<span class='text-error'>","</span>");

?>
<div  id="dimensions"  <?php if(isset($set_as_non_exportable) && $set_as_non_exportable == 0) {}else{?>style="display:block;"<?php }?>>

<?php
echo form_fieldset("Weight & Size ");
?>
<div class="row-fluid">
<div class="span3">
<?php

echo form_label('<b>WEIGHT(G)&nbsp;<em>*</em></b>  ', 'WEIGHT&nbsp;<em>*</em>');
echo form_input(array('name' => 'weight', 'value' => set_value("weight",$info["weight"]),'class' =>'span' ));
echo br();
echo form_error("weight","<span class='text-error'>","</span>");


?></div>
<div class="span3">
<?php 
echo form_label('<b>Height(CM)&nbsp;<em>*</em></b>  ', 'Height&nbsp;<em>*</em>');
echo form_input(array('name' => 'height', 'value' => set_value("height",$info["height"]),'class' =>'span' ));
echo form_error("height","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span3">
<?php
echo form_label('<b>length(CM)&nbsp;<em>*</em></b>  ', 'length&nbsp;<em>*</em>');
echo form_input(array('name' => 'length', 'value' => set_value("length",$info["length"]),'class' =>'span' ));
echo form_error("length","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span3">
<?php
echo form_label('<b>Width(CM)&nbsp;<em>*</em></b>  ', 'Width&nbsp;<em>*</em>');
echo form_input(array('name' => 'width', 'value' => set_value("width",$info["width"]),'class' =>'span' ));
echo form_error("width","<span class='text-error'>","</span>");
echo br();
?>
</div>

</div>
</div>


<?php
if(isset($info['set_as_new']) && !isset($set_as_new))
$set_as_new = $info['set_as_new'];
?>
<label><input type="checkbox" name="set_as_new" <?php if(isset($set_as_new) && $set_as_new == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as new Arrival</b></label>

<?php
if(isset($set_as_clearance)){}else{
if(isset($info['set_as_clearance']))
$set_as_clearance = $info['set_as_clearance'];}
?>
<label><input type="checkbox" id="set_as_clearance" name="set_as_clearance" <?php if(isset($set_as_clearance) && $set_as_clearance == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>set as clearance</b></label>

<?php
if(isset($set_as_non_exportable)){}else{
if(isset($info['set_as_non_exportable']))
$set_as_non_exportable = $info['set_as_non_exportable'];}
?>
<label><input type="checkbox" id="set_as_non_exportable" name="set_as_non_exportable" <?php if(isset($set_as_non_exportable) && $set_as_non_exportable == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>set as non-exportable product</b></label>

<?php
if(isset($info['set_as_soon']) && !isset($set_as_soon))
$set_as_soon = $info['set_as_soon'];
?>

<label><input type="checkbox" name="set_as_soon" <?php if(isset($set_as_soon) && $set_as_soon == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as soon</b></label>

<?php
if(isset($set_as_pro)){}else{
if(isset($info['set_as_pro']))
$set_as_pro = $info['set_as_pro'];}
?>
<label><input type="checkbox" id="set_as_pro" name="set_as_pro" <?php if(isset($set_as_pro) && $set_as_pro == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as pro</b></label>


<?php
echo br();
echo form_fieldset("PRODUCT STATUS");

echo form_label('<b>STATUS</b>', 'STATUS');
$options = array();
$options[''] = '-select status-';
$options[0] = 'Published';

$options[1] = 'Unpublished';
$options[2] = 'Under Review';
$options[5] = 'Not Saved';
?>
<label>
<?php
echo form_dropdown('status',$options,set_value('status',$info['status'])); 
echo form_error("status","<span class='text-error'>","</span>");


?>
</label>

<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Client</label>


<?php
echo br(); 

echo form_fieldset_close();
?>