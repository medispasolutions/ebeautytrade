<?php 
	$lang = "";
	if($this->config->item("language_module")) {
		$lang = getFieldLanguage($this->lang->lang());
	}
?>
<?php
echo form_fieldset("PRICE");
?>

<div class="row-fluid">
<div class="span4">
<?php 

//PRICE SECTION.
echo form_label('<b>PRICE *</b> ('.$this->config->item('default_currency').') ', 'PRICE');
echo form_input(array('name' => 'list_price', 'value' => set_value("list_price",$info["list_price"]),'class' =>'span',"onkeyup"=>"sellingPrice()" ));
echo form_error("list_price","<span class='text-error'>","</span>");
?>
</div>
<div class="span4">
<?php

echo form_label('<b>DISCOUNT</b>', 'DISCOUNT');
echo form_input(array('name' => 'discount', 'value' => set_value("discount",$info["discount"]),'class' =>'span',"onkeyup"=>"sellingPrice()" ));
echo form_error("discount","<span class='text-error'>","</span>");
?>
</div>
<div class="span4">
<?php


//DISCOUNT EXPIRATION SECTION.
/*echo form_label('<b>DISCOUNT EXPIRATION</b> ', 'DISCOUNT EXPIRATION');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'discount_expiration', 'value' => set_value("discount_expiration",$this->fct->date_out_formate($info["discount_expiration"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("discount_expiration","<span class='text-error'>","</span>");

echo form_label('<span style="color:#F03">Not Visible for the users, system will use it as expiry date for the discount.</span>');*/
?>
</div>

</div>

<div class="row-fluid">

<div class="span4">
<?php

//PRICE SECTION.
echo form_label('<b>Selling Price</b> ('.$this->config->item('default_currency').') ', 'Selling Price');
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span','readonly'=>'true' ));
echo form_error("price","<span class='text-error'>","</span>");
?>
</div>
<div class="span4">
<?php



//RETAIL PRICE&nbsp;<em>*</em>: RETAIL PRICE.
echo form_label('<b>SUGGESTED RETAIL PRICE&nbsp;<em>*</em></b> ('.$this->config->item('default_currency').') ', 'SUGGESTED RETAIL PRICE');
echo form_input(array('name' => 'retail_price', 'value' => set_value("retail_price",$info["retail_price"]),'class' =>'span' ));
echo form_error("retail_price","<span class='text-error'>","</span>");
echo form_label('<span style="color:#F03">This price will be used only as information as Suggested Retail Price.</span>  ', 'This price will be used only as information as Suggester Retail Price</span>.');
echo br();
?>
</div>

</div>

<?php if(isset($id)){ ?>
<?php $segements=$this->fct->getAll_cond('product_price_segments','sort_order',array('id_products'=>$id));
   $data2['segements']=$segements;?>
<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2 style="text-transform:uppercase;">Multi buy</h2>
              <div class="box-icon pull-right">
             
              <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
              <a id="product_price_segments_btn" class="pull-right fancyClick3" href="<?php echo site_url('back_office/product_price_segments/add?id_product='.$id);?>" style="margin-top:5px;margin-right:5px;text-transform:uppercase;<?php if(count($segements)>=3) echo "display:none;"; ?>">[Add Price Segements]</a>
              
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
              <div class="priceSegements">
        
<?php echo $this->load->view('back_office/blocks/products_segements',$data2,true);?>
</div>




              </div>
            </div>
          </div>
        </div>
        <?php } ?>
<?php
echo form_fieldset_close();
?>