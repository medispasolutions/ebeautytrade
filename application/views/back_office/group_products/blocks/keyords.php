<?php 
	$lang = "";
	if($this->config->item("language_module")) {
		$lang = getFieldLanguage($this->lang->lang());
	}
?>
<?php

echo form_fieldset("Meta Tags");
//PAGE TITLE SECTION.
?>
<div class="row-fluid">
<div class="span6">
<?php
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max '.getLimitText('page_title').' Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span","onKeyDown"=>"limitText(this.form.meta_title,0,".getLimitText('page_title').");","onKeyUp"=>"limitText(this.form.meta_title,0,".getLimitText('page_title').");" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();?>
</div>
<div class="span6">
<?php 
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<div class="row-fluid">
<div class="span6">
<?php
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max '.getLimitText('meta_description').' Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100,"onKeyDown"=>"limitText(this.form.meta_description,0,".getLimitText('meta_description').");","onKeyUp"=>"limitText(this.form.meta_description,0,".getLimitText('meta_description').");"));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<?php 
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max '.getLimitText('meta_description').' Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100,"onKeyDown"=>"limitText(this.form.meta_keywords,0,".getLimitText('meta_description').");","onKeyUp"=>"limitText(this.form.meta_keywords,0,".getLimitText('meta_description').");" ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<?php
echo form_fieldset_close();
?>
