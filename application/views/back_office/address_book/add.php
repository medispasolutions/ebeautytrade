<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
            anchor('back_office/address_book/'.$this->session->userdata("back_link"),'<b>List address book</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<div class="hundred pull-left">   
<?
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/address_book/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["first_name"] = "";$info["street_address"] = "";$info["city"] = "";$info["state"] = "";$info["country"] = "";$info["company"] = "";$info["phone"] = "";$info["email"] = "";$info["last_name"] = "";$info["postal_code"] = "";
$info["title"] = "";
$info["meta_title"] = "";
$info["meta_description"] = "";
$info["meta_keywords"] = "";
$info["title_url"] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title", "value" => set_value("title",$info["title"]),"class" =>"span" ));
echo form_error("title","<span class='text-error'>","</span>");
echo br();
//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title", "value" => set_value("meta_title",$info["meta_title"]),"class" =>"span" ));
echo form_error("meta_title","<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url", "value" => set_value("title_url",$info["title_url"]),"class" =>"span" ));
echo form_error("title_url","<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description", "value" => set_value("meta_description",$info["meta_description"]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description","<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords", "value" => set_value("meta_keywords",$info["meta_keywords"]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords","<span class='text-error'>","</span>");
echo br();

//FIRST NAME SECTION.
echo form_label('<b>FIRST NAME</b>', 'FIRST NAME');
echo form_input(array('name' => 'first_name', 'value' => set_value("first_name",$info["first_name"]),'class' =>'span' ));
echo form_error("first_name","<span class='text-error'>","</span>");
echo br();
//STREET ADDRESS SECTION.
echo form_label('<b>STREET ADDRESS</b>', 'STREET ADDRESS');
echo form_input(array('name' => 'street_address', 'value' => set_value("street_address",$info["street_address"]),'class' =>'span' ));
echo form_error("street_address","<span class='text-error'>","</span>");
echo br();
//CITY SECTION.
echo form_label('<b>CITY</b>', 'CITY');
echo form_input(array('name' => 'city', 'value' => set_value("city",$info["city"]),'class' =>'span' ));
echo form_error("city","<span class='text-error'>","</span>");
echo br();
//STATE SECTION.
echo form_label('<b>STATE</b>', 'STATE');
echo form_input(array('name' => 'state', 'value' => set_value("state",$info["state"]),'class' =>'span' ));
echo form_error("state","<span class='text-error'>","</span>");
echo br();
//COUNTRY SECTION.
echo form_label('<b>COUNTRY</b>', 'COUNTRY');
$items = $this->fct->getAll("countries","sort_order"); 
echo '<select name="country"  class="span">';
echo '<option value="" > - select countries - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_countries"]; ?>" <? if(isset($id)){  if($info["id_countries"] == $valll["id_countries"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("country","<span class='text-error'>","</span>");
echo br();
//COMPANY SECTION.
echo form_label('<b>COMPANY</b>', 'COMPANY');
echo form_input(array('name' => 'company', 'value' => set_value("company",$info["company"]),'class' =>'span' ));
echo form_error("company","<span class='text-error'>","</span>");
echo br();
//PHONE SECTION.
echo form_label('<b>PHONE</b>', 'PHONE');
echo form_input(array('name' => 'phone', 'value' => set_value("phone",$info["phone"]),'class' =>'span' ));
echo form_error("phone","<span class='text-error'>","</span>");
echo br();
//EMAIL SECTION.
echo form_label('<b>EMAIL</b>', 'EMAIL');
echo form_input(array('name' => 'email', 'value' => set_value("email",$info["email"]),'class' =>'span' ));
echo form_error("email","<span class='text-error'>","</span>");
echo br();
//LAST NAME SECTION.
echo form_label('<b>LAST NAME</b>', 'LAST NAME');
echo form_input(array('name' => 'last_name', 'value' => set_value("last_name",$info["last_name"]),'class' =>'span' ));
echo form_error("last_name","<span class='text-error'>","</span>");
echo br();
//POSTAL CODE SECTION.
echo form_label('<b>POSTAL CODE</b>', 'POSTAL CODE');
echo form_input(array('name' => 'postal_code', 'value' => set_value("postal_code",$info["postal_code"]),'class' =>'span' ));
echo form_error("postal_code","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>