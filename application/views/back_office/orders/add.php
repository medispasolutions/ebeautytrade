<script>
$(document).ready(function(e) {

	});
	
	function cashRules(data){
	var  value=data.value;
	if(value=="checque"){
		$('#checque_details').show('fast');}else{
		$('#checque_details').hide('fast');	}}	
</script>

<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
            anchor('back_office/orders/'.$this->session->userdata("back_link"),'<b>List orders</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
            );
$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/orders/invoice/'.$id).'" target="_blank" class="btn btn-info top_btn">print / preview</a>');
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<div class="hundred pull-left">   
<br />
<label><b>Sales Order ID:</b> <?php echo $order_details['id_orders']; ?></label>
<label><b>Customer Name:</b> <?php echo $order_details['user']['name']; ?></label>
<label><b>Created Date:</b> <?php echo $order_details['created_date']; ?></label>
<label><b>Status:</b> <? 
if($order_details["status"] == 'paid' || $order_details["status"] == 'completed') {?>
<span class="label label-success"><?php echo $order_details["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $order_details["status"]; ?></span>
<?php }?></label>
<label><b>Sub Total:</b> <?php echo $order_details['total_price_currency'].' '.$order_details['currency']; ?></label>
<?php if($order_details['discount']>0){?>
<label><b>Discount:</b> <?php echo $order_details['discount'].' '.$order_details['currency']; ?></label>
<?php } ?>
<?php if($order_details['redeem_discount_miles_amount']>0){ ?>
<label><b>Redeem Miles Amount:</b> <?php echo $order_details['redeem_discount_miles_amount'].' '.$order_details['currency']; ?></label>
<?php } ?>

<?php if($order_details['shipping_charge_currency']>0){ ?>
<label><b>Shipping & Handling :</b><?php if($order_details['shipping_charge_currency']){
					echo $order_details['shipping_charge_currency'].' '.$order_details['currency'];
					}else{ echo "Free" ;}?></label>
<?php } ?>

<?php
$return_details=$this->admin_fct->return_details($order_details['id_orders'],$order_details['rand']);?>
 <?php if(!empty($return_details['amount_return'])){?>
<label style="color:#F00;"><b>Amount Return:</b> <?php echo $return_details['amount_return'].' '.$order_details['currency']; ?></label>
<?php } ?>

<label><b>Amount:</b> <?php echo $order_details['amount_currency'].' '.$order_details['currency']; ?></label>





  <?php if(isset($order_details['delivery']['branch_name'])){?>
<div class="row-fluid">
<div class="span5">
<fieldset>
<legend>Shipping Address</legend>
<?php $shipping=$order_details['delivery'];?>
                  <address>
     <?php if($shipping['pickup']==1){?>
                <div style="color:red;font-size:16; padding-bottom:5px;"> <?php echo lang('pickup');?></div>
                 <?php } ?>
				<?php if(!empty($shipping['branch_name'])){?>
				<?php echo $shipping['branch_name'];?>
				<br>
				<?php }?>

                  
                  <?php echo $shipping['name'];?> <br>
                  <?php if(!empty($shipping['company']) && $shipping['company']!=0){?>
                  <?php echo $shipping['company'];?> <br>
                  <?php }?>
                  <?php echo $shipping['street_one'];?> <br>
                  <?php if(!empty($shipping['street_two'])){?>
                  <?php echo $shipping['street_two'];?> <br>
                  <?php }?>
                  <?php echo $shipping['city'] ?>,<?php echo $shipping['state'] ?>
                  <?php if(!empty($shipping['postal_code']) && $shipping['postal_code']!=0){?>
                  ,<?php echo $shipping['postal_code'] ?>
                  <?php } ?>
                  <br>
                  <?php echo $shipping['country']['title'] ?> <br>
                  T: <?php echo $shipping['phone'] ?>
                  <?php if(!empty($shipping['fax']) && $shipping['fax']!=0){?>
                  <br> F: <?php echo $shipping['fax'] ?>
                  <?php } ?>
                  
                  <?php if(!empty($shipping['mobile']) && $shipping['mobile']!=0){?>
                 <br> M: <?php echo $shipping['mobile'] ?>
                  <?php } ?> 
                  
                  </address>
</fieldset>
</div>
<div class="span5">
<fieldset>
<legend>Shipping Charges</legend>

                  <address>
 <?php if($order_details['shipping_charge_currency']>0){
					echo $order_details['shipping_charge_currency'].' '.$order_details['currency'];
					}else{ echo "Free Shipping " ;}?>
      			</address>
</fieldset>
</div>
</div>
<?php } ?>
  <?php if(isset($order_details['billing']['branch_name'])){?>
<div class="row-fluid">
<div class="span5">
<fieldset>
<legend>Billing Address</legend>
 <?php $billing=$order_details['billing'];?>
                  <address>
                  <?php echo $billing['name'];?> <br>
                  <?php if(!empty($billing['company']) && $billing['company']!=0){?>
                  <?php echo $billing['company'];?> <br>
                  <?php }?>
                  <?php echo $billing['street_one'];?> <br>
                  <?php if(!empty($billing['street_two']) && $billing['street_two']!=0){?>
                  <?php echo $billing['street_two'];?> <br>
                  <?php }?>
                  <?php echo $billing['city'] ?>,<?php echo $billing['state'] ?>
                  <?php if(!empty($billing['postal_code']) && $billing['postal_code']!=0){?>
                  ,<?php echo $billing['postal_code'] ?>
                  <?php }?>
                  <br>
                  <?php echo $billing['country']['title'] ?> <br>
                  T: <?php echo $billing['phone'] ?> 
                  <?php if(!empty($billing['fax']) && $billing['fax']!=0){?>
                  <br> F: <?php echo $billing['fax'] ?>
                  <?php } ?>
                 <?php if(!empty($billing['mobile']) && $billing['mobile']!=0){?>
                 <br> M: <?php echo $billing['mobile'] ?>
                  <?php } ?>
                  
                  </address>
</fieldset>
</div>
<div class="span5">
<fieldset>
<legend>Payment Method</legend>

               <?php echo $this->ecommerce_model->getPaymentMethod($order_details['payment_method']);?>
</fieldset>
</div>
</div>
<?php } ?>
<?php 
$line_items_brands = $order_details['line_items_brands'];
foreach($line_items_brands as $line_items_brand){?>
<?php if((!empty($line_items_brand['id_brands']) || (empty($line_items_brand['id_brands']) && count($line_items_brands)>1))){?>
<label style="font-size:20px;color:#a0d9f4;"><?php echo $line_items_brand['deliver_by'];?></label>
<?php } ?>
<?php $line_items = $line_items_brand['line_items'];?>
<?php $i=0; if(!empty($line_items)){$i++;?>

<table class="table table-striped">
<tr>
    <th  width="5%">#</th>
    <th width="10%">REF</th>
    <th width="12%">BARCODE</th>
    <th width="40%">ITEM DESCRIPTION</th>
    <th  width="10%"> PRICE  <?php echo $order_details['currency'];?></th>
    <th width="8%">Qty</th>
    <th width="15%">SUB-TOTAL <?php echo $order_details['currency'];?> </th>
    
            
</tr>
<?php 
 
$net_price = 0;
foreach($line_items as $pro) {
	?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $pro['product']['sku']; ?></td>
<td><?php echo $pro['product']['barcode']; ?></td>
<td>
	<?php 
    $product_name = '';
	//if(empty($pro['product']['sku'])) 
	$product_name .= $pro['product']['title'];
	//else
	//$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
	if(!empty($pro['options'])) {
		$options = $pro['options_en'];
		$options = unSerializeStock($options);
		$product_name .= '<br />';
		$c = count($options);
		$i=0;
		foreach($options as $key => $opt) {
			$i++;
			$product_name .= $opt;
			if($i != $c) $product_name .= ' - ';
		}
	}
	echo $product_name;
	?>

    </td>
    

    <td><?php echo $pro['price_currency'].' '.$pro['currency']; ?></td>
    <td><?php echo $pro['quantity']; ?></td>
    <td><?php echo $pro['total_price_currency'].' '.$pro['currency']; ?></td>
</tr>
<?php }?>

</table>
<?php }?>
<?php $line_items = $line_items_brand['line_items_redeem_by_miles'];?>
<?php $i=0; if(!empty($line_items)){$i++;?>
<table class="table table-striped">
<tr>
    <th  width="5%">#</th>
    <th width="10%">REF</th>
    <th  width="12%">BARCODE</th>
    <th width="40%">ITEM(S) REDEEMED BY MILES</th>
    <th  width="10%"> MILES </th>
    <th width="8%">Qty</th>
    <th width="15%">SUB-TOTAL </th>
</tr>

<?php 


$net_price = 0;
foreach($line_items as $pro) {
	?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $pro['product']['sku']; ?></td>
<td><?php echo $pro['product']['barcode']; ?></td>
<td>
	<?php 
    $product_name = '';
	//if(empty($pro['product']['sku'])) 
	$product_name .= $pro['product']['title'];
	//else
	//$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
	if(!empty($pro['options'])) {
		$options = $pro['options_en'];
		$options = unSerializeStock($options);
		$product_name .= '<br />';
		$c = count($options);
		$i=0;
		foreach($options as $key => $opt) {
			$i++;
			$product_name .= $opt;
			if($i != $c) $product_name .= ' - ';
		}
	}
	echo $product_name;
	?>

    </td>
    

    <td>
	<?php if($pro['quantity']!=0){?>
	<?php echo $pro['redeem_miles']; ?>
    <?php }else{echo "0";} ?></td>
    <td><?php echo $pro['quantity']; ?></td>
    <td><?php echo $pro['redeem_miles']*$pro['quantity']; ?></td>
</tr>
<?php }?>

</table>
<?php } ?>
<?php } ?>

<?php if(!empty($order_details['redeem_discount_miles_amount'])) {?>
<label><b>Redeem Miles: </b> <?php echo $order_details['redeem_miles'].' miles'; ?></label>
<?php }?>

<?php if(!empty($line_items)) {?>
<label><b>Net Price:</b> <?php echo $order_details['amount_currency'].' '.$pro['currency']; ?></label>
<?php }?>
<?php //if($order_details['status'] != 'paid') {?>


<form id="SetPaidForm" action="<?php echo site_url('back_office/orders/updatestatus'); ?>" method="post">

<input type="hidden" name="id_orders" value="<?php echo $id; ?>" />
<input type="hidden" name="rand" value="<?php echo $info["rand"]; ?>" />
<div class="row-fluid">
    <div class="span5">

<?php 
echo br();
echo form_label('<b>COMMENTS </b>', 'COMMENTS');
echo form_textarea(array("name" => "comments", "value" => set_value("comments",$info["comments"]),"class" =>"input-xlarge", "style" => "height:100px", "id" => "comments", "rows" => 5, "cols" => 500 ));
echo br();
echo form_error("comments","<span class='text-error'>","</span>");

?>
    </div>
</div>


<div class="row-fluid">

<?php if($order_details['status']!="paid" && $order_details['status']!="completed"){ ?>
    <div class="span3" style="margin-left:0;">
        <label>
            <label><b>Status</b></label>
                <select name="status">
                <option value="unread" <?php if($order_details['status'] == 'unread') echo 'selected="selected"'; ?>>Unread</option>
                <option value="pending" <?php if($order_details['status'] == 'pending') echo 'selected="selected"'; ?>>Pending</option>
                <option value="prepared" <?php if($order_details['status'] == 'prepared') echo 'selected="selected"'; ?>>Prepared</option>
                <option value="completed" <?php if($order_details['status'] == 'completed') echo 'selected="selected"'; ?>>Completed</option>
                <option value="canceled" <?php if($order_details['status'] == 'canceled') echo 'selected="selected"'; ?>>Canceled</option>
                <option value="paid" <?php if($order_details['status'] == 'paid') echo 'selected="selected"'; ?>>Paid --legacy--</option>
            </select>
        </label>
    </div>
<?php } ?>

    <div class="span3">
        <label>
            <label><b>Payment method</b></label>
            <select name="payment_method">
                <option value="">-- select --</option>
                <option value="cheque-on-delivery" <?php if($order_details['payment_method'] == 'cheque-on-delivery') echo 'selected="selected"'; ?>>Cheque on delivery</option>
                <option value="cash-on-delivery" <?php if($order_details['payment_method'] == 'cash-on-delivery') echo 'selected="selected"'; ?>>Cash on delivery</option>
                <option value="later-payment" <?php if($order_details['payment_method'] == 'later-payment') echo 'selected="selected"'; ?>>Later payment</option>
            </select>
        </label>
    </div>

    <div class="span3">
        <label>
            <label><b>Payment status</b></label>
            <select name="payment_status">
                <option value="unpaid" <?php if($order_details['payment_status'] == 'unpaid') echo 'selected="selected"'; ?>>Unpaid</option>
                <option value="paid" <?php if($order_details['payment_status'] == 'paid') echo 'selected="selected"'; ?>>Paid</option>
            </select>
        </label>
    </div>
</div>

<div class="row-fluid">
<!--<div class="span3">
<?php if($order_details['shipping']==0){ ?>
<label>
<label>Shipping Charge (<?php echo $order_details['currency'];?>)</label>
<input type="text" value="<?php echo $order_details['customer_shipping_charge'];?>" name="customer_shipping_charge" />
</label>
<?php } ?>
</div>-->


<div class="span3">

<label>
<label><b>Shipping Charge (<?php echo $order_details['currency'];?>)</b></label>
<input type="text" value="<?php echo $order_details['shipping_charge_currency'];?>" name="shipping" />
</label>

</div>

<div class="span3">

<label>
<label><b>Discount(<?php echo $order_details['currency'];?>)</b></label>
<input type="text" value="<?php echo $order_details['discount'];?>" name="discount" />
</label>

</div>
</div>
<?php if($order_details['payment_method']=="cash" || $order_details['payment_method']=="checque"){?>

<div class="row-fluid">
<div class="span3"><label><label><b>Payment Method</b></label>
<?php $payment_methods=$this->ecommerce_model->getPaymentMethods();?>
<select name="payment_method" id="payment_method" style="text-transform:capitalize;" onchange="cashRules(this)">

<?php 
foreach($payment_methods as $val){
	if($val['payment_method']=="cash" || $val['payment_method']=="checque"){
?>
<option value="<?php echo $val['payment_method'];?>" <?php if($val['payment_method']==$order_details['payment_method']) echo "selected";?> > <?php echo $this->ecommerce_model->getPaymentMethod($val['payment_method']);?></option>
<?php } ?>
<?php } ?>
</select></label>
</div>

<div class="span9" id="checque_details" <?php if($order_details['payment_method']!="checque") echo "style='display:none'";?>>
<div class="span3">

<label>
<label><b>Bank</b></label>
<input type="text" value="<?php echo $order_details['bank'];?>" name="bank" />
</label>

</div>

<div class="span3">

<label>
<label><b>Number</b></label>
<input type="text" value="<?php echo $order_details['number'];?>" name="number" />
</label>

</div>
</div>
</div>


<?php } ?>

<fieldset>
<legend>Terms</legend>
<div class="row-fluid">
<div class="span5">
<label class="field-title">DELIVERY TIME:</label>
<label><input type="text" class="input-xlarge" name="delivery_time" value="<?= set_value('delivery_time',$info["delivery_time"]); ?>" >
</label>
</div>
<div class="span5">
<label class="field-title">DELIVERY FEE: </label>
<label><input type="text" class="input-xlarge" name="delivery_fee" value="<?= set_value('delivery_fee',$info["delivery_fee"]); ?>" >
</label>
</div>
</div>

<div class="row-fluid">
<div class="span5">
<label class="field-title">MIN. ORDER: </label>
<label><input type="text" class="input-xlarge" name="min_order" value="<?= set_value('min_order',$info["min_order"]); ?>" >
</label>
</div>
<div class="span5">
<label class="field-title">CREDIT: </label>
<label><input type="text" class="input-xlarge" name="credit" value="<?= set_value('credit',$info["credit"]); ?>" >
</label>
</div>
</div>



</fieldset>
<?php
echo br();
if(isset($info['printed']) && !isset($printed))
$printed = $info['printed'];
?>

<label><input type="checkbox" name="printed" <?php if(isset($printed) && $printed == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Printed</b></label>


<?php

if(isset($info['delivered']) && !isset($delivered))
$delivered = $info['delivered'];
?>

<label><input type="checkbox" name="delivered" <?php if(isset($delivered) && $delivered == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Delivered</b></label>


<?php
echo br();
if(isset($info['infrom_client']) && !isset($infrom_client))
$infrom_client = $info['infrom_client'];
?>

<label><input type="checkbox" name="inform_client" <?php if(isset($infrom_client) && $infrom_client == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Inform Client</b></label>


<input type="submit" value="Save Changes" class="btn btn-primary" id="SetPaid" />

</form>
 </div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>