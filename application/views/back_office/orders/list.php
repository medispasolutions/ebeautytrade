<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>

<script>
function delete_row(id){
window.location="<?= base_url(); ?>back_office/orders/delete/"+id;
return false;
}
$(document).ready(function(){
$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
function checkAddress(){
	var address=$('#address_autocomplete').val();

	if(address==""){ $('#id_address').val('');}}
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li class="active"><?= $title; ?></li>
<? if ($this->acl->has_permission('orders','add')){ ?>
<li class="pull-right">
<a href="<?= site_url('back_office/orders/add'); ?>" id="topLink" class="btn btn-info top_btn" title="">Add orders</a>
</li><? } ?>
</ul> 
</div>
<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">

<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
 <form method="get" action="<?=base_url()?>back_office/orders">
 <div class="row-fluid">
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Order ID<br /><input type="text" id="order_id" name="order_id" value="<? if(isset($_GET['order_id'])) { echo $_GET['order_id']; }?>" /></div>
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Customer First Name<br /><input type="text" id="customer_first_name" name="customer_first_name" value="<? if(isset($_GET['customer_first_name'])) { echo $_GET['customer_first_name']; }?>" /></div>

<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Customer Surname<br /><input type="text" id="customer_surname" name="customer_surname" value="<? if(isset($_GET['customer_surname'])) { echo $_GET['customer_surname']; }?>" /></div>

<!--Account Manager -->
<?php if(!$supplier){?>
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label><?php echo lang('account_manager');?></label>  
<?php 
;

$account_manager=$this->fct->getAll('account_manager','title');
?>

<select name="account_manager" id="account_manager">
<option value="">-Select <?php echo lang('account_manager');?>-</option>
<?php 


foreach($account_manager as $val){
$cl = '';
	if(isset($_GET['account_manager']) && $_GET['account_manager'] == $val['id_account_manager'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_account_manager'];?>"  <?php echo $cl;?>><?php echo $val['title'];?></option>
<?php } ?>
</select></div>
<?php } ?>


</div>
 <div class="row-fluid">
<!--Areas -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label><?php echo lang('areas');?></label>  
<?php 
$cond3=array();
$users_areas=$this->fct->getAll_cond('users_areas','title asc',$cond3);
?>

<select name="id_users_areas" id="users_areas">
<option value="">-Select <?php echo lang('area');?>-</option>
<?php 
foreach($users_areas as $val){
$cl = '';
	if(isset($_GET['id_users_areas']) && $_GET['id_users_areas'] == $val['id_users_areas'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_users_areas'];?>"  <?php echo $cl;?>><?php echo $val['title'];?></option>
<?php } ?>
</select></div>
<!--City -->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;" id="city"><label>City</label><input type="text" id="city" name="city" value="<? if(isset($_GET['city'])) { echo $_GET['city']; }?>" /></div>
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Read/Unread<br />

<select name="read" id="read">
<option value="">- All -</option>
<option value="unread" <?php if(isset($_GET['read']) && $_GET['read'] == "unread") echo 'selected="selected"'; ?>>Unread</option>
<option value="read" <?php if(isset($_GET['read']) && $_GET['read'] == "read") echo 'selected="selected"'; ?>>Read</option>
</select>
</div>

<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Address<br /><input type="text" id="address_autocomplete" onkeyup="checkAddress()" name="address" value="<? if(isset($_GET['address'])) { echo $_GET['address']; }?>" />
<input type="hidden" id="id_address" name="id_address" value="<? if(isset($_GET['id_address'])) { echo $_GET['id_address']; }?>" />
<div id="loader_1"></div></div>
</div>

<div class="row-fluid">
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Status<br />
<select name="status" id="status">
<option value="">- All -</option>
<option value="unread" <?php if(isset($_GET['status']) && $_GET['status'] == 'unread') { ?>selected="selected"<?php }?>>Unread</option>
<option value="pending" <?php if(isset($_GET['status']) && $_GET['status'] == 'pending') { ?>selected="selected"<?php }?>>Pending</option>
<option value="prepared" <?php if(isset($_GET['status']) && $_GET['status'] == 'prepared') { ?>selected="selected"<?php }?>>Prepared</option>
<option value="completed" <?php if(isset($_GET['status']) && $_GET['status'] == 'completed') { ?>selected="selected"<?php }?>>Completed</option>
<option value="canceled" <?php if(isset($_GET['status']) && $_GET['status'] == 'canceled') { ?>selected="selected"<?php }?>>Canceled</option>
<option value="paid" <?php if(isset($_GET['status']) && $_GET['status'] == 'paid') { ?>selected="selected"<?php }?>>Paid -- legacy --</option>
</select>
</div>



<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Payment Method<br />
<?php $payment_methods=$this->ecommerce_model->getPaymentMethods();?>
<select name="payment_method" id="payment_method" style="text-transform:capitalize;">
<option value="">- All -</option>
<?php 
foreach($payment_methods as $val){?>
<option value="<?php echo $val['payment_method'];?>" <?php if(isset($_GET['payment_method']) && $_GET['payment_method'] == $val['payment_method']) echo 'selected="selected"'; ?>> <?php echo $this->ecommerce_model->getPaymentMethod($val['payment_method']);?></option>
<?php } ?>
</select>
</div>

<!--DATE -->
<div class="fl" style="width: 360px; float:left;">
<div class="fl" style="width: 180px; float:left;">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['from_date'])) echo $_GET['from_date'] ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 180px; float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['to_date'])) echo $_GET['to_date'] ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>


<div class="fl" style="margin:21px 0 0 0; float:left"><input type="submit" class="btn btn-primary" value="Filter" />

<button type="submit" class="btn btn-primary  btn-danger" name="report_submit" value="export"><i class="icon-download"></i> Export</button>
</div>
</div>
<div class="fl" style="margin:0 15px 0 0;width: 110px; float:left;">

<label><input type="checkbox" name="printed" value="1" <?php if($this->input->get('printed')==1){ echo "checked" ;}?> /> <b>Printed</b></label>
<label><input type="checkbox" name="delivered" value="1" <?php if($this->input->get('delivered')==1){ echo "checked" ;}?> /> <b>Delivered</b></label>
</div>
</form>
              </div>
            </div>
          </div>
        </div>
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/orders/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="4" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th>ORDER ID</th>
<th>CREATED DATE</th>
<th>CUSTOMER NAME</th>
<th>AMOUNT</th>
<th>STATUS</th>

<th></th>
<th style="text-align:center;" width="250">ACTION</th></tr>
</thead><!--<tfoot><tr>
<td colspan="3">
<? if ($this->acl->has_permission('orders','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>-->
<tbody>
<? 
if(count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>
<tr id="<?=$val["id_orders"]; ?>">
<!--<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_orders"] ; ?>" /></td>-->
<td><? echo $val["id_orders"]; ?></td>
<td><? echo $val["created_date"]; ?></td>
<td><? echo $val["name"]; ?></td>
<td><? echo $val["amount_currency"] .' '.lang($val["currency"]); ?></td>
<td><? 
if($val["status"] == 'paid' || $val["status"] == 'completed') {?>
<span class="label label-success"><?php echo $val["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $val["status"]; ?></span>
<?php }?></td>

<td class="" width="" ><?php if($val["readed"] == 1) echo  '<span class="label label-success">Read</span>'; else echo '<span class="label label-important">Unread</span>'; ?></td>

<td style="text-align:center"><? if ($this->acl->has_permission('orders','index')){ ?>
<a href="<?= site_url('back_office/orders/view/'.$val["id_orders"].'/'.$val['rand']);?>" class="table-edit-link" title="View" >
<i class="icon-search" ></i> View</a> <span class="hidden"> | </span>
<? } ?>
<? if ($this->acl->has_permission('orders','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_orders"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="9" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<!--<form action="<?= site_url("back_office/orders"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>-->
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 