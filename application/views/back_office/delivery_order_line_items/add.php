<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/delivery_order_line_items/'.$this->session->userdata("back_link"),'<b>List delivery order line items</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/delivery_order_line_items/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/delivery_order_line_items/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["id_products"] = "";
$info["options"] = "";
$info["brands_title"] = "";
$info["id_orders"] = "";
$info["quantity"] = "";
$info["id_brands"] = "";
$info["combination"] = "";
$info["type"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();

//ID PRODUCTS SECTION.
echo form_label('<b>ID PRODUCTS</b>', 'ID PRODUCTS');
echo form_input(array('name' => 'id_products', 'value' => set_value("id_products",$info["id_products"]),'class' =>'span' ));
echo form_error("id_products","<span class='text-error'>","</span>");
echo br();
//OPTIONS SECTION.
echo form_label('<b>OPTIONS</b>', 'OPTIONS');
echo form_input(array('name' => 'options', 'value' => set_value("options",$info["options"]),'class' =>'span' ));
echo form_error("options","<span class='text-error'>","</span>");
echo br();
//BRANDS TITLE SECTION.
echo form_label('<b>BRANDS TITLE</b>', 'BRANDS TITLE');
echo form_input(array('name' => 'brands_title', 'value' => set_value("brands_title",$info["brands_title"]),'class' =>'span' ));
echo form_error("brands_title","<span class='text-error'>","</span>");
echo br();
//ID ORDERS SECTION.
echo form_label('<b>ID ORDERS</b>', 'ID ORDERS');
echo form_input(array('name' => 'id_orders', 'value' => set_value("id_orders",$info["id_orders"]),'class' =>'span' ));
echo form_error("id_orders","<span class='text-error'>","</span>");
echo br();
//QUANTITY SECTION.
echo form_label('<b>QUANTITY</b>', 'QUANTITY');
echo form_input(array('name' => 'quantity', 'value' => set_value("quantity",$info["quantity"]),'class' =>'span' ));
echo form_error("quantity","<span class='text-error'>","</span>");
echo br();
//ID BRANDS SECTION.
echo form_label('<b>ID BRANDS</b>', 'ID BRANDS');
echo form_input(array('name' => 'id_brands', 'value' => set_value("id_brands",$info["id_brands"]),'class' =>'span' ));
echo form_error("id_brands","<span class='text-error'>","</span>");
echo br();
//COMBINATION SECTION.
echo form_label('<b>COMBINATION</b>', 'COMBINATION');
echo form_input(array('name' => 'combination', 'value' => set_value("combination",$info["combination"]),'class' =>'span' ));
echo form_error("combination","<span class='text-error'>","</span>");
echo br();
//TYPE SECTION.
echo form_label('<b>TYPE</b>', 'TYPE');
echo form_input(array('name' => 'type', 'value' => set_value("type",$info["type"]),'class' =>'span' ));
echo form_error("type","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>