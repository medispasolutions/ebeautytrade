<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
<script src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate3.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>
</head>

<body>
<div class="container-fluid">
<div class="row-fluid">


<div class="span10-fluid" >
<!--<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/task'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/task/edit/'.$product['id_task']); ?>" >Edit <?php echo $product['title']; ?></a></li>
<span class="divider">/</span>
<li><a href="<?= site_url('back_office/task/purchases?id_product='.$product['id_task']); ?>" >List Of Purchases</a></li>
<span class="divider">/</span>
<li class="active">List of purchases</li>

</ul>-->
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms','id'=>'priceSegements');
echo form_open_multipart('back_office/task_comments/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["product"] = "";
$info["min_qty"] = "";
$info["id_task"] = "";
$info["price"] = "";
$info["discount"] = "";
$info["discount_expiration"] = "";
$info["image"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
//META DESCRIPTION SECTION.
echo form_label('<b>DESCRIPTION*&nbsp:</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("description".$lang,"<span class='text-error'>","</span>");
echo br();


/*//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();*/

//PRODUCT&nbsp;<em>*</em>: SECTION.
if($id_task != '') {
	echo form_hidden("id_task",$id_task);

}


echo br();
?>
         <div id="loader-bx"></div>
        <div class="FormResult"></div>
<?php
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>     
</div>
</div>

<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?></body>