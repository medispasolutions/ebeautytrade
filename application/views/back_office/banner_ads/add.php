<script type="text/javascript">
$(document).ready(function(){
	
$('#TypeChange').change(function(){
	var type = $(this).val();
	if(type == 'image') {
		$('.VideoBox').slideUp('fast');
		$('.ImageBox').slideDown('fast');
	}
	else if(type == 'video'){
		$('.ImageBox').slideUp('fast');
		$('.VideoBox').slideDown('fast');
	}
	else {
		$('.VideoBox').slideUp('fast');
		$('.ImageBox').slideUp('fast');
	}
});
	
});
</script>

<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
            anchor('back_office/banner_ads/'.$this->session->userdata("back_link"),'<b>List banner ads</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<div class="hundred pull-left">   
<?
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/banner_ads/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["user"] = "";$info["image"] = "";$info["pages"] = "";$info["positions"] = "";
$info["title"] = "";
$info["meta_title"] = "";
$info["type"] = "";
$info["video"] = "";
$info['categories']="";
$info['link']="";
$info['target']="";
$info["meta_description"] = "";
$info["meta_keywords"] = "";
$info["title_url"] = "";
$info["expiry_date"] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title", "value" => set_value("title",$info["title"]),"class" =>"span" ));
echo form_error("title","<span class='text-error'>","</span>");
echo br();

echo form_label('<b>Link&nbsp;<em>*</em>:</b>', 'Link');
echo form_input(array("name" => "link", "value" => set_value("link",$info["link"]),"class" =>"span" ));
echo form_error("link","<span class='text-error'>","</span>");
echo br();


//SET AS FEATURED SECTION.
echo form_label('<b>TARGET</b>', 'TARGET');
$options = array(
                  "" => " - select option - ",
                  "_blank" => "Blank",
                  "_new" => "New",
                );
echo form_dropdown("target", $options,set_value("target",$info["target"]), 'class="span"');
echo form_error("target","<span class='text-error'>","</span>");
echo br();

/*//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title", "value" => set_value("meta_title",$info["meta_title"]),"class" =>"span" ));
echo form_error("meta_title","<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url", "value" => set_value("title_url",$info["title_url"]),"class" =>"span" ));
echo form_error("title_url","<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description", "value" => set_value("meta_description",$info["meta_description"]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description","<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords", "value" => set_value("meta_keywords",$info["meta_keywords"]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords","<span class='text-error'>","</span>");
echo br();
*/


//EXPIRY DATE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>EXPIRY DATE&nbsp;<em>*</em>:</b>', 'EXPIRY DATE&nbsp;<em>*</em>:');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'expiry_date', 'value' => set_value("expiry_date",$this->fct->date_out_formate($info["expiry_date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("expiry_date","<span class='text-error'>","</span>");
echo br();


//USER SECTION.
echo form_label('<b>USER</b>', 'USER');
$items = $this->fct->getAll("user","username"); 
echo '<select  class="span" id="searchByCategory">';
echo '<option value="" > - select website_users - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_user"]; ?>" <? if(isset($id)){  if($info["id_user"] == $valll["id_user"]){ echo 'selected="selected"'; } } ?> ><?= $valll["username"]; ?> - <?= $valll["email"]; ?></option>
<?
}
echo "</select>";
echo form_error("user","<span class='text-error'>","</span>");
?>
<input type="hidden" id="searchByCategory_hidden" name="user" value="<?php echo $this->input->get("user"); ?>" />
<?php
echo br();
echo br();
//TYPE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>TYPE&nbsp;<em>*</em>:</b>', 'TYPE&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
                  'video' => "Video",
                  'image' => "Image",
                );
echo form_dropdown("type", $options,set_value("type",$info["type"]), 'class="span" id="TypeChange"');
echo form_error("type","<span class='text-error'>","</span>");
echo br();
//IMAGE SECTION.
?>
<div class="ImageBox" style=" <?php if(set_value('type',$info['type']) == 'image') {?>display:block<?php } else {?>display:none<?php }?>">
<?php echo form_label('<b>IMAGE</b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo anchor('uploads/banner_ads/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);
echo anchor("back_office/banner_ads/delete_image/image/".$info["image"]."/".$info["id_banner_ads"], img('images/delete.png'),array('class' => 'cur','onClick' => "return confirm('Are you sure you want to delete this image ?')" ));
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();?>
</div>
<div class="VideoBox" style=" <?php if(set_value('type',$info['type']) == 'video') {?>display:block<?php } else {?>display:none<?php }?>">
<?php
//TITLE SECTION.
echo form_label('<b>Video&nbsp;<em>*</em>:</b>', 'Video');
echo form_input(array("name" => "video", "value" => set_value("video",$info["video"]),"class" =>"span" ));
echo form_error("video","<span class='text-error'>","</span>");
echo br();
?>
</div>
<?php
/*//PAGES SECTION.
echo form_label('<b>PAGES</b>', 'PAGES');
echo form_input(array('name' => 'pages', 'value' => set_value("pages",$info["pages"]),'class' =>'span' ));
echo form_error("pages","<span class='text-error'>","</span>");
echo br();
//POSITIONS SECTION.
echo form_label('<b>POSITIONS</b>', 'POSITIONS');
echo form_input(array('name' => 'positions', 'value' => set_value("positions",$info["positions"]),'class' =>'span' ));
echo form_error("positions","<span class='text-error'>","</span>");
echo br();
echo br();*/

//BLOCKS SECTION.
echo form_label('<b>BLOCKS</b>', 'BLOCKS');

	echo form_label("Please choose the blocks where this banner should appear, in case the page do not include the ad block that you choose then it will not appear","help_text",array("class"=>"yellow"));
	$blocks = array(
		'menu_header_brand_ads'=>"Menu Dropdown Brand",
		'ads_steps'=>"Ads Steps (Width:1365px,Height:445px)",
		'videos_images_left_side'=>"Videos/Images Left Side (Width:200px)",
		'home_bottom_left'=>"Home Bottom Left (Width:130px,Height:445px)",
		'home_bottom_right'=>"Home Bottom Right (Width:130px,Height:445px)",
		'pages_top'=>"PagesTop (Width:960px,Height:160px)",
		'directory_left'=>"Directory left (Width:480px,Height:120px)",
		'directory_right'=>"Directory Right (Width:480px,Height:120px)",
		
		
	);
	//$selected_sections = array();
	if(!isset($selected_blocks)) {
		$selected_blocks = explode("|",$info['positions']);
	}
	echo form_multiselect('blocks[]', $blocks, $selected_blocks,'style="height:150px; width:100%"');
	//echo form_input(array('name' => 'sections', 'value' => set_value("sections",$info["sections"]),'class' =>'span' ));
echo form_error("blocks","<span class='text-error'>","</span>");
echo br();
echo br();

//SECTIONS SECTION.
echo form_label('<b>PAGES</b>', 'PAGES');

	echo form_label("Please choose the pages where this banner should appear, in case the page do not include the ad block that you choose then it will not appear","help_text",array("class"=>"yellow"));
	$pages = array(
     
	    'all'=>"All Pages",
		'home'=>"Home Page",
		'categories'=>"Categories",
		/*'directory_contactlist'=>"Directory(contactlist)",*/
		'directory'=>"Directory",
		'register'=>"Register(Width:590px,Height:768px)",
		
	);
	//$selected_sections = array();
	if(!isset($selected_pages)) {
		$selected_pages = explode("|",$info['pages']);
	}
	echo form_multiselect('pages[]', $pages, $selected_pages,'style="height:150px; width:100%"');
	//echo form_input(array('name' => 'sections', 'value' => set_value("sections",$info["sections"]),'class' =>'span' ));
echo form_error("pages","<span class='text-error'>","</span>");
echo br();
echo br();


//SECTIONS SECTION.
echo form_label('<b>CATEGORIES</b>', 'CATEGORIES');
$categories = $this->fct->getAll("categories","sort_order");
	echo form_label("Please choose the categories where this banner should appear, in case the page do not include the ad block that you choose then it will not appear","help_text",array("class"=>"yellow"));
	$options_cats = array();
	foreach($categories as $cat) {
		$options_cats[$cat['title']] = $cat['title'];
	}
	/*$pages = array(
		'home'=>"Home Page",
		'Categories'=>"Categories",
		'inside_ad'=>"Ad Page",
	);*/
	//$selected_sections = array();
	if(!isset($selected_categories)) {
		$selected_categories = explode("|",$info['categories']);
	}
	echo form_multiselect('categories[]', $options_cats, $selected_categories,'style="height:150px; width:100%"');
	//echo form_input(array('name' => 'sections', 'value' => set_value("sections",$info["sections"]),'class' =>'span' ));
echo form_error("categories","<span class='text-error'>","</span>");
echo br();
echo br();

if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>