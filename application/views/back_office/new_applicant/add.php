<?php
$lang = "";
if ($this->config->item("language_module")) {
    $lang = getFieldLanguage($this->lang->lang());
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#RoleChange').change(function () {
            var val = $(this).val();
            if (($('#trading_name').is(":visible") && $(this).prop('checked') == false) || (val != 5 && val != 4)) {
                $('#trading_name').stop(true, true).slideUp('fast');
            } else {
                $('#trading_name').stop(true, true).slideDown('fast');
            }
        });
    });
    $(function () {
        $('input[name=variable]:radio').change(function () {
            var val = $(this).val();
            if (val == "exist") {
                $('#db_names').css('display', 'block');
            } else {
                $('#db_names').css('display', 'none');
            }
        });
    });
</script>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <? $this->load->view('back_office/includes/left_box'); ?>
        </div>

        <div class="span10 cont_h">
            <div class="span10-fluid">
                <ul class="breadcrumb">
                    <li><a href="<?= site_url('back_office/new_applicant'); ?>">List All Applicant</a> <span class="divider">/</span>
                    </li>
                    <li><?= $title; ?></li>
                </ul>
            </div>
            <div class="hundred pull-left">
                <form action="<?= site_url('back_office/new_applicant/submit'); ?>" method="post" class="middle-forms"
                      enctype="multipart/form-data">
                    <p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em>
                        <?
                        $countries = $this->fct->getAll('countries', 'title');
                        if (isset($id)) {
                            echo '<input type="hidden" name="id" value="' . $id . '" />';
                        } else {
                            $info['name'] = "";
                            $info['email'] = "";
                            $info['id_account_manager'] = "";
                            $info['address'] = "";
                            $info['username'] = "";
                            $info['password'] = "";
                            $info['id_roles'] = "";
                            $info['status'] = "";
                            $info['license_number'] = "";
                            $info['id_discount_groups'] = "";
                            $info['id_users_areas'] = "";
                            $info['company_name'] = "";
                            $info['id_countries'] = "";
                            $info['state'] = "";
                            $info['comments'] = "";
                            $info['city'] = "";
                            $info['trading_name'] = "";
                            $info['trading_licence'] = "";
                            $info['address_1'] = "";
                            $info['address_2'] = "";
                            $info['salutation'] = "";
                            $info['banner_image'] = "";
                            if (!isset($info['mobile'])) {
                                $info['mobile'] = "";
                            }
                            if (!isset($info['fax'])) {
                                $info['fax'] = "";
                            }

                            if (!isset($info['phone'])) {
                                $info['phone'] = "";
                            }

                            $info['available_balance'] = "";
                            $info['type'] = "";
                            $info['miles'] = "";
                            $info['position'] = "";
                            $info['first_name'] = "";
                            $info['last_name'] = "";
                            $info['gender'] = "";
                            $info['id_brands'] = "";
                            $info['guest'] = "";
                            $info['id_supplier_info'] = "";
                            $info["completed"] = 0;
                        }
                        $info['thumb'] = "";
                        $info['thumb'] = "";
                        ?>

                        <? if ($this->session->userdata('success_message')){ ?>
                    <div class="alert alert-success">
                        <?= $this->session->userdata('success_message'); ?>
                    </div>
                    <? } ?>
                    <? if ($this->session->userdata('error_message')) { ?>
                        <div class="alert alert-error">
                            <?= $this->session->userdata('error_message'); ?>
                        </div>
                    <? } ?>
                    </p>
                    <fieldset>
                        <?php if (isset($id)) {?>
                            <input type="hidden" name="id" value="<?php echo $id;?>"/>
                        <?php } ?>

                        <?php echo form_fieldset("Contact Person"); ?>
                        <?php if($info["application_type"] ==  1){ ?>
                            <h4>Application form related to a UAE supplier</h4>
                            <?php $currency = "AED"; ?>
                        <?php } else { ?>
                            <h4>Application form related to an overseas supplier</h4>
                            <?php $currency = "USD"; ?>
                        <?php } ?>
                        
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">First Name<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="first_name"
                                              value="<?= set_value('first_name', $info["first_name"]); ?>"/>
                                    <?= form_error('first_name', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Last Name<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="last_name"
                                              value="<?= set_value('last_name', $info["last_name"]); ?>"/>
                                    <?= form_error('last_name', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Position<em>*</em></label>
                                <label>
                                    <input type="text" class="input-xxlarge" name="position"
                                           value="<?= set_value('position', $info["position"]); ?>"/>
                                    <?= form_error('position', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Email<em>*</em></label>
                                <label>
                                    <input type="text" class="input-xlarge" name="email" value="<?= set_value('email', $info["email"]); ?>"/>
                                    <?= form_error('email', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Phone<em>*</em></label>
                                <label>
                                    <input type="text" class="input-xlarge" name="phone" value="<?= set_value('phone', $info["phone"]); ?>"/>
                                    <?= form_error('phone', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <?php echo form_fieldset_close(); ?>
                        <?php echo form_fieldset("Company details"); ?>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Local Lisence<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="random_number" value="<?= set_value('random_number', $info["random_number"]); ?>"/>
                                        <?= form_error('random_number', '<span class="text-error">', '</span>'); ?>
                                    </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Company Legal Name<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="company_name" value="<?= set_value('company_name', $info["company_name"]); ?>"/>
                                        <?= form_error('company_name', '<span class="text-error">', '</span>'); ?>
                                    </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Company Phone Number<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="company_number" value="<?= set_value('company_number', $info["company_number"]); ?>"/>
                                    <?= form_error('company_number', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">License or Tax Registration Number<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="registration_number" value="<?= set_value('registration_number', $info["registration_number"]); ?>"/>
                                        <?= form_error('registration_number', '<span class="text-error">', '</span>'); ?>
                                    </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Where is, your business based?<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="business_based" value="<?= set_value('business_based', $info["business_based"]); ?>"/>
                                    <?= form_error('business_based', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">City<em>*</em></label>
                                <label><input type="text" class="input-xlarge" name="city" value="<?= set_value('city', $info["city"]); ?>"/>
                                    <?= form_error('city', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                            <div class="span5">
                                <label class="field-title">Website</label>
                                <label><input type="text" class="input-xlarge" name="website" value="<?= set_value('website', $info["website"]); ?>"/>
                                    <?= form_error('website', '<span class="text-error">', '</span>'); ?>
                                </label>
                            </div>
                        </div>
                        
                        <?php echo form_fieldset_close(); ?>
                        <?php echo form_fieldset("Brand Details");

                            $this->db->where('application_brand.random_number', $info["random_number"]);
                            $this->db->join('application_brand', 'application_brand.random_number = supplier_application.random_number', 'left');
                            $query = $this->db->get('supplier_application');
                            //print_r($query);
                            $i = 0;
                            foreach ($query->result() as $row){
                                $i = $i + 1; 
                                echo "<h5>Brand Details ".$i."</h5>";
                        ?>
                        <div class="row-fluid">
                            <div class="span4">
                                <label class="field-title">Brand Name<em>*</em></label>
                                <label>
                                    <input type="text" class="input-xlarge" name="brand_name" value="<?= $row->brand_name; ?>"/>
                                </label>
                            </div>
                            <div class="span4">
                                <label class="field-title">Brand Country<em>*</em></label>
                                <label>
                                    <input type="text" class="input-xlarge" name="country" value="<?= $row->country; ?>"/>
                                </label>
                            </div>
                            <div class="span4">
                                <label class="field-title">Brand Website</label>
                                <label>
                                    <input type="text" class="input-xlarge" name="brand_website" value="<?= $row->brand_website; ?>"/>
                                </label>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Special requirements</label>
                                <label>
                                    <input type="text" class="input-xlarge" name="brand_description" value="<?= set_value('brand_description', $info["brand_description"]); ?>"/>
                                    <label>
                                        <?= form_error('brand_description', '<span class="text-error">', '</span>'); ?>
                                    </label>
                            </div>
                        </div>
                        
                        <?php echo form_fieldset_close(); ?>
                        

                        <?php echo form_fieldset("Select Your plan"); ?>
                        <?php if($info["plan_selected"] == 0){ ?>                        
                        <div class="row-fluid">
                            <div class="span5">
                                <label class="field-title">Plan selected<em>*</em>:</label>
                                <label>
                                    <?php
                                    $landingPlan = $info["plan_selected"];
                                        if($landingPlan == "free1"){
                                            $type = "UAE Free Plan";
                                            $BrandNumber = "1 Brand";
                                        } else if($landingPlan == "free2"){
                                            $type = "UAE Free Plan";
                                            $BrandNumber = "Up to 3 Brands";
                                        } else if($landingPlan == "free3"){
                                            $type = "UAE Free Plan";
                                            $BrandNumber = "Up to 6 Brands ";
                                        } else if($landingPlan == "free4"){
                                            $type = "UAE Free Plan";
                                            $BrandNumber = "Unlimited Brands";
                                        } else if($landingPlan == "gold1"){
                                            $type = "UAE Gold Plan";
                                            $BrandNumber = "1 Brand";
                                        } else if($landingPlan == "gold2"){
                                            $type = "UAE Gold Plan";
                                            $BrandNumber = "Up to 3 Brands";
                                        } else if($landingPlan == "gold3"){
                                            $type = "UAE Gold Plan";
                                            $BrandNumber = "Up to 6 Brands ";
                                        } else if($landingPlan == "gold4"){
                                            $type = "UAE Gold Plan";
                                            $BrandNumber = "Unlimited Brands";
                                        } else if($landingPlan == "fbe1"){
                                            $type = "UAE FBE Plan";
                                            $BrandNumber = "1 Brand";
                                        } else if($landingPlan == "fbe2"){
                                            $type = "UAE FBE Plan";
                                            $BrandNumber = "Up to 3 Brands";
                                        } else if($landingPlan == "fbe3"){
                                            $type = "UAE FBE Plan";
                                            $BrandNumber = "Up to 6 Brands ";
                                        } else if($landingPlan == "fbe4"){
                                            $type = "UAE FBE Plan";
                                            $BrandNumber = "Unlimited Brands";
                                        } else if($landingPlan == "free1-overseas"){
                                            $type = "Overseas Free Plan";
                                            $BrandNumber = "1 Brand";
                                        } else if($landingPlan == "free2-overseas"){
                                            $type = "Overseas Free Plan";
                                            $BrandNumber = "2 Brand";
                                        } else if($landingPlan == "free3-overseas"){
                                            $type = "Overseas Free Plan";
                                            $BrandNumber = "3 Brand";
                                        } else if($landingPlan == "gold1-overseas"){
                                            $type = "Overseas Gold Plan";
                                            $BrandNumber = "1 Brand";
                                        } else if($landingPlan == "gold2-overseas"){
                                            $type = "Overseas Gold Plan";
                                            $BrandNumber = "2 Brand";
                                        } else if($landingPlan == "gold3-overseas"){
                                            $type = "Overseas Gold Plan";
                                            $BrandNumber = "3 Brand";
                                        } else if($landingPlan == "fbe1-overseas"){
                                            $type = "Overseas FBE Plan";
                                            $BrandNumber = "1 Brand";
                                        } else if($landingPlan == "fbe2-overseas"){
                                            $type = "Overseas FBE Plan";
                                            $BrandNumber = "2 Brand";
                                        } else if($landingPlan == "fbe3-overseas"){
                                            $type = "Overseas FBE Plan";
                                            $BrandNumber = "3 Brand";
                                        }
                                    ?>
                                    
                                <p><?php echo $type ?> <br/> <?php echo $BrandNumber?></p>
                                <p>Amount: <?php echo $info["amount"];?> <sup><?php echo $currency; ?></sup></p>
                                
                            </div>
                        </div>
                        <?php } else { ?>
                            <span class="label label-important">Incomplete</span>
                        <?php } ?>
                        
                        <?php echo form_fieldset_close(); ?>

                        <br/>
                        <?php echo form_fieldset(" "); ?>

                        <?php if (isset($id)) { ?>
                            <label><input type="checkbox" name="inform_client" value="1" style="margin-top:0"/>&nbsp;&nbsp;Inform
                                Supplier</label>
                            <?php /*?><?php }else{ ?><?php */ ?><?php } ?>
                        <?php
                        if (isset($info['pre_registration']) && !isset($pre_registration))
                            $pre_registration = $info['pre_registration'];
                        ?>

                        <label><input type="checkbox"
                                      name="pre_registration" <?php if (isset($pre_registration) && $pre_registration == 1) { ?> checked="checked"<?php } ?>
                                      value="1" style="margin:0"/>&nbsp;&nbsp;Set as pre-registration</label>
                    </fieldset>
                    <p class="pull-right">
                        <input type="submit" name="submit" value="Save Changes" class="btn btn-primary"/>
                    </p>
                </form>
            </div>
        </div><!-- end of div#mid-col -->

        <!-- end of div#right-col -->
        <span class="clearFix">&nbsp;</span>
    </div>
    <? $this->session->unset_userdata('success_message'); ?>
    <? $this->session->unset_userdata('error_message'); ?>

    <script>
        $(document).ready(function (e) {
            var baseurl = $('#baseurl').val();
            var max_fields = 50; //maximum input boxes allowed
            var wrapper = $(".input_fields_wrap2"); //Fields wrapper
            var add_button = $(".add_field_button2"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function (e) {
                var cc = $('.remove_d').length;
                var rand = Math.floor((Math.random() * 1000) + 1) + "a";//on add input button click
                e.preventDefault();
                if (x < max_fields) { //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div><input  type="hidden" name="trading_license[]" value="' + rand + '" ><input class="textbox input-file" type="file" name="trading_license_' + rand + '"  id="trade_' + rand + '"><a class="remove_field remove_d" id="remove_' + rand + '" alt="Delete" tiltle="Delete">Remove</a></div>'); //add input box
                }
                if (cc > 4) {
                    add_button.hide();
                }
            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                var id = $(this).attr('id').replace('remove_', '');
                var cc = $('.remove_d').length;
                $('#trade_' + id).parent('div').remove();
                x--;
                if (cc > 4) {
                } else {
                    add_button.show();
                }
            })
        });
    </script>
</div>