<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/users_credits/'.$this->session->userdata("back_link"),'<b>List users petty cash</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/users_credits/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/users_credits/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["id_user"] = "";
$info["fax"] = "";
$info["payment_date"] = "";
$info["gateway_response_id"] = "";
$info["default_currency"] = "";
$info["currency"] = "";
$info["amount_default_currency"] = "";
$info["id_countries"] = "";
$info["postal_code"] = "";
$info["telephone"] = "";
$info["status"] = "";
$info["first_name"] = "";
$info["last_name"] = "";
$info["amount"] = "";
$info["email"] = "";
$info["street_one"] = "";
$info["street_two"] = "";
$info["city"] = "";
$info["state"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();

//ID USER SECTION.
echo form_label('<b>ID USER</b>', 'ID USER');
echo form_input(array('name' => 'id_user', 'value' => set_value("id_user",$info["id_user"]),'class' =>'span' ));
echo form_error("id_user","<span class='text-error'>","</span>");
echo br();
//FAX SECTION.
echo form_label('<b>FAX</b>', 'FAX');
echo form_input(array('name' => 'fax', 'value' => set_value("fax",$info["fax"]),'class' =>'span' ));
echo form_error("fax","<span class='text-error'>","</span>");
echo br();
//PAYMENT DATE SECTION.
echo form_label('<b>PAYMENT DATE</b>', 'PAYMENT DATE');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'payment_date', 'value' => set_value("payment_date",$this->fct->date_out_formate($info["payment_date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("payment_date","<span class='text-error'>","</span>");
echo br();
//GATEWAY RESPONSE ID SECTION.
echo form_label('<b>GATEWAY RESPONSE ID</b>', 'GATEWAY RESPONSE ID');
echo form_input(array('name' => 'gateway_response_id', 'value' => set_value("gateway_response_id",$info["gateway_response_id"]),'class' =>'span' ));
echo form_error("gateway_response_id","<span class='text-error'>","</span>");
echo br();
//DEFAULT CURRENCY SECTION.
echo form_label('<b>DEFAULT CURRENCY</b>', 'DEFAULT CURRENCY');
echo form_input(array('name' => 'default_currency', 'value' => set_value("default_currency",$info["default_currency"]),'class' =>'span' ));
echo form_error("default_currency","<span class='text-error'>","</span>");
echo br();
//CURRENCY SECTION.
echo form_label('<b>CURRENCY</b>', 'CURRENCY');
echo form_input(array('name' => 'currency', 'value' => set_value("currency",$info["currency"]),'class' =>'span' ));
echo form_error("currency","<span class='text-error'>","</span>");
echo br();
//AMOUNT DEFAULT CURRENCY SECTION.
echo form_label('<b>AMOUNT DEFAULT CURRENCY</b>', 'AMOUNT DEFAULT CURRENCY');
echo form_input(array('name' => 'amount_default_currency', 'value' => set_value("amount_default_currency",$info["amount_default_currency"]),'class' =>'span' ));
echo form_error("amount_default_currency","<span class='text-error'>","</span>");
echo br();
//ID COUNTRIES SECTION.
echo form_label('<b>ID COUNTRIES</b>', 'ID COUNTRIES');
echo form_input(array('name' => 'id_countries', 'value' => set_value("id_countries",$info["id_countries"]),'class' =>'span' ));
echo form_error("id_countries","<span class='text-error'>","</span>");
echo br();
//POSTAL CODE SECTION.
echo form_label('<b>POSTAL CODE</b>', 'POSTAL CODE');
echo form_input(array('name' => 'postal_code', 'value' => set_value("postal_code",$info["postal_code"]),'class' =>'span' ));
echo form_error("postal_code","<span class='text-error'>","</span>");
echo br();
//TELEPHONE SECTION.
echo form_label('<b>TELEPHONE</b>', 'TELEPHONE');
echo form_input(array('name' => 'telephone', 'value' => set_value("telephone",$info["telephone"]),'class' =>'span' ));
echo form_error("telephone","<span class='text-error'>","</span>");
echo br();
//STATUS SECTION.
echo form_label('<b>STATUS</b>', 'STATUS');
echo form_input(array('name' => 'status', 'value' => set_value("status",$info["status"]),'class' =>'span' ));
echo form_error("status","<span class='text-error'>","</span>");
echo br();
//FIRST NAME SECTION.
echo form_label('<b>FIRST NAME</b>', 'FIRST NAME');
echo form_input(array('name' => 'first_name', 'value' => set_value("first_name",$info["first_name"]),'class' =>'span' ));
echo form_error("first_name","<span class='text-error'>","</span>");
echo br();
//LAST NAME SECTION.
echo form_label('<b>LAST NAME</b>', 'LAST NAME');
echo form_input(array('name' => 'last_name', 'value' => set_value("last_name",$info["last_name"]),'class' =>'span' ));
echo form_error("last_name","<span class='text-error'>","</span>");
echo br();
//AMOUNT SECTION.
echo form_label('<b>AMOUNT</b>', 'AMOUNT');
echo form_input(array('name' => 'amount', 'value' => set_value("amount",$info["amount"]),'class' =>'span' ));
echo form_error("amount","<span class='text-error'>","</span>");
echo br();
//EMAIL SECTION.
echo form_label('<b>EMAIL</b>', 'EMAIL');
echo form_input(array('name' => 'email', 'value' => set_value("email",$info["email"]),'class' =>'span' ));
echo form_error("email","<span class='text-error'>","</span>");
echo br();
//STREET ONE SECTION.
echo form_label('<b>STREET ONE</b>', 'STREET ONE');
echo form_input(array('name' => 'street_one', 'value' => set_value("street_one",$info["street_one"]),'class' =>'span' ));
echo form_error("street_one","<span class='text-error'>","</span>");
echo br();
//STREET TWO SECTION.
echo form_label('<b>STREET TWO</b>', 'STREET TWO');
echo form_input(array('name' => 'street_two', 'value' => set_value("street_two",$info["street_two"]),'class' =>'span' ));
echo form_error("street_two","<span class='text-error'>","</span>");
echo br();
//CITY SECTION.
echo form_label('<b>CITY</b>', 'CITY');
echo form_input(array('name' => 'city', 'value' => set_value("city",$info["city"]),'class' =>'span' ));
echo form_error("city","<span class='text-error'>","</span>");
echo br();
//STATE SECTION.
echo form_label('<b>STATE</b>', 'STATE');
echo form_input(array('name' => 'state', 'value' => set_value("state",$info["state"]),'class' =>'span' ));
echo form_error("state","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>