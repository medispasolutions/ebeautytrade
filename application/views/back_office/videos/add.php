<script type="text/javascript">
$(document).ready(function(){
	$('#TypeChange').change(function(){
		var type = $(this).val();
		if(type == 'video') {
			$('#VimeoBlock').slideUp('fast');
			$('#YoutubeBlock').slideUp('fast');
			$('#VideoBlock').slideDown('fast');
		}
		else if(type == 'youtube') {
			$('#VideoBlock').slideUp('fast');
			$('#VimeoBlock').slideUp('fast');
			$('#YoutubeBlock').slideDown('fast');
		}
		else if(type == 'vimeo') {
			$('#VideoBlock').slideUp('fast');
			$('#YoutubeBlock').slideUp('fast');
			$('#VimeoBlock').slideDown('fast');
		}
		else {
			$('#VideoBlock').slideUp('fast');
			$('#YoutubeBlock').slideUp('fast');
			$('#VimeoBlock').slideUp('fast');
		}
	});
});
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/videos/'.$this->session->userdata("back_link"),'<b>List videos</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
			if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/videos/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/videos/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["type"] = "";
$info["youtube"] = "";
$info["vimeo"] = "";
$info["video_file"] = "";
$info["image"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
$info["lang"] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");

if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//TYPE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>TYPE&nbsp;<em>*</em>:</b> (not translated)', 'TYPE&nbsp;<em>*</em>:');
$options = array(
                  "" => " - select option - ",
                  'video' => "Custom Video",
                  'youtube' => "Youtube",
				  'vimeo' => "Vimeo",
                );
echo form_dropdown("type", $options,set_value("type",$info["type"]), 'class="span" id="TypeChange"');
echo form_error("type","<span class='text-error'>","</span>");
echo br();
?>
<div id="YoutubeBlock" style=" <?php if(set_select('type') == 'youtube' || (isset($info['type']) && $info['type'] == 'youtube')) {?>display:block<?php } else {?>display:none<?php }?>">
<?php
//YOUTUBE SECTION.
echo form_label('<b>YOUTUBE</b> (ex: http://www.youtube.com/watch?v=jALUW3EqjgE) (not translated)', 'YOUTUBE');
echo form_input(array('name' => 'youtube', 'value' => set_value("youtube",$info["youtube"]),'class' =>'span' ));
echo form_error("youtube","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div id="VimeoBlock" style=" <?php if(set_select('type') == 'vimeo' || (isset($info['type']) && $info['type'] == 'vimeo')) {?>display:block<?php } else {?>display:none<?php }?>">
<?php
//VIMEO SECTION.
echo form_label('<b>VIMEO</b> (ex: http://vimeo.com/94971722) (not translated)', 'VIMEO');
echo form_input(array('name' => 'vimeo', 'value' => set_value("vimeo",$info["vimeo"]),'class' =>'span' ));
echo form_error("vimeo","<span class='text-error'>","</span>");
echo br();
?>
</div>
<?php //echo 'test: '.set_select('type',$info['type']); ?>
<div id="VideoBlock" style=" <?php if(set_select('type') == 'video' || (isset($info['type']) && $info['type'] == 'video')) {?>display:block<?php } else {?>display:none<?php }?>">
<?php
//VIDEO SECTION.
echo form_label('<b>VIDEO</b> (upload from local machine, should be mp4 format) (not translated)', 'VIDEO');
echo form_upload(array("name" => "video_file", "class" => "input-large"));
echo "<span >";
if($info["video_file"] != ""){ 
echo '<span id="video_file_'.$info["id_videos"].'">'.anchor('uploads/videos/'.$info["video_file"],'show file',array("class" => 'blue','target'=>'_blank'));
echo nbs(3);
echo '<a class=\'cur\' onclick=\'removeFile("videos","video_file","'.$info["video_file"].'",'.$info["id_videos"].')\'><img src="'.base_url().'images/delete.png" /></a>'.'</span>';
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo form_error("video_file","<span class='text-error'>","</span>");
echo form_hidden('video_validate',1);
echo br();
echo br();
?>
</div>
<?php
//IMAGE SECTION.
echo form_label('<b>IMAGE</b> (width:368px; height:206px) (not translated)', 'IMAGE');
echo form_label('This field will be the default image for Youtube and Vimeo types, ex: if you uploaded an image and the type of the video is Youtube the system will display this image instead of Youtube default one.', '',array('class'=>'yellow'));
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_videos"].'">'.anchor('uploads/videos/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);
echo '<a class=\'cur\' onclick=\'removeFile("videos","image","'.$info["image"].'",'.$info["id_videos"].')\'><img src="'.base_url().'images/delete.png" /></a>'.'</span>';
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>