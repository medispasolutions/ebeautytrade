<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
function delete_row(id){
window.location="<?= base_url(); ?>back_office/products/delete_attribute/"+id;
return false;
}

$(function(){
/*$("#table-1").tableDnD({
onDrop: function(table, row) {
var ser=$.tableDnD.serialize();
$("#result").load("<?= base_url(); ?>back_office/product_attributes/sorted?"+ser);
}
});*/

$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li><span class="divider">/</span>
<li class="active">Attributes</li>
<? if ($this->acl->has_permission('product_attributes','add')){ ?>
<?php if(!empty($selected_product_attributes) || count($product_attributes) < 2) {?>
<li class="pull-right">
<a href="<?= site_url('back_office/products/add_attribute?id_product='.$product['id_products']); ?>" id="topLink" class="btn btn-info top_btn" title="">Add Attribute</a>
</li><? } ?><? } ?>
<!--<li class="pull-right">
<a href="<?= site_url('back_office/control/add_photos/products/'.$product['id_products']); ?>" id="topLink" class="btn btn-info top_btn" title="">Manage Gallery</a>
</li>
<li class="pull-right">
<a href="<?= site_url('back_office/translation/section/products/'.$product['id_products']); ?>" id="topLink" class="btn btn-info top_btn" title="">Translate</a>
</li>
-->
<li class="pull-right">
<a href="<?= site_url('back_office/products/stock/'.$product['id_products']); ?>" id="topLink" class="btn btn-info top_btn" title="">Stock</a>
</li>



<div class="span10-fluid" >

</div>


</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/product_attributes/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="3" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th>
TITLE
</th><th style="text-align:center;" width="250">ACTION</th></tr>
</thead>
<tbody>
<? 
if(count($product_attributes) > 0){
$i=0;
foreach($product_attributes as $val){
$i++; 
?>
<tr id="<?=$val["id_product_attributes"]; ?>">
<td class="title_search">
<? echo $val["title"]; ?>
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('product_options','index')){ ?>
<!--<a href="<?= site_url('back_office/products/options');?>?id_product=<?php echo $product['id_products']; ?>&id_attribute=<?php echo $val['id_attributes']; ?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> View Options</a> <span class="hidden"> | </span>-->
<? } ?>
<? if ($this->acl->has_permission('product_attributes','edit')){ ?>
<!--<a href="<?= site_url('back_office/products/edit_attribute/'.$val["id_product_attributes"]);?>?id_product=<?php echo $product['id_products']; ?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>-->
<? } ?>
<? if ($this->acl->has_permission('product_attributes','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_product_attributes"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="3" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 