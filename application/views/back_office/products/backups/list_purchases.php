<script type="text/javascript" src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script>
function delete_row(id){
window.location="<?= site_url("back_office/products/delete_purchases"); ?>/"+id;
return false;
}


			
	
$(function(){
/*$("#table-1").tableDnD({
onDrop: function(table, row) {
var ser=$.tableDnD.serialize();
$("#result").load("<?= site_url("back_office/purchases/sorted"); ?>"+"?"+ser);
}
});*/

/*$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});*/

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="0";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recipes of: <?=$chef['name']?></title>
<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
<script src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script>
function delete_row(id){
window.location="<?= site_url("back_office/products/delete_stock_price_segment"); ?>/"+id+"/<?php echo $product['id_products']; ?>/<?php echo $id_stock; ?>";
return false;
}
</script>
</head>
<body>
<div class="container-fluid">
<div class="row-fluid">
<?php $default_currency=$this->config->item('default_currency');?>


<div class="span10-fluid" >
<ul class="breadcrumb" style="background-color:inherit;">
<!--<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li><span class="divider">/</span>

<li class="active">List of purchases</li>-->
<? if ($this->acl->has_permission('purchases','add')){ ?>
<?php if(!empty($product)) {?>
<li class="pull-right">
<?php if($type=="product") {
	$link=site_url('back_office/products/add_purchases?id_product='.$product['id_products']);}else{
	$link=site_url('back_office/products/add_purchases?id_stock='.$product['id_products_stock']);	}?>
<a href="<?= $link; ?>" id="topLink" class="btn btn-info top_btn" title="">Add Purchases</a>
</li><? } ?><? } ?>
</ul>
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/purchases/delete_all', $attributes); 
?>  		
<table class="table table-striped" >
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="8" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="8" align="center" style="text-align:center">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<td width="2%">&nbsp;</td>

<th>Quantity</th>
<th>Price(<?php echo $default_currency;?>)</th>
<th>Cost(<?php echo $default_currency;?>)</th>
<th>Status</th>
<th style="text-align:center;" width="250">ACTION</th></tr>

</thead><tfoot><tr>
<td class="col-chk"></td>
<td colspan="7"></td>
</tr>
</tfoot>
<tbody>
<? 
$total_quantity=0;
$total_price=0;
$total_cost=0;
if(count($info) > 0){
$i=0;
foreach($info as $val){
	$total_quantity=$total_quantity+$val['quantity'];
	$total_price=$total_price+$val['price'];
	$total_cost=$total_cost+$val['cost'];
	
$i++; 
?>
<tr id="<?=$val["id_purchases"]; ?>">
<td class="col-chk"></td>

<td >
<? echo $val["quantity"]; ?>
</td>
<td>
<? echo $val["price"]; ?>
</td>
<td>
<? echo $val["cost"]; ?>
</td>

<td >
<? if($val['status'] == 1){ echo '<span class="label label-success">Stock in Consignment</span>';} ?>
<? if($val['status'] == 2){ echo '<span class="label label-important">Stock Paid</span>';} ?>
</td>

<td style="text-align:center">
<? if ($this->acl->has_permission('products','edit')){ ?>
<a href="<?= site_url('back_office/products/edit_purchases/'.$val["id_purchases"].'?id_'.$type.'='.$val['id_products']);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>
<? } ?>
<? if ($this->acl->has_permission('products','delete')){ ?>
<a onClick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_purchases"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="7" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>

<table cellspacing="2">
<tr><td><strong>Total Quantity</strong></td><td> :<?php echo $total_quantity;?></td></tr>
<tr><td><strong>Total Price</strong></td><td> :<?php echo $total_price;?> <?php echo $default_currency;?></td></tr>
<tr><td><strong>Total Cost</strong></td><td> :<?php echo $total_cost;?> <?php echo $default_currency;?></td></tr>
</table>

</div>

</div>   
</div>
</body>
</html>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 