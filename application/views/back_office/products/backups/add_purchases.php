<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
<script src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>

</head>

<body>
<div class="container-fluid">
<div class="row-fluid">


<div class="span10-fluid" >
<!--<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >
<li><a href="<?= site_url('back_office/products/purchases?id_product='.$product['id_products']); ?>" >List Of Purchases</a></li>
<span class="divider">/</span>
<li class="active">List of purchases</li>

</ul>-->Edit <?php echo $product['title']; ?></a></li>
<span class="divider">/</span>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
$default_currency=$this->config->item('default_currency');
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/products/submitPurchases', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["id_products"] = "";
$info["quantity"] = "";
$info["price"] = "";
$info["cost"] = "";
$info["id_user"] = "";
$info["status"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>
<?php if($type=="stock"){?>
<input type="hidden" name="id_products"  value="<?php echo set_value("id_products",$product_stock["id_products_stock"]);?>" />
<?php } ?>
<?php if($type=="product"){?>
<input type="hidden" name="id_products"  value="<?php echo set_value("id_products",$product["id_products"]);?>" />
<?php } ?>
<input type="hidden" name="type"  value="<?php echo set_value("type",$type);?>" />
<input type="hidden" name="sku"  value="<?php echo set_value("sku",$sku);?>" />


<table cellspacing="2">
<?php if(isset($id)){?>
<tr><td><strong>Invoice ID</strong></td><td> : <?php echo $id;?></td></tr>
<tr><td><strong>Invoice Date</strong></td><td> : <?php echo $info['created_date'];?></td></tr>
<?php } ?>
<tr><td><strong>Sku</strong></td><td>: <?php echo $sku;?></td></tr>
</table>
<br />
<?php
//TITLE SECTION.



//QUANTITY SECTION.
echo form_label('<b>QUANTITY</b>', 'QUANTITY');
echo form_input(array('name' => 'quantity', 'value' => set_value("quantity",$info["quantity"]),'class' =>'span' ));
echo form_error("quantity","<span class='text-error'>","</span>");
echo br();
//PRICE SECTION.
echo form_label('<b>PRICE('.$default_currency.')</b>', 'PRICE');
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span' ));
echo form_error("price","<span class='text-error'>","</span>");
echo form_label('<span style="color:#F03">This is used for inventory not for the website.</span>  ', 'This is used for inventory not for the website.');

//COST SECTION.
echo form_label('<b>COST('.$default_currency.')</b>', 'COST');
echo form_input(array('name' => 'cost', 'value' => set_value("cost",$info["cost"]),'class' =>'span' ));
echo form_error("cost","<span class='text-error'>","</span>");
echo form_label('<span style="color:#F03">This is used for inventory not for the website.</span>  ', 'This is used for inventory not for the website.');
echo br();
//STATUS SECTION.
echo form_label('<b>STATUS</b>', 'STATUS');
$options = array(
                  "" => " - select option - ",
                  1 => "Stock Paid",
                  2 => "Stock in Consignment",
                );
echo form_dropdown("status", $options,set_value("status",$info["status"]), 'class="span"');
echo form_error("status","<span class='text-error'>","</span>");
echo br();


echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
?>
<a href=""  onclick="javascript:history.go(-1)" >&lt;&lt; go back</a>
<?php
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>

</div>     
</div>
</div>

<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?></body>