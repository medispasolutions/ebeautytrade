

<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	$('.fancyClick').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 
		 afterClose: function() {}});
	$('.fancyClick2').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 
		 afterClose: function() {
		var url="";
		var id=$('#id_stock_active').val();
		var url="<?php echo site_url('back_office/products/getQuantity/stock');?>/"+id;
		
		$.post(url,{id : ""},function(data){
			$('#remanin_quantity').html(data);
		});
			/*location.reload();*/
			}});
		
});
function sellingPrice(field,obj){
	var obj_parent=$(obj).parent();
	var id_product=$(obj).attr('id').replace(field+'_','');
	
	
	var list_price=$("#list_price_"+id_product).val();

	var discount=$("#discount_"+id_product).val();;

	if(discount>=0 && discount<=100){
	var price = roundToTwo(list_price - ((list_price * discount) / 100));
	$("#price_"+id_product).val(price);
	}else{
		obj_parent.find("input[name='discount[]']").val('0');
		$("#price_"+id_product).val(list_price);
		alert('Discount should be between 0 & 100.');
		}

	}
	
	
	
	
	
function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}	
	</script>

<script>
function delete_row(id){
window.location="<?= site_url("back_office/products/delete"); ?>/"+id;
return false;
}

$(function(){
	$('#category').change(function(){
		var id = $(this).val();
		if(id == '') {
			return false;
		}
		else {
			var url = "<?php echo site_url("back_office/categories_sub/load_sub_filter2"); ?>";
			$("#sub_category").attr("disabled","disabled");
			$("#sub_category").html("");
			$("#category").attr("disabled","disabled");
			$.post(url,{id : id},function(data){
				$("#sub_category").removeAttr("disabled");
				$("#category").removeAttr("disabled");
				$("#sub_category").html(data);
			});
		}
	});
	
$("#table-1").tableDnD({
onDrop: function(table, row) {
var ser=$.tableDnD.serialize();
$("#result").load("<?= site_url("back_office/products/sorted"); ?>"+"?"+ser);
}
});

$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script>

<input type="hidden" value="<?php echo $url;?>" name="products_url" id="products_url" />

<?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="1";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li class="active"><?= $title; ?></li>
<? if ($this->acl->has_permission('products','add')){ ?>
<li class="pull-right">
<?php if(isset($bool_kits) && $bool_kits){?>
<a href="<?= site_url('back_office/group_products/add'); ?>" id="topLink" class="btn btn-info top_btn" title="">Add <?php echo lang('group_products');?></a>
<?php }else{ ?>
<a href="<?= site_url('back_office/products/add'); ?>" id="topLink" class="btn btn-info top_btn" title="">Add products</a>
<?php } ?>
</li><? } ?>
</ul> 
</div>
<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">



<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
              <?php if(isset($bool_kits) && $bool_kits){
				$form_url=site_url("back_office/group_products"); 
				   }else{
				$form_url=site_url("back_office/products"); 	     } ?>
            <form method="get" action="<?=$form_url;?>">
<!--Product ID-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">Product ID<br /><input type="text" id="product_id" name="product_id" value="<? if(isset($_GET['product_id'])) { echo $_GET['product_id']; }?>" /></div>
<!--Product Name-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;">Product Description<br /><input type="text" id="product_name" name="product_name" value="<? if(isset($_GET['product_name'])) { echo $_GET['product_name']; }?>" /></div>
<!--Product SKU-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">Product SKU<br /><input type="text" id="product_sku" name="product_sku" value="<? if(isset($_GET['product_sku'])) { echo $_GET['product_sku']; }?>" /></div>

<!--Barcode-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">Barcode<br /><input type="text" id="barcode" name="barcode" value="<? if(isset($_GET['barcode'])) { echo $_GET['barcode']; }?>" /></div>
<?php if(!isset($supplier)){?>

<!--Sales-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Sales<br />
<select name="sales" id="sales">
<option <?php if($this->input->get('sales')=="2") echo 'selected="selected"';?> value="2">-All-</option>
<option <?php if($this->input->get('sales')=="1") echo 'selected="selected"';?>  value="1">Active</option>
<option <?php if($this->input->get('sales')=="0") echo 'selected="selected"';?> value="0">InActive</option>
</select>
</div>
<!--STATUS-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Status<br />
<select name="status" id="status">
<option <?php if($this->input->get('status')=="") echo 'selected="selected"';?> value="">-All-</option>
<option <?php if($this->input->get('status')!="" && $this->input->get('status')=="0") echo 'selected="selected"';?>  value="0">Published</option>
<option <?php if($this->input->get('status')=="1") echo 'selected="selected"';?> value="1">Unpublished</option>
<option <?php if($this->input->get('status')=="2") echo 'selected="selected"';?> value="2">Please Add</option>
<option <?php if($this->input->get('status')=="2") echo 'selected="selected"';?> value="3">Draft</option>

</select>
</div>

<!--Group Categories-->
<!--<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Group Category<br />
<?php $categories = $this->fct->getAll('group_categories','title'); ?>
<select name="group_category" id="group_category">
<option value="">- All -</option>
<?php foreach($categories as $cat) {
	$cl = '';
	if(isset($_GET['group_category']) && $_GET['group_category'] == $cat['id_group_categories'])
	$cl = 'selected="selected"';
?>
<option value="<?php echo $cat['id_group_categories']; ?>" <?php echo $cl; ?>><?php echo $cat['title'.getFieldLanguage($this->lang->lang())]; ?></option>
<?php }?>
</select>
</div>-->
<?php if(isset($supplier)){?>
<!--Published-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">New Modifications<br />
<select name="published" id="published">
<option <?php if($this->input->get('published')=="2")echo 'selected="selected"';?> value="2">Inactive</option>
<option <?php if($this->input->get('published')=="1")echo 'selected="selected"';?>  value="1">Active</option>
</select>
</div>
<?php } ?>


<!--Category-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Category<br />
<?php $categories = $this->fct->getAll_cond('categories','title',array('id_parent'=>0)); ?>
<select name="category" id="category">
<option value="">- All -</option>
<?php foreach($categories as $cat) {
	$cl = '';
	if(isset($_GET['category']) && $_GET['category'] == $cat['id_categories'])
	$cl = 'selected="selected"';
?>
<option value="<?php echo $cat['id_categories']; ?>" <?php echo $cl; ?>><?php echo $cat['title'.getFieldLanguage($this->lang->lang())]; ?></option>
<?php }?>
</select>
</div>
<!--Sub Category-->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Sub Category<br />
<?php 
if($this->input->get("category") != "") {
$category = $this->fct->getonerow("categories",array("id_categories"=>$this->input->get("category")));
if(!empty($category)) {
$id_selected_category=$category['id_categories'];

}
}

?>
<select name="sub_category" id="sub_category">
<?php if(isset($id_selected_category)){
$limit = $this->custom_fct->getCategoriesPages($id_selected_category);
$sub_categories = $this->custom_fct->getCategoriesPages($id_selected_category,array(),array(),$limit,0);?>
<?php
	$cl = '';
	$selected_options=array();
	if(isset($_GET['sub_category']) && !empty($_GET['sub_category'])){
		$id_sub_category=$_GET['sub_category'];
		$selected_options=array($id_sub_category);}	
?>
<option value="">- All -</option>

<?php 

$s_id = '';
if(isset($id)) {
$s_id = $info['id_parent'];
}
echo displayLevels($sub_categories,0,$s_id,$selected_options);

?>



<?php } ?>
</select>
</div>


<!--Sales-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">New Arrivals<br />
<select name="news" id="news">
<option <?php if($this->input->get('news')=="") echo 'selected="selected"';?>value="">-All-</option>
<option <?php if($this->input->get('news')=="1") echo 'selected="selected"';?>  value="1">Active</option>
<option <?php if($this->input->get('news')=="0") echo 'selected="selected"';?> value="0">InActive</option>
</select>
</div>
<?php } ?>
<!--Brands-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Brands<br />
<?php 
$cond2=array();
if(isset($supplier)){
	$user=$this->ecommerce_model->getUserInfo();
	$cond2['id_user']=$user['id_user'];}
$brands = $this->fct->getAll_cond('brands','title',$cond2); ?>
<select name="brand" id="brand">
<option value="">- All -</option>
<?php foreach($brands as $brand) {
	$cl = '';
	if(isset($_GET['brand']) && $_GET['brand'] == $brand['id_brands'])
	$cl = 'selected="selected"';
?>
<option value="<?php echo $brand['id_brands']; ?>" <?php echo $cl; ?>><?php echo $brand['title'.getFieldLanguage($this->lang->lang())]; ?></option>
<?php }?>
</select>
</div>


<input type="hidden" name="section" value="uploadProducts" />
<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" name="report_submit" value="Filter" />

<button type="submit" class="btn btn-primary  btn-danger" name="report_submit" value="export" ><i class="icon-download"></i> Export</button>

<? if ($this->acl->has_permission('guide','delete_all')){ ?>
<!--<input type="submit" class="btn btn-primary" name="report_submit" value="import" />-->
<button type="submit" class="btn btn-primary  btn-danger" name="report_submit" value="import" ><i class="icon-upload"></i> Import</button>
<?php } ?>
</div>
</form>
              </div>
            </div>
          </div>
        </div>
</div>
<!--<div class="span10-fluid" style="width:90%;margin:0 10px 0 0; float:left">

<form action="<?php echo site_url("back_office/excel_c/do_import"); ?>" accept-charset="utf-8" method="post" enctype="multipart/form-data">
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;">Uplaod File(.xlsx)<br /><input type="file" id="product_name" name="tmp_name"  /></div>
<input type="hidden" name="section" value="uploadProducts" />

<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" name="upload" value="Upload" />
</div>
</form>
</div>-->
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form','id'=>'updateProducts');
echo form_open_multipart('back_office/products/submit_products', $attributes); 
?>  
<?php if(isset($supplier)){?>
<table class="total" border=0 cellpadding="5" cellspacing="2" >
<tr><th colspan="2" ><span style="color:#0088cc;">Total items quantity</span></th></tr>
<tr><td><strong>Real stock</strong></td><td><?php echo $total['total_qty'];?></td></tr>
<tr><td><strong>Virtual stock</strong></td><td><?php echo $total['total_virtual_qty'];?></td></tr>
</table>	

<a class="refresh" onclick="refresh_page()"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>	
<?php } ?>
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="15" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th></th>
<th width="2%"></th>
<th>Product Description</th>
<?php if(!isset($supplier)){?>
<th></th>
<?php } ?>
<th>SKU</th>
<?php if(isset($supplier)){?>
<th>Multi Prices</th>
<th  style="width:100px;padding-right:20px;text-align:left;">Retail Price(<?php echo $this->config->item('default_currency') ;?>)</th>
<th  style="width:100px;text-align:left;">Virtual Stock</th>
<th style="width:75px;text-align:left;">Trade Price(<?php echo $this->config->item('default_currency') ;?>)</th>
<th  style="width:75px;text-align:left;">Discount(%)</th>
<th  style="width:75px;text-align:left;">Price after Discount(<?php echo $this->config->item('default_currency') ;?>)</th>
<?php }else{ ?>
<th>New</th>
<th>STATUS</th>
<?php } ?>
<th style="text-align:center;" width="150">ACTION</th></tr>




</thead><tfoot><tr>
<td class="col-chk"><input type="checkbox" id="checkAllAuto" /></td>
<td colspan="10">
<? if ($this->acl->has_permission('products','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="deleteAll()" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>
<tbody>
<input type="hidden" name="ajax" value="1" />
<input type="hidden" name="method" id="method" value="delete_all" />
<? 
if(isset($info) && !empty($info)){
$i=0;
foreach($info as $val){
	

$i++; 
?>
<tr id="<?=$val["id_products"]; ?>">
<td style="position:relative;width: 21px;">
<?php if($val["list_price"]>$val['price']){?>
<img style="position:absolute;top:0;left:0;" src="<?php echo base_url();?>front/img/salelogo1.png" />
<?php } ?></td>
<td class="col-chk">

<input type="checkbox" name="cehcklist[]" value="<?= $val["id_products"] ; ?>" />
<input type="hidden" name="product_id[]" value="<?= $val["id_products"] ; ?>" />
<input type="hidden" name="rand[]" value="<?= $val["rand"] ; ?>" /></td>

<td class="title_search">
<? echo $val["title".$lang]; ?><br />
<?php /*?><b>Created Date :</b> <?php echo  date("F d,Y", strtotime($val['created_date'])); ?><?php */?>
</td>
<?php if(!isset($supplier)){?>
<td >
<?php 
if($val['deleted_supplier']==1) {echo '<span class="label label-important">Deleted By Supplier</span>';}else{

if($val['published']==1) { echo '<span class="label label-important">New Modifications</span>';} ?>
<?php } ?>
</td>
<?php } ?>
<td >
<? echo $val["sku"]; ?>
</td>
<?php if(isset($supplier)){?>
<td style="padding-right:20px;">
<a class="label label-success fancyClick" href="<?php echo route_to('back_office/products/stock_price_segments/'.$val['id_products']);?>" data-original-title="">Manage multi buy</a>
</td>

<td style="text-align:left;">
<input type="text" class="updateQty" name="retail_price[]" value="<?php echo $val['retail_price'];?>" />
</td style="">
<td style="text-align:left;">
<input type="text" class="updateQty" name="stock_status_quantity_threshold[]" value="<?php echo $val['stock_status_quantity_threshold'];?>" />
</td>

<td style="text-align:left;">
<input type="text" class="updateQty" name="list_price[]" id="list_price_<?php echo $val['id_products'];?>" value="<?php echo $val['list_price'];?>" onkeyup="sellingPrice('list_price',this)"  />
</td>
<td style="text-align:left;">
<input type="text" class="updateQty" name="discount[]" id="discount_<?php echo $val['id_products'];?>" value="<?php echo $val['discount'];?>"  min="0" max="100"  onkeyup="sellingPrice('discount',this)"  />
</td>

<td style="text-align:left;">
<input type="text" class="updateQty" disabled="disabled" name="price[]" id="price_<?php echo $val['id_products'];?>"  value="<?php echo $val['price'];?>"  min="0" max="100"   />
</td>

<?php }else{ ?>
<td><?php if($val['set_as_new']==1) { echo  '<span class="label label-success">Active</span>';}else{
	echo '<span class="label label-important ">InActive</span>' ;} ?></td> 
<td class="">
<? if($val['status'] == 1) {echo '<span class="label label-important">Unpublished</span>';}
 if($val['status'] == 0) { echo '<span class="label label-success">Published</span>';} 
  if($val['status'] == 2) { echo '<span class="label label-warning">Please Add</span>';} //changed "under review" to "Please Add"
if($val['status'] == 3) { echo '<span class="label label-warning">Draft</span>';}?>
</td>
 <?php } ?>
<td style="text-align:center">
<? if($this->config->item("language_module")) { 
?>
<a href="<?= site_url('back_office/translation/section/products/'.$val["id_products"]);?>" class="table-edit-link" title="Translate" >
<i class="icon-search" ></i> Translate</a> <span class="hidden"> | </span>
<? }?>



<? if ($this->acl->has_permission('products','edit')){ ?>
<?php if(isset($bool_kits) && $bool_kits){?>
<a href="<?= site_url('back_office/group_products/view/'.$val["id_products"].'/'.$val['rand']);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a><?php }else{ ?>
<a href="<?= site_url('back_office/products/edit/'.$val["id_products"]);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a>
<?php } ?> <span class="hidden"> | </span>

<? } ?>
<? if ($this->acl->has_permission('products','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_products"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
<? if ($this->acl->has_permission('products','add')){ ?>
<br />

<a href="<?php echo route_to("products/details/".$val['title_url']); ?>?preview=1" target="_blank"><i class="icon-search" ></i>   Preview Product</a>

<? } ?>
</td>

<!--<td style="text-align:center">
<div class="btn-group" style="width:150px;">
<button class="btn btn-default btn-flat" type="button">Action</button>
<button data-toggle="dropdown" class="btn btn-default btn-flat dropdown-toggle" type="button">
<span class="caret"></span>
<span class="sr-only"></span>
</button>
<ul class="dropdown-menu" role="menu" style="text-align:left">

<? if ($this->acl->has_permission('products','edit')){ ?>
<li>
<a href="<?= site_url('back_office/products/edit/'.$val["id_products"]);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a></li>
<? } ?>
<? if ($this->acl->has_permission('products','delete')){ ?>
<li>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_products"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a></li>
<? } ?>
<li>
<a href="<?php echo route_to("products/details/".$val['id_products']); ?>?preview=1" target="_blank"><i class="icon-search" ></i>   Preview Product</a></li>
</ul>
</div>
</td>-->
</tr>

<?php if(!empty($val['products_stock'])){
	$products_stock=$val['products_stock'];

	 ?>
<tr class="tr-sub-table"><td colspan="10">
<table  cellpadding="0" cellspacing="1" width="100%">
<?php foreach($products_stock as $val2){
	
$com="";
$txt="";
$j=0;
$options=unSerializeStock($val2['combination']);$i++; 

$c = count($options);
foreach($options as $opt) {$j++;
if($j==1){$com="";}else{$com=", ";}?>
 <?php $txt=$txt.$com.$opt; ?>
<?php } ?>
<tr><td style="text-align:center"><?php echo $txt;?></td><td><?php echo $val2['sku'];?></td>
<td style="text-align:center">
<? if ($this->acl->has_permission('products','edit')){ ?>
<a href="<?= site_url('back_office/products/stock/'.$val["id_products"]);?>#stock-<?php echo $val2['id_products_stock'];?>" class="table-edit-link" title="Edit" >
Edit</a>
<? } ?>

</td>
</tr>
<?php } ?>
</table>
</td></tr>

	
	<?php } ?>

<? }  } else { ?>
<tr class='odd'><td colspan="10" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/products"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 

<script>
function updateQty_cart(){
	
if($(".updateQty").length>0){
$(".updateQty").focus(function() {

$(this).keyup(function(){	
var obj=$(this);
var bool=true;
if($('#updateProducts').length>0){
	$('#method').val('update_quantity');
	$(".updateQty").parent().removeClass('loader_success');
	obj.parent().addClass('loader_input');
	$('#updateProducts').submit();
	}


});});}}
$(document).ready(function(){
	updateQty_cart();
	});
	
function deleteAll(){
	$('#method').val('delete_all');
	$('#updateProducts').submit();}	
</script>
