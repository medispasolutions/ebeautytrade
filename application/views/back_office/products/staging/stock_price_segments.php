<?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recipes of: <?=$chef['name']?></title>
<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
<script src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script>
function delete_row(id){
	
window.location="<?= site_url("back_office/products/delete_stock_price_segment"); ?>/"+id+"/<?php echo $id_products; ?>/<?php echo $id_stock; ?>";
return false;
}
</script>
</head>
<body>
<div class="container-fluid">
<div class="row-fluid">
<div class="span10" style="width:100%;">
<div class="hundred pull-left">   	
<div class="container-fluid">
<div class="row-fluid">

<div class="span12">
 <h3><?php echo $product['title'].': '; ?>
 <?php 	$options=unSerializeStock($stock['combination']);  ?>
      <?php
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
 </h3>
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/products/delete_all_stock_price_segments', $attributes); 
echo form_hidden("id_products",$id_products);
echo form_hidden("id_stock",$id_stock);
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="6" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<td width="2%">&nbsp;</td>

<th>QTY</th>
<th>Unit Price (<?php echo $this->config->item("default_currency"); ?>)</th>
<th style="text-align:center;" width="250">ACTION</th></tr>
</thead><tfoot><tr>
<!--<td class="col-chk"><input type="checkbox" id="checkAllAuto" /></td>-->
<td colspan="6">
<? if ($this->acl->has_permission('product_price_segments','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?>
<a style="float:right; margin-right:10px" href="<?php echo site_url('back_office/products/add_more_stock_price_segments/'.$id_products.'/'.$id_stock); ?>">add more ?</a>
</td>
</tr>
</tfoot>
<tbody>
<? 
if(count($price_segments) > 0){
$i=0;
foreach($price_segments as $val){
$i++; 
?>
<tr id="<?=$val["id_product_price_segments"]; ?>">
<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_product_price_segments"] ; ?>" /></td>
<!--<td>
Get <?php echo $val['min_qty'];?> for <?php echo $val['price'];?> <?php echo $this->config->item('default_currency');?> only
</td>-->
<td>
<? echo $val["min_qty"]; ?>
</td>
<td>
<? echo $val["price"]; ?>
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('products','edit')){ ?>
<a href="<?= site_url('back_office/products/edit_stock_price_segments/'.$val["id_product_price_segments"].'/'.$id_products.'/'.$id_stock);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>
<? } ?>
<? if ($this->acl->has_permission('products','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_product_price_segments"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>

</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 