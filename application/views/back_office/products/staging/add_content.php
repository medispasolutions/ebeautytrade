<?php $lang = getFieldLanguage($this->lang->lang())?>

<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>

<?php if(isset($errors)){
	echo $errors;}	?>
<?php $size_arr=getMaxSize('file');?>
<script type="text/javascript">

function delete_segement(id_segement,id_product){
	$('#segement_'+id_segement).html("<td colspan='10'><div class='loader'></div></td>");
		var url=$('#baseurl').val()+"back_office/products/delete_segement";
		$.post(url,{id_segement :id_segement,id_product :id_product},function(data){
			if($('.priceSegements tr').length>2){
		$('#segement_'+id_segement).remove();}else{
			$('.priceSegements').html('<div class="empty">No segments found</div>');
			}
			if($('.priceSegements tr').length>=4){
				$('#product_price_segments_btn').hide();}else{
				$('#product_price_segments_btn').show();}
		});
     
	}</script>
<script>
$(document).ready(function(){
	$('.fancyClick').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 afterClose: function() {}});
	$('.fancyClick2').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 afterClose: function() {
			 var url="";
			 <?php if(isset($id)){ ?>
			 var url="<?php echo site_url('back_office/products/getQuantity/products/'.$id);?>";
			 <?php } ?>
		$.post(url,{id : ""},function(data){
			$('#remanin_quantity').html(data);
		});
			/*location.reload();*/
			}});
			
			$('.fancyClick3').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 afterClose: function() {
			 
		var url=$('#baseurl').val()+'back_office/product_price_segments/getProductSegements';

var id_product=$("input[name='id']").val();
$('.priceSegements').html('<div class="loader"></div>');

		 $.post(url,{id_product : id_product},function(data){
			
			 var Parsedata = JSON.parse(data);
			$('.priceSegements').html(Parsedata.html);
				if($('.priceSegements tr').length>=4){
				$('#product_price_segments_btn').hide();}else{
				$('#product_price_segments_btn').show();}
		   });
		  
		}  });
			
	});
$(function(){
$('#checkAllAuto1').click(function(){
	if($('#checkAllAuto1').is(':checked')) {
		//$("input[type=checkbox]").attr('checked','checked');
		$('.cehcklist_1').each(function(){
			//$(this).attr("checked",null);
			$(this).attr("checked","checked");
		});
	}
	else {
		//$("input[type=checkbox]").removeAttr('checked');
		$('.cehcklist_1').each(function(){
			$(this).attr("checked",null);
		});
	}
});
});
</script>
<script type="text/javascript">
function sellingPrice(){
	var list_price=$("input[name='list_price']").val();
	discount=0;
	if($("input[name='discount']").length>0){
	var discount=$("input[name='discount']").val();}
	if(discount>=0 && discount<=100){
	var price = roundToTwo(list_price - ((list_price * discount) / 100));
	$("input[name='price']").val(price);}else{
		$("input[name='discount']").val('0');
		$("input[name='price']").val(list_price);
		alert('Discount should be between 0 & 100.');
		
		}
		miles_number();
}

function sellingRetailPrice(){

	var list_price=$("input[name='retail_list_price']").val();
	var discount=$("input[name='retail_discount']").val();

	if(discount>=0 && discount<=100){	
	var price = roundToTwo(list_price - ((list_price * discount) / 100));
	
	$("input[name='retail2_price']").val(price);}else{

		$("input[name='retail_discount']").val('0');
		
		$("input[name='retail2_price']").val(list_price);
		alert('Discount should be between 0 & 100.');
		
	
		}	miles_number();
}
	
	function miles_number(){
	var general_miles=$("input[name='general_miles']").val();
	var price=$("input[name='price']").val();
	if(general_miles>=0 && general_miles<=100){
	var price_miles=(general_miles*price)/100;
	var miles=Math.round(price_miles*10);
	$("input[name='miles']").val(miles);}else{
		$("input[name='miles']").val(0);
		$("input[name='general_miles']").val(0);
		alert('Miles(%) should be between 0 & 100.');}
	}
	
function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('#set_general_miles').change(function(){
		 if($('#general_miles_input').is(":visible") && $(this).prop('checked')==false){
			$('#general_miles_input').stop(true,true).slideUp('fast');
			 }else{
			$('#general_miles_input').stop(true,true).slideDown('fast'); }
    	});
		
		$('#set_as_redeemed_by_miles').change(function(){
		 if($('#redeemed_miles_input').is(":visible") && $(this).prop('checked')==false){
			$('#redeemed_miles_input').stop(true,true).slideUp('fast');
			 }else{
			$('#redeemed_miles_input').stop(true,true).slideDown('fast'); }
    	});
		
		/*$('#exportable').change(function(){
		 if($('#exportable').is(":visible") && $(this).prop('checked')==false){
			$('#dimensions').stop(true,true).slideDown('fast');
			}else{
			$('#dimensions').stop(true,true).slideUp('fast'); }
    	});*/
	
});

$(document).ready(function(){
	$('select[name="id_group_categories"]').change(function(){
		
		var value=$(this).val();

		if(value==1 || value==0 ){
			$('#group_categories_professional').stop(true,true).slideDown('fast');}else{
				$('#group_categories_professional').stop(true,true).slideUp('fast');}
		
		if(value==2 || value==0 ){
			$('#group_categories_retail').stop(true,true).slideDown('fast');}else{
				$('#group_categories_retail').stop(true,true).slideUp('fast');}
    	});
		if($('select[name="stock_status"]').length>0){
		$('select[name="stock_status"]').change(function(){
		
		var value=$(this).val();

		if(value==1 || value==2 ){
			$('#stock_status_threshold').stop(true,true).slideDown('fast');}else{
				$('#stock_status_threshold').stop(true,true).slideUp('fast');}
		
		
    	});}
		
	
});

function removeRow(section,field,id)
{
	if(confirm('Are you sure you want to delete this record ?')) {
		$('#'+field+'_'+id).html('Loading...');
		var url = '<?php echo site_url(); ?>back_office/products/delete_row';
		$.post(url,{section : section,field : field,id : id},function(data){
			$('#'+field+'_'+id).html(data);
			  $('#section_repeat_'+id).remove(); 
		});
	}
	else {
		return false;
	}
}
$(document).ready(function(e) {
	
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){
	  var rand=Math.floor((Math.random() * 1000) + 1)+"a";//on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><div class="contactrow"><div class="contactlabel">Tab Title <span><small class="blue">(Max <?php echo getLimitText('title');?> Characters)</small></span><span>*</span></div><input class="span" type="text" value="" id="" maxlength="<?php echo getLimitText('title');?>" name="title_product_info['+rand+']" onKeyDown="limitText(this.form.title_product_info_'+rand+',0,<?php echo getLimitText('title');?>);" onKeyUp="limitText(this.form.title_product_info_'+rand+',0,<?php echo getLimitText('title');?>);"></div><div class="contactrow"><div class="contactlabel">Tab Description<small class="blue">(Max <?php echo getLimitText('description');?> Characters)</small></span></div><textarea id="description_'+rand+'" class="ckeditor" rows="3" onKeyDown="limitText(this.form.description_'+rand+',0,<?php echo getLimitText('description');?>);" onKeyUp="limitText(this.form.description_'+rand+',0,<?php echo getLimitText('description');?>);" cols="100" name="description_product_info['+rand+']" ></textarea></div><div class="contactrow"><div class="contactrow"><div class="contactlabel">File for this Tab<small class="blue">(Max Size <?php echo $size_arr['size_mega'];?>Mb)</small></div><input class="span" type="file" value="" name="file_product_info_'+rand+'" onchange="getfilesize(1)"></div><div class="contactrow"><div class="contactrow"><div class="contactlabel">File(2) for this Tab<small class="blue">(Max Size <?php echo $size_arr['size_mega'];?>Mb)</small></div><input class="span" type="file" value="" name="file2_product_info_'+rand+'" onchange="getfilesize(1)"></div><div class="contactlabel">Youtube link <small class="blue">(ex:https://www.youtube.com/watch?v=C0DPdy98e4c)</small></div><input class="span" type="text" value="" name="video_product_info['+rand+']"></div><a href="#" class="remove_field remove_d"><i class="icon-remove-sign"></i>Remove this Tab</a><hr></div>'); //add input box
		
			CKEDITOR.replace( "description_"+rand,{
       	  filebrowserUploadUrl : '<?php echo base_url(); ?>texteditor/upload'
		  
    	});
/*		var id = 'description_'+rand;
		 CKEDITOR.replace( id,{
       	 filebrowserUploadUrl : '<?php echo base_url(); ?>texteditor/upload'
		  
    	});*/
	
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })	
	

   var cont=$('#other_qualification').val(); 
   	if(cont=='other')
	{
		$('.other_qualification').css('display','block');
		$('.other_qualification').addClass('acty');
	}
	var cont1=$('#other_qualification_1').val(); 
   	if(cont1=='other')
	{
		$('.other_qualification_1').css('display','block');
		$('.other_qualification_1').addClass('acty');
	}
})


</script>
<script type="text/javascript">
function changeCategory()
{
	console.log($('#categoryBox').val());
	var categories = $('#categoryBox').val();
	$('#subCategoriesBox').html('<option value="">Loading...</option>');
	var url = '<?php echo site_url('back_office/products/filtercategories'); ?>';
	$.post(url,{categories : categories},function(data){
	     $('#subCategoriesBox').html(data);
	});
}
</script>

<script type="text/javascript">
$(document).ready(function(e) {

	$('select[name="id_delivery_terms"]').change(function(){
	
		var obj=$(this);
		var val=obj.val();
		if(val==""){
			var text="";}else{
			var text=$("select[name='id_delivery_terms'] option:selected").text();}
		
		if(val==3){
			$('#delivery_day').val('');
			$('#delivery_terms_txt').slideDown('fast');
			}else{
				
				$('#delivery_day').val(text);
			$('#delivery_terms_txt').slideUp('fast');
		    	}
		})

	});
	

function SelectedCategory(id,title)
{

var url="<?php echo site_url('back_office/categories/edit')?>/"+id ;

if($('#selelect_btn_'+id).length>0){
$('#selelect_btn_'+id).remove();

$('#multi_select_categories option[value="'+id+'"]').attr('selected',false);
}else{
var btn_selet="<div class='btn btn_select' id='selelect_btn_"+id+"'><a href='"+url+"' >"+title+"</a><span class='notification red remove_selected' onclick='SelectedCategory("+id+")'>x</span></div>";	
$('.select_categories').append(btn_selet);
	}
}

function SelectedCategory_b2c(id,title)
{

var url="<?php echo site_url('back_office/categories/edit')?>/"+id ;

if($('#selelect_btn_b2c_'+id).length>0){
$('#selelect_btn_b2c_'+id).remove();

$('#multi_select_categories_b2c option[value="'+id+'"]').attr('selected',false);
}else{
var btn_selet="<div class='btn btn_select' id='selelect_btn_b2c_"+id+"'><a href='"+url+"' >"+title+"</a><span class='notification red remove_selected' onclick='SelectedCategory_b2c("+id+")'>x</span></div>";	
$('.select_categories_b2c').append(btn_selet);
	}
}

function addRelatedProduct(){
	var id=$('#id_related_product').val();
var title=$('input[name="related_product"').val();
	if(id!=""){
		var url="<?php echo site_url('back_office/products/edit')?>/"+id ;
var btn_select="<div class='btn btn_select' id='related_product_"+id+"'><input type='hidden' name='related_products_arr[]' value='"+id+"' ><a href='"+url+"' >"+title+"</a><span class='notification red remove_selected' onclick='removeRelatedProduct("+id+")'>x</span></div>";	
$('.related_products').append(btn_select);
$('#id_related_product').val('');
$('input[name="related_product"').val('');}else{
	alert('No product found');}
	}
	
function removeRelatedProduct(id){

	$('#related_product_'+id).remove();}	
</script>
<?php 
$bool_kits=false;
if(isset($info["group_products"]) && $info["group_products"]==1){
	$bool_kits=true;}
	
$attributes = array('class' => 'middle-forms');
?>
	<?php if($bool_kits){
		$link='back_office/group_products/submit_details';}else{
		$link='back_office/products/submit';	}
		
echo form_open_multipart($link, $attributes); 

if(isset($info['id_products']) && !empty($info['id_products'])){
$id=$info['id_products'];

echo form_hidden('id', $id);
$brand_training=$this->fct->getonerecord('brand_certified_training',array('id_products'=>$id));
 
//echo '<input type="text" name="id1" value="'.$id.'" />';
} else {
$info["sku"] = "";
$info["barcode"] = "";
$info["threshold"] = "";
$info["quantity"] = "";
$info["miles"] = "";
$info["retail2_price"] = "";
$info["catalog"] = "";
$info["price"] = "";
$info["youtube"] = "";
$info["account_type"] = "";
$info["retail_price"] = "";
$info["set_as_special"] = "";
$info["exclude_from_shipping_calculation"] = "";
$info["delivery_days"] = "";
$info['set_as_training']="";
$info['stock_status_quantity_threshold']="";
$info['set_as_soon']="";
$info["description".$lang] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
$info["discount"] = "";
$info["brief".$lang] = "";
$info["lang"] = "";
$info['id_delivery_terms']="";
$info["list_price"] = "";
$info["id_categories"] = "";
$info["location_code"] = "";
$info["id_units"] = "";
$info["id_miles"] = "";
$info["access_code"] = "";
$info["deleted_supplier"] = "";
$info["id_brands"] = "";
$info["id_group_categories"] = "";
$info['set_as_clearance']="";
$info["id_categories_sub"] = "";
$info["discount_expiration"] = "";
$info["features"] = "";
$info["datasheet"] = "";
$info["specifications"] = "";
$info["wholesale_price"] = "";
$info["related_products"] = "";
$info["display_in_home"] = "";
$info["set_as_trends_updates"] = "";
$info["downloads"] = "";
$info["id_designers"] = "";
$info["size_and_fit"] = "";
$info["editors_notes_and_details	"] = "";
$info["washing_information"] = "";
$info["why_and_when"] = "";
$info["video"] = "";
$info["stock_status"] = "";
$info["pdf"] = "";
$info["set_as_pro"] = "";
$info["set_general_miles"] = "";
$info["general_miles"] = "";
$info["set_as_redeemed_by_miles"] = "";
$info["amount_of_miles"] = "";
$info["brief_one"] = "";
$info["brief_two"] = "";
$info["availability"] = "";
$info["image"] = "";
$info["delivery_day"] = "Next day delivery ";
$info["published"] = "";
$info["hide_price"] = "";
$info["status"] = "";
$info["redeem_miles"] = "";
$info["packing"] = "";
$info["fabric"] = "";
$info["dimensions"] = "";
$info["weight"] = "";
$info["rand"] = "";
$info["width"] = "";
$info["height"] = "";
$info["length"] = "";
$info["promote_to_front"] = "";
$info['set_as_clearance']="";
$info['set_as_non_exportable']="";
$info["editors_notes_and_details"] = "";
$info["set_as_new"] = "";
$info["work"] = "";
$info["washing_instructions"] = "";
$info["group_products"] = 0;

$info["retail2_price"] = "";
$info["retail_list_price"] = "";
$info["retail_discount"] = "";
$info["retail_discount_expiration"] = "";
$info["retail_discount_status"] = "";
}



	

echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
$brand_training="";
if(isset($brand_training['id_brand_certified_training'])){
	echo form_hidden('id_brand_certified_training',$brand_training['id_brand_certified_training']);}else{
$brand_training["title".$lang] = "";
$brand_training["image"] = "";
$brand_training["description".$lang] = "";
		}
		
	$user = $this->fct->getonerecord('user',array('id_user' => $this->session->userdata('user_id')));


?>
<input type="hidden" name="rand" value="<?php echo $info['rand'];?>" />


<?php

/*echo form_label('<b>Quantity&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "quantity", "value" => set_value("quantity",$info["quantity"]),"class" =>"span" ));
echo form_error("quantity","<span class='text-error'>","</span>");
echo br();*/
?>
<div class="row-fluid">
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>SKU&nbsp;<em>*</em></b>  ', 'SKU&nbsp;<em>*</em>');
echo form_input(array('name' => 'sku', 'value' => set_value("sku",$info["sku"]),'class' =>'span' ));
echo form_error("sku","<span class='text-error'>","</span>");
echo br();
?>


</div>

<div class="span6">
<?php
//TITLE SECTION.
/*echo form_label('<b>Brief(Line one) <small class="blue">(Max 35 Characters)</small>&nbsp;:</b>', 'Brief(Line one)');
echo form_input(array("id"=>"brief_one",'maxlength'=>35,"name" => "brief_one".$lang, "value" => set_value("brief_one".$lang,$info["brief_one".$lang]),"class" =>"span"));
echo form_error("brief_one","<span class='text-error'>","</span>");
echo br();
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>Brief(Line Two) <small class="blue">(Max 35 Characters)</small>&nbsp;:</b>  ', 'Brief(Line Two)&nbsp;<em>*</em>:');
echo form_input(array('name' => 'brief_two','maxlength'=>35, 'value' => set_value("brief_two",$info["brief_two"]),'class' =>'span' ));
echo form_error("brief_two","<span class='text-error'>","</span>");
echo br();*/
?>
<?php
//META DESCRIPTION SECTION.
echo form_label('<b>BRIEF&nbsp;<small class="blue">(Max 115 Characters)</small></b>', 'BRIEF');
echo form_textarea(array("name" => "brief".$lang,"style" =>"height:54px;", "value" => set_value("brief".$lang,$info["brief".$lang]),"class" =>"span","rows" => 4, "cols" =>100,"onKeyDown"=>"limitText(this.form.brief,0,115);","onKeyUp"=>"limitText(this.form.brief,0,115);"));
echo form_error("brief".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>





</div>

<div class="row-fluid">

<div class="span6">
<?php
//TITLE SECTION.
echo form_label('<b>Product Name&nbsp;<small class="blue">(Max '.getLimitText('title').' Characters)</small><em>*</em></b>', 'Product Name');
echo form_input(array("id"=>"title","name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span","onKeyDown"=>"limitText(this.form.title,0,".getLimitText('title').");","onKeyUp"=>"limitText(this.form.title,0,".getLimitText('title').");" ));
echo form_error("title","<span class='text-error'>","</span>");
echo br();

?>
</div>
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>BARCODE&nbsp;</b>  ', 'BARCODE&nbsp;<em>*</em>:');
echo form_input(array('name' => 'barcode', 'value' => set_value("barcode",$info["barcode"]),'class' =>'span' ));
echo form_error("barcode","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>

<div class="row-fluid">
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>Threshold <small class="blue">(Min Quantity to receive stock notification)</small>&nbsp;</b> (DISABLED)', 'Threshold&nbsp;');
echo form_input(array('name' => 'threshold', 'value' => set_value("sku",$info["threshold"]),'class' =>'span' ), null, " disabled");
echo form_error("threshold","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6" id="stock_status_select" >
<?php if(!$bool_kits){?>
<?php if($supplier){ ?>
	<input type="hidden" name="stock_status" value="2" />
	<?php }else{?>


<?php
echo form_label('<b>Stock Status</b> (DISABLED) ', 'Stock Status');



$stock_status = getStockStatus();

$options = array();

foreach($stock_status as $key=>$val) {
	$options[$key] = $val;
}

echo form_dropdown('stock_status',$options,set_value('stock_status',$info['stock_status']), " disabled");

echo form_error("stock_status","<span class='text-error'>","</span>");
echo br();
}
?>

<div  id="stock_status_threshold" <?php if($info['stock_status']!=1 && $info['stock_status']!=2 && !$supplier) echo "style='display:none'";?>>
<label><b>Available Stock <small class="blue">(optional: availability in stock for online purchases)</small></b> (DISABLED)</label>
<?php
//TITLE SECTION.

echo form_input(array("id"=>"stock_status_quantity_threshold","name" => "stock_status_quantity_threshold", "value" => set_value("stock_status_quantity_threshold",$info["stock_status_quantity_threshold"]),"class" =>"span"), null, " disabled");
echo br();
?>

<?php if(isset($id) && !$bool_kits){ ?>
<label>(DISABLED) <strong>REAL STOCK(in spamiles warehouse): </strong><span id="remanin_quantity"><?php echo $info['quantity'];?></span></label>
<br />
<?php } ?>
</div>


</div>
<?php } ?>


</div>




<div class="row-fluid">
<div class="span6">
<?php
echo form_label('<b>BRANDS</b> ', 'BRANDS');

$cond=array();
if(checkIfSupplier_admin()){
$user=$this->ecommerce_model->getUserInfo();
$cond['id_user']=$user['id_user'];
}

$brands = $this->fct->getAll_cond('brands','title asc',$cond);
$options = array();
$options[""] = '--Select Brands--';
foreach($brands as $brand) {
	$options[$brand['id_brands']] = $brand['title'];
}

echo form_dropdown('id_brands',$options,set_value('id_brands',$info['id_brands'])); 
echo form_error("id_brands","<span class='text-error'>","</span>");
echo br();

?>
</div>
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>Youtube <small class="blue">( ex:https://www.youtube.com/watch?v=C0DPdy98e4c)</small>', 'Youtube');
echo form_input(array('name' => 'youtube', 'value' => set_value("youtube",$info["youtube"]),'class' =>'span' ));
echo form_error("youtube","<span class='text-error'>","</span>");
echo br();
?>
</div>


</div>

<div class="row-fluid">
<div class="span6">

<!--//QUANTITY SECTION.-->

<?php 

//BACKGROUND SECTION.
echo "<label>";
echo form_label('<b>PRINTABLE DATASHEET <small class="blue">(Max Size 3Mb)</small></b>', 'DATASHEET');
echo form_upload(array("name" => "datasheet", "class" => "input-large ccfile","onchange"=>"getfilesize(3)"));
echo "<span >";
if($info["datasheet"] != ""){ 
echo '<span id="datasheet_'.$info["id_products"].'">' ;?>
<a target="_blank" href="<?php echo base_url();?>uploads/products/<?php echo $info["datasheet"];?>">Show File</a>
<?php 
echo nbs(3);?>
<a class="cur" onclick='removeFile("products","datasheet","<?php echo $info["datasheet"]; ?>",<?php echo $info["id_products"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo "</label>";
echo form_error("datasheet","<span class='text-error'>","</span>");


?></div>
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>CATALOG LINK</small>', 'CATALOG');
echo form_input(array('name' => 'catalog', 'value' => set_value("catalog",$info["catalog"]),'class' =>'span' ));
echo form_error("catalog","<span class='text-error'>","</span>");
echo br();
?>
</div>




</div>

<div class="row-fluid">
<?php if(!$supplier){?>
<div class="span6">

<?php
echo form_label('<b>Delivery Terms</b> (DISABLED)', 'Delivery Terms');



$delivery_terms = getDeliveryTerms();

$options = array();
$options[""] = '-Select Delivery Term-';
foreach($delivery_terms as $key=>$val) {
	$options[$key] = $val;
}

echo form_dropdown('id_delivery_terms',$options,set_value('id_delivery_terms',$info['id_delivery_terms']), " disabled");
echo form_error("id_delivery_terms","<span class='text-error'>","</span>");
echo br();

?>
<div  id="delivery_terms_txt" <?php if($info['id_delivery_terms']!=3) echo "style='display:none'";?>>
<?php
//TITLE SECTION.

echo form_input(array("id"=>"delivery_day","name" => "delivery_day".$lang, "value" => set_value("delivery_day".$lang,$info["delivery_day".$lang]),"class" =>"span"), null, " disabled");
echo form_error("delivery_day","<span class='text-error'>","</span>");
echo br();
?></div>



</div>
<?php } ?>


<?php if(!$supplier){?>
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>Access Code <small class="blue">(This will force users to user the inserted code before adding this product to cart)</small> (DISABLED)', 'Access Code');
echo form_input(array('name' => 'access_code', 'value' => set_value("access_code",$info["access_code"]),'class' =>'span' ), null, " disabled");
echo form_error("access_code","<span class='text-error'>","</span>");
echo br();
?>
</div>
<?php } ?>

</div>
<?php if(!$supplier){?>
<div class="row-fluid">
<div class="span6">
<?php 
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>LOCATION CODE</b>', 'LOCATION CODE');
echo form_input(array('name' => 'location_code', 'value' => set_value("location_code",$info["location_code"]),'class' =>'span' ));
echo form_error("location_code","<span class='text-error'>","</span>");
echo br();
?>
</div>

<div class="span6" >
<?php
if(isset($exclude_from_shipping_calculation)){}else{

if(isset($info['exclude_from_shipping_calculation']))
$exclude_from_shipping_calculation = $info['exclude_from_shipping_calculation'];}

?>
<label style="margin-left:25px;margin-top:25px;"><input type="checkbox" id="exclude_from_shipping_calculation" name="exclude_from_shipping_calculation" <?php if(isset($exclude_from_shipping_calculation) && $exclude_from_shipping_calculation == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Exclude from shipping calculation</b></label>
</div>

</div><?php } ?>
<?php if(!$supplier){?>
<div class="row-fluid">
<div class="span6">
<?php $categories_section='b2b';?>
<?php 
$cond['categories.section']=$categories_section;
$selected_options=array();
if(isset($id)) {
$selected_options = $this->fct->select_product_categories($id,$cond);
}



if(isset($h_selected_options)){
$selected_options =$h_selected_options ;	}

echo form_label('<b>SUPPLIER\'S COMMENT ON CATEGORIES</b>');
echo form_input(array('name' => 'categories_comments', 'value' => set_value("categories_comments",$info["categories_comments"]),'class' =>'span' ));

echo form_label('<b>SUPPLIER\'S COMMENT ON ATTRIBUTES</b>');
echo form_input(array('name' => 'attributes_comments', 'value' => set_value("attributes_comments",$info["attributes_comments"]),'class' =>'span' ));


//CATEGORY SECTION.
echo form_label('<b>CATEGORIES(B2B)</b>', 'CATEGORIES');
$cond_cat=array();
$cond_cat['c.section']=$categories_section;
 if($supplier){
$cond_cat['no_id_categories']=522;
$items = $this->custom_fct->getCategoriesPages(0,array('c.id_categories'=>$cond_cat['no_id_categories'],'c.section'=>$categories_section),array(),12000,0);
echo '<select style="display:none;"  name="categories[]"  class="span" multiple="multiple" id="" style="height:220px;" >';
/*echo '<option value=""  > - set as parent - </option>';*/
$s_id = "";
if(isset($id) && isset($info['id_parent'])) {
$s_id = $info['id_parent'];
}
echo displayLevels($items,0,$s_id,$selected_options);
echo "</select>";
}
$items = $this->custom_fct->getCategoriesPages(0,$cond_cat,array(),12000,0);

//print '<pre>'; print_r($items); exit; 
$categories = $items;
echo '<select name="categories[]"  class="span" multiple="multiple" id="multi_select_categories" style="height:220px;"  onchange="getSelectedOptions(this)">';
/*echo '<option value=""  > - set as parent - </option>';*/
$s_id = "";
if(isset($id) && isset($info['id_parent'])) {
$s_id = $info['id_parent'];
}
echo displayLevels($items,0,$s_id,$selected_options);
echo "</select>";
echo form_error("categories","<span class='text-error'>","</span>");
?>


</label>
</div>
<div class="span6">

<?php echo br(); ?>
<div class="select_categories">
<?php if(isset($selected_options) && !empty($selected_options)) { ?>

<?php foreach($selected_options as $cat_id){
	$category_selected=$this->fct->getonerecord('categories',array('id_categories'=>$cat_id));
	echo "<div class='btn btn_select' id='selelect_btn_".$cat_id."'><a href=".site_url('back_office/categories/edit/'.$cat_id).">".$category_selected['title']."</a><span class='notification red remove_selected'
	 onclick='SelectedCategory(".$cat_id.")' >x</span></div>";
	?>
	<?php } ?>
	<?php } ?>
   
</div>
</div>
</div>

<div class="row-fluid">
<div class="span6">
<?php $categories_section='b2c';?>
<?php 
$cond['categories.section']=$categories_section;
$selected_options=array();
if(isset($id)) {
$selected_options = $this->fct->select_product_categories($id,$cond);
}


if(isset($h_selected_options)){
$selected_options =$h_selected_options ;	}
//CATEGORY SECTION.
echo form_label('<b>CATEGORIES('.$categories_section.')</b>', 'CATEGORIES');
$cond_cat=array();
$cond_cat['c.section']=$categories_section;
 if($supplier){
$cond_cat['no_id_categories']=522;
$items = $this->custom_fct->getCategoriesPages(0,array('c.id_categories'=>$cond_cat['no_id_categories'],'section'=>$categories_section),array(),12000,0);
echo '<select style="display:none;"  name="categories_'.$categories_section.'[]"  class="span" multiple="multiple" id="" style="height:220px;" >';
/*echo '<option value=""  > - set as parent - </option>';*/
$s_id = "";
if(isset($id) && isset($info['id_parent'])) {
$s_id = $info['id_parent'];
}
echo displayLevels($items,0,$s_id,$selected_options);
echo "</select>";
}
$items = $this->custom_fct->getCategoriesPages(0,$cond_cat,array(),12000,0);

//print '<pre>'; print_r($items); exit; 
$categories = $items;
echo '<select name="categories_'.$categories_section.'[]"  class="span" multiple="multiple" id="multi_select_categories_'.$categories_section.'" style="height:220px;"  onchange="getSelectedOptions(this)">';
/*echo '<option value=""  > - set as parent - </option>';*/
$s_id = "";
if(isset($id) && isset($info['id_parent'])) {
$s_id = $info['id_parent'];
}
echo displayLevels($items,0,$s_id,$selected_options);
echo "</select>";
echo form_error("categories_".$categories_section,"<span class='text-error'>","</span>");
?>


</label>
</div>
<div class="span6">

<?php echo br(); ?>
<div class="select_categories_b2c">
<?php if(isset($selected_options) && !empty($selected_options)) { ?>

<?php foreach($selected_options as $cat_id){
	$category_selected=$this->fct->getonerecord('categories',array('id_categories'=>$cat_id));
	echo "<div class='btn btn_select' id='selelect_btn_".$cat_id."'><a href=".site_url('back_office/categories/edit/'.$cat_id).">".$category_selected['title']."</a><span class='notification red remove_selected'
	 onclick='SelectedCategory(".$cat_id.")' >x</span></div>";
	?>
	<?php } ?>
	<?php } ?>
   
</div>
</div>
</div>
<?php } ?>


<!--<a href="#myModal" onclick="popup_categories();" data-toggle="modal" >[add more]</a>-->


<?php 
echo br();
echo form_label('<b>Product Full Description </b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"ckeditor","id" => "description".$lang, "rows" => 15, "cols" =>100 ));
echo br();
echo form_error("description","<span class='text-error'>","</span>");

?>
<?php if(!$supplier){?>
<div  id="dimensions"  <?php if(isset($set_as_non_exportable) && $set_as_non_exportable == 0) {}else{?>style="display:block;"<?php }?>>

<?php
echo form_fieldset("Weight & Size ");
?>
<div class="row-fluid">
<div class="span3">
<?php

echo form_label('<b>WEIGHT(G)&nbsp;<em>*</em></b>  ', 'WEIGHT&nbsp;<em>*</em>');
echo form_input(array('name' => 'weight', 'value' => set_value("weight",$info["weight"]),'class' =>'span' ));
echo br();
echo form_error("weight","<span class='text-error'>","</span>");


?></div>
<div class="span3">
<?php 
echo form_label('<b>Height(CM)&nbsp;<em>*</em></b> (DISABLED) ', 'Height&nbsp;<em>*</em>');
echo form_input(array('name' => 'height', 'value' => set_value("height",$info["height"]),'class' =>'span' ), null, " disabled");
echo form_error("height","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span3">
<?php
echo form_label('<b>length(CM)&nbsp;<em>*</em></b>  ', 'length&nbsp;<em>*</em>');
echo form_input(array('name' => 'length', 'value' => set_value("length",$info["length"]),'class' =>'span' ));
echo form_error("length","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span3">
<?php
echo form_label('<b>Width(CM)&nbsp;<em>*</em></b>  ', 'Width&nbsp;<em>*</em>');
echo form_input(array('name' => 'width', 'value' => set_value("width",$info["width"]),'class' =>'span' ));
echo form_error("width","<span class='text-error'>","</span>");
echo br();
?>
</div>

</div>
</div>
<?php } ?>


<br />
<?php if(!$supplier){?>
<div class="row-fluid" style="display:none;">
<div class="span6">
<?php
echo form_label('<b>Group Category</b> ', 'Group Category');

$cond=array();

$categories = $this->fct->getAll('group_categories','title asc');
$options = array();
$options[0] = 'Both';
foreach($categories as $val) {
	$options[$val['id_group_categories']] = $val['title'];
}

echo form_dropdown('id_group_categories',$options,set_value('id_group_categories',$info['id_group_categories'])); 
echo form_error("id_group_categories","<span class='text-error'>","</span>");
echo br();

?>
</div>


</div>
<?php } ?>

<div class="row-fluid" id="group_categories_professional" <?php if($info['id_group_categories']!=1 && !empty($info['id_group_categories'])) echo "style='display:block'";?>>
<?php
echo form_fieldset("PRICING");
?>

<div class="row-fluid">
<div class="span4">
<?php 

//PRICE SECTION.
echo form_label('<b>Trade Price *</b> ('.$this->config->item('default_currency').') ', 'PRICE');
echo form_input(array('name' => 'list_price', 'value' => set_value("list_price",$info["list_price"]),'class' =>'span',"onkeyup"=>"sellingPrice()" ));
echo form_error("list_price","<span class='text-error'>","</span>");
?>
</div>

<?php if(!$bool_kits){?>
<div class="span4">
<?php

echo form_label('<b>DISCOUNT</b>', 'DISCOUNT');
echo form_input(array('name' => 'discount', 'value' => set_value("discount",$info["discount"]),'class' =>'span',"onkeyup"=>"sellingPrice()" ));
echo form_error("discount","<span class='text-error'>","</span>");
?>
</div>
<?php if(!$supplier){?>
<div class="span4">
<?php


//DISCOUNT EXPIRATION SECTION.
echo form_label('<b>DISCOUNT EXPIRATION</b> ', 'DISCOUNT EXPIRATION');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'discount_expiration', 'value' => set_value("discount_expiration",$this->fct->date_out_formate($info["discount_expiration"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("discount_expiration","<span class='text-error'>","</span>");

echo form_label('<span style="color:#F03">Not Visible for the users,  system will use <br>it as expiry date for the discount.</span>');
?>
</div>
<?php  } ?>
<?php  } ?>

</div>

<div class="row-fluid">
<div class="span4">
<?php



//RETAIL PRICE&nbsp;<em>*</em>: RETAIL PRICE.
echo form_label('<b>RETAIL PRICE (SRP for consumer)<em>*</em></b> ('.$this->config->item('default_currency').') ', 'SUGGESTED RETAIL PRICE');
echo form_input(array('name' => 'retail_price', 'value' => set_value("retail_price",$info["retail_price"]),'class' =>'span' ));
echo form_error("retail_price","<span class='text-error'>","</span>");
echo form_label('<span style="color:#F03">This price will be used only as information as Suggested Retail Price.</span>  ', 'This price will be used only as information as Suggester Retail Price</span>.');
echo br();
?>
</div>
<div class="span4">
<?php

//PRICE SECTION.
echo form_label('<b>Price after Discount (Spa & Saloon price)</b> ('.$this->config->item('default_currency').') ', 'Selling Price');
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span','readonly'=>'true' ));
echo form_error("price","<span class='text-error'>","</span>");
?>
</div>


</div>
<?php echo form_fieldset_close();?>
</div>
<div class="row-fluid" id="group_categories_retail" <?php if($info['id_group_categories']!=2 && !empty($info['id_group_categories'])) echo "";?> style="display:none;">
<?php
echo form_fieldset("RETAIL PRICE");
?>

<div class="row-fluid">
<div class="span4">
<?php 

//PRICE SECTION.
echo form_label('<b>PRICE *</b> ('.$this->config->item('default_currency').') ', 'PRICE');
echo form_input(array('name' => 'retail_list_price', 'value' => set_value("retail_list_price",$info["retail_list_price"]),'class' =>'span',"onkeyup"=>"sellingRetailPrice()" ));
echo form_error("retail_list_price","<span class='text-error'>","</span>");
?>
</div>
<div class="span4">
<?php

echo form_label('<b>DISCOUNT</b>', 'DISCOUNT');
echo form_input(array('name' => 'retail_discount', 'value' => set_value("retail_discount",$info["retail_discount"]),'class' =>'span',"onkeyup"=>"sellingRetailPrice()" ));
echo form_error("retail_discount","<span class='text-error'>","</span>");
?>
</div>
<div class="span4">
<?php


//DISCOUNT EXPIRATION SECTION.
echo form_label('<b>DISCOUNT EXPIRATION</b> ', 'DISCOUNT EXPIRATION');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'retail_discount_expiration', 'value' => set_value("retail_discount_expiration",$this->fct->date_out_formate($info["retail_discount_expiration"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("retail_discount_expiration","<span class='text-error'>","</span>");

echo form_label('<span style="color:#F03">Not Visible for the users,  system will use <br>it as expiry date for the discount.</span>');
?>
</div>

</div>
<div class="row-fluid">

<div class="span4">
<?php

//PRICE SECTION.
echo form_label('<b>Selling Price</b> ('.$this->config->item('default_currency').') ', 'Selling Price');
echo form_input(array('name' => 'retail2_price', 'value' => set_value("retail_price",$info["retail2_price"]),'class' =>'span','readonly'=>'true' ));
echo form_error("retail2_price","<span class='text-error'>","</span>");
?>
</div>


</div>
<?php echo form_fieldset_close();?></div>



<?php if(isset($id)){ ?>
<?php $segements=$this->fct->getAll_cond('product_price_segments','sort_order',array('id_products'=>$id));
   $data2['segements']=$segements;?>
<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2 style="text-transform:uppercase;">Multi buy</h2>
              <div class="box-icon pull-right">
             
              <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
              <a id="product_price_segments_btn" class="pull-right fancyClick3" href="<?php echo site_url('back_office/product_price_segments/add?id_product='.$id);?>" style="margin-top:5px;margin-right:5px;text-transform:uppercase;<?php if(count($segements)>=3) echo "display:none;"; ?>">[Add Price Segements]</a>
              
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
              <div class="priceSegements">
        
<?php echo $this->load->view('back_office/blocks/products_segements',$data2,true);?>
</div>




              </div>
            </div>
          </div>
        </div>
      
        

  <br /> 
  <?php } ?>
  
  <?php if(!$supplier){?>
<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2 style="text-transform:uppercase;">Related Products (products to display in the inside page below this product)</h2>
              <div class="box-icon pull-right">
             
              <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
              
              
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
           <div class="row-fluid">   
<div class="span3" id="title_bx" >
<label>Product Description</label>
<input type="text" name="related_product" value="" class="searchRelatedProducts" id="searchRelatedProducts" />
<input type="hidden" id="id_related_product" value="" />
<div class="loader_1"></div>
</div>

<div class="span3" ><a class="btn btn-primary"  style="margin-left:10px; margin-top:22px;"  onclick="addRelatedProduct()">Add</a></div>
</div>
<div class="related_products">
<?php 
$selected_options2=array();
if(isset($id)) {
$selected_options2 = $this->fct->select_product_related($id);

}
if(isset($h_selected_options2)){
$selected_options2 =$h_selected_options2 ;	} ?>
<?php if(isset($selected_options2) && !empty($selected_options2)) { ?>

<?php foreach($selected_options2 as $id_pro){
	$product_selected=$this->fct->getonerecord('products',array('id_products'=>$id_pro));
	echo "<div class='btn btn_select' id='related_product_".$id_pro."'><input type='hidden' value='".$id_pro."' name='related_products_arr[]' ><a href=".site_url('back_office/products/edit/'.$id_pro).">".$product_selected['title']."</a><span class='notification red remove_selected'
	 onclick='removeRelatedProduct(".$id_pro.")' >x</span></div>";
	?>
	<?php } ?>
	<?php } ?>

</div>

              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        
<?php

//MILES SECTION.

/*echo form_label('<b>Miles</b> ', 'Miles');
$miles = $this->fct->getAll('miles','sort_order');
$options = array();
$options[""] = '--Select Miles--';
foreach($miles as $mile) {
	$options[$mile['id_miles']] = $mile['title'];
}

echo form_dropdown('id_miles',$options,set_value('id_miles',$info['id_miles'])); 
echo form_error("id_miles","<span class='text-error'>","</span>");
echo br();*/

/*echo form_label('<b>Redeem Miles&nbsp;<em>*</em>:</b>', 'Redeem Miles');
echo form_input(array("name" => "redeem_miles", "value" => set_value("redeem_miles",$info["redeem_miles"]),"class" =>"span" ));
echo form_error("redeem_miles","<span class='text-error'>","</span>");
echo br();*/


echo '<div class="row-fluid">';
if(isset($info['set_general_miles']) && !isset($set_general_miles)){
$set_general_miles = $info['set_general_miles'];
}
?>
<div class="row-fluid">
<?php if(!$supplier){?>
<label class="label-left"><input type="checkbox" name="set_general_miles" id="set_general_miles" <?php if(isset($set_general_miles) && $set_general_miles == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Generate miles</b>

<div id="general_miles_input" <?php if(isset($set_general_miles) && $set_general_miles == 1) {}else{?>style="display:none;"<?php }?> >
<div class="row-fluid">
<div class="span6">
<?php
echo form_label('<b>miles(%):</b>  ', 'Generate Miles&nbsp;<em>*</em>:');
echo form_input(array('name' => 'general_miles', 'value' => set_value("general_miles",$info["general_miles"]),'class' =>'span',"onkeyup"=>"miles_number()","onkeydown"=>"miles_number()"  ));
echo form_error("general_miles","<span class='text-error'>","</span>");
 ?>
 </div> 
 
 <div class="span6">
<?php

//PRICE SECTION.
echo form_label('<b>Miles(No):', 'Miles');
echo form_input(array('name' => 'miles', 'value' => set_value("miles",$info["miles"]),'class' =>'span','id'=>'miles' ));
echo form_error("miles","<span class='text-error'>","</span>");
?>
</div>
 </div> </div>
</label>

<?php
if(isset($info['set_as_redeemed_by_miles']) && !isset($set_as_redeemed_by_miles))
$set_as_redeemed_by_miles = $info['set_as_redeemed_by_miles'];
?>

<label class="label-left"><input type="checkbox" name="set_as_redeemed_by_miles" id="set_as_redeemed_by_miles" <?php if(isset($set_as_redeemed_by_miles) && $set_as_redeemed_by_miles == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as redeemed by miles</b>

<div id="redeemed_miles_input" <?php if(isset($set_as_redeemed_by_miles) && $set_as_redeemed_by_miles == 1) {}else{?>style="display:none;"<?php }?> >
<div class="row-fluid">
<div class="span12">
<?php
echo form_label('<b>Amount Of Miles:</b>  ', 'Amount Of Miles&nbsp;<em>*</em>:');
echo form_input(array("name" => "redeem_miles", "value" => set_value("redeem_miles",$info["redeem_miles"]),"class" =>"span" ));
echo form_error("redeem_miles","<span class='text-error'>","</span>");
/*echo form_input(array('name' => 'amount_of_miles', 'value' => set_value("amount_of_miles",$info["amount_of_miles"]),'class' =>'span' ));*/
 ?>
  </div></div> 
 </div>
</label>

<?php
if(isset($info['promote_to_front']) && !isset($promote_to_front))
$promote_to_front = $info['promote_to_front'];
?>
<label class="label-left"><input type="checkbox" name="promote_to_front" <?php if(isset($promote_to_front) && $promote_to_front == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Promote to front</b></label>

<?php } ?>

<?php if(!$supplier){?>
<?php
if(isset($display_in_home)){}else{
if(isset($info['display_in_home']))
$display_in_home = $info['display_in_home'];}
?>
<label ><input type="checkbox" id="display_in_home" name="display_in_home" <?php if(isset($display_in_home) && $display_in_home == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as featured</b></label>
<?php } ?>

</div>
<?php
if(isset($info['set_as_new']) && !isset($set_as_new))
$set_as_new = $info['set_as_new'];
?>

<label ><input type="checkbox" name="set_as_new" <?php if(isset($set_as_new) && $set_as_new == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as new Arrival</b></label>

<?php
if(isset($info['set_as_special']) && !isset($set_as_special))
$set_as_special = $info['set_as_special'];
?>

<label ><input type="checkbox" name="set_as_special" <?php if(isset($set_as_special) && $set_as_special == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as Offers</b></label>




<?php
if(isset($set_as_clearance)){}else{
if(isset($info['set_as_clearance']))
$set_as_clearance = $info['set_as_clearance'];}
?>
<label ><input type="checkbox" id="set_as_clearance" name="set_as_clearance" <?php if(isset($set_as_clearance) && $set_as_clearance == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>set as clearance</b></label>

<?php if(!$supplier){?>
<?php

if(isset($set_as_non_exportable)){}else{
if(isset($info['set_as_non_exportable']))
$set_as_non_exportable = $info['set_as_non_exportable'];}
?>
<label ><input type="checkbox" id="set_as_non_exportable" name="set_as_non_exportable" <?php if(isset($set_as_non_exportable) && $set_as_non_exportable == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>set as non-exportable product</b> (means only deliverable in UAE)</label>
<?php } ?>
<?php
if(isset($info['set_as_soon']) && !isset($set_as_soon))
$set_as_soon = $info['set_as_soon'];
?>

<label><input type="checkbox" name="set_as_soon" <?php if(isset($set_as_soon) && $set_as_soon == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as soon</b> (only for products when available in two weeks)</label>


<?php
if(isset($set_as_pro)){}else{
if(isset($info['set_as_pro']))
$set_as_pro = $info['set_as_pro'];}
?>
<label><input type="checkbox" id="set_as_pro" name="set_as_pro" <?php if(isset($set_as_pro) && $set_as_pro == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as pro</b> (only for professional use products)</label>


<?php /*?><?php
if(isset($set_as_trends_updates)){}else{
if(isset($info['set_as_trends_updates']))
$set_as_trends_updates = $info['set_as_trends_updates'];}
?>
<label><input type="checkbox" id="set_as_trends_updates" name="set_as_trends_updates" <?php if(isset($set_as_trends_updates) && $set_as_trends_updates == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as trends and updates</b></label>

<?php */?>


</div>
<?php 
echo br();
echo form_fieldset("ADDITIONAL TABS  (optional)</small>");
echo form_label('<span style="color:#F03">Will appear as tabs in the product inside page.');
//DESCRIPTION SECTION.
?>
<input name="tabs_files" type="hidden" />
<?php echo form_error("tabs_files","<span class='text-error'>","</span>");?>
<div class="input_fields_wrap repeat">
<?php
if(isset($product_informations)){}else{
if(isset($id)) {
	
$product_informations=$this->fct->getAll_cond("product_information",'sort_order',array('id_products'=>$id));}}

if(isset($product_informations) && !empty($product_informations)){
 foreach($product_informations as $val){?>
<div id="section_repeat_<?php echo $val["id_product_information"];?>">

<div class="contactrow">
<div class="contactlabel">Tab Title <span><small class="blue">(Max <?php echo getLimitText('title');?> Characters)</small></span></div>
<input class="span" maxlength="<?php echo getLimitText('title');?>" type="text" value="<?=set_value('title_product_info[]',$val['title'])?>" id="title_<?php echo $val['id_product_information'];?>" onKeyUp="limitText(this.form.title_<?php echo $val['id_product_information'];?>,0,<?php echo getLimitText('title');?>);" name="title_product_info[<?php echo $val['id_product_information'];?>]">
</div>

<div class="contactrow">
<div class="contactlabel">Tab Description</div>
<textarea id="description_<?php echo $val['id_product_information'];?>" onKeyDown="limitText(this.form.description_<?php echo $val['id_product_information'];?>,0,<?php echo getLimitText('description');?>);" onKeyUp="limitText(this.form.description_<?php echo $val['id_product_information'];?>,0,<?php echo getLimitText('description');?>);" class="ckeditor" rows="3" cols="100" name="description_product_info[<?php echo $val['id_product_information'];?>]" ><?=set_value('description_product_info[]',$val['description'])?></textarea>
</div>

<div class="contactrow">
<div class="contactlabel">File for this Tab <small class="blue">(Max Size <?php echo $size_arr['size_mega'];?>Mb)</small></div>
<input class="input-large" type="file"  name="file_product_info_<?php echo $val['id_product_information'];?>" onchange="getfilesize(1)">
<?php if(!empty($val['file'])){?>
<input class="input-large" type="hidden"  name="file_product_info_label_<?php echo $val['id_product_information'];?>" value="<?php echo $val['file'];?>" />
<span id="file_<?php echo $val["id_product_information"]; ?>"> 
<a href="<?= base_url(); ?>uploads/product_information/<?= $val["file"]; ?>" target="_blank"  >
show file
</a>
&nbsp;&nbsp;&nbsp;
<a class="cur" onclick='removeFile("product_information","file","<?php echo $val["file"]; ?>",<?php echo $val["id_product_information"]; ?>)' >
<img src="<?= base_url(); ?>images/delete.png"  /></a></span>
<?php } ?>
</div>

<div class="contactrow">
<div class="contactlabel">File(2) for this Tab <small class="blue">(Max Size <?php echo $size_arr['size_mega'];?>Mb)</small></div>
<input class="input-large" type="file"  name="file2_product_info_<?php echo $val['id_product_information'];?>" onchange="getfilesize(1)">
<?php if(!empty($val['file2'])){?>
<input class="input-large" type="hidden"  name="file2_product_info_label_<?php echo $val['id_product_information'];?>" value="<?php echo $val['file2'];?>" />
<span id="file2_<?php echo $val["id_product_information"]; ?>"> 
<a href="<?= base_url(); ?>uploads/product_information/<?= $val["file2"]; ?>" target="_blank"  >
show file
</a>
&nbsp;&nbsp;&nbsp;
<a class="cur" onclick='removeFile("product_information","file2","<?php echo $val["file2"]; ?>",<?php echo $val["id_product_information"]; ?>)' >
<img src="<?= base_url(); ?>images/delete.png"  /></a></span>
<?php } ?>
</div>

<div class="contactrow">
<div class="contactlabel">Youtube link <small class="blue">(ex:https://www.youtube.com/watch?v=C0DPdy98e4c)</small></div>
<input class="span"  type="text" value="<?=set_value('video_product_info[]',$val['video'])?>" name="video_product_info[<?php echo $val['id_product_information'];?>]">
</div>





<a onclick='removeRow("product_information","description","<?php echo $val["id_product_information"]; ?>")' class="remove_d"><i class="icon-remove-sign"></i> Remove this Tab</a>
<span id="description_<?php $val["id_product_information"];?>"></span>
<hr>
</div>
<? }}?>
</div>
<div class="contactrow field">
	<button class="add_more btn add_field_button">Add Tab</button>
</div>
<?php
echo br();

if(!checkIfSupplier_admin()){
echo form_fieldset("Meta Tags");
//PAGE TITLE SECTION.
?>
<div class="row-fluid">
<div class="span6">
<?php
echo form_label('<b>Page Title:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span","onKeyDown"=>"limitText(this.form.meta_title,0,".getLimitText('page_title').");","onKeyUp"=>"limitText(this.form.meta_title,0,".getLimitText('page_title').");" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();?>
</div>
<div class="span6">
<?php 
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<div class="row-fluid">
<div class="span6">
<?php
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100,"onKeyDown"=>"limitText(this.form.meta_description,0,".getLimitText('meta_description').");","onKeyUp"=>"limitText(this.form.meta_description,0,".getLimitText('meta_description').");"));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<?php 
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100,"onKeyDown"=>"limitText(this.form.meta_keywords,0,".getLimitText('meta_description').");","onKeyUp"=>"limitText(this.form.meta_keywords,0,".getLimitText('meta_description').");" ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<?php
echo form_fieldset_close();


}

$user = $this->fct->getonerecord('user',array('id_user' => $this->session->userdata('user_id')));
if(!checkIfSupplier_admin()){

echo br();
echo form_fieldset("PRODUCT STATUS");

echo form_label('<b>STATUS</b>', 'STATUS');
$options = array();
$options[''] = '-select status-';
$options[0] = 'Published';

$options[1] = 'Unpublished';
$options[2] = 'Under Review';
?>
<label>
<?php
echo form_dropdown('status',$options,set_value('status',$info['status'])); 
echo form_error("status","<span class='text-error'>","</span>");


?>
</label>

<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Client</label>


<?php
echo br(); } ?>


<?php echo form_fieldset_close();?>

<?php

echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
if(!$bool_kits){
echo form_submit(array('name' => 'submit','value' => 'Save and Continue','class' => 'btn btn-primary','style' => 'margin-left:10px') );
echo form_submit(array('name' => 'submit','value' => 'Save and manage gallery','class' => 'btn btn-primary','style' => 'margin-left:10px') );
}
/*if(isset($id)) {
	echo '<a href="'.route_to("products/details/".$info['title_url']).'?preview=1" target="_blank" class="btn btn-primary" style="margin-left:10px">Preview Product</a>';
}*/
echo '</p>';


echo form_close();
?>

<script>
function getfilesize(maxSize)
 {
	
  var iSize = ($(".ccfile")[0].files[0].size / 1024);
var maxSize1=maxSize/100;
        if (((iSize / 1024) / 1024) > maxSize1)
        {
            
   iSize = (Math.round(((iSize / 1024) / 1024) * 100) / 100);
   $('.ccfile').val('');alert('The file you are trying to upload exceeds '+maxSize+'MB');
            //$("#ccc_"+id).html( iSize + "Gb");
        }
        else
        {
            iSize = (Math.round((iSize / 1024) * 100) / 100)

   if(iSize>maxSize)
   {
    $('.ccfile').val('');alert('Thes file you are trying to upload exceeds '+maxSize+'MB')
   }
        }

 }

</script>
