<script>
$(document).ready(function(){
	$('.fancyClick').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 
		 afterClose: function() {}});
	$('.fancyClick2').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 
		 afterClose: function() {
		var url="";
		var id=$('#id_stock_active').val();
		var url="<?php echo site_url('back_office/products/getQuantity/stock');?>/"+id;
		
		$.post(url,{id : ""},function(data){
			$('#remanin_quantity').html(data);
		});
			/*location.reload();*/
			}});
		
});

function sellingPrice(id){
	var list_price=$("#list_price_"+id).val();
	var discount=$("#discount_"+id).val();

	if(discount>=0 && discount<=100){
	var price = roundToTwo(list_price - ((list_price * discount) / 100));
	$("#price_"+id).val(price);}else{
		$("#discount_"+id).val('0');
		$("#price_"+id).val(list_price);
		alert('Discount should be between 0 & 100.');
		}
miles_number(id);
	}
	
function sellingRetailPrice(id){
	var list_price=$("#retail_list_price_"+id).val();
	var discount=$("#retail_discount_"+id).val();

	if(discount>=0 && discount<=100){
	var price = roundToTwo(list_price - ((list_price * discount) / 100));
	$("#retail2_price_"+id).val(price);}else{
		$("#retail_discount_"+id).val('0');
		$("#retail2_price_"+id).val(list_price);
		alert('Discount should be between 0 & 100.');
		}
miles_number(id);
	}	
	
	function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}

	function miles_number(id){
	
	var general_miles=$("#general_miles_"+id).val();

	var price=$("#price_"+id).val();
	
	if(general_miles>=0 && general_miles<=100){
	var price_miles=(general_miles*price)/100;
	var miles=Math.round(price_miles*10);
		
	$("#miles_"+id).val(miles);}else{
		$("#miles_"+id).val(0);
		$("#general_miles_"+id).val(0);
		alert('Miles(%) should be between 0 & 100.');
		}
	}

$(function(){
	$('.fancyClick2').click(function(){
		var id=this.id.replace('stock_','');
		$('#id_stock_active').val(id);
	});
$('#checkAllAuto1').click(function(){
	if($('#checkAllAuto1').is(':checked')) {
		//$("input[type=checkbox]").attr('checked','checked');
		$('.cehcklist_1').each(function(){
			//$(this).attr("checked",null);
			$(this).attr("checked","checked");
		});
	}
	else {
		//$("input[type=checkbox]").removeAttr('checked');
		$('.cehcklist_1').each(function(){
			$(this).attr("checked",null);
		});
	}
});
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('.set_general_miles').change(function(){
		 var obj=$(this).parent().parent().find('.general_miles_input');
		 if(obj.is(":visible") && $(this).prop('checked')==false){
			obj.stop(true,true).slideUp('fast');
			 }else{
			obj.stop(true,true).slideDown('fast'); }
    	});
		
		$('.set_as_redeemed_by_miles').change(function(){
			var obj=$(this).parent().parent().find('.redeemed_miles_input');
		 if(obj.is(":visible") && $(this).prop('checked')==false){
			obj.stop(true,true).slideUp('fast');
			 }else{
			obj.stop(true,true).slideDown('fast'); }
    	});});</script>
<?php
$sego =$this->uri->segment(4);
$gallery_status="";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/attributes?id_product='.$product['id_products']); ?>" >Attributes</a></li><span class="divider">/</span>
<li class="active">Stock</li>
</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/products/update_stock', $attributes); 
?>  
<input type="hidden" name="id_product" value="<?php echo $product['id_products']; ?>" />	
<input type="hidden" id="id_stock_active" value="" />			
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="5" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="5" align="center" style="text-align:center">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th><!--<input type="checkbox" id="checkAllAuto1" style="margin:0" />--> ITEM</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<? 
if(count($stock) > 0){
$i=0;
foreach($stock as $val){
	
	
$i++; 
$arr = serializeStock($val,$product['id_products']);
$product_in_stock = $this->fct->getonerow('products_stock',array('combination_id'=>$arr['id'],'id_products'=>$product['id_products']));
$cl = '';
if(!empty($product_in_stock)) {
	$item['sku'] = $product_in_stock['sku'];
	$item['miles'] = $product_in_stock['miles'];
	$item['barcode'] = $product_in_stock['barcode'];
	$item['threshold'] = $product_in_stock['threshold'];
	$item['quantity'] = $product_in_stock['quantity'];
	$item['list_price'] = $product_in_stock['list_price'];
	$item['price'] = $product_in_stock['price'];
	$item['discount_status'] = $product_in_stock['discount_status'];
	$item['id_miles'] = $product_in_stock['id_miles'];
	$item['discount'] = $product_in_stock['discount'];
	$item['redeem_miles'] = $product_in_stock['redeem_miles'];
	$item['set_as_redeemed_by_miles'] = $product_in_stock['set_as_redeemed_by_miles'];
	$item['set_general_miles'] = $product_in_stock['set_general_miles'];
	$item['general_miles'] = $product_in_stock['general_miles'];
	$item['retail_price'] = $product_in_stock['retail_price'];
	$item['discount_expiration'] = '';
	if(!empty($product_in_stock['discount_expiration']) && $product_in_stock['discount_expiration'] != '0000-00-00')
	$item['discount_expiration'] = $this->fct->date_out_formate($product_in_stock["discount_expiration"]);
	if($product_in_stock['status'] == 1)
	$cl = 'checked="checked"';
	$item['hide_price'] = $product_in_stock['hide_price'];
	
	$item['retail2_price'] = $product_in_stock['retail2_price'];
	$item['retail_list_price'] = $product_in_stock['retail_list_price'];
	$item['retail_discount'] = $product_in_stock['retail_discount'];
	$item['retail_discount_status'] = $product_in_stock['retail_discount_status'];
	$item['retail_discount_expiration'] = '';
	if(!empty($product_in_stock['retail_discount_expiration']) && $product_in_stock['retail_discount_expiration'] != '0000-00-00')
	$item['retail_discount_expiration'] = $this->fct->date_out_formate($product_in_stock["retail_discount_expiration"]);
}
else {
	$item['miles'] = $product['miles'];
	$item['sku'] = $product['sku'].'-'.$i;
	$item['barcode'] = "";
	if(!empty($product['barcode'])){
	$item['barcode'] = $product['barcode'].'-'.$i;}
	$item['threshold'] = $product['threshold'];
	$item['quantity'] = 0;
	$item['discount_status'] = $product['discount_status'];
	$item['id_miles'] = $product['id_miles'];
	$item['list_price'] = $product['list_price'];
	$item['redeem_miles'] = $product['redeem_miles'];
	$item['set_as_redeemed_by_miles'] = $product['set_as_redeemed_by_miles'];
	$item['set_general_miles'] = $product['set_general_miles'];
	$item['general_miles'] = $product['general_miles'];
	$item['price'] = $product['price'];
	$item['retail_price'] = $product['retail_price'];
	$item['discount'] = $product['discount'];
	$item['discount_expiration'] = '';
	
	if(!empty($product['discount_expiration']) && $product['discount_expiration'] != '0000-00-00')
	$item['discount_expiration'] = $this->fct->date_out_formate($product["discount_expiration"]);
	
	
	$item['retail2_price'] = $product['retail2_price'];
	$item['retail_list_price'] = $product['retail_list_price'];
	$item['retail_discount'] = $product['retail_discount'];
	$item['retail_discount_status'] = $product['retail_discount_status'];
	$item['retail_discount_expiration'] = '';
	if(!empty($product['retail_discount_expiration']) && $product['retail_discount_expiration'] != '0000-00-00')
	$item['retail_discount_expiration'] = $this->fct->date_out_formate($product["retail_discount_expiration"]);

	
	$item['hide_price'] = $product['hide_price'];
}
// let us make it checked all the time, the admin will set the quantity to 0 to disable it
$cl = 'checked="checked"';
?>
<tr id="stock-<?php echo $product_in_stock['id_products_stock'];?>">
<td valign="middle" style="width:5%;">
<input type="hidden" name="cehcklist_ids[]" value="<?php echo $arr['id']; ?>" />
<!--<input type="checkbox" class="cehcklist_1" name="cehcklist[<?php echo $arr['id']; ?>]" <?php echo $cl; ?> value="1" />-->
<input type="hidden" name="items[]" value="<?php echo $arr['str']; ?>" />
<b><?php echo $arr['title']; ?></b>
</td>
<td valign="middle" style="width:20%;">
<table border="0" cellpadding="5" cellspacing="0">
<tr>
<td><label>SKU</label> <input type="text" name="sku[]" value="<?php echo $item['sku']; ?>" /></td>
</tr>
<tr>
<td><label><strong>REMAINING  QUANTITY: </strong><span id="remanin_quantity"><?php echo $item['quantity'];?></span></label>
 <input type="hidden" name="quantity[]" value="<?php echo $item['quantity']; ?>" /></td>
</tr>
<tr>
<td><label><strong>Threshold: <small class="blue">(Min Quantity to receive stock notification)</small></strong></label>
 <input type="text" name="threshold[]" value="<?php echo $item['threshold']; ?>" /></td>
</tr>

<tr>
<td><label>BARCODE</label> <input type="text" name="barcode[]" value="<?php echo $item['barcode']; ?>" /></td>
</tr>

<!--<tr>
<td><select name="hide_price[]">
<option value="0" <?php if($item['hide_price'] == 0) echo 'selected="selected"'; ?>>Display price</option>
<option value="1" <?php if($item['hide_price'] == 1) echo 'selected="selected"'; ?>>Do not display price</option>
</select></td>
</tr>-->
<input type="hidden" name="hide_price[]" value="0" /> 
</table>
</td>
<td valign="middle"  style="width:25%;">
<table border="0" cellpadding="5" cellspacing="0">

<tr>
<?php $miles = $this->fct->getAll('miles','sort_order');
?>
<td>
<!--<label><b>Miles :</b></label>
<select name="miles[]">
<option  value="" >-Select Miles-</option>
<?php foreach($miles as $mile){?>
<option value="<?php echo $mile['id_miles'];?>" <?php if($mile['id_miles']==$item['id_miles']) echo 'selected="selected"'; ?>><?php echo $mile['title'];?></option>
<?php } ?>

</select>-->

<?php
if(isset($item['set_general_miles']))
$set_general_miles = $item['set_general_miles'];
?>
<div>
<label><input type="checkbox" name="set_general_miles[]"  class="set_general_miles" <?php if(isset($set_general_miles) && $set_general_miles == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set general miles</b></label>
<div class="general_miles_input" <?php if(isset($set_general_miles) && $set_general_miles == 1) {}else{?>style="display:none;"<?php }?> >
<div class="row-fluid">

<?php
echo form_label('<b>General Miles(%):</b>  ', 'General Miles&nbsp;<em>*</em>:');
echo form_input(array('name' => 'general_miles[]', 'value' => set_value("general_miles",$item["general_miles"]),'class' =>'span',"onkeyup"=>"miles_number(".$i.")","onkeydown"=>"miles_number(".$i.")","id"=>'general_miles_'.$i ));
echo form_error("general_miles","<span class='text-error'>","</span>");
 ?>

 
 </div>
 <div class="row-fluid">
<?php

//PRICE SECTION.
echo form_label('<b>Miles(No):', 'Miles');
echo form_input(array('name' => 'miles[]', 'value' => set_value("miles",$item["miles"]),'class' =>'span',"id"=>'miles_'.$i ));
echo form_error("miles","<span class='text-error'>","</span>");
?>
</div> </div></div></td>
</tr>
<tr>
<td>

 
<?php
if(isset($item['set_as_redeemed_by_miles']) || $item['set_as_redeemed_by_miles']==0)
$set_as_redeemed_by_miles = $item['set_as_redeemed_by_miles'];
?>
 <div>
<label><input type="checkbox" name="set_as_redeemed_by_miles[]" class="set_as_redeemed_by_miles" <?php if(isset($set_as_redeemed_by_miles) && $set_as_redeemed_by_miles == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as redeemed by miles</b></label>
<div class="redeemed_miles_input" <?php if(isset($set_as_redeemed_by_miles) && $set_as_redeemed_by_miles == 1) {}else{?>style="display:none;"<?php }?> >
<div class="row-fluid">

<?php
echo form_label('<b>Amount Of Miles:</b>  ', 'Amount Of Miles&nbsp;<em>*</em>:');
echo form_input(array("name" => "redeem_miles[]", "value" => set_value("redeem_miles",$item["redeem_miles"]),"class" =>"span" ));
echo form_error("redeem_miles","<span class='text-error'>","</span>");
/*echo form_input(array('name' => 'amount_of_miles', 'value' => set_value("amount_of_miles",$info["amount_of_miles"]),'class' =>'span' ));*/
 ?>
 </div> </div> 
 </div> 
</td>
</tr>
</table>
</td>
<td valign="middle"  style="width:25%;">
<table border="0" cellpadding="5" cellspacing="0">
<tr><td><b>Professional Price</b></td></tr>
<tr>
<td><label>List Price</label> <input type="text" name="list_price[]" value="<?php echo $item['list_price']; ?>"  onkeyup="sellingPrice(<?php echo $i;?>)"   id="list_price_<?php echo $i;?>" /></td>
</tr>
<tr>
<td><label>Discount (%)</label> <input type="text" name="discount[]" id="discount_<?php echo $i;?>" onkeyup="sellingPrice(<?php echo $i;?>)" value="<?php echo $item['discount']; ?>" /></td>
</tr>
<tr>
<td><label>Expiry Date</label> <div class="input-append date" data-date="" data-date-format="dd/mm/yyyy"><input type="text" name="discount_expiration[]" class="input-small" value="<?php echo $item['discount_expiration']; ?>" /><span class="add-on"><i class="icon-th"></i></span></div>
<span style="color:#F03">Not Visible for the users, system will use it as expiry date for the discount.</span></td>
</tr>

<tr>
<td><label>Selling Price</label> <input type="text" name="price[]" value="<?php echo $item['price']; ?>" id="price_<?php echo $i;?>"  readonly="readonly"/>
</td>
</tr>

<tr>
<td><label>SUGGESTED RETAIL PRICE</label> <input type="text" name="retail_price[]" value="<?php echo $item['retail_price']; ?>" />
<br />
<span style="color:#F03">This price will be used only as information as Suggested  Retail Price</span>

</td>
</tr>
</table>
</td>
<td valign="middle"  style="width:25%;">
<table border="0" cellpadding="5" cellspacing="0">
<tr><td><b>Retail Price</b></td></tr>
<tr>
<td><label>List Price</label> <input type="text" name="retail_list_price[]" value="<?php echo $item['retail_list_price']; ?>"  onkeyup="sellingRetailPrice(<?php echo $i;?>)"   id="retail_list_price_<?php echo $i;?>" /></td>
</tr>
<tr>
<td><label>Discount (%)</label> <input type="text" name="retail_discount[]" id="retail_discount_<?php echo $i;?>" onkeyup="sellingRetailPrice(<?php echo $i;?>)" value="<?php echo $item['retail_discount']; ?>" /></td>
</tr>
<tr>
<td><label>Expiry Date</label> <div class="input-append date" data-date="" data-date-format="dd/mm/yyyy"><input type="text" name="retail_discount_expiration[]" class="input-small" value="<?php echo $item['retail_discount_expiration']; ?>" /><span class="add-on"><i class="icon-th"></i></span></div>
<span style="color:#F03">Not Visible for the users, system will use it as expiry date for the discount.</span></td>
</tr>

<tr>
<td><label>Selling Price</label> <input type="text" name="retail2_price[]" value="<?php echo $item['retail2_price']; ?>" id="retail2_price_<?php echo $i;?>"  readonly="readonly"/>
</td>
</tr>


</table>
</td>

<td  style="width:25%;"><?php if(isset($product_in_stock['id_products_stock']) && !empty($product_in_stock['id_products_stock'])) {?><a class="label label-success fancyClick"  href="<?php echo site_url('back_office/products/stock_price_segments/'.$product['id_products'].'/'.$product_in_stock['id_products_stock']); ?>">Manage multi buy</a>
<br />
<!--<a style="margin-top:10px;" id="stock_<?php echo $product_in_stock['id_products_stock'];?>" class="loadFuncy label label-success fancyClick2" href="<?php echo site_url('back_office/products/purchases?id_stock='.$product_in_stock['id_products_stock']); ?>">Manage Quantity</a>-->
<?php }?></td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="5" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  
<?php
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
?>	
<? echo form_close();  ?>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 