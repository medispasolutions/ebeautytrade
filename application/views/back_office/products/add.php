
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/products','<b>List products</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if(isset($id)) {
		/*	if ($this->acl->has_permission('stock','index')){
				$ul['stock'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/stock').'" class="btn btn-info top_btn">Stock</a>');
			}*/
			if ($this->acl->has_permission('product_attributes','index')){
				/*$ul['stock'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/stock/'.$id).'" class="btn btn-info top_btn">Stock</a>');*/
					
				$ul['attributes'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/attributes?id_product='.$id).'" class="btn btn-info top_btn">Attributes</a>');
			/*	$ul['price_segments'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/product_price_segments?id_product='.$id).'" class="btn btn-info top_btn">Price Segments</a>');*/
		/*		$ul['quantity'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/purchases?id_product='.$id).'" class="btn btn-info  fancyClick2 top_btn">Manage Quantity</a>');*/
			}
			$ul['gallery'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/control/manage_gallery/products/'.$id).'" class="btn btn-info top_btn">Manage Gallery</a>');
/*			$ul['manage_product_information'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/product_information?id_product='.$id).'" class="btn btn-info top_btn">Manage Product Information</a>');*/
/*$ul['brand_training'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/brand_certified_training/edit/'.$id).'" class="btn btn-info top_btn">Brand Certified Training</a>');*/
			}
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/products/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);			
			}
?>

</div>
<div class="hundred pull-left">   


<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
?>

<?php $this->load->view('back_office/products/add_content');?>




</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>
