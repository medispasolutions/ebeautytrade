<?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recipes of: <?=$chef['name']?></title>
<link href="<?= base_url(); ?>css/bootstrap.css" rel="stylesheet" media="screen">
<script src="<?= base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script>
function delete_row(id){
window.location="<?= site_url("back_office/products/delete_stock_price_segment"); ?>/"+id;
return false;
}
</script>
</head>
<body>
<div class="container-fluid">
<div class="row-fluid">
<div class="span10" style="width:100%;">
<div class="hundred pull-left">   	
<div class="container-fluid">
<div class="row-fluid">

<div class="span12">
 <h3><?php echo $product['title'].': '.$stock['combination']; ?></h3>
<div id="result"></div> 
<? 

$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/products/submit_stock_price_segments', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["product"] = "";
$info["min_qty"] = "";
$info["price"] = "";
$info["discount"] = "";
$info["discount_expiration"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
/*if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}*/
echo form_hidden("id_products",$id_products);
echo form_hidden("id_stock",$id_stock);
//TITLE SECTION.
/*echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();*/
//MIN QTY&nbsp;<em>*</em>: SECTION.
echo form_label('<b>MIN QTY&nbsp;<em>*</em>:</b>', 'MIN QTY&nbsp;<em>*</em>:');
echo form_label("please specify the minimum quantity of this offer, should be in integer","help_text",array("class"=>"yellow"));
echo form_input(array('name' => 'min_qty', 'value' => set_value("min_qty",$info["min_qty"]),'class' =>'span' ));
echo form_error("min_qty","<span class='text-error'>","</span>");
echo br();
//PRICE SECTION.
echo form_label('<b>Unit Price </b> ('.$this->config->item("default_currency").')', 'PRICE');
echo form_label("Should be Decimal format 10,2","help_text",array("class"=>"yellow"));
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span' ));
echo form_error("price","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">
<a style="" href="'.site_url('back_office/products/stock_price_segments/'.$id_products.'/'.$id_stock).'"><< go back</a>
';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 