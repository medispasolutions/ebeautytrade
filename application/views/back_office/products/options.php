<script>
$(function(){
$('input[type=checkbox]').click(function(){
	var checkBoxObj = $(this);
	if(checkBoxObj.is(':checked')) {
		checkBoxObj.val(1);
	}
	else {
		checkBoxObj.val('not_selected');
	}
});
	  
$('#checkAllAuto1').click(function(){
	
$("INPUT[type='checkbox']").attr('checked', $('#checkAllAuto1').is(':checked'));  
if($('#checkAllAuto1').is(':checked')) {
	$("INPUT[type='checkbox']").val(1);
	}
	else {
		$("INPUT[type='checkbox']").val('not_selected');
	}  
});
});
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/attributes?id_product='.$product['id_products']); ?>" >Attributes</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit_attribute/'.$attribute['id_attributes'].'?id_product='.$product['id_products']); ?>" ><?php echo $attribute['title']; ?></a></li><span class="divider">/</span>
<li class="active">List of Options</li>
<? if ($this->acl->has_permission('product_options','add')){ ?>
<?php if(!empty($selected_product_attributes)) {?>
<li class="pull-right">
<a href="<?= site_url('back_office/products/add_option?id_product='.$product['id_products']); ?>" id="topLink" class="btn btn-info top_btn" title="">Add Attribute</a>
</li><? } ?><? } ?>
</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/products/update_options', $attributes); 
?>  
<input type="hidden" name="id_product" value="<?php echo $product['id_products']; ?>" />	
<input type="hidden" name="id_attribute" value="<?php echo $attribute['id_attributes']; ?>" />		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="5" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th width="100"><input type="checkbox" id="checkAllAuto1" style="margin:0" /> ACTIVATE</th>
<th>
TITLE
</th>
<!--<th>
SKU
</th>-->
<!--<th>
QUANTITY
</th>
<th>
PRICE
</th>-->
</tr>
</thead>
<tbody>
<? 
if(count($product_options) > 0){
$i=0;
foreach($product_options as $val){
	$cl = '';
if(!empty($val['selected_option']) && $val['selected_option']['status'] == 1) {
	$cl = 'checked="checked"';
}
$i++; 
?>
<tr id="<?=$val["id_attribute_options"]; ?>">
<td class="col-chk"><input type="checkbox" name="cehcklist[<?= $val["id_attribute_options"] ; ?>]" <?php echo $cl; ?> value="<?php if($cl != '') echo 1; else echo 'not_selected'; ?>" />
<input type="hidden" name="checklist_ids[]" value="<?= $val["id_attribute_options"] ; ?>" />
</td>
<td class="title_search">
<? echo $val["title"]; ?>
</td>
<!--<td class="title_search">
<input type="text" name="product_option_sku[]" value="<?php if(isset($val['selected_option']['sku'])) echo $val['selected_option']['sku']; ?>" />
</td>-->
<!--<td class="title_search">
<input type="text" name="product_option_qty[]" value="<?php if(isset($val['selected_option']['quantity'])) echo $val['selected_option']['quantity']; ?>" />
</td>-->
<!--<td class="title_search">
<input type="text" name="product_option_price[]" value="<?php if(isset($val['selected_option']['price'])) echo $val['selected_option']['price']; ?>" />
</td>-->
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="5" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  
<?php
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
?>	
<? echo form_close();  ?>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 