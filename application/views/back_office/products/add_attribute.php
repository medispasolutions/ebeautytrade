<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >

<ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/attributes?id_product='.$product['id_products']); ?>" >Attributes</a></li><span class="divider">/</span>
<?php if(isset($id)) {?>
<li class="active">Edit Attribute</li>
<li class="pull-right">
<a href="<?= site_url('back_office/products/options').'?id_product='.$product['id_products'].'&id_attribute='.$id; ?>" id="topLink" class="btn btn-info top_btn" title="">View Options</a>
</li>
<?php } else {?>
<li class="active">Add Attribute</li>
<?php }?>



</ul>
</div>
<div class="hundred pull-left">   
<?
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/products/submit_attribute?id_product='.$product['id_products'], $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["product"] = "";
$info["attribute"] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");

//PRODUCT&nbsp;<em>*</em>: SECTION.
if(isset($_GET['id_product']) && !empty($_GET['id_product'])) {?>
<input type="hidden" name="product" value="<?php echo $_GET['id_product']; ?>" />
<?php
}
else {
	echo form_label('<b>PRODUCT&nbsp;<em>*</em>:</b>', 'PRODUCT&nbsp;<em>*</em>:');
	$items = $this->fct->getAll("products","sort_order"); 
	echo '<select name="product"  class="span">';
	echo '<option value="" > - select products - </option>';
	foreach($items as $valll){ 
	?>
	<option value="<?= $valll["id_products"]; ?>" <? if(isset($id)){  if($info["id_products"] == $valll["id_products"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
	<?
	}
	echo "</select>";
	echo form_error("product","<span class='text-error'>","</span>");
	echo br();
}

//ATTRIBUTE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>ATTRIBUTE&nbsp;<em>*</em>:</b>', 'ATTRIBUTE&nbsp;<em>*</em>:');
echo '<select name="attribute"  class="span">';
echo '<option value="" > - select attributes - </option>';
foreach($product_attributes as $valll){ 
$cl = '';
if(isset($id) && $info['id_attributes'] == $valll['id_attributes'])
$cl = 'selected="selected"';
?>
<option value="<?= $valll["id_attributes"]; ?>" <?php echo $cl; ?>><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("attribute","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>