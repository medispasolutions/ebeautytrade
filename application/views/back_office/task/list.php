<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
function delete_row(id){
window.location="<?= site_url("back_office/task/delete"); ?>/"+id;
return false;
}

$(function(){
$("#table-1").tableDnD({
onDrop: function(table, row) {
var ser=$.tableDnD.serialize();
$("#result").load("<?= site_url("back_office/task/sorted"); ?>"+"?"+ser);
}
});

$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li class="active"><?= $title; ?></li>
<? if ($this->acl->has_permission('task','add')){ ?>
<li class="pull-right">
<a href="<?= site_url('back_office/task/add'); ?>" id="topLink" class="btn btn-info top_btn" title="">Add task</a>
</li><? } ?>
</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> <div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form method="get" action="<?=site_url("back_office/task")?>">
<!--Product ID-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">Keyword<br /><input type="text" id="keyword" name="keyword" value="<? if(isset($_GET["keyword"])) { echo $_GET["keyword"]; }?>" /></div>

<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Location<br />
<?php $options=$this->admin_fct->getTaskLocations();?>
<?php echo form_dropdown("location", $options,set_value("location",$this->input->get('location')), 'class="span"');?>
</div>

<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">Priority<br />
<?php
$options =getPriorities();
echo form_dropdown("priority", $options,set_value("priority",$this->input->get('priority')), 'class="span"');
 ?>
</div>

<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">Status<br />
<?php
$options =getTaskStatus();
echo form_dropdown("status", $options,set_value("status",$this->input->get('status')), 'class="span"');
 ?>
</div>

<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">Category<br />
<?php
$categories =$this->fct->getAll('categories_task','sort_order asc');
$options=array();
$options['']='-Select Category-';
foreach($categories as $category){
$options[$category['id_categories_task']]=$category['title'.$lang];	
	}
echo form_dropdown("category", $options,set_value("category",$this->input->get('category')), 'class="span"');
 ?>
</div>

<!--DATE -->
<div class="fl" style="width: 320px;float:left;">
<div class="fl" style="width: 150px; float:left;">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($from_date)) echo $from_date ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 150px; float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($to_date)) echo $to_date ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>







<input type="hidden" name="section" value="uploadProducts" />
<div class="fl" style="margin:21px 0 0 0; float:right">
<input type="submit" class="btn btn-primary" name="report_submit" value="Filter" />

</div>
</form>
              </div>
            </div>
          </div>
        </div><? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/task/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="6" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<td width="2%">&nbsp;</td>
<th>Task#</th>
<th>
<a href="<?= site_url('back_office/task/index/title'); ?>" class="<? if($sego == "title") echo 'green'; else echo 'order_link'; ?>" >
Description
</a>
</th>
<th>Created DAte</th>
<th>
<a href="<?= site_url('back_office/task/index/date'); ?>" class="<? if($sego == "date") echo 'order_active'; else echo 'order_link'; ?>" >
Deadline
</a>
</th>
<th>
<a href="<?= site_url('back_office/task/index/priority'); ?>" class="<? if($sego == "priority") echo 'order_active'; else echo 'order_link'; ?>" >
STATUS
</a>
</th>
<th>
<a href="<?= site_url('back_office/task/index/status'); ?>" class="<? if($sego == "status") echo 'order_active'; else echo 'order_link'; ?>" >
PRIORITY
</a>
</th><th style="text-align:center;" width="150">ACTION</th></tr>

</thead><tfoot><tr>
<td class="col-chk"><input type="checkbox" id="checkAllAuto" /></td>
<td colspan="5">
<? if ($this->acl->has_permission('task','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>
<tbody>
<? 
if(count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>
<tr id="<?=$val["id_task"]; ?>">
<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_task"] ; ?>" /></td>
<td><?php echo $val["id_task"];?></td>
<td class="title_search">
<? echo $val["description".$lang]; ?>
</td>
<td class="date_search">
<small><? echo $val["created_date"];?></small>
</td>
<td class="date_search">
<small><? echo $val["date"];?></small>
</td>
<td class="priority_search">
<?php if(!empty($val['status'])){?>
<? if($val["status"] == 3)
echo "<span class=' label label-success' >".getTaskStatus($val['status'])."</span>";
elseif($val["status"] == 2 ){
echo "<span class=' label label-pending ' >".getTaskStatus($val['status'])."</span>"; }else{
echo "<span class=' label label-important ' >".getTaskStatus($val['status'])."</span>";	}  ?>
<?php } ?>
</td>
<td class="status_search">
<?php if(!empty($val['priority'])){?>
<? if($val["priority"] == 3 || $val["priority"] == 5)
echo "<span class=' label label-success' >".getPriorities($val['priority'])."</span>";
else
echo "<span class=' label label-important ' >".getPriorities($val['priority'])."</span>";   ?>
<?php } ?>
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('task','edit')){ ?>
<a href="<?= site_url('back_office/task/edit/'.$val["id_task"]);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>
<? } ?>
<? if ($this->acl->has_permission('task','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_task"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/task"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 