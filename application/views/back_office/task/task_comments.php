<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script type="text/javascript">
function delete_comment(id_task_comments,id_task){
	$('#comment_'+id_task_comments).html("<div class='loader'></div>");
		var url=$('#baseurl').val()+"back_office/task/delete_comment";
		$.post(url,{id_task_comments :id_task_comments,id_task :id_task},function(data){
			$('#comment_'+id_task_comments).remove();
			if($('.media-body').length==0){
			$('.comments-bx').html('<div class="empty">No comments found</div>');
			}
			/*if($('.priceSegements tr').length>=4){
				$('#product_price_segments_btn').hide();}else{
				$('#product_price_segments_btn').show();}*/
		});
     
	}
	
	$(function(){
$("#table-1").tableDnD({
onDrop: function(table, row) {
	
var ser=$.tableDnD.serialize();

$("#result").load("<?= site_url("back_office/task_comments/sorted"); ?>"+"?"+ser);
}
});});
</script>
 <?php if(!empty($comments)) {
	
	 ?>   
     
<div id="result"></div>
<div class="comments-bx">

<?php foreach($comments as $comment){?>
<div class="media-body" id="comment_<?php echo $comment['id_task_comments'];?>">
                          <div class="well well-lg">
                              <?php if(!empty($comment['id_user'])){?>
                              <h4 class="media-heading text-uppercase reviews">
							  <?php echo $this->fct->getonecell('user','name',array('id_user'=>$comment['id_user']));?> </h4>
                              <?php } ?>
                              <span class="media-date">
                              <?php echo $comment['created_date'];?>
                              </span>
                              <div class="media-comment">
                                <?php echo $comment['description'];?>
                              </div>
                              <? if ($this->acl->has_permission('task_comments','edit') && $comment["id_user"]==$this->session->userdata("uid")){ ?>
                              <a class="btn btn-info btn-circle text-uppercase  fancyClick3" href="<?= route_to('back_office/task_comments/edit/'.$comment["id_task_comments"]);?>?id_task=<?php echo $comment["id_task"];?>" id="Edit" title="Edit"><span class="glyphicon glyphicon-share-alt"></span> Edit</a>
                              <a class="btn btn-warning btn-circle text-uppercase" data-toggle="collapse"onclick="if(confirm('Are you sure you want delete this segment ?')){ delete_comment('<?=$comment["id_task_comments"];?>','<?=$comment["id_task"];?>'); }" title="Delete"><span class="glyphicon glyphicon-comment"></span> Delete</a>
                         <?php } ?>
                          </div>              
                        </div>
<?php } ?>

</div>


<?php }else{ ?>
<div class="empty">No Comments found</div>
<?php }?>
