<script>
$(document).ready(function(){


			$('.fancyClick3').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 afterClose: function() {
			 
		var url=$('#siteurl').val()+'back_office/task/getComments';

var id_task=$("input[name='id']").val();
$('.priceSegements').html('<div class="loader"></div>');

		 $.post(url,{id_task : id_task},function(data){
			
			 var Parsedata = JSON.parse(data);
			$('.priceSegements').html(Parsedata.html);
			
		   });
		  
		}  });
			
	});
</script>

<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/task','<b>List task</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/task/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/task/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["date"] = "";
$info["description"] = "";
$info["enquiry"] = "";
$info["location"] = "";
$info["spamiles_note"] = "";
$info["priority"] = "";
$info["dow_group_notes"] = "";
$info["status"] = "";
$info["task_category"] = "";
$info["id_categories_task"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>

<?php
//DESCRIPTION SECTION.
echo form_label('<b>DESCRIPTION</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("description","<span class='text-error'>","</span>");
echo br();
//ENQUIRY SECTION.
?>
<div class="row-fluid">
<div class="span6">
<?php 
//SPAMILES NOTE SECTION.

echo form_label('<b>SPAMILES NOTES</b>', 'SPAMILES NOTES');
echo form_textarea(array("name" => "spamiles_note".$lang, "value" => set_value("spamiles_note".$lang,$info["spamiles_note".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("spamiles_note".$lang,"<span class='text-error'>","</span>");
echo br();
echo br();
?>
</div><div class="span6">
<?php
//DOW GROUP NOTES SECTION.

echo form_label('<b>DOW GROUP NOTES</b>', 'DOW GROUP NOTES');
echo form_textarea(array("name" => "dow_group_notes".$lang, "value" => set_value("dow_group_notes".$lang,$info["dow_group_notes".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("dow_group_notes".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div></div>

<?php

echo form_label('<b>ENQUIRY</b>', 'ENQUIRY');

echo form_textarea(array("name" => "enquiry".$lang, "value" => set_value("enquiry".$lang,$info["enquiry".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("enquiry","<span class='text-error'>","</span>");
echo br();

//LOCATION SECTION.
echo form_label('<b>LOCATION</b>', 'LOCATION');
echo form_label("homepage, Inside page, etc…","help_text",array("class"=>"yellow"));
echo form_input(array('name' => 'location', 'value' => set_value("location",$info["location"]),'class' =>'span' ));
echo form_error("location","<span class='text-error'>","</span>");
echo br();
?>

<div class="row-fluid">
<div class="span6">
<?php
echo form_label('<b>Categories</b> ', 'Categories');

$cond=array();


$categories = $this->fct->getAll_cond('categories_task','sort_order',$cond);
$options = array();
$options[""] = '--Select Category--';
foreach($categories as $val) {
	$options[$val['id_categories_task']] = $val['title'];
}

echo form_dropdown('id_categories_task',$options,set_value('id_categories_task',$info['id_categories_task'])); 
echo form_error("id_categories_task","<span class='text-error'>","</span>");
echo br();

?>
</div>
<div class="span6">
<?php
//DATE SECTION.
echo form_label('<b>DEADLINE</b>', 'DEADLINE');
echo form_label(" ","help_text",array("class"=>"yellow"));
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'date', 'value' => set_value("date",$this->fct->date_out_formate($info["date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("date","<span class='text-error'>","</span>");
echo br();
 ?>
</div>
</div>




<div class="row-fluid">
<div class="span6">
<?php 
//STATUS SECTION.
echo form_label('<b>STATUS</b>', 'STATUS');
$options =getTaskStatus();
echo form_dropdown("status", $options,set_value("status",$info["status"]), 'class="span"');
echo form_error("status","<span class='text-error'>","</span>");
echo br();
?>
</div><div class="span6">
<?php  //PRIORITY SECTION.
echo form_label('<b>PRIORITY</b>', 'PRIORITY');
$options =getPriorities();
echo form_dropdown("priority", $options,set_value("priority",$info["priority"]), 'class="span"');
echo form_error("priority","<span class='text-error'>","</span>");
echo br(); ?>
</div></div>

<?php

echo br();
echo form_fieldset_close();

if(isset($id)){ ?>
<?php $comments=$this->fct->getAll_cond('task_comments','sort_order',array('id_task'=>$id));


   $data2['comments']=$comments;
   ?>
   <div id="specifications_options">
   <?php $data2['lang']=$lang;?>
<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2 style="text-transform:uppercase;">Comments</h2>
              <div class="box-icon pull-right">
             
              <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
              <a id="product_price_segments_btn" class="pull-right fancyClick3" href="<?php echo site_url('back_office/task_comments/add?id_task='.$id);?>" style="margin-top:5px;margin-right:5px;text-transform:uppercase;<?php if(count($comments)>=200) echo "display:none;"; ?>">[Add Comment]</a>
              
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
              <div class="priceSegements">
			<?php echo $this->load->view('back_office/task/task_comments',$data2,true);?>
			 </div>

 </div>
            </div>
          </div>
        </div>
        </div>
        <?php }


echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}

echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>