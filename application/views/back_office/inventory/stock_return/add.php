
<style>

.price_p{
	display:none;}
    
 
table tr th:first-child{
			  width:2%;} 
  </style>
<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate2.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>

<script type="text/javascript" >

function check_qty_request(i){
var qty_request = $('#qty_'+i).val();
var qty_remain = $('#remain_quantity_'+i).val();

if(!isNumber(qty_request)){
alert('Invalid Quantity.');
$('#qty_'+i).val(0);
return false;
}
if(parseFloat(qty_request) > parseFloat(qty_remain)){
alert('The Quantity you asked for are more than the available in the stock.');
$('#qty_'+i).val(0);
return false;
}
/*var total = 0;
$('.qty_request').each(function(){
total = parseFloat(total) + parseFloat($(this).val());
});
if(total > qty_item_requested){
alert('The Quantity you asked for are more than the requested quantity.');
$('#qty_request'+i).val(0);
return false;
}*/
}
</script>
<script type="text/javascript" >
$(document).ready(function(){
	$('#supplier').change(function(){
		var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	$('#id_user').val(val);
	$('#title_bx').hide();
		$('.empty').show();
		$('.order-details-table .btn').hide();
	$('.order-details-table').find('.table').parent().parent().remove()
	if(val==""){$('#suppliers_brands').html('');$('.product_stock').html('');}else{
$('#suppliers_brands').html('<div class="loader"></div>');	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('#suppliers_brands').html(Parsedata.html);

$('#brands').change(function(){
	var obj=$(this);
	var val =obj.val();
	if(val==""){$('#title_bx').hide();}else{
		$('.product_stock').html('');
		$('#title_bx input').val('');
		$('#title_bx').show();}
    	});

});
		
}
    	});
	
	});</script>
<script type="text/javascript" >
function remove2(id){
var parent=$('#row-'+id).parent().parent();
$('#row-'+id).remove();
	
if(parent.find('tr').length<3){
	parent.parent().parent().remove();
	}
	if($('.order-details-table tr').length<4){
		$('.empty').show();
		$('.order-details-table .btn').hide();
		}
	}
</script>
<div class="container-fluid" id="delivery_request">
<div class="row-fluid">
  <div class="span2">
    <? $this->load->view("back_office/includes/left_box"); ?>
  </div>
  <div class="span10" >
    <div class="span10-fluid" >
      <?
$ul = array(
anchor('back_office/inventory/'.$this->session->userdata("back_link"),'<b>List inventory</b>').'<span class="divider">/</span>',anchor('back_office/inventory/stock_return','<b>List stock return</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			);

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
    </div>
    <div class="hundred pull-left"> <br />
      <?php echo form_fieldset_close();?>
      
      <div class="hundred pull-left">
        <h3>Order Details:</h3>
            <form id="insertOrder" method="post" enctype="multipart/form-data" action="<?php echo site_url('back_office/inventory/insertStockOrder');?>" >
                  <?php $user=$this->ecommerce_model->getUserInfo($this->session->userdata('user_id'));?>
                  <?php if(checkIfSupplier_admin()){ ?>
         <input type="hidden" value="5" name="id_role" />
         <?php } ?>
          <input type="hidden" value="1" name="return" />
          <input type="hidden" value="<?php echo $user['id_user'];?>" name="id_user" id="id_user" />
        <table class="table table-striped order-details-table">
          <tr>
              <th style="width:100px;" >ORDER ID</th>
            <th>Product Name</th>
            <th>Sku</th>
            <th style="text-align:center;">Quantity</th>
            <th style="text-align:center;">Return Quantity</th>
<th></th>

       
          </tr>
    
          <tr class="empty">
            <td colspan="10" style="text-align:center"><strong>empty</strong></td>
          </tr>
       <tfoot>
       <tr><td colspan="10">
 <div id="loader-bx"></div>
        <div class="FormResult"></div>
 <p class="pull-right" >
     
<input type="submit" name="submit" value="Send Order" class="btn btn-primary" style="display:none;">
</p></td></tr>
       </tfoot>
        </table>
        
        </form>
        <div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-info-sign"></i> Add Product(s)</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form id="addToStockOrder" method="post" enctype="multipart/form-data" action="<?php echo site_url('back_office/inventory/addToReturnStock');?>" >
              <div class="row-fluid">
              <?php if(!checkIfSupplier_admin()){ ?>
<!--<div class="span3">
<label>Supplier *</label>
<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name asc',$cond3);
?>

<select name="id_user" id="supplier">
<option value="">-Select Supplier-</option>
<?php 
foreach($suppliers as $val){?>
<option value="<?php echo $val['id_user'];?>"><?php echo $val['name'];?></option>
<?php } ?>
</select>

</div>-->
<?php }else{ ?>
<!--<div class="span3">
<label>Brand *</label>
<?php 

$cond=array();
if(checkIfSupplier_admin()){
$cond['id_user']=$user['id_user'];	
	}
$brands=$this->fct->getAll_cond('brands','title asc',$cond);
?>

<select name="brands" id="brands">
<option value="">-Select Brand-</option>
<?php 
foreach($brands as $val){?>
<option value="<?php echo $val['id_brands'];?>"><?php echo $val['title'];?></option>
<?php } ?>
</select>

</div>-->
<?php } ?>

<!--<div class="span3" id="suppliers_brands">
</div>
<div class="span3" id="title_bx" style="display:none;">
<label>Product Name</label>
<input type="text" name="title" value="" class="searchProducts" id="searchProducts" />

</div>-->

<div class="span3" >
<label>Order ID</label>
<input type="text" name="order_id" value="" class="order_id" id="order_id" />

</div>
<!--<div class="span3">
<label>Quantity</label>
<input type="text" name="quantity" value="" />
</div>-->
<div class="span4">


</div>
</div>

<div class="product_stock"></div>
<div id="loader_1"></div>

</form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div></div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>
