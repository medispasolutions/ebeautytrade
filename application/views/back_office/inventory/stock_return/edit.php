<style>
.table{
	width:100%;}
</style>

<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate2.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>

<script type="text/javascript" >
$(document).ready(function(){

		

	$('#supplier').change(function(){
		var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	$('#supplier').val(val);
	$('#title_bx').hide();
		$('.empty').show();

		$('.order-details-table .form-controls').hide();
	$('.order-details-table').find('.table').parent().parent().remove()
	if(val==""){/*$('#suppliers_brands').html('');*/
	$('.product_stock').html('');}
$('#suppliers_brands').html('<div class="loader"></div>');	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('#suppliers_brands').html(Parsedata.html);

});

});});
		
</script>
<script type="text/javascript" >
$(function(){




$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});</script>
<script type="text/javascript" >

function remove2(id_line_item,id_order,rand){
window.location="<?= site_url('back_office/inventory/remove_product_stock_return'); ?>/"+id_line_item+"/"+id_order+"/"+rand;
return false;
}

function getBrands(id_brand){

	var id_supplier="";
		if($('#supplier')){
		var id_supplier=$('#supplier').val();}
		
	var baseurl=$('#baseurl').val();

		$('.product_stock').html('');
		$('#title_bx input').val('');
		$('#title_bx').show();
		$('.product_stock').html('');
		
		$.post(baseurl+'back_office/inventory/getStockReachedThreshold',{id_brand:id_brand,id_supplier:id_supplier},function(data){
	$('#loader_1').html('');
	var Parsedata = JSON.parse(data);

loadSearch();
	});	}
	
function loadSearch(){

	$("#match2 input[name='search']").live('keyup', function(e){

e.preventDefault();
var id =this.id;
$('#match2 tbody tr.tr_m').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr.tr_m').css('display',"");	
} else {
$('#match2 tbody tr.tr_m td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});}	

function backToForm(){
	$('#add-more').show();
		$('#addNewProducts').slideUp();
		$('.section-details').slideDown();
		scroll_To('.section-details',20);}
	function addNewProducts(){
		$('#add-more').hide();
	
		$('#addNewProducts').slideDown();
		$('.section-details').slideUp();
		/*scroll_To('#addNewProducts_form',20)*/;
		
		}</script>
<script>
$(document).ready(function(){$('.fancyClick3').fancybox({
		'type': 'iframe', 'width' : '90%', 'height' : '90%' ,
		 afterClose: function() {}  });});
    
function check_qty_request(i){

var qty_request = $('#qty_'+i).val();
var qty_remain = $('#remain_quantity_'+i).val();

if(!isNumber(qty_request)){
alert('Invalid Quantity.');
$('#qty_'+i).val(0);
return false;
}
if(parseFloat(qty_request) > parseFloat(qty_remain)){
alert('The Quantity you asked for are more than the available in the stock.');
$('#qty_'+i).val(0);
return false;
}
/*var total = 0;
$('.qty_request').each(function(){
total = parseFloat(total) + parseFloat($(this).val());
});
if(total > qty_item_requested){
alert('The Quantity you asked for are more than the requested quantity.');
$('#qty_request'+i).val(0);
return false;
}*/
}
    </script>
    
    
<div class="container-fluid">
<?php $line_items=$order_details['line_items'];
$bool=false;?>
<?php if(!empty($line_items)){?>
<?php $bool=true;?>
<?php } ?>
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >

<div class="span10-fluid" >


<?
$ul = array(
anchor('back_office/inventory','<b>Inventory</b>').'<span class="divider">/</span>',
anchor('back_office/inventory/stock_return','<b>Stock Return</b>').'<span class="divider">/</span>',
 $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
            );
/*$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/print_stock_return/'.$order_details['id_stock_return'].'/'.$order_details['rand']).'" target="_blank" class="btn btn-info top_btn">Print Order</a>');*/
/*$ul['new_order'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/add_delivery_order').'" target="_blank" class="btn btn-info top_btn">Add New Order</a>');*/
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>

<br />
<table class=" table-striped" id="table-1" style="width:100%; ">
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
</tr>
<? } ?>
</tr>
</tr></table>
<div class="hundred pull-left">  


<h1 style="font-size:28px;width:100%;text-align:center;">Stock Return</h1>

<form id="SetPaidForm" action="<?php echo site_url('back_office/inventory/UpdateStockReturn'); ?>" method="post">


<input type="hidden" name="id_stock_return" value="<?php echo $order_details['id_stock_return']; ?>" />
<input type="hidden" name="rand" value="<?php echo $order_details['rand']; ?>" />




<div class="row-fluid" id="order-details-table2">
<?php $data['order_details']=$order_details;?>
<?php $this->load->view('back_office/blocks/stock_line_items',$data);?>
 </div>

<?php //if($order_details['status'] != 'paid') {?>

<? if (!checkIfSupplier_admin()){ ?>
    <?php if($order_details['status']!=1){ ?>
<div class="row-fluid order-operation" <?php if(!$bool) echo "style='display:none'";?>  >
<div class="span4 pull-right">
<label><strong>Status</strong></label>
<label>
<select name="status">
<option value="2" <?php if($order_details['status'] == '2') echo 'selected="selected"'; ?>>Pending</option>
<option value="1" <?php if($order_details['status'] == '1') echo 'selected="selected"'; ?>>Approved</option>
<option value="4" <?php if($order_details['status'] == '4') echo 'selected="selected"'; ?>>In progress</option>
<option value="3" <?php if($order_details['status'] == '3') echo 'selected="selected"'; ?>>Canceled</option>
</select>
</label></div>
 


</div>  <?php } ?>
<?php } ?>

 <?php if($order_details['status']!=1){ ?>
<!--<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Supplier</label>-->

<div class="pull-right">
<a id="add-more" class="btn btn-primary order-operation" onclick="addNewProducts()" data-original-title=""  <?php if(!$bool) echo "style='display:none'";?> > + Add More Products</a>
<input type="submit" value="Save Changes" class="btn btn-primary order-operation" id="SetPaid" <?php if(!$bool) echo "style='display:none'";?> />
</div>
<br />
<?php } ?>

        
 </form>      

 
 <div class="row-fluid sortable" id="addNewProducts" style="margin-top:20px;<?php if($bool){?>display:none<?php } ?>" >
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-info-sign"></i> Add Product(s)</h2>
              <?php if($bool){?>
              <div class="box-icon"><a  onclick="backToForm()" class="btn  btn-round"><i class="icon-remove"></i></a></div>
              <?php } ?>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form  method="post" enctype="multipart/form-data" id="addToReturnStock" action="<?php echo site_url('back_office/inventory/getDeliveryOrdersToReturn');?>" >
            <input type="hidden" name="section" value="stock_return" />
             <input type="hidden" name="id_stock_return" value="<?php echo $order_details['id_stock_return'];?>" />
             <input type="hidden" name="rand" value="<?php echo $order_details['rand'];?>" />
              <div class="row-fluid">
              <div class="span3" >
<label>ID <small class="blue" style="font-size:11px"><b>(Delivery Note / Purchase Invoice)</b></small></label>
<input type="text" name="id_delivery_order" value=""  id="id_delivery_order" />

</div>
              
<?php 
$id_user=0;
$cond_b=array();
if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo();
	$id_user=$user['id_user'];
	$cond_b['id_user']=$id_user; ?>
<input type="hidden" name="id_user" value="<?php echo $order_details['user']['id_user'];?>"  id="supplier" />
<?php  }else{?>
  <div class="span3 form-item">
<label>Trading Name *</label>
<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name asc',$cond3);
?>

<select name="id_user" id="supplier"  >
<option value="">-Select trading name-</option>
<?php 
foreach($suppliers as $val){?>
<option value="<?php echo $val['id_user'];?>" ><?php echo $val['trading_name'];?></option>
<?php } ?>
</select>
 <div id="id_user-error" class="form-error"></div>

</div>    
<?php } ?>        
              
<div class="span3" id="suppliers_brands">
<label>Brand </label>
<?php 
$brands = $this->fct->getAll_cond('brands','title asc',$cond_b);
 ?>


<select name='brands' id="brands">
<option value="">-Select All-</option>
<?php 
foreach($brands as $val){?>
<option value="<?php echo $val['id_brands'];?>"><?php echo $val['title'];?></option>
<?php } ?>
</select>

</div>

<div class="span3" >
<!--Approval-->
<label>Status</label><select name="order_type">
<option value="">-Select Status-</option>
<option value="2"><?php echo lang('paid_m'); ?></option>
<option value="1"><?php echo lang('consignment_m'); ?></option>

</select></div>


<div class="row-fluid" style="float:left;">
<!--DATE -->
<div class="span4" style="margin-left:0;" >
<div class="fl" style="width: 40%;  float:left;margin-right:5%">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['from_date'])) echo $_GET['from_date'] ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 40%;  float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['to_date'])) echo $_GET['to_date'] ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>

<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" value="Filter" /> 
<!--<input type="submit" class="btn btn-primary" name="report_submit" value="export" />-->
</div>

</div>




</div>
</form>
<div class="product_stock"></div>

<div id="loader_1"></div>
 

            
            </div>
          </div>
        </div>
 </div>
  </div>
 

 
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>