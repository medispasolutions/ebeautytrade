<div class="inventory">
 <?php if ($this->acl->has_permission('orders','index')){ ?>    
<div class="row-fluid sortable ">
          <div class="box span12">
            <div class="box-header well">
              <h2>Delivery</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <div class="head-btns">

<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/request/stock');?>">
<img src="<?php echo base_url();?>images/return_requisition.png" /><div>Stock Request</div><div></div></a>


<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/request/delivery_note');?>">
<img src="<?php echo base_url();?>images/1371608008_Business.png" /><div>Delivery Note</div><div></div></a>

<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/request/lpo');?>">
<img src="<?php echo base_url();?>images/return_requisition.png" /><div>LPO</div><div></div></a>

<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/request/invoices');?>">
<img style="width:50px;" src="<?php echo base_url();?>images/invoice-icon.png" /><div>Purchaces Invoices</div><div></div></a>


<?php /*?><?php if(checkIfSupplier_admin()){?>
<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/delivery_order');?>">
<img src="<?php echo base_url();?>images/delivery_note.png" /><div>Delivery Order</div><div></div></a>
<?php } ?><?php */?>

</div>
              </div>
            </div>
          </div>
        </div>
 <?php } ?>   
 <?php if ($this->acl->has_permission('orders','index')){ ?>       
<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2>Return</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <div class="head-btns">


<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/order_return');?>">
<img src="<?php echo base_url();?>images/adjustment.png" /><div>Order Return</div><div></div></a>

<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/stock_return');?>">
<img src="<?php echo base_url();?>images/return_product.png" /><div>Stock Return</div><div></div></a>
</div>
              </div>
            </div>
          </div>
        </div>
    <?php } ?>    
<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2>Reports</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
<div class="head-btns">
<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/index/page');?>">
<img src="<?php echo base_url();?>images/1365081049_download_box_seule.png" /><div>Products Inventory</div><div></div></a>
<?php if ($this->acl->has_permission('orders','index')){ ?>
<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/sales_history');?>">
<img src="<?php echo base_url();?>images/1366650752_invoice.png" /><div>Sales History</div><div></div></a>
<?php } ?>


<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/profits');?>">
<img src="<?php echo base_url();?>images/1371608008_Business.png" /><div>Sales Report</div><div></div></a>


 <?php if ($this->acl->has_permission('orders','index')){ ?>
<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/inventory/invoices');?>">
<img style="width:50px;"  src="<?php echo base_url();?>images/invoice-icon.png" /><div>Invoices</div><div></div></a>
<?php } ?>

<!--<a data-rel="tooltip" title="" class="well span3 top-block" href="<?php echo site_url('back_office/supplier');?>">
<img src="<?php echo base_url();?>images/inventory_value.png" /><div>Supplier </div><div></div></a>-->

</div>
              </div>
            </div>
          </div>
        </div>
        </div>
        

