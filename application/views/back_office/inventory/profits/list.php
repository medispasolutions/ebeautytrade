<script>
function delete_row(id){
window.location="<?= base_url(); ?>back_office/sales_history/delete/"+id;
return false;
}
$(document).ready(function(){
$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});

$(document).ready(function(){
	$('#supplier').change(function(){
		$('#suppliers_brands').show();
		var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	$('#id_user').val(val);
	$('.empty').show();

$('#suppliers_brands').html('<div class="loader"></div>');	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('#suppliers_brands').html(Parsedata.html);



}); });});
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<form method="get" id="filter" action="<?=base_url()?>back_office/inventory/profits">
<div class="span10-fluid" >
<ul class="breadcrumb">


<li><a href="<?php echo site_url('back_office/inventory');?>"><b>List Inventory</b></a></li>
<li class="divider">/</li>
<li class="active"><?= $title; ?></li>
</ul> 
</div>
<br />
<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left;margin-top:5px;">
<?php

if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}

?> 
<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            

<?php if(!checkIfSupplier_admin()){ ?>
<!--Suppliers -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label><?php echo lang('trading_name');?></label>  
<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name',$cond3);
?>

<select name="supplier" id="supplier">
<option value="">-Select <?php echo lang('trading_name');?>-</option>
<?php 


foreach($suppliers as $val){
$cl = '';
	if(isset($_GET['supplier']) && $_GET['supplier'] == $val['id_user'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_user'];?>"  <?php echo $cl;?>><?php echo $val['trading_name'];?></option>
<?php } ?>
</select></div>
<?php } ?>

<!--Suppliers -->


<!--Brands -->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;"  id="suppliers_brands">
<label>Brand</label> 
	<?php 
	$cond['user.status'] =  1;
	$cond2=array();
	if($this->input->get('supplier')!=""){
	$cond2['id_user']=$this->input->get('supplier');	}
if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo();
	$cond2['id_user']=$user['id_user'];}
	$brands=$this->fct->getAll_cond('brands','title',$cond2); ?>
	<select name="id_brands" id="brands">
	<option value="">- Select Brand -</option>
	<?php foreach($brands as $brand) {
		$cl = '';
	if(isset($_GET['supplier']) && $_GET['supplier'] == $brand['supplier'])
	$cl = 'selected="selected"';
	$cl = '';
	if((isset($_GET['id_brands']) && $_GET['id_brands'] == $brand['id_brands']) || isset($_GET['brands']) && $_GET['brands'] == $brand['id_brands'])
	$cl = 'selected="selected"';
	?>
   <option value="<?php echo $brand['id_brands']; ?>" <?php echo $cl; ?>><?php echo $brand['title']; ?></option>
<?php }?>
</select></div>

<!--Categories -->
<!--<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;"  id="suppliers_brands">
<label>Categories</label> 
	<?php 

	$cond3=array();
	$cond3['id_parent']=0;
	$categories=$this->fct->getAll_cond('categories','title',$cond3); ?>
	<select name="id_category" id="id_category">
	<option value="">- Select Category -</option>
	<?php foreach($categories as $val) {
		$cl = '';
	if(isset($_GET['id_category']) && $_GET['id_category'] == $val['id_categories'])
	$cl = 'selected="selected"';
	$cl = '';

	?>
   <option value="<?php echo $val['id_categories']; ?>" <?php echo $cl; ?>><?php echo $val['title']; ?></option>
<?php }?>
</select></div>-->
<?php if ($this->acl->has_permission('orders','index')){ ?>
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label><?php echo lang('account_manager');?></label>  
<?php 
;

$account_manager=$this->fct->getAll('account_manager','title');
?>

<select name="account_manager" id="account_manager">
<option value="">-Select <?php echo lang('account_manager');?>-</option>
<?php 


foreach($account_manager as $val){
$cl = '';
	if(isset($_GET['account_manager']) && $_GET['account_manager'] == $val['id_account_manager'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_account_manager'];?>"  <?php echo $cl;?>><?php echo $val['title'];?></option>
<?php } ?>
</select></div>

<?php }?>
<!--Country -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label><?php echo lang('country');?></label>  
<?php 
$cond3=array();
$countries=$this->fct->getAll_cond('countries','title',$cond3);
?>

<select name="country" id="country">
<option value="">-Select <?php echo lang('country');?>-</option>
<?php 


foreach($countries as $val){
$cl = '';
	if(isset($_GET['country']) && $_GET['country'] == $val['id_countries'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_countries'];?>"  <?php echo $cl;?>><?php echo $val['title'];?></option>
<?php } ?>
</select></div>

<!--Areas -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label><?php echo lang('areas');?></label>  
<?php 
$cond3=array();
$users_areas=$this->fct->getAll_cond('users_areas','title',$cond3);
?>

<select name="id_users_areas" id="users_areas">
<option value="">-Select <?php echo lang('area');?>-</option>
<?php 
foreach($users_areas as $val){
$cl = '';
	if(isset($_GET['id_users_areas']) && $_GET['id_users_areas'] == $val['id_users_areas'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_users_areas'];?>"  <?php echo $cl;?>><?php echo $val['title'];?></option>
<?php } ?>
</select></div>


<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;" >
<!--Approval-->
<label>Status</label><select name="order_type">
<option value="">-Select Status-</option>
<option value="2" <? if(isset($_GET['order_type']) &&$_GET['order_type']==2 ) { echo 'selected="selected"'; }?>>Paid stock</option>
<option value="1" <? if(isset($_GET['order_type']) &&$_GET['order_type']==1 ) { echo 'selected="selected"'; }?>>In Consignment</option>

</select></div>

<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label>Payment Method</label> 
<?php $payment_methods=$this->ecommerce_model->getPaymentMethods();?>
<select name="payment_method" id="payment_method" style="text-transform:capitalize;">
<option value="">- All -</option>
<?php 
foreach($payment_methods as $val){?>
<option value="<?php echo $val['payment_method'];?>" <?php if(isset($_GET['payment_method']) && $_GET['payment_method'] == $val['payment_method']) echo 'selected="selected"'; ?>> <?php echo $this->ecommerce_model->getPaymentMethod($val['payment_method']);?></option>
<?php } ?>
</select>
</div>

<!--City -->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;" id="city"><label>City</label><input type="text" id="city" name="city" value="<? if(isset($_GET['city'])) { echo $_GET['city']; }?>" /></div>

<!--Product Name -->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;" id="title_bx"><label>Product Name</label><input type="text" id="product_name" name="product_name" value="<? if(isset($_GET['product_name'])) { echo $_GET['product_name']; }?>" /></div>

<!--Product Name -->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;" id="title_bx"><label>Customer Name</label><input type="text" id="customer_name" name="customer_name" value="<? if(isset($_GET['customer_name'])) { echo $_GET['customer_name']; }?>" /></div>





<!--DATE -->
<div class="fl" style="width: 320px;float:left;">
<div class="fl" style="width: 150px; float:left;">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($from_date)) echo $from_date ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 150px; float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($to_date)) echo $to_date ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>



<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" name="report_submit" value="Filter" /> 
<!--<input type="submit" class="btn btn-primary" name="report_submit" value="export" />-->

<button type="submit" class="btn btn-primary  btn-danger" name="report_submit" value="export" ><i class="icon-download"></i> Export</button>
<?php if(!checkIfSupplier_admin()){ ?>
<button onclick="if(confirm('Are you sure you want to generate an invoice?')){$('#filter').submit(); }"  class="btn btn-info" name="report_submit" value="invoice" ><i class="icon-success"></i>Generate Invoice</button>
<?php } ?>
</div>


              </div>
            </div>
          </div>
        </div>
</div>

<h2 style="text-align:center;"><?php echo  date("F d,Y", strtotime($this->fct->date_in_formate($from_date))); ?> - <?php echo  date("F d,Y", strtotime($this->fct->date_in_formate($to_date))); ?></h2>
<div class="hundred pull-left" id="match2">  

<div id="result"></div> 


<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/sales_history/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>


</thead><!--<tfoot><tr>
<td colspan="3">
<? if ($this->acl->has_permission('sales_history','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>-->
<tbody>
<?php $total_credits_price=0;?>
<?

if(isset($info) && !empty($info)  && count($info) > 0){
$i=0;
$brands=$info['brands'];
$currency=$info['currency'];
foreach($brands as $brand){
	
$i++; 
$line_items=$brand['line_items'];
if(!empty($line_items)){
	
	?>
<tr><td colspan="13" style="text-align:center;"><b><?php echo $brand['title'];?></b></td></tr>
<tr>
<th>ORDER</th>
<th>Customer Name</th>
<th><?php echo lang('trading_name');?></th>
<th  style="width:140px;">Product</th>
<th>SKU</th>
<th><?php echo lang('barcode');?></th>
<th><?php echo lang('shelf_location');?></th>
<th><?php echo lang('created_date');?></th>
<th style="text-align:center;">Price <br />(<?php echo $currency;?>)</th>
<th style="text-align:center;"><?php echo lang('quantity');?></th>
<th style="text-align:center;"><?php echo lang('reqturn_quantity');?></th>
<!--<th style="text-align:center;"><?php echo lang('paid_m'); ?></th>
<th style="text-align:center;"><?php echo lang('consignment_m'); ?></th>-->
<th style="text-align:center;"><?php echo lang('credit_cart');?><br />(<?php echo $currency;?>)</th>
<th style="text-align:center;"><?php echo lang('total_price');?> <br />(<?php echo $currency;?>)</th>
</tr>
<?php } ?>
<?php  if(!empty($line_items)){?>
<?php foreach($line_items as $val) {  ?>
<tr id="<?=$val["id_orders"]; ?>">
<td>#<? echo $val["id_orders"]; ?></td>
<td ><? echo $val['order']["user"]['name']; ?> </td>
<td ><? echo $val["trading_name"]; ?> </td>
<td><? echo $val["product"]['title']; ?>
   <br />    <?php
   $options=array();
   if(isset($val["product"]['combination'])){
	  	$options=unSerializeStock($val["product"]['combination']);  }
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
</td>
<td><? echo $val["product"]['sku']; ?></td>
<td><? echo $val["product"]['barcode']; ?></td>
<?php
	if(!isset($val['product']['location_code'])){
		$val['product']['location_code']="";}
 ?>
<td style="text-align:center;"><? echo $val["product"]['location_code']; ?></td>
<td ><?php echo  date("F d,Y", strtotime($val['created_date'])); ?></td>
<td style="text-align:center;"><? echo $val["price"]; ?> </td>
<?php $total_quantity=$val["price"]*$val["quantity"];?>
<td style="text-align:center;"><? echo $val["quantity"]; ?></td>
<td style="text-align:center;"><? echo $val["return_quantity"]; ?></td>
<!--<td style="text-align:center;"><? echo $val["qty_sold_by_admin"]; ?></td>
<td style="text-align:center;"><? echo $val["qty_payable"]; ?></td>-->
<td style="text-align:center;"><? echo $val["credits_val"]; ?></td>
<td style="text-align:center;"><? echo $val["total_price"]; ?></td>
<td>


</td>
</tr>
<?php } ?>
<?php } ?>

<tr><td colspan="11" align="right" style="text-align:right"><b>Total Brand Price:</b></td><td colspan="2"><b><?php echo $brand['total_brand_price'];?> <?php  echo $info['currency'];?></b></td></tr>

<?php }}
if($info['total_amount']==0){?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<? }else{ ?> 
<tr>
<td colspan="10">
<br />
<?php $currency=$info['currency'];?>
<?php /*?><label><b>Total Amount :</b> <?php echo $info['total_amount']; ?> <?php echo $currency; ?></label>
<label><b>Total Amount Credits :</b> <?php echo $info['total_amount_credits']; ?> <?php echo $currency; ?></label>
<label><b>Shipping Charge:</b> <?php echo $info['total_customer_charge']; ?> <?php echo $currency; ?></label><?php */?>
<label><b>Total Price:</b> <?php echo $info['net_price']; ?> <?php echo $currency; ?></label>
</td>
</tr>
<?php } ?>
</tbody>
</table>  	
<? echo form_close();  ?>

</div>
</form>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 