<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
            anchor('back_office/orders/'.$this->session->userdata("back_link"),'<b>List orders</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
            );
$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/orders/invoice/'.$id).'" target="_blank" class="btn btn-info top_btn">View Invoice</a>');
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<div class="hundred pull-left">   
<label><b>Order ID:</b> <?php echo $order_details['id_orders']; ?></label>
<label><b>Customer Name:</b> <?php echo $order_details['user']['first_name'].' '.$order_details['user']['surname']; ?></label>
<label><b>Customer Email:</b> <a href="mailto:<?php echo $order_details['user']['email']; ?>"><?php echo $order_details['user']['email']; ?></a></label>
<label><b>Created Date:</b> <?php echo $order_details['created_date']; ?></label>
<label><b>Amount:</b> <?php echo $order_details['amount'].' '.lang($order_details['default_currency']); ?></label>
<label><b>Payment Method:</b> <?php echo $order_details['paymentmethod']['title']; ?></label>
<label><b>Status:</b> <? 
if($order_details["status"] == 'paid' || $order_details["status"] == 'completed') {?>
<span class="label label-success"><?php echo $order_details["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $order_details["status"]; ?></span>
<?php }?></label>
<?php if($order_details['status'] == 'paid') {?>
<label><b>Payment Date:</b> <?php echo $order_details['payment_date']; ?></label>
<?php }?>
<label><b>Comments:</b> <?php echo $order_details['comments']; ?></label>
<table class="table table-striped">
<tr>
    <th>Product Name</th>
    <th>Price</th>
    <th>Quantity</th>
    <th>Total Price</th>
</tr>
<?php 
$line_items = $order_details['line_items'];
//print '<pre>';print_r($line_items);exit;
if(!empty($line_items)) {?>
<?php 
$net_price = 0;
foreach($line_items as $pro) {
	?>
<tr>
<td>
<?php if(!empty($pro['product']['gallery'])) {?>
            <img align="top" width="50" src="<?php echo base_url(); ?>uploads/products/gallery/120x120/<?php echo $pro['product']['gallery'][0]['image']; ?>" title="<?php echo $pro['product']['title']; ?>" alt="<?php echo $pro['product']['title']; ?>" />
        <?php }?>
	<?php 
    $product_name = '';
	//if(empty($pro['product']['sku'])) 
	$product_name .= $pro['product']['title'];
	//else
	//$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
	if(!empty($pro['options'])) {
		$options = $pro['options_en'];
		$options = unSerializeStock($options);
		$product_name .= '<br />';
		$c = count($options);
		$i=0;
		foreach($options as $key => $opt) {
			$i++;
			$product_name .= $opt;
			if($i != $c) $product_name .= ' - ';
		}
	}
	echo $product_name;
	?>

    </td>
    <td><?php echo $pro['price'].' '.lang($pro['default_currency']); ?></td>
    <td><?php echo $pro['quantity']; ?></td>
    <td><?php echo $pro['total_price'].' '.lang($pro['default_currency']); ?></td>
<?php }?>
<?php }?>
</table>
<?php if(!empty($line_items)) {?>
<label><b>Net Price:</b> <?php echo $order_details['amount'].' '.lang($pro['default_currency']); ?></label>
<?php }?>
<?php //if($order_details['status'] != 'paid') {?>
<?php echo br(); ?>
<form id="SetPaidForm" action="<?php echo site_url('back_office/orders/updatestatus'); ?>" method="post">
<input type="hidden" name="id_orders" value="<?php echo $id; ?>" />
<label>
<select name="status">
<option value="pending" <?php if($order_details['status'] == 'pending') echo 'selected="selected"'; ?>>Pending</option>
<option value="paid" <?php if($order_details['status'] == 'paid') echo 'selected="selected"'; ?>>Paid</option>
<option value="completed" <?php if($order_details['status'] == 'completed') echo 'selected="selected"'; ?>>Completed</option>
<option value="canceled" <?php if($order_details['status'] == 'canceled') echo 'selected="selected"'; ?>>Canceled</option>
</select>
</label>
<input type="submit" value="Update Status" class="btn btn-primary" id="SetPaid" />
</form>
<?php //}?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>