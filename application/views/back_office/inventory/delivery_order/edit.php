<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
anchor('back_office/inventory','<b>Inventory</b>').'<span class="divider">/</span>',
anchor('back_office/inventory/delivery_order','<b>Delivery Order</b>').'<span class="divider">/</span>',
 $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
            );
$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/print_delivery_order/'.$order_details['id_delivery_order'].'/'.$order_details['rand']).'" target="_blank" class="btn btn-info top_btn">Print Order</a>');
/*$ul['new_order'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/add_delivery_order').'" target="_blank" class="btn btn-info top_btn">Add New Order</a>');*/
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<table class="table table-striped" id="table-1">

<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
</tr>
<? } ?>
</table>
<div class="hundred pull-left">   

<form id="SetPaidForm" action="<?php echo site_url('back_office/inventory/updateOrder'); ?>" method="post">


<input type="hidden" name="id_delivery_order" value="<?php echo $order_details['id_delivery_order']; ?>" />
<input type="hidden" name="rand" value="<?php echo $order_details['rand']; ?>" />
<br />
<label><b>Order ID:</b> <?php echo $order_details['id_delivery_order']; ?></label>
<label><b>Customer Name:</b> <?php echo $order_details['user']['name']; ?></label>
<label><b>Email:</b> <?php echo $order_details['user']['email']; ?></label>
<label><b>Created Date:</b> <?php echo  date("F d,Y", strtotime($order_details['created_date'])); ?></label>
 <?php if($order_details['status']==1){ ?>
<label><b>Status:</b> <span class="label label-success">Approved</span></label>
 <?php } ?>
 
 <?php if($order_details['type']==1){ ?>
<label><b>Type :</b> <?php echo lang('paid_m'); ?></label>
<?php } ?> 
<?php if($order_details['type']==2){ ?>
<label><b>Type :</b> <?php echo lang('consignment_m'); ?></label>
 <?php } ?> 
<?php echo form_error("qty",'<div class="alert alert-error">','</div>');?>
<br />


<?php $brands_line_items=$order_details['line_items'];?>

<?php if(!empty($brands_line_items)) {?>
<?php foreach($brands_line_items as $brand) {
 $line_items=$brand['line_items'];?> 
<table class="table table-striped order-details-table">
  <tr><td colspan="8" style="font-size:25px; background:#CCC; color:white; text-align:center;"><?php echo $brand['title'];?></td></tr>
          <tr>
          
            <th width="20%">Product Name</th>
            <th width="15%">Sku</th>
            <th width="15%" style="text-align:center;">Quantity</th>
             <?php if($order_details['status']==1){ ?>
              <th width="15%" style="text-align:center;">Return Quantity</th>
              <th width="15%" style="text-align:center;">Sold Quantity</th>
			 <?php } ?>
             
<!--            <th width="15%">Price(<?php echo $this->config->item('default_currency');?>)</th>
            <th width="15%">Cost(<?php echo $this->config->item('default_currency');?>)</th>-->
     
          </tr>
 
          
     <?php foreach($line_items as $pro){

		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];	
		$options=unSerializeStock($pro['stock']['combination']); 
			 }else{
		$sku=$pro['product']['sku'];
		$options="";}
		  ?>
		
	
<tr class="order-table-row">
    
    
        <td>
		 <input type="hidden" name="ids[]" value="<?php echo $pro['id_delivery_order_line_items'];?>" />
		<?php echo $pro['product']['title'];?>
        <br />    <?php
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
        </td>
        <td><?php echo $sku;?></td>
        <td style="text-align:center;">
        <?php if($order_details['status']==1){ echo $pro['quantity'];}else{ ?>
        <input type="text"  name="qty[]"  style="width:50px;" value="<?php echo $pro['quantity'];?>" >
        <?php }?></td>
        
         <?php if($order_details['status']==1){ ?>
		  <td style="text-align:center;"><?php echo $pro['return_quantity'];?></td>
          <td style="text-align:center;"><?php echo $pro['sold_quantity'];?></td>
		  <?php } ?>
        
<!--        <td>
        <?php if($order_details['status']==1){ echo $pro['price'];}else{ ?>
        <input type="text"  name="price[]"  style="width:50px;" value="<?php echo $pro['price'];?>" >
        <?php }?></td>-->
        
             <!--   <td>
        <?php if($order_details['status']==1){ echo $pro['cost'];}else{ ?>
        <input type="text"  name="cost[]"  style="width:50px;" value="<?php echo $pro['cost'];?>" >
        <?php }?></td>-->

        <!--<td>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2(<?php echo $rand;?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>
Remove
</a></td>-->
        </tr>
			 
	 <?php } ?> 

       
        </table>
     <?php } ?>
<?php }?>
<?php //if($order_details['status'] != 'paid') {?>
<?php echo br(); ?>
<? if (!checkIfSupplier_admin()){ ?>
    <?php if($order_details['status']!=1){ ?>
<div class="row-fluid">
<div class="span4">
<label><strong>Status</strong></label>
<label>
<select name="status">
<option value="2" <?php if($order_details['status'] == '2') echo 'selected="selected"'; ?>>Pending</option>
<option value="1" <?php if($order_details['status'] == '1') echo 'selected="selected"'; ?>>Approved</option>
<option value="4" <?php if($order_details['status'] == '4') echo 'selected="selected"'; ?>>In progress</option>
<option value="3" <?php if($order_details['status'] == '3') echo 'selected="selected"'; ?>>Canceled</option>
</select>
</label></div>
 
<div class="span4">
<label><strong>Type</strong></label>
<label>
<select name="type">
<option value="2" <?php if($order_details['type'] == '2') echo 'selected="selected"'; ?>><?php echo lang('consignment_m'); ?></option>
<option value="1" <?php if($order_details['type'] == '1') echo 'selected="selected"'; ?>><?php echo lang('paid_m'); ?></option>

</select>
</label></div>

</div>  <?php } ?>
<?php } ?>

 <?php if($order_details['status']!=1){ ?>
 <?php if ($this->acl->has_permission('orders','index')){ ?>
<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Supplier</label>
<?php } ?>
<br />
<div class="pull-right">
<input type="submit" value="Save Changes" class="btn btn-primary" id="SetPaid" />
</div>
<?php } ?>

        
 </form>       </div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>