<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate2.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>
<script type="text/javascript" >
$(document).ready(function(){
	var baseurl=$('#baseurl').val();
	$('#brands').change(function(){
	
	var obj=$(this);
	var val =obj.val();
	if(val==""){$('#title_bx').hide();}else{
		$('.product_stock').html('');
		$('#title_bx input').val('');
		$('#title_bx').show();
		$('#loader_1').html('<div class="loader"></div>');
		$.post(baseurl+'back_office/inventory/getStockReachedThreshold',{id_brand:val},function(data){
	$('#loader_1').html('');
	var Parsedata = JSON.parse(data);
$('.product_stock').html(Parsedata.html);
	});
		}
    	});});</script>
<script type="text/javascript" >
function remove2(id){
var parent=$('#row-'+id).parent().parent();

	$('#row-'+id).remove();
	
if(parent.find('tr').length<3){
	parent.parent().parent().remove();
	}
	if($('.order-details-table tr').length<4){
		$('.empty').show();
		$('.order-details-table .form-controls').hide();
		}
	}
</script>
<div class="container-fluid">
<div class="row-fluid">
  <div class="span2">
    <? $this->load->view("back_office/includes/left_box"); ?>
  </div>
  <div class="span10" >
    <div class="span10-fluid" >
      <?
$ul = array(
anchor('back_office/inventory','<b>List inventory</b>').'<span class="divider">/</span>',anchor('back_office/inventory/delivery_order','<b>List delivery order</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
            );

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
    </div>
    <div class="hundred pull-left"> <br />
      <?php echo form_fieldset_close();?>
      
      <div class="hundred pull-left">
        <h3>Order Details:</h3>
            <form id="insertOrder" method="post" enctype="multipart/form-data" action="<?php echo site_url('back_office/inventory/insertOrder');?>" >
        <table class="table table-striped order-details-table">
<!--          <tr>
            <th>Brand Name</th>
            <th>Product Name</th>
            <th>Sku</th>
            <th>Quantity</th>
             <th >Price(<?php echo $this->config->item('default_currency');?>)</th>
            <th >Cost(<?php echo $this->config->item('default_currency');?>)</th>
            <th>Option(s)</th>
            <th></th>
          </tr>-->
          
          <?php $user = $this->ecommerce_model->getUserInfo();?>
         <input type="hidden" value="5" name="id_role" />
         <input type="hidden" value="<?php echo $user['id_user'];?>" name="id_user" />
          <tr class="empty">
            <td colspan="10" style="text-align:center"><strong>empty</strong></td>
          </tr>
       <tfoot>
       <tr><td colspan="10">
 <div id="loader-bx"></div>
        <div class="FormResult"></div>
        <div  class="form-controls" style="display:none;">
 <p class="pull-right" >
     <? if (!checkIfSupplier_admin()){ ?>

<div class="row-fluid">
<!--<div class="span4">
<label><strong>Status</strong></label>
<label>
<select name="status">
<option value="2">Pending</option>
<option value="1">Approved</option>
<option value="4">In progress</option>

</select>
</label></div>-->


</div> 
<?php }else{ ?>
 <input type="hidden" value="2" name="status" />
<?php } ?>
<input type="submit" name="submit" value="Save Order" class="btn btn-primary" >
</p>
</div></td></tr>
       </tfoot>
        </table>
        
        </form>
        <div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-info-sign"></i> Add Product(s)</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form id="addToOrder" method="post" enctype="multipart/form-data" action="<?php echo site_url('back_office/inventory/addToOrder');?>" >
              <div class="row-fluid">

<div class="span3">
<label>Brand *</label>
<?php 

$cond=array();
if(checkIfSupplier_admin()){
$cond['id_user']=$user['id_user'];	
	}
$brands=$this->fct->getAll_cond('brands','title asc',$cond);
?>

<select name="brands" id="brands">
<option value="">-Select Brand-</option>
<?php 
foreach($brands as $val){?>
<option value="<?php echo $val['id_brands'];?>"><?php echo $val['title'];?></option>
<?php } ?>
</select>

</div>

<div class="span3" id="title_bx" style="display:none;">
<label>Product Name</label>
<input type="text" name="title" value="" class="searchProducts" id="searchProducts" />

</div>
<!--<div class="span3">
<label>Quantity</label>
<input type="text" name="quantity" value="" />
</div>-->
<div class="span4">


</div>
</div>

<div class="product_stock"></div>

<div id="loader_1"></div>

</form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div></div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>
