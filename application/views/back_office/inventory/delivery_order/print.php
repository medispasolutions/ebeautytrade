<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
	<title><?php echo lang("invoice"); ?></title>
	<meta charset="utf-8">
    <!-- Favicon --> 
	<link rel="shortcut icon" href="<?php echo base_url(); ?>front/img/favicon.ico">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>front/css/print.css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery-1.10.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>front/js/jquery.PrintArea.js"></script>
	<script type="text/javascript">
    	function calculateIDPos(){
			var WindowWidth = $(window).width();
			var RightPos = ((WindowWidth - 600) / 2) - 80;
			$('#printButton').css('right',RightPos); 
		}
		$(document).ready(function(){
			calculateIDPos();
			$('#printButton').click(function(){
				$('#printThis').printArea();
			});
		});
		$(window).bind("load",function(){
			calculateIDPos();
		});
    </script>
   
</head>
<body>
<input type="submit" style="position:absolute;   background: #25b0e5;
    border: 1px solid #25b0e5;
    color: #ffffff;
    cursor: pointer;
    display: inline-block;
    font-size: 15px;
    padding: 5px 8px;
    text-transform: uppercase;" class="printButton" id="printButton"  value="<?php echo lang("print"); ?>" />
<div class="printing-container" id="printThis" style="width:670px; margin:0 auto">
<?php echo $template; ?>
</div>
</body>
</html>



