<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
function delete_row(id,rand){
window.location="<?= site_url("back_office/inventory/delete_delivery_order"); ?>/"+id+"/"+rand;
return false;
}

$(function(){
	$('#category').change(function(){
	
		var id = $(this).val();
		if(id == '') {
			return false;
		}
		else {
			var url = "<?php echo site_url("back_office/categories_sub/load_sub_filter2"); ?>";
			$("#sub_category").attr("disabled","disabled");
			$("#sub_category").html("");
			$("#category").attr("disabled","disabled");
			$.post(url,{id : id},function(data){
				$("#sub_category").removeAttr("disabled");
				$("#category").removeAttr("disabled");
				$("#sub_category").html(data);
			});
		}
	});
	


$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="1";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">

<li><a href="<?php echo site_url('back_office/inventory');?>"><b>List inventory</b></a></li>
<li class="divider">/</li>
<li class="active"><?php echo $title;?></li>


<li class="pull-right">
<a href="<?= site_url('back_office/inventory/add_delivery_order'); ?>" id="topLink" class="btn btn-info top_btn" title="">Add New Order</a>
</li>

</ul> 
</div>
<?php /*?><? $this->load->view("back_office/inventory/head"); ?><?php */?>
<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">

<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form method="get" action="<?=site_url("back_office/inventory/delivery_order")?>">

<div class="row-fluid">
<!--Supplier-->
<div class="span3" >Order ID<br /><input type="text" id="delivery_order" name="delivery_order" value="<? if(isset($_GET['delivery_order'])) { echo $_GET['delivery_order']; }?>" /></div>
<?php if ($this->acl->has_permission('banner','index')){ ?>
<!--Supplier-->
<div class="span3" >Supplier Name<br /><input type="text" id="product_name" name="name" value="<? if(isset($_GET['name'])) { echo $_GET['name']; }?>" /></div>
<?php } ?>


<!--Approval-->
<div class="span3" >Status<br /><select name="status">
<option value="">-Select Status-</option>
<option value="2" <? if(isset($_GET['status']) &&$_GET['status']==2 ) { echo 'selected="selected"'; }?>>Pending</option>
<option value="1" <? if(isset($_GET['status']) &&$_GET['status']==1 ) { echo 'selected="selected"'; }?>>Approved</option>
<option value="3" <? if(isset($_GET['status']) &&$_GET['status']==3 ) { echo 'selected="selected"'; }?>>Canceled</option>
<option value="4" <? if(isset($_GET['status']) &&$_GET['status']==4 ) { echo 'selected="selected"'; }?>>In progress</option>
</select></div>

<!--Order Status-->
<div class="span3" >Type<br /><select name="type">
<option value="">-Select Type-</option>
<option value="2" <?php if(isset($_GET['type']) &&$_GET['type']==2 ) echo 'selected="selected"'; ?>><?php echo lang('consignment_m'); ?></option>
<option value="1" <?php if(isset($_GET['type']) &&$_GET['type']==1 ) echo 'selected="selected"'; ?>><?php echo lang('paid_m'); ?></option>

</select></div>

</div>
<div class="row-fluid">
<!--DATE -->
<div class="span4" style="margin-left:0;" >
<div class="fl" style="width: 40%;  float:left;margin-right:5%">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['from_date'])) echo $_GET['from_date'] ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 40%;  float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['to_date'])) echo $_GET['to_date'] ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>

<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" value="Filter" /> 
<!--<input type="submit" class="btn btn-primary" name="report_submit" value="export" />-->
</div>

</div>
</form>
              </div>
            </div>
          </div>
        </div>



</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/products/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>


<th width="200">
Request No
</th>
<th>Date</th>
<th>Customer Name</th>
<th>Type</th>
<th>Status</th>


</tr>

</thead><tfoot>
</tfoot>
<tbody>
<? 
if(isset($info) && !empty($info)){
$i=0;
foreach($info as $val){
	

$i++; 
?>
<tr id="<?=$val["id_delivery_order"]; ?>">

<td class="title_search">
<b>D-ORDER#<? echo $val["id_delivery_order".$lang]; ?></b>
</td>
<td >
<?php echo  date("F d,Y", strtotime($val['created_date'])); ?>
</td>
<td >
<? echo $val["name"]; ?>
</td>



<td >
<? 
if($val["type"] == '1') {?>
<span class="label label-success"><?php echo lang('paid_m'); ?></span>
<?php } else {?>
<span class="label label-important"><?php echo lang('consignment_m'); ?></span>
<?php }?>
</td>

<td >
<? 
if($val["status"] == '2') {?>
<span class="label label-warning"><?php echo lang('pending'); ?></span>
<?php } ?>
<?php  if($val["status"] == '1') {?>
<span class="label label-success"><?php echo lang('approved'); ?></span>
<?php }?>
<?php  if($val["status"] == '3') {?>
<span class="label label-important"><?php echo lang('canceled'); ?></span>
<?php }?>
<?php  if($val["status"] == '4') {?>
<span class="in_progress"><?php echo lang('in_progress'); ?>...</span>
<?php }?>
</td>

<td>
<? if ($this->acl->has_permission('delivery_order','index')){ ?>
<a href="<?= site_url('back_office/inventory/view_delivery_order/'.$val["id_delivery_order"].'/'.$val['rand']);?>" class="table-edit-link" title="View" >
<i class="icon-search" ></i> View Details</a>
<? } ?>

<? if ($this->acl->has_permission('delivery_order','delete')){ ?>
<span class="hidden"> | </span>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_delivery_order"];?>','<?=$val["rand"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>

</tr>

<?php  }} else { ?>
<tr class='odd'><td colspan="10" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?=$url?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 