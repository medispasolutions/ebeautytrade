<script>
function delete_row(id){
window.location="<?= base_url(); ?>back_office/orders/delete/"+id;
return false;
}
$(document).ready(function(){
$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li><a href="<?php echo site_url('back_office/inventory');?>"><b>List Inventory</b></a></li>
<li class="divider">/</li>
<li class="active">Manage Returns</li>

</ul> 
</div>
<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">
<div class="row-fluid">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form method="get" action="<?=site_url("back_office/inventory/order_return")?>">

<div class="row-fluid">
<!--Supplier-->
<div class="span3" >Order ID<br /><input type="text" id="order" name="order_id" value="<? if(isset($_GET['order_id'])) { echo $_GET['order_id']; }?>" /></div>

<!--Supplier-->
<div class="span3" >Customer Name<br /><input type="text" id="customer_name" name="name" value="<? if(isset($_GET['name'])) { echo $_GET['name']; }?>" /></div>

<div class="span2" > <br /><input type="submit" class="btn btn-primary" value="Filter" /> </div>

</div>

</form>
              </div>
            </div>
          </div>
        </div>

</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/orders/delete_all', $attributes); 
?> 
<?php if((isset($_GET['name']) && !empty($_GET['name'])) || (isset($_GET['order_id']) && !empty($_GET['order_id']))){?> 		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="4" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th>ORDER ID</th>
<th>CREATED DATE</th>
<th>CUSTOMER NAME</th>
<th>AMOUNT</th>
<th>STATUS</th>

<th style="text-align:center;" width="250">ACTION</th></tr>
</thead><!--<tfoot><tr>
<td colspan="3">
<? if ($this->acl->has_permission('orders','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>-->
<tbody>
<? 
if(count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>
<tr id="<?=$val["id_orders"]; ?>">
<!--<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_orders"] ; ?>" /></td>-->
<td><? echo $val["id_orders"]; ?></td>
<td><? echo $val["created_date"]; ?></td>
<td><? echo $val["name"]; ?></td>
<td><? echo $val["amount_currency"] .' '.lang($val["currency"]); ?></td>
<td><? 
if($val["status"] == 'paid' || $val["status"] == 'completed') {?>
<span class="label label-success"><?php echo $val["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $val["status"]; ?></span>
<?php }?></td>


<td style="text-align:center"><? if ($this->acl->has_permission('orders','index')){ ?>
<a href="<?= site_url('back_office/inventory/manage_order/'.$val["id_orders"].'/'.$val['rand']);?>" class="table-edit-link" title="View" >
<i class="icon-edit"></i> Manage Order</a> 
<? } ?>

</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<?php } ?>
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<!--<form action="<?= site_url("back_office/orders"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>-->
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 