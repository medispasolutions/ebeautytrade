
<script>
function check_qty_request(i){

var qty_request = $('#qty_'+i).val();
var qty_remain = $('#remain_quantity_'+i).val();

if(!isNumber(qty_request)){
alert('Invalid Quantity.');
$('#qty_'+i).val(0);
return false;
}
if(parseFloat(qty_request) > parseFloat(qty_remain)){
alert('The Quantity you asked for are more than the available in the selected item.');
$('#qty_'+i).val(0);
return false;
}
/*var total = 0;
$('.qty_request').each(function(){
total = parseFloat(total) + parseFloat($(this).val());
});
if(total > qty_item_requested){
alert('The Quantity you asked for are more than the requested quantity.');
$('#qty_request'+i).val(0);
return false;
}*/
}
$(document).ready(function() {
$(".return_qty").click(function() {

$(this).keyup(function(){

	var obj=$(this);
	var return_qty=$(this).val();

		var qty=obj.parent().find('.old_qty').val();

		if(return_qty>qty){
			$(this).val(qty);
			alert('The quantity is greater than '+qty);
			}
			if(return_qty<0){
			$(this).val(0);
			alert('The quantity is less than zero.');
			}	
	});});});
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
anchor('back_office/inventory','<b>List inventory</b>').'<span class="divider">/</span>',anchor('back_office/inventory/order_return','<b>List order return</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			);
$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/orders/invoice/'.$id).'?order_return=1" target="_blank" class="btn btn-info top_btn">print / preview</a>');
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<div class="hundred pull-left">   
<table class="table table-striped" id="table-1">

<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
</tr>
<? } ?>
</table>
<br />
<label><b>Order ID:</b> <?php echo $order_details['id_orders']; ?></label>
<label><b>Customer Name:</b> <?php echo $order_details['user']['name']; ?></label>
<label><b>Created Date:</b> <?php echo $order_details['created_date']; ?></label>
<label><b>Status:</b> <? 
if($order_details["status"] == 'paid' || $order_details["status"] == 'completed') {?>
<span class="label label-success"><?php echo $order_details["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $order_details["status"]; ?></span>
<?php }?></label>
<label><b>Sub Total:</b> <?php echo $order_details['sub_total'].' '.$order_details['currency']; ?></label>
<?php if($order_details['discount']>0){?>
<label><b>Discount:</b> <?php echo $order_details['discount'].' '.$order_details['currency']; ?></label>
<?php } ?>
<?php if($order_details['redeem_discount_miles_amount']>0){ ?>
<label><b>Redeem Miles:</b> <?php echo $order_details['redeem_miles'].' Miles'; ?></label>
<?php } ?>

<?php if($order_details['shipping_charge_currency']>0){ ?>
<label><b>Shipping & Handling :</b><?php if($order_details['shipping_charge_currency']){
					echo $order_details['shipping_charge_currency'].' '.$order_details['currency'];
					}else{ echo "Free" ;}?></label>
<?php } ?>

<?php if($order_details['payment_method']!=""){ ?>
<label><b>Payment Method:</b>  <?php echo $this->ecommerce_model->getPaymentMethod($order_details['payment_method']);?></label>
<?php } ?>

<?php
$return_details=$this->admin_fct->return_details($order_details['id_orders'],$order_details['rand']);?>
 <?php if(!empty($return_details['amount_return'])){?>
<label style="color:#F00;"><b>Amount Return:</b> <?php echo $return_details['amount_return'].' '.$order_details['currency']; ?></label>
<?php } ?>

<label><b>Amount:</b> <?php echo $order_details['amount_currency'].' '.$order_details['currency']; ?></label>


<?php if($order_details['comments'] != '' && $order_details['comments'] != 0) {?>
<label><b>Comments:</b> <?php echo $order_details['comments']; ?></label>
<?php } ?>

<form id="SetPaidForm" action="<?php echo site_url('back_office/inventory/updateReturnOrder'); ?>" method="post" enctype="multipart/form-data" accept-charset="utf-8">
<?php echo form_error("quantity","<span class='text-error'>","</span>"); ?>
<?php echo form_error("quantity2","<span class='text-error'>","</span>"); ?>

<?php 
$line_items_brands = $order_details['line_items_brands'];
foreach($line_items_brands as $line_items_brand){?>
<?php if((!empty($line_items_brand['id_brands']) || (empty($line_items_brand['id_brands']) && count($line_items_brands)>1))){?>
<label style="font-size:20px;color:#a0d9f4;"><?php echo $line_items_brand['deliver_by'];?></label>
<?php } ?>
<?php 

$line_items = $line_items_brand['line_items'];
if(!empty($line_items)){?>
<table class="table table-striped">
<tr><th width="2%"></th>
    <th>Product Name</th>
    <th>SKU</th>

    <th>Price</th>
    <th style="text-align:center;">Quantity</th>
    <th style="text-align:center;"> Returned Quantity</th>
   
    <th>Total Price</th>
     <th style="text-align:center;"> Return New Quantity</th>
     <th style="text-align:center;"> Return To Stock</th>
</tr>
<?php 

//print '<pre>';print_r($line_items);exit;


$net_price = 0;
$selected_items=array();
foreach($line_items as $pro) {

$selected=false;
$cl="";
if(in_array($pro['id_line_items'],$selected_items)){ 
$cl = 'checked="checked"';
$selected=true;
}	?>
<tr>

<td class="col-chk">
<input class="checklist-chkbx" type="checkbox" name="checklist[]" <?php echo $cl; ?>  value="<?= $pro["id_line_items"] ; ?>" />
                 </td>
            
       
<td>
<?php if(!empty($pro['product']['gallery'])) {
	?>
            <img align="top" width="50" src="<?php echo base_url(); ?>uploads/products/gallery/120x120/<?php echo $pro['product']['gallery'][0]['image']; ?>" title="<?php echo $pro['product']['title']; ?>" alt="<?php echo $pro['product']['title']; ?>" />
        <?php }?>
	<?php 
    $product_name = '';
	//if(empty($pro['product']['sku'])) 
	$product_name .= $pro['product']['title'];
	//else
	//$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
	if(!empty($pro['options_en'])) {
		$options = $pro['options_en'];
		$options = unSerializeStock($options);
		$product_name .= '<br />';
		$c = count($options);
		$i=0;
		foreach($options as $key => $opt) {
			$i++;
			$product_name .= $opt;
			if($i != $c) $product_name .= ' - ';
		}
	}
	echo $product_name;
	?>

    </td>
    
    <td><?php echo $pro['product']['sku']; ?></td>
              
     <td><?php echo $pro['price_currency'].' '.$pro['currency']; ?></td>
      <?php $old_quantity=$pro['quantity']+$pro['return_quantity'];?>
    <td style="text-align:center;"><?php echo $old_quantity; ?></td>
    <td style="text-align:center;">
    <?php echo $pro['return_quantity']; ?>
   
    </td>
     
    
    
    
  
  
<td><?php echo $pro['total_price_currency'].' '.$pro['currency']; ?></td>
    
    <td style="text-align:center;">
     <?php $remaining_return_qty=$pro['quantity'];?>
    <input type="hidden" class="old_qty" value="<?php echo $old_quantity;?>" />
    <input type="hidden" id="remain_quantity_<?php echo $pro['id_line_items'];?>" value="<?php echo $remaining_return_qty;?>" />
    <input class="span qty checklist_qty return_qty" maxlength="12" title="Qty" size="4"   id="qty_<?php echo $pro['id_line_items'];?>" name="qty[<?php echo $pro['id_line_items'];?>]" onkeyup="check_qty_request(<?php echo $pro['id_line_items'];?>)" style="width:50px;text-align:center;"  value="0"   <?php if(!$selected){ echo "disabled=disabled";} ?>></td>
    
<td class="col-chk" style="text-align:center;">
<input  type="checkbox" name="return_to_stock[]" <?php echo $cl; ?>  value="1" />
  </td>
<?php }?>



</table>

<?php } ?>

<?php 

$line_items = $line_items_brand['line_items_redeem_by_miles'];
if(!empty($line_items)){?>
<table class="table table-striped">
<tr><th width="2%"></th>
    <th>Product Name</th>
    <th>SKU</th>
    <th>Miles</th>

    <th style="text-align:center;">Quantity</th>
    <th style="text-align:center;"> Returned Quantity</th>
   
    <th>Total Miles</th>
     <th style="text-align:center;"> Return New Quantity</th>
     <th style="text-align:center;"> Return To Stock</th>
</tr>
<?php 

//print '<pre>';print_r($line_items);exit;
if(!empty($line_items)) {?>
<?php 
$net_price = 0;
$selected_items=array();
foreach($line_items as $pro) {

$selected=false;
$cl="";
if(in_array($pro['id_line_items'],$selected_items)){ 
$cl = 'checked="checked"';
$selected=true;
}	?>
<tr>

<td class="col-chk">
<input class="checklist-chkbx" type="checkbox" name="checklist[]" <?php echo $cl; ?>  value="<?= $pro["id_line_items"] ; ?>" />
                 </td>
            
       
<td>
<?php if(!empty($pro['product']['gallery'])) {
	?>
            <img align="top" width="50" src="<?php echo base_url(); ?>uploads/products/gallery/120x120/<?php echo $pro['product']['gallery'][0]['image']; ?>" title="<?php echo $pro['product']['title']; ?>" alt="<?php echo $pro['product']['title']; ?>" />
        <?php }?>
	<?php 
    $product_name = '';
	//if(empty($pro['product']['sku'])) 
	$product_name .= $pro['product']['title'];
	//else
	//$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
	if(!empty($pro['options_en'])) {
		$options = $pro['options_en'];
		$options = unSerializeStock($options);
		$product_name .= '<br />';
		$c = count($options);
		$i=0;
		foreach($options as $key => $opt) {
			$i++;
			$product_name .= $opt;
			if($i != $c) $product_name .= ' - ';
		}
	}
	echo $product_name;
	?>

    </td>
    
    <td><?php echo $pro['product']['sku']; ?></td>
                
     <td>
	 
	 <?php echo $pro['redeem_miles']; ?>
     </td>
      <?php $old_quantity=$pro['quantity']+$pro['return_quantity'];?>
    <td style="text-align:center;"><?php echo $old_quantity; ?></td>
    <td style="text-align:center;">
    <?php echo $pro['return_quantity']; ?>
   
    </td>
     

<td><?php echo $pro['quantity']*$pro['redeem_miles']; ?></td>
    
    <td style="text-align:center;">
     <?php $remaining_return_qty=$pro['quantity'];?>
    <input type="hidden" class="old_qty" value="<?php echo $old_quantity;?>" />
    <input type="hidden" id="remain_quantity_<?php echo $pro['id_line_items'];?>" value="<?php echo $remaining_return_qty;?>" />
    <input class="span qty checklist_qty return_qty" maxlength="12" title="Qty" size="4"   id="qty_<?php echo $pro['id_line_items'];?>" name="qty[<?php echo $pro['id_line_items'];?>]" onkeyup="check_qty_request(<?php echo $pro['id_line_items'];?>)" style="width:50px;text-align:center;"  value="0"   <?php if(!$selected){ echo "disabled=disabled";} ?>></td>
    
<td class="col-chk" style="text-align:center;">
<input  type="checkbox" name="return_to_stock[]" <?php echo $cl; ?>  value="1" />
  </td>
<?php }?>
<?php }else{?>

<?php } ?>
</table>

<?php } ?><?php } ?>
<?php //if($order_details['status'] != 'paid') {?>
<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Client</label>
<?php echo br(); ?>

<input type="hidden" name="id_order" value="<?php echo $id; ?>" />
<input type="hidden" name="rand" value="<?php echo $order_details['rand']; ?>" />

<input type="submit" value="Update Order" class="btn btn-primary" id="SetPaid" />
</form>
        
        </div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>