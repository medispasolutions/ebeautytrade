<table class="table table-striped" id="table-1">
<thead>

<tr>
<th>ORDER ID</th>
<th>CREATED DATE</th>
<th>CUSTOMER NAME</th>
<th>AMOUNT</th>
<th>STATUS</th>
<th></th>
<th style="text-align:center;" width="250">ACTION</th></tr>
</thead>

<tbody>
<? 
if(count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>
<tr id="<?=$val["id_orders"]; ?>">
<!--<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_orders"] ; ?>" /></td>-->
<td><? echo $val["id_orders"]; ?></td>
<td><? echo $val["created_date"]; ?></td>
<td><? echo $val["name"]; ?></td>
<td><? echo $val["amount_currency"] .' '.lang($val["currency"]); ?></td>
<td><? 
if($val["status"] == 'paid' || $val["status"] == 'completed') {?>
<span class="label label-success"><?php echo $val["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $val["status"]; ?></span>
<?php }?></td>

<td class="" width="" ><?php if($val["readed"] == 1) echo  '<span class="label label-success">Read</span>'; else echo '<span class="label label-important">Unread</span>'; ?></td>

<td style="text-align:center"><? if ($this->acl->has_permission('orders','index')){ ?>
<a href="<?= site_url('back_office/orders/view/'.$val["id_orders"].'/'.$val['rand']);?>" class="table-edit-link" title="View" >
<i class="icon-search" ></i> View</a> <span class="hidden"> | </span>
<? } ?>
<? if ($this->acl->has_permission('orders','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_orders"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>