<?php 
$bool=false;
if(isset($order_details['user']) && !empty($order_details['user']) && count($order_details['line_items'])>0){$bool=true;}
?>

<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/jquery.dow.form.validate2.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/custom/forms_calls.js"></script>

<script type="text/javascript" >
$(document).ready(function(){
$('#brands').change(function(){
	var id_supplier="";
		if($('#supplier')){
		var id_supplier=$('#supplier').val();}
		
	var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	
	getBrands(val);
		
	});
		
	
	if($('#supplier').length>0){
		var baseurl=$('#baseurl').val();
		var val=$('#supplier').val();
	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('.product_stock').html(Parsedata.html2);
loadSearch();
});
		}
	$('#supplier').change(function(){
		var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	$('#supplier').val(val);
	$('#title_bx').hide();
		$('.empty').show();

		$('.order-details-table .form-controls').hide();
	$('.order-details-table').find('.table').parent().parent().remove()
	if(val==""){/*$('#suppliers_brands').html('');*/
	$('.product_stock').html('');}
$('#suppliers_brands').html('<div class="loader"></div>');	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('#suppliers_brands').html(Parsedata.html);


$('.product_stock').html(Parsedata.html2);

$('#brands').change(function(){
	
	var obj=$(this);
	var val =obj.val();
	
getBrands(val);
	});
		
		
		
		
    	});

});});
		
</script>
<script type="text/javascript" >
$(function(){




$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});</script>
<script type="text/javascript" >

function remove2(section,id_line_item,id_order,rand){
window.location="<?= site_url('back_office/inventory/remove_product'); ?>/"+section+"/"+id_line_item+"/"+id_order+"/"+rand;
return false;
}

function getBrands(id_brand){

	var id_supplier="";
		if($('#supplier')){
		var id_supplier=$('#supplier').val();}
		
	var baseurl=$('#baseurl').val();

		$('.product_stock').html('');
		$('#title_bx input').val('');
		$('#title_bx').show();
		$('.product_stock').html('');
		$('#loader_1').html('<div class="loader"></div>');
		$.post(baseurl+'back_office/inventory/getStockReachedThreshold',{id_brand:id_brand,id_supplier:id_supplier},function(data){
	$('#loader_1').html('');
	var Parsedata = JSON.parse(data);
$('.product_stock').html(Parsedata.html);
loadSearch();
	});	}
	
function loadSearch(){

	$("#match2 input[name='search']").live('keyup', function(e){

e.preventDefault();
var id =this.id;
$('#match2 tbody tr.tr_m').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr.tr_m').css('display',"");	
} else {
$('#match2 tbody tr.tr_m td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr.tr_m td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});}	
$(document).ready(function(){
	


});
function backToForm(){
	$('#add-more').show();
		$('#addNewProducts').slideUp();
		$('.section-details').slideDown();
		scroll_To('.section-details',20);}
	function addNewProducts(){
		$('#add-more').hide();
	
		$('#addNewProducts').slideDown();
		$('.section-details').slideUp();
		/*scroll_To('#addNewProducts_form',20)*/;
		
		}</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" id="match2">
<div class="span10-fluid" >
<?
$ul = array(
anchor('back_office/inventory','<b>List Inventory</b>').'<span class="divider">/</span>',
anchor('back_office/inventory/request/'.$section,'<b>'.$page_title.'</b>').'<span class="divider">/</span>',
 $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
            );
			if($bool){
$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/print_delivery_order/'.$section.'/'.$order_details['id_delivery_order'].'/'.$order_details['rand']).'" target="_blank" class="btn btn-info top_btn">print / preview</a>');}
/*$ul['new_order'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/add_delivery_order').'" target="_blank" class="btn btn-info top_btn">Add New Order</a>');*/
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>

<table class="table table-striped" id="table-1">

<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>

<? if($this->session->userdata("error_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-error">
<?= $this->session->userdata("error_message"); ?>
</div>
</td>
</tr>
<? } ?>
</table>
<div class="hundred pull-left" >   
<h1 style="font-size:28px;width:100%;text-align:center;"><?php echo $section_name;?>
<?php if(!empty($order_details['id_invoice'])){?>
(#<?php echo $order_details['id_invoice'];?>)
<?php }else{ ?>
(#<?php echo $order_details['id_delivery_order'];?>)
<?php } ?>
</h1>
<?php 
if(isset($errors)){
	echo $errors;}?>
    <?php if($bool){?>
<form id="SetPaidForm" action="<?php echo site_url('back_office/inventory/'.$form); ?>" method="post">


<input type="hidden" name="id_delivery_order" value="<?php echo $order_details['id_delivery_order']; ?>" />
<input type="hidden" name="rand" value="<?php echo $order_details['rand']; ?>" />
<br />
<?php if($bool){?>
<?php if($order_details['invoice']==1){
	$parent=$order_details['parent'];?>


<?php if($order_details['export']==1 && $parent['id_delivery_order']>0){  ?>
<?php 
if($parent['type']==1){
$return_section="delivery_note";	
	}
	
if($parent['type']==2){
$return_section="lpo";	
	}	
	?>
    <label><b>lpo/delivery note:</b> 
<a href="<?php echo route_to('back_office/inventory/view_delivery_request/'.$return_section.'/'.$parent['id_delivery_order'].'/'.$parent['rand']);?>">#<?php echo $parent['id_delivery_order']; ?></a>
<?php }else{  ?>
<?php if($order_details['id_old_delivery_order']>0){ ?>
<label><b>lpo/delivery note:</b> 
<?php if($order_details['status']!=1 ){ ?>
#<input type="text" name="id_old_delivery_order" value="<?php echo $order_details['id_old_delivery_order'];?>" style="width:70px; height:15px;" />
<?php }else{ ?>
#<?php echo  $order_details['id_old_delivery_order'];?>
<?php } ?><?php } ?>
<?php } ?>

</label>
<?php } ?>
<?php if($order_details['invoice']==1){?>
<label><b>Invoice ID: </b>
<?php if($order_details['status']!=1){?>
 #<input type="text" name="id_invoice" value="<?php echo $order_details['id_invoice'];?>" style="width:120px; height:15px;" />
 <?php }else{?>
 #<?php echo $order_details['id_invoice'];?>
 <?php } ?>
<?php }else{ ?>
<label><b><?php echo $section_name;?> :</b> #<?php echo $order_details['id_delivery_order']; ?>
<?php } ?>
</label>
<?php if(isset($order_details['user']) && !empty($order_details['user'])){?>
<label><b><?php echo lang('trading_name');?>:</b> <?php echo $order_details['user']['trading_name']; ?></label>
<label><b>Email:</b> <?php echo $order_details['user']['email']; ?></label>
<?php } ?>
<label><b>Created Date:</b> <?php echo  date("F d,Y h:i A", strtotime($order_details['created_date'])); ?></label>
 <?php if($order_details['status']==1){ ?>
<label><b>Status:</b> <span class="label label-success">Approved</span></label>
 <?php } ?>
 
  <?php if($order_details['status']==5){ ?>
<label><b>Status:</b> <span class="label label-success"><?php echo lang('approved_by_client');?></span></label>
 <?php } ?>
  <?php } ?>
 
<?php /*?> <?php if($order_details['type']==1){ ?>
<label><b>Type :</b> <?php echo lang('paid_m'); ?></label>
<?php } ?> 
<?php if($order_details['type']==2){ ?>
<label><b>Type :</b> <?php echo lang('consignment_m'); ?></label>
 <?php } ?> <?php */?>
<?php echo form_error("qty",'<div class="alert alert-error">','</div>');?>
<br />


<?php $brands_line_items=$order_details['line_items'];?>

<?php if(!empty($brands_line_items)) {?>
<?php foreach($brands_line_items as $brand) {

 $line_items=$brand['line_items'];?> 
<table class="table table-striped order-details-table">
  <tr><td colspan="7" style="font-size:25px; background:#CCC; color:white; text-align:center;"><?php echo $brand['title'];?></td></tr>
          <tr>
          
            <th width="20%">Product Name</th>
            <th width="15%">Sku</th>
            <?php if($order_details['export']==1 || $order_details['invoice']==1){ ?>
             <th width="15%" >Expire Date</th>
             <?php } ?>
            <th width="15%" style="text-align:center;">Quantity</th>
            
             <?php if($order_details['status']==1){ ?>
<!--              <th width="10%" style="text-align:center;">Return Quantity</th>
              <th width="10%" style="text-align:center;">Sold Quantity</th>-->
			 <?php } ?>
             
            <th width="15%">Price(<?php echo $this->config->item('default_currency');?>)</th>
            <th width="15%">Cost(<?php echo $this->config->item('default_currency');?>)</th>
     <th  width="10%"></th>
          </tr>
          
 
          
     <?php foreach($line_items as $pro){
		

		if($pro['type']=="option"){
		$sku=$pro['stock']['sku'];	
		$options=unSerializeStock($pro['stock']['combination']); 
			 }else{
		$sku=$pro['product']['sku'];
		$options="";}
		  ?>
		
	
<tr class="order-table-row">
    
    
        <td>
		 <input type="hidden" name="ids[]" value="<?php echo $pro['id_delivery_order_line_items'];?>" />
		<?php echo $pro['product']['title'];?>
        <br />    <?php
		if(!empty($options)){
		 $i=0; foreach($options as $opt){$i++;
		if($i==1){
			echo $opt;
			}else{
			echo ", ".$opt;
			}}}?>
        </td>
        <td><?php echo $sku;?></td>
      
       <?php if(($order_details['export']==1 || $order_details['invoice']==1)){ ?>
        <td >
        <?php if($order_details['status']==1){ echo $pro['expire_date'];}else{ ?>
<input type="text"  name="expire_date[]"   class="date" data-date-format="dd/mm/yyyy" id="datepicker" value="<?php echo $this->fct->date_out_formate($pro['expire_date']);?>" >
        <?php }?></td>
        <?php } ?>
        <td style="text-align:center;">
       <?php if($order_details['status']!=5 && $order_details['status']!=1  && empty($order_details['exported_order'])){?> 
	    <input type="text"  name="qty[]"  style="width:50px;" value="<?php echo set_value("qty",$pro["quantity"]);?>" >
		<?php }else{ ?>
         <input type="hidden"  name="qty[]"  value="<?php echo set_value("qty",$pro["quantity"]);?>" >
        <?php  echo $pro['quantity']; }?></td>
        
     
<?php /*?>   <?php if($order_details['status']==1){ ?>
		  <td style="text-align:center;"><?php echo $pro['return_quantity'];?></td>
          <td style="text-align:center;"><?php echo $pro['sold_quantity'];?></td>
		  <?php } ?><?php */?>
        
        <td>
         <?php if($order_details['status']!=5 && $order_details['status']!=1  && empty($order_details['exported_order'])){?>
           <input type="text"  name="price[]"  style="width:50px;" value="<?php echo $pro['price'];?>" >
          <?php }else{?>
          <input type="hidden"  name="price[]" value="<?php echo $pro['price'];?>" >
		<?php	   echo $pro['price']; }?></td>
        
                <td>
             
        <?php if($order_details['status']!=1 && empty($order_details['exported_order'])){?>
         <input type="text"  name="cost[]"  style="width:75px;" value="<?php echo $pro['cost'];?>" > <?php }else{ ?>
         
		<?php	 echo $pro['cost'];}?></td>
        
        <td style="text-align:center;">
       <?php if($order_details['status']!=5 && $order_details['status']!=1 && (empty($order_details['exported_order']) || $order_details['invoice']==1) ){ ?>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2('<?php echo $section;?>',<?php echo $pro['id_delivery_order_line_items'];?>,<?php echo $pro['id_orders'];?>,<?php echo $order_details['rand'];?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>

</a>
<?php } ?></td>

        <!--<td>
        <a class="table-delete-link cur" onclick="if(confirm('Are you sure you want to remove this item ?')){ remove2(<?php echo $rand;?>); }" data-original-title="Delete">
<i class="icon-remove-sign"></i>
Remove
</a></td>-->
        </tr>
			 
	 <?php } ?> 


       
        </table>
     <?php } ?>
<?php }?>
<?php if($bool){?>
<div class="row-fluid">
 <?php if($order_details['status']!=5 && $order_details['status']!=1 && (empty($order_details['exported_order']) || $order_details['invoice']==1) ){ ?>
<div class="pull-left">
<a class="btn btn-primary" id="add-more" onclick="addNewProducts()"> + Add More Products</a></div>
<?php } ?>
<table width="300px" align="right">

<tr><td style="text-align:right; font-size:18px;padding:30px 0;"><b>Total Cost </b>:</td><td> <?php echo $order_details['total_cost'];?> <?php echo $order_details['currency'];?></td></tr>
</table>
</div>
<?php } ?>

<div class="section-details">
<div class="row-fluid">
<div class="span3">
<label class="field-title"><b>Payment Terms:</b></label>
<?php if($order_details['status']!=1 && empty($order_details['exported_order'])){ ?>
<label><input type="text" class="input-xxlarge" name="payment_terms" value="<?=$order_details["payment_terms"]; ?>" />
<?php }else{ ?>
<label><?=$order_details["payment_terms"]; ?></label>
<?php } ?>
</label>
</div>

<div class="span3">
<label class="field-title"><b>Delivery Date:</b></label>
<label>
<?php if($order_details['status']!=1 && empty($order_details['exported_order'])){ ?>
<input type="text" class="input-xxlarge date" name="delivery_date" data-date-format="dd/mm/yyyy" id="datepicker" value="<?php echo $this->fct->date_out_formate($order_details['delivery_date']);?>"/>
<?php }else{?>
<?php echo  date("d.m.Y", strtotime($order_details['delivery_date'])); ?>
<?php } ?> 
</label>
</div>

</div>

<div class="row-fluid">
<span class="span12">
<label class="field-title"><b>Order Conditions:</b> </label>
<label>
<?php if($order_details['status']!=1 && empty($order_details['exported_order'])){ ?>
<textarea class="input-xxlarge ckeditor" id="order_conditions" name="order_conditions" ><?=$order_details["order_conditions"]; ?></textarea>
<?php }else{?>
<?=$order_details["order_conditions"]; ?>
<?php } ?>
</label>
</span>
</div>

<?php //if($order_details['status'] != 'paid') {?>
<?php echo br(); ?>
<? if (!checkIfSupplier_admin()){ ?>
    <?php if($order_details['status']!=1 && ( $order_details['export']==1 || $order_details['invoice']==1)){ ?>
<div class="row-fluid">
<div class="span4" style="float:right">
<label><strong>Status</strong></label>
<label>
<select name="status">
<option value="2" <?php if($order_details['status'] == '2') echo 'selected="selected"'; ?>>Pending</option>
<option value="1" <?php if($order_details['status'] == '1') echo 'selected="selected"'; ?>>Approved</option>
<option value="4" <?php if($order_details['status'] == '4') echo 'selected="selected"'; ?>>In progress</option>
<option value="5" <?php if($order_details['status'] == '5') echo 'selected="selected"'; ?>>Approved by client</option>
<option value="3" <?php if($order_details['status'] == '3') echo 'selected="selected"'; ?>>Canceled</option>
</select>
</label></div>
 
<!--<div class="span4">
<label><strong>Type</strong></label>
<label>
<select name="type">
<option value="2" <?php if($order_details['type'] == '2') echo 'selected="selected"'; ?>><?php echo lang('consignment_m'); ?></option>
<option value="1" <?php if($order_details['type'] == '1') echo 'selected="selected"'; ?>><?php echo lang('paid_m'); ?></option>

</select>
</label></div>-->


</div>  <?php } ?>
<?php } ?>

<input type="hidden" name="type" value="<?php echo $order_details['type'];?>" />
<input type="hidden" name="section" value="<?php echo $section;?>" />
<div class="pull-right">

 

 <?php if($order_details['status']!=1 && (empty($order_details['exported_order']) || $order_details['invoice']==1) ){ ?>

<?php if($order_details['invoice']!=1){?>
<input type="submit" name="submit" value="Send to Supplier" class="btn btn-primary" id="SetPaid" />
<?php } ?>

<?php 

if($order_details['export']!=1){?>
<?php if($section=="lpo"){
$export_to="Invoice";	
	}?>
    
<?php if($section=="stock"){
$export_to="Delivery Note";	
	}?>
  <?php if($order_details['invoice']!=1 && isset($export_to)){?>  
  <?php if($order_details['status']==5){?>
	  <input type="hidden" name="status_m" value="<?php echo $order_details['status'];?>" />
  <?php }?>
<input type="submit" name="submit" value="Export to <?php echo $export_to;?>" class="btn btn-primary" id="SetPaid" />
<?php } ?>
<?php } ?>
<input type="submit" name="submit" value="Save Changes" class="btn btn-primary" id="SetPaid" />
<?php if($section=="delivery_note"){?>
<input type="submit" name="submit" value="Export to Invoice" class="btn btn-primary" id="SetPaid" />
<?Php } ?>
</div>
<?php }else{?>
<?php  }?>
</div>

        
 </form>    
 <?php }?>   </div>
 
 
 
 <div class="row-fluid sortable" id="addNewProducts" style="margin-top:20px; <?php if($bool){?>display:none<?php } ?>">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-info-sign"></i> Add Product(s)</h2>
              <?php if($bool){?>
              <div class="box-icon"><a  onclick="backToForm()" class="btn  btn-round"><i class="icon-remove"></i></a></div>
              <?php } ?>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form  method="post" enctype="multipart/form-data" id="addNewProducts_form" action="<?php echo site_url('back_office/inventory/insertNewProducts');?>" >
            <input type="hidden" name="section" value="<?php echo $section;?>" />
             <input type="hidden" name="id_delivery_order" value="<?php echo $order_details['id_delivery_order'];?>" />
             <input type="hidden" name="rand" value="<?php echo $order_details['rand'];?>" />
              <div class="row-fluid">
<?php if(isset($order_details['user']) && !empty($order_details['user'])){ ?>
<input type="hidden" name="id_user" value="<?php echo $order_details['user']['id_user'];?>"  id="supplier" />
<?php  }else{?>
  <div class="span3 form-item">
<label>Trading Name *</label>
<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name asc',$cond3);
?>

<select name="id_user" id="supplier"  >
<option value="">-Select trading name-</option>
<?php 
foreach($suppliers as $val){?>
<option value="<?php echo $val['id_user'];?>" ><?php echo $val['trading_name'];?></option>
<?php } ?>
</select>
 <div id="id_user-error" class="form-error"></div>

</div>    
<?php } ?>        
              
<div class="span3" id="suppliers_brands">
<label>Brand </label>
<?php 
$brands=array();
if(isset($order_details['user']) && !empty($order_details['user'])){
$brands = $this->fct->getAll_cond('brands','title asc',array('id_user'=>$order_details['id_user']));
 } ?>


<select name='brands' id="brands">
<option value="">-Select All-</option>
<?php 
foreach($brands as $val){?>
<option value="<?php echo $val['id_brands'];?>"><?php echo $val['title'];?></option>
<?php } ?>
</select>

</div>


<!--<div class="span3" id="title_bx">
<label>Product Name</label>
<input type="text" name="title" value="" class="searchProducts" id="searchProducts" />

</div>-->



<!--<div class="span3">
<label>Quantity</label>
<input type="text" name="quantity" value="" />
</div>-->
<div class="span4">


</div>
</div>

<div class="product_stock"></div>

<div id="loader_1"></div>
 
</form>
              </div>
            </div>
          </div>
        </div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>