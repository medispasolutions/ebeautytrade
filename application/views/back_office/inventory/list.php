<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" >
$(document).ready(function(){

loadBrands();
	
	$('#supplier').change(function(){
	$('#suppliers_brands').show();
	var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	$('#id_user').val(val);
	/*$('#title_bx, #filter').hide();*/

$('#suppliers_brands').html('<div class="loader"></div>');	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('#suppliers_brands').html(Parsedata.html);

if(val!=""){
		$('.product_stock').html('');

		$('#title_bx input').val('');
		$('#title_bx').show();}else{
		
		$	}

loadBrands();

});});

    	
	});
    
   function loadBrands(){
   $('#brands').change(function(){
	var obj=$(this);
	var val =obj.val();
	if(val=="dddd"){$('#total_quantity').hide();/*$('#title_bx').hide();*/$('#filter, #table-1, .pagination_container').hide();}else{
		$('.product_stock').html('');

		$('#filter').show();
	$('#total_quantity').show();/*$('#title_bx').hide();*/$('#filter, #table-1, .pagination_container').show();
		$('#title_bx input').val('');
		$('#title_bx').show();}
    	});} 
    </script>

<script>
function delete_row(id){
window.location="<?= site_url("back_office/products/delete"); ?>/"+id;
return false;
}

$(function(){
	$('#category').change(function(){
	
		var id = $(this).val();
		if(id == '') {
			return false;
		}
		else {
			var url = "<?php echo site_url("back_office/categories_sub/load_sub_filter2"); ?>";
			$("#sub_category").attr("disabled","disabled");
			$("#sub_category").html("");
			$("#category").attr("disabled","disabled");
			$.post(url,{id : id},function(data){
				$("#sub_category").removeAttr("disabled");
				$("#category").removeAttr("disabled");
				$("#sub_category").html(data);
			});
		}
	});
	


$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="1";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<?php if($page==""){?>
<li class="active"><?= $title; ?></li>
<?php }else{ ?>
<li><a href="<?php echo site_url('back_office/inventory');?>"><b>List Inventory</b></a></li>
<li class="divider">/</li>
<li class="active">products inventory</li>
<?php } ?>

</ul> 
</div>
<?php if($page==""){?>
<? $this->load->view("back_office/inventory/head"); ?>
<?php }else{ ?>

<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">
<div class="row-fluid" style="margin-top:10px;">
<a class="btn btn-primary pull-right" href="javascript: history.go(-1)">Back</a>
</div>
<form method="get" action="<?=site_url("back_office/inventory/index/page")?>">
<input type="hidden" name="search" value="on" />
<br />


<div class="row-fluid sortable" id="filter"  >
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
<div class="sortable row-fluid">
<div class="row-fluid">
<?php $user=$this->ecommerce_model->getUserInfo();?>
<?php $supplier=false;?>
<?php if(!checkIfSupplier_admin()){?>
    <div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">
    <label><?php echo lang('trading_name');?></label>

	<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name',$cond3);
?>

<select name="supplier" id="supplier">
<option value="all">-Select <?php echo lang('trading_name');?>-</option>
<?php 


foreach($suppliers as $val){
$cl = '';
	if(isset($_GET['supplier']) && $_GET['supplier'] == $val['id_user'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_user'];?>"  <?php echo $cl;?>><?php echo $val['trading_name'];?></option>
<?php } ?>
</select>
</div>
<?php }else{ $supplier=true; ?>
 
<input type="hidden" name="supplier" value="<?php echo $user['id_user'];?>" />
<?php } ?>


<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;"   id="suppliers_brands">

<label>Brand</label>
	<?php 
	$cond['user.status'] =  1;
	$cond2=array();
	if($supplier){
	$cond2['id_user']=$user['id_user'];}
	if($this->input->get('supplier')!=""){
	$cond2['id_user']=$this->input->get('supplier');	}
	$brands=$this->fct->getAll_cond('brands','title',$cond2); ?>
	<select name="brands" id="brands">
	<option value="">- Select All -</option>
	<?php foreach($brands as $brand) {
		$cl = '';
	if(isset($_GET['supplier']) && $_GET['supplier'] == $brand['supplier'])
	$cl = 'selected="selected"';
	$cl = '';
	if(isset($_GET['brands']) && $_GET['brands'] == $brand['id_brands'])
	$cl = 'selected="selected"';
	?>
   <option value="<?php echo $brand['id_brands']; ?>" <?php echo $cl; ?>><?php echo $brand['title']; ?></option>
<?php }?>


</select></div>
<!--Product Name-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;" id="title_bx"><label>Product Name</label><input type="text" id="product_name" name="product_name" value="<? if(isset($_GET['product_name'])) { echo $_GET['product_name']; }?>" /></div>
<!--Approval-->
<!--<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;" >

<label>Status</label><select name="order_type">
<option value="">-Select Status-</option>
<option value="2" <? if(isset($_GET['order_type']) &&$_GET['order_type']==2 ) { echo 'selected="selected"'; }?>>Paid stock</option>
<option value="1" <? if(isset($_GET['order_type']) &&$_GET['order_type']==1 ) { echo 'selected="selected"'; }?>>In Consignment</option>

</select></div>-->

</div>
<!--DATE -->


<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" value="Filter" /> 
<!--<input type="submit" class="btn btn-primary" name="report_submit" value="export" />-->
<button type="submit" class="btn btn-primary  btn-danger" name="report_submit" value="export" ><i class="icon-download"></i> Export</button>
</div>

             </div>
            </div>
          </div>
        </div>
        
 </form>

</div>

 <!--<div id="total_quantity">
 <label><b>Total products in <?php echo lang('paid_m'); ?></b> = <?php echo $info['total_qty_payable'];?> / <b>paid</b> = <?php echo $info['total_qty_payable_paid'];?></label>
 <label><b>Total products in <?php echo lang('consignment_m'); ?></b> = <?php echo $info['total_qty_sold_by_admin'];?> / <b>paid</b> = <?php echo $info['total_qty_sold_by_admin_paid'];?></label></div>-->
 
<?php if($this->input->get('search')!=""){?>
<div id="total_quantity">
 <label><b>Total Qty</b> = <?php echo $info['total_qty'];?></label>
 </div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/products/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>


<th>
Product Name
</th>

<th>SKU</th>
<th>Brand</th>

<th style="text-align:center; width:150px;"><?php echo lang('quantity'); ?></th>
<?php /*?><?php if($supplier_admin){?>
<td>Status</td>
<?php } ?><?php */?>
<!--<th style="text-align:center"><?php echo lang('paid_m'); ?></th>-->
<th></th>
</tr>

</thead><tfoot>
</tfoot>
<tbody>
<? 

$products=$info['products'];
if(isset($products) && !empty($products)){
$i=0;

foreach($products as $val){

$i++; 
?>
<tr id="<?=$val["id_products"]; ?>">



<td class="title_search">
<b><? echo $val["title".$lang]; ?></b><br />
<?php if(isset($val['combination']) && !empty($val['combination'])){
$options=unSerializeStock($val['combination']);
if(!empty($options)) { 

		$j=0;
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="";}else{$com=", ";}?>
                          <?php echo $com.$opt; ?>
                          <?php }}}?>
</td>

<td >
<? echo $val["sku"]; ?>
</td>


<td >
<? echo $val["brand_name"]; ?>
</td>

<td style="text-align:center">
<? echo $val["quantity"]; ?>
</td>

<?php /*?><?php if($supplier_admin){?>
<td><?php if($val['remain_paid']==1){echo "In Consignment";
	}elseif($val['remain_consignment']==1){ echo "Paid stock";}?></td>
<?php } ?><?php */?>

<?php /*?><td  style="text-align:center">
<? echo $val["qty_payable"]; ?>
</td><?php */?>



<td>
<? if ($this->acl->has_permission('inventory','index')){ ?>
<a href="<?= site_url('back_office/inventory/view/'.$val["type"].'/'.$val["id_products"]);?>" class="table-edit-link" title="View" >
<i class="icon-search" ></i> View Details</a>
<? } ?>
</td>

</tr>

<?php  }} else { ?>
<tr class='odd'><td colspan="10" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<!--<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/products"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>-->
<div class="pagination" style="text-align:center;">
<? echo $this->pagination->create_links(); ?>
</div>
</div> 
<?php } ?>
 <?php } ?>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 