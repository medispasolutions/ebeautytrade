<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" >
$(document).ready(function(){
	$('#supplier').change(function(){
		$('#suppliers_brands').show();
		var baseurl=$('#baseurl').val();
	var obj=$(this);
	var val =obj.val();
	$('#id_user').val(val);
	$('#title_bx, #filter').hide();
	$('#table-1, .pagination_container').remove();
		$('.empty').show();
		$('.order-details-table .btn').hide();
	$('.order-details-table').find('.table').parent().parent().remove()
	if(val==""){$('#suppliers_brands').html('');$('.product_stock').html('');}else{
$('#suppliers_brands').html('<div class="loader"></div>');	
$.post(baseurl+'back_office/inventory/getBrands',{id_supplier:val},function(data){
	
var Parsedata = JSON.parse(data);
$('#suppliers_brands').html(Parsedata.html);


$('#brands').change(function(){
	var obj=$(this);
	var val =obj.val();
	if(val==""){$('#title_bx').hide();$('#filter, #table-1, .pagination_container').hide();}else{
		$('.product_stock').html('');
		$('#filter').show();
		$('#title_bx input').val('');
		$('#title_bx').show();}
    	});

});
		
}
    	});
	});</script>

<script>
function delete_row(id){
window.location="<?= site_url("back_office/products/delete"); ?>/"+id;
return false;
}

$(function(){
	$('#category').change(function(){
	
		var id = $(this).val();
		if(id == '') {
			return false;
		}
		else {
			var url = "<?php echo site_url("back_office/categories_sub/load_sub_filter2"); ?>";
			$("#sub_category").attr("disabled","disabled");
			$("#sub_category").html("");
			$("#category").attr("disabled","disabled");
			$.post(url,{id : id},function(data){
				$("#sub_category").removeAttr("disabled");
				$("#category").removeAttr("disabled");
				$("#sub_category").html(data);
			});
		}
	});
	


$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="1";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<?php if($page==""){?>
<li class="active"><?= $title; ?></li>
<?php }else{ ?>
<li><a href="<?php echo site_url('back_office/inventory');?>"><b><?= $title; ?></b></a></li>
<li class="divider">/</li>
<li class="active">Reports</li>
<?php } ?>

</ul> 
</div>
<?php if($page==""){?>
<? $this->load->view("back_office/inventory/head"); ?>
<?php }else{ ?>

<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">
<form method="get" action="<?=site_url("back_office/inventory/index/page")?>">
<br />
<div class="row-fluid">
    <div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">
    <label>Supplier</label>

	<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name asc',$cond3);
?>

<select name="supplier" id="supplier">
<option value="">-Select Supplier-</option>
<?php 


foreach($suppliers as $val){
$cl = '';
	if(isset($_GET['supplier']) && $_GET['supplier'] == $val['id_user'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_user'];?>"  <?php echo $cl;?>><?php echo $val['name'];?></option>
<?php } ?>
</select>
</div>

<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px; <?php if(!checkIfSupplier_admin() && $this->input->get('brands')==""){ echo "display:none"; }?>"  id="suppliers_brands">

<label>Brand</label>
	<?php 
	$cond['user.status'] =  1;
	$brands=$this->fct->getAll('brands','title asc'); ?>
	<select name="brands" id="brands">
	<option value="">- All -</option>
	<?php foreach($brands as $brand) {
		$cl = '';
	if(isset($_GET['supplier']) && $_GET['supplier'] == $brand['supplier'])
	$cl = 'selected="selected"';
	$cl = '';
	if(isset($_GET['brands']) && $_GET['brands'] == $brand['id_brands'])
	$cl = 'selected="selected"';
	?>
   <option value="<?php echo $brand['id_brands']; ?>" <?php echo $cl; ?>><?php echo $brand['title']; ?></option>
<?php }?>
</select></div>
</div>

<div class="row-fluid sortable" id="filter"  <?php if($this->input->get('brands')=="") { ?>style="display:none;"<?php } ?>>
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
<div class="sortable row-fluid">




<!--Product Name-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;" id="title_bx">Product Name<br /><input type="text" id="product_name" name="product_name" value="<? if(isset($_GET['product_name'])) { echo $_GET['product_name']; }?>" /></div>

<!--SKU-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:42px;">SKU<br /><input type="text" id="sku" name="sku" value="<? if(isset($_GET['sku'])) { echo $_GET['sku']; }?>" /></div>

<!--Sales-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Sales<br />
<select name="sales" id="sales">
<option <?php if(isset($_GET['sales']) && $_GET['sales'] == 2)echo 'selected="selected"';?> value="2">-All-</option>
<option <?php if(isset($_GET['sales']) && $_GET['sales'] == 1)echo 'selected="selected"';?>  value="1">Set</option>
<option <?php if(isset($_GET['sales']) && $_GET['sales'] == 0)echo 'selected="selected"';?> value="0">Unset</option>
</select>
</div>

<!--Category-->
<div class="fl" style="margin:0 15px 0 0;width: 225px; float:left;">Category<br />
<?php $categories = $this->fct->getAll_cond('categories','title',array('id_parent'=>0)); ?>
<select name="category" id="category">
<option value="">- All -</option>
<?php foreach($categories as $cat) {
	$cl = '';
	if(isset($_GET['category']) && $_GET['category'] == $cat['id_categories'])
	$cl = 'selected="selected"';
?>
<option value="<?php echo $cat['id_categories']; ?>" <?php echo $cl; ?>><?php echo $cat['title'.getFieldLanguage($this->lang->lang())]; ?></option>
<?php }?>
</select>
</div>

<!--Sub Category-->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Sub Category<br />
<?php 
if($this->input->get("category") != "") {
$category = $this->fct->getonerow("categories",array("id_categories"=>$this->input->get("category")));
if(!empty($category)) {
$id_selected_category=$category['id_categories'];

}
}

?>
<select name="sub_category" id="sub_category">
<?php if(isset($id_selected_category)){
$limit = $this->custom_fct->getCategoriesPages($id_selected_category);
$sub_categories = $this->custom_fct->getCategoriesPages($id_selected_category,array(),array(),$limit,0);?>
<?php
	$cl = '';
	$selected_options=array();
	if(isset($_GET['sub_category']) && !empty($_GET['sub_category'])){
		$id_sub_category=$_GET['sub_category'];
		$selected_options=array($id_sub_category);}	
?>
<option value="">- All -</option>

<?php 

$s_id = '';
if(isset($id)) {
$s_id = $info['id_parent'];
}
echo displayLevels($sub_categories,0,$s_id,$selected_options);

?>



<?php } ?>
</select>
</div>

<!--DATE -->
<div class="fl" style="width: 360px; float:left;">
<div class="fl" style="width: 180px; float:left;">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['from_date'])) echo $_GET['from_date'] ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 180px; float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['to_date'])) echo $_GET['to_date'] ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>

<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" value="Filter" /> 
<input type="submit" class="btn btn-primary" name="report_submit" value="export" />
</div>

             </div>
            </div>
          </div>
        </div>
        
 </form>

</div>
 <?php if($this->input->get('supplier')!="" && $this->input->get('brands')!=""){ ?>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/products/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="10" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>


<th>
Product Name
</th>
<th>Created Date</th>
<th>SKU</th>
<th>Brand</th>
<th>Qty sold</th>
<th>Qty payable</th>
<th></th>
</tr>

</thead><tfoot>
</tfoot>
<tbody>
<? 
if(isset($info) && !empty($info)){
$i=0;
foreach($info as $val){
	

$i++; 
?>
<tr id="<?=$val["id_products"]; ?>">



<td class="title_search">
<b><? echo $val["title".$lang]; ?></b><br />
<?php if(isset($val['combination']) && !empty($val['combination'])){
$options=unSerializeStock($val['combination']);
if(!empty($options)) { 

		$j=0;
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="";}else{$com=", ";}?>
                          <?php echo $com.$opt; ?>
                          <?php }}}?>
</td>
<td >
<?php echo  date("F d,Y", strtotime($val['created_date'])); ?>
</td>
<td >
<? echo $val["sku"]; ?>
</td>

<td >
<? echo $val["brand_name"]; ?>
</td>

<td style="text-align:center">
<? echo $val["qty_sold_by_admin"]; ?>
</td>

<td  style="text-align:center">
<? echo $val["qty_payable"]; ?>
</td>



<td>
<? if ($this->acl->has_permission('inventory','index')){ ?>
<a href="<?= site_url('back_office/inventory/view/'.$val["type"].'/'.$val["id_products"]);?>" class="table-edit-link" title="View" >
<i class="icon-search" ></i> View Details</a>
<? } ?>
</td>

</tr>

<?php  }} else { ?>
<tr class='odd'><td colspan="10" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/products"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div> <?php } ?>
 <?php } ?>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 