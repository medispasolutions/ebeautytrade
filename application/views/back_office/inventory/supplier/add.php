

<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
anchor('back_office/inventory/'.$this->session->userdata("back_link"),'<b>List inventory</b>').'<span class="divider">/</span>',anchor('back_office/inventory/index/page','<b>list inventory reports</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			);

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<?php $product=$inventory['product'];?>
<div class="hundred pull-left">
<br />   
<?php echo form_fieldset("Product Detailts"); ?>
<label><b>Product Name: </b> <?php echo $product['title']; ?></label>
<?php if(isset($product['combination']) && !empty($product['combination'])){ ?>
	<label><b>Option(s): </b> 
<?php $options=unSerializeStock($product['combination']);
if(!empty($options)) { 

		$j=0;
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="";}else{$com=", ";}?>
                          <?php echo $com.$opt; ?>
                          <?php }}?>
                          </label>
						  <?php }?>
<label><b>SKU: </b> <?php echo $product['sku']; ?></label>
<label><b>Brand Name: </b> <?php echo $product['brand_name']; ?></label>
<label><b>Created Date: </b> <?php echo $product['created_date']; ?></label>
<label><b>Price: </b> <?php echo $product['list_price']; ?> <?php echo $this->config->item('default_currency'); ?></label>
<label><b>Sell Price: </b> <?php echo $product['price']; ?> <?php echo $this->config->item('default_currency'); ?></label>
<?php if($product['discount_status']==1){?>
<label><b>Discount Expiration: </b> <?php echo $product['discount_expiration'] ;?></label>
<?php } ?>
<label><b>Remaining Quantity: </b> <?php echo $product['quantity']; ?></label>

<?php echo form_fieldset_close();?>

<div class="hundred pull-left">   

<h3>Product Purchases</h3>
<?php $purchases=$inventory['product']['purchases'];?>



<table class="table table-striped">
<tr>
    <th>Status</th>
    <th>Created Date</th>
    <th>Quantity</th>
    <th>price</th>
    <th>cost</th>
</tr>
<?php if(!empty($purchases)) {?>
<?php 
$net_price = 0;
foreach($purchases as $val) {
	?>
<tr>

<td><? 
if($val["status"] == 1) {?>
<span class="label label-success">paid</span>
<?php } else {?>
<span class="label label-important">payable</span>
<?php }?></td>
<td><?php echo $val['created_date'];?></td>
<td><?php echo $val['quantity'];?></td>
<td><?php echo $val['price'];?> <?php echo $this->config->item('default_currency'); ?></td>
<td><?php echo $val['cost'];?> <?php echo $this->config->item('default_currency'); ?></td>
</tr>
<?php }?>
<?php }else{ ?>
<tr><td colspan="10" style="text-align:center">No Purchases Found</td></tr>
<?php } ?>
</table>



<?php
$line_items = $inventory['line_items'];?>

<h3>Product Orders</h3>
<table class="table table-striped">
<tr>
    <th>Order Id</th>
    <th>Created Date</th>
    <th>Status</th>

    <th>Price</th>
    <th>Quantity</th>
    <th>Total Price</th>

</tr>
<?php 
$line_items=$inventory['line_items'];
//print '<pre>';print_r($line_items);exit;
if(!empty($line_items)) {?>
<?php 
$net_price = 0;
foreach($line_items as $line_item) {
	
	?>
<tr>
<td><?php echo $line_item['id_orders'];?></td>
<td><?php echo $line_item['created_date'];?></td>
<td><? 
if($line_item["status"] == 'paid' || $line_item["status"] == 'completed') {?>
<span class="label label-success"><?php echo $line_item["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $line_item["status"]; ?></span>
<?php }?></td>

<td><?php echo $line_item['price_currency'].' '.$line_item['currency']; ?></td>
<td style="text-align:center;"><?php echo $line_item['quantity']; ?></td>
<td><?php echo $line_item['total_price'].' '.$line_item['currency']; ?></td>
</tr>
<?php }?>
<?php }else{ ?>
<tr><td colspan="10" style="text-align:center">No orders found</td></tr>
<?php } ?>
</table>


</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>