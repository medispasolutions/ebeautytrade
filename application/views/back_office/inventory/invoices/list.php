<script>
function delete_row(id){
window.location="<?= base_url(); ?>back_office/sales_history/delete/"+id;
return false;
}
$(document).ready(function(){
$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">


<li><a href="<?php echo site_url('back_office/inventory');?>"><b>List Inventory</b></a></li>
<li class="divider">/</li>
<li class="active"><?= $title; ?></li>
</ul> 
</div>
<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">

<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form method="get" action="<?=base_url()?>back_office/inventory/invoices">
 <!--FIRST NAME -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Invoice ID<br /><input type="text" id="invoice" name="invoice" value="<? if(isset($_GET['invoice'])) { echo $_GET['invoice']; }?>" /></div> 
<?php if(!checkIfSupplier_admin()){ ?>
<!--Suppliers -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;"><label><?php echo lang('trading_name');?></label>  
<?php 
$cond3['id_roles']=5;
$cond3['completed']=1;
$suppliers=$this->fct->getAll_cond('user','trading_name asc',$cond3);
?>

<select name="supplier" id="supplier">
<option value="">-Select <?php echo lang('trading_name')?>-</option>
<?php 


foreach($suppliers as $val){
$cl = '';
	if(isset($_GET['supplier']) && $_GET['supplier'] == $val['id_user'])
	$cl = 'selected="selected"';	
	?>
<option value="<?php echo $val['id_user'];?>"  <?php echo $cl;?>><?php echo $val['trading_name'];?></option>
<?php } ?>
</select></div>
<?php } ?>
<!--STATUS -->
<!--<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Status<br />
<select name="status" id="status">
<option value="">- All -</option>
<option value="pending" <?php if(isset($_GET['status']) && $_GET['status'] == 'pending') { ?>selected="selected"<?php }?>>Pending</option>
<option value="completed" <?php if(isset($_GET['status']) && $_GET['status'] == 'completed') { ?>selected="selected"<?php }?>>Completed</option>
<option value="paid" <?php if(isset($_GET['status']) && $_GET['status'] == 'paid') { ?>selected="selected"<?php }?>>Paid</option>
<option value="canceled" <?php if(isset($_GET['status']) && $_GET['status'] == 'canceled') { ?>selected="selected"<?php }?>>Canceled</option>
</select>
</div>-->

<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;" >
<!--Approval-->
<label>Status</label><select name="order_type">
<option value="">-Select Status-</option>
<option value="2" <? if(isset($_GET['order_type']) &&$_GET['order_type']==2 ) { echo 'selected="selected"'; }?>>Paid stock</option>
<option value="1" <? if(isset($_GET['order_type']) &&$_GET['order_type']==1 ) { echo 'selected="selected"'; }?>>In Consignment</option>

</select></div>


<br />
<div style="float:left;width:100%;">
<!--DATE -->
<div class="fl" style="width: 360px; float:left;">
<div class="fl" style="width: 180px; float:left;">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['from_date'])) echo $_GET['from_date'] ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 180px; float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['to_date'])) echo $_GET['to_date'] ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>



<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" name="report_submit" value="Filter" /> 
<!--<input type="submit" class="btn btn-primary" name="report_submit" value="export" />-->
<button type="submit" class="btn btn-primary  btn-danger" name="report_submit" value="export" ><i class="icon-download"></i> Export</button>
</div>
</div>

</form>
              </div>
            </div>
          </div>
        </div>
</div>


<div class="hundred pull-left" id="match2">   
<?php

if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}

?>
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/sales_history/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="4" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th><?php echo lang('invoice_id');?></th>
<th><?php echo lang('created_date');?></th>
<th><?php echo lang('trading_name');?></th>
<th><?php echo lang('total_amount');?></th>
<th><?php echo lang('total_amount_credits');?></th>
<th><?php echo lang('shipping_charge');?></th>
<th><?php echo lang('net_price');?></th>
<th><?php echo lang('status');?></th>
<th></th>
</tr>
</thead><!--<tfoot><tr>
<td colspan="3">
<? if ($this->acl->has_permission('sales_history','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>-->
<tbody>
<?

if(isset($info) && !empty($info)  && count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>
<tr id="<?=$val["id_invoices"]; ?>">
<!--<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_orders"] ; ?>" /></td>-->
<td>#<? echo $val["id_invoices"]; ?></td>
<td><?php echo  date("F d,Y", strtotime($val['created_date'])); ?></td>
<td><?php if(!empty($val['user'])){ echo $val['user']['trading_name'] ;};?></td>
<td><? echo $val["total_amount"]; ?> <?php echo $val['currency'];?></td>
<td><? echo $val["total_amount_credits"]; ?> <?php echo $val['currency'];?></td>
<td><? echo $val["total_customer_charge"]; ?> <?php echo $val['currency'];?></td>
<td><? echo $val["net_price"]; ?> <?php echo $val['currency'];?></td>
<td><? 
if($val["status"] == 'paid' || $val["status"] == 'completed') {?>
<span class="label label-success"><?php echo $val["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $val["status"]; ?></span>
<?php }?></td>
<td><a href="<?php echo site_url('back_office/inventory/view_invoice_details/'.$val['id_invoices'].'/'.$val['rand']);?>">View Details</a></td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/inventory/invoices"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 