<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
anchor('back_office/inventory','<b>List inventory</b>').'<span class="divider">/</span>',anchor('back_office/inventory/invoices','<b>List invoices</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			);
$ul['invoice'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/inventory/invoice/'.$id.'/'.$order_details['rand']).'" target="_blank" class="btn btn-info top_btn">print / preview</a>');
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<div class="hundred pull-left">   

<?php

if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success" style="margin-top:10px;">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}

?> 

<br />

<label><b>Order ID:</b> <?php echo $order_details['id_invoices']; ?></label>
<label><b><?php echo lang('trading_name');?>:</b> <?php echo $userData['trading_name']; ?></label>
<label><b>Created Date:</b> <?php echo $order_details['created_date']; ?></label>
<label><b>Status:</b> <? 
if($order_details["status"] == 'paid' || $order_details["status"] == 'completed') {?>
<span class="label label-success"><?php echo $order_details["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $order_details["status"]; ?></span>
<?php }?></label>
<label><b>Total Amount:</b> <?php echo $order_details['total_amount'].' '.$order_details['currency']; ?></label>

<label><b>Total Amount credits:</b> <?php echo $order_details['total_amount_credits'].' '.$order_details['currency']; ?></label>

<label><b>Shpping Charge: </b> <?php echo $order_details['shipping_charge'].' '.$order_details['currency']; ?></label>

<label><b>Net Price: </b> <?php echo $order_details['net_price'].' '.$order_details['currency']; ?></label>




<h2 style="color:#a0d9f4">DETAILS</h2>

<table class="table table-striped">

<?php $brands=$info['brands'];?>
<?php if(!empty($brands)) {?>

<?php  foreach($brands as $brand) {?>
<tr><td colspan="12" style="text-align:center;"><?php echo $brand['title'];?></td></tr>
<?php 

$line_items = $brand['line_items'];
//print '<pre>';print_r($line_items);exit;

$net_price = 0; ?>
<tr>
    <th ><?php echo lang('order_id');?></th>
    <th>Product</th>
    <th>SKU</th>
    <th><?php echo lang('barcode');?></th>
    <th><?php echo lang('shelf_location');?></th>
    <th><?php echo lang('created_date');?></th>
    <th>Price(<?php echo $order_details['currency']; ?>)</th>
    <th><?php echo lang('quantity');?></th>
    <th><?php echo lang('return_quantity');?></th>
<!--    <th><?php echo lang('paid_m'); ?></th>
    <th><?php echo lang('consignment_m'); ?></th>-->
    <th><?php echo lang('credit_cart');?>(<?php echo $order_details['currency']; ?>)</th>
    <th><?php echo lang('total_price');?>(<?php echo $order_details['currency']; ?>)</th>
</tr>
<?php
foreach($line_items as $pro) { 
 ?>
    
<tr>
   <td>#<?php echo $pro['id_invoices']; ?></td>
<td>

	<?php 
    $product_name = '';
	//if(empty($pro['product']['sku'])) 
	$product_name .= $pro['product']['title'];
	//else
	//$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
	if(!empty($pro['options'])) {
		$options = $pro['options'];
		$options = unSerializeStock($options);
		$product_name .= '<br />';
		$c = count($options);
		$i=0;
		foreach($options as $key => $opt) {
			$i++;
			$product_name .= $opt;
			if($i != $c) $product_name .= ' - ';
		}
	}
	echo $product_name;
	?>

    </td>
  
    <td><?php echo $pro['sku']; ?></td>
    <td><?php echo $pro['product']['barcode']; ?></td>
    <td><?php echo $pro['product']['location_code']; ?></td>
    <td><?php echo  date("F d,Y", strtotime($pro['orders_created_date'])); ?></td>
                
    
    <td><?php echo $pro['price'].' '.$order_details['currency']; ?></td>
    <td><?php echo $pro['quantity']; ?></td>
    <td><?php echo $pro['return_quantity']; ?></td>
<!--    <td><?php echo $pro['qty_sold_by_admin']; ?></td>
    <td><?php echo $pro['qty_payable']; ?></td>-->
    <td><?php echo $pro['credit_cart'].' '.$order_details['currency']; ?></td>
     <td><?php echo $pro['total_price'].' '.$order_details['currency']; ?></td>
    </tr>
<?php }?>

<?php } ?>

<?php }else{?>
<tr><td colspan="6" align="center">Empty</td></tr>
<?php } ?>
</table>

<?php //if($order_details['status'] != 'paid') {?>
<?php echo br(); ?>
<?php if(!checkIfSupplier_admin()){?>
<form id="SetPaidForm" action="<?php echo site_url('back_office/inventory/updateInvoice'); ?>" method="post">
<div class="row-fluid">
<input type="hidden" name="id_invoices" value="<?php echo $id; ?>" />
<input type="hidden" name="rand" value="<?php echo $order_details['rand']; ?>" />
<div class="span3" style="margin-left:0;;">
<label>
<label>Status</label>
<select name="status">
<option value="pending" <?php if($order_details['status'] == 'pending') echo 'selected="selected"'; ?>>Pending</option>
<option value="paid" <?php if($order_details['status'] == 'paid') echo 'selected="selected"'; ?>>Paid</option>
<option value="completed" <?php if($order_details['status'] == 'completed') echo 'selected="selected"'; ?>>Completed</option>
<option value="canceled" <?php if($order_details['status'] == 'canceled') echo 'selected="selected"'; ?>>Canceled</option>
</select>
</label>
</div>
</div>
<div class="row-fluid">
<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Client</label>
<br /></div>
<input type="submit" value="Update Status" class="btn btn-primary" id="SetPaid" />
</form>
<?php } ?>
        
        </div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>