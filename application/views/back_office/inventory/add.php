

<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
$ul = array(
anchor('back_office/inventory','<b>List inventory</b>').'<span class="divider">/</span>',anchor('back_office/inventory/index/page','<b>list products inventory</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
			
			);

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
?>
</div>
<?php $product=$inventory['product'];?>
<div class="hundred pull-left">
<div class="row-fluid" style="margin-top:10px;">
<a class="btn btn-primary pull-right" href="javascript: history.go(-1)">Back</a>
</div>
<br />   
<?php echo form_fieldset(""); ?>
<label><b>Product Name: </b> <?php echo $product['title']; ?>
<?php if(isset($product['combination']) && !empty($product['combination'])){ ?>
	<i> 
<?php $options=unSerializeStock($product['combination']);
if(!empty($options)) { 
echo "-";
		$j=0;
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="";}else{$com=", ";}?>
                          <?php echo $com.$opt; ?>
                          <?php }}?></i>
                          </label>
						  <?php }?>
<label><b>SKU: </b> <?php echo $product['sku']; ?></label>
<label><b>Brand Name: </b> <?php echo $product['brand_name']; ?></label>

<?php echo form_fieldset_close();?>
<br />


<div class="hundred pull-left">   

<?php /*?><?php $purchases=$inventory['product']['purchases'];?><?php */?>


<?php $delivery_orders_line_items=$inventory['orders_line_items'];?>
<table class="table table-striped">
<tr> 
<th width="15%">Delivery Note / Purchaces Invoices</th>
<th  width="10%">Type</th>
<th  width="10%">Created Date</th>
<th style="text-align:center;"  width="10%">Quantity</th>

</tr>
<?php if(!empty($delivery_orders_line_items)) {

	?>
<?php 
$net_price = 0;
$total_remain_quantity=0;
foreach($delivery_orders_line_items as $val) {

	
	if($val['invoice']==1){
	$id_invoice=$val["id_invoice"];
	$link=route_to('back_office/inventory/view_delivery_request/invoices/'.$val['id_delivery_order'].'/'.$val['rand']);}else{
	$id_invoice=$val["id_delivery_order"];
	$link=route_to('back_office/inventory/view_delivery_request/delivery_note/'.$val['id_delivery_order'].'/'.$val['rand']);	}
	$remaining_quantity=$val['quantity']-$val['return_quantity']-$val['sold_quantity'];
	$total_remain_quantity=$total_remain_quantity+$remaining_quantity;

	
      if($val["id_role"] == 5) { $order_name="D-ORDER#"; }else{$order_name="D-RQ#"; }?>
	
	<?php if($remaining_quantity>0){ ?>
<tr>
<td><a href="<?php echo $link;?>" target="_blank"><?php echo '#'.$id_invoice;?></a></td>


<td><? 
if($val["invoice"] == 1) {?>
<span class="label label-success"><?php echo lang('paid_m'); ?></span>
<?php } else {?>
<span class="label label-important"><?php echo lang('consignment_m'); ?></span>
<?php }?></td>

<td><?php echo $val['created_date'];?></td>
<td style="text-align:center;"><?php echo $val['quantity']-$val['return_quantity']-$val['sold_quantity'];?></td>


</tr>
<?php } ?>
<?php }?>
<tr><td colspan="3" style="text-align:right"><b>Total Quantity:</b></td><td style="text-align:center"><?php echo $total_remain_quantity;?></td></tr>
<?php }else{ ?>
<tr><td colspan="10" style="text-align:center">No Data Found</td></tr>
<?php } ?>




</table>


</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>