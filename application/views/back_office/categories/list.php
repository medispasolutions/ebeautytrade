<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
function delete_row(id){
window.location="<?= site_url("back_office/categories/delete"); ?>/"+id;
return false;
}

$(function(){
$("#table-1").tableDnD({
onDrop: function(table, row) {
var ser=$.tableDnD.serialize();
$("#result").load("<?= site_url("back_office/categories/sorted"); ?>"+"?"+ser);
}
});

$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="1";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<?php if(!empty($parent_levels)) {?>
<li><a href="<?php echo site_url("back_office/categories"); ?>?section=<?php echo $section;?>"><?= $title; ?></a></li>
<span class="divider">/</span>
<?php 
$cc =count($parent_levels);
$i = 0;
foreach($parent_levels as $lvl) {$i++;?>
<?php if($i != $cc) {?>
<li><a href="<?php echo site_url("back_office/categories").'?section='.$lvl['section'].'&category='.$lvl['id_categories']; ?>&section=<?php echo $lvl['section'];?>"><?= $lvl['title']; ?></a></li>
<span class="divider">/</span>
<?php } else {?>
<li><?= $lvl['title']; ?></li>
<?php }?>

<?php }?>
<?php } elseif(!empty($current_category)) {?>
<li><a href="<?php echo site_url("back_office/categories"); ?>?section=<?php echo $lvl['section'];?>"><?= $title; ?></a></li>
<span class="divider">/</span>
<li class="active"><?= $current_category['title']; ?></li>
<?php } else {?>
<li class="active"><?= $title; ?></li>
<?php }?>

<? if ($this->acl->has_permission('categories','add')){ ?>
<li class="pull-right">
<a href="<?= site_url('back_office/categories/add'); ?>?section=<?php echo $section;?>" id="topLink" class="btn btn-info top_btn" title="">Add categories</a>
</li><? } ?>
</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/categories/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="6" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<td width="2%">&nbsp;</td>
<th>CATEGORY</th>
<th>TITLE</th>
<th>SUB LEVELS</th>
<th style="text-align:center;" width="250">ACTION</th></tr>
<tr>
<td></td>
<td><input type="text" name="search" class="search_box" id="category_search" /></td>
<td><input type="text" name="search" class="search_box" id="title_search" /></td>
<td></td>
<td></td>
</tr>
</thead><tfoot><tr>
<td class="col-chk"><input type="checkbox" id="checkAllAuto" /></td>
<td colspan="6">
<? if ($this->acl->has_permission('categories','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>
<tbody>
<? 
if(count($info) > 0){

$i=0;
foreach($info as $val){

$i++; 
?>
<tr id="<?=$val["id_categories"]; ?>">
<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_categories"] ; ?>" /></td>
<td class="category_search">
<? 
/*if($val["id_parent"] != 0){
$cond=array("id_categories" => $val["id_parent"]);
echo $this->fct->getonecell("categories","title",$cond); } else { echo "<b>Parent Level</b>"; }   */
if(!empty($val['parent_level'])) {?>
<?php 
$cc = count($val['parent_level']);
$i = 0;
foreach($val['parent_level'] as $lvl) {$i++;?>
<a href="<?php echo site_url("back_office/categories/edit/".$lvl['id_categories']); ?>"><?php echo $lvl['title']; ?></a>
<?php if($i != $cc) echo '->'; ?>
<?php }?>
<?php } else {?>
<b>Parent Level</b>
<?php }?>
</td>
<td class="title_search">
<? echo $val["title".$lang]; ?>
</td>
<td>
<?php if(!empty($val['sub_levels'])) {?>
<a href="<?php echo site_url("back_office/categories").'?section='.$val['section'].'&category='.$val['id_categories']; ?>">view sub</a>
<?php }?>
</td>
<td style="text-align:center"><a href="<?= site_url("back_office/control/manage_gallery/categories/".$val["id_categories"]); ?>?section=<?php echo $this->input->get('section');?>" title="Add Photos">
<i class="icon-film"></i> Manage Gallery</a>&nbsp;<? if ($this->acl->has_permission('categories','index')){ ?>

<? } ?>
<? if ($this->acl->has_permission('categories','edit')){ ?>
<a href="<?= site_url('back_office/categories/edit/'.$val["id_categories"]);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Edit</a> <span class="hidden"> | </span>
<? } ?>
<? if ($this->acl->has_permission('categories','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_categories"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<!--<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/categories"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>-->
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 