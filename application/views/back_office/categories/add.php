<link rel="stylesheet" href="<?php echo base_url(); ?>js/colorpicker/css/colorpicker.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 //$('.colorpicker').each(function(){
  //alert("aaaaa");
  //var id = this.id;
  //$("#"+id).spectrum({ preferredFormat: "hex",showInput:true });
  $('.fillColor').ColorPicker({
   onSubmit: function(hsb, hex, rgb, el) {
    $(el).val(hex);
    $(el).ColorPickerHide();
   },
   onBeforeShow: function () {
    $(this).ColorPickerSetColor(this.value);
   }
  })
  .bind('keyup', function(){
   $(this).ColorPickerSetColor(this.value);
  });
 //});
 //$(".colorpicker").spectrum({ preferredFormat: "hex",showInput:true });
});</script>
<script type="text/javascript">
$(document).ready(function(){
	$('#set_color').change(function(){
		 if($('#set_color').is(":visible") && $(this).prop('checked')==false){
			$('#color-category').stop(true,true).slideUp('fast');
			 }else{
			$('#color-category').stop(true,true).slideDown('fast'); }
    	});	});</script>
<script type="text/javascript">
////////////////////////////////////////////////
function delete_row_1(elem)
{
	if(confirm("Are your sure?")) {
		var url = '<?php echo site_url("back_office/categories_listing/delete_item"); ?>';
		$.post(url,{id : elem},function(data){
			ParseData = JSON.parse(data);
			if(ParseData.result == 1) {
				$("#record-"+elem).remove();
			}
			else {
				alert("Error in deleting the record, please contact the administrator...");
			}
		});
	}
}
function addMoreListing()
{
	 var cc = $('.count-loc-tr').length;
	 if(cc%2 == 0) var cl = 'even';
	 else  var cl = 'odd';
	 var new_cc = cc + 1;
	 var url = '<?php echo site_url("back_office/categories_listing/addMore"); ?>';
	 $.post(url,{new_cc : new_cc, cl : cl},function(data){
		$("#table-1").append(data);
	 });
}
function remove_row(elem) {
	$("#row-"+elem).remove();
}
//////////////////////////////////////////////////////////////////
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/categories?section='.$section,'<b>List categories</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
			if(isset($id)) {
			$ul['gallery'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/control/manage_gallery/categories/'.$id).'?section='.$section.'" class="btn btn-info top_btn">Manage Gallery</a>');}
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/content/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/categories/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
}else{
$info["category"] = "";
$info["section"] = $section;
$info["id_group_categories"] = "";
$info["status"] = "";
$info["image"] = "";
$info["set_color"] = "";
$info["bold"] = "";
$info["position"] = "";
$info["color"] = "84ddfd";
$info["enable_sort_by_brand"] = 0;
$info["description".$lang] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}



//CATEGORY SECTION.
echo form_label('<b>CATEGORY</b>', 'CATEGORY');
$items = $this->custom_fct->getCategoriesPages(0,array('section'=>$section),array(),12000,0);

//print '<pre>'; print_r($items); exit; 
$categories = $items;
echo '<select name="category'.'"  class="span">';
echo '<option value="" > - set as parent - </option>';
$s_id = '';
if(isset($id)) {
$s_id = $info['id_parent'];
}
echo displayLevels($items,0,$s_id);
echo "</select>";
echo form_error("category","<span class='text-error'>","</span>");
echo br();

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");

//IMAGE SECTION.
echo form_label('<b>IMAGE</b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_categories"].'">'.anchor('uploads/categories/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("categories","image","<?php echo $info["image"]; ?>",<?php echo $info["id_categories"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
echo br();
//DESCRIPTION SECTION.
echo form_label('<b>DESCRIPTION</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"ckeditor","id" => "description11".$lang, "rows" => 15, "cols" =>100 ));
echo form_error("description".$lang,"<span class='text-error'>","</span>");
echo br();
?>

<div style="width:100%;display:inline-block;">
<div class="span6">
<?php
echo form_label('<b>Group</b> ', 'Group');

$cond=array();


$groups = $this->fct->getAll_cond('group_categories','title asc',$cond);
$options = array();
$options[""] = '--Select Group--';
foreach($groups as $group) {
	$options[$group['id_group_categories']] = $group['title'];
}

echo form_dropdown('id_group_categories',$options,set_value('id_group_categories',$info['id_group_categories'])); 
echo form_error("id_group_categories","<span class='text-error'>","</span>");
echo br();

?>
</div></div>
<?php if(isset($info['section']) && !empty($info['section'])){?>
<input type="hidden" name="section" value="<?php echo $info['section'];?>" />
<?php  } ?>


<div style="width:100%;display:inline-block;">
<div class="span6">
<?php
echo form_label('<b>Position</b> ', 'Position');

$cond=array();
$options[""] = '-Select Position-';
$options = getCategoriesPositions();

echo form_dropdown('position',$options,set_value('position',$info['position'])); 
echo form_error("position","<span class='text-error'>","</span>");
echo br();

?>
</div></div>

<?php

if(isset($info['activate_view_all']) && !isset($activate_view_all))
$activate_view_all = $info['activate_view_all'];
?>

<label><input type="checkbox" name="activate_view_all" <?php if(isset($activate_view_all) && $activate_view_all == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Activate view all</b></label>


<?php

if(isset($info['bold']) && !isset($bold))
$bold = $info['bold'];
?>

<label><input type="checkbox" name="bold" <?php if(isset($bold) && $bold == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set Bold</b></label>



<?php

if(isset($info['set_color']) && !isset($set_color))
$set_color = $info['set_color'];
?>

<label><input type="checkbox" name="set_color" id="set_color" <?php if(isset($set_color) && $set_color == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set Color</b></label>
<div class="row-fluid color-category" id="color-category" <?php if($info['set_color']!=1){ echo "style='display:none'";}?>>
<div class="span6">
<?php 
//TITLE SECTION.
echo form_input(array("name" => "color", "value" => set_value("color",$info["color".$lang]),"class" =>"span fillColor" ));
echo form_error("color","<span class='text-error'>","</span>");
?></div>
</div>


<?php

if(isset($info['enable_sort_by_brand']) && !isset($enable_sort_by_brand))
$enable_sort_by_brand = $info['enable_sort_by_brand'];
?>

<label><input type="checkbox" name="enable_sort_by_brand" <?php if(isset($enable_sort_by_brand) && $enable_sort_by_brand == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Enable sort by brand</b></label>

<?php
echo br();
echo form_label('<b>STATUS</b>', 'STATUS');
$options = array();
$options[''] = '-select status-';
$options[0] = 'Published';

$options[1] = 'Unpublished';

?>
<label>
<?php
echo form_dropdown('status',$options,set_value('status',$info['status'])); 
echo form_error("status","<span class='text-error'>","</span>");
?>
</label>

<?php
echo br();
echo form_fieldset("Meta Tags");
//PAGE TITLE SECTION.
echo form_label('<b>Page Title:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();


echo form_fieldset_close();

?>

<?php


if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}

echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>