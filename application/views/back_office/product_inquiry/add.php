<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/product_inquiry','<b>List product inquiry</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/product_inquiry/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/product_inquiry/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["options"] = "";
$info["options_en"] = "";
$info["quantity"] = "";
$info["id_products"] = "";
$info["id_user"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

      $product=$this->fct->getonerecord('products',array('id_products'=>$info['id_products']));
	  $user=$this->fct->getonerecord('user',array('id_user'=>$info['id_user']));

if(!empty($user)) {?>
<label class="field-title"><h6>User :</h6></label><label>
<div class="bg_emails" >
<?= $user["name"]; ?></div>
</label>
<?php }?>
<?php
if(!empty($product)) {?>
<label class="field-title"><h6>Product :</h6></label><label>
<div class="bg_emails" >
<?php $options=unSerializeStock($info['options_en']); ?>
<?php echo $product['title'];?> <br /> <?php if(!empty($options)) {
		$j=0;
		$c = count($options);
		foreach($options as $opt) {$j++;
			if($j==1){$com="";}else{$com=", ";}?>
                          <?php echo $com.$opt; ?>
                          <?php }}?></div>
</label>
<?php }?>


<?php if(!empty($info["quantity"])) {?>
<label class="field-title"><h6>Quantity Request :</h6></label><label>
<div class="bg_emails" >
<?= $info["quantity"]; ?></div>
</label>
<?php }?>


<?php
echo form_fieldset_close();

echo br();


echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>