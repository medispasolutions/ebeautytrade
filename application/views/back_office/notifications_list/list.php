<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
function delete_row(id){
window.location="<?= site_url("back_office/notifications_list/delete"); ?>/"+id;
return false;
}


</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li class="active">Notifications</li>
<? if ($this->acl->has_permission('notifications_list','add')){ ?>
<? } ?>
</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/notifications_list/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="3" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<td width="2%">&nbsp;</td>
<th>
<a href="<?= site_url('back_office/notifications_list/index/title'); ?>" class="<? if($sego == "title") echo 'green'; else echo 'order_link'; ?>" >
TITLE
</a>
</th><th style="text-align:center;" width="250">ACTION</th></tr>

</thead>
<tbody>
<? 
if(count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>

 <?php if($val['type']== "stock") {
	 $stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$val['id'])); ?>
<tr >
<td class="col-chk"></td>
<td class="title_search">
<?php 
$product_name = $val['title'] ;
if(!empty($stock['combination'])) {
		$options = $stock['combination'];
		$options = unSerializeStock($options);
		
		$c = count($options);

		foreach($options as $key => $opt) {
	
			$product_name .= ' - '.$opt;
			
		}
	}?>
<?php echo $product_name;?>  
</u></i> reached the threshold.
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('notifications_list','edit')){ ?>
<a href="<?php echo site_url('back_office/products/stock/'.$stock['id_products']);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Manage</a>
<? } ?>

</td>
</tr>
<?php } ?>

 <?php if($val['type']== "product") {
 ?>
<tr >
<td class="col-chk"></td>
<td class="title_search">
<?php echo $val['title'];?>
</u></i> reached the threshold.
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('notifications_list','edit')){ ?>
<a href="<?php echo site_url('back_office/products/edit/'.$val['id']);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Manage</a>
<? } ?>

</td>
</tr>
<?php } ?>

<?php if($val['type']== "delivery_order") { ?>
<?php if($val['title']==5){
	$order=$this->fct->getonerecord('delivery_order',array('id_delivery_order'=>$val['id'])); ?>
<tr>
<td class="col-chk"></td>
<td class="title_search">
<i class="yellow1"><u>D-ORDER#<?php echo $val['id'];?></u></i> has been added and needs to be Activated.
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('notifications_list','edit')){ ?>
<a href="<?php echo site_url('back_office/inventory/view_delivery_order/'.$val['id'].'/'.$order['rand']);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Manage</a> 
<? } ?>

</td>
</tr>
<?php }else{
	$order=$this->fct->getonerecord('delivery_order',array('id_delivery_order'=>$val['id']));  ?>
<tr>
<td class="col-chk"></td>
<td class="title_search">
<i class="yellow1"><u>D-RQ#<?php echo $val['id'];?></u></i> has been added and needs to be Activated.
</td>
<td style="text-align:center">
<? if ($this->acl->has_permission('notifications_list','edit')){ ?>
<a href="<?php echo site_url('back_office/inventory/view_delivery_request/'.$val['id'].'/'.$order['rand']);?>" class="table-edit-link" title="Edit" >
<i class="icon-edit" ></i> Manage </a> 
<? } ?>

</td>
</tr>
<?php } ?>
<?php } ?>


<? }  } else { ?>
<tr class='odd'><td colspan="3" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>

</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 