<?
if(!$this->session->userdata('user_id'))
redirect(site_url('back_office/home'));

?>

<? echo doctype(); ?>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $title; ?></title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>front/img/favicon.ico" />
<?php if($this->router->method == 'add_photos' || ($this->router->class == 'group_products' && $this->router->method == 'view')) {?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<?php } else {?>

<script src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script>
<script src="<?= base_url(); ?>js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="<?php echo base_url(); ?>js/custom/jquery.dow.form.validate.js"></script>
<script src="<?php echo base_url(); ?>js/custom/forms_calls.js"></script>

<?php }?>

<? $this->load->view('back_office/includes/inc_header'); ?>
</head>
<body>

<input type="hidden" id="siteurl" value="<?php echo site_url(); ?>" />
<input type="hidden" id="baseurl" value="<?php echo base_url(); ?>" />
<div id="container" class="hundred"   >
<div class="hidden" style="display:none;"><!-- the modal box container - this div is hidden until it is called from a modal box trigger. see cleanity.js for details -->
<div id="sample-modal" style="height:300px;">
<h2 style="font-size:160%; font-weight:bold; margin:10px 0;">Modal Box Content</h2>
<p>Place your desired modal box content here</p></div>
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

</div>
<? $this->load->view('back_office/includes/header'); ?>
<? $this->load->view($content); ?>
</div>
<div class="distance" ></div>
<? $this->load->view('back_office/includes/footer'); ?>
</body>
</html>
