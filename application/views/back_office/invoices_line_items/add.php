<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/invoices_line_items/'.$this->session->userdata("back_link"),'<b>List invoices line items</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/invoices_line_items/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/invoices_line_items/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["id_orders"] = "";
$info["qty_sold_by_admin"] = "";
$info["qty_payable"] = "";
$info["id_line_items"] = "";
$info["rand"] = "";
$info["id_products"] = "";
$info["sku"] = "";
$info["orders_created_date"] = "";
$info["options"] = "";
$info["price"] = "";
$info["quantity"] = "";
$info["return_quantity"] = "";
$info["credit_cart"] = "";
$info["total_price"] = "";
$info["id_invoices"] = "";
$info["id_brands"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();

//ID ORDERS SECTION.
echo form_label('<b>ID ORDERS</b>', 'ID ORDERS');
echo form_input(array('name' => 'id_orders', 'value' => set_value("id_orders",$info["id_orders"]),'class' =>'span' ));
echo form_error("id_orders","<span class='text-error'>","</span>");
echo br();
//QTY SOLD BY ADMIN SECTION.
echo form_label('<b>QTY SOLD BY ADMIN</b>', 'QTY SOLD BY ADMIN');
echo form_input(array('name' => 'qty_sold_by_admin', 'value' => set_value("qty_sold_by_admin",$info["qty_sold_by_admin"]),'class' =>'span' ));
echo form_error("qty_sold_by_admin","<span class='text-error'>","</span>");
echo br();
//QTY PAYABLE SECTION.
echo form_label('<b>QTY PAYABLE</b>', 'QTY PAYABLE');
echo form_input(array('name' => 'qty_payable', 'value' => set_value("qty_payable",$info["qty_payable"]),'class' =>'span' ));
echo form_error("qty_payable","<span class='text-error'>","</span>");
echo br();
//ID LINE ITEMS SECTION.
echo form_label('<b>ID LINE ITEMS</b>', 'ID LINE ITEMS');
echo form_input(array('name' => 'id_line_items', 'value' => set_value("id_line_items",$info["id_line_items"]),'class' =>'span' ));
echo form_error("id_line_items","<span class='text-error'>","</span>");
echo br();
//RAND SECTION.
echo form_label('<b>RAND</b>', 'RAND');
echo form_input(array('name' => 'rand', 'value' => set_value("rand",$info["rand"]),'class' =>'span' ));
echo form_error("rand","<span class='text-error'>","</span>");
echo br();
//ID PRODUCTS SECTION.
echo form_label('<b>ID PRODUCTS</b>', 'ID PRODUCTS');
echo form_input(array('name' => 'id_products', 'value' => set_value("id_products",$info["id_products"]),'class' =>'span' ));
echo form_error("id_products","<span class='text-error'>","</span>");
echo br();
//SKU SECTION.
echo form_label('<b>SKU</b>', 'SKU');
echo form_input(array('name' => 'sku', 'value' => set_value("sku",$info["sku"]),'class' =>'span' ));
echo form_error("sku","<span class='text-error'>","</span>");
echo br();
//ORDERS CREATED DATE SECTION.
echo form_label('<b>ORDERS CREATED DATE</b>', 'ORDERS CREATED DATE');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'orders_created_date', 'value' => set_value("orders_created_date",$this->fct->date_out_formate($info["orders_created_date"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("orders_created_date","<span class='text-error'>","</span>");
echo br();
//OPTIONS SECTION.
echo form_label('<b>OPTIONS</b>', 'OPTIONS');
echo form_input(array('name' => 'options', 'value' => set_value("options",$info["options"]),'class' =>'span' ));
echo form_error("options","<span class='text-error'>","</span>");
echo br();
//PRICE SECTION.
echo form_label('<b>PRICE</b>', 'PRICE');
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span' ));
echo form_error("price","<span class='text-error'>","</span>");
echo br();
//QUANTITY SECTION.
echo form_label('<b>QUANTITY</b>', 'QUANTITY');
echo form_input(array('name' => 'quantity', 'value' => set_value("quantity",$info["quantity"]),'class' =>'span' ));
echo form_error("quantity","<span class='text-error'>","</span>");
echo br();
//RETURN QUANTITY SECTION.
echo form_label('<b>RETURN QUANTITY</b>', 'RETURN QUANTITY');
echo form_input(array('name' => 'return_quantity', 'value' => set_value("return_quantity",$info["return_quantity"]),'class' =>'span' ));
echo form_error("return_quantity","<span class='text-error'>","</span>");
echo br();
//CREDIT CART SECTION.
echo form_label('<b>CREDIT CART</b>', 'CREDIT CART');
echo form_input(array('name' => 'credit_cart', 'value' => set_value("credit_cart",$info["credit_cart"]),'class' =>'span' ));
echo form_error("credit_cart","<span class='text-error'>","</span>");
echo br();
//TOTAL PRICE SECTION.
echo form_label('<b>TOTAL PRICE</b>', 'TOTAL PRICE');
echo form_input(array('name' => 'total_price', 'value' => set_value("total_price",$info["total_price"]),'class' =>'span' ));
echo form_error("total_price","<span class='text-error'>","</span>");
echo br();
//ID INVOICES SECTION.
echo form_label('<b>ID INVOICES</b>', 'ID INVOICES');
echo form_input(array('name' => 'id_invoices', 'value' => set_value("id_invoices",$info["id_invoices"]),'class' =>'span' ));
echo form_error("id_invoices","<span class='text-error'>","</span>");
echo br();
//ID BRANDS SECTION.
echo form_label('<b>ID BRANDS</b>', 'ID BRANDS');
echo form_input(array('name' => 'id_brands', 'value' => set_value("id_brands",$info["id_brands"]),'class' =>'span' ));
echo form_error("id_brands","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>