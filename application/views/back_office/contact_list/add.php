<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/contact_list/'.$this->session->userdata("back_link"),'<b>List contact list</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/contact_list/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/contact_list/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["phone"] = "";
$info["email"] = "";
$info["website"] = "";
$info["image"] = "";
$info["set_as_featured"] = "";
$info["guide"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();

//GUIDE SECTION.
echo form_label('<b>Directory</b>', 'Directory');
$items = $this->fct->getAll("guide","sort_order"); 
echo '<select name="guide'.'"  class="span">';
echo '<option value="" > - select guide - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_guide"]; ?>" <? if(isset($id)){  if($info["id_guide"] == $valll["id_guide"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("guide","<span class='text-error'>","</span>");
echo br();

//PHONE SECTION.
echo form_label('<b>PHONE</b>', 'PHONE');
echo form_input(array('name' => 'phone', 'value' => set_value("phone",$info["phone"]),'class' =>'span' ));
echo form_error("phone","<span class='text-error'>","</span>");
echo br();
//EMAIL SECTION.
echo form_label('<b>EMAIL</b>', 'EMAIL');
echo form_input(array('name' => 'email', 'value' => set_value("email",$info["email"]),'class' =>'span' ));
echo form_error("email","<span class='text-error'>","</span>");
echo br();
//WEBSITE SECTION.
echo form_label('<b>WEBSITE</b>', 'WEBSITE');
echo form_input(array('name' => 'website', 'value' => set_value("website",$info["website"]),'class' =>'span' ));
echo form_error("website","<span class='text-error'>","</span>");
echo br();
//IMAGE SECTION.
echo form_label('<b>IMAGE (Width:680px,Height:270px)</b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_contact_list"].'">'.anchor('uploads/contact_list/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("contact_list","image","<?php echo $info["image"]; ?>",<?php echo $info["id_contact_list"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
//SET AS FEATURED SECTION.
echo form_label('<b>SET AS FEATURED</b>', 'SET AS FEATURED');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("set_as_featured", $options,set_value("set_as_featured",$info["set_as_featured"]), 'class="span"');
echo form_error("set_as_featured","<span class='text-error'>","</span>");
echo br();



echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>