<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/currency_rates/'.$this->session->userdata("back_link"),'<b>List currency rates</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/currency_rates/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/currency_rates/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["currency_from"] = "";
$info["currency_to"] = "";
$info["rate"] = "";
$info["flag"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//PAGE TITLE SECTION.
/*echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();

//CURRENCY FROM&nbsp;<em>*</em>: SECTION.
echo form_label('<b>CURRENCY FROM&nbsp;<em>*</em>:</b>', 'CURRENCY FROM&nbsp;<em>*</em>:');
echo form_input(array('name' => 'currency_from', 'value' => set_value("currency_from",$info["currency_from"]),'class' =>'span' ));
echo form_error("currency_from","<span class='text-error'>","</span>");
echo br();*/
//CURRENCY TO&nbsp;<em>*</em>: SECTION.
echo form_hidden("currency_from",$this->config->item("default_currency"));
echo form_label('<b>CURRENCY CODE&nbsp;<em>*</em>:</b>', 'CURRENCY CODE&nbsp;<em>*</em>:');
echo form_input(array('name' => 'currency_to', 'value' => set_value("currency_to",$info["currency_to"]),'class' =>'span' ));
echo form_error("currency_to","<span class='text-error'>","</span>");
echo br();
//RATE&nbsp;<em>*</em>: SECTION.
echo form_label('<b>RATE&nbsp;<em>*</em>:</b> ('.$this->config->item("default_currency").' to this currency)', 'RATE&nbsp;<em>*</em>:');
echo form_input(array('name' => 'rate', 'value' => set_value("rate",$info["rate"]),'class' =>'span' ));
echo form_error("rate","<span class='text-error'>","</span>");
echo br();
//FLAG SECTION.
echo form_label('<b>FLAG</b>', 'FLAG');

	echo form_label("Width:16px; Height:11px;","help_text",array("class"=>"yellow"));
	echo form_upload(array("name" => "flag", "class" => "input-large"));
echo "<span >";
if($info["flag"] != ""){ 
echo '<span id="flag_'.$info["id_currency_rates"].'">'.anchor('uploads/currency_rates/'.$info["flag"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("currency_rates","flag","<?php echo $info["flag"]; ?>",<?php echo $info["id_currency_rates"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("flag","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>