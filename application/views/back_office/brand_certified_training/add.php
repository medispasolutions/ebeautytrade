<script type="text/javascript" src="<?= base_url(); ?>css/autocomplete.css"></script>
<script type="text/javascript" src="<?= base_url(); ?>js/autocomplete.js"></script>
<script type="text/javascript">

function SelectedCategory(id,title)
{
	var url="<?php echo site_url('back_office/training_categories/edit/')?>/"+id ;
if($('#selelect_btn_'+id).length>0){

$('#selelect_btn_'+id).remove();
$('#multi_select_categories option[value="'+id+'"]').attr('selected',false);

}else{
	
var btn_select="<div class='btn btn_select' id='selelect_btn_"+id+"'><a href='"+url+"' >"+title+"</a><span class='notification red remove_selected' onclick='SelectedCategory("+id+")'>x</span></div>";	
$('.select_categories').append(btn_select);

	}
}
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(anchor('back_office/brand_certified_training','<b>List Brand Certified Training</b>').'<span class="divider">/</span>',
            
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/supplier_info/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/brand_certified_training/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["image"] = "";
$info["description"] = "";
$info["id_products"] = "";
$info["duration"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>



<?php 
//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ,"onKeyDown"=>"limitText(this.form.title,0,".getLimitText('title').");","onKeyUp"=>"limitText(this.form.title,0,".getLimitText('title').");"));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();

?>
<label><b>Product Name</b></label>

<input type="text" name="product" value="<?php echo $this->fct->getonecell('products','title',array('id_products'=>$info['id_products']));?>" class="searchProducts span" id="searchProducts2" />
<input type="hidden" name="id_products" value="<?php echo $info['id_products'];?>" />
<div id="loader_1"></div>

<div class="row-fluid">
<div class="span6">
<? 
echo form_label('<b>BRAND CERTIFIED TRAINING CATEGORIES</b>', 'Brand Certified Training');
$training = $this->fct->getAll('training_categories','sort_order');	

$selected_training = array();
if(isset($info["id_brand_certified_training"])) {
$selected_training = $this->brand_certified_training_m->select_product_training($info["id_brand_certified_training"]);
}
if(isset($val_training)) {
 $selected_training = $val_training;

}
?>
<select multiple="multiple" id="multi_select_categories" style="height:200px" name="training[]" class="input-xxlarge">

<? 
foreach($training as $valll){ 
$cl = '';

if(in_array($valll['id_training_categories'],$selected_training)) $cl = 'selected="selected"';
?>
<?php /*?><?php echo $this->custom_fct->printsubcategories($valll,$selected_categories,0); ?><?php */?>
<option  value="<?php echo $valll['id_training_categories'] ;?>" <?php echo $cl  ;?>><?php echo $valll['title'];?></option>
<? 
} ?>
</select>
<input type="hidden" name="validate_training_categories" value="1" /></label>
</div>

<div class="span6">

<?php echo br(); ?>
<div class="select_categories">
<?php if(isset($selected_training) && !empty($selected_training)) { ?>

<?php foreach($selected_training as $cat_id){
	$category_selected=$this->fct->getonerecord('training_categories',array('id_training_categories'=>$cat_id));
	echo "<div class='btn btn_select' id='selelect_btn_".$cat_id."'><a href=".site_url('back_office/training_categories/edit/'.$cat_id).">".$category_selected['title']."</a><span class='notification red remove_selected'
	 onclick='SelectedCategory(".$cat_id.")' >x</span></div>";
	?>
	<?php } ?>
	<?php } ?>
   
</div>
</div>
</div>
<?php

echo form_label('<b>Duration&nbsp;<em>*</em>:</b>', 'Duration');
echo form_input(array("name" => "duration".$lang, "value" => set_value("duration".$lang,$info["duration".$lang]),"class" =>"span" ,"onKeyDown"=>"limitText(this.form.duration,0,".getLimitText('title').");","onKeyUp"=>"limitText(this.form.duration,0,".getLimitText('title').");"));
echo form_error("duration".$lang,"<span class='text-error'>","</span>");
echo br();



$size_arr=getMaxSize('image');
//IMAGE SECTION.
echo "<label>";
echo form_label('<b>IMAGE <small class="blue">(Max Size '.$size_arr['size_mega'].'Mb)</small></b>', 'IMAGE');
echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($info["image"] != ""){ 
echo '<span id="image_'.$info["id_brand_certified_training"].'">'.anchor('uploads/brand_certified_training/'.$info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("brand_certified_training","image","<?php echo $info["image"]; ?>",<?php echo $info["id_brand_certified_training"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo "</label>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
//DESCRIPTION SECTION.
echo form_label('<b>DESCRIPTION ', 'DESCRIPTION');
echo form_textarea(array("name" => "description", "value" => set_value("description",$info["description"]),"class" =>"ckeditor","id" => "description", "rows" => 15, "cols" =>100 ));
echo form_error("description","<span class='text-error'>","</span>");
echo br();
?>
<label>

<?php


if(isset($info['set_as_training']))
$set_as_training = $info['set_as_training'];
?>
<label style="display:none;"><input type="checkbox" name="set_as_training" <?php if(isset($set_as_training) && $set_as_training == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as training</b></label>

<?php


if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>