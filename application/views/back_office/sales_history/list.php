<script>
function delete_row(id){
window.location="<?= base_url(); ?>back_office/sales_history/delete/"+id;
return false;
}
$(document).ready(function(){
$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">


<li><a href="<?php echo site_url('back_office/inventory');?>"><b>List Inventory</b></a></li>
<li class="divider">/</li>
<li class="active"><?= $title; ?></li>
</ul> 
</div>
<div class="span10-fluid" style="width:100%;margin:0 10px 0 0; float:left">

<div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form method="get" action="<?=base_url()?>back_office/sales_history">
<!--FIRST NAME -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">First Name<br /><input type="text" id="first_name" name="first_name" value="<? if(isset($_GET['first_name'])) { echo $_GET['first_name']; }?>" /></div>

<!--LAST NAME -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Last Name<br /><input type="text" id="last_name" name="last_name" value="<? if(isset($_GET['last_name'])) { echo $_GET['last_name']; }?>" /></div>
<!--COUNTRY -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Country <br />    <select name="country_id" id="billing_country" value="<?php echo set_value('country_id');?>" class="select_form">

		<option value=""><?php echo lang('select'); ?></option>
        <?php foreach($countries as $country) {
				$cl = '';
				if(isset($_GET['country_id']) && $country['id_countries'] ==$_GET['country_id']) $cl = 'selected="selected"';
			?>
        	<option value="<?php echo $country['id_countries']; ?>" <?php echo $cl; ?>><?php echo $country['title']; ?></option>
        <?php }?>

                        </select></div>
<!--STATUS -->
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Status<br />
<select name="status" id="status">
<option value="">- All -</option>
<option value="pending" <?php if(isset($_GET['status']) && $_GET['status'] == 'pending') { ?>selected="selected"<?php }?>>Pending</option>
<option value="paid" <?php if(isset($_GET['status']) && $_GET['status'] == 'paid') { ?>selected="selected"<?php }?>>Paid</option>
<option value="completed" <?php if(isset($_GET['status']) && $_GET['status'] == 'completed') { ?>selected="selected"<?php }?>>Completed</option>
<option value="canceled" <?php if(isset($_GET['status']) && $_GET['status'] == 'canceled') { ?>selected="selected"<?php }?>>Canceled</option>
</select>
</div>

<br />
<div class="fl" style="margin:0 15px 0 0;width: 230px; float:left;">Payment Method<br />
<?php $payment_methods=$this->ecommerce_model->getPaymentMethods();?>
<select name="payment_method" id="payment_method" style="text-transform:capitalize;">
<option value="">- All -</option>
<?php 
foreach($payment_methods as $val){?>
<option value="<?php echo $val['payment_method'];?>" <?php if(isset($_GET['payment_method']) && $_GET['payment_method'] == $val['payment_method']) echo 'selected="selected"'; ?>> <?php echo $this->ecommerce_model->getPaymentMethod($val['payment_method']);?></option>
<?php } ?>
</select>
</div>
<!--DATE -->
<div class="fl" style="width: 360px; float:left;">
<div class="fl" style="width: 180px; float:left;">From <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['from_date'])) echo $_GET['from_date'] ;?>" name="from_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>

<div class="fl" style="width: 180px; float:left;">To <br /><div class="input-append date" data-date-format="dd/mm/yyyy" data-date="">
<input class="input-small" type="text" size="16" value="<?php if(isset($_GET['to_date'])) echo $_GET['to_date'] ;?>" name="to_date">
<span class="add-on">
<i class="icon-th"></i>
</span>
</div>
</div>
</div>



<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" name="report_submit" value="Filter" /> 
<!--<input type="submit" class="btn btn-primary" name="report_submit" value="export" />-->
<button type="submit" class="btn btn-primary  btn-danger" name="report_submit" value="export" ><i class="icon-download"></i> Export</button>
</div>

</form>
              </div>
            </div>
          </div>
        </div>
</div>


<div class="hundred pull-left" id="match2">   
<div id="result"></div> 
<? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/sales_history/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="4" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<th>ORDER ID</th>
<th>CREATED DATE</th>
<th>CUSTOMER NAME</th>
<th>E-mail</th>
<th>AMOUNT</th>
<th>STATUS</th>
</tr>
</thead><!--<tfoot><tr>
<td colspan="3">
<? if ($this->acl->has_permission('sales_history','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>-->
<tbody>
<?

if(isset($info) && !empty($info)  && count($info) > 0){
$i=0;
foreach($info as $val){

$i++; 
?>
<tr id="<?=$val["id_orders"]; ?>">
<!--<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_orders"] ; ?>" /></td>-->
<td><? echo $val["id_orders"]; ?></td>
<td><?php echo  date("F d,Y", strtotime($val['created_date'])); ?></td>
<td><? echo $val["first_name"]; ?> <? echo $val["last_name"]; ?></td>
<td><? echo $val["email"]; ?></td>
<td><? echo $val["amount_currency"] .' '.lang($val["currency"]); ?></td>
<td><? 
if($val["status"] == 'paid' || $val["status"] == 'completed') {?>
<span class="label label-success"><?php echo $val["status"]; ?></span>
<?php } else {?>
<span class="label label-important"><?php echo $val["status"]; ?></span>
<?php }?></td>

</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="6" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/sales_history"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 