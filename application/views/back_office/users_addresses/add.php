<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/users_addresses/'.$this->session->userdata("back_link"),'<b>List users addresses</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/users_addresses/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/users_addresses/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["state"] = "";
$info["postal_code"] = "";
$info["city"] = "";
$info["country"] = "";
$info["billing"] = "";
$info["shipping"] = "";
$info["branch"] = "";
$info["id_countries"] = "";
$info["pickup"] = "";
$info["address"] = "";
$info["type"] = "";
$info["as_default"] = "";
$info["street_one"] = "";
$info["street_two"] = "";
$info["place"] = "";
$info["mobile"] = "";
$info["address_name"] = "";
$info["fax"] = "";
$info["phone"] = "";
$info["floor_name"] = "";
$info["appartment_office"] = "";
$info["first_name"] = "";
$info["last_name"] = "";
$info["name"] = "";
$info["company_name"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("Contact Information");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//STATE SECTION.
?>
<div class="row-fluid">
<div class="span6">
<?php
//FIRST NAME SECTION.
echo form_label('<b>FIRST NAME</b>', 'FIRST NAME');
echo form_input(array('name' => 'first_name', 'value' => set_value("first_name",$info["first_name"]),'class' =>'span' ));
echo form_error("first_name","<span class='text-error'>","</span>");
echo br();
?>
</div><div class="span6">
<?php
//LAST NAME SECTION.
echo form_label('<b>LAST NAME</b>', 'LAST NAME');
echo form_input(array('name' => 'last_name', 'value' => set_value("last_name",$info["last_name"]),'class' =>'span' ));
echo form_error("last_name","<span class='text-error'>","</span>");
echo br();
?>

 </div>
 </div>
 
 <div class="row-fluid">
 
<div class="span6">
<?php
//COMPANY NAME SECTION.
echo form_label('<b>COMPANY NAME</b>', 'COMPANY NAME');
echo form_input(array('name' => 'company_name', 'value' => set_value("company_name",$info["company_name"]),'class' =>'span' ));
echo form_error("company_name","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span6">
<label class="field-title">FAX:</label>
<label><input type="text" class="input-xlarge" name="fax" value="<?= set_value('fax',$info["fax"]); ?>" />
<?= form_error('fax','<span class="text-error">','</span>'); ?></label>
<!--<label>
<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="fax[0]" value="<?php echo getPhoneField($info['fax'],0);?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="fax[1]" value="<?php echo getPhoneField($info['fax'],1);?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="fax[2]" value="<?php echo getPhoneField($info['fax'],2);?>" />

</label>-->
</div>
</div>

<div class="row-fluid">


<div class="span6">
<label class="field-title">PHONE<em>*</em>:</label>
<label><input type="text" class="input-xlarge" name="phone" value="<?= set_value('phone',$info["phone"]); ?>" />
<?= form_error('phone','<span class="text-error">','</span>'); ?></label>
<!--<label>
<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="phone[0]" value="<?php echo getPhoneField($info['phone'],0);?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="phone[1]" value="<?php echo getPhoneField($info['phone'],1);?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="phone[2]" value="<?php echo getPhoneField($info['phone'],2);?>" />
<?= form_error('phone','<span class="text-error">','</span>'); ?>
</label>-->
</div>

<div class="span6">
<label class="field-title">MOBILE:</label>
<label><input type="text" class="input-xlarge" name="mobile" value="<?= set_value('mobile',$info["mobile"]); ?>" />
<?= form_error('mobile','<span class="text-error">','</span>'); ?></label>
<!--<label>
<input type="text" class="span phone_1"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Zip Code" style="width:50px;" maxlength="4"  name="mobile[0]" value="<?php echo getPhoneField($info['mobile'],0);?>" />
<input type="text" class="span phone_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57'  title="Area Code" style="width:50px;"  maxlength="4" name="mobile[1]" value="<?php echo getPhoneField($info['mobile'],1);?>" />
<input type="text" class="span phone_3" onkeypress='return event.charCode >= 48 && event.charCode <= 57' title="Phone Number" style="width:150px;" maxlength="10" name="mobile[2]" value="<?php echo getPhoneField($info['mobile'],2);?>" />
<?= form_error('mobile','<span class="text-error">','</span>'); ?>
</label>-->
</div>
</div>
<?php
echo form_fieldset_close();
 echo form_fieldset("Address");?>
 <div class="row-fluid">
 <div class="span6">
<?php
//STREET ONE SECTION.
echo form_label('<b>STREET ONE</b>', 'STREET ONE');
echo form_input(array('name' => 'street_one', 'value' => set_value("street_one",$info["street_one"]),'class' =>'span' ));
echo form_error("street_one","<span class='text-error'>","</span>");
echo br();
?>
</div>

<div class="span6">
<?php
//STREET TWO SECTION.
echo form_label('<b>STREET TWO</b>', 'STREET TWO');
echo form_input(array('name' => 'street_two', 'value' => set_value("street_two",$info["street_two"]),'class' =>'span' ));
echo form_error("street_two","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
 
 <div class="row-fluid">
 <div class="span6">
<?php
//CITY SECTION.
echo form_label('<b>CITY</b>', 'CITY');
echo form_input(array('name' => 'city', 'value' => set_value("city",$info["city"]),'class' =>'span' ));
echo form_error("city","<span class='text-error'>","</span>");
echo br();
?>
</div>
 
<div class="span6">
 <?php 
echo form_label('<b>STATE</b>', 'STATE');
echo form_input(array('name' => 'state', 'value' => set_value("state",$info["state"]),'class' =>'span' ));
echo form_error("state","<span class='text-error'>","</span>");
echo br();
?>
 
</div></div>
 <div class="row-fluid">
<div class="span6">

<?php
//POSTAL CODE SECTION.
echo form_label('<b>POSTAL CODE</b>', 'POSTAL CODE');
echo form_input(array('name' => 'postal_code', 'value' => set_value("postal_code",$info["postal_code"]),'class' =>'span' ));
echo form_error("postal_code","<span class='text-error'>","</span>");
echo br();?>

</div>
<div class="span6">

<?php $countries=$this->fct->getAll('countries','title');?>
<label class="field-title"><b>Country<em>*</em></b>:</label>
<label>
<select name="id_countries" class="input-xxlarge" >
          <option value="">-Select Country-</option>
          <?php foreach($countries as $country){?>
          <option value="<?php echo $country['id_countries'];?>" <? if(set_value('id_countries',$info["id_countries"]) == $country['id_countries']) echo 'selected="selected"'; ?> ><?php echo $country['title'];?></option>
          <?php } ?>
          </select>
          <?= form_error('id_countries','<span class="text-error">','</span>'); ?>
</label>
</div>
</div> 
 

 

 <!--<div class="row-fluid">
 <div class="span6">
<?php
//PLACE SECTION.
echo form_label('<b>PLACE</b>', 'PLACE');
echo form_input(array('name' => 'place', 'value' => set_value("place",$info["place"]),'class' =>'span' ));
echo form_error("place","<span class='text-error'>","</span>");
echo br();
?>
</div></div>-->
<?php

//ADDRESS NAME SECTION.




echo form_fieldset_close();

echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}

echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>