<!DOCTYPE html>
<!--[if lte IE 9]>     <html lang="en" class="old-ie"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
        <title>Product Import | EBeautyTrade</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            @-ms-viewport {
                width: device-width;
            }
        </style>
        <meta name="robots" content="noindex,nofollow">
        <link rel="stylesheet" href="/public/suppliers-backoffice/build/css/codebase-ca6f8e0b49.css">
    </head>
    <?php
    $user = $this->serviceLocator->user($this->acl->getCurrentUserId());
    $info = $user->toArray();
    
    
    $this->db->select('title, id_brands');
    $this->db->from('brands');
    $this->db->like("id_user",$info['id_user']);
    $this->db->like("deleted", 0);
    $this->db->like("status", 1);
    $query=$this->db->get();

    ?>
    <body>
        <div class="site-wrapper">
            <div class="site-wrapper__sidebar  js-site-sidebar  ">
                <div class="site-sidebar ">
                    <div class="profile-widget">
                        <img class="profile-widget__user-avatar" src="<?php echo $info["photo"]; ?>" alt="">
                             <p class="profile-widget__username"><?php echo $info["name"]; ?></p>
                        <img class="profile-widget__logo" src="<?php echo $info["logo"]; ?>" alt="">
                    </div>
                    <nav class="menu menu--sidebar">
                        <a class="menu__item _is-active" href="/back_office/dashboard">Dashboard</a>
                        <a class="menu__item " href="/back_office/profile/edit">Profile</a>
                        <p class="menu__header">Brands</p>
                        <a class="menu__subitem " href="/back_office/brands/listing"><span class="icon icon--10 icon--cw-90"></span>List all</a>
                        <a class="menu__subitem " href="/back_office/brands/create"><span class="icon icon--10 icon--cw-90"></span>Add new</a>
                        <p class="menu__header">Products</p>
                        <a class="menu__subitem " href="/back_office/products/live_on_site"><span class="icon icon--10 icon--cw-90"></span>Live on site</a>
                        <a class="menu__subitem " href="/back_office/products/unpublished"><span class="icon icon--10 icon--cw-90"></span>Unpublished</a>
                        <a class="menu__subitem " href="/back_office/products/specials"><span class="icon icon--10 icon--cw-90"></span>Specials</a>
                        <a class="menu__subitem " href="/back_office/products/create"><span class="icon icon--10 icon--cw-90"></span>Add new</a>
                        <p class="menu__header">Orders</p>
                        <a class="menu__subitem " href="/back_office/buyers/listing"><span class="icon icon--10 icon--cw-90"></span>Buyers list</a>
                        <a class="menu__subitem " href="/back_office/orders/listing"><span class="icon icon--10 icon--cw-90"></span>Orders list</a>
                    </nav>
                    <img class="site-sidebar__logo" src="/public/suppliers-backoffice/build/img/spamiles-logo-primary-6c7bd0ab73.png" alt="Ebeautytrade">
                </div>
            </div>
            <main role="main" class="site-wrapper__main-column">
                <div class="site-header">
                    <div class="site-header__group  [ visible-xs-block visible-sm-block ]">
                        <img class="site-header__logo" src="/public/suppliers-backoffice/build/img/spamiles-logo-primary-6c7bd0ab73.png" alt="Spamiles">
                    </div>
                    <div class="site-header__group  [ hidden-xs hidden-sm ]">
                        <button class="[ site-sidebar-toggle site-sidebar-toggle--hide ]  [ btn btn--l btn--transparent btn--icon ]  js-tp  js-site-sidebar-toggle  " data-tp-addClass="tp-white" data-tp-position="center bottom" data-tp-animation="move" data-tp-maxWidth="300" data-tp-content="Hide sidebar"><span class="icon icon--slate-light icon--outdent icon--25"></span></button>
                        <button class="[ site-sidebar-toggle site-sidebar-toggle--show ]  [ btn btn--l btn--transparent btn--icon ]  js-tp  js-site-sidebar-toggle  " data-tp-addClass="tp-white" data-tp-position="center bottom" data-tp-animation="move" data-tp-maxWidth="300" data-tp-content="Show sidebar"><span class="icon icon--slate-light icon--indent icon--25"></span></button>
                        <a class="[ btn btn--l btn--slate-light btn--outline ]  [ text--uppercase text--bold ]  js-md-open" href="#" data-md-id="soonInfoBrands">Boost your brand</a>
                    </div>
                    <div class="site-header__group">
                        <nav class="header-nav  hidden-xs">
                            <a class="header-nav__item  [ btn btn--l btn--transparent btn--icon ]  js-tp" data-tp-addClass="tp-white" data-tp-position="center bottom" data-tp-animation="move" data-tp-maxWidth="300" data-tp-content="Ebeautytrade" href="http://ebeautytrade.com" target="_blank"><span class="icon icon--slate-light icon--globe icon--25"></span></a>
                            <a class="header-nav__item  [ btn btn--l btn--transparent btn--icon btn--relative btn--ov-visible ]  js-tp" data-tp-addClass="tp-white" data-tp-position="center bottom" data-tp-animation="move" data-tp-maxWidth="300" data-tp-content="Messages" href="/back_office/messages/listing"><span class="icon icon--slate-light icon--envelope icon--30"></span></a>
                        </nav>
                        <div class="site-header__dropdown  [ dropdown dropdown--l dropdown--right ]  hidden-xs">
                            <button class="dropdown__toggle">
                                <span class="dropdown__toggle-flex-container">
                                    <img
                                        class="dropdown__toggle-avatar"
                                        src="<?php echo $info["photo"]; ?>"
                                        alt=""
                                        >
                                    <span class="dropdown__toggle-text  [ text--slate text--bold text--kerning-wide ]"><?php echo $info["name"]; ?></span>
                                    <span class="dropdown__toggle-icon  [ icon icon--slate-light icon--triangle icon--cw-180 ]"></span>
                                </span>
                            </button>
                            <div class="dropdown__items">
                                <a class="dropdown__item  text--nowrap" href="/back_office/profile/edit"><span class="icon icon--slate-light icon--pencil icon--25"></span>Edit profile</a>
                                <a class="dropdown__item  text--nowrap" href="/back_office/messages/listing"><span class="badge-container"><span class="icon icon--slate-light icon--envelope icon--25"></span></span>Messages</a>
                                <a class="dropdown__item  text--nowrap" href="/back_office/home/logout"><span class="icon icon--slate-light icon--o-logout icon--25"></span>Logout</a>
                            </div>
                        </div>
                        <div class="site-header__dropdown  [ dropdown dropdown--l dropdown--right ]  [ hidden-md hidden-lg ]">
                            <button class="dropdown__toggle">
                                <span class="dropdown__toggle-flex-container">
                                    <span class="dropdown__toggle-icon  [ icon icon--slate-light icon--hamburger icon--25 ]"></span>
                                </span>
                            </button>
                            <div class="dropdown__items">
                                <a class="dropdown__item  text--nowrap  [ hidden-sm hidden-lg ]" href="/back_office/messages/listing"><span class="badge-container"><span class="icon icon--slate-light icon--envelope"></span></span>Messages</a>
                                <nav class="menu menu--dropdown">
                                    <a class="menu__item is-active" href="/back_office/dashboard">Dashboard</a>
                                    <a class="menu__item " href="/back_office/profile/edit">Profile</a>
                                    <p class="menu__header">Brands</p>
                                    <a class="menu__subitem " href="/back_office/brands/listing"><span class="icon icon--10 icon--cw-90"></span>List all</a>
                                    <a class="menu__subitem " href="/back_office/brands/create"><span class="icon icon--10 icon--cw-90"></span>Add new</a>
                                    <p class="menu__header">Products</p>
                                    <a class="menu__subitem " href="/back_office/products/live_on_site"><span class="icon icon--10 icon--cw-90"></span>Live on site</a>
                                    <a class="menu__subitem " href="/back_office/products/unpublished"><span class="icon icon--10 icon--cw-90"></span>Unpublished</a>
                                    <a class="menu__subitem " href="/back_office/products/specials"><span class="icon icon--10 icon--cw-90"></span>Specials</a>
                                    <a class="menu__subitem " href="/back_office/products/create"><span class="icon icon--10 icon--cw-90"></span>Add new</a>
                                    <p class="menu__header">Orders</p>
                                    <a class="menu__subitem " href="/back_office/buyers/listing"><span class="icon icon--10 icon--cw-90"></span>Buyers list</a>
                                    <a class="menu__subitem " href="/back_office/orders/listing"><span class="icon icon--10 icon--cw-90"></span>Orders list</a>
                                </nav>
                                <a class="dropdown__item  text--nowrap  [ hidden-sm visible-xs-block ]" href="/back_office/home/logout"><span class="icon icon--slate-light icon--o-logout icon--25"></span>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="site-wrapper__content">
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="span2">
                                <? //$this->load->view("back_office/includes/left_box"); ?>
                            </div>
                            <div class="span10" >
                                <div class="span10-fluid" >
                                    <?
                                    if(isset($breadcrumbs)) {

                                    echo $breadcrumbs;
                                    }else{ ;?>
                                    <?php } ?>
                                </div>
                                <style>
                                    @import url('//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

                                    h1{
                                        font-size: 48px;
                                        margin-top: 60px;
                                        margin-bottom: 30px;
                                    }
                                    input[type="file"] {
                                        display: none;
                                    }
                                    .custom-file-upload {
                                        display: inline-block;
                                        padding: 8px 20px;
                                        cursor: pointer;
                                        border: 1px solid #27a9e1;
                                        color: #27a9e1;
                                        /* background: #27a9e1; */
                                        border-radius: 3px;
                                        font-weight: bold;
                                    }

                                    .custom-file-upload .fa{
                                        margin-right: 5px;
                                    }

                                    #importfile-id{
                                        margin-top: 15px;
                                        font-size: 14px;
                                        font-weight: bold;
                                        min-width: 140px;
                                    }
                                </style>

                                <div class="hundred pull-left">  
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <h1> Suppliers Bulk Product <small>Import</small></h1>
                                        </div>
                                        <?php
                                        if(checkIfSupplier_admin()){
                                            $user=$this->ecommerce_model->getUserInfo();
                                            $userid=$user['id_user'];
                                            $output = '';
                                            $output .= form_open_multipart('back_office/supplier_import/save');
                                            $output .= '<div class="row">';
                                            $output .= '<div class="col-lg-12 col-sm-12"><div class="form-group">';
                                            $output .= form_label('<i class="fa fa-cloud-upload"></i> Upload File', 'userfile', array('class'=>'custom-file-upload'));
                                            $data = array(
                                                'name' => 'userfile',
                                                'id' => 'userfile',
                                                'class' => 'form-control filestyle',
                                                'value' => '',
                                                'data-icon' => 'false'
                                            );
                                            $output .= form_upload($data);
                                            $output .= '</div> <span style="color:red; font-weight: bold; display: inline-block; margin-bottom: 5px;">Please choose an Excel file(.xls or .xlxs) as Input</span> (Download Excel Template by clicking on the following link <a style="font-weight: bold;" href="https://www.ebeautytrade.com/uploads/product-import-template/Supplier-Product-Template.xlsx" target="_blank">Product Import Excel Template</a>)</div>';
                                            $output .= '<div class="col-lg-12 col-sm-12"><div class="form-group text-right">';
                                            $data = array(
                                                'name' => 'importfile',
                                                'id' => 'importfile-id',
                                                'class' => 'btn btn--primary btn--m',
                                                'value' => 'Import',
                                            );
                                            $output .= form_submit($data, 'Import Data');
                                            $output .= '</div>
                                                </div></div>';
                                            $output .= form_close();
                                            echo $output;
                                        }else{
                                            echo 'You must be logged in as Supplier in order to view this page';
                                        }
                                        ?>
                                         <div>
                                            <h3 style="margin: 0 0 10px;">For bulk import, kinldy use the brand ID below</h3>
                                            <table style="background: transparent;margin: 0 0 20px;">
                                                <tr style='border: 1px solid #000'>
                                                    <th style='padding: 10px'>Brand name</th>
                                                    <th style='padding: 10px'>Brand Id</th>
                                                </tr>
                                            <?php 
                                                foreach ($query->result_array() as $row)
                                                {  
                                                	echo "<tr style='border: 1px solid #000'><td style='padding: 10px'>".$row['title']."</td><td style='padding: 10px'>".$row['id_brands']."</td></tr>";
                                                }
                                            ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>     
                            </div>
                        </div>
                    </div>
            </main>
        </div>
        <div class="global-overlay  js-global-overlay  "></div>
        <div class="[ modal modal--s modal--common-padding ]  js-md" data-md-id="soonInfoBrands">
            <button class="modal__closing-button  js-md-close">
                <span class="icon icon--10 icon--x icon--slate-light"></span>
            </button>
            <div class="margin-top--20">
                Very soon you will be able to schedule and feature your brands from your Dashboard.
            </div>
            <div class="text--center  margin-top--30">
                <button class="[ btn btn--m btn--secondary btn--outline ]  [ text--bold text--uppercase ]  js-md-close"><span class="icon icon--secondary icon--x icon--10"></span><span>Close</span></button>
            </div>
        </div>
        <div class="[ modal modal--s modal--common-padding ]  js-md" data-md-id="soonInfoAnalytics">
            <button class="modal__closing-button  js-md-close">
                <span class="icon icon--10 icon--x icon--slate-light"></span>
            </button>
            <div class="margin-top--20">
                Soon you will be able to connect your brand(s) for analytics preview.
            </div>
            <div class="text--center  margin-top--30">
                <button class="[ btn btn--m btn--secondary btn--outline ]  [ text--bold text--uppercase ]  js-md-close"><span class="icon icon--secondary icon--x icon--10"></span><span>Close</span></button>
            </div>
        </div>
        <!--[if lte IE 9]>
        <p class="feature-warning">You are using an <strong>outdated</strong> browser. This website may not work properly. <a href="http://browsehappy.com/" rel="nofollow" target="_blank">Update your browser now</a></p>
        <![endif]-->
        <noscript>
        <p class="feature-warning"><strong>This website requires JavaScript to work properly. Please turn JavaScript on in your browser.</p>
        </noscript>

        <script>
            (function() {
                try {
                    window.BIACONFIG = {"app":{"ENVIRONMENT":"production", "environments":{"DEVELOPMENT":"development", "PRODUCTION":"production"}, "LANGUAGE":"en", "CKEDITOR_PATH":"\/public\/suppliers-backoffice\/build\/ckeditor", "breakpoints":{"BP_XS_MIN":320, "BP_SM_MIN":768, "BP_MD_MIN":1024, "BP_LG_MIN":1200, "BP_XL_MIN":1366}}, "context":{"id":"dashboard", "contexts":{"TEST":"test", "DASHBOARD":"dashboard", "BRANDS_LISTING":"brandsListing", "PRODUCTS_LISTING_LIVE_ON_SITE":"productsListingLiveOnSite", "PRODUCTS_LISTING_UNPUBLISHED":"productsListingUnpublished", "PRODUCTS_LISTING_SPECIALS":"productsListingSpecials", "BUYERS_LISTING":"buyersListing", "ORDERS_LISTING":"ordersListing", "MESSAGES_LISTING":"messagesListing", "PROFILE_MANAGE":"profileManage", "BRAND_ADD":"brandAdd", "BRAND_EDIT":"brandEdit", "PRODUCT_ADD":"productAdd", "PRODUCT_EDIT":"productEdit", "ORDER_VIEW":"orderView", "LOGIN_VIEW":"loginView", "FORGOTTEN_PASSWORD_VIEW":"forgottenPasswordView", "RESET_PASSWORD_VIEW":"resetPasswordView", "WELCOME_VIEW":"welcomeView"}, "user":{"id":1873, "EMAIL":"<?php echo $info["email"]; ?>", "FIRST_NAME":"<?php echo $info["first_name"]; ?>", "LAST_NAME":"<?php echo $info["last_name"];?>", "USER_AVATAR_URL":"\/uploads\/user\/avatars\/200x200\/default-user-img.jpg", "COMPANY_LOGO_URL":"\/uploads\/user\/no_avatar.jpg", "PROFILE_COMPLETION_PERCENT":100, "UNREAD_MESSAGES":0}, "ENABLE_UNLOAD_PREVENTION":false}, "state":{"IS_USER_KNOWN":true, "IS_USER_LOGGED_IN":true, "IS_SITE_SIDEBAR_VISIBLE":true}, "build":{"img":{"examples\/gallery-thumbnail-1.png":"\/public\/suppliers-backoffice\/build\/img\/examples\/gallery-thumbnail-1-77071eaece.png", "examples\/gallery-thumbnail-2.png":"\/public\/suppliers-backoffice\/build\/img\/examples\/gallery-thumbnail-2-dff63086bb.png", "examples\/gallery-thumbnail-3.png":"\/public\/suppliers-backoffice\/build\/img\/examples\/gallery-thumbnail-3-1432f3e95f.png", "examples\/gallery-thumbnail-4.png":"\/public\/suppliers-backoffice\/build\/img\/examples\/gallery-thumbnail-4-d0fbb51c38.png", "examples\/gallery-thumbnail-5.png":"\/public\/suppliers-backoffice\/build\/img\/examples\/gallery-thumbnail-5-da9f626824.png", "gallery-guidelines\/background.jpg":"\/public\/suppliers-backoffice\/build\/img\/gallery-guidelines\/background-bd03759c11.jpg", "gallery-guidelines\/blurry.jpg":"\/public\/suppliers-backoffice\/build\/img\/gallery-guidelines\/blurry-399d87c09f.jpg", "gallery-guidelines\/size.jpg":"\/public\/suppliers-backoffice\/build\/img\/gallery-guidelines\/size-e704322808.jpg", "gallery-guidelines\/watermark.jpg":"\/public\/suppliers-backoffice\/build\/img\/gallery-guidelines\/watermark-c32f036e77.jpg", "icons\/bell-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/bell-slate-light-95bff0d5cc.svg", "icons\/calendar-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/calendar-slate-light-62c130fd2e.svg", "icons\/chat-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/chat-slate-light-ac7cb121ac.svg", "icons\/checkbox-off-danger.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/checkbox-off-danger-3dfa2a1bb7.svg", "icons\/checkbox-off.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/checkbox-off-5f18517b74.svg", "icons\/checkbox-on.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/checkbox-on-8560592854.svg", "icons\/clone-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/clone-white-7224fcd698.svg", "icons\/envelope-open-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/envelope-open-white-88f2078834.svg", "icons\/envelope-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/envelope-slate-light-e707c682f1.svg", "icons\/envelope-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/envelope-white-d41d8cd98f.svg", "icons\/exclamation-quinary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/exclamation-quinary-ac2f6fb7b6.svg", "icons\/eye-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/eye-white-6f4f670b8e.svg", "icons\/facebook-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/facebook-white-84b01c2ac9.svg", "icons\/globe-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/globe-slate-light-b4dd367c5e.svg", "icons\/googleplus-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/googleplus-white-e468e826d5.svg", "icons\/hamburger-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/hamburger-slate-light-269ce507c8.svg", "icons\/i-senary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/i-senary-d724f9bd39.svg", "icons\/indent-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/indent-slate-light-77abe8bdd6.svg", "icons\/linkedin-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/linkedin-white-4b7d6ad1e0.svg", "icons\/list-completed-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/list-completed-white-e1bb76117e.svg", "icons\/loupe-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/loupe-slate-light-bc61c7c8b7.svg", "icons\/o-filter-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/o-filter-white-76e495d2bd.svg", "icons\/o-logout-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/o-logout-slate-light-01c92c1364.svg", "icons\/o-logout-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/o-logout-white-342b1697dd.svg", "icons\/o-plus-primary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/o-plus-primary-227189b1ae.svg", "icons\/o-plus-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/o-plus-white-62ef141a49.svg", "icons\/out-i-primary-dark.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/out-i-primary-dark-3944ce75c8.svg", "icons\/out-i-secondary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/out-i-secondary-9a0d562d28.svg", "icons\/out-i-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/out-i-slate-light-8281c6e33f.svg", "icons\/out-question-mark-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/out-question-mark-slate-light-148f41c1fe.svg", "icons\/outdent-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/outdent-slate-light-5df7c5f227.svg", "icons\/pencil-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/pencil-slate-light-435a004ff1.svg", "icons\/pencil-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/pencil-white-731c3a0a02.svg", "icons\/phone-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/phone-slate-light-58f712fc7f.svg", "icons\/radio-off-danger.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/radio-off-danger-06192541dc.svg", "icons\/radio-off.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/radio-off-b9680fd49f.svg", "icons\/radio-on.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/radio-on-2db08a330b.svg", "icons\/refresh-primary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/refresh-primary-35e974307e.svg", "icons\/refresh-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/refresh-white-766ce10883.svg", "icons\/test-gray.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/test-gray-14ea6264df.svg", "icons\/test-primary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/test-primary-50427b8a32.svg", "icons\/test-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/test-white-46dafe9ca1.svg", "icons\/tick-gray-darker.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/tick-gray-darker-ae038f82c6.svg", "icons\/tick-quaternary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/tick-quaternary-b5102a7f5e.svg", "icons\/tick-tertiary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/tick-tertiary-36ad812f1e.svg", "icons\/tick-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/tick-white-57257022e7.svg", "icons\/triangle-primary-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/triangle-primary-light-1c301d3d2a.svg", "icons\/triangle-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/triangle-slate-light-3de81753ae.svg", "icons\/triangle-slate.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/triangle-slate-9ab4de7bca.svg", "icons\/twitter-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/twitter-white-3a817fbe86.svg", "icons\/unlock-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/unlock-slate-light-bb00dbedbe.svg", "icons\/user-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/user-slate-light-da94c8ae15.svg", "icons\/user-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/user-white-fb6434d0b7.svg", "icons\/wedge-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/wedge-slate-light-0b3e1c88dc.svg", "icons\/wedge-slate.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/wedge-slate-092dc0106c.svg", "icons\/wedge-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/wedge-white-da87bebc6a.svg", "icons\/x-secondary.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/x-secondary-58a296eec5.svg", "icons\/x-slate-light.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/x-slate-light-8793ac6f72.svg", "icons\/x-white.svg":"\/public\/suppliers-backoffice\/build\/img\/icons\/x-white-97044e9078.svg", "spamiles-logo-primary.png":"\/public\/suppliers-backoffice\/build\/img\/spamiles-logo-primary-6c7bd0ab73.png", "spamiles-logo-white.png":"\/public\/suppliers-backoffice\/build\/img\/spamiles-logo-white-ac60527109.png", "user-avatar.svg":"\/public\/suppliers-backoffice\/build\/img\/user-avatar-d14a2141f6.svg"}, "js":{"codebase.js":"\/public\/suppliers-backoffice\/build\/js\/codebase.255597457a85ba985083.js"}, "css":{"codebase.css":"\/public\/suppliers-backoffice\/build\/css\/codebase-ca6f8e0b49.css", "test.css":"\/public\/suppliers-backoffice\/build\/css\/test-2d0c7373f6.css"}}, "international":[], "CommunicationInterface":{"locale":{"ERROR_TITLE":"An error occurred while processing your request", "ERROR_INFO":"Sending your request has failed. Please try again.", "SUPPORT_INFO":"In case of further problems please contact Support Department attaching the following information:"}}, "EasyEdit":{"locale":{"ERROR_TITLE":"An error occurred while saving", "ERROR_CONTENT":"An error occurred while saving. Please try again or contact Support Department."}}, "Mark":{"locale":{"ERROR_TITLE":"An error occurred while saving", "ERROR_CONTENT":"An error occurred while marking as read\/unread. Please try again or contact Support Department."}}, "Gallery":{"locale":{"ERROR_TITLE":"An error occurred while saving", "ERROR_CONTENT":"An error occurred while saving. Please try again or contact Support Department."}}};
                    console.log('%cJSON data set to window.BIACONFIG object is valid:', 'color: #090');
                    console.log(window.BIACONFIG);
                } catch (error) {
                    console.log('%cJSON data passed to window.BIACONFIG object is invalid:', 'color: #900');
                    console.log(error);
                }
            })();</script>
        <script src="/public/suppliers-backoffice/build/js/codebase.255597457a85ba985083.js" defer></script>
    </body>
</html>