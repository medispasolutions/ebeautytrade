<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/banner/'.$this->session->userdata("back_link"),'<b>List banner</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/banner/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/banner/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["background"] = "";
$info["id_brands"] = "";
$info["link"] = "";
$info["featured_image"] = "";
$info["featured_title"] = "";
$info["featured_url"] = "";
$info["banner_link"] = "";
$info["slide_delay"] = "";
$info["version"] = "";
$info["pages"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
?>
<div class="row-fluid">
<div class="span6">
<?php
echo form_label('<b>BRANDS</b> ', 'BRANDS');

$cond=array();

$brands = $this->fct->getAll_cond('brands','title asc',$cond);
$options = array();
$options[""] = '--Select Brands--';
foreach($brands as $brand) {
	$options[$brand['id_brands']] = $brand['title'];
}

echo form_dropdown('id_brands',$options,set_value('id_brands',$info['id_brands'])); 
echo form_error("id_brands","<span class='text-error'>","</span>");
echo br();

?>
</div>
</div>
<?php
//BACKGROUND SECTION.
echo form_label('<b>BACKGROUND (With Brand = > Width:968px,Height:292px; Without Brand => Width:1200px,Height:292px )</b>', 'BACKGROUND');
echo form_upload(array("name" => "background", "class" => "input-large"));
echo "<span >";
if($info["background"] != ""){ 
echo '<span id="background_'.$info["id_banner"].'">'.anchor('uploads/banner/'.$info["background"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("banner","background","<?php echo $info["background"]; ?>",<?php echo $info["id_banner"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("background","<span class='text-error'>","</span>");
echo br();
//LINK SECTION.
echo form_label('<b>LINK </b>', 'LINK');
echo form_input(array('name' => 'link', 'value' => set_value("link",$info["link"]),'class' =>'span' ));
echo form_error("link","<span class='text-error'>","</span>");
echo br();
echo br();
echo form_fieldset(" ");

//FEATURED TITLE SECTION.
echo form_label('<b>FEATURED TITLE</b>', 'FEATURED TITLE');
echo form_input(array('name' => 'featured_title', 'value' => set_value("featured_title",$info["featured_title"]),'class' =>'span' ));
echo form_error("featured_title","<span class='text-error'>","</span>");
echo br();

//FEATURED IMAGE SECTION.
echo form_label('<b>FEATURED IMAGE (Width:300px,Height:380px)</b>', 'FEATURED IMAGE');
echo form_upload(array("name" => "featured_image", "class" => "input-large"));
echo "<span >";
if($info["featured_image"] != ""){ 
echo '<span id="featured_image_'.$info["id_banner"].'">'.anchor('uploads/banner/'.$info["featured_image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("banner","featured_image","<?php echo $info["featured_image"]; ?>",<?php echo $info["id_banner"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("featured_image","<span class='text-error'>","</span>");
echo br();
echo br();
//FEATURED URL SECTION.
echo form_label('<b>FEATURED URL</b>', 'FEATURED URL');
echo form_input(array('name' => 'featured_url', 'value' => set_value("featured_url",$info["featured_url"]),'class' =>'span' ));
echo form_error("featured_url","<span class='text-error'>","</span>");
echo br();

//VERSION SECTION.
echo form_label('<b>VERSION</b>', 'VERSION');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("version", $options,set_value("version",$info["version"]), 'class="span"');
echo form_error("version","<span class='text-error'>","</span>");
echo br();

echo form_fieldset_close();

echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}

echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>