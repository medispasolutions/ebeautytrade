<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" ><ul class="breadcrumb">
<li><a href="<?= site_url('back_office/products'); ?>" >List of Products</a></li><span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/edit/'.$product['id_products']); ?>" >Edit <?php echo $product['title']; ?></a></li>
<span class="divider">/</span>
<li><a href="<?= site_url('back_office/products/purchases?id_product='.$product['id_products']); ?>" >List of Product Informations</a></li>
<span class="divider">/</span>
<li class="active"><?php echo $title;?></li>

</ul>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/product_information/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["description"] = "";
$info["id_products"] = "";
$info["description_supplier"] = "";
$info["title_supplier"] = "";
$info["file"] = "";
$info["video"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();

//DESCRIPTION SECTION.
echo form_label('<b>DESCRIPTION</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description", "value" => set_value("description",$info["description"]),"class" =>"ckeditor","id" => "description", "rows" => 15, "cols" =>100 ));
echo form_error("description","<span class='text-error'>","</span>");
echo br();
//ID PRODUCTS SECTION.
echo form_label('<b>ID PRODUCTS</b>', 'ID PRODUCTS');
echo form_input(array('name' => 'id_products', 'value' => set_value("id_products",$product["id_products"]),'class' =>'span' ));
echo form_error("id_products","<span class='text-error'>","</span>");
echo br();
//DESCRIPTION SUPPLIER SECTION.
echo form_label('<b>DESCRIPTION SUPPLIER</b>', 'DESCRIPTION SUPPLIER');
echo form_textarea(array("name" => "description_supplier", "value" => set_value("description_supplier",$info["description_supplier"]),"class" =>"ckeditor","id" => "description_supplier", "rows" => 15, "cols" =>100 ));
echo form_error("description_supplier","<span class='text-error'>","</span>");
echo br();
//TITLE SUPPLIER SECTION.
echo form_label('<b>TITLE SUPPLIER</b>', 'TITLE SUPPLIER');
echo form_input(array('name' => 'title_supplier', 'value' => set_value("title_supplier",$info["title_supplier"]),'class' =>'span' ));
echo form_error("title_supplier","<span class='text-error'>","</span>");
echo br();
//FILE SECTION.
echo form_label('<b>FILE</b>', 'FILE');
echo form_upload(array("name" => "file", "class" => "input-large"));
echo "<span >";
if($info["file"] != ""){ 
echo '<span id="file_'.$info["id_product_information"].'">'.anchor('uploads/product_information/'.$info["file"],'show file',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("product_information","file","<?php echo $info["file"]; ?>",<?php echo $info["id_product_information"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo form_error("file","<span class='text-error'>","</span>");
echo br();
//VIDEO SECTION.
echo form_label('<b>VIDEO</b>', 'VIDEO');
echo form_input(array('name' => 'video', 'value' => set_value("video",$info["video"]),'class' =>'span' ));
echo form_error("video","<span class='text-error'>","</span>");
echo br();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>