<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(anchor('back_office/users','<b>List Users</b>').'<span class="divider">/</span>',
            anchor('back_office/users/edit/'.$info['id_user'],'<b>Edit User('.$info['name'].')</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/supplier_info/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/users/submit_brand', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["name"] = "";
$info["mobile"] = "";
$info["image"] = "";
$info["logo"] = "";
$info["video"] = "";
$info["position"] = "";
$info["banner_image"] = "";
$info["set_as_featured"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>


<?php echo form_fieldset(" ");?>





<?php  
	if(!empty($supplier_info)){
		}else{
$supplier_info['name'] = "";
$supplier_info['image'] = "";
$supplier_info['logo'] = "";
$supplier_info['overview'] = "";
$supplier_info['mobile'] = "";
$supplier_info['position'] = "";
$supplier_info['video'] = "";
$supplier_info['banner_image'] = "";
}
	?>
    
<?php echo form_fieldset("");?>
<input type="hidden" name="id_user" value="<?php echo $info['id_user'];?>" />
<input type="hidden" name="id_supplier_info" value="<?php echo $supplier_info['id_supplier_info'];?>" />
<div class="row-fluid">
<?php $brands=$this->fct->getAll('brands','title asc');?>

<div class="span5">
<label>Brands:</label>
<label>
<select name="id_brands" class="input-xxlarge" >
<option value="" >-Select Brand-</option>
<?php foreach($brands as $brand){?>
<option value="<?php echo $brand['id_brands'];?>" <? if(set_value('id_brands',$info["id_brands"]) == $brand['id_brands']) echo 'selected="selected"'; ?> ><?php echo $brand['title'];?></option>
<?php } ?>

</select>
<?= form_error('id_brands','<span class="text-error">','</span>'); ?>
</label> 
</div>
<div class="span5">
<label class="field-title">Banner Image:</label>
<?php

echo form_upload(array("name" => "banner_image", "class" => "input-large"));
echo "<span >";
if($supplier_info["banner_image"] != ""){ 
echo '<span id="banner_image_'.$supplier_info["id_supplier_info"].'">'.anchor('uploads/supplier_info/'.$supplier_info["banner_image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("supplier_info","banner_image","<?php echo $supplier_info["banner_image"]; ?>",<?php echo $supplier_info["id_supplier_info"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No  Image Available</span>"; } 
echo "</span>";
echo form_error("banner_image","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>




<div class="row-fluid">
<div class="span5">
<label class="field-title">Name of support person<em>*</em>:</label>
<label><input type="text" class="input-xlarge" name="name" value="<?= set_value('name',$supplier_info["name"]); ?>" />
<?= form_error('name','<span class="text-error">','</span>'); ?>
</label>
</div>
<div class="span5">
<label class="field-title">Mobile of support person:</label>
<label><input type="text" class="input-xlarge" name="mobile" value="<?= set_value('mobile',$supplier_info["mobile"]); ?>" />
<?= form_error('mobile','<span class="text-error">','</span>'); ?>
</label>
</div>
</div>
<div class="row-fluid">
<div class="span5">
<label class="field-title">Photo of contact person:</label>
<?php

echo form_upload(array("name" => "image", "class" => "input-large"));
echo "<span >";
if($supplier_info["image"] != ""){ 
echo '<span id="image_'.$supplier_info["id_supplier_info"].'">'.anchor('uploads/supplier_info/'.$supplier_info["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("supplier_info","image","<?php echo $supplier_info["image"]; ?>",<?php echo $supplier_info["id_supplier_info"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("image","<span class='text-error'>","</span>");
echo br();
?>
</div>
<div class="span5">
<label class="field-title">Logo:</label>
<?php

echo form_upload(array("name" => "logo", "class" => "input-large"));
echo "<span >";
if($supplier_info["logo"] != ""){ 
echo '<span id="logo_'.$supplier_info["id_supplier_info"].'">'.anchor('uploads/supplier_info/'.$supplier_info["logo"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("supplier_info","logo","<?php echo $supplier_info["logo"]; ?>",<?php echo $supplier_info["id_supplier_info"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("logo","<span class='text-error'>","</span>");
echo br();
?>
</div>
</div>
<div class="row-fluid">
<div class="span5">
<label class="field-title">Position:</label>
<label><input type="text" class="input-xlarge" name="position" value="<?= set_value('position',$supplier_info["position"]); ?>" />
<?= form_error('position','<span class="text-error">','</span>'); ?>
</label>
</div>
<div class="span5">
<label class="field-title">video:(<b>ex:</b> https://www.youtube.com/watch?v=C0DPdy98e4c)</label>
<label><input type="text" class="input-xlarge" name="video" value="<?= set_value('video',$supplier_info["video"]); ?>" />
<?= form_error('video','<span class="text-error">','</span>'); ?>
</label>
</div>
</div>
<?php 
//OVERVIEW SECTION.
echo form_label('<b>OVERVIEW</b>', 'OVERVIEW');
echo form_textarea(array("name" => "overview", "value" => set_value("overview",$supplier_info["overview"]),"class" =>"ckeditor","id" => "overview", "rows" => 15, "cols" =>100 ));
echo form_error("overview","<span class='text-error'>","</span>");
echo br();
?>
<div class="row-fluid">

<div class="span5">
<label style="margin-top:25px;" >
<?php
if(isset($supplier_info['set_as_featured']))
$set_as_featured = $supplier_info['set_as_featured'];
?>
<input type="checkbox"  name="set_as_featured"  <?php if(isset($set_as_featured) && $set_as_featured == 1) {?> checked="checked"<?php }?> value="1"  />&nbsp;&nbsp;Set as featured</label>

</div>
</div>
<?php 

echo form_fieldset_close();?>
<?php echo br(); ?>
<?php echo form_fieldset("Advertisements (Width:850px,Height:160px)");?>
<div class="row-fluid">

<?php for($i=1;$i<5;$i++){ ?>

<div class="span5">
<label class="field-title" style="margin-top:10px;"><b>Slide <?php echo $i;?>:</b></label>
<?php

echo form_upload(array("name" => "slide_".$i, "class" => "input-large"));
echo "<span >";
if($supplier_info["slide_".$i] != ""){ 
echo '<span id="slide_'.$i.'_'.$supplier_info["id_supplier_info"].'">'.anchor('uploads/supplier_info/'.$supplier_info["slide_".$i],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("supplier_info","slide_<?php echo $i;?>","<?php echo $supplier_info["slide_".$i]; ?>",<?php echo $supplier_info["id_supplier_info"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No  Image Available</span>"; } 
echo "</span>";
echo form_error("slide_".$i,"<span class='text-error'>","</span>");
echo br();
?>
</div>
<?php 
if($i%2==0 && $i!=4){?>
</div>
<div class="row-fluid">
<?php }} ?>
</div>

<?php echo form_fieldset_close();?>
<?php
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>