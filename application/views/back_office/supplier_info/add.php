<?php $size_arr=getMaxSize('file');?>

<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >

<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(anchor('back_office/users','<b>List Users</b>').'<span class="divider">/</span>',
            anchor('back_office/users/edit/'.$info['id_user'],'<b>Edit User('.$info['name'].')</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/supplier_info/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/supplier_info/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
	
echo form_hidden('id', $id);
} else {
$info["name"] = "";
$info["mobile"] = "";
$info["image"] = "";
$info["logo"] = "";
$info["video"] = "";
$info["position"] = "";
$info["banner_image"] = "";
$info["set_as_featured"] = "";
$info["website"] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}

echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>


<?php echo form_fieldset(" ");?>
<!--
<div class="row-fluid">

<div class="span5">
<label class="field-title"><strong>Miles :</strong> <?php echo $info['miles'];?></label>
</div>
</div>
<div class="row-fluid">
<div class="span5">
<label class="field-title"><strong>Available Balance :</strong> <?php echo $info['available_balance'];?> <?php echo $this->config->item('default_currency');?></label>
</div>
</div>
-->
<div class="row-fluid">
<div class="span5">
<input type="hidden" name="id_user" value="<?= set_value('id_user',$info["id_user"]); ?>" />
<label class="field-title">Trading Name<em>*</em>:</label>
<label><input type="text" class="input-xlarge" name="trading_name" value="<?= set_value('trading_name',$info["trading_name"]); ?>" />
<?= form_error('trading_name','<span class="text-error">','</span>'); ?>
</label>
</div>
<div class="span5">

<label class="field-title">Trading License:</label>
    <div class="form-item input_fields_wrap2">
    <?php
    if ($info['trading_licence']): ?>
        <a href="/uploads/user/<?= $info["trading_licence"]; ?>" target="_blank"  >show file</a>
        <br>
    <?php
    endif;
    ?>
        Upload file: <input class="textbox input-file" type="file" name="trading_licence" id="trade">
    </div>
    <span id="trading_licence_f-error" class="form-error"></span>

</div>
<br />
</div>
<div class="row-fluid">

<div class="span5">

<label class="field-title">Website: (DISABLED, TO BE DELETED)</label>
<label><input disabled type="text" class="input-xlarge" name="website" value="<?= set_value('website',$info["website"]); ?>" />
<?= form_error('website','<span class="text-error">','</span>'); ?>
</label>
</div>

<div class="span5">
<label class="field-title">Business Type<em>*</em>:</label>
<label>
<?php $business_type = $this->fct->getAll("business_type","title");?>
          <select name ="business_type" class="input-xxlarge">
          <option value="">-Select Business Type-</option>
          <?php foreach($business_type as $val){?>
          <option <?php if($val['id_business_type']==$info['business_type']){ echo "selected";}?> value="<?php echo $val['id_business_type'];?>"><?php echo $val['title'];?></option>
          <?php } ?>
          </select>
<?= form_error('business_type','<span class="text-error">','</span>'); ?>
</label>
</div>

<!--<div class="span5">
<label class="field-title">How Diy You Hear About Us?<em>*</em>:</label>
<label><?php $hear_about_us = $this->fct->getAll("how_did_you_hear_about_us","title");?>
          <select name ="hear_about_us" class="input-xxlarge">
           <option value="">-Select-</option>
          <?php foreach($hear_about_us as $val){?>
          <option <?php if($val['id_how_did_you_hear_about_us']==$info['hear_about_us']){ echo "selected";}?> value="<?php echo $val['id_how_did_you_hear_about_us'];?>"><?php echo $val['title'];?></option>
          <?php } ?>
          </select>
<?= form_error('hear_about_us','<span class="text-error">','</span>'); ?>
</label>
</div>-->

</div>
<div class="row-fluid">
<div class="span6">
<?php
//LOGO SECTION.
echo form_label('<b>LOGO <small class="blue">(Max Size '.$size_arr['size_mega'].'Mb)</small></b>', 'LOGO');
echo form_upload(array("name" => "logo", "class" => "input-large"));
echo "<span >";
if($info["logo"] != ""){ 
echo '<span id="logo_'.$info["id_user"].'">'.anchor('uploads/user/'.$info["logo"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("users","logo","<?php echo $info["logo"]; ?>",<?php echo $info["id_user"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo "<div></div>";
echo form_error("logo","<span class='text-error'>","</span>");

?>
<?php echo br();?>

</div>



</div>

<div class="row-fluid">


<?php if($info['id_roles']==20){?>
<div class="span5 " >
<label class="field-title">Product/service Categories<em>*</em>:</label>
<label>
<? 

$categories =$this->fct->getAll_cond('categories','sort_order',array('id_parent'=>0));	

$selected_categories = array();
if(isset($info['id_user'])) {
$selected_categories = $this->users_categories_m->select_user_categories($info['id_user']);
}


if(isset($val_categories)) {
 $selected_categories = $val_categories;
}
?>
<select multiple="multiple" style="height:200px" name="categories[]" class="input-xxlarge">
<? 
foreach($categories as $valll){ 
$cl = '';

if(in_array($valll['id_categories'],$selected_categories)) $cl = 'selected="selected"';
?>
<?php /*?><?php echo $this->custom_fct->printsubcategories($valll,$selected_categories,0); ?><?php */?>
<option  value="<?php echo $valll['id_categories'] ;?>" <?php echo $cl  ;?>><?php echo $valll['title'];?></option>
<? 
} ?>
</select>
<input type="hidden" name="validate_categories" value="1" />
<!--<a href="#myModal" onclick="popup_categories();" data-toggle="modal" >[add more]</a>-->

</label>
</div>
<?php } ?>

<div class="span5">
<label class="field-title">Comments</label>
<label>
<textarea class="span" rows="3" cols="100" name="comments"><?php echo $info['comments'];?></textarea>
</label>
</div>
</div>

<div class="row-fluid">
<div class="span5 " >
<?php


if(isset($info['display_in_menu']))
$display_in_menu = $info['display_in_menu'];
?>
<label ><input type="checkbox" id="display_in_menu" name="display_in_menu" <?php if(isset($display_in_menu) && $display_in_menu == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Display in menu </b></label>

<?php echo br();?>

</div></div>


<fieldset>
<legend>Terms</legend>
<div class="row-fluid">
    <div class="span12">
        <div class="span4">
            <label class="field-title">DELIVERY TIME:</label>
            <label>
                <select name ="delivery_time" class="input-xxlarge">
                    <option value="">-Select-</option>
                    <option <?php if("1-3-days"==$info['delivery_time']){ echo "selected";}?> value="1-3-days">1-3 days</option>
                    <option <?php if("3-5-weeks"==$info['delivery_time']){ echo "selected";}?> value="3-5-weeks">3-5 weeks</option>
                    <option <?php if("other"==$info['delivery_time']){ echo "selected";}?> value="other">other..</option>
                </select>

            </label>
        </div>
        <div class="span3">
            <label class="field-title">DELIVERY OTHER DURATION:</label>
            <label>
                <input type="text" class="input-large" name="delivery_time_period" value="<?= set_value('delivery_time_period',$info["delivery_time_period"]); ?>" >
            </label>
        </div>
        <div class="span3">
            <label class="field-title">DELIVERY OTHER DURATION TYPE:</label>
            <label>
                <select name ="delivery_time_period_type" class="input-xxlarge">
                    <option value="">N/A</option>
                    <option <?php if("days"==$info['delivery_time_period_type']){ echo "selected";}?> value="days">days</option>
                    <option <?php if("weeks"==$info['delivery_time_period_type']){ echo "selected";}?> value="weeks">weeks</option>
                </select>

            </label>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span5">
        <label class="field-title">DELIVERY FEE: </label>
        <label>
            <select name ="delivery_fee" class="input-xxlarge">
                <option value="">-Select-</option>
                <option <?php if("free"==$info['delivery_fee']){ echo "selected";}?> value="free">free</option>
                <option <?php if("courier-price"==$info['delivery_fee']){ echo "selected";}?> value="courier-price">as per the courier rate</option>
                <option <?php if("free-on-bigger-orders"==$info['delivery_fee']){ echo "selected";}?> value="free-on-bigger-orders">free on orders &gt;:</option>
                <option <?php if("fixed-fee"==$info['delivery_fee']){ echo "selected";}?> value="fixed-fee">fixed fee:</option>
            </select>
</label>
</div>
    <div class="span5">
        <label class="field-title">DELIVERY FEE PRICE OR AMOUNT OF ORDER FOR FREE: </label>
        <label><input type="text" class="input-xlarge" name="delivery_fee_price" value="<?= set_value('delivery_fee_price',$info["delivery_fee_price"]); ?>" >
        </label>
    </div>
</div>

<div class="row-fluid">
<div class="span5">
<label class="field-title">MIN. ORDER: (DISABLED, TO BE DELETED)</label>
<label><input disabled type="text" class="input-xlarge" name="min_order" value="<?= set_value('min_order',$info["min_order"]); ?>" >
</label>
</div>
<div class="span5">
<label class="field-title">CREDIT TERMS: </label>

<label>
    <select name ="credit_terms" class="input-xxlarge">
        <option value="">-Select-</option>
        <option <?php if("consult-with-supplier"==$info['credit_terms']){ echo "selected";}?> value="consult-with-supplier">Consult with Supplier</option>
        <option <?php if("do-not-publish"==$info['credit_terms']){ echo "selected";}?> value="do-not-publish">Do not publish</option>
    </select>
</label>
</div>
</div>



</fieldset>


<?php echo form_fieldset_close();?>


<?php
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>


<script>
$(document).ready(function(e) {

var baseurl=$('#baseurl').val();

    var max_fields      = 50; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap2"); //Fields wrapper
    var add_button      = $(".add_field_button2"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){
		 var cc=$('.remove_d').length;
	  var rand=Math.floor((Math.random() * 1000) + 1)+"a";//on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
 $(wrapper).append('<div><input  type="hidden" name="trading_license[]" value="'+rand+'" ><input class="textbox input-file" type="file" name="trading_license_'+rand+'"  id="trade_'+rand+'"><a class="remove_field remove_d" id="remove_'+rand+'" alt="Delete" tiltle="Delete">Remove</a></div>'); //add input box

		}
		
		if(cc>4){add_button.hide();}
    });

       $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	var id=$(this).attr('id').replace('remove_','');
 var cc=$('.remove_d').length;
    $('#trade_'+id).parent('div').remove(); x--;
    if(cc>4){}else{add_button.show();} })	

});


</script>
