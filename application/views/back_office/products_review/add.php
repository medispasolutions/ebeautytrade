<script type="text/javascript">
function removeRow(section,field,id)
{
	if(confirm('Are you sure you want to delete this record ?')) {
		$('#'+field+'_'+id).html('Loading...');
		var url = '<?php echo site_url(); ?>back_office/products/delete_row';
		$.post(url,{section : section,field : field,id : id},function(data){
			$('#'+field+'_'+id).html(data);
			  $('#section_repeat_'+id).remove(); 
		});
	}
	else {
		return false;
	}
}
$(document).ready(function(e) {
	
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ 
	  var rand=Math.floor((Math.random() * 1000) + 1)+"a";//on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><div class="contactrow"><div class="contactlabel">Title<span>*</span></div><input class="span" type="text" value="" name="title_product_info['+rand+']"></div><div class="contactrow"><div class="contactlabel">Description <span</span></div><textarea id="description_'+rand+'" class="ckeditor" rows="15" cols="100" name="description_product_info['+rand+']" ></textarea></div><a href="#" class="remove_field remove_d"><i class="icon-remove-sign"></i> Remove</a><hr></div>'); //add input box
			
		var id = 'description_'+rand;
		 CKEDITOR.replace( id,{
       	 filebrowserUploadUrl : '<?php echo base_url(); ?>texteditor/upload'
		  
    	});
	
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })	
	

   var cont=$('#other_qualification').val(); 
   	if(cont=='other')
	{
		$('.other_qualification').css('display','block');
		$('.other_qualification').addClass('acty');
	}
	var cont1=$('#other_qualification_1').val(); 
   	if(cont1=='other')
	{
		$('.other_qualification_1').css('display','block');
		$('.other_qualification_1').addClass('acty');
	}
})


</script>
<script type="text/javascript">
function changeCategory()
{
	console.log($('#categoryBox').val());
	var categories = $('#categoryBox').val();
	$('#subCategoriesBox').html('<option value="">Loading...</option>');
	var url = '<?php echo site_url('back_office/products/filtercategories'); ?>';
	$.post(url,{categories : categories},function(data){
	     $('#subCategoriesBox').html(data);
	});
}
</script>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/products_review/'.$this->session->userdata("back_link"),'<b>List products review</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if(isset($id)) {
	$id_products=$info['id_products'];
		/*	if ($this->acl->has_permission('stock','index')){
				$ul['stock'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/stock').'" class="btn btn-info top_btn">Stock</a>');
			}*/
			if ($this->acl->has_permission('product_attributes','index')){
				/*$ul['stock'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/stock/'.$id).'" class="btn btn-info top_btn">Stock</a>');*/
				$ul['attributes'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/attributes?id_product='.$id_products).'" class="btn btn-info top_btn">Attributes</a>');
				$ul['price_segments'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/product_price_segments?id_product='.$id_products).'" class="btn btn-info top_btn">Price Segments</a>');
				$ul['quantity'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/products/purchases?supplier=1&id_product='.$id_products).'" class="btn btn-info top_btn">Manage Quantity</a>');
			}
			$ul['gallery'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/control/manage_gallery/products_review/'.$id).'" class="btn btn-info top_btn">Manage Gallery</a>');
			}
if($this->config->item("language_module") && isset($id_products)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/products/'.$id_products).'" class="btn btn-info top_btn">Translate</a>');
			}
$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);			
			}
?>

</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/products/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
$brand_training="";
if(isset($id)){
echo form_hidden('id', $id);
$brand_training=$this->fct->getonerecord('brand_certified_training',array('id_products'=>$id));
 
//echo '<input type="text" name="id1" value="'.$id.'" />';
} else {
$info["sku"] = "";
$info["quantity"] = "";
$info["price"] = "";
$info["account_type"] = "";
$info["delivery_days"] = "";
$info['set_as_training']="";
$info["description".$lang] = "";
$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
$info["discount"] = "";
$info["brief".$lang] = "";
$info["lang"] = "";
$info["list_price"] = "";
$info["id_categories"] = "";
$info["id_units"] = "";
$info["id_miles"] = "";
$info["id_brands"] = "";
$info['set_as_clearance']="";
$info["id_categories_sub"] = "";
$info["discount_expiration"] = "";
$info["features"] = "";
$info["datasheet"] = "";
$info["specifications"] = "";
$info["wholesale_price"] = "";
$info["related_products"] = "";
$info["display_in_home"] = "";
$info["downloads"] = "";
$info["id_designers"] = "";
$info["size_and_fit"] = "";
$info["editors_notes_and_details	"] = "";
$info["washing_information"] = "";
$info["why_and_when"] = "";
$info["video"] = "";
$info["pdf"] = "";
$info["availability"] = "";
$info["image"] = "";
$info["hide_price"] = "";
$info["status"] = "";
$info["packing"] = "";
$info["fabric"] = "";
$info["editors_notes_and_details"] = "";
$info["set_as_new"] = "";
$info["work"] = "";
$info["washing_instructions"] = "";
}
if(isset($brand_training['id_brand_certified_training'])){
	echo form_hidden('id_brand_certified_training',$brand_training['id_brand_certified_training']);}else{
$brand_training["title".$lang] = "";
$brand_training["image"] = "";
$brand_training["description".$lang] = "";
		}

if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo br();
echo form_fieldset("Product Details");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//BRIEF SECTION.
echo form_label('<b>BRIEF&nbsp:</b>', 'BRIEF');
echo form_textarea(array("name" => "brief".$lang, "value" => set_value("brief".$lang,$info["brief".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("brief".$lang,"<span class='text-error'>","</span>");
echo br();
//SKU&nbsp;<em>*</em>: SECTION.
echo form_label('<b>SKU&nbsp;<em>*</em>:</b>  ', 'SKU&nbsp;<em>*</em>:');
echo form_input(array('name' => 'sku', 'value' => set_value("sku",$info["sku"]),'class' =>'span' ));
echo form_error("sku","<span class='text-error'>","</span>");
echo br();

//TITLE AR SECTION.
/*echo form_label('<b>TITLE AR</b>', 'TITLE AR');
echo form_input(array('name' => 'title_ar', 'value' => set_value("title_ar",$info["title_ar"]),'class' =>'span' ));
echo form_error("title_ar","<span class='text-error'>","</span>");
echo br();*/

/*echo form_label('<b>UNIT</b> ', 'UNIT');
$lang_select = $this->lang->lang();
if(isset($lang)) {
	$lang_select = $lang;
}
$units = $this->fct->getAll('units','title');
$options = array();
foreach($units as $unit) {
	$options[$unit['id_units']] = $unit['title'];
}*/
?>
<!--<label>
<?php

echo form_dropdown('unit',$options,set_value('unit',$info['id_units'])); 
echo form_error("unit","<span class='text-error'>","</span>");
echo br();

?>
</label>-->

<?php

echo form_label('<b>BRANDS</b> ', 'BRANDS');
$brands = $this->fct->getAll('brands','title asc');
$options = array();
$options[""] = '--Select Brands--';
foreach($brands as $brand) {
	$options[$brand['id_brands']] = $brand['title'];
}

echo form_dropdown('id_brands',$options,set_value('id_brands',$info['id_brands'])); 
echo form_error("id_brands","<span class='text-error'>","</span>");
echo br();

?>
<?php

//QUANTITY SECTION.
echo form_label('<b>QUANTITY</b> ', 'QUANTITY');
echo form_input(array('name' => 'quantity', 'value' => set_value("quantity",$info["quantity"]),'class' =>'span' ));
echo form_error("quantity","<span class='text-error'>","</span>");
echo br();

?>
</label>
<?php /*?><?php
echo form_label('<b>CATEGORIES</b>  <span id="categoriesLoader"></span>', 'CATEGORIES');
$lang_select = $this->lang->lang();
if(isset($lang)) {
	$lang_select = $lang;
}
$categories = $this->fct->getAll('categories','title');
$options = array();
foreach($categories as $category) {
	$options[$category['id_categories']] = $category['title'];
}
?><?php */?>
<?php

//BACKGROUND SECTION.
echo form_label('<b>DATASHEET</b>', 'DATASHEET');
echo form_upload(array("name" => "datasheet", "class" => "input-large"));
echo "<span >";
if($info["datasheet"] != ""){ 
echo '<span id="datasheet_'.$info["id_products"].'">' ;?>
<a target="_blank" href="<?php echo base_url();?>uploads/products/<?php echo $info["datasheet"];?>">Show File</a>
<?php 
echo nbs(3);?>
<a class="cur" onclick='removeFile("products","datasheet","<?php echo $info["datasheet"]; ?>",<?php echo $info["id_products"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No File Available</span>"; } 
echo "</span>";
echo form_error("datasheet","<span class='text-error'>","</span>");
echo br();

echo br();
?><?php 


$test_arr=array(1,2,3);

$selected_options=array();
if(isset($id)) {
$selected_options = $this->fct->select_product_categories($id);

}
//CATEGORY SECTION.
echo form_label('<b>CATEGORY</b>', 'CATEGORY');
$items = $this->custom_fct->getCategoriesPages(0,array(),array(),12000,0);

//print '<pre>'; print_r($items); exit; 
$categories = $items;
echo '<select name="categories[]"  class="span" multiple="multiple" style="height:150px;">';
echo '<option value="" > - set as parent - </option>';
$s_id = '';
if(isset($id)) {
$s_id = $info['id_parent'];
}
echo displayLevels($items,0,$s_id,$selected_options);
echo "</select>";
echo form_error("category","<span class='text-error'>","</span>");
echo br();
?>


<?php 
echo form_label('<b>DESCRIPTION</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "description".$lang, "value" => set_value("description".$lang,$info["description".$lang]),"class" =>"ckeditor","id" => "description".$lang, "rows" => 15, "cols" =>100 ));
echo form_error("description".$lang,"<span class='text-error'>","</span>");
echo br();
echo br();
?>

<?php

echo br();
echo form_fieldset("PRICE");
//PRICE SECTION.
echo form_label('<b>PRICE *</b> ('.$this->config->item('default_currency').') ', 'PRICE');
echo form_input(array('name' => 'list_price', 'value' => set_value("list_price",$info["list_price"]),'class' =>'span' ));
echo form_error("list_price","<span class='text-error'>","</span>");
echo br();
/*echo form_label('<b>DISCOUNT</b> (integer) ', 'DISCOUNT');
echo form_input(array('name' => 'discount', 'value' => set_value("discount",$info["discount"]),'class' =>'span' ));
echo form_error("discount","<span class='text-error'>","</span>");
echo br();
*/
//DISCOUNT EXPIRATION SECTION.
echo form_label('<b>DISCOUNT EXPIRATION</b> ', 'DISCOUNT EXPIRATION');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'discount_expiration', 'value' => set_value("discount_expiration",$this->fct->date_out_formate($info["discount_expiration"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("discount_expiration","<span class='text-error'>","</span>");


//PRICE SECTION.
echo form_label('<b>SPECIAL PRICE *</b> ('.$this->config->item('default_currency').') ', 'PRICE');
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span' ));
echo form_error("price","<span class='text-error'>","</span>");
echo br();

/*
echo form_label('<b>FABRIC&nbsp;:</b>', 'FABRIC');
echo form_input(array("name" => "fabric".$lang, "value" => set_value("fabric".$lang,$info["fabric".$lang]),"class" =>"span" ));
echo form_error("fabric".$lang,"<span class='text-error'>","</span>");
echo br();
echo form_label('<b>CUT&nbsp;:</b>', 'CUT');
echo form_input(array("name" => "work".$lang, "value" => set_value("work".$lang,$info["work".$lang]),"class" =>"span" ));
echo form_error("work".$lang,"<span class='text-error'>","</span>");
echo br();
/*
echo form_label('<b>WASHING INSTRUCTION</b>', 'WASHING INSTRUCTION');
echo form_textarea(array("name" => "washing_instructions".$lang, "value" => set_value("washing_instructions".$lang,$info["washing_instructions".$lang]),"class" =>"ckeditor","id" => "washing_instructions".$lang, "rows" => 15, "cols" =>100 ));
echo form_error("washing_instructions".$lang,"<span class='text-error'>","</span>");
echo br();*/
echo form_fieldset("PRODUCT INFORMATION");
//DESCRIPTION SECTION.
?>

<div class="input_fields_wrap repeat">
<?php
$product_informations=$this->fct->getAll_cond("product_information",'sort_order',array('id_products'=>$id));
 foreach($product_informations as $val){?>
<div id="section_repeat_<?php echo $val["id_product_information"];?>">

<div class="contactrow">
<div class="contactlabel">Title <span>*</span></div>
<input class="span" type="text" value="<?=set_value('title_product_info[]',$val['title'])?>" name="title_product_info[<?php echo $val['id_product_information'];?>]">

</div>

<div class="contactrow">
<div class="contactlabel">Description <span>*</span></div>
<textarea id="description_<?php echo $val['id_product_information'];?>" class="ckeditor" rows="15" cols="100" name="description_product_info[<?php echo $val['id_product_information'];?>]" ><?=set_value('description_product_info[]',$val['description'])?></textarea>
</div>
<a onclick='removeRow("product_information","description","<?php echo $val["id_product_information"]; ?>")' class="remove_d"><i class="icon-remove-sign"></i> Remove</a>
<span id="description_<?php $val["id_product_information"];?>"></span>
<hr>
</div>
<? }?>
</div>
<div class="contactrow field">
	<button class="add_more btn add_field_button">Add Product Information</button>
</div>

<?php
echo form_fieldset_close();
echo br();
echo form_fieldset("Brand Certified Training");
 ?>
<?php 
//TITLE SECTION.
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "brand_training_title".$lang, "value" => set_value("brand_training_title".$lang,$brand_training["title".$lang]),"class" =>"span" ));
echo form_error("brand_training_title".$lang,"<span class='text-error'>","</span>");
echo br();

echo form_label('<b>DESCRIPTION</b>', 'DESCRIPTION');
echo form_textarea(array("name" => "brand_training_description".$lang, "value" => set_value("brand_training_description".$lang,$brand_training["description".$lang]),"class" =>"ckeditor","id" => "brand_training_description".$lang, "rows" => 15, "cols" =>100 ));
echo form_error("brand_training_description".$lang,"<span class='text-error'>","</span>");
echo br();


//BACKGROUND SECTION.
echo form_label('<b>IMAGE</b>', 'IMAGE');
echo form_upload(array("name" => "brand_training_image", "class" => "input-large"));
echo "<span >";
if($brand_training["image"] != ""){ 
echo '<span id="image_'.$brand_training["id_brand_certified_training"].'">'.anchor('uploads/brand_certified_training/'.$brand_training["image"],'show image',array("class" => 'blue gallery'));
echo nbs(3);?>
<a class="cur" onclick='removeFile("brand_certified_training","image","<?php echo $brand_training["image"]; ?>",<?php echo $brand_training["id_brand_certified_training"]; ?>)'><img src="<?php echo base_url()?>images/delete.png" /></a></span>
<?php
} else { echo "<span class='blue'>No Image Available</span>"; } 
echo "</span>";
echo form_error("brand_training_image","<span class='text-error'>","</span>");
echo br();
echo br();
echo form_label('<b>SET AS TRAINING</b>', 'SET AS TRAINING');
$options = array();
$options[0] = 'Unset';
$options[1] = 'Set';

?>
<label>
<?php
echo form_dropdown('set_as_training',$options,set_value('set_as_training',$info['set_as_training'])); 
echo form_error("set_as_training","<span class='text-error'>","</span>");
echo br();
?>
</label>
<?php

?>
<?php
echo form_fieldset_close();
echo br();
echo form_fieldset("PRODUCT STATUS");

echo form_label('<b>STATUS</b>', 'STATUS');
$options = array();
$options[0] = 'Published';
$options[1] = 'Unpublished';
?>
<label>
<?php
echo form_dropdown('status',$options,set_value('status',$info['status'])); 
echo form_error("status","<span class='text-error'>","</span>");
echo br();

?>
</label>
<?php

echo form_label('<b>AVAILABILITY</b>', 'AVAILABILITY');
$options = array();
$options[0] = 'Out of stock';
$options[1] = 'In stock';

?>
<label>
<?php
echo form_dropdown('availability',$options,set_value('availability',$info['availability'])); 
echo form_error("availability","<span class='text-error'>","</span>");

?>
<?php
/*echo br();
echo form_label('<b>SET AS NEW</b>', 'SET AS NEW');
$options = array();
$options[0] = 'Unset';
$options[1] = 'Set';*/
?>
<label>
<?php
/*echo form_dropdown('set_as_new',$options,set_value('set_as_new',$info['set_as_new'])); 
echo form_error("set_as_new","<span class='text-error'>","</span>");*/

if(isset($info['set_as_new']))
$set_as_new = $info['set_as_new'];
?>

<label><input type="checkbox" name="set_as_new" <?php if(isset($set_as_new) && $set_as_new == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as new</b></label>
</label>
<?php
if(isset($info['set_as_clearance']))
$set_as_clearance = $info['set_as_clearance'];
?>

<label><input type="checkbox" name="set_as_clearance" <?php if(isset($set_as_clearance) && $set_as_clearance == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>set as non-exportable product</b></label>
</label>
<?php /*?><?php
if(isset($info['promote_to_front']))
$promote_to_front = $info['promote_to_front'];
?>
<label><input type="checkbox" name="promote_to_front" <?php if(isset($promote_to_front) && $promote_to_front == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Set as Featured Product</b></label><?php */?>

<?php

if(isset($info['display_in_home']))
$display_in_home = $info['display_in_home'];
?>
<label><input type="checkbox" name="display_in_home" <?php if(isset($display_in_home) && $display_in_home == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Display in home</b></label>


<?php
echo br();
//MILES SECTION.

echo form_label('<b>Miles</b> ', 'Miles');
$miles = $this->fct->getAll('miles','sort_order');
$options = array();
$options[""] = '--Select Miles--';
foreach($miles as $mile) {
	$options[$mile['id_miles']] = $mile['title'];
}

echo form_dropdown('id_miles',$options,set_value('id_miles',$info['id_miles'])); 
echo form_error("id_miles","<span class='text-error'>","</span>");
echo br();
echo form_fieldset_close();


 /*?><?php
echo br();
echo br();

if(isset($info['hide_price']))
$hide_price = $info['hide_price'];
?><?php */?>
<!--<label><input type="checkbox" name="hide_price" <?php if(isset($hide_price) && $hide_price == 1) {?> checked="checked"<?php }?> value="1" style="margin:0" /> <b>Do not display price</b></label>-->
<?php


echo br();

echo form_fieldset("Meta Tags");
//PAGE TITLE SECTION.
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();
echo form_fieldset_close();
echo form_fieldset("");
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo form_submit(array('name' => 'submit','value' => 'Save and Continue','class' => 'btn btn-primary','style' => 'margin-left:10px') );

echo form_submit(array('name' => 'submit','value' => 'Save and manage gallery','class' => 'btn btn-primary','style' => 'margin-left:10px') );

echo form_submit(array('name' => 'submit','value' => 'Save and manage colors','class' => 'btn btn-primary','style' => 'margin-left:10px') );
echo form_submit(array('name' => 'submit','value' => 'Save and manage sizes','class' => 'btn btn-primary','style' => 'margin-left:10px') );


/*if(isset($id)) {
	echo '<a href="'.route_to("products/details/".$info['title_url']).'?preview=1" target="_blank" class="btn btn-primary" style="margin-left:10px">Preview Product</a>';
}*/
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>