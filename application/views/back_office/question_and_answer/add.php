<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/question_and_answer/'.$this->session->userdata("back_link"),'<b>List question and answer</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/question_and_answer/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/question_and_answer/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["phone"] = "";
$info["email"] = "";
$info["question"] = "";
$info["name"] = "";
$info["answer"] = "";
$info["reply"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}


//NAME SECTION.
echo form_label('<b>NAME</b>', 'NAME');
echo form_input(array('name' => 'name', 'value' => set_value("name",$info["name"]),'class' =>'span' ));
echo form_error("name","<span class='text-error'>","</span>");
echo br();


//PHONE SECTION.
echo form_label('<b>PHONE</b>', 'PHONE');
echo form_input(array('name' => 'phone', 'value' => set_value("phone",$info["phone"]),'class' =>'span' ));
echo form_error("phone","<span class='text-error'>","</span>");
echo br();
//EMAIL SECTION.
echo form_label('<b>EMAIL</b>', 'EMAIL');
echo form_input(array('name' => 'email', 'value' => set_value("email",$info["email"]),'class' =>'span' ));
echo form_error("email","<span class='text-error'>","</span>");
echo br();
//QUESTION SECTION.
echo form_fieldset("QUESTION");

echo form_textarea(array("name" => "question", "value" => set_value("question",$info["question"]),"class" =>"ckeditor","id" => "question", "rows" => 15, "cols" =>100 ));
echo form_error("question","<span class='text-error'>","</span>");
echo br();

//QUESTION SECTION.

echo form_fieldset("ANSWER");
echo form_textarea(array("name" => "answer", "value" => set_value("answer",$info["answer"]),"class" =>"ckeditor","id" => "answer", "rows" => 15, "cols" =>100 ));
echo form_error("answer","<span class='text-error'>","</span>");
echo br();
?>
<label><input type="checkbox" name="inform_client" value="1" style="margin-top:0" />&nbsp;&nbsp;Inform Client</label>
<?php

echo form_label('<b>STATUS</b>', 'STATUS');
$options = array();
$options[''] = '-select status-';
$options[0] = 'Published';

$options[1] = 'Unpublished';
$options[2] = 'Under Review';
?>
<label>
<?php
echo form_dropdown('status',$options,set_value('status',$info['status'])); 
echo form_error("status","<span class='text-error'>","</span>");


?>
</label>

<?php

if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}
echo form_fieldset_close();
echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>