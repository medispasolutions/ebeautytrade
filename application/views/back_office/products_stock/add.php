<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/products_stock/'.$this->session->userdata("back_link"),'<b>List products stock</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );
if($this->config->item("language_module") && isset($id)) {
			   $ul['translate'] = array('li_attributes' => 'class = "pull-right"', 'contents' => '<a href="'.site_url('back_office/translation/section/products_stock/'.$id).'" class="btn btn-info top_btn">Translate</a>');
			}

$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');
echo form_open_multipart('back_office/products_stock/submit', $attributes); 
echo '<p class="alert alert-info">Please complete the form below. Mandatory fields marked <em>*</em></p>';
if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["miles"] = "";
$info["retail2_price"] = "";
$info["retail_list_price"] = "";
$info["retail_discount_status"] = "";
$info["retail_discount_expiration"] = "";
$info["retail_discount"] = "";
$info["return_quantity"] = "";
$info["threshold"] = "";
$info["threshold_flag"] = "";
$info["barcode"] = "";
$info["set_as_redeemed_by_miles"] = "";
$info["set_general_miles"] = "";
$info["general_miles"] = "";
$info["product"] = "";
$info["combination"] = "";
$info["sku"] = "";
$info["quantity"] = "";
$info["price"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
if($this->session->userdata("success_message")){ 
echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}
echo form_label('<b>Title&nbsp;<em>*</em>:</b>', 'Title');
echo form_input(array("name" => "title".$lang, "value" => set_value("title".$lang,$info["title".$lang]),"class" =>"span" ));
echo form_error("title".$lang,"<span class='text-error'>","</span>");
echo br();
//MILES SECTION.
echo form_label('<b>MILES</b>', 'MILES');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("miles", $options,set_value("miles",$info["miles"]), 'class="span"');
echo form_error("miles","<span class='text-error'>","</span>");
echo br();
//RETAIL2 PRICE SECTION.
echo form_label('<b>RETAIL2 PRICE</b>', 'RETAIL2 PRICE');
echo form_input(array('name' => 'retail2_price', 'value' => set_value("retail2_price",$info["retail2_price"]),'class' =>'span' ));
echo form_error("retail2_price","<span class='text-error'>","</span>");
echo br();
//RETAIL LIST PRICE SECTION.
echo form_label('<b>RETAIL LIST PRICE</b>', 'RETAIL LIST PRICE');
echo form_input(array('name' => 'retail_list_price', 'value' => set_value("retail_list_price",$info["retail_list_price"]),'class' =>'span' ));
echo form_error("retail_list_price","<span class='text-error'>","</span>");
echo br();
//RETAIL DISCOUNT STATUS SECTION.
echo form_label('<b>RETAIL DISCOUNT STATUS</b>', 'RETAIL DISCOUNT STATUS');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("retail_discount_status", $options,set_value("retail_discount_status",$info["retail_discount_status"]), 'class="span"');
echo form_error("retail_discount_status","<span class='text-error'>","</span>");
echo br();
//RETAIL DISCOUNT EXPIRATION SECTION.
echo form_label('<b>RETAIL DISCOUNT EXPIRATION</b>', 'RETAIL DISCOUNT EXPIRATION');
echo '<div class="input-append date" data-date="" data-date-format="dd/mm/yyyy">';
echo form_input(array('name' => 'retail_discount_expiration', 'value' => set_value("retail_discount_expiration",$this->fct->date_out_formate($info["retail_discount_expiration"])),'class' =>'input-small','size'=>16 ));
echo '<span class="add-on"><i class="icon-th"></i></span></div>';
echo form_error("retail_discount_expiration","<span class='text-error'>","</span>");
echo br();
//RETAIL DISCOUNT SECTION.
echo form_label('<b>RETAIL DISCOUNT</b>', 'RETAIL DISCOUNT');
echo form_input(array('name' => 'retail_discount', 'value' => set_value("retail_discount",$info["retail_discount"]),'class' =>'span' ));
echo form_error("retail_discount","<span class='text-error'>","</span>");
echo br();
//RETURN QUANTITY SECTION.
echo form_label('<b>RETURN QUANTITY</b>', 'RETURN QUANTITY');
echo form_input(array('name' => 'return_quantity', 'value' => set_value("return_quantity",$info["return_quantity"]),'class' =>'span' ));
echo form_error("return_quantity","<span class='text-error'>","</span>");
echo br();
//THRESHOLD SECTION.
echo form_label('<b>THRESHOLD</b>', 'THRESHOLD');
echo form_input(array('name' => 'threshold', 'value' => set_value("threshold",$info["threshold"]),'class' =>'span' ));
echo form_error("threshold","<span class='text-error'>","</span>");
echo br();
//THRESHOLD FLAG SECTION.
echo form_label('<b>THRESHOLD FLAG</b>', 'THRESHOLD FLAG');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("threshold_flag", $options,set_value("threshold_flag",$info["threshold_flag"]), 'class="span"');
echo form_error("threshold_flag","<span class='text-error'>","</span>");
echo br();
//BARCODE SECTION.
echo form_label('<b>BARCODE</b>', 'BARCODE');
echo form_input(array('name' => 'barcode', 'value' => set_value("barcode",$info["barcode"]),'class' =>'span' ));
echo form_error("barcode","<span class='text-error'>","</span>");
echo br();
//SET AS REDEEMED BY MILES SECTION.
echo form_label('<b>SET AS REDEEMED BY MILES</b>', 'SET AS REDEEMED BY MILES');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("set_as_redeemed_by_miles", $options,set_value("set_as_redeemed_by_miles",$info["set_as_redeemed_by_miles"]), 'class="span"');
echo form_error("set_as_redeemed_by_miles","<span class='text-error'>","</span>");
echo br();
//SET GENERAL MILES SECTION.
echo form_label('<b>SET GENERAL MILES</b>', 'SET GENERAL MILES');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("set_general_miles", $options,set_value("set_general_miles",$info["set_general_miles"]), 'class="span"');
echo form_error("set_general_miles","<span class='text-error'>","</span>");
echo br();
//GENERAL MILES SECTION.
echo form_label('<b>GENERAL MILES</b>', 'GENERAL MILES');
$options = array(
                  "" => " - select option - ",
                  1 => "Active",
                  0 => "InActive",
                );
echo form_dropdown("general_miles", $options,set_value("general_miles",$info["general_miles"]), 'class="span"');
echo form_error("general_miles","<span class='text-error'>","</span>");
echo br();
//PRODUCT SECTION.
echo form_label('<b>PRODUCT</b>', 'PRODUCT');
$items = $this->fct->getAll("products","sort_order"); 
echo '<select name="product'.'"  class="span">';
echo '<option value="" > - select products - </option>';
foreach($items as $valll){ 
?>
<option value="<?= $valll["id_products"]; ?>" <? if(isset($id)){  if($info["id_products"] == $valll["id_products"]){ echo 'selected="selected"'; } } ?> ><?= $valll["title"]; ?></option>
<?
}
echo "</select>";
echo form_error("product","<span class='text-error'>","</span>");
echo br();
//COMBINATION SECTION.
echo form_label('<b>COMBINATION</b>', 'COMBINATION');
echo form_input(array('name' => 'combination', 'value' => set_value("combination",$info["combination"]),'class' =>'span' ));
echo form_error("combination","<span class='text-error'>","</span>");
echo br();
//SKU SECTION.
echo form_label('<b>SKU</b>', 'SKU');
echo form_input(array('name' => 'sku', 'value' => set_value("sku",$info["sku"]),'class' =>'span' ));
echo form_error("sku","<span class='text-error'>","</span>");
echo br();
//QUANTITY SECTION.
echo form_label('<b>QUANTITY</b>', 'QUANTITY');
echo form_input(array('name' => 'quantity', 'value' => set_value("quantity",$info["quantity"]),'class' =>'span' ));
echo form_error("quantity","<span class='text-error'>","</span>");
echo br();
//PRICE SECTION.
echo form_label('<b>PRICE</b>', 'PRICE');
echo form_input(array('name' => 'price', 'value' => set_value("price",$info["price"]),'class' =>'span' ));
echo form_error("price","<span class='text-error'>","</span>");
echo br();
echo form_fieldset_close();
echo form_fieldset("Meta Tags");
//PAGE TITLE SECTION.
?><div class="row-fluid"><div class="span6"><?php
echo form_label('<b>Page Title&nbsp;<small class="blue">(Max 65 Characters)</small>:</b>', 'Page Title');
echo form_input(array("name" => "meta_title".$lang, "value" => set_value("meta_title".$lang,$info["meta_title".$lang]),"class" =>"span" ));
echo form_error("meta_title".$lang,"<span class='text-error'>","</span>");
echo br();
?></div><div class="span6"><?php
//TITLE URL SECTION.
echo form_label('<b>TITLE URL&nbsp;<em></em>:</b>', 'TITLE URL');
echo form_input(array("name" => "title_url".$lang, "value" => set_value("title_url".$lang,$info["title_url".$lang]),"class" =>"span" ));
echo form_error("title_url".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div></div>
<div class="row-fluid"><div class="span6">
<?php
//META DESCRIPTION SECTION.
echo form_label('<b>META DESCRIPTION&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META DESCRIPTION');
echo form_textarea(array("name" => "meta_description".$lang, "value" => set_value("meta_description".$lang,$info["meta_description".$lang]),"class" =>"span","rows" => 3, "cols" =>100));
echo form_error("meta_description".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div><div class="span6">
<?php
//META KEYWORDS SECTION.
echo form_label('<b>META KEYWORDS&nbsp;<small class="blue">(Max 160 Characters)</small>:</b>', 'META KEYWORDS');
echo form_textarea(array("name" => "meta_keywords".$lang, "value" => set_value("meta_keywords".$lang,$info["meta_keywords".$lang]),"class" =>"span","rows" => 3, "cols" =>100 ));
echo form_error("meta_keywords".$lang,"<span class='text-error'>","</span>");
echo br();
?>
</div></div><?php
echo form_fieldset_close();
echo br();
if ($this->uri->segment(3) != "view" ){
echo '<p class="pull-right">';
echo form_submit(array('name' => 'submit','value' => 'Save Changes','class' => 'btn btn-primary') );
echo '</p>';
}

echo form_close();
?>
</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>