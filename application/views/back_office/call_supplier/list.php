<script type="text/javascript" src="<?= base_url(); ?>js/jquery.tablednd_0_5.js"></script>
<script>
function delete_row(id){
window.location="<?= site_url("back_office/call_supplier/delete"); ?>/"+id;
return false;
}

$(function(){
$("#table-1").tableDnD({
onDrop: function(table, row) {
var ser=$.tableDnD.serialize();
$("#result").load("<?= site_url("back_office/call_supplier/sorted"); ?>"+"?"+ser);
}
});

$("#match2 input[name='search']").live('keyup', function(e){
e.preventDefault();
var id =this.id;
$('#match2 tbody tr').css('display','none');
var searchtxt = $.trim($(this).val());
var bigsearchtxt = searchtxt.toUpperCase(); 
var smallsearchtxt = searchtxt.toLowerCase();
var fbigsearchtxt = searchtxt.toLowerCase().replace(/\b[a-z]/g, function(letter) {
return letter.toUpperCase();
});
if(searchtxt == ""){
$('#match2 tbody tr').css('display',"");	
} else {
$('#match2 tbody tr td.'+id+':contains("'+searchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+bigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+fbigsearchtxt+'")').parent().css('display',"");
$('#match2 tbody tr td.'+id+':contains("'+smallsearchtxt+'")').parent().css('display',"");
}
});

$("#show_items").change(function(){
	var val = $(this).val();
	$("#result").html(val);
	$("#show_items").submit();
});

});
</script><?php
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$sego =$this->uri->segment(4);
$gallery_status="0";
?>
<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>

<div class="span10">
<div class="span10-fluid" >
<ul class="breadcrumb">
<li class="active"><?= $title; ?></li>
<?php /*?><? if ($this->acl->has_permission('call_supplier','add')){ ?>
<li class="pull-right">
<a href="<?= site_url('back_office/call_supplier/add'); ?>" id="topLink" class="btn btn-info top_btn" title="">Add call supplier</a>
</li><? } ?><?php */?>
</ul> 
</div>
<div class="hundred pull-left" id="match2">   
<div id="result"></div> <div class="row-fluid sortable">
          <div class="box span12">
            <div class="box-header well">
              <h2><i class="icon-search"></i> Filter by</h2>
              <div class="box-icon"><a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a><a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a></div>
            </div>
            <div class="box-content">
              <div class="sortable row-fluid">
            <form method="get" action="<?=site_url("back_office/call_supplier")?>">
<!--Product ID-->
<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;"><?php echo 'Brand';?><br />
<?php 

$brands=$this->fct->getAll('brands','title'); ?>
<select name="brand" id="brand">
<option value="">- All -</option>
<?php foreach($brands as $val) {
	$cl = '';
	if(isset($_GET['brand']) && $_GET['brand'] == $val['id_brands'])
	$cl = 'selected="selected"';
?>
<option value="<?php echo $val['id_brands']; ?>" <?php echo $cl; ?>><?php echo $val['title']; ?></option>
<?php }?>
</select></div>


<div class="fl" style="margin:0 15px 0 0;width: 200px; float:left;margin-right:38px;">User<br />
<?php 
$cond['user.status'] =  1;
$cond['user.completed'] =  1;

$users=$this->fct->getAll_cond('user','sort_order',$cond); ?>
<select name="user" id="user">
<option value="">- All -</option>
<?php foreach($users as $val) {
	$cl = '';
	if(isset($_GET['user']) && $_GET['user'] == $val['id_user'])
	$cl = 'selected="selected"';
?>
<option value="<?php echo $val['id_user']; ?>" <?php echo $cl; ?>><?php echo $val['name']; ?></option>
<?php }?>
</select></div>







<input type="hidden" name="section" value="uploadProducts" />
<div class="fl" style="margin:21px 0 0 0; float:left">
<input type="submit" class="btn btn-primary" name="report_submit" value="Filter" />

</div>
</form>
              </div>
            </div>
          </div>
        </div><? 
$attributes = array('name' => 'list_form');
echo form_open_multipart('back_office/call_supplier/delete_all', $attributes); 
?>  		
<table class="table table-striped" id="table-1">
<thead>
<? if($this->session->userdata("success_message")){ ?>
<tr><td colspan="3" align="center" style="text-align:center">
<div class="alert alert-success">
<?= $this->session->userdata("success_message"); ?>
</div>
</td>
<tr>
<? } ?>
<tr>
<td width="2%">&nbsp;</td>

<th >User</th>
<th >Brand</th>
<th >Created Date</th>
<th style="text-align:center;" width="250">ACTION</th></tr>

</thead><tfoot><tr>
<td class="col-chk"><input type="checkbox" id="checkAllAuto" /></td>
<td colspan="8">
<? if ($this->acl->has_permission('call_supplier','delete_all')){ ?>
<div class="pull-right">
<select class="form-select" name="check_option">
<option value="option1">Bulk Options</option>
<option value="delete_all">Delete All</option>
</select>
<a class="btn btn-primary btn_mrg" onclick="document.forms['list_form'].submit();" style="cursor:pointer;">
<span>perform action</span>
</a>
</div> 
<? } ?></td>
</tr>
</tfoot>
<tbody>
<? 
if(count($info) > 0){
$i=0;
foreach($info as $val){
$i++; 
?>
<tr id="<?=$val["id_call_supplier"]; ?>">
<td class="col-chk"><input type="checkbox" name="cehcklist[]" value="<?= $val["id_call_supplier"] ; ?>" /></td>
<td >
<?php echo $this->fct->getonecell('user','username',array('id_user'=>$val["id_user"]));?>
</td>
<td >
<?php echo $this->fct->getonecell('brands','title',array('id_brands'=>$val["id_brands"]));?>
</td>
<td >
<?=$val["created_date"]; ?>
</td>
<td style="text-align:center">

<? if ($this->acl->has_permission('call_supplier','delete')){ ?>
<a onclick="if(confirm('Are you sure you want delete this item ?')){ delete_row('<?=$val["id_call_supplier"];?>'); }" class="table-delete-link cur" title="Delete" >
<i class="icon-remove-sign" ></i> Delete</a>
<? } ?>
</td>
</tr>
<? }  } else { ?>
<tr class='odd'><td colspan="7" style='text-align:center;'>No records available . </td></tr>
<?  } ?>
</tbody>
</table>  	
<? echo form_close();  ?>
<div class="pagination_container">
<div class="span2 pull-left">
<? $search_array = array("25","100","200","500","1000","All"); ?>
<form action="<?= site_url("back_office/call_supplier"); ?>" method="post"  id="show_items">
Show Items&nbsp;
<select name="show_items"  class="input-mini">
<? for($i =0 ; $i < count($search_array); $i++){ ?>
<option value="<?= $search_array[$i]; ?>" <? if($show_items == $search_array[$i]) echo 'selected="selected"'; ?>><?= ($search_array[$i] == "") ? 'All' : $search_array[$i]; ?></option>
<? } ?>
</select>
</form>
</div>
<? echo $this->pagination->create_links(); ?>
</div>
</div>
</div>
</div>   
</div>
<? 
$this->session->unset_userdata("success_message"); 
$this->session->unset_userdata("error_message"); 
?> 