<div class="container-fluid">
<div class="row-fluid">
<div class="span2">
<? $this->load->view("back_office/includes/left_box"); ?>
</div>
<div class="span10" >
<div class="span10-fluid" >
<?
if(isset($breadcrumbs)) {
	echo $breadcrumbs;
}
else {
$ul = array(
            anchor('back_office/products','<b>List products</b>').'<span class="divider">/</span>',
            $title => array('li_attributes' => 'class = "active"', 'contents' => $title),
            );


$ul_attributes = array(
                    'class' => 'breadcrumb'
                    );
echo ul($ul, $ul_attributes);
}
?>
</div>
<div class="hundred pull-left">   
<?
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$attributes = array('class' => 'middle-forms');


if(isset($id)){
echo form_hidden('id', $id);
} else {
$info["description".$lang] = "";
$info["image"] = "";

$info["title".$lang] = "";
$info["meta_title".$lang] = "";
$info["meta_description".$lang] = "";
$info["meta_keywords".$lang] = "";
$info["title_url".$lang] = "";
}
echo br();
if($this->session->userdata("success_message")){ 

echo '<div class="alert alert-success">';
echo $this->session->userdata("success_message");
echo '</div>';
}
if($this->session->userdata("error_message")){
echo '<div class="alert alert-error">';
echo $this->session->userdata("error_message"); 
echo '</div>';
}
echo form_fieldset("");
if($this->config->item("language_module")) {
	$data['info'] = $info;
	$this->load->view('back_office/translation/add_view_language',$data);
}

?>
<h3>PHP: Parse and Retrieve Data from XLSX Files</h3>
<div id="datacontent">
<h4>Upload *.xlsx File:</h4>

<?php  if(isset($error)) {?>
	<?php echo '<span style="color:red">'.$error.'</span><br />'; ?>
<?php }?>
<?php if(isset($success)) {?>
	<?php echo '<span style="color:green>'.$success.'</span><br />'; ?>
<?php }?>
<form action="<?php echo site_url("back_office/excel_c/do_import"); ?>" accept-charset="utf-8" method="post" enctype="multipart/form-data">
<input type="hidden" name="section" value="<?php echo $this->input->get("section"); ?>" />
<p>File: <input type="file" name="tmp_name"  />
<input type="submit" class="btn btn-primary"  value="Parse" /></p>
</form>
</div>

</div>
</div>     
</div>
</div>
<? 
$this->session->unset_userdata("success_message");
$this->session->unset_userdata("error_message"); 
?>