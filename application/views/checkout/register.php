<?php
if(isset($userData) && !empty($userData)) {
	$buttonLabel = lang('update_profile');
	$formAction = site_url('user/updateProfile');
	$pTitle = lang('user_account_update_profile');
	$pStar = '';
	$formID = 'updateProfile';
	$menuBlock = "user/account_menu";
	$update=true;
}
else {
	$update=false;
	$userData['first_name'] = '';
	$userData['last_name'] = '';
	$userData['email'] = '';
	$userData['phone'] = '';
	$userData['company_name'] = '';
	$userData['company_email'] = '';
	$userData['company_website'] = '';
	$userData['mobile'] = '';
	$userData['gender'] = '';
	$userData['id_countries'] = '';
	$userData['state'] = '';
	$userData['city'] = '';
	$userData['country'] = '';
	$userData['address'] = '';
	$buttonLabel = lang('register');
	$pTitle = lang('create_an_account');
	$formAction = route_to('user/submit');
	$pStar = ' *';
	$formID = 'registration';
	$menuBlock = "user/user_menu";
}


?>



<div class="innerContent account-create">
  <div class="centered"> 
 <div class="page-title">
<h1>Create an Account</h1>
</div>
<h2 class="legend">Personal Information</h2>
  <form id="<?php echo $formID;?>" method="post" accept-charset="utf-8" action="<?php echo $formAction ;?>" >
<!--ROW 1-->
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" for="email"> First Name <em>*</em> </label>
          <input class="input-text" type="text" name="first_name" >
          <span id="first_name-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" for="email"> Last Name <em>*</em> </label>
          <input class="input-text" type="text" name="last_name" >
          <span id="last_name-error" class="form-error"></span> </div>               
</div>

<!--ROW 2-->
<div class="row_form">
<div class="form-item">
          <label class="required" for="email"> Email Address <em>*</em> </label>
          <input class="input-text" type="text" name="email" >
          <span id="email-error" class="form-error"></span> </div>
               
</div>

<!--ROW 3-->
<div class="row_form">
<div class="form-item mod">
<input id="is_subscribed" class="checkbox validation-passed" type="checkbox" value="1" title="Sign Up for Newsletter" name="is_subscribed">
<label >  Sign Up for Newsletter</label>


<span id="is_subscribed-error" class="form-error"></span></div>
               
</div>

<!--ROW 4-->
<div class="row_form">
<div class="form-item customer-dob">
<label for="month">Date of Birth</label>
<div class="dob-month">
<input id="month" class="input-text" type="text" title="Month" value="" name="month">
<div id="advice-validate-custom-month" class="validation-advice" style=""> </div>
<label  class="lbl_date">MM</label>
</div>

<div class="dob-day">
<input id="day" class="input-text" type="text" title="Day" value="" name="day">
<label class="lbl_date">DD</label>
</div>

<div class="dob-year">
<input id="year" class="input-text validate-custom" type="text" title="Year" value="" name="year" autocomplete="off">
<label class="lbl_date">YYYY</label>
</div>

<span id="month-error" class="form-error"></span>
</div>
               
</div>


<!--Login Information-->
<h2 class="legend">Login Information</h2>
<!--ROW 5-->
<div class="row_form">
<div class="form-item gutter-right">
          <label class="required" for="password">Password <em>*</em> </label>
          <input class="input-text" type="password" name="password" >
          <span id="password-error" class="form-error"></span> </div>
<div class="form-item gutter-right">
          <label class="required" for="email">Confirm Password <em>*</em> </label>
          <input class="input-text" type="password" name="confirm_password" >
          <span id="confirm_password-error" class="form-error"></span> </div>               
</div>

<!--ROW 6-->
<div class="row_form">
<div class="form-item">
          <label class="required" for="password">Please type the letters below <em>*</em> </label>
          <input class="input-text" type="password" name="country" >
          <span id="country-error" class="form-error"></span> </div>               
</div>

<label class="lbl_required mod">* Required Fields</label>
  
<div class="buttons-set">
  <div id="loader-bx"></div>
      <div class="FormResult"> </div>
          <input class="button" title="Create an Account" type="submit" value="Submit">
          <a class="back-link" href="<?php echo route_to('user/login');?>" >
          <i class="fa fa-fw"> </i>
          Back</a> </div>  
    
 </form>
  
  </div>
</div>
