<?php $login = checkUserIfLogin(); ?>
<?php

$cart_items = $this->cart->contents();
if (empty($cart_items))
    $cart_items = $this->ecommerce_model->loadUserCart();
$cartPrices['total_redeem_miles'] = 0;

$cartPrices = $this->ecommerce_model->getCheckoutAttr($supplier);
if ($miles == 1) {
    $cart_part = 'miles';
} else {
    $cart_part = 'products';
}

$cond['ids_brands'] = $ids_brands;
$products = $this->ecommerce_model->getCartProducts($cart_items, $cart_part, $cond);

//////////////////////MILES///////////////////
$selected_redeem = $this->session->userdata('selected_redeem');
$product_quantities = $this->session->userdata('product_quantities');
$user = $this->ecommerce_model->getUserInfo();

if (empty($selected_redeem)) {
    $selected_redeem = array();
}
if (empty($product_quantities)) {
    $product_quantities = array();
}

$user_miles = 0;
if ($login) {
    $user = $this->ecommerce_model->getUserInfo();
    $user_miles = $user['miles'] - $cartPrices['total_redeem_miles'];
}

if ($miles == 1) {
}
?>
<?php if (!empty($products)) { ?>
    <?php if ($cart_part == 'miles') {
        ?>
        <label class="cart-name">Rewards in Cart</label>
    <?php } else { ?>
        <!--<label class="cart-name">Product(s) in Cart</label>  -->
    <?php } ?>

    <?php
    $i = 0;
    foreach ($products as $product) {
        $i++;
        /////////////////////////////PRICE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        $list_price = $product['product_info']['list_price'];
        $product_qty = $product['qty'];
        if (in_array($product['rowid'], $selected_redeem)) {
            $qty = $product_qty - $product_quantities[$product['rowid']];
        }
        $total_price = $list_price * $product['qty'];
        $total_discount_price = $product['price'] * $product['qty'];

        /////////////////////////////MILES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        $redeem_miles = $product['redeem_miles'] * $product_qty;
        $miles = $product['miles'] * $product_qty;
        $bool = false;
        if (in_array($product['rowid'], $selected_redeem)) {
            $bool = true;
        }

        ?>
        <input type="hidden" name="product_id[]" value="<?php echo $product['product_info']['id_products']; ?>"/>
        <input type="hidden" name="product_rowid[]" value="<?php echo $product['rowid']; ?>"/>
        <?php if ($i % 2 == 0) {
            $row = "even";
        } else {
            $row = "odd";
        } ?>
        <?php $product_url = route_to('products/details/' . $product['product_info']['title_url']); ?>
        <?php if ($$cart_part == "miles") {
            $product_url = route_to('products/details/' . $product['product_info']['title_url']) . '?miles=1';
        } ?>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <a class="product-image" title="<?php echo $product['product_info']['title']; ?>"
                       href="<?php echo $product_url; ?>">
                        <?php if (isset($product['product_info']['gallery'][0]['image'])) { ?>
                            <img alt="<?php echo $product['product_info']['title']; ?>"
                                 src="<?php echo base_url(); ?>uploads/products/gallery/295x295/<?php echo $product['product_info']['gallery'][0]['image']; ?>"/>
                        <?php } else { ?>
                            <img src="<?php echo base_url(); ?>front/img/default_product.png">
                        <?php } ?>
                    </a>
                </div>
                <div class="col-md-9">
                    <h4><a title="<?php echo $product['product_info']['title']; ?>"
                           href="<?php echo $product_url; ?>"><?php echo $product['product_info']['title']; ?></a></h4>
                    <p><?php echo $product['qty']; ?> * <?php echo changeCurrency($product['price']); ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-md-3">
        </div>
        <div class="cart-right-column col-md-9">
            <div class="totals grid-full alpha omega">
                <div class="totals-inner" style="text-align:right;">
                    <p>
                        <?php if ($cartPrices['total_tax_price'] != 0) { ?>
                            <label class="total_supplier_price" style="float: none;">Tax 5%</label>
                            <?php echo changeCurrency($cartPrices['total_tax_price']); ?>
                        <?php } ?>
                    </p>
                    <p style="display:none">
                        <label class="total_supplier_price" style="float: none;">Subtotal</label>
                        <?php echo changeCurrency($cartPrices['net_discount_price']); ?>
                    </p>
                    <?php
                    ///////// Continue for Shopping//////////
                    $last_brand_field = count($ids_brands) - 1;
                    if (isset($ids_brands[$last_brand_field])) {
                        $brand = $this->fct->getonerow('brands', array('id_brands' => $ids_brands[$last_brand_field]));
                        $url = route_to('brands/details/' . $brand['id_brands']);
                    } else {
                        $url = site_url('');
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>