<div class="innerContent newsletter_cont">
  <div class="page-title border">
    <h1><i class="fa fa-fw"></i>Newsletter</h1>
  </div>
  <form id="NewsLetterForm"  method="post" accept-charset="utf-8" action="<?php echo route_to('user/add_newsletter') ;?>">
    <div class="form-item">
      <label class="required" for="email"> Email Address <em>*</em> </label>
      <input class="textbox" type="text" name="email_newsletter"  >
      <div id="email_newsletter-error" class="form-error"></div>
      <div id="loader-bx"></div>
    </div>
    <div class="buttons-set">
      <div class="FormResult"></div>
      <input type="submit" title="Login" value="<?php echo lang('submit');?>" class="btn">
    </div>
  </form>
</div>
