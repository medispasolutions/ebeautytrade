

<?php $categories2=$this->ecommerce_model->getCategoriesByBrand(array('id_brands'=>$brand['id_brands'],'id_parent'=>0));?>

<div class="form-item">
<select class="selectbox" name="categories[]" onchange="loadSubModuleBrand('<?php echo route_to('brands/loadSearchBrand');?>','categories','sub_categories',this)">
 <option value="">Select Category</option>
<?php foreach($categories2 as $val){?>
	<option  <?php if(isset($selected_categories) && in_array($val['id_categories'],$selected_categories)) echo "selected"; ?> value="<?php echo $val['id_categories'];?>"><?php echo $val['title'];?></option>
<?php }?>
</select>
</div>

<?php 
	if(isset($parent_levels) && !empty($parent_levels)){
	 foreach($parent_levels as $val){?>
  
    <?php $sub_levels=$this->fct->getAll_cond('categories','sort_order asc',array('id_parent'=>$val['id_categories']));?>
    <?php if(!empty($sub_levels)){?>
    <div class="form-item">
    
      <select class="selectbox" name="categories[]" onchange="loadSubModuleBrand('<?php echo route_to('brands/loadSearchBrand');?>','categories','sub_categories',this,<?php echo $val['id_categories'];?>)">
        <option value="">Select All</option>
        <?php foreach($sub_levels as $val){?>
        <option <?php if(isset($selected_categories) && in_array($val['id_categories'],$selected_categories)) echo "selected"; ?> value="<?php echo $val['id_categories'];?>"><?php echo $val['title'.$lang];?></option>
        <?php } ?>
      </select>
    </div>
    <?php } ?>

     <?php } ?> 
<?php } ?>