<?php $attributes=$product['attributes'];?>
<div class="productsAttribuesOptions <?php if(isset($list)) echo "optionsListProducts";?>">
  <div class="page-title border">
    <h1> Add Product: <?php echo $product['title'];?> </h1>
  </div>

   <?php if(isset($list)){?>
   <?php $this->load->view('load/productAttribuesOptions');?>
   <?php } ?>
  
  <div class="note">Choose Categories</div>
  
  <!--OPTIONS-->
  <input type="hidden" id="popup_cls" value="stocklist_categories_p" />
  <div class="row" id="stocklist_categories_l">
  <?php if(!empty($info)){?>
    <?php foreach($info as $val){?>
    <div class="stockList_row">
      <label><span>
        <input type="checkbox" name="stocklist[]" class="stocklist_category"  value="<?php echo $val['id_checklist_categories'];?>"/>
        <?php echo $val['title'];?></span></label>
    </div>
    <?php } ?>
    <?php }else{ ?>
    <div class="empty" style="text-align:left;">No categories found</div>
    <?php } ?>
  </div>
  <div class="buttons-set ta-r">
    <div id="loader-bx"></div>
    <div class="FormResult "></div>
    
    <!--<input class="btn" type="submit" value="Submit" title="Submit">--> 
    <a class="btn" title="Add New Category" onclick="addNewCategory()" style="margin-right:10px;">Add New Category</a> <a class="btn" title="<?php echo lang('submit');?>" onclick="addToStockList()"><?php echo lang('submit');?></a>
    <div class="addNewCategory_p">
      <form id="updatePopUpForm2" method="post" accept-charset="utf-8" action="<?php echo site_url('user/submitCategory');?>" >
        <div class="FormResult"> </div>
        <div class="row" id="addNewCategory_pd" style="display:none;">
          <input type="hidden" name="product_stocklist" value="1" />
          <div class="page-title border">
            <h1> Add New Category</h1>
          </div>
          <div class="my-account "> 
            
            <!--ROW 1-->
            <div class="row_form">
              <div class="form-item gutter-right">
                <label class="required" >Name <em>*</em></label>
                <input class="textbox" type="text" name="title" value="" >
                <span id="title-error" class="form-error"></span> </div>
            </div>
            
            <!--<label class="lbl_required mod">* Required Fields</label>-->
            <div class="buttons-set">
              <div id="loader-bx"></div>
              <input class="btn" type="submit" value="Save" title="Save">
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php if(isset($list) || isset($details)){ ?>
<form id="favorites-form" action="<?php echo site_url('products/add_to_favorite');?>" onsubmit="return validateForm()"  method="post">
<div  style="display:none;">
<input  type="hidden" class="qty-frm" name="qty" value="1" />
<input  type="hidden"   name="popup_favorites" value="1" />
   <?php foreach($attributes as $attr){
	   if(!empty($attr['options'])){?>
	   <input type="hidden"  name="options[]"  id="fav_attr_input_<?php echo $attr['id_attributes'];?>"   />
       <?php  } ?>
      <?php  } ?>
      <input type="hidden" name="product_id" value="<?php echo $product['id_products'] ;?>" /> 
      <input type="hidden" name="categories" id="categories_stocklist" value="" />
      </div>
      <div class="FormResult productsAttribuesOptions"></div>
</form>
<?php } ?>

<script>$("#favorites-form").dowformvalidate();</script>
