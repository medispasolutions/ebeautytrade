<?php $login = checkUserIfLogin(); ?>
<?php

$cart_items = $this->cart->contents();
if (empty($cart_items))
    $cart_items = $this->ecommerce_model->loadUserCart();
$cartPrices['total_redeem_miles'] = 0;

$cartPrices = $this->ecommerce_model->getCheckoutAttr($supplier);
if ($miles == 1) {
    $cart_part = 'miles';
} else {
    $cart_part = 'products';
}
$cond['ids_brands'] = $ids_brands;

$products = $this->ecommerce_model->getCartProducts($cart_items, $cart_part, $cond);

//////////////////////MILES///////////////////
$selected_redeem = $this->session->userdata('selected_redeem');
$product_quantities = $this->session->userdata('product_quantities');
$user = $this->ecommerce_model->getUserInfo();

if (empty($selected_redeem)) {
    $selected_redeem = array();
}
if (empty($product_quantities)) {
    $product_quantities = array();
}

$user_miles = 0;
if ($login) {
    $user = $this->ecommerce_model->getUserInfo();
    $user_miles = $user['miles'] - $cartPrices['total_redeem_miles'];
}


if ($miles == 1) {
}
?>

<?php if (!empty($products)) { ?>
    <?php if ($cart_part == 'miles') {

        ?>
        <label class="cart-name">Rewards in Cart</label>

    <?php } else { ?>
        <!--<label class="cart-name">Product(s) in Cart</label>  -->
    <?php } ?>
    <table class="data-table cart-table shopping-cart-table mod" border=0>
        <tr class="first last">

            <th colspan="2" class="product_cart_name"><span class="nobr">Item Description </span></th>
            <?php /*?>            <?php if($section == 'checkout') {?>
            <th style="text-align:center;">Redeem Miles</th>
            <?php } ?><?php */ ?>
            <th class="ta-c">SKU</th>
            <?php if ($login) { ?>
                <th class="ta-c product_cart_price">
                    <?php if ($cart_part == 'miles') {
                        echo "Miles";
                    } else {
                        echo lang('cart_product_price');
                    } ?>
                </th>
            <?php } ?>
            <!--<th>Delivery Date</th>-->

            <th class="ta-c product_cart_qty" rowspan="1">Quantity</th>
            <?php if ($login) { ?>
                <th class="ta-c product_sub_total" colspan="1">Sub-Total</th>
            <?php } ?>
            <?php if ($section == 'cart') { ?>
                <th class="col-delete product_delete ta-c" rowspan="1"></th>
            <?php } ?>
        </tr>
        <?php
        $i = 0;

        foreach ($products as $product) {

            $brandID[] =  $product['product_info']['id_brands'];
            $i++;
            /////////////////////////////PRICE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\	 
            $list_price = $product['product_info']['list_price'];

            $product_qty = $product['qty'];

            if (in_array($product['rowid'], $selected_redeem)) {
                $qty = $product_qty - $product_quantities[$product['rowid']];
            }
            $total_price = $list_price * $product['qty'];
            $total_discount_price = $product['price'] * $product['qty'];

            /////////////////////////////MILES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            $redeem_miles = $product['redeem_miles'] * $product_qty;
            $miles = $product['miles'] * $product_qty;
            $bool = false;
            if (in_array($product['rowid'], $selected_redeem)) {
                $bool = true;
            }


            ?>
            <input type="hidden" name="product_id[]" value="<?php echo $product['product_info']['id_products']; ?>"/>
            <input type="hidden" name="product_rowid[]" value="<?php echo $product['rowid']; ?>"/>
            <?php if ($i % 2 == 0) {
                $row = "even";
            } else {
                $row = "odd";
            } ?>
            <?php $product_url = route_to('products/details/' . $product['product_info']['title_url']); ?>
            <?php if ($$cart_part == "miles") {
                $product_url = route_to('products/details/' . $product['product_info']['title_url']) . '?miles=1';
            } ?>
            <tr class="first <?php echo $row; ?>" id="row-<?php echo $product['rowid']; ?>">
                <td class="product-image-td"><a class="product-image"
                                                title="<?php echo $product['product_info']['title']; ?>"
                                                href="<?php echo $product_url; ?>">
                        <?php if (isset($product['product_info']['gallery'][0]['image'])) { ?>
                            <img alt="<?php echo $product['product_info']['title']; ?>"
                                 src="<?php echo base_url(); ?>uploads/products/gallery/295x295/<?php echo $product['product_info']['gallery'][0]['image']; ?>"/>
                        <?php } else { ?>
                            <img src="<?php echo base_url(); ?>front/img/default_product.png">
                        <?php } ?>
                    </a></td>


                <td>
                    <div class="note">
                        <?php if ($product['product_info']['set_as_non_exportable'] == 1) {
                            $exportable = true; ?>
                            This product is non-exportable
                        <?php } ?>
                    </div>
                    <h2 class="product-name"><a title="<?php echo $product['product_info']['title']; ?>"
                                                href="<?php echo $product_url; ?>"><?php echo $product['product_info']['title']; ?></a>
                    </h2>
                    <dl class="item-options">
                        <?php
                        $options = "";
                        if (isset($product['cart_options']) && !empty($product['cart_options'])) {
                            $options = $product['cart_options'];
                        } ?>
                        <?php if (!empty($options)) {
                            $cart_options = $options;
                            $i = 0;
                            $c = count($cart_options);
                            foreach ($cart_options as $key => $opt) {
                                if (isset($opt['id_attributes'])) {
                                    $attribute = $this->fct->getonerecord('attributes', array('id_attributes' => $opt['id_attributes'])); ?>
                                    <dt><i class="fa fa-fw"></i><?php echo $attribute['title']; ?></dt>
                                    <dd><?php echo $opt['title']; ?> </dd>
                                <?php }
                            }
                        } ?>
                    </dl>
                    <?php if ($miles && $cart_part != "miles") { ?>
                        <div class="earned">
                            <span><?php echo $miles; ?></span>
                            <i class="icon">
                                <img src="<?php echo base_url(); ?>front/img/miles.png">
                            </i>earned.
                        </div>
                    <?php } ?>
                    <?php if ($product['product_info']['exclude_from_shipping_calculation'] == 1) { ?>
                        <!--<div class="deliver_day"><i class="icon"><img src="<?php echo base_url(); ?>front/img/free_shipping-128.png"></i>Free Shipping</div>-->
                    <?php } ?>
                </td>
                <td class="ta-c"><?php echo $product['sku']; ?></td>
                <?php /*?>
                <?php if($section == 'checkout') {?>
                <td style="text-align:center;">
                <?php if($redeem_miles!=0 && $user_miles>$redeem_miles){?>
                <input type="checkbox"  class="miles_redeem" style="display:none;" id="miles_redeem_<?php echo $product['rowid']; ?>"  name="miles_redeem[<?php echo $product['rowid']; ?>]" value="<?php echo $redeem_miles;?>" <?php if($bool) echo "checked"; ?> />
                <input type="hidden"  class="check_redeem" value="<?php if($bool) {?>1<?php }else{?>0<?php } ?>" />
                <?php if($bool){?>
                <img class="btn_miles" src="<?php echo base_url();?>front/img/removed_miles.png" />
                <?php }else{?>
                <img class="btn_miles" src="<?php echo base_url();?>front/img/add_miles.png" />
                <?php } ?>
                <?php }else{?> - <?php } ?>
                <span class="loader_m"></span></td>
                <?php } ?><?php */ ?>
                <?php if ($login) { ?>
                    <td class="ta-c">
                        <?php if ($cart_part == 'miles') {
                            echo $product['redeem_miles'];
                        } else {
                            echo changeCurrency($product['price']);
                        } ?></td>
                <?php } ?>
                <td class="ta-c"><span class="cell-label">Qty</span>
                    <?php if ($section == 'cart') { ?>
                        <input class="textbox qty updateQty_cart" maxlength="12" title="Qty" size="4"
                               name="product_quantity[]" value="<?php echo $product['qty']; ?>">
                    <?php } else { ?>
                        <?php echo $product['qty']; ?>
                    <?php } ?></td>
                <?php if ($login) { ?>
                    <td class="col-total  ta-c">
                        <?php if ($cart_part == 'miles') {
                            echo $redeem_miles;
                        } else {
                            echo changeCurrency($total_discount_price);
                        } ?></td>
                <?php } ?>
                <?php if ($section == 'cart') { ?>
                    <td class="col-delete a-center last" id="delete_<?php echo $product['rowid']; ?>">
                        <!--<a title="Edit item parameters" class="btn-edit" href="<?php echo route_to('products/details/' . $product['rowid'] . '/configure/update'); ?>"><i class="fa fa-fw"></i></a> <span class="divider">|</span> -->
                        <a class="btn-remove btn-remove2" onclick="removeItem('<?php echo $product['rowid']; ?>')"
                           title="Remove item"><!--<i class="fa fa-fw"></i>--> Remove</a></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr class="last-result">
            <td colspan="8"></td>
        </tr>
        <tr>
            <td></td>
            <td><p>Special Instructions</p>
                <textarea class="form-control" name="comments" rows="2"></textarea>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr class="last-result" style="margin: 0 0 5px">
            <td colspan="4"></td>
            <td style="text-align: center;border-top: 1px solid #efefef;"><label class="total_supplier_price" style="float: none;">Promocode</label></td>
  
            <td class="col-total ta-c" style="border-top: 1px solid #efefef; padding: 6px;font-size: 16px;">
                <div class="row_form">
                    <div class="form-item third">
                        <input type="text" id="promocode" name="promocode">
                    </div>
                </div>
            </td>
                <?php
                $i = 0;
                foreach( $brandID as $v ) { 
                $i++;
                ?>
                <input type="hidden" name="brandid<?php echo $i ?>" value="<?php echo $v; ?>">
                <?php } ?>
                <input type="hidden" name="brand-qty" value="<?php echo $i; ?>">
                
            <td style="border-top: 1px solid #efefef;padding: 6px 0;">
                <div class="form-item full">
                    <input type="submit" id="submitPromocode" value="Send" class="btn">
                </div>
            </td>
        </tr>
    </table>
    <table class="data-table cart-table shopping-cart-table mod" border=0>
        <tr class="last-result" style="text-align: center;border-top: 1px solid #efefef;border-bottom: 1px solid #efefef;">
            <td style="padding:6px" colspan="2"></td>
            <td style="padding:6px">Deliver Time: <?php echo $supplier['delivery_time']; ?></td>
            <td style="padding:6px">Deliver fee: 
                <?php if($supplier['delivery_fee_price']){ 
                    echo $supplier['delivery_fee_price']; ?></td>
                <?php } else { ?>
                    Free
                <?php } ?>
        <?php if($cartPrices['total_tax_price'] != 0){ ?>
            <td style="padding:6px;text-align: right;"><label class="total_supplier_price" style="float: none;">Tax 5% </label>
                <?php echo changeCurrency($cartPrices['total_tax_price']); ?>
            </td>
        <?php } else { ?>
            <td style="padding:6px"></td>
        <?php }?>
            <td style="padding:6px;text-align: right;"><label class="total_supplier_price" style="float: none;">Subtotal  </label> <?php echo changeCurrency($cartPrices['net_discount_price']); ?>
            </td>
        </tr>
        <tr> 
            <td style="padding:6px" colspan="4"></td>
            <td style="text-align: right;">
                <a class="" href="<?php echo $url; ?>" title="Continue Shopping" type="button"> <i style="display:none" class="fa fa-fw"></i>Continue Shopping</a>
            </td>
            <td style="text-align:right;">

                <?php
                ///////// Continue for Shopping//////////
                $last_brand_field = count($ids_brands) - 1;
                if (isset($ids_brands[$last_brand_field])) {
                    $brand = $this->fct->getonerow('brands', array('id_brands' => $ids_brands[$last_brand_field]));
                    $url = route_to('brands/details/' . $brand['id_brands']);
                } else {
                    $url = site_url('');
                }
                ?>

                <a class="btn" type="button" title="Submit Order" onclick="sumbitOrder(<?php echo $supplier['id_user']; ?>)"> <i style="display:none" class="fa fa-fw"></i> Checkout</a>
            </td>
        </tr>
    </table>
        <input type="hidden" name="action_cart" id="action_cart" value=""/>
        <input type="hidden" name="id_supplier" id="id_supplier" value="<?php echo $supplier['id_user']; ?>"/>
<?php } ?>