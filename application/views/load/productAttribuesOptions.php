<?php
 ///////////////////////////Attributes\\\\\\\\\\\\\\\\\\\
$attributes=$product['attributes'];
$gallery=$product['gallery'];
$brand=$product['brand'];

$informations=$product['informations'];
 ///////////////////////////PRICES\\\\\\\\\\\\\\\\\\\
	$first_available_stock = array();
	if(isset($product['first_available_stock']) && !empty($product['first_available_stock'])) {
		$first_available_stock = $product['first_available_stock'];
	}
	if(isset($first_available_stock) && !empty($first_available_stock)) {
		$quantity = $first_available_stock['quantity'];
		$list_price = $first_available_stock['list_price'];
		$price = $first_available_stock['price'];
		$discount = $first_available_stock['discount'];
		$discount_expiration = $first_available_stock['discount_expiration'];
		$stock_options = unSerializeStock($first_available_stock['combination']);
		$hide_price = $first_available_stock['hide_price'];
		$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$first_available_stock['id_products'],"id_stock"=>$first_available_stock['id_products_stock']));
	}
	else {
	
		$quantity = $product['quantity'];
		//echo $quantity;exit;
		$list_price = $product['list_price'];
		$price = $product['price'];
		$discount = $product['discount'];
		$discount_expiration = $product['discount_expiration'];
		$hide_price = $product['hide_price'];
		$price_segments = $this->fct->getAll_cond("product_price_segments",'sort_order',array("id_products"=>$product['id_products']));
	}

?>



<div class="<?php  if(!isset($list)){?> productsAttribuesOptions <?php } ?>">
<?php  if(!isset($list)){?>
<div class="page-title border">
<h1>
<?php echo $product['title'];?>
</h1>
</div>
<?php } ?>
<?php $attributes=$product['attributes'];?>
<?php if(!empty($attributes)){?>
<div class="note"> Plz choose product options. </div>
<?php } ?>
<?php  if(!isset($list)){?>
<form id="cart-form" action="<?php echo route_to('cart/add');?>" onsubmit="return validateForm('true','#cart-form')" method="post"> 
  <?php } ?>
  <input type="hidden" name="product_id" value="<?php echo $product['id_products'] ;?>" /> 
  <input type="hidden"  id="id_product" value="<?php echo $product['id_products'] ;?>" /> 
  <input type="hidden"  id="redeem" name="redeem" value="<?php if(isset($redeem)) echo $redeem;?>" /> 
<!--OPTIONS--> 

 <?php if(!isset($list)){?>
<div class="products_details_2">
<label ><span class="lbl_t">Price: </span>  
<div class="p-price" id="p-price-<?php echo $product['id_products']; ?>">
 <?php 
 ///////////////////////////checkoptions\\\\\\\\\\\\\\\\\\\
$bool_option=false;
if(!empty($first_available_stock)){
	$bool_option=true;}	
?>
        
                      
   
<?php if(isset($redeem) && $redeem==1){ ?>  
  <?php if($bool_option) { echo '<span class="price">'.lang('miles_based_on_selection').'</span>'; }else{?>

<span class="price new_price"><?php echo $product['miles'].' miles'; ?></span>
			<?php		   }}else{ ?>
<?php if($bool_option) { echo '<span class="price">'.lang('based_on_selection').'</span>'; }else{?>
<?php if($hide_price == 0) {?>

					   <?php if(displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price,$discount_expiration,$price)) {?>
                       <span class="price old_price" itemprop="price"><?php echo changeCurrency(displayWasCustomerPrice($list_price)); ?></span>
                       <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span>
					   <?php } else {?>
                       <span class="price new_price"><?php echo changeCurrency(displayCustomerPrice($list_price,$discount_expiration,$price)); ?></span>
                       <?php }?> 
                       <?php } }?>
   <?php } ?>                 
   </div>        
</label>

</div>
<?php } ?>
<div class="container_attributes">   
<?php 
 ///////////////////////////Attributes\\\\\\\\\\\\\\\\\\\

foreach($attributes as $attr){
	$bool=true;
	if($attr['id_attributes']==1){
	$bool=false;
	$sizes=$attr['options'];
	if(!empty($sizes)){ ?>
     <div class="option_container">
    <div class="options_list">
<dl class="product-options-dl">
<dt id="color_option" class="product-options-dt">
<label class="required">
Pick Size
<em>*</em>
</label>
</dt>
<dd class="product-options-dd">
<div class="form-item mod" id="attr_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>">
     <?php foreach($sizes as $size){
		 ;?>
<a class="op op_size" title="<?php echo $size['title'];?>" alt="<?php echo $size['title'];?>"  >
<?php echo $size['title'];?>
<input type="hidden" value="<?php echo $size['id_attribute_options'];?>">
<input type="hidden" id="option_val_<?php echo $size['id_attribute_options'];?>"  value="<?php echo $size['option'];?>"/>
</a>
     <?php  } ?>

<input type="hidden"  name="options[]"  id="attr_input_<?php echo $attr['id_attributes'];?>" class="req_input" />
<div id="attr-error-<?php echo $attr['id_attributes'];?>" class="form-error mod">The <?php echo $attr['title'];?>  field is required.</div>
</div>
</dd>
</dl>
</div>
<div  id="attr_select_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>" class="option_select">
<dl class="product-options-dl">
<dt class="product-options-dt" >
<label class="required">
Your Selection {<?php echo $attr['title'];?>}
</label>
</dt>
<dd class="product-options-dd">
<div class="selected_option">
<div class="imgTable">
<div class="imgCell"><div class="selected_option_val"></div></div>
</div>
</div>
</dd>
</dl>
</div>
</div>
	<?php }} ?>
<?php if($attr['id_attributes']==2){
	$bool=false;
	$colors=$attr['options'];
	if(!empty($colors)){
		 ?>
          <div class="option_container">
    <div class="options_list">
<dl class="product-options-dl">
<dt id="color_option" class="product-options-dt">
<label class="required">
Pick Color
<em>*</em>
</label>
</dt>
<dd class="product-options-dd">
<div class="form-item mod" id="attr_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>">
     <?php foreach($colors as $color){?>
<a class="op op_color " title="<?php echo $color['title'];?>" alt="<?php echo $color['title'];?>" style="background:#<?php echo $color['option'];?>" >
<input type="hidden" value="<?php echo $color['id_attribute_options'];?>">
<input type="hidden" id="option_val_<?php echo $color['id_attribute_options'];?>"  value="<?php echo $color['option'];?>"/>

</a>
     <?php  } ?>


<input type="hidden"  name="options[]"  id="attr_input_<?php echo $attr['id_attributes'];?>"  class="req_input"/>
<div id="attr-error-<?php echo $attr['id_attributes'];?>" class="form-error mod">The <?php echo $attr['title'];?>  field is required.</div>
</div>
</dd>
</dl>
</div>

<div  id="attr_select_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>" class="option_select">
<dl class="product-options-dl">
<dt class="product-options-dt" >
<label class="required">
Your Selection {<?php echo $attr['title'];?>}
</label>
</dt>
<dd class="product-options-dd">
<div class="selected_option">
<div class="imgTable">
<div class="imgCell"><div class="selected_option_val"></div></div>
</div>
</div>
</dd>
</dl>
</div>
</div>
	<?php }}

if($bool){
	$bool=true;
	$options=$attr['options'];
	if(!empty($options)){	 ?>
              <div class="option_container">
    <div class="options_list">
<dl class="product-options-dl">
<dt id="color_option" class="product-options-dt">
<label class="required">
 <?php echo $attr['title'];?>
<em>*</em>
</label>
</dt>
<dd class="product-options-dd">
<div class="form-item mod" id="attr_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>">
     <?php foreach($options as $op){?>
<a class="op" title="<?php echo $op['title'];?>" alt="<?php echo $op['title'];?>"  >
<?php echo $op['title'];?>
<input type="hidden" value="<?php echo $op['id_attribute_options'];?>">
<input type="hidden" id="option_val_<?php echo $op['id_attribute_options'];?>"  value="<?php echo $op['option'];?>"/>
</a>
     <?php  } ?>

<input type="hidden"  name="options[]"  id="attr_input_<?php echo $attr['id_attributes'];?>" class="req_input" />
<div id="attr-error-<?php echo $attr['id_attributes'];?>" class="form-error mod">The <?php echo $attr['title'];?>  field is required.</div>
</div>
</dd>
</dl>
</div>
<div  id="attr_select_<?php echo $attr['id_attributes'];?>_<?php echo $product['id_products'];?>" class="option_select">
<dl class="product-options-dl">
<dt class="product-options-dt" >
<label class="required">
Your Selection {<?php echo $attr['title'];?>}
</label>
</dt>
<dd class="product-options-dd">
<div class="selected_option">
<div class="imgTable">
<div class="imgCell"><div class="selected_option_val"></div></div>
</div>
</div>
</dd>
</dl>
</div>
</div>
 <?php } ?>
<?php }
} ?>

</div>
<?php if(!isset($list)){?>

<div id="p-miles-<?php echo $product['id_products'];?>" <?php if(isset($redeem)) echo "style='display:none'";?>>
<?php 
if(isset($product['miles']) && $product['miles']>0 && !$bool_option ){?>
<div class="earned">
<i class="icon">
<img src="<?php echo base_url();?>front/img/miles.png"> 
</i>Earn
<span class="miles-label"><?php echo $product['miles'];?></span> 
miles with this product</div>
<?php } ?>
</div>
<div  id="p-stock-<?php echo $product['id_products']; ?>"  class="stock-status">
<?php if(!$bool_option && $product['quantity']<1){?> 
Out of Stock
<?php } ?>
</div>
<div class="buttons-set ta-r">
<div id="loader-bx"></div>
<div class="FormResult"></div>
<div id="p-btn-<?php echo $product['id_products']; ?>" class="row">
<div class="quantity_popup">
<span class="lbl_t">Quantity: </span>
<div class="segements"  id="r-PriceSegments-<?php echo $product['id_products']; ?>" >
<?php if(!empty($price_segments) && checkUserIfLogin()) {

	 ?>
 <select id="qty_segements" class="combobox">

      

            <?php foreach($price_segments as $segment) {?>
<?php $price_segments = 'Get '.$segment['min_qty'].' for '.changeCurrency($segment['price'],false).' '.$this->session->userdata('currency').' only'; ?>
            <option value="<?php echo $segment['min_qty']; ?>"><?php echo $price_segments; ?></option>
		   <?php }?>
  </select>  
   <?php }else{?>
  <input   class="textbox qty-txt" id="qty" type="text" name="qty"  value="1">
  <?php } ?>
  
</div>
<div class="choosed_segement"></div>
<!--<input class="textbox qty-txt" type="text" value="1" name="qty" id="qty">-->

</div>
<?php  if(!isset($list)){?>
<input class="btn" type="submit" value="Add To Cart" title="Submit">
<?php } ?>
</div>
<?php  if(!isset($list)){?>
<label class="lbl_required mod">* Required Fields</label>
<?php } ?>
</div>
<?php } ?>
</form>
</div>

<script>$('#cart-form').dowformvalidate({updateonly : true});</script>
