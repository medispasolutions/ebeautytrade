<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Landing extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->template->write_view('content', 'content/Landing', $data);
        $this->template->render();
    }
    
    public function contactperson()
    {

        $_data['first_name'] = $_POST['fname1'];
        $_data['last_name'] = $_POST['lname1'];
        $_data['position'] = $_POST['position1'];
        $_data['phone'] = $_POST['phone1'];
        $_data['email'] = $_POST['email1'];
        $_data['application_type'] = $_POST['applicationtype1'];
        $_data['created_date'] = date('Y-m-d h:i:s');
        
        $this->db->insert('supplier_application', $_data);
        $new_id = $this->db->insert_id();
        echo json_encode($new_id);
    }
    
    public function companydetails()
    {

        $_data['company_name'] = $_POST['companyName1'];
        $_data['registration_number'] = $_POST['registrationNumber1'];
        $_data['company_address'] = $_POST['companyAddress1'];
        $_data['company_number'] = $_POST['companyNumber1'];
        $_data['business_based'] = $_POST['businessBased1'];
        $_data['city'] = $_POST['city1'];
        $_data['website'] = $_POST['website1'];
        $userId = $_POST['userid1'];
        
        $this->db->where("id_supplier_application", $userId);
        $this->db->update('supplier_application', $_data);
        
        echo "true";
    }
    
    public function brandsdetails()
    {

		$_branddata['brand_name'] =  $_POST['brandname1'];
		$_branddata['country'] =  $_POST['country1'];
		$_branddata['brand_website'] =   $_POST['brandwebsite1'];
		$_branddata['random_number'] =  $_POST['random1'];
		$_branddata['created_date'] = date('Y-m-d h:i:s');
		
		$this->db->insert('application_brand', $_branddata);
        
        echo "true";
    }

    public function brandinfo()
    {

        echo "true";
    }
    
    public function send()
    {
        
        
        $randomNumber = $this->input->post('random-number');
        
        if($this->input->post('uae-free-brand-number')){
            $planselected = $this->input->post('uae-free-brand-number');
        } else if($this->input->post('uae-gold-brand-number')){
            $planselected = $this->input->post('uae-gold-brand-number');
        } else if($this->input->post('uae-fbe-brand-number')){
            $planselected = $this->input->post('uae-fbe-brand-number');
        } else if($this->input->post('overseas-free-brand-number')){
            $planselected = $this->input->post('overseas-free-brand-number');
        } else if($this->input->post('overseas-gold-brand-number')){
            $planselected = $this->input->post('overseas-gold-brand-number');
        } else if($this->input->post('overseas-fbe-brand-number')){
            $planselected = $this->input->post('overseas-fbe-brand-number');
        }
        
    	if($planselected == "free1"){
            $amount = 0;
        } else if($planselected == "free2"){
            $amount = 0;
        } else if($planselected == "free3"){
            $amount = 0;
        } else if($planselected == "free4"){
            $amount = 0;
        } else if($planselected == "gold1"){
            $amount = 1548;
        } else if($planselected == "gold2"){
            $amount = 2148;
        } else if($planselected == "gold3"){
            $amount = 2748;
        } else if($planselected == "gold4"){
            $amount = 3588;
        } else if($planselected == "fbe1"){
            $amount = 2748;
        } else if($planselected == "fbe2"){
            $amount = 4548;
        } else if($planselected == "fbe3"){
            $amount = 6348;
        } else if($planselected == "fbe4"){
            $amount = 8148;
        } else if($planselected == "free1-overseas"){
            $amount = 0;
        } else if($planselected == "free2-overseas"){
            $amount = 0;
        } else if($planselected == "free3-overseas"){
            $amount = 0;
        } else if($planselected == "gold1-overseas"){
            $amount = 948;
        } else if($planselected == "gold2-overseas"){
            $amount = 1548;
        } else if($planselected == "gold3-overseas"){
            $amount = 2748;
        } else if($planselected == "fbe1-overseas"){
            $amount = 1200;
        } else if($planselected == "fbe2-overseas"){
            $amount = 2400;
        } else if($planselected == "fbe3-overseas"){
            $amount = 3600;
        }
        
        if($this->input->post('application-type') == 1){

            $_data['plan_selected'] = $planselected;
            $_data['random_number'] = $randomNumber;
            $_data['terms_condition'] = '1';
            $_data['status'] = '1';
            $_data['amount'] = $amount;
            
            $_data['brand_description'] = $this->input->post('brand-description');
            
            $userId = $this->input->post('userid');
            
            $number = $this->input->post('amountofbrands');
        	for($i=1; $i<=$number; $i++)
        	{
    			$_branddata['brand_name'] = $this->input->post('brand-name'.$i);
    			$_branddata['country'] = $this->input->post('country'.$i);
    			$_branddata['brand_website'] = $this->input->post('brand-website'.$i);
    			$_branddata['random_number'] = $randomNumber;
    			$_branddata['created_date'] = date('Y-m-d h:i:s');
    			$this->db->insert('application_brand', $_branddata);
        	}

            $this->db->where("id_supplier_application", $userId);
            $this->db->update('supplier_application', $_data);
    
            $return['result'] = 1;
            $return['message'] = lang('landing_success');

            $_userdata['company_name'] = $this->input->post('company-name');
            $_userdata['name'] = $this->input->post('first-name').' '.$this->input->post('last-name');
            $_userdata['first_name'] = $this->input->post('first-name');
            $_userdata['last_name'] = $this->input->post('last-name');
            $_userdata['email'] = $this->input->post('email');
            $_userdata['position'] = $this->input->post('position');
            $_userdata['password'] = md5($randomNumber.'2019');
            $_userdata['phone'] = $this->input->post('phone');
            $_userdata['address_1'] = $this->input->post('company-address');
            $_userdata['company_phone'] = $this->input->post('company-number');
            $_userdata['company_website'] = $this->input->post('website');
            $_userdata['deleted'] = '0';
            $_userdata['created_date'] = date('Y-m-d h:i:s');
            $_userdata['sort_order'] = '0';
            $_userdata['id_roles'] = '5';
            $_userdata['level'] = '0';
            $_userdata['id_countries'] = $this->input->post('first-name');
            $_userdata['city'] = $this->input->post('city');
            $_userdata['created_date'] = date('Y-m-d h:i:s');
            $_userdata['random_number'] = $this->input->post('random-number');
            $_userdata['plan_selected'] = $planselected;
            
            $this->load->model('send_emails');
            $this->send_emails->sendReplyToUaeFreeAppicantNoPayment($_userdata);
            $this->send_emails->sendEmailToAdminForFreeApplicant($_userdata);
            
            if($planselected == "free1" || $planselected == "free2" || $planselected == "free3" || $planselected == "free4"){
                
                $_userdata['id_user_type'] = '2';
                $_userdata['status'] = '1';
                $this->db->insert('user', $_userdata);
                
                $return['redirect'] = 'https://www.ebeautytrade.com/?open=afterApplicationSubmitted';
                echo json_encode($return);
                
            } else if($planselected == "gold1" || $planselected == "gold2" || $planselected == "gold3" || $planselected == "gold4"){
                
                $_userdata['id_user_type'] = '3';
                $_userdata['status'] = '3';
                $_userdata['expire_date'] = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
                $this->db->insert('user', $_userdata);
                
                $return['redirect'] = 'https://www.ebeautytrade.com/?open=afterApplicationSubmitted';
                echo json_encode($return);
                
            } else {
                
                $_userdata['id_user_type'] = '4';
                $_userdata['status'] = '3';
                $_userdata['expire_date'] = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
                $this->db->insert('user', $_userdata);
                
                $return['redirect'] = 'https://www.ebeautytrade.com/?open=afterApplicationSubmitted';
                echo json_encode($return);
                
            }
        
        } else {
            
            $_data['plan_selected'] = $planselected;
            $_data['random_number'] = $randomNumber;
            $_data['terms_condition'] = '1';
            $_data['status'] = '1';
            $_data['amount'] = $amount;
            $_data['brand_description'] = $this->input->post('brand-description');
            
            $userId = $this->input->post('overseas_userid');
            
            $number = $this->input->post('Overseasamountofbrands');
        	for($i=1; $i<=$number; $i++)
        	{
    			$_branddata['brand_name'] = $this->input->post('brand-name'.$i);
    			$_branddata['country'] = $this->input->post('country'.$i);
    			$_branddata['brand_website'] = $this->input->post('brand-website'.$i);
    			$_branddata['random_number'] = $randomNumber;
    			$_branddata['created_date'] = date('Y-m-d h:i:s');
    			$this->db->insert('application_brand', $_branddata);
        	}
        	
            /*$_data['captcha'] = $this->input->post('captcha');*/
            $_data['created_date'] = date('Y-m-d h:i:s');
    
            $this->db->where("id_supplier_application", $userId);
            $this->db->update('supplier_application', $_data);
            
            $_userdata['company_name'] = $this->input->post('company-name');
            $_userdata['name'] = $this->input->post('first-name').' '.$this->input->post('last-name');
            $_userdata['first_name'] = $this->input->post('first-name');
            $_userdata['last_name'] = $this->input->post('last-name');
            $_userdata['email'] = $this->input->post('email');
            $_userdata['position'] = $this->input->post('position');
            $_userdata['password'] = md5($randomNumber.'2019');
            $_userdata['phone'] = $this->input->post('phone');
            $_userdata['address_1'] = $this->input->post('company-address');
            $_userdata['company_phone'] = $this->input->post('company-number');
            $_userdata['company_website'] = $this->input->post('website');
            $_userdata['deleted'] = '0';
            $_userdata['created_date'] = date('Y-m-d h:i:s');
            $_userdata['sort_order'] = '0';
            $_userdata['id_roles'] = '5';
            $_userdata['level'] = '0';
            $_userdata['status'] = '2';
            $_userdata['id_user_type'] = '0';
            $_userdata['id_countries'] = $this->input->post('first-name');
            $_userdata['city'] = $this->input->post('city');
            $_userdata['random_number'] = $this->input->post('random-number');
            $_userdata['plan_selected'] = $planselected;
            
            $this->db->insert('user', $_userdata);
                
            $this->load->model('send_emails');
            $this->send_emails->sendReplyToUaeFreeAppicantNoPayment($_userdata);
            $this->send_emails->sendEmailToAdminForOverseasApplicant($_userdata);
            
            $return['result'] = 1;
            $return['message'] = lang('landing_success');
            $return['redirect'] = 'https://ebeautytrade.com/landing-page-payment?'.$randomNumber;
        ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <script>
                $(document).ready(function(){
                    var url = "https://www.ebeautytrade.com/?open=afterApplicationSubmitted";
                    $(location).attr('href', url); // Using this
                });
            </script>
        <?php          
        }
    }
}