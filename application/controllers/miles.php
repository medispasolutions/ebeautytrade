<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Miles extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}

public function index()
	{
		
 // get product attribute options
if(checkUserIfLogin()){
       if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		if($this->session->userdata('login_id') == '') {
$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$this->session->set_userdata('redirect_link',$url);
		}
		
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>1));
		$data['vouchers'] = $this->fct->getAll('vouchers','sort_order');
                $this->template->write_view('header', 'blocks/header', $data);
				$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
                $this->template->write_view('content', 'content/miles', $data);
                $this->template->write_view('footer', 'blocks/footer', $data);
                    
                $this->template->render();}else{
		$this->session->set_flashdata('error_message',lang('login_to_continue'));
	
		redirect(route_to('user/login'));
					}
           /* }*/
            
      
    }



public function send()
	{   $id=$this->input->post('id');
	    $voucher="";
		if($id!=""){
		$voucher=$this->fct->getonerecord('vouchers',array('id_vouchers'=>$id));	
		}
		$user=$this->ecommerce_model->getUserInfo();
		
		if(empty($voucher) && !empty($user)){
			$return['result']=0;
			$return['message']="Error!";}else{
			$miles=$this->fct->getonecell('miles','miles',array('id_miles'=>$voucher['id_miles']));
			$user_miles=$user['miles'];
			if(!empty($miles) && !empty($user_miles) && $user_miles!=0 && $user_miles>=$miles ){
				$image=$voucher['image'];
				if(!empty($image) && file_exists('./uploads/vouchers/'.$image) && !file_exists('./uploads/users_vouchers/'.$image)){
				;	
		copy('./uploads/vouchers/'.$image, './uploads/users_vouchers/'.$image);
		$this->fct->createthumb($image,"users_vouchers","295x295");
		$data['image']=$image;}
				$data["created_date"]=date("Y-m-d h:i:s");
				$data['id_user']=$user['id_user'];
				$data['id_vouchers']=$voucher['id_vouchers'];
				$data['title']=$voucher['title'];
				$data['description']=$voucher['description'];
				$data['miles']=$miles;
				$this->db->insert('users_vouchers',$data);	
				
				$new_user_miles=$user_miles-$miles;
				$update['miles']=$new_user_miles;
				$this->db->where('id_user',$user['id_user']);
				$this->db->update('user',$update);
			$return['result']=1;
			$return['message']="Success.";
				}else{
			$return['result']=0;
			$return['message']="Your miles less than 5 miles.";
					}
			}
			
		die(json_encode($return));	
		
		}            


	
	
	

}