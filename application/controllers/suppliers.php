<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {


        // get product attribute options


        $data['seo'] = $this->fct->getonerow('static_seo_pages', array(
            'id_static_seo_pages' => 38
        ));
        $data['deliver_return'] = $this->fct->getonerecord('dynamic_pages', array(
            'id_dynamic_pages' => 5
        ));
        $data['page_title'] = $data['seo']['title'];

        $data['categories'] = $this->ecommerce_model->getProductsCategories('', 1000, 0);
        $suppliers = $this->custom_fct->getSuppliers(array('list' => 1), 100, 0);

        $data['suppliers'] = $suppliers;


        $show_items = 20;
        $this->load->library("pagination");


        $url = route_to("suppliers") . '?pagination=on';

        $count_news = $this->custom_fct->getSuppliers(array('list' => 1));
        $config["base_url"] = $url;
        $config["total_rows"] = $count_news;
        $config["per_page"] = $show_items;
        $config["use_page_numbers"] = TRUE;
        $config["page_query_string"] = TRUE;
//print "<pre>";print_r($config);exit;
        $this->pagination->initialize($config);
        if (isset($_GET["per_page"])) {
            if ($_GET["per_page"] != "") $page = $_GET["per_page"];
            else $page = 0;
        } else $page = 0;

        $data["suppliers"] = $this->custom_fct->getSuppliers(array('list' => 1), $show_items, $page);

        if ($this->session->userdata('login_id')) {
            $userData = $this->ecommerce_model->getUserInfo();
        }


        /*    if (empty($data['product'])) {
                redirect(route_to('products'));
            } else {*/

        $this->fct->addVisit("suppliers", $this->session->userdata("session_id"), '');


        $id_user = 0;
        if ($this->session->userdata('login_id')) {
            $id_user = $this->session->userdata('login_id');
        }


        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }

        $breadcrumbs = "";
        $breadcrumbs .= '<ul>';
        $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';


        $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
        $breadcrumbs .= '<li>' . lang('suppliers_title') . '</li>';
        $breadcrumbs .= '</ul>';

        $data['breadcrumbs'] = $breadcrumbs;
        $limit_r = 25;

        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        //$this->template->write_view('content', 'blocks/breadcrumbs', $data);
        $this->template->write_view('content', 'content/suppliers', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
        /* }*/


    }

    public function campaign()
    {

        echo $this->load->view('emails/campaign_email');

    }

    public function details($id_brands = "")
    {
        $parent_levels = array();
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $account_status = $this->ecommerce_model->updateGroupCategoryProducts();
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        // get product attribute options

        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 6));

        $dir = "";
        $sort_order = 'products.sku';
        $brand = "";

        if (!empty($id_brands)) {
            $brand = $this->custom_fct->getOneBrand($id_brands);
        }


        if (!empty($brand)) {
            ////TRACING//////	
            $insert_log['section'] = $brand['title'];
            $this->fct->insertNewLog($insert_log);

            $url = route_to('brands/details/' . $id_brands) . '?pagination=on';
            $data['brand'] = $brand;
            $data['seo'] = $brand;

            if (checkUserIfLogin()) {
                $data['login'] = true;
            } else {
                $data['login'] = false;
            }

            if ($this->input->get('sort')) {
                $sort_order = $this->ecommerce_model->getSortOrder($this->input->get('sort'));


            }
            if (empty($data['brand']['meta_title']))
                $data['seo']['meta_title'] = $data['brand']['title'];

            if (!empty($data['brand']['meta_description']))
                $data['seo']['meta_description'] = $data['brand']['meta_description'];

            if (!empty($data['brand']['meta_keywords']))
                $data['seo']['meta_keywords'] = $data['brand']['meta_keywords'];

            if (!empty($data['brand']['logo'])) {
                $data['og_image'] = base_url() . 'uploads/brands/' . $data['brand']['logo'];
            }

            $id_user = 0;
            if ($this->session->userdata('login_id')) {
                $id_user = $this->session->userdata('login_id');
            }


            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>Shop By Brand</li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            /*				$breadcrumbs .= '<li><a href=' . route_to('brands') . '>brands</a></li>';
                            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';*/
            $breadcrumbs .= '<li>' . $data['brand']['title'] . '</li>';
            $breadcrumbs .= '</ul>';

            $data['breadcrumbs'] = $breadcrumbs;
            $cond['brand'] = $brand['id_brands'];

            if ($this->input->get('sort') != "") {
                $url .= "&sort=" . $this->input->get('sort');
            }


            /*	$count_news = $this->ecommerce_model->getProductsGroupByCategories($cond);
                $data['categories'] = $this->ecommerce_model->getProductsGroupByCategories($cond,$count_news,0,$sort_order,$dir);*/


            if ($this->input->get('category') != "" && is_numeric($this->input->get('category'))) {
                $cond['id_categories'] = $this->input->get('category');
                $category = $this->fct->getonerecord('categories', array('id_categories' => $this->input->get('category')));
                $data['category'] = $category;
                $url .= "&category=" . $this->input->get('category');
            }

            ///////////////////////////////SEARCH///////////////////////

            if ($this->input->get('categories') != "") {
                $arr = $this->input->get('categories');
                $id_c = "";
                foreach ($arr as $key => $val) {
                    if (!empty($val)) {
                        $id_c = $val;
                    }
                    $url .= '&categories=' . $val;
                }
                if ($id_c != "") {

                    $category_url = $this->fct->getonecell('categories', 'title_url' . $lang, array('id_categories' => $id_c));
                }
            }

            $parent_levels = array();
            $sub_categories = array();
            if (!empty($category_url)) {

                $category = $this->fct->getonerecord('categories', array('title_url' . $lang => $category_url));
                $cond['id_categories'] = $category['id_categories'];
                $id_category = $category['id_categories'];
                $items = $this->custom_fct->getCategoriesPages($id_category, array(), array(), 100, 0);
                $categories_ids = $this->custom_fct->getCategoriesIdsTreeParentLevels($items, $id_category);
                $cond['categories'] = implode(",", $categories_ids);
                $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($id_category);
                $parent_levels = arrangeParentLevels($parent_levels);
                $data['parent_levels'] = $parent_levels;
                $parent_ids = $this->ecommerce_model->getIds('id_categories', $parent_levels);
                $data['selected_categories'] = $parent_ids;
                $cond2['id_parent'] = $category['id_categories'];
                $sub_categories = $this->ecommerce_model->getSubCategories2($cond2, 100, 0);
                $data['sub_categories'] = $sub_categories;
            }

            if (!empty($category)) {
                $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($category['id_categories']);
                $parent_levels = arrangeParentLevels($parent_levels);


                $cond2['id_parent'] = $category['id_categories'];
            } else {
                /*$cond2['id_parent']=0;*/
                $main_categories = $this->fct->getAll_cond('categories', 'sort_order asc', array('id_parent' => 0));

                $parent_ids = $this->ecommerce_model->getIds('id_categories', $main_categories);
                $cond2['ids_parent'] = $parent_ids;

            }


            $data['parent_levels'] = $parent_levels;
            $cond2['id_brands'] = $brand['id_brands'];
            if ($this->input->get('keywords') != "") {
                $keywords = $this->input->get('keywords');

                if (!empty($keywords))
                    $cond2['keywords'] = $keywords;
            }

            $sub_categories = $this->custom_fct->getSubCategories3($cond2, 100, 0);
            $data['sub_categories_cc'] = $sub_categories;


            ////////////////////////////////////////////////

            /*if($this->input->get('sort')=="category" || $this->input->get('sort')==""){
            $limit=4;	
            $count_news = $this->ecommerce_model->getProductsGroupByCategories($cond);}else{
            $limit=24;
            $count_news = $this->ecommerce_model->getProducts($cond);}*/

            if ($this->input->get('keywords') != "") {
                $url .= "&keywords=" . $this->input->get('keywords');
                $cond['keywords'] = $this->input->get('keywords');
            }

            $count_news = $this->ecommerce_model->getProducts($cond);
            $show_items = 40;

            /*if(isset($_GET['per_page'])) {
                if($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            }
            else $page = 0;*/


            $data['show_items'] = $show_items;
            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $cc = $count_news;
            $config['base_url'] = $url;
            $config['total_rows'] = $cc;
            $config['num_links'] = '8';
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $config['per_page'] = $show_items;

            $this->pagination->initialize($config);
            if ($this->input->get('per_page')) {
                if ($this->input->get('per_page') != "")
                    $page = $this->input->get('per_page');
                else
                    $page = 0;
            } else
                $page = 0;
            $data['count'] = $cc;
            $data['per_page'] = $page;
            $num_page_prev = 1;
            if ($page > 0) {
                $num_page = intval($page / $show_items);
            } else {
                $num_page = 0;
            }


            /*if($this->input->get('sort')=="" || $this->input->get('sort')=="category"){
            $data['categories'] = $this->ecommerce_model->getProductsGroupByCategories($cond,$limit,$page,$sort_order,$dir);
            }else{
            $data['products'] = $this->ecommerce_model->getProducts($cond,$limit,$page,$sort_order,$dir);	}*/

            $data['products'] = $this->ecommerce_model->getProducts($cond, $show_items, $page, $sort_order, $dir);


            $data['show_items'] = $show_items;
            $data['count'] = $count_news;
            $data['offset'] = $page;
            $data['section_list'] = "brands";
            /////////////////////////////////ADVERTISEMENTS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

            $data['current_url'] = $url;
            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);

            $this->template->write_view('content', 'content/brand_details', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('using_fake_link'));
            redirect(site_url());
        }
        /* }*/
    }

    public function training($id_training_categories = "")
    {

        if (!empty($id_training_categories)) {
            $training = $this->fct->getonerecord('training_categories', array('id_training_categories' => $id_training_categories));
            $data['training'] = $training;
            $data['seo'] = $training;
        }
        if (!empty($training)) {
            $cond['training'] = $id_training_categories;
            $url = route_to('brands/training/' . $id_training_categories) . '?redirect=on';
        } else {
            $url = route_to('brands/training') . '?redirect=on';
        }
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }

        $cond['products.status'] = 0;
        $cond['set_as_training'] = 1;

        if ($this->input->get('show_items')) {
            $show_items = $this->input->get('show_items');
        } else {
            $show_items = "6";
        }
        if (isset($category)) {
        }
        
        $count = $this->ecommerce_model->getProducts($cond);

        $data['show_items'] = $show_items;
        $show_items = ($show_items == 'All') ? $count_news : $show_items;
        $cc = $count;
        $config['base_url'] = $url;
        $config['total_rows'] = $cc;
        $config['num_links'] = '8';
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;

        $config['per_page'] = $show_items;

        $this->pagination->initialize($config);
        if ($this->input->get('per_page')) {
            if ($this->input->get('per_page') != "")
                $page = $this->input->get('per_page');
            else
                $page = 0;
        } else
            $page = 0;
        $data['count'] = $cc;
        $data['per_page'] = $page;
        $num_page_prev = 1;
        if ($page > 0) {
            $num_page = intval($page / $show_items);
        } else {
            $num_page = 0;
        }

        $data['products'] = $this->ecommerce_model->getProducts($cond, $show_items, $page);


        if (isset($training) && !empty($training)) {

            if (empty($training['meta_title']))
                $data['seo']['meta_title'] = $training['title'];

            if (!empty($training['meta_description']))
                $data['seo']['meta_description'] = $training['meta_description'];

            if (!empty($training['meta_keywords']))
                $data['seo']['meta_keywords'] = $training['meta_keywords'];

            if (!empty($training['image'])) {
                $data['og_image'] = $training['image'];
            }
        } else {
            $data['seo'] = $this->fct->getonerecord('static_seo_pages', array('id_static_seo_pages' => 22));
        }


        $breadcrumbs = "";
        $breadcrumbs .= '<ul>';
        $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';

        if (isset($training) && !empty($training)) {
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li><a href=' . route_to('brands/training') . '>Brand Certified Training</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>' . $training['title'] . '</li>';
        } else {
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>Brand Certified Training</li>';
        }

        $breadcrumbs .= '</ul>';

        $data['breadcrumbs'] = $breadcrumbs;


        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);

        $this->template->write_view('content', 'content/brands_training', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
        /* }*/
    }

    function validate_captcha()
    {
        $row_count = 1;
        if (!isset($_POST['no_captcha'])) {
            $expiration = time() - 7200; // Two hour limit
            $this->db->query("DELETE FROM captcha WHERE captcha_time < " . $expiration);
            // Then see if a captcha exists:
            $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
            $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
            $query = $this->db->query($sql, $binds);
            $row = $query->row();
            $row_count = $row->count;
            if ($row_count == 0) {
                $this->form_validation->set_message('validate_captcha', lang('notcorrectcharacters'));
                return false;
            } else {
                return true;
            }
        }
    }

    /*public function phone()
    {  
    $phone=$this->input->post('phone');
    $bool=true;
    foreach($phone as $key=>$val){
        if(empty($val)){
            $bool=false;}}
    
        if(!$bool) {
                $this->form_validation->set_message('phone',"The phone fields is required.");
               return false;
                }else{
                    return true;
                }
        
        }*/

    public function send()
    {
        $login = checkUserIfLogin();
        //print '<pre>';
        //print_r($_POST);exit;
        /*if($login){*/
        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('name', lang('name'), 'trim|required');
        $this->form_validation->set_rules('phone', lang('phone'), 'trim|required');
        /*$this->form_validation->set_rules("phone", "phone", "callback_phone[]");*/
        $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email');
        $this->form_validation->set_rules('message', lang('message'), 'trim|required');
        //$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {


            $_data['name'] = $this->input->post('name');
            $_data['email'] = $this->input->post('email');

            $phone = $this->input->post("phone");
            $_data['phone'] = $phone;

            $_data['id_brands'] = $this->input->post('id_brands');
            $_data['type'] = 'meeting_request';
            $_data['message'] = $this->input->post('message');
            $_data['created_date'] = date('Y-m-d h:i:s');
            $_data['lang'] = $this->lang->lang();
            $this->db->insert('contactform', $_data);
            $new_id = $this->db->insert_id();
            $_data['id_message'] = $new_id;
            // send emails
            $brand = $this->custom_fct->getOneBrand($_data['id_brands']);
            $_data['brand'] = $brand;

            $this->load->model('send_emails');
            $this->send_emails->sendRequestToAdmin($_data);
            if ($login) {
                $this->send_emails->sendRequestToBrand($_data);
            }
            $this->send_emails->sendRequestToClient($_data);
            $return['result'] = 1;
            $return['message'] = lang('contact_success');
        }/*}else{
		$this->session->set_flashdata('error_message',lang('login_to_continue_meeting_request'));
		$return['message'] = lang('system_is_redirecting_to_page');
		$return['result'] = 1;
		$return['redirect_link']=route_to('user/login');
			}*/
        echo json_encode($return);
    }

    public function send_book_meeting()
    {
        $login = checkUserIfLogin();
        //print '<pre>';
        //print_r($_POST);exit;
        /*if($login){*/
        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('name', lang('name'), 'trim|required');
        $this->form_validation->set_rules('phone', lang('phone'), 'trim|required');
        /*$this->form_validation->set_rules("phone", "phone", "callback_phone[]");*/
        $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email');

        //$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {


            $_data['name'] = $this->input->post('name');
            $_data['email'] = $this->input->post('email');

            $phone = $this->input->post("phone");
            $_data['phone'] = $phone;

            $_data['id_brands'] = $this->input->post('id_brands');
            $_data['type'] = 'book_meeting';
            $_data['message'] = $this->input->post('message');
            $_data['created_date'] = date('Y-m-d h:i:s');
            $_data['lang'] = $this->lang->lang();
            $this->db->insert('contactform', $_data);
            $new_id = $this->db->insert_id();
            $_data['id_message'] = $new_id;
            // send emails

            $this->load->model('send_emails');
            $this->send_emails->sendBookingMeeting($_data);

            $return['result'] = 1;
            $return['thank_you_content'] = $this->load->view('load/thank_you_content', array(), true);
        }/*}else{
		$this->session->set_flashdata('error_message',lang('login_to_continue_meeting_request'));
		$return['message'] = lang('system_is_redirecting_to_page');
		$return['result'] = 1;
		$return['redirect_link']=route_to('user/login');
			}*/
        echo json_encode($return);
    }

    public function loadSearchBrand()
    {

        $data = array();
        $parent_levels = array();
        $data['c_bool'] = true;
        $lang = "";
        $data['brand'] = $this->custom_fct->getOneBrand($this->input->post('id_brands'));
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $id_category = $this->input->post('id_category');
        $data['s_keyord'] = 1;
        if ($id_category != "") {

            $category = $this->fct->getonerecord('categories', array('id_categories' => $id_category));


            $data['category'] = $category;
            $cond['id_categories'] = $category['id_categories'];
            $items = $this->custom_fct->getCategoriesPages($id_category, array(), array(), 100, 0);
            $categories_ids = $this->custom_fct->getCategoriesIdsTreeParentLevels($items, $id_category);
            $cond['categories'] = implode(",", $categories_ids);

            $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($id_category);
            $parent_levels = arrangeParentLevels($parent_levels);

            $data['title'] = "";
            $cond2['id_parent'] = $category['id_categories'];


            $sub_categories = $this->ecommerce_model->getSubCategories2($cond2, 100, 0);
            $data['sub_categories'] = $sub_categories;
        }

        $data['parent_levels'] = $parent_levels;
        $parent_ids = $this->ecommerce_model->getIds('id_categories', $parent_levels);
        $data['selected_categories'] = $parent_ids;


        $html = $this->load->view('load/loadSearchBrand', $data, true);
        $return['html'] = $html;
        die(json_encode($return));

    }
    
///////////////////////////////////BRANDS////////////////////////////////////////
    public function getVideo($id_brands = "")
    {
        $data['result'] = "";

        $brand = $this->fct->getonerecord('brands', array('id_brands' => $id_brands));

        if (isset($brand['id_brands'])) {
            $data['result'] = $this->custom_fct->getOneBrand($brand['id_brands']);
        }
        if (!empty($data['result'])) {
            $data['video'] = $data['result']['video'];
            $html = $this->load->view('load/video', $data, true);
            echo $html;
            exit;
        }
    }

    public function bookmeeting()
    {
        $html = $this->load->view('load/bookmeeting', array(), true);
        $return['html'] = $html;
        die(json_encode($return));
    }
}