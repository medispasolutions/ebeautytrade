<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Designers extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}
	public function index()
	{
		
		if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>1));
		$data['designers']=$this->ecommerce_model->getDesignersByLetters();


	    $breadcrumbs="";
		$breadcrumbs .='<ul>';
		$breadcrumbs .='<li><a href='. site_url().'>Home</a></li>';
		$breadcrumbs .='<li class="divider"><i class="fa fa-arrow-right"></i></li>';

	
		$breadcrumbs .='<li>Designers</li>';
		$breadcrumbs .='</ul>';
		
		$data['breadcrumbs']=$breadcrumbs;


		$this->template->write_view('header','blocks/header',$data);
		$this->template->write_view('content','content/designers',$data);
		$this->template->write_view('footer','blocks/footer',$data);
		$this->template->render();
	}
}