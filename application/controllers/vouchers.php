<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vouchers extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}

public function index()
	{
		
 // get product attribute options
if(checkUserIfLogin()){
       if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		if($this->session->userdata('login_id') == '') {
$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$this->session->set_userdata('redirect_link',$url);
		}
		
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>23));
		$data['vouchers'] = $this->fct->getAll('vouchers','sort_order');
                $this->template->write_view('header', 'blocks/header', $data);
				$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
                $this->template->write_view('content', 'content/vouchers', $data);
                $this->template->write_view('footer', 'blocks/footer', $data);
                    
                $this->template->render();}else{
		$this->session->set_flashdata('error_message',lang('login_to_continue'));
	
		redirect(route_to('user/login'));
					}
           /* }*/ }


public function details($id="",$rand="")
	{
		
		if(!empty($id)){
		$voucher=$this->fct->getonerecord('users_vouchers',array('id_users_vouchers'=>$id,'rand'=>$rand));
		$data['voucher']=$voucher;	}
 // get product attribute options

if(checkUserIfLogin()){
	if(!empty($voucher)){
       if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		if($this->session->userdata('login_id') == '') {
$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$this->session->set_userdata('redirect_link',$url);
		}
		
		
		  		if (!empty($voucher['title']))
                    $data['seo']['meta_title'] = $voucher['title'];
             


                $this->template->write_view('header', 'blocks/header', $data);
				$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
                $this->template->write_view('content', 'content/voucher_details', $data);
                $this->template->write_view('footer', 'blocks/footer', $data);
                    
                $this->template->render();}else{
		$this->session->set_flashdata('error_message',lang('access_denied'));
		redirect(route_to('vouchers'));
			}}else{
		$this->session->set_flashdata('error_message',lang('login_to_continue'));
		redirect(route_to('user/login'));
					}
           }
		   
public function printVoucher($id="",$rand="")
	{
		
		if(!empty($id)){
		$voucher=$this->fct->getonerecord('users_vouchers',array('id_users_vouchers'=>$id,'rand'=>$rand));
		$data['voucher']=$voucher;	}
 // get product attribute options

if(checkUserIfLogin()){
	if(!empty($voucher)){
       if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		if($this->session->userdata('login_id') == '') {
$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$this->session->set_userdata('redirect_link',$url);
		}
		
		$data['title']="Print ".$voucher['title']."";
		  
		$this->load->view('content/voucher_print',$data);
   }else{
		$this->session->set_flashdata('error_message',lang('access_denied'));
		redirect(route_to('vouchers'));
			}}else{
		$this->session->set_flashdata('error_message',lang('login_to_continue'));
		redirect(route_to('user/login'));
					}
           } 
public function send()
	{   $id=$this->input->post('id');
	    $voucher="";
		if($id!=""){
		$voucher=$this->fct->getonerecord('vouchers',array('id_vouchers'=>$id));	
		}
		$user=$this->ecommerce_model->getUserInfo();
	
		if(empty($voucher) && !empty($user)){
			$return['result']=0;
			$return['message']="Error!";}else{
			$miles=$this->fct->getonecell('miles','miles',array('id_miles'=>$voucher['id_miles']));
			$user_miles=$user['miles'];
			if(!empty($miles) && !empty($user_miles) && $user_miles!=0 && $user_miles>=$miles ){
				$image=$voucher['image'];
				 
				if(!empty($image) && file_exists('./uploads/vouchers/'.$image)){

		copy('./uploads/vouchers/'.$image, './uploads/users_vouchers/'.$image);
		$this->fct->createthumb($image,"users_vouchers","295x295");
		$data['image']=$image;
		}
				$data["created_date"]=date("Y-m-d h:i:s");
				$data['id_user']=$user['id_user'];
				$data['rand']= $this->fct->generate_password(8);;
				$data['id_vouchers']=$voucher['id_vouchers'];
				$data['title']=$voucher['title'];
				$data['description']=$voucher['description'];
				$data['miles']=$miles;
				$this->db->insert('users_vouchers',$data);	
				$new_id=$this->db->insert_id();
				$new_user_miles=$user_miles-$miles;
				$update['miles']=$new_user_miles;
				$this->db->where('id_user',$user['id_user']);
				$this->db->update('user',$update);
			$this->session->set_flashdata('success_message','Congratulations! You just earned '.$miles.' miles');		
			$redirect_link = route_to('vouchers/details/'.$new_id.'/'.$data['rand']);
			$return['redirect_link'] = $redirect_link;	
			$return['result']=1;
			$return['message']=lang('system_is_redirecting_to_page');
			
				}else{
				
			$return['result']=0;
			$return['message']="Sorry! Your miles less than ".$miles." miles.";
					}
			}
			
		die(json_encode($return));	
		
		}            


	
	
	

}