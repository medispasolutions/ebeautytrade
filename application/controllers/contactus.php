<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contactus extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($type = 'contact_message', $data = array())
    {

        if ($this->session->userdata('currency') == "") {
            $this->session->set_userdata('currency', $this->config->item("default_currency"));
        }
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 36));
        $data['settings'] = $this->fct->getonerow('settings', array('id_settings' => 1));
        $data['branches'] = $this->fct->getAll('contact_branches', 'sort_order');

        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'blocks/breadcrumbs', $data);
        $this->template->write_view('content', 'content/contact', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
    }

    public function validate_captcha($recaptcha_response_value)
    {
        $captcha = $this->serviceLocator->captcha();
        if ($captcha->validate($recaptcha_response_value)) {
            return true;
        }

        $this->form_validation->set_message('validate_captcha', "Captcha code is not valid - try reloading page");
        return false;
    }

    public function send()
    {
        $allPostedVals = $this->input->post(null, true);

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('message', 'Message', 'trim|required');
        $this->form_validation->set_rules('g-recaptcha-response',"Recaptcha" , 'trim|required|xss_clean|callback_validate_captcha[]');


        if ($this->form_validation->run() == false) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {
            $_data['name'] = $this->input->post('name');
            $_data['email'] = $this->input->post('email');
            $_data['subject'] = $this->input->post('subject');
            $_data['phone'] = $this->input->post('phone');
            $_data['message'] = $this->input->post('message');
            $_data['type'] = 'contact_message';


            /*$_data['captcha'] = $this->input->post('captcha');*/
            $_data['created_date'] = date('Y-m-d h:i:s');


            $this->db->insert('contactform', $_data);
            $new_id = $this->db->insert_id();
            $_data['id_message'] = $new_id;


            // send emails
            //$_data['lang']=$lang;
            $this->load->model('send_emails');
            $this->send_emails->sendContactUsToAdmin($_data);
            $this->send_emails->sendContactUsToUser($_data);
            //$this->session->set_flashdata('success_message',lang('contact_success'));
            $return['result'] = 1;
            $return['message'] = lang('contact_success');

        }

        echo json_encode($return);
    }

}