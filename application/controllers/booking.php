<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}
	public function index($type='contact_message',$data = array())
	{
		if($this->session->userdata('currency') == "") {
		$this->session->set_userdata('currency',$this->config->item("default_currency"));
	}
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>37));
		$data['settings'] = $this->fct->getonerow('settings',array('id_settings'=>1));

	// css files
	$this->template->add_css('front/css/zabuto_calendar.css');
	$this->template->add_css('front/css/timepicki.css');
	
	// js files
	$this->template->add_js('front/js/zabuto_calendar.min.js');
	$this->template->add_js('front/js/timepicki.js');
	
	
	$this->template->write_view('content','content/booking',$data);
	$this->template->render();
	}
	
	public function validatecaptcha()
{
include_once './captcha/securimage.php';
$securimage = new Securimage();
$captcha = $this->input->post('captcha');
if(empty($captcha)){
$this->form_validation->set_message('validatecaptcha','The Captcha field is required.');
return false; 
}
elseif($securimage->check($captcha) == false){
$this->form_validation->set_message('validatecaptcha','The Security Code you have entered is incorrect, please try again and enter the correct code.');
return false; 
}else{
return true; 
}
}
	public function send()
	{
		$allPostedVals = $this->input->post(NULL,TRUE);

		$this->form_validation->set_rules('subject','Business Name','trim|required');
		$this->form_validation->set_rules('name','Name','trim|required');
		$this->form_validation->set_rules('email','E-mail','trim|required|valid_email');
		$this->form_validation->set_rules('phone','Phone','trim|required');
		$this->form_validation->set_rules('date','Date','trim|required');
		$this->form_validation->set_rules('time_slot','Time Slot','trim|required');
		/*$this->form_validation->set_rules('time_to','Time To','trim|required');*/
	    //$this->form_validation->set_rules('captcha','Captcha','trim|required|xss_clean|callback_validatecaptcha[]');
	
		if($this->form_validation->run() == FALSE) {
				 $return['result'] = 0;
				$return['errors'] = array();
				$return['message'] = 'Error!';
				//$return['captcha'] = $this->fct->createNewCaptcha();
				$find =array('<p>','</p>');
				$replace =array('','');
				foreach($allPostedVals as $key => $val) {
					if(form_error($key) != '') {
						$return['errors'][$key] = str_replace($find,$replace,form_error($key));
						
			
					}
				}
				
				
		}
		else {
			$_data['name'] = $this->input->post('name');
			$_data['subject'] = $this->input->post('subject');
			$_data['email'] = $this->input->post('email');
			$_data['subject'] = $this->input->post('subject');
			$_data['phone'] = $this->input->post('phone');
			$_data['schedule_date'] = $this->input->post('date');
			
			$time_slot=$this->input->post('time_slot');
			$time_slot_arr=explode('-',$time_slot);
		
			$_data['time_from'] = $time_slot_arr[0];
			$_data['time_to'] = $time_slot_arr[1];
			
			$_data['type'] = 'booking';
	
			/*$_data['captcha'] = $this->input->post('captcha');*/
			$_data['created_date'] = date('Y-m-d h:i:s');

	
			$this->db->insert('contactform',$_data);
			$new_id=$this->db->insert_id();
			$_data['id_message']=$new_id;
		
			
			// send emails
		//$_data['lang']=$lang;
		    $this->load->model('send_emails');
			$this->send_emails->sendContactUsToAdmin($_data,36);
			$this->send_emails->sendContactUsToUser($_data,36);
		//$this->session->set_flashdata('success_message',lang('booking_success'));
			$return['result'] = 1;
			$return['redirect_link'] = route_to('booking/success');
			$return['message'] = lang('system_is_redirecting_to_page');
			
		}
		
		echo json_encode($return);
	}
	
	function validate_captcha()
{
 $row_count = 1;
 if(!isset($_POST['no_captcha'])) {
  $expiration = time()-7200; // Two hour limit
  $this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
  // Then see if a captcha exists:
  $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
  $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
  $query = $this->db->query($sql, $binds);
  $row = $query->row();
  $row_count = $row->count;
  if($row_count == 0) {
   $this->form_validation->set_message('validate_captcha','Characters do not match');
   return false;
  }
  else {
   return true;
  }
 }
}

	public function success()
	{
		if($this->session->userdata('currency') == "") {
		$this->session->set_userdata('currency',$this->config->item("default_currency"));
	}
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>37));
		$data['settings'] = $this->fct->getonerow('settings',array('id_settings'=>1));

	$this->template->write_view('content','content/booking_success',$data);
	$this->template->render();
	}	

}