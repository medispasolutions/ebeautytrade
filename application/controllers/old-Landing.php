<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->template->write_view('content', 'content/Landing', $data);
        $this->template->render();
    }
    
    public function send()
    {
        $_data['first-name'] = $this->input->post('first-name');
        $_data['last-name'] = $this->input->post('last-name');
        $_data['position'] = $this->input->post('position');
        $_data['phone'] = $this->input->post('phone');
        $_data['email'] = $this->input->post('email');
        $_data['company-name'] = $this->input->post('company-name');
        $_data['company-number'] = $this->input->post('company-number');
        $_data['registration-number'] = $this->input->post('registration-number');
        $_data['business-based'] = $this->input->post('business-based');
        $_data['city'] = $this->input->post('city');
        $_data['website'] = $this->input->post('website');
        $_data['application-type'] = $this->input->post('application-type');
        $_data['brand-description'] = $this->input->post('brand-description');
        $_data['plan-selected'] = $this->input->post('planselected');
        $_data['random-number'] = $this->input->post('random-number');

        $number = $this->input->post('amountofbrands');

    	for($i=1; $i<=$number; $i++)
    	{
			$_branddata['brand-name'] = $this->input->post('brand-name'.$i);
			$_branddata['country'] = $this->input->post('country'.$i);
			$_branddata['brand-website'] = $this->input->post('brand-website'.$i);
			$_branddata['random-number'] = $this->input->post('random-number');
			$_branddata['created_date'] = date('Y-m-d h:i:s');
			$this->db->insert('application_brand', $_branddata);
            $_data['terms-condition'] = $number;
    	}

        /*$_data['captcha'] = $this->input->post('captcha');*/
        $_data['created_date'] = date('Y-m-d h:i:s');

        $this->db->insert('supplier_application', $_data);
        $new_id = $this->db->insert_id();
        $_data['id_message'] = $new_id;

        /* send emails
        //$_data['lang']=$lang;
        $this->load->model('send_emails');
        $this->send_emails->sendContactUsToAdmin($_data);
        $this->send_emails->sendContactUsToUser($_data);
        //$this->session->set_flashdata('success_message',lang('contact_success'));
        */
        $return['result'] = 1;
        $return['message'] = lang('contact_success');

        echo json_encode($return);
    }
}