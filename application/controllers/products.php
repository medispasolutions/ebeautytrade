<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Products extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($title_url = "")
    {
        /*	$options=$this->fct->getAll_cond('products_categories','id_products_categories desc',array('ids_parent'=>""));

                $i=0;
            foreach($options as $val){$i++;
                $new_id=$val['id_categories'];
                if(!empty($new_id)){

                    $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($new_id);
                    $parent_levels = arrangeParentLevels($parent_levels);
                    $ids_parent=$this->ecommerce_model->getIds('id_categories',$parent_levels);

                    if(!empty($ids_parent)){
                        $ids_parent=implode(',',$ids_parent);
                    }else{$ids_parent="";}

                $update['ids_parent']=$ids_parent;
                $this->db->where(array('id_products_categories'=>$val['id_products_categories']));
                $this->db->update('products_categories',$update);}

                }*/

        /*		$users=$this->fct->getAll_cond('user','id_user desc',array('correlation_id'=>""));


                $i=0;
            foreach($users as $val){
                $update['correlation_id']=rand();
                $this->db->where(array('id_user'=>$val['id_user']));
                $this->db->update('user',$update);
                }*/

        /*	$products=$this->fct->getAll_cond('products','id_products',array('title_url'=>""));
            echo "<pre>";
            print_r($products);exit;*/

        $account_status = $this->ecommerce_model->updateGroupCategoryProducts();
        if (checkUserIfLogin()) {
            $login = true;
        } else {
            $login = false;
        }
        $data['login'] = $login;
        if (empty($title_url)) {
            $url = route_to('products') . '?pagination=on';
        } else {
            $url = route_to('products/index/' . $title_url) . '?pagination=on';
        }

        $data['current_url'] = $url;
        $data['title'] = 'Products';
        $data['page'] = "";
        $cond['products.status'] = 0;
        $sort_order = 'products.sku';
        $dir = "";

        if ($this->input->get('show_items')) {
            $show_items = $this->input->get('show_items');
        } else {
            $show_items = "40";
        }

        $show_items = "40";

        if ($title_url == "new-arrivals") {
            $cond['set_as_news'] = 1;
            $data['title'] = 'New Arrivals';
            $data['page'] = "new-arrivals";
            $id_seo = 18;
        }

        if ($title_url == "offers") {
            $cond['offers_month'] = 1;
            $data['title'] = 'Offers of The Month';
            $data['page'] = "offers";
            $id_seo = 19;
        }

        if ($title_url == "clearance") {
            $cond['clearance'] = 1;
            $data['title'] = 'Clearance';
            $data['page'] = "clearance";
            $id_seo = 20;
        }

        if ($title_url == "miles") {
            $cond['miles'] = 1;
            $data['title'] = 'miles';
            $data['page'] = "miles";
            $sort_order = 'products.sku';
            $id_seo = 21;
        }

        if ($this->input->get('sort')) {
            $sort_order = $this->ecommerce_model->getSortOrder($this->input->get('sort'));
        }


        if (isset($id_seo) && !empty($id_seo)) {
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array(
                'id_static_seo_pages' => $id_seo
            ));
        }
        $count = $this->ecommerce_model->getProducts($cond);

        $data['show_items'] = $show_items;
        $show_items = ($show_items == 'All') ? $count_news : $show_items;
        $cc = $count;
        $config['base_url'] = $url;
        $config['total_rows'] = $cc;
        $config['num_links'] = '8';
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['per_page'] = $show_items;

        $this->pagination->initialize($config);
        if ($this->input->get('per_page')) {
            if ($this->input->get('per_page') != "")
                $page = $this->input->get('per_page');
            else
                $page = 0;
        } else
            $page = 0;
        $data['count'] = $cc;
        $data['per_page'] = $page;
        $num_page_prev = 1;
        if ($page > 0) {
            $num_page = intval($page / $show_items);
        } else {
            $num_page = 0;
        }

        $data['products'] = $this->ecommerce_model->getProducts($cond, $show_items, $page, $sort_order, $dir);


        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'blocks/breadcrumbs', $data);
        $this->template->write_view('content', 'content/products', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);
        $this->template->render();

    }


    ///////////////////////////////////////////////////////////////////////
    public function details($product_url = '', $classifications_url = "", $category_url = "")
    {

        if (is_numeric($product_url)) {
            $cond_p['id_products'] = $product_url;
        } else {
            $cond_p['title_url'] = $product_url;
        }
        $product = $this->fct->getonerecord('products', $cond_p);

        // get product attribute options

        if (checkUserIfLogin()) {
            $login = true;
        } else {
            $login = false;
        }
        $data['login'] = $login;
        if ($this->input->get('miles') == 1 && $product['set_as_redeemed_by_miles']) {
            $data['redeem'] = 1;
        }

        if ($product_url != '' && !empty($product)) {
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array(
                'id_static_seo_pages' => 6
            ));
            $data['deliver_return'] = $this->fct->getonerecord('dynamic_pages', array(
                'id_dynamic_pages' => 5
            ));
            $data['page_title'] = $data['seo']['title'];

            $data['categories'] = $this->ecommerce_model->getProductsCategories('', 1000, 0);
            //$data['brands']     = $this->fct->getAll("brands", "sort_order");


            if ($this->session->userdata('login_id')) {
                $userData = $this->ecommerce_model->getUserInfo();
            }


            $product_url = urldecode($product_url);
            $product_url = xss_clean($product_url);
            $cond['products.deleted'] = 0;
            $data['product'] = $this->ecommerce_model->getOneProduct($product_url, $cond);

            ////TRACING//////
            $insert_log['section'] = $data['product']['title'];
            $this->fct->insertNewLog($insert_log);

            $data['seo'] = $data['product'];

            // js files
            $this->template->add_js('front/js/jquery.elevatezoom.min.js');
            /*$account_product_type = $data['product']['account_type'];*/

            /*$data['category']        = $data['product']['category'];*/


            /*    if (empty($data['product'])) {
                    redirect(route_to('products'));
                } else {*/
            if (empty($data['product']['meta_title']))
                $data['seo']['meta_title'] = $data['product']['title'];

            if (!empty($data['product']['meta_description']))
                $data['seo']['meta_description'] = $data['product']['meta_description'];

            if (!empty($data['product']['meta_keywords']))
                $data['seo']['meta_keywords'] = $data['product']['meta_keywords'];

            if (!empty($data['product']['gallery'][0]['image'])) {
                $data['og_image'] = base_url() . 'uploads/products/gallery/516x744/' . $data['product']['gallery'][0]['image'];
            }


            $this->fct->addVisit("products", $this->input->ip_address(), $data['product']['id_products']);

            $data['recently_viewed_products'] = $this->ecommerce_model->RecentlyViewed($data['product']['id_products']);

            $id_user = 0;
            if ($this->session->userdata('login_id')) {
                $id_user = $this->session->userdata('login_id');
            }
            $compare_basket = $this->ecommerce_model->getUserComparedProducts($id_user, $this->session->userdata("session_id"));
            $count = $this->config->item('max_compare') - count($compare_basket);
            $data['c_count'] = $count;
            $data['compare_basket'] = $compare_basket;


            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);


            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            /*        if($this->session->userdata('prev_page_name')){
            $breadcrumbs .='<li><a href='.$this->session->userdata('prev_link').'>'.$this->session->userdata('prev_page_name').'</a></li>';
            $breadcrumbs .='<li class="divider">></li>';
            }*/
            /*if(isset($data['category']) && !empty($data['category'])){
            $breadcrumbs .='<li class="divider">></li>';
            $breadcrumbs .='<li><a href='.route_to('products').'?pagination=on&category='.$data['category']['title_url'].'>'.$data['category']['title'].'</a></li>';
            }*/

            $breadcrumbs .= '<li>' . $data['product']['title'] . '</li>';
            $breadcrumbs .= '</ul>';

            $data['breadcrumbs'] = $breadcrumbs;
            $limit_r = 25;
            $data['related_products'] = $this->ecommerce_model->getRelatedProducts($data['product']['id_products'], $limit_r);

            $data['products_viewed'] = $this->ecommerce_model->RecentlyViewed($data['product']['id_products'], 5, false);


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'content/products_details', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);

            $this->template->render();
            /* }*/

            $this->serviceLocator->clicksService()->addClickOnProduct(
                $this->session->userdata('login_id'),
                $product['id_brands'],
                $product['id_products']
            );

            $this->serviceLocator->buyersService()->addBuyerForProductView(
                $this->session->userdata('login_id'),
                $product['id_brands']
            );

        } else {
            redirect(route_to('products'));
        }
    }


    ///////////////////////////////////////////////////////////////////////
    public function details3($product_url = '', $classifications_url = "", $category_url = "")
    {

        if (is_numeric($product_url)) {
            $cond_p['id_products'] = $product_url;
        } else {
            $cond_p['title_url'] = $product_url;
        }
        /*$product=$this->fct->getonerecord('products',$cond_p);*/
        $product = array(412412);
        $data['product'] = $product;
        // get product attribute options

        /*  if(checkUserIfLogin()){$login=true;}else{$login=false;}*/
        $login = true;
        $data['login'] = $login;
        /*	  if($this->input->get('miles')==1 && $product['set_as_redeemed_by_miles']){
              $data['redeem']=1;}*/

        if ($product_url != '' && !empty($product)) {
            /*            $data['seo']            = $this->fct->getonerow('static_seo_pages', array(
                            'id_static_seo_pages' => 6
                        ));
                        $data['deliver_return'] = $this->fct->getonerecord('dynamic_pages', array(
                            'id_dynamic_pages' => 5
                        ));
                        $data['page_title']     = $data['seo']['title'];*/

            /*$data['categories'] = $this->ecommerce_model->getProductsCategories('', 1000, 0);*/
            // $data['brands']     = $this->fct->getAll("brands", "sort_order");
            /*      if ($this->session->userdata('login_id')) {
                      $userData = $this->ecommerce_model->getUserInfo();
                  }*/


            /*         $product_url = urldecode($product_url);
                     $product_url = xss_clean($product_url);*/
            $cond['products.deleted'] = 0;
            /*$data['product'] = $this->ecommerce_model->getOneProduct($product_url,$cond);*/

            ////TRACING//////
            /*	$insert_log['section']=$data['product']['title'];
                $this->fct->insertNewLog($insert_log);*/

            $data['seo'] = $data['product'];

            // js files
            /*$this->template->add_js('front/js/jquery.elevatezoom.min.js');*/
            /*$account_product_type = $data['product']['account_type'];*/

            /*$data['category']        = $data['product']['category'];*/


            /*    if (empty($data['product'])) {
                    redirect(route_to('products'));
                } else {*/
            /*              if (empty($data['product']['meta_title']))
                              $data['seo']['meta_title'] = $data['product']['title'];

                          if (!empty($data['product']['meta_description']))
                              $data['seo']['meta_description'] = $data['product']['meta_description'];

                          if (!empty($data['product']['meta_keywords']))
                              $data['seo']['meta_keywords'] = $data['product']['meta_keywords'];

                          if (!empty($data['product']['gallery'][0]['image'])) {
                          $data['og_image'] = base_url() . 'uploads/products/gallery/516x744/'.$data['product']['gallery'][0]['image'];
                          }
                          */


            /*                $this->fct->addVisit("products", $this->input->ip_address(), $data['product']['id_products']);

                            $data['recently_viewed_products'] = $this->ecommerce_model->RecentlyViewed($data['product']['id_products']);

                            $id_user = 0;
                            if ($this->session->userdata('login_id')) {
                                $id_user = $this->session->userdata('login_id');
                            }
                            $compare_basket         = $this->ecommerce_model->getUserComparedProducts($id_user, $this->session->userdata("session_id"));
                            $count                  = $this->config->item('max_compare') - count($compare_basket);
                            $data['c_count']        = $count;
                            $data['compare_basket'] = $compare_basket;*/


            /*     $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                 $this->session->set_userdata('redirect_link', $url);*/


            $breadcrumbs = "";
            /*         $breadcrumbs .= '<ul>';
                     $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';
                     $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';*/
            /*        if($this->session->userdata('prev_page_name')){
            $breadcrumbs .='<li><a href='.$this->session->userdata('prev_link').'>'.$this->session->userdata('prev_page_name').'</a></li>';
            $breadcrumbs .='<li class="divider">></li>';
            }*/
            /*if(isset($data['category']) && !empty($data['category'])){
            $breadcrumbs .='<li class="divider">></li>';
            $breadcrumbs .='<li><a href='.route_to('products').'?pagination=on&category='.$data['category']['title_url'].'>'.$data['category']['title'].'</a></li>';
            }*/

            $breadcrumbs .= '<li>' . $data['product']['title'] . '</li>';
            $breadcrumbs .= '</ul>';

            /*       $data['breadcrumbs']      = $breadcrumbs;
                   $limit_r                  = 25;
                   $data['related_products'] = $this->ecommerce_model->getRelatedProducts($data['product']['id_products'], $limit_r);

                      $data['products_viewed'] = $this->ecommerce_model->RecentlyViewed($data['product']['id_products'],4,false);*/


            $this->template->write_view('header', 'blocks/header', $data);
            /*	$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
                $this->template->write_view('content', 'content/products_details', $data);*/
            $this->template->write_view('footer', 'blocks/footer', $data);

            $this->template->render();
            /* }*/

        } else {
            redirect(route_to('products'));
        }
    }

    public function search_by_keyword()

    {


        $keyword = $_POST['term'];
        $cond['keywords'] = $keyword;
        $cond['autocomplete'] = 1;
        $cond['brand'] = $this->input->post('id_brands');
        $cond['id_categories'] = $this->input->post('id_categories');

        $count = $this->ecommerce_model->getProducts($cond);
        $results = $this->ecommerce_model->getProducts($cond, $count, 0, 'sku', 'asc');


        $cc = $count;

        $i = 0;

        echo '[';
        if (!empty($results)) {
            foreach ($results as $result) {

                $i++;

                if ($i == $cc) $coma = '';

                else $coma = ',';

                echo '{"label" : "' . str_replace("'", "", $result['title']) . '","value":"' . $result['title_url'] . '"}' . $coma;

//echo '"'.str_replace("'","",$result['title']).'"'.$coma;

            }

        }

        echo ']';

    }


    public function checkStock($section = 'main')
    {
        $id_products = $this->input->post('id_products');

        //new ways - after 18.01.2018
        $product = $this->fct->getonerow('products', array('id_products' => $id_products));
        $result['result'] = 1;
        $result['status'] = '<span class="available">' . lang('available_in_stock') . '</span>';
        $result['price'] = '';
        $result['retail_price'] = "";
        $result['discount'] = '';

        if ($product['discount'] > 0) {
            $result['discount'] = '<div class="dis">' . lang('off') . ' ' . $product['discount'] . ' %' . '</div>';
            if (!empty($product['discount_expiration']) && $product['discount_expiration'] != '0000-00-00') {
                $result['discount'] .= '<div class="dis newEx" data-countdown="' . changeToCountDownDate($product['discount_expiration']) . '"></div><script>$(document).ready(function(){ $(".newEx").each(function() {var $this = $(this), finalDate = $(this).data("countdown");$this.countdown(finalDate, function(event) {$this.html(event.strftime("%D ' . lang('days') . ' %H:%M:%S"));});});});</script>';
            }
            $result['sku'] = '<label><span class="lbl_t" style="display:block">Item Number: </span>' . $product['sku'] . '</label>';


            $result['price'] .= '  <span class="price new_price">' . changeCurrency(displayCustomerPrice($product['list_price'], $product['discount_expiration'], $product['price'])) . '</span>';
            if (displayWasCustomerPrice($product['list_price']) != displayCustomerPrice($product['list_price'], $product['discount_expiration'], $product['price'])) {
                $result['price'] .= '<span class="price old_price" itemprop="price">' . changeCurrency(displayWasCustomerPrice($product['list_price'])) . '</span>';
            }

        } else {
            $result['price'] .= '<span class="price new_price" itemprop="price">' . changeCurrency(displayCustomerPrice($product['list_price'], $product['discount_expiration'], $product['price'])) . '</span>';
        }

        if (isset($product['retail_price']) && !empty($product['retail_price']) && $product['retail_price'] > 0) {
            $result['retail_price'] = '<span class="lbl_t" style="display:block">' . lang('suggester_retail_price') . '</span>' . changeCurrency(displayWasCustomerPrice($product['retail_price']));
        }

        $result['price_segments'] = '';
        $price_segments = $this->fct->getAll_cond("product_price_segments", 'sort_order', array("id_products" => $id_products));
        //print_r($price_segments);
        if (!empty($price_segments) && checkUserIfLogin()) {
            $result['price_segments'] .= '<ul id="qty_segements" class="combobox">';
            foreach ($price_segments as $segment) {
                $price_segments = 'Get ' . $segment['min_qty'] . ' for ' . changeCurrency($segment['price'], false) . ' ' . $this->session->userdata('currency') . ' only';
                $result['price_segments'] .= '<option value="' . $segment['min_qty'] . '">' . $price_segments . '</option>';

            }
            $result['price_segments'] .= '</ul>';
        } else {
            $result['price_segments'] .= '<input   class="textbox qty-txt" id="qty" type="text" name="qty"  value="1">';
        }


        if ($result['hide_price'] == 1) {
            $result['price'] = '<a onclick="CallEnquiryForm(' . $product['id_products'] . ')" class="btn3_pg1 full">' . lang("send_enquiry") . '</a>';
        }

        echo json_encode($result);
        return;

        //old ways - before 18.01.2018

        $id_attributes = $this->input->post('id_attributes');
        $options = $this->input->post('arr');
        $user = array();
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
        }

        //$options = sort($options);
        $product = $this->fct->getonerow('products', array('id_products' => $id_products));
        $stock_status = $product['stock_status'];

        $cc = count($options);
        $i = 0;
        $combination = '{';
        foreach ($options as $opt) {
            $i++;
            $d = $this->fct->getonerow('attribute_options', array('id_attribute_options' => $opt));
            $combination .= $d['id_attribute_options'] . ':' . $d['title'];
            if ($i != $cc) $combination .= ',';
        }
        $combination .= '}';

        $stock = $this->fct->getonerow('products_stock', array('status' => 1, 'id_products' => $id_products, 'combination' => $combination));
        $stock = $this->ecommerce_model->updateProduct($stock, 'stock');
        $result['sku'] = $stock['sku'];
        /*$checkFavorites=array();
        sort($options);
        if(!empty($user)){
        $options_str=implode(',',$options);
        $checkFavorites = $this->fct->getonerow('favorites',array('id_products'=>$id_products,'options'=>$options_str,'id_user'=>$user['id_user']));}

        if(empty($checkFavorites)){
            $result['favorites_results'] = 0;
            }else{
            $result['favorites_results'] = 1;
        }*/

        $result['favorites_results'] = 0;

        /*echo $this->db->last_query();exit;*/

        //PW added false to skip stock checking
        if (false && $stock['quantity'] == 0 && $stock_status == 0) {
            $result['result'] = 0;
            $result['status'] = '<span class="not_available">' . lang('not_available_in_stock') . '</span>';
        } else {
            $result['result'] = 1;
            $result['status'] = '<span class="available">' . lang('available_in_stock') . '</span>';
        }

        $result['miles'] = '';
        if (isset($stock['miles']) && $stock['miles'] > 0 && $stock['set_general_miles'] == 1) {
            $result['miles'] .= '<div class="earned">';
            $result['miles'] .= '<i class="icon">';
            $result['miles'] .= '<img src="' . base_url() . 'front/img/miles.png">';
            $result['miles'] .= '</i> Earn  <span class="miles-label">' . round($stock['miles']) . '</span>';
            $result['miles'] .= ' miles with this purchase</div>';
        }

        //$result['price'] = lang('price');
        $result['price'] = '';
        $result['retail_price'] = "";
        $result['hide_price'] = $stock['hide_price'];
        $result['discount'] = '';
        if ($this->input->post('redeem') != 1) {
            if ($stock['discount'] != 0) {
                $result['discount'] = '<div class="dis">' . lang('off') . ' ' . $stock['discount'] . ' %' . '</div>';
                if (!empty($stock['discount_expiration']) && $stock['discount_expiration'] != '0000-00-00') {
                    $result['discount'] .= '<div class="dis newEx" data-countdown="' . changeToCountDownDate($stock['discount_expiration']) . '"></div><script>$(document).ready(function(){ $(".newEx").each(function() {var $this = $(this), finalDate = $(this).data("countdown");$this.countdown(finalDate, function(event) {$this.html(event.strftime("%D ' . lang('days') . ' %H:%M:%S"));});});});</script>';
                }
                $result['sku'] = '<label><span class="lbl_t" style="display:block">Item Number: </span>' . $stock['sku'] . '</label>';


                $result['price'] .= '  <span class="price new_price">' . changeCurrency(displayCustomerPrice($stock['list_price'], $stock['discount_expiration'], $stock['price'])) . '</span>';
                if (displayWasCustomerPrice($stock['list_price']) != displayCustomerPrice($stock['list_price'], $stock['discount_expiration'], $stock['price'])) {
                    $result['price'] .= '<span class="price old_price" itemprop="price">' . changeCurrency(displayWasCustomerPrice($stock['list_price'])) . '</span>';
                }

            } else {
                $result['price'] .= '<span class="price new_price" itemprop="price">' . changeCurrency(displayCustomerPrice($stock['list_price'], $stock['discount_expiration'], $stock['price'])) . '</span>';
            }

            if (isset($stock['retail_price']) && !empty($stock['retail_price']) && $stock['retail_price'] > 0) {
                $result['retail_price'] = '<span class="lbl_t" style="display:block">' . lang('suggester_retail_price') . '</span>' . changeCurrency(displayWasCustomerPrice($stock['retail_price']));
            }
        } else {
            if (!empty($stock['redeem_miles']) && $stock['set_as_redeemed_by_miles'] == 1) {
                $redeem_miles = $stock['redeem_miles'] . ' miles';
            } else {
                $redeem_miles = lang('you_cannot_redeem');
            }
            $result['price'] .= '<span class="price new_price" itemprop="price">' . $redeem_miles . '</span>';
        }
        $result['price_segments'] = '';
        $price_segments = $this->fct->getAll_cond("product_price_segments", 'sort_order', array("id_products" => $stock['id_products'], "id_stock" => $stock['id_products_stock']));
        //print_r($price_segments);
        if (!empty($price_segments) && checkUserIfLogin()) {
            $result['price_segments'] .= '<ul id="qty_segements" class="combobox">';
            foreach ($price_segments as $segment) {
                $price_segments = 'Get ' . $segment['min_qty'] . ' for ' . changeCurrency($segment['price'], false) . ' ' . $this->session->userdata('currency') . ' only';
                $result['price_segments'] .= '<option value="' . $segment['min_qty'] . '">' . $price_segments . '</option>';

            }
            $result['price_segments'] .= '</ul>';
        } else {
            $result['price_segments'] .= '<input   class="textbox qty-txt" id="qty" type="text" name="qty"  value="1">';
        }


        if ($result['hide_price'] == 1)
            $result['price'] = '<a onclick="CallEnquiryForm(' . $product['id_products'] . ')" class="btn3_pg1 full">' . lang("send_enquiry") . '</a>';
        //else
        //$result['price'] .= changeCurrency(displayCustomerPrice($stock['list_price'],$stock['discount_expiration'],$stock['price']));
        echo json_encode($result);
    }


    public function getMiles()
    {
        if (checkUserIfLogin()) {
            $id_products = $this->input->post('id_product');
            $qty = $this->input->post('qty');
            $options = $this->input->post('arr');
            $miles = 0;
            $result = 1;

            /////////////////////GET PRODUCT DETAILS//////////////
            $product = $this->fct->getonerow('products', array('id_products' => $id_products));
            $price = $product['price'];
            $general_miles = $product['general_miles'];
            $set_general_miles = $product['set_general_miles'];
            $miles = $product['miles'];
            $price_segments = $this->fct->getAll_cond("product_price_segments", 'min_qty asc', array("id_products" => $product['id_products']));

            /////////////////////GET PRODUCT DETAILS//////////////
            if (!empty($options)) {
                $cc = count($options);
                $i = 0;
                $combination = '{';
                foreach ($options as $opt) {
                    $i++;
                    $d = $this->fct->getonerow('attribute_options', array('id_attribute_options' => $opt));
                    $combination .= $d['id_attribute_options'] . ':' . $d['title'];
                    if ($i != $cc) $combination .= ',';
                }
                $combination .= '}';

                $stock = $this->fct->getonerow('products_stock', array('status' => 1, 'id_products' => $id_products, 'combination' => $combination));

                if (!empty($stock)) {

                    $price_segments = $this->fct->getAll_cond("product_price_segments", 'min_qty asc', array("id_products" => $stock['id_products'], "id_stock" => $stock['id_products_stock']));
                    $price = $stock['price'];
                    $general_miles = $stock['general_miles'];
                    $set_general_miles = $stock['set_general_miles'];
                    $miles = $stock['miles'];

                }
            }


            /////////////////////CHECK PRICES SEGEMENTS MILES//////////////
            foreach ($price_segments as $price_segment) {
                if ($price_segment['price'] != '' && $price_segment['price'] < $price && $price_segment['min_qty'] <= $qty) {
                    $price = $price_segment['price'];
                }
            }

            if ($general_miles >= 0 && $general_miles <= 100 && $set_general_miles == 1) {
                /*$miles=getMiles($general_miles,$price);*/
                $miles = $miles * $qty;
                $result = 1;
            }


            $return['miles'] = round($miles);
            $return['result'] = $result;
            die(json_encode($return));
        }
    }

    public function getMultiQty()
    {
        $results = array();
        $id_products = $this->input->post('id_products');
        $options = $this->input->post('combination');
        $options = explode(",", $options);

        arsort($options);

        $i = 0;
        $combination = '{';
        $cc = count($options);
        foreach ($options as $opt) {
            $i++;

            $d = $this->fct->getonerow('attribute_options', array('id_attribute_options' => $opt));

            $combination .= $d['id_attribute_options'] . ':' . $d['title'];
            if ($i != $cc) $combination .= ',';
        }
        $combination .= '}';

        $options = $this->input->post('arr');
        $product = $this->fct->getonerow('products', array('id_products' => $id_products));
        $check_stock = $this->ecommerce_model->getProductStock(array('id_products' => $id_products));
        $cc = count($options);
        $i = 0;


        $cond['id_products'] = $id_products;
        $stock = array();


        $stock = $this->fct->getonerow('products_stock', array('status' => 1, 'id_products' => $id_products, 'combination' => $combination));


        if (!empty($check_stock)) {
            if (!empty($stock)) {
                $id_products_stock = $stock['id_products_stock'];
            } else {
                $id_products_stock = 0;
            }
            $cond['id_stock'] = $id_products_stock;
        }
        $cond['id_products'] = $id_products;
        $results = $this->fct->getAll_cond("product_price_segments", 'sort_order', $cond);


        //print_r($price_segments);


//$results=$this->fct->getimagegallery($keyword);
        $price_segments = "";
        $cc = count($results);
        $i = 0;
        echo '[';
        if (!empty($results)) {
            $price_segments = "";
            foreach ($results as $segment) {
                /*$price_segments .= 'Get '.$segment['min_qty'].' for '.changeCurrency($segment['price'],false).' only';*/
                $price_segments = 'Get ' . $segment['min_qty'] . ' for ' . changeCurrency($segment['price'], false) . ' ' . $this->session->userdata('currency') . ' only';
                $i++;
                if ($i == $cc) $coma = '';
                else $coma = ',';
//$res = preg_replace('/\.[^.]+$/','',$result['title']);
//$res =str_replace(".jpg","",$result['image']);
                echo '{"label" : "' . str_replace("'", "", $price_segments) . '", "value": "' . $segment['min_qty'] . '__' . $price_segments . '"}' . $coma;
//echo '"'.str_replace("'","",$result['title']).'"'.$coma;
            }
        }
        echo ']';

        //else
        //$result['price'] .= changeCurrency(displayCustomerPrice($stock['list_price'],$stock['discount_expiration'],$stock['price']));

    }

    public function notify($title_url = "")
    {
        if (checkUserIfLogin()) {
            $userData = $this->ecommerce_model->getUserInfo();
            $product = $this->fct->getonerecord('products', array('title_url' => $title_url));
            if (!empty($product) && $product['availability'] == 1) {
                $check = $this->fct->getonerecord('products_notify', array('id_products' => $product['id_products'], 'id_user' => $userData['id_user'], 'status' => 0));
                if (empty($check)) {
                    $insert['id_products'] = $product['id_products'];
                    $insert['id_user'] = $userData['id_user'];
                    $insert['created_date'] = date('Y-m-d h:i:s');
                    $this->db->insert('products_notify', $insert);
                    $this->session->set_flashdata('success_message', 'Alert subscription has been saved.');
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $update['id_products'] = $product['id_products'];
                    $update['id_user'] = $userData['id_user'];
                    $update['status'] = 0;
                    $update['created_date'] = date('Y-m-d h:i:s');
                    $this->db->where(array('id_products_notify' => $check['id_products_notify']));
                    $this->db->update('products_notify', $update);
                    $this->session->set_flashdata('success_message', 'Alert subscription has been saved.');
                }
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('error_message', lang('access_denied'));
                redirect(route_to('user/login'));
            }
        } else {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

    ///////////////////////////////////////////////////////////////////////
    public function category($id_category = "", $all = "")
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $data['page'] = "";
        $data['all'] = $all;
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        $data['section_list'] = 'categories';
        $false = true;
        if (!empty($id_category)) {
            $account_status = $this->ecommerce_model->updateGroupCategoryProducts($id_category);
            $category = $this->fct->getonerow("categories", array("id_categories" => $id_category));
            $bool = true;
        }
        if ($bool) {
            $like = array();
            $cond = array();
            $dir = "";
            $sort_order = 'products.sku';
            $cond['status'] = 0;

            /*$items = $this->custom_fct->getCategoriesPages($id_category,$cond,$like,100,0);
            $categories_ids = $this->custom_fct->getCategoriesIdsTreeParentLevels($items,$id_category);
            $cond['categories'] =  implode(",",$categories_ids);	*/
            $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($id_category);
            $parent_levels = arrangeParentLevels($parent_levels);
            $data['category'] = $category;

            $cond2['id_parent'] = $id_category;

            if ($this->input->get('brand') != "") {
                $brand = $this->input->get('brand');

                if (!empty($brand))
                    $cond2['id_brands'] = $brand;
            }

            if ($this->input->get('keywords') != "") {
                $keywords = $this->input->get('keywords');

                if (!empty($keywords))
                    $cond2['keywords'] = $keywords;
            }
            $sub_categories = $this->custom_fct->getSubCategories($cond2, 100, 0);
            $data['sub_categories_cc'] = $sub_categories;


            /*$cond2['status']=0;
            $categories = $this->custom_fct->getCategoriesPages($id_category,$cond2,$like,100,0);
            $data['sub_categories']=$categories;*/


            if (checkUserIfLogin()) {
                $data['login'] = true;
            } else {
                $data['login'] = false;
            }

            $data['seo'] = $this->fct->getonerow('static_seo_pages', array(
                'id_static_seo_pages' => 6
            ));
            $data['page_title'] = $data['seo']['title' . getFieldLanguage()];


            if (empty($all)) {
                $url = route_to('products/category/' . $id_category);
            } else {
                $url = route_to('products/category/' . $id_category . '/' . $all);
            }
            $url .= '?pagination=on';
            $data['id_category'] = $id_category;

            $data['current_url'] = $url;

            if ($this->input->get('brand') != "") {
                $brand = $this->input->get('brand');
                $url .= '&brand=' . $brand;
                if (!empty($brand))
                    $cond['id_brands'] = $brand;
            }

            if ($this->input->get('brand') != "") {
                $brand = $this->input->get('brand');
                $url .= '&brand=' . $brand;
                if (!empty($brand))
                    $cond['id_brands'] = $brand;
            }

            if ($this->input->get('keywords') != "") {
                $keywords = $this->input->get('keywords');
                $url .= '&keywords=' . $keywords;
                if (!empty($keywords))
                    $cond['keywords'] = $keywords;
            }

            $cond['products.status'] = 0;

            $data['selected_category'] = $this->fct->getonerow("categories", array(
                "id_categories" => $id_category
            ));

            $data['seo'] = $data['selected_category'];

            if (!empty($category['meta_title'])) {
                $data['seo']['meta_title'] = $category['meta_title'];
            } else {
                $data['seo']['meta_title'] = $category['title'];
            }
            if (!empty($category['meta_description']))
                $data['seo']['meta_description'] = $category['meta_description'];

            if (!empty($category['meta_keywords']))
                $data['seo']['meta_keywords'] = $category['meta_keywords'];

            if (!empty($category['image'])) {
                $data['og_image'] = $category['image'];
            }


            if ($this->input->get('sort')) {
                $sort_order = $this->ecommerce_model->getSortOrder($this->input->get('sort'));
            }

            $arr_parent = array();
            $cc = count($parent_levels);
            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';

            $i = 0;
            foreach ($parent_levels as $lvl) {
                $i++;
                array_push($arr_parent, $lvl['id_categories']);
                if ($lvl['set_as_redeem_miles_categories'] == 1) {
                    $data['page'] = "miles";
                    $cond['miles'] = 1;
                }
                if ($i != $cc) {
                    $breadcrumbs .= '<li><a href="' . route_to('products/category/' . $lvl['id_categories']) . '">' . $lvl['title'] . '</a></li>';
                    $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
                } else {
                    $breadcrumbs .= '<li>' . $lvl['title'] . '</li>';
                }
            }
            $breadcrumbs .= '</ul>';


            $limit = 40;
            $show_items = $limit;
            $cond['id_categories'] = $category['id_categories'];


            $count_news = $this->ecommerce_model->getProducts($cond);

            /*if(isset($_GET['per_page'])) {
                if($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            }
            else $page = 0;*/

            $data['show_items'] = $show_items;
            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $cc = $count_news;
            $config['base_url'] = $url;
            $config['total_rows'] = $cc;
            $config['num_links'] = '8';
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $config['per_page'] = $show_items;

            $this->pagination->initialize($config);
            if ($this->input->get('per_page')) {
                if ($this->input->get('per_page') != "")
                    $page = $this->input->get('per_page');
                else
                    $page = 0;
            } else
                $page = 0;
            $data['count'] = $cc;
            $data['per_page'] = $page;
            $num_page_prev = 1;
            if ($page > 0) {
                $num_page = intval($page / $show_items);
            } else {
                $num_page = 0;
            }

            $data['products'] = $this->ecommerce_model->getProducts($cond, $limit, $page, $sort_order, $dir);


            $data['show_items'] = $limit;
            $data['count'] = $count_news;
            $data['offset'] = $page;


            $id_user = 0;
            if ($this->session->userdata('login_id')) {
                $id_user = $this->session->userdata('login_id');
            }


            $page_ads = "categories";
            $block_ads = "pages_top";
            $category_ads = $category['title'];
            $id_user_ads = "";
            $type = "";
            $data['advertisements'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads, $category_ads, $id_user_ads);


            $data['arr_parent'] = $arr_parent;


            $data['breadcrumbs'] = $breadcrumbs;

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);

            $this->template->write_view('content', 'content/category', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);

            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(site_url(''));
        }
    }

    ///////////////////////////////////////////////////////////////////////
    public function brand($brand_url)
    {
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array(
            'id_static_seo_pages' => 6
        ));
        $data['page_title'] = $data['seo']['title' . getFieldLanguage()];

        $data['categories'] = $this->ecommerce_model->getProductsCategories('', 1000, 0);
        $data['brands'] = $this->fct->getAll("brands", "sort_order");

        $brand_url = urldecode($brand_url);
        $brand_url = xss_clean($brand_url);

        $url = route_to('products/brand/' . $brand_url);
        $url .= '?pagination=on';
        $data['brand_url'] = $brand_url;

        $cond = array();
        $cond['products.status'] = 0;
        if ($brand_url != "") {
            $cond['brands.title_url'] = $brand_url;
            $data['selected_brand'] = $this->fct->getonerow("brands", array(
                "title_url" => $brand_url
            ));
            if (!empty($data['selected_brand']['meta_title']))
                $data['seo']['meta_title'] = $data['selected_brand']['meta_title'];

            if (!empty($data['selected_brand']['meta_description']))
                $data['seo']['meta_description'] = $data['selected_brand']['meta_description'];

            if (!empty($data['selected_brand']['meta_keywords']))
                $data['seo']['meta_keywords'] = $data['selected_brand']['meta_keywords'];
        }

        $cc = $this->ecommerce_model->getProducts($cond);
        $config['base_url'] = $url;
        $config['total_rows'] = $cc;
        $config['num_links'] = '8';
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['per_page'] = 8;
        $this->pagination->initialize($config);
        if (isset($_GET['per_page'])) {
            if ($_GET['per_page'] != '')
                $page = $_GET['per_page'];
            else
                $page = 0;
        } else
            $page = 0;
        $data['count'] = $cc;
        $data['products'] = $this->ecommerce_model->getProducts($cond, $config['per_page'], $page);

        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        $id_user = 0;
        if ($this->session->userdata('login_id')) {
            $id_user = $this->session->userdata('login_id');
        }
        $compare_basket = $this->ecommerce_model->getUserComparedProducts($id_user, $this->session->userdata("session_id"));
        $count = $this->config->item('max_compare') - count($compare_basket);
        $data['c_count'] = $count;
        $data['compare_basket'] = $compare_basket;

        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('content', 'content/products', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
    }

    ///////////////////////////////////////////////////////////////////////
    public function add_to_favorite()
    {
        if (checkUserIfLogin()) {

            /*print '<pre>';
            print_r($_POST);
            exit;*/
            $data['lang'] = $this->lang->lang();
            $lang = $this->lang->lang();
            $id_products = $this->input->post('product_id');
            $qty = $this->input->post('qty');
            $qty_month = $qty;
            $options = array();
            $stock_text = '';
            if (isset($_POST['options']) && !empty($_POST['options'])) {
                $options = $this->input->post('options');
                //print '<pre>';print_r($options);
                sort($options);
                $options_text = implode(',', $options);
                //print '<pre>';print_r($options);exit;
            }
            $product = $this->fct->getonerow('products', array('id_products' => $id_products));


            $cond_check = array(
                'id_products' => $id_products,
                'id_user' => $this->session->userdata('login_id')
            );
            if (!empty($options)) {
                $cond_check['options'] = $options_text;
                $results = $this->ecommerce_model->getOptionsByIds($options);
                foreach ($results as $res) {
                    $stock_text .= $res['title' . getFieldLanguage($this->lang->lang())] . ' ';
                }
            }

            $check = $this->fct->getonerow('favorites', $cond_check);
            if (!empty($check)) {

                $message = $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . lang('was_alread_added');
                if ($this->input->post('popup_favorites') != "") {

                    $return['result'] = 1;
                    $return['message'] = $message;

                    die(json_encode($return));
                } else {
                    $this->session->set_flashdata('success_message', $message);
                    if ($this->session->userdata('redirect_from_favorites')) {
                        $redirect_from_favorites = $this->session->userdata('redirect_from_favorites');
                        $this->session->unset_userdata('redirect_from_favorites');
                        redirect($redirect_from_favorites);
                    } else {
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            } else {
                $data = array();
                $data['qty'] = $qty;
                $data['qty_month'] = $qty_month;
                $data['id_products'] = $product['id_products'];
                $data['id_user'] = $this->session->userdata('login_id');
                $data['created_date'] = date('Y-m-d h:i:s');
                if (!empty($options)) {
                    $data['options'] = $options_text;
                }
                $this->db->insert('favorites', $data);
                $new_id = $this->db->insert_id();
                if ($this->input->post('categories') != "") {
                    $categories = $this->input->post('categories');

                    $stocklist_categories = explode(',', $categories);

                    $cond2['id_user'] = $data['id_user'];
                    $cond2['id_favorites'] = $new_id;
                    $this->pages_fct->insert_user_checklist_categories2($cond2, $stocklist_categories);
                }
                $message = $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . lang('added_to_favorites');

                if ($this->input->post('popup_favorites') != "") {

                    $return['result'] = 1;
                    $return['message'] = $message;
                    die(json_encode($return));
                } else {
                    $this->session->set_flashdata('success_message', $message);
                    if ($this->session->userdata('redirect_from_favorites')) {
                        $redirect_from_favorites = $this->session->userdata('redirect_from_favorites');
                        $this->session->unset_userdata('redirect_from_favorites');
                        redirect($redirect_from_favorites);
                    } else {
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            }
        } else {
            //$this->session->set_userdata('redirect_link',route_to('products/add_to_favorite/'.$product_url));
            $this->session->set_userdata('redirect_link', $_SERVER['HTTP_REFERER']);
            $this->session->set_userdata('redirect_from_favorites', $_SERVER['HTTP_REFERER']);
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

    ///////////////////////////////////////////////////////////////////////
    public function AjaxCompareProduct()
    {
        //if($this->session->userdata('login_id') != "") {
        $id_user = 0;
        if ($this->session->userdata('login_id')) {
            $id_user = $this->session->userdata('login_id');
        }
        $session_id = $this->session->userdata('session_id');
        $id_products = $this->input->post('id_products');
        $old = $this->ecommerce_model->getUserComparedProducts($id_user, $session_id);
        $count = count($old);
        if ($count >= $this->config->item('max_compare')) {
            $return['result'] = 0;
            $return['message'] = lang('compare_count_error');
        } else {
            $data = array();
            $data['id_products'] = $id_products;
            $data['id_user'] = $id_user;
            $data['session_id'] = $session_id;
            $data['created_date'] = date('Y-m-d h:i:s');
            //    print_r($data);exit;
            $this->db->insert('compare_products', $data);
            $compare_basket = $this->ecommerce_model->getUserComparedProducts($id_user, $session_id);
            $count = $this->config->item('max_compare') - count($compare_basket);
            $return['c_count'] = $count;
            $return['result'] = 1;
            $return['message'] = '<span class="success">' . lang('added_to_compare') . '</span>';
        }
        /*    }
        else {
        $return['result'] = 0;
        $return['message'] = lang('compare_login_error');
        $this->session->set_userdata('redirect_link',$_SERVER['HTTP_REFERER']);
        }*/
        echo json_encode($return);

    }

    public function AjaxRemoveCompareProduct()
    {
        $id_user = 0;
        if ($this->session->userdata('login_id')) {
            $id_user = $this->session->userdata('login_id');
        }
        $id_products = $this->input->post('id_products');
        $cond['id_products'] = $id_products;
        /*$this->db->where("session_id",$this->session->userdata('session_id'));
        if($id_user != 0)
        $this->db->or_where("id_user",$id_user);
        $this->db->delete('compare_products');*/
        $q = 'DELETE FROM compare_products WHERE id_products = ' . $id_products . ' AND ( session_id = "' . $this->session->userdata('session_id') . '"';
        if ($id_user != 0)
            $q .= ' OR id_user = ' . $id_user;
        $q .= ' )';
        $this->db->query($q);
        $return['result'] = 1;
        $return['message'] = '<span class="success">' . lang('deleted_from_compare') . '</span>';
        $compare_basket = $this->ecommerce_model->getUserComparedProducts($id_user, $this->session->userdata('session_id'));
        $count = $this->config->item('max_compare') - count($compare_basket);
        $return['c_count'] = $count;
        echo json_encode($return);
    }

    public function add_to_compare($product_url = '')
    {

        if ($this->session->userdata('login_id')) {
            if (empty($product_url)) {
                $this->session->set_flashdata('error_message', lang('no_product_selected'));
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $data['lang'] = $this->lang->lang();
                $lang = $this->lang->lang();
                if (isset($_POST['options']) && !empty($_POST['options'])) {

                    $options = $this->input->post('options');
                    if (!empty($options[0])) {
                        sort($options);
                        $options_text = implode(',', $options);
                    } else {
                        $options = array();
                    }

                }
                $product_url = urldecode($product_url);
                $product = $this->fct->getonerow('products', array(
                    'title_url' . getUrlFieldLanguage($lang) => $product_url
                ));
                if (empty($product)) {
                    $this->session->set_flashdata('error_message', lang('no_product_selected'));
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $cond_check = array(
                        'id_products' => $product['id_products'],
                        'id_user' => $this->session->userdata('login_id')
                    );
                    $check = $this->fct->getonerow('compare_products', $cond_check);
                    if (!empty($check)) {

                        $this->session->set_flashdata('success_message', 'The product ' . $product['title' . getFieldLanguage($lang)] . ' ' . lang('was_already_added_compare'));
                        if ($this->session->userdata('redirect_from_compare')) {
                            $redirect_from_compare = $this->session->userdata('redirect_from_compare');
                            $this->session->unset_userdata('redirect_from_compare');
                            redirect($redirect_from_compare);
                        } else {
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                    } else {
                        $data = array();
                        $data['id_products'] = $product['id_products'];
                        $data['id_user'] = $this->session->userdata('login_id');
                        $data['created_date'] = date('Y-m-d h:i:s');
                        if (!empty($options)) {
                            $data['options'] = $options_text;
                        }


                        $this->db->insert('compare_products', $data);
                        $this->session->set_flashdata('success_message', 'The product ' . $product['title' . getFieldLanguage($lang)] . ' ' . lang('added_to_compare'));
                        if ($this->session->userdata('redirect_from_compare')) {
                            $redirect_from_compare = $this->session->userdata('redirect_from_compare');
                            $this->session->unset_userdata('redirect_from_compare');
                            redirect($redirect_from_compare);
                        } else {
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                    }
                }
            }
        } else {
            $this->session->set_userdata('redirect_link', route_to('products/add_to_compare/' . $product_url));
            $this->session->set_userdata('redirect_from_compare', $_SERVER['HTTP_REFERER']);
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

    ///////////////////////////////////////////////////////////////////////

    function validate_captcha()
    {
        $row_count = 1;
        if (!isset($_POST['no_captcha'])) {
            $expiration = time() - 7200; // Two hour limit
            $this->db->query("DELETE FROM captcha WHERE captcha_time < " . $expiration);
            // Then see if a captcha exists:
            $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
            $binds = array(
                $_POST['captcha'],
                $this->input->ip_address(),
                $expiration
            );
            $query = $this->db->query($sql, $binds);
            $row = $query->row();
            $row_count = $row->count;
            if ($row_count == 0) {
                $this->form_validation->set_message('validate_captcha', lang('notcorrectcharacters'));
                return false;
            } else {
                return true;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////

    public function attributesOptions()
    {
        $return['result'] = 0;
        $product_url = $this->input->post('product_url');
        $product_url = urldecode($product_url);
        $product_url = xss_clean($product_url);
        $product = $this->ecommerce_model->getOneProduct($product_url);
        $attributes = $product['attributes'];

        foreach ($attributes as $attr) {
            if (!empty($attr['options'])) {
                $return['result'] = 1;
                die(json_encode($return));
            }
        }
        die(json_encode($return));
    }

    public function getProductAttributesOptions()
    {
        $return['result'] = 0;
        $product_url = $this->input->post('product_url');
        $product_url = urldecode($product_url);
        $product_url = xss_clean($product_url);
        $data['product'] = $this->ecommerce_model->getOneProduct($product_url);
        $data['redeem'] = $this->input->post('redeem');
        if (!empty($data['product'])) {
            $return['html'] = $this->load->view('load/productAttribuesOptions', $data, true);
            $return['result'] = 1;
        } else {
            $return['result'] = 1;
        }
        die(json_encode($return));

    }


    public function checkAccessCode()
    {
        $login = checkUserIfLogin();
        $id = $this->input->post('id');

        if (checkProductGroup($id)) {
            $return['result'] = 0;

            $user = $this->ecommerce_model->getUserInfo();

            $access_codes = unSerializeStock($user['access_code']);
            if (empty($access_codes)) {
                $access_codes = array();
            }

            $data['product'] = $this->fct->getonerecord('products', array('id_products' => $id));
            $brand = $this->fct->getonerecord('brands', array('id_brands' => $data['product']['id_brands']));
            $bool = false;


            if (in_array($brand['access_code'], $access_codes) || in_array($data['product']['access_code'], $access_codes)) {
                $bool = true;
            }

            if (!empty($data['product']) && !empty($brand) && (!empty($brand['access_code']) || !empty($data['product']['access_code'])) && $bool == false) {
                $data['brand'] = $brand;
                $return['html'] = $this->load->view('load/accessCode', $data, true);
                $return['result'] = 0;
            } else {
                $return['result'] = 1;
            }
        } else {
            $return['result'] = 4;
            $this->session->set_flashdata('error_message', lang('log_to_buy'));
            $return['redirect_link'] = route_to('user/login');
        }
        die(json_encode($return));
    }

    public function checkStockList()
    {
        if (checkUserIfLogin()) {
            $return['result'] = 0;
            $id = $this->input->post('id');
            if ($this->input->post('list') != "") {
                $data['list'] = 1;
            }

            if ($this->input->post('details') != "") {
                $data['details'] = 1;
            }
            $data['product'] = $this->ecommerce_model->getOneProduct($id);
            $user = $this->ecommerce_model->getUserInfo();
            $checklist_categories = $this->pages_fct->getUserCheckListCategories(array('id_user' => $user['id_user']), 10, 0);
            $data['info'] = $checklist_categories;

            $bool = false;


            if (!empty($checklist_categories)) {
                $return['html'] = $this->load->view('load/stockList_categories', $data, true);
                $return['result'] = 0;
            } else {
                $return['html'] = $this->load->view('load/stockList_categories', $data, true);
                $return['result'] = 0;
            }
        } else {
            $return['result'] = 4;
            $this->session->set_flashdata('error_message', lang('login_to_continue'));
            $return['redirect_link'] = route_to('user/login');
        }
        die(json_encode($return));
    }

    public function validateAccessCode()
    {
        $return['result'] = 0;
        $id_brands = $this->input->post('id_brands');
        $id_product = $this->input->post('id_product');
        $access_code = $this->input->post('access_code');
        $user = $this->ecommerce_model->getUserInfo();
        $access_codes = unSerializeStock($user['access_code']);
        if (empty($access_codes)) {
            $access_codes = array();
        }

        $brand = $this->fct->getonerecord('brands', array('id_brands' => $id_brands, 'access_code' => $access_code));
        $product = $this->fct->getonerecord('products', array('id_products' => $id_product, 'access_code' => $access_code));


        if (!empty($brand) || !empty($product)) {

            if (empty($brand)) {
                $id_brands = rand();
            }

            if (!in_array($access_code, $access_codes)) {
                $access_codes[$id_brands] = $this->input->post('access_code');

                $i = 0;
                $c = count($access_codes);
                $str = "";
                foreach ($access_codes as $key => $val) {
                    $i++;
                    $str .= $key . ':' . $val;
                    if ($i != $c) {
                        $str .= ',';
                    }
                }
                $update['access_code'] = $str;
                $this->db->where(array('id_user' => $user['id_user']));
                $this->db->update('user', $update);

            }

            $return['result'] = 1;
        } else {
            $return['result'] = 0;

        }
        die(json_encode($return));
    }


}