<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pages extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    public function customer_service($id_page)
    {

        if ($this->session->userdata('currency') == "") {
            $this->session->set_userdata('currency', $this->config->item("default_currency"));
        }

        if (!empty($id_page)) {
            $section = $this->fct->getonerecord('customer_service', array('title_url' => $id_page));

        }
        if (!empty($section)) {

            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href="' . site_url() . '" >Home</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>' . $section['title'] . '</li>';
            $breadcrumbs .= '</ul>';

            $data['breadcrumbs'] = $breadcrumbs;
            $data['seo'] = $section;
            $data['section'] = $section;
            if (empty($data['seo']['meta_title'])) {
                $data['seo']['meta_title'] = $section['title'];
            }

            $result = $this->getPageInfo1($id_page);
            $page = $result['page'];
            if (isset($result['info'])) {
                $data['info'] = $result['info'];
            }

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'content/' . $page, $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(site_url());
        }
    }

    public function about_spamiles($id_page)
    {


        if ($this->session->userdata('currency') == "") {
            $this->session->set_userdata('currency', $this->config->item("default_currency"));
        }

        if (!empty($id_page)) {
            $section = $this->fct->getonerecord('about_spamiles', array('id_about_spamiles' => $id_page));
        }
        if (!empty($section)) {

            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href="' . site_url() . '" >Home</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>' . $section['title'] . '</li>';
            $breadcrumbs .= '</ul>';

            $data['breadcrumbs'] = $breadcrumbs;
            $data['seo'] = $section;
            if (empty($data['seo']['meta_title'])) {
                $data['seo']['meta_title'] = $section['title'];
            }
            $data['section'] = $section;
            $result = $this->getPageInfo2($id_page);
            $page = $result['page'];
            if (isset($result['info'])) {
                $data['info'] = $result['info'];
            }

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'content/' . $page, $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(site_url());
        }
    }

    public function getPageInfo1($id)
    {
        switch ($id) {
            case '1':
                $data['page'] = "faq";
                $data['info'] = $this->fct->getAll('faq', 'sort_order');
                break;
            default:
                $data['page'] = "customer_service";
        }

        return $data;
    }

    public function getPageInfo2($id)
    {
        switch ($id) {
            case '3':
                $data['page'] = "testimonials";
                $data['info'] = $this->fct->getAll('testimonials', 'sort_order');
                break;
            case '6':
                $data['page'] = "site_map";
                break;
            default:
                $data['page'] = "about_spamiles";
        }

        return $data;
    }


}