<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class LandingPagePayment extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'payment/LandingPagePayment', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        if ($this->input->get('open') == "afterPaymentFailed") {
            $data['popup_content'] = $this->load->view('popup/afterPaymentFailed', $data, true);
            $this->template->write_view('footer', 'popup/template', $data);
        }

        $this->template->render();
    }
    
    public function send()
    {
        $randomNumber = $this->input->post('random-number');
        $planSelected = $this->input->post('plan-selected');
        $applicationType = $this->input->post('application-type');
        
        $cond = array("random_number" => $randomNumber);
        $data = $this->fct->getonerecord("user", $cond);
        if($applicationType == "1"){
            $this->load->model('send_emails');
            $this->send_emails->sendReplyToUaeFreeAppicant($data);
            $this->send_emails->sendEmailToAdminForFreeApplicant($data);
        } if($applicationType == "2"){
            $this->load->model('send_emails');
            $this->send_emails->sendReplyToOverseasAppicant($data);
            $this->send_emails->sendEmailToAdminForOverseasApplicant($data);
        }
        $return['result'] = 1;
        $return['message'] = lang('landing_success');
        $return['redirect'] = 'https://www.ebeautytrade.com/?open=afterApplicationSubmitted';
        echo json_encode($return);

    }
}