<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opening_new_spa extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}
	public function index()
	{	
	
		
		////TRACING//////	
		$insert_log['section']='events';
		$this->fct->insertNewLog($insert_log);
		
		$url=route_to('events').'?pagination=on';
		$data['new_url']=$url;
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>35));
		$data['page_title'] = $data['seo']['title'.getFieldLanguage()];
		$cond=array();
		
				$breadcrumbs = "";
                $breadcrumbs .= '<ul>';
                $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
       			$breadcrumbs .= '<li> Opening a New Spa</li>';
                $breadcrumbs .= '</ul>';
				
		$data['breadcrumbs']=$breadcrumbs;	
		
		$data['projects']=$this->fct->getAll('projects_type','sort_order');
		$data['services']=$this->fct->getAll('services_enquiry','sort_order');	

		$this->template->write_view('header','blocks/header',$data);
		$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
		$this->template->write_view('content', 'blocks/breadcrumbs', $data);
	
		$this->template->write_view('content','content/opening_new_spa',$data);
		$this->template->write_view('footer','blocks/footer',$data);
		$this->template->render();
	}
	
function validate_captcha()
{
	$row_count = 1;
	if(!isset($_POST['no_captcha'])) {
		$expiration = time()-7200; // Two hour limit
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		// Then see if a captcha exists:
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		$row_count = $row->count;
		if($row_count == 0) {
			$this->form_validation->set_message('validate_captcha',lang('notcorrectcharacters'));
			return false;
		}
		else {
			return true;
		}
	}
}
public function send()
{
		//print '<pre>';
		//print_r($_POST);exit;
		$allPostedVals = $this->input->post(NULL, TRUE);
		
		/*Pesonal Information*/
		
		$this->form_validation->set_rules('first_name','first name','trim|xss_clean|required');
		$this->form_validation->set_rules('last_name','last name','trim|xss_clean|required');
		$this->form_validation->set_rules('position','position','trim|xss_clean|required');
		$this->form_validation->set_rules('email','email','trim|required|valid_email');
		$this->form_validation->set_rules('mobile',lang('mobile'),'trim|xss_clean|required');
		$this->form_validation->set_rules('phone',lang('phone'),'trim|xss_clean');
		
		/*COMPANY DETAILS */
		$this->form_validation->set_rules('trade_name','trade name','trim|xss_clean');
		$this->form_validation->set_rules('website','website','trim|xss_clean');
		$this->form_validation->set_rules('city',lang('city'),'trim|xss_clean');
		$this->form_validation->set_rules('id_country','country','trim');
		if($this->input->post("company_email")!=""){
		$this->form_validation->set_rules('company_email',lang('email'),'trim|valid_email');}
		$this->form_validation->set_rules('company_phone','phone','trim|xss_clean');

		
		//$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');
		if($this->form_validation->run() == FALSE) {
			$return['result'] = 0;
			$return['errors'] = array();
			$return['message'] = 'Error!';
			//$return['captcha'] = $this->fct->createNewCaptcha();
			$find =array('<p>','</p>');
			$replace =array('','');
			foreach($allPostedVals as $key => $val) {
				if(form_error($key) != '') {
					$return['errors'][$key] = str_replace($find,$replace,form_error($key));
				}
			}
		}
		else {
		
			$_data['salutation'] = $this->input->post('salutation');
			$_data['id_consultants'] = $this->input->post('consultant');
			$_data['first_name'] = $this->input->post('first_name');
			$_data['last_name'] = $this->input->post('last_name');
			$_data['name'] = $_data['first_name'].' '.$_data['last_name'];
			$_data['position'] = $this->input->post('position');
			$_data['email'] = $this->input->post('email');
			$_data['mobile'] = $this->input->post('mobile');
			$_data['phone'] = $this->input->post('phone');
			$_data['phone'] = $this->input->post('phone');
			$_data['trade_name'] = $this->input->post('trade_name');
			$_data['website'] = $this->input->post('website');
			$_data['address'] = $this->input->post('address');
			$_data['city'] = $this->input->post('city');
			$_data['id_countries'] = $this->input->post('id_countries');
			$_data['company_email'] = $this->input->post('company_email');
			$_data['company_phone'] = $this->input->post('company_phone');
			
			
			$_data['comments'] = $this->input->post('comments');
			
			$_data['created_date'] = date('Y-m-d h:i:s');
			/*$_data['lang'] = $this->lang->lang();*/
			$this->db->insert('opening_a_new_spa',$_data);
			$new_id=$this->db->insert_id();
			$_data['id_opening_a_new_spa']=$new_id;
			
	$projects = array();
	if(isset($_POST['projects']) && !empty($_POST['projects'])) {
		$projects = $_POST['projects'];
	}
	$this->fct->insert_spa_projects($new_id,$projects);
	
	$services = array();
	if(isset($_POST['services']) && !empty($_POST['services'])) {
		$services = $_POST['services'];
	}
	$this->fct->insert_spa_services($new_id,$services);
	
	///////////////////INSERT CONSULTANTS MEETING/////////////////
	$this->fct->insert_consultants_messages($new_id);
			// send emails

			
     		$this->load->model('send_emails');
			$this->send_emails->sendSpaToAdmin($_data);
			$this->send_emails->sendSpaToClient($_data);
			$return['result'] = 1;
			$return['message'] = 'Your spa has been successfully submitted and received.';
		}
		echo json_encode($return);
	}	
	
///////////////////////////////////BRANDS////////////////////////////////////////
public function getConsultant($id_consultants="")
	{
	$data['result']="";
	$consultant=$this->fct->getonerecord('consultants',array('id_consultants'=>$id_consultants));
	
	if(!empty($consultant)){
	$data['consultant'] = $consultant;
	$html=$this->load->view('load/consultant',$data,true);
	echo $html;exit;
	}
		}		
	

}