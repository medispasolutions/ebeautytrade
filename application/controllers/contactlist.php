<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Contactlist extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index($id = "")
    {
        //some pages are obly for main admins
        $restricted = [
            1, //spa-consultants
            2, //spa-software
        ];
        if (in_array($id, $restricted) && !$this->acl->isCurrentUserAAdmin()) {
            redirect(site_url());
        }

        if (!empty($id)) {
            $guide = $this->fct->getonerecord('guide', array('id_guide' => $id));
        }
        if (!empty($guide)) {

            ////TRACING//////
            $insert_log['section'] = $guide['title'];
            $this->fct->insertNewLog($insert_log);

            $contactus = $this->fct->getAll_cond('contact_list', 'sort_order', array('id_guide' => $id));
            $url = route_to('contactlist/index/' . $guide['id_guide']) . '?pagination=on';
            $data['new_url'] = $url;
            $data['guide'] = $guide;
            $data['seo'] = $guide;
            $data['seo']['meta_title'] = $guide['title'];

            $data['branches'] = $this->fct->getAll_1("branches", "id_branches");
            $data['settings'] = $this->fct->getonerow('settings', array('id_settings' => 1));

            $show_items = 21;
            $sort_order = "created_date";
            $desc = "desc";
            $cond['id_guide'] = $guide['id_guide'];

            $count = $this->fct->getTablePagination('contact_list', $cond);

            $data['show_items'] = $show_items;
            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $cc = $count;
            $config['base_url'] = $url;
            $config['total_rows'] = $cc;
            $config['num_links'] = '8';
            $config['use_page_numbers'] = true;
            $config['page_query_string'] = true;

            $config['per_page'] = $show_items;

            $this->pagination->initialize($config);
            if ($this->input->get('per_page')) {
                if ($this->input->get('per_page') != "") {
                    $page = $this->input->get('per_page');
                } else {
                    $page = 0;
                }
            } else {
                $page = 0;
            }
            $data['count'] = $cc;
            $data['per_page'] = $page;
            $num_page_prev = 1;
            if ($page > 0) {
                $num_page = intval($page / $show_items);
            } else {
                $num_page = 0;
            }

            $data['contactus'] = $this->fct->getTablePagination('contact_list', $cond, $show_items, $page, $sort_order,
                $desc);


            /////////////////////////////////ADVERTISEMENTS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            /*	    $page_ads="directory_contactlist";
                    $block_ads="pages_top";
                    $category_ads="";
                    $id_user_ads="";
                    $type="image";

                    $data['advertisements'] = $this->custom_fct->getBannerAds($page_ads,$type,$block_ads,$category_ads,$id_user_ads);*/

            $page_ads = "directory";
            $block_ads = "directory_left";
            $category_ads = "";
            $id_user_ads = "";
            $type = "image";

            $data['home_bottom_ads_left'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads, $category_ads,
                $id_user_ads);

            $page_ads = "directory";
            $block_ads = "directory_right";
            $category_ads = "";
            $id_user_ads = "";
            $type = "image";

            $data['home_bottom_ads_right'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads,
                $category_ads, $id_user_ads);


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'blocks/breadcrumbs', $data);
            $this->template->write_view('content', 'content/contactlist', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);

            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('using_fake_link'));
            redirect(site_url());
        }
    }


    function validate_captcha()
    {
        $row_count = 1;
        if (!isset($_POST['no_captcha'])) {
            $expiration = time() - 7200; // Two hour limit
            $this->db->query("DELETE FROM captcha WHERE captcha_time < " . $expiration);
            // Then see if a captcha exists:
            $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
            $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
            $query = $this->db->query($sql, $binds);
            $row = $query->row();
            $row_count = $row->count;
            if ($row_count == 0) {
                $this->form_validation->set_message('validate_captcha', lang('notcorrectcharacters'));
                return false;
            } else {
                return true;
            }
        }
    }

    public function send()
    {
        //print '<pre>';
        //print_r($_POST);exit;
        $allPostedVals = $this->input->post(null, true);
        $this->form_validation->set_rules('name', lang('name'), 'trim|required');
        $this->form_validation->set_rules('phone', lang('phone'), 'trim');
        $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email');
        $this->form_validation->set_rules('message', lang('message'), 'trim|required');
        //$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');
        if ($this->form_validation->run() == false) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {
            $_data['name'] = $this->input->post('name');
            $_data['email'] = $this->input->post('email');
            $_data['phone'] = $this->input->post('phone');
            $_data['type'] = 'contactus';
            $_data['message'] = $this->input->post('message');
            $_data['created_date'] = date('Y-m-d h:i:s');
            $_data['lang'] = $this->lang->lang();
            $this->db->insert('contactform', $_data);
            // send emails
            $this->load->model('send_emails');
            $this->send_emails->sendContactToAdmin($_data);
            $return['result'] = 1;
            $return['message'] = lang('contact_success');
        }
        echo json_encode($return);
    }

}