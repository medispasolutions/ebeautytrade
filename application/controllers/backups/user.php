<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    //=================================================
    // functions validated by BI

    /**
     * This is the login/register page for shop users
     */
    public function login()
    {
        if (checkUserIfLogin()) {
            return redirect(route_to('user'));
        }

        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 7));
        //policy for popup
        $data['private_policy'] = $this->fct->getonerow('dynamic_pages', array('id_dynamic_pages' => 5));
        //terms of use disclaimer for popup
        $data['disclaimer'] = $this->fct->getonerow('dynamic_pages', array('id_dynamic_pages' => 6));

        $this->renderStandardTemplate('user/login', $data);
    }

    /**
     * This is the password reminder page for shop user
     * @param array $data
     */

    public function password($data = array())
    {
        if (checkUserIfLogin()) {
            return redirect(route_to('user'));
        }

        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 8));
        //i don't know what's that
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }

        $this->renderStandardTemplate('user/password', $data);
    }

    /**
     * This is an alias for login page
     */
    function register()
    {
        return redirect(route_to('login'));
    }

    /**
     * This function is used to log the user into the system (login form is submitted here)
     */
    public function validate()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);

        $this->form_validation->set_rules('email', lang('email'), 'trim|required|xss_clean|callback_validate_user[]');
        $this->form_validation->set_rules('password', lang('password'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {

            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $cond = array(
                'email' => $email,
                'deleted' => 0,
                'password' => md5($password)
            );
            $user = $this->fct->getonerecord_supplier('user', $cond);
            //if  id_roles =5 stop everything
            if($user==null){
                     $return['message'] = "Sorry, but you can only access this account from through: <a href='https://www.spamiles.com/back_office'>https://www.spamiles.com/back_office<a/>";
            }
            else{
                $this->session->unset_userdata('auto_login');
                $this->session->unset_userdata('discount_auto_login');

                $this->loginSucces($user, true);

                if ($this->session->userdata('redirect_link')) {

                    $redirect_link = $this->session->userdata('redirect_link');
                    $this->session->unset_userdata('redirect_link');
                } else {
                    $redirect_link = site_url('user');
                }

                $return['result'] = 1;
                ////TRACING//////
                $insert_log['section'] = "login";
                $this->fct->insertNewLog($insert_log);
                $return['message'] = "System is redirecting to page.";
                if ($this->input->post('redirect') == 1) {
                    $return['redirect_link'] = $redirect_link;
                } else {
                    $return['redirect_link'] = site_url('');
                }
            }
        }
        echo json_encode($return);
    }

    /**
     * This function is used to register a user (user registration is submitted here)
     */
  public function submit(){
         
        $originalPostedVals = $this->input->post(NULL, TRUE);
        if (!isset($originalPostedVals['agree'])) {
          $originalPostedVals['agree'] = null;
        }

        //first check the email, if it's ok we will add it to the db
        $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email|xss_clean|callback_check_if_email_exists[]');
        $this->form_validation->set_rules('confirm_email', lang('confirm_email'), 'trim|required|valid_email|xss_clean|callback_check_email_confirmation[]');
        
        
       // $this->form_validation->set_rules('con_password', lang('con_password'), 'trim|required|valid_email|xss_clean|callback_check_email_confirmation[]');
        
        
        if ($this->form_validation->run() !== false) {
            $data = array();
            $data['lang'] = "";
            $data['email'] = $this->input->post('email');
            $data['id_roles'] = 4;
            $data['correlation_id'] = rand();
            $data['id_countries'] = $this->input->post('country');
            $data['completed'] = 0;
            $data['status'] = 3;
            $data['created_date'] = date("Y-m-d h:i:s");
            if ($this->check_account()) {
                $this->db->insert('user', $data);
                $return['result'] = 1;
                $new_id = $this->db->insert_id();
                $this->ecommerce_model->createChecklistCategories($new_id);
                $this->load->model('send_emails');
                $user = $this->fct->getonerecord('user', array('id_user' => $new_id));
                $this->send_emails->sendFirstStep($user);
            } else {
                $cond['email'] = $this->input->post('email');
                $cond['guest'] = 0;
                $cond['completed'] = 0;
                if ($this->session->userdata('login_id')) {
                    $cond['id_user !='] = $this->session->userdata('login_id');
                }
                $check = $this->fct->getonerow('user', $cond);
                $new_id = $check['id_user'];
            }
        //return error
        } else {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $find = array('<p>', '</p>');
            $replace = array('', '');
            if (form_error('email') != '') {
                $return['errors']['email'] = str_replace($find, $replace, form_error('email'));
            }
                        
                        if (form_error('confirm_email') != '') {
                $return['errors']['confirm_email'] = str_replace($find, $replace, form_error('confirm_email'));
            }
            
            
            
            if (form_error('password') != '') {
                $return['errors']['password'] = str_replace($find, $replace, form_error('password'));
            }
                        
                        if (form_error('con_password') != '') {
                $return['errors']['con_password'] = str_replace($find, $replace, form_error('con_password'));
            }

            
            
            
            

                
           echo json_encode($return);
           return;
        }

        //we have the account id under $new_id
        //let's check other data
        $this->form_validation->set_rules('trading_name', lang('trading_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('trade_license_no', lang('trade_license_no'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('b_type', lang('b_type'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('address', lang('address'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('country', lang('country'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('phone', lang('phone'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('family_name', lang('family_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('position', lang('position'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', lang('password'), 'trim|required|xss_clean|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('con_password', lang('Confirm Password'), 'required|matches[password]');
        $this->form_validation->set_rules('agree', 'Private Policy', 'trim|required|xss_clean');
        /*
        $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('last_name', lang('first_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('address', lang('address'), 'trim|xss_clean');
        $this->form_validation->set_rules('city', lang('city'), 'trim|xss_clean');
        $this->form_validation->set_rules('position', lang('position'), 'trim|xss_clean');
        $this->form_validation->set_rules('password', lang('password'), 'trim|required|xss_clean|min_length[8]|max_length[20]');
        $this->form_validation->set_rules('phone', lang('phone'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('agree', 'Private Policy', 'trim|required|xss_clean'); */

        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!!';
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($originalPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }

            echo json_encode($return);
            return;
        }

        $id_user = $new_id;
        $cond['id_user'] = $id_user;
        $user = $this->fct->getonerow('user', $cond);
        $update['trading_name'] = $this->input->post("trading_name");
        $update['license_number'] = $this->input->post("trade_license_no");
        $update['first_name'] = $this->input->post("first_name");
        $update['business_type'] = $this->input->post("b_type");
        $update['last_name'] = $this->input->post("family_name");
        //$name = explode(" ", $this->input->post("name"));
        //$update['first_name'] = array_shift($name);
        //$update['last_name'] = implode(" ", $name);
        $token = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
        $update['token'] = $token;
        //$update['name'] = $this->input->post("name");
        $update['name'] = $update['first_name'].' '.$update['family_name'];
                $update['address_1'] = $this->input->post("address");
                $update['city'] = $this->input->post("city");
        $update['completed'] = 1;
        $update['agree'] = 1;
        $phone = $this->input->post("phone");
        $update['phone'] = $phone;
                $update['position'] = $this->input->post("position");

        if ($this->input->post("is_subscribed")) {
            $update['is_subscribed'] = 1;
        }

        $update['password'] = md5($this->input->post('password'));
        $update['created_date'] = date('Y-m-d h:i:s');
        $update['status'] = 2;

        //write
        $this->db->where('id_user', $id_user);
        $this->db->update('user', $update);

        if ($this->input->post("is_subscribed") == 1) {
            $newsletter['id_user'] = $new_id;
            $newsletter['created_date'] = date('Y-m-d h:i:s');
            $newsletter['email'] = $user['email'];
            $this->db->insert('newsletter', $newsletter);
        }

        $_data['id'] = $new_id;
        $c_user['id_user'] = $new_id;
        $user = $this->fct->getoneuser($c_user);
        $user['password'] = $this->input->post('password');
        $this->load->model('send_emails');

        //send email to the admins
        $this->send_emails->sendUserToAdmin($user);
        //send email to the user
        $this->send_emails->sendUserReply($user);

        $return['redirect_link'] = route_to('?open=afterRegistration');
        $return['message'] = lang('Please wait...');
        $return['result'] = 1;

        echo json_encode($return);
    }


    /**
     * This function is used to send a password reset request to a shop user (form is submitted here)
     */
    public function password_sendrequest()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email|xss_clean|callback_validate_email[]');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {
            $return['result'] = 1;
            $return['message'] = lang('password_request_sent') . '<a href="mailto:' . $this->input->post('email') . '">' . $this->input->post('email') . '</a>';
        }
        echo json_encode($return);
    }

    /**
     * This is a data validator
     * Validate email field during login (returns a proper error if sth is wrong)
     * @return bool
     */
    public function validate_user()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $bool = true;
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $cond = array(
            'email' => $email,
            'guest' => 0,
            'deleted' => 0,
            'pre_registration' => 0,
            'status' => 1,
            'completed' => 1,
            'password' => md5($password)
        );
        //print_r($cond);exit;
        $user = $this->fct->getoneuser($cond);


        if (empty($user)) {
            $cond = array(
                'username' => $email,
                'guest' => 0,
                'status' => 1,
                'pre_registration' => 0,
                'deleted' => 0,
                'completed' => 1,
                'password' => md5($password)
            );
            //print_r($cond);exit;
            $user = $this->fct->getoneuser($cond);
        }

        if (empty($user)) {

            $cond2 = array(
                'email' => $email);
            //print_r($cond);exit;
            $user_email = $this->fct->getoneuser($cond2);
            if (empty($user_email)) {
                $cond2 = array(
                    'username' => $email);
                //print_r($cond);exit;
                $user_email = $this->fct->getoneuser($cond2);
            }


            if (empty($user_email)) {
                $this->form_validation->set_message('validate_user', lang('account_does_not_exists'));
            } else {
                $this->form_validation->set_message('validate_user', lang('incorrect_email_pass'));
            }
            return false;
        } else {
            if ($user['status'] == 0) {
                //	$this->form_validation->set_message('validate_user',lang('account_is_blocked'));
                $this->form_validation->set_message('validate_user', lang('account_is_blocked'));
                $bool = "false";
                return false;
            }

            if ($bool) {

                return true;
            }
        }
    }

    /**
     * This is a data validator
     * @return bool
     */
    public function check_if_email_exists()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $email = $this->input->post('email');
        $cond['email'] = $email;
        $cond['guest'] = 0;
        $cond['deleted'] = 0;
        $cond['completed'] = 1;

        if ($this->input->post('id_user') != "") {
            $cond['id_user !='] = $this->input->post('id_user');
        }

        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $cond['id_user !='] = $user['id_user'];
        }

        if ($this->input->post('token') != "") {
            $cond['token !='] = $this->input->post('token');
        }
        //print_r($cond);exit;
        $check = $this->fct->getonerow('user', $cond);

        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('check_if_email_exists', lang('email_exists'));
            return false;
        }
    }

    /**
     * This is a data validator
     * @return bool
     */
    public function check_account()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $email = $this->input->post('email');
        $cond['email'] = $email;
        $cond['guest'] = 0;
        $cond['completed'] = 0;
        $cond['deleted'] = 0;
        //echo 'aasa '.$this->session->userdata('login_id');exit;
        if ($this->session->userdata('login_id')) {
            $cond['id_user !='] = $this->session->userdata('login_id');
        }
        //print_r($cond);exit;
        $check = $this->fct->getonerow('user', $cond);

        if (empty($check)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This is a data validator (used by password_sendrequest()
     * @return bool
     */
    public function validate_email()
    {
        $email = $this->input->post('email');
        $cond = array(
            'email' => $email,
            'guest' => 0
        );
        $user = $this->fct->getonerow('user', $cond);
        //print_r($user);exit;
        if (empty($user)) {
            $this->form_validation->set_message('validate_email', lang('account_does_not_exists'));
            return false;
        } else {
            if ($user['status'] == 0) {
                $this->form_validation->set_message('validate_email', lang('account_is_blocked'));
                return false;
            } else {
                $this->load->model('send_emails');
                $request = $this->fct->create_password_request($user);
                $this->send_emails->sendPasswordRequest($user, $request);
                return true;
            }
        }
    }

    //=================================================
    // other functions

/////////////////////////////////////////////////////////////////////////////////////////////
    public function SendEmail()
    {
        $this->load->library('email');
        $m = '<p>this is a test message.</p>';

//$config = Array(
//		  'protocol' => 'smtp',
//		  'smtp_host' => 'mail.invent-uae.com',
//		  'smtp_port' => 25,
//		  'mailtype'  => 'html',
//		  'charset'   => 'iso-8859-1',
//		  'smtp_user' => 'rami@invent-uae.com',
//          'smtp_pass' => '@Rami123!',
//		  'starttls' => false
//		);
        /*$config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.office365.com',
		  'smtp_port' => 587,
		  'mailtype'  => 'html',
		  'charset'   => 'iso-8859-1',
		  'smtp_user' => 'contact@saiid-kobeisy.com',
          'smtp_pass' => 'Csk@03862128',
		  'starttls' => false
		);*/
        /*		$config['smtp_host'] = 'portal.office.com'; //change this with your smtp host
        $config['smtp_user'] = 'contact@saiid-kobeisy.com'; //your smtp user email
        $config['smtp_pass'] = 'Csk@03862128'; //your smtp password
        $config['smtp_port'] = '587';
        $config['smtp_crypto'] = 'tls';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['smtp_timeout'] = '7';
        $config['newline'] = "\r\n";
        $config['crlf'] = "\r\n";*/

        /*	$config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'smtp.office365.com',
    'smtp_port' => 587,
    'mailtype'  => 'html',
    'charset'   => 'utf-8',
    'smtp_user' => 'orders@saiid-kobeisy.com',
    'smtp_pass' => 'Osk@03862128',
    'starttls' => TRUE
  );*/


        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['smtp_timeout'] = '5';
        $config['starttls'] = TRUE;
        /*
$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.office365.com';

$config['smtp_user'] = 'orders@saiid-kobeisy.com';
$config['smtp_pass'] = 'Osk@03862128';
$config['smtp_port'] = '587';

$config['starttls'] = TRUE;
$config['smtp_crypto'] = 'tls';
$config['smtp_timeout'] = '5';
$config['charset']='utf-8';
$config['newline']="\r\n";
$config['crlf'] = "\r\n";*/
        /*
// The message
$message = "Line 1\r\nLine 2\r\nLine 3";
// In case any of our lines are larger than 70 characters, we should use wordwrap()
$message = wordwrap($message, 70, "\r\n");
// Send
mail('orders@saiid-kobeisy.com', 'My Subject', $message);
exit("AA");*/
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('saiid.kobeisy27@gmail.com', 'No-Reply');
        $this->email->to('contact@saiid-kobeisy.com');
        $this->email->subject('Welcome to Our Companydd');
        $this->email->message($m);
        if ($this->email->send()) {
            echo 'Sent Successfully!';
        } else {
            echo $this->email->print_debugger();
        }
    }


    public function updateAllUsersMiles($id_user = 0)
    {
        $this->ecommerce_model->updateAllUsersMiles($id_user);
    }

    public function updateUserMilesLineItems($id_user = 506)
    {
        $this->ecommerce_model->updateUserMilesLineItems($id_user);
    }

    public function calculateUserMiles($id_user = 643)
    {
        $cond = array('redeem' => 1, 'id_user' => $id_user);
        $redeem_miles = $this->checkUserMiles($cond);

        $cond = array('miles' => 1, 'id_user' => $id_user, 'paid' => 1);
        $miles = $this->checkUserMiles($cond);

        $remaining_miles = $miles - $redeem_miles;

        return $remaining_miles;
    }

    public function checkUserMiles($cond = array())
    {
        $q = " SELECT line_items.*, .products.redeem_miles as products_redeem_miles from line_items ";
        $q .= " join products on line_items.id_products=products.id_products ";
        $q .= " join orders on orders.id_orders=line_items.id_orders ";
        $q .= " where ";

        $q .= "  orders.id_user=" . $cond['id_user'];

        if (isset($cond['redeem'])) {
            $q .= " and line_items.redeem=1";
        }
        if (isset($cond['paid'])) {
            $q .= " and (orders.status='paid' || orders.status='completed')";
        }
        $q .= " group by line_items.id_line_items";

        $query = $this->db->query($q);
        $results = $query->result_array();


        $amount = 0;
        if (isset($cond['redeem'])) {
            foreach ($results as $val) {
                $amount = $amount + ($val['redeem_miles'] * $val['quantity']);
            }
        }
        if (isset($cond['miles'])) {
            foreach ($results as $val) {
                $amount = $amount + ($val['miles'] * $val['quantity']);
            }
        }

        return $amount;
    }

    public function SendEmail2()
    {

        $this->load->library('email');
        $m = 'this is a test message.';

//$config = Array(
//		  'protocol' => 'smtp',
//		  'smtp_host' => 'mail.invent-uae.com',
//		  'smtp_port' => 25,
//		  'mailtype'  => 'html',
//		  'charset'   => 'iso-8859-1',
//		  'smtp_user' => 'rami@invent-uae.com',
//          'smtp_pass' => '@Rami123!',
//		  'starttls' => false
//		);
        /*$config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.office365.com',
		  'smtp_port' => 587,
		  'mailtype'  => 'html',
		  'charset'   => 'iso-8859-1',
		  'smtp_user' => 'contact@saiid-kobeisy.com',
          'smtp_pass' => 'Csk@03862128',
		  'starttls' => false
		);*/
        /*		$config['smtp_host'] = 'portal.office.com'; //change this with your smtp host
        $config['smtp_user'] = 'contact@saiid-kobeisy.com'; //your smtp user email
        $config['smtp_pass'] = 'Csk@03862128'; //your smtp password
        $config['smtp_port'] = '587';
        $config['smtp_crypto'] = 'tls';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['smtp_timeout'] = '7';
        $config['newline'] = "\r\n";
        $config['crlf'] = "\r\n";*/

        /*	$config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'smtp.office365.com',
    'smtp_port' => 587,
    'mailtype'  => 'html',
    'charset'   => 'utf-8',
    'smtp_user' => 'orders@saiid-kobeisy.com',
    'smtp_pass' => 'Osk@03862128',
    'starttls' => TRUE
  );*/


        /*$config['mailtype'] = 'html';
$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.office365.com';

$config['smtp_user'] = 'orders@saiid-kobeisy.com';
$config['smtp_pass'] = 'Osk@03862128';
$config['smtp_port'] = '25';

$config['starttls'] = TRUE;
$config['smtp_crypto'] = 'tls';
$config['smtp_timeout'] = '5';
$config['charset']='utf-8';
$config['newline']="\r\n";
$config['crlf'] = "\r\n";*/

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.office365.com',
            'smtp_port' => 25,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'smtp_user' => 'orders@saiid-kobeisy.com',
            'smtp_pass' => 'Osk@03862128',
            'starttls' => TRUE
        );

        /*
// The message
$message = "Line 1\r\nLine 2\r\nLine 3";
// In case any of our lines are larger than 70 characters, we should use wordwrap()
$message = wordwrap($message, 70, "\r\n");
// Send
mail('orders@saiid-kobeisy.com', 'My Subject', $message);
exit("AA");*/
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('orders@saiid-kobeisy.com');
        $this->email->to('orders@saiid-kobeisy.com');
        $this->email->subject('testffffff');
        $this->email->message($m);
        if ($this->email->send()) {
            echo 'Sent Successfully!';
        } else {
            echo $this->email->print_debugger();
        }

    }

    public function index()
    {
        if (checkUserIfLogin()) {
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 10));
            $user = $this->ecommerce_model->getUserInfo();
            $data['user'] = $user;
            $cond1['billing'] = 1;
            $cond1['id_user'] = $user['id_user'];
            $data['billing'] = $this->ecommerce_model->getUserAddresses($cond1, 1, 0);

            $cond2['shipping'] = 1;
            $cond2['id_user'] = $user['id_user'];
            $data['shipping'] = $this->ecommerce_model->getUserAddresses($cond2, 1, 0);

            $user = $this->ecommerce_model->getUserInfo();
            $cond3['id_user'] = $user['id_user'];
            $limit = 20;
            $offset = 0;
            $cond['group_by'] = 'currency';
            $data['currencies'] = $this->custom_fct->getCredits($cond3, $limit, $offset);

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/account', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->login();
        }
    }

///////////////////////////////////////////////////POPUP/////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function lgn()
    {
        $data = array();
        $html = $this->load->view('content', 'popup/login', $data);
        $return['html'] = $html;
        $return['result'] = 1;
        die($return);
    }

///////////////////////////////////////////////////CREDITS PAYMENT//////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    function ni_payment_success1()
    {
        if ($this->session->userdata('login_id') != "") {
            if (!isset($_REQUEST['responseParameter'])) redirect(route_to("user"));

            $data['lang'] = $this->lang->lang();

            $responseParameter = $_REQUEST['responseParameter'];
            $merchantId = $_REQUEST['merchantId'];

            $enc = MCRYPT_RIJNDAEL_128;
            $key = $this->config->item("payment_encryption_key");
            $mode = MCRYPT_MODE_CBC;
            $iv = $this->config->item("payment_iv");
            $rand = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
            $merchantId = $this->config->item("payment_merchant_id");
            $crypt = base64_decode($responseParameter);
            $padtext = mcrypt_decrypt($enc, base64_decode($key), $crypt, $mode, $iv);
            $pad = ord($padtext{strlen($padtext) - 1});
            if ($pad > strlen($padtext)) return false;
            if (strspn($padtext, $padtext{strlen($padtext) - 1}, strlen($padtext) - $pad) != $pad) {
                $text = "Error";
            }
            $text = substr($padtext, 0, -1 * $pad);
            $arr = explode("|", $text);
            //print '<pre>';print_r($arr);exit;
            if (!isset($arr[0]) || !isset($arr[1])) die("Error in response data, please contact the administrator.");
            $order_id = $arr[0];
            $ref_id = $arr[1];


            $this->paymentSuccess1($arr);

        } else {
            //$this->session->set_userdata("redirect_link",route_to("user/ni_payment_success"));
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }


    function paymentSuccess1($arr)
    {
        $order_id = $arr[0];
        $ref_id = $arr[1];

        //// After Success

        $credits_update['updated_date'] = date("Y-m-d H:i:s");
        $credits_update['payment_date'] = date("Y-m-d H:i:s");
        $credits_update['status'] = "1";
        $credits_update['gateway_response_id'] = $ref_id;
        $this->db->where("id_users_credits", $order_id);
        $this->db->update("users_credits", $credits_update);
        $id_users_credits = $order_id;

        $user = $this->ecommerce_model->getUserInfo();
        $user_credits = $this->fct->getonerecord('users_credits', array('id_users_credits' => $id_users_credits));
        $user_update['available_balance'] = $user['available_balance'] + $user_credits['amount_default_currency'];
        $this->db->where("id_user", $user['id_user']);
        $this->db->update("user", $user_update);

        $user = $this->ecommerce_model->getUserInfo();
        $user_credits = $this->fct->getonerecord('users_credits', array('id_users_credits' => $id_users_credits));
        $data_email['user'] = $user;
        $data_email['user_credits'] = $user_credits;
        $data_email['user_credits']['country'] = $this->fct->getonerecord('countries', array('id_countries' => $user_credits['id_countries']));


        $this->load->model('send_emails');
        $this->send_emails->sendCreditsToAdmin($data_email);
        $this->send_emails->sendCreditsToClient($data_email);

        $this->session->set_flashdata('success_message', 'Your petty cash has been add to your balanace.');
        redirect(route_to('user/credits'));

    }


    function payment_error1($id)
    {
        if (checkUserIfLogin()) {
            if (!isset($_REQUEST['responseParameter'])) redirect(route_to("user"));
            $responseParameter = $_REQUEST['responseParameter'];
            $merchantId = $_REQUEST['merchantId'];
            $enc = MCRYPT_RIJNDAEL_128;
            $key = $this->config->item("payment_encryption_key");
            $mode = MCRYPT_MODE_CBC;
            $iv = $this->config->item("payment_iv");
            $rand = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
            $merchantId = $this->config->item("payment_merchant_id");
            $crypt = base64_decode($responseParameter);
            $padtext = mcrypt_decrypt($enc, base64_decode($key), $crypt, $mode, $iv);
            $pad = ord($padtext{strlen($padtext) - 1});
            if ($pad > strlen($padtext)) return false;
            if (strspn($padtext, $padtext{strlen($padtext) - 1}, strlen($padtext) - $pad) != $pad) {
                $text = "Error";
            }
            $text = substr($padtext, 0, -1 * $pad);
            $arr = explode("|", $text);

            //// After Success

            $credits_update['updated_date'] = date("Y-m-d H:i:s");
            $credits_update['status'] = "3";
            $this->db->where("id_users_credits", $id);
            $this->db->update("users_credits", $credits_update);

            $this->session->set_flashdata('error_message', $arr[9]);
            redirect(route_to('user/credits'));

        } else {
            //$this->session->set_userdata("redirect_link",route_to("user/ni_payment_success"));
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

/////////////////////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    function ni_payment_success()
    {
        if (checkUserIfLogin()) {
            if (!isset($_REQUEST['responseParameter'])) redirect(route_to("user"));

            $data['lang'] = $this->lang->lang();

            $responseParameter = $_REQUEST['responseParameter'];
            $merchantId = $_REQUEST['merchantId'];

            $enc = MCRYPT_RIJNDAEL_128;
            $key = $this->config->item("payment_encryption_key");
            $mode = MCRYPT_MODE_CBC;
            $iv = $this->config->item("payment_iv");
            $rand = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
            $merchantId = $this->config->item("payment_merchant_id");
            $crypt = base64_decode($responseParameter);
            $padtext = mcrypt_decrypt($enc, base64_decode($key), $crypt, $mode, $iv);
            $pad = ord($padtext{strlen($padtext) - 1});
            if ($pad > strlen($padtext)) return false;
            if (strspn($padtext, $padtext{strlen($padtext) - 1}, strlen($padtext) - $pad) != $pad) {
                $text = "Error";
            }
            $text = substr($padtext, 0, -1 * $pad);
            $arr = explode("|", $text);
            //print '<pre>';print_r($arr);exit;
            if (!isset($arr[0]) || !isset($arr[1])) die("Error in response data, please contact the administrator.");
            $order_id = str_replace('o-', "", $arr[0]);
            $ref_id = $arr[1];


            $this->paymentSuccess($arr);

        } else {
            //$this->session->set_userdata("redirect_link",route_to("user/ni_payment_success"));
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }


    function paymentSuccess($arr)
    {  // set order as paidorderData
        $order_id = str_replace('o-', "", $arr[0]);
        $ref_id = $arr[1];
        $order_update['updated_date'] = date("Y-m-d H:i:s");
        $order_update['payment_date'] = date("Y-m-d H:i:s");
        $order_update['status'] = "paid";
        $order_update['gateway_response_id'] = $ref_id;
        $this->db->where("id_orders", $order_id);
        $this->db->update("orders", $order_update);


        $this->cart->destroy();

        //$this->session->set_flashdata('success_message',lang('order_completed').$order_id.'. One of our sales persons will review your order and contact you soon.');
        $this->session->set_flashdata('success_message', 'Your order is complete! Your order number is ' . $order_id . '.A member of Spamiles team will review your order and may contact you for confirmation.');
        $orderData = $this->ecommerce_model->getOrder($order_id);

        //////////////UPDATE QUANTITIES//////////
        $cond_d['id_orders'] = $orderData['id_orders'];
        $cond_d['rand'] = $orderData['rand'];
        $this->ecommerce_model->updateOrderQuantities($cond_d);
        //////////////SEND EMAILS//////////
        $this->load->model('send_emails');
        $this->send_emails->sendOrderToAdmin($orderData);
        $this->send_emails->sendOrderToUser($orderData);

        /*if(checkUserIfLogin()){*/
        redirect(route_to('user/orders/' . $order_id . '/' . $orderData['rand']));
    }


    public function print_orders_history()
    {
        $data['user_orders'] = $this->ecommerce_model->getUserOrders($this->session->userdata('login_id'));
        $this->load->view('user/print_orders_history', $data);
    }

/////////////////////////////////////////////////////////////////////////////////////////////



    public function auto_login($id_user = 0, $correlation_id = 0)
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $bool = true;
        $current_user = $this->ecommerce_model->getUserInfo();


        $this->ecommerce_model->removeSession();

        $cond = array(
            'id_user' => $id_user,
            'correlation_id' => $correlation_id,
            'status' => 1,
            'pre_registration' => 0,
            'deleted' => 0,
            'completed' => 1,

        );
        //print_r($cond);exit;
        $user = $this->fct->getoneuser($cond);


        if ($bool) {
            $this->session->set_userdata('auto_login', 1);
            $this->session->set_userdata('discount_auto_login', 1);


            if ($this->input->get('role_access') == "supplier_admin") {

                $user['admin'] = 1;
                $this->session->set_userdata('role_access', $this->input->get('role_access'));
                $this->session->set_userdata('current_id_user', $current_user['id_user']);
                $this->loginSucces($user, $bool);


                redirect(route_to('back_office/products'));
            } elseif ($this->input->get('role_access') == "admin") {


                $this->session->unset_userdata('role_access');
                $this->session->unset_userdata('current_id_user');

                $this->loginSucces($user, $bool);
                redirect(route_to('back_office'));
            } else {

                $this->session->unset_userdata('role_access');
                $this->session->unset_userdata('current_id_user');

                $this->loginSucces($user, $bool);
                /*redirect(route_to('products/index/new-arrivals'));*/
                redirect(site_url(''));
            }
        } else {
            $return['message'] = lang('access_denied');
            redirect(route_to('back_office/users'));
        }
    }

    public function expire_date()
    {
        $bool = true;

        $email = $this->input->post('lemail');
        $password = $this->input->post('lpassword');
        $cond = array(
            'email' => $email,
            'password' => md5($password)
        );
        //print_r($cond);exit;
        $user = $this->fct->getoneuser($cond);

        if (empty($user)) {
            return true;
        } else {

            if ($user['type'] == "wholesale" && $user['status'] == "1" && !$this->input->post('reactivate')) {
                if ($user['expire_date'] < date('Y-m-d h:i:s')) {
                    $this->form_validation->set_message('expire_date', lang('account_is_expired'));
                    return false;
                }
            }
        }

    }

    function validateGoogleReCaptcha()
    {
        // set post fields
        $post = [
            'secret' => '6LepIiYTAAAAAAKMaCzgcpCfqo-2Y4mWHcpGKD0G',
            'response' => $this->input->post("g-recaptcha-response"),
            // 'remoteip'   => ,
        ];

        $ch = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // execute!
        $response = curl_exec($ch);

        // close the connection, release resources used
        curl_close($ch);
        $response = json_decode($response);
        if ($response->success == true) {
            return true;
        } else {
            $this->form_validation->set_message("validateGoogleReCaptcha", "Captcha is not correct, please validate that you are not a robot.");
            return false;
        }
        //print '<pre>'; print_r($response); exit;
        // do anything you want with your response
        // var_dump($response);exit;
    }



    function update_quotation()
    {
        $ids_line_items = $this->input->post('ids_line_items');
        $quantities = $this->input->post('product_quantity');
        $product_rowid = $this->input->post('product_rowid');
        $product_id = $this->input->post('product_id');
        $discount_type_arr = $this->input->post('discount_type');
        $discount_arr = $this->input->post('discount');
        $return_message = "";

        $id_quotation = $this->input->post('id');
        $rand = $this->input->post('rand');

        $flag = 0;
        $i = 0;
        foreach ($ids_line_items as $key => $id_line_item) {
            $discount_type = $discount_type_arr[$key];
            $qty = $quantities[$key];
            $discount = $discount_arr[$key];
            $line_item = $this->fct->getonerow('quotation_line_items', array('id_quotation_line_items' => $id_line_item));
            $currency = $line_item['currency'];
            $price = $line_item['price_currency'];
            $list_price = $line_item['price_old_currency'];
            $new_price = $price;

            if ($discount_type == 1) {
                ///////////IF PERCENTAGE///////
                $new_price = displayProductDiscountPrice($discount, '', $list_price);
                $discount = $list_price - $new_price;
            } else {
                ///////////IF CURRENCY///////
                if ($discount_type == 2) {
                    $new_price = $list_price - $discount;


                }
            }


            /////////////////////////CHECK QUANTITY///////////
            $cond_p = array();
            $cond_p['id_products'] = $line_item['id_products'];
            if (!empty($line_item['options_en'])) {
                $cond_p['combination'] = $line_item['options_en'];
                $product_stock = $this->fct->getonerow('products_stock', $cond_p);
                $product = $product_stock;
            } else {
                $product = $this->fct->getonerow('products', $cond_p);
            }

            if ($product['quantity'] < $qty) {
                $qty = $line_item['quantity'];
                if ($product['quantity'] < $line_item['quantity']) {
                    $qty = $product['quantity'];
                }
                $flag = 1;
            }

            // Miles
            if (!empty($line_item['miles'])) {
                $miles = ($line_item['miles'] / $line_item['quantity']) * $qty;
            } else {
                $miles = 0;
            }

            if (!empty($line_item['redeem_miles'])) {
                $redeem_miles = ($line_item['redeem_miles'] / $line_item['quantity']) * $qty;
            } else {
                $redeem_miles = 0;
            }


            $total_price = 0;
            $update_line_item['updated_date'] = date('Y-m-d h:i:s');


            // Price
            $total_price = $list_price * $qty;
            $total_discount_price = $new_price * $qty;


            $update_line_item['miles'] = $miles;
            $update_line_item['redeem_miles'] = $redeem_miles;

            $update_line_item['price_old'] = $list_price;
            $update_line_item['price'] = $new_price;
            $update_line_item['discount'] = $discount;
            $update_line_item['discount_type'] = $discount_type;
            $update_line_item['quantity'] = $qty;

            $update_line_item['price_old_currency'] = $list_price;
            $update_line_item['price_currency'] = $new_price;
            $update_line_item['total_price_old'] = $total_price;
            $update_line_item['total_price_old_currency'] = $total_price;

            $update_line_item['total_price_currency'] = $total_discount_price;
            $update_line_item['total_price'] = $total_discount_price;

            $this->db->where(array('id_quotation_line_items' => $id_line_item));
            $this->db->update('quotation_line_items', $update_line_item);
        }


        /*redirect(route_to('cart'));*/
        if ($flag == 1) {
            $return_message = 'Your quotation is updated successfully, some of the products having no stock are not updated.';
        } else {
            $return_message = 'Your quotation is updated successfully.';
        }
        $return['message'] = $return_message;
        $this->ecommerce_model->updateQuotation($id_quotation, $rand);
        $cond['rand'] = $rand;
        $data['order_details'] = $this->ecommerce_model->getQuotation($id_quotation, $cond);


        $return['cart'] = $this->load->view('blocks/quotation', $data, true);
        $return['result'] = 1;
        /*	if($flag==0){
	$return['result']=1;}else{
	$return['result']=0;	}*/

        die(json_encode($return));
    }


    public function removeQuotationLineItem()
    {
        $bool = true;
        $id_line_item = $this->input->post('id');
        $rand = $this->input->post('rand');
        $line_item = $this->fct->getonerow('quotation_line_items', array('id_quotation_line_items' => $id_line_item));
        if (empty($line_item)) {
            $bool = false;
        } else {
            $check = $this->fct->getonerow('quotation', array('id_quotation' => $line_item['id_quotation'], 'rand' => $rand));
            if (empty($check)) {
                $bool = false;
            }
        }

        if ($bool) {
            $this->db->delete('quotation_line_items', array('id_quotation_line_items' => $id_line_item));

            $this->ecommerce_model->updateQuotation($line_item['id_quotation'], $rand);
            $cond['rand'] = $rand;
            $data['order_details'] = $this->ecommerce_model->getQuotation($line_item['id_quotation'], $cond);

            $return['cart'] = $this->load->view('blocks/quotation', $data, true);
            $return['result'] = 1;

        } else {
            $return['result'] = 0;
        }

        die(json_encode($return));


    }


    public function login_by_token($id_user = "", $token = "")
    {
        if (!empty($id_user) && !empty($token)) {
            $lang = "";
            if ($this->config->item("language_module")) {
                $lang = getFieldLanguage($this->lang->lang());
            }
            $data['lang'] = $lang;
            $bool = true;

            $cond = array(
                'id_user' => $id_user,
                'guest' => 0,
                'deleted' => 0,
                'status' => 1,
                'completed' => 1,
                'token' => $token
            );


            //print_r($cond);exit;
            $user = $this->fct->getonerecord('user', $cond);


            if (empty($user)) {
                $bool = false;
                $this->session->set_flashdata('error_message', lang('account_is_blocked'));
                $redirect_link = route_to('user/login');
            } else {
                $this->loginSucces($user, $bool);
                $redirect_link = route_to('user/profile');
            }

        } else {

            $this->session->set_flashdata('error_message', lang('account_is_blocked'));
            $redirect_link = route_to('user/login');
        }

        redirect($redirect_link);

    }


    public function reactivateAccount()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);

        $this->form_validation->set_rules('lemail', lang('username'), 'trim|required|xss_clean|callback_validate_user[]');
        $this->form_validation->set_rules('lpassword', lang('password'), 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {
            $update['expire_date'] = date('Y-m-d h:i:s', strtotime('+' . getExpireDate() . ' days'));
            $cond_uu['email'] = $this->input->post('lemail');
            $cond_uu['password'] = md5($this->input->post('lpassword'));
            $this->db->update('user', $update, $cond_uu);

            if ($this->session->userdata('redirect_link')) {
                $redirect_link = $this->session->userdata('redirect_link');
                $this->session->unset_userdata('redirect_link');
            } else {
                $redirect_link = site_url('user');
            }

            $this->session->set_flashdata('success_message', lang('account_is_reactivate'));
            $return['result'] = 1;
            $return['message'] = "System is redirecting to page.";
            $return['redirect_link'] = $redirect_link;
        }
        echo json_encode($return);
    }




    public function login2()
    {
        $data = array();
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;

        $id = $this->input->post('id');

        $data['id'] = $id;
        $html = $this->load->view('popup/login2', $data, true);

        $data['html'] = $html;
        $data['result'] = 1;

        die(json_encode($data));
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function logout()
    {
        $this->cart->destroy();
        if (checkUserIfLogin()) {
            /*	$user=$this->ecommerce_model->getUserInfo();
     	$this->db->query('DELETE FROM shopping_cart WHERE id_user = '.$user['id_user']);*/
            $this->ecommerce_model->removeSession();
        }
        //$this->session->set_flashdata('success_message',lang('logout_success'));
        redirect(route_to('user'));
    }


/////////////////////////////////////////////////////////////////////////////////////////////
    function validate_captcha()
    {
        $row_count = 1;
        if (!isset($_POST['no_captcha'])) {
            $expiration = time() - 7200; // Two hour limit
            $this->db->query("DELETE FROM captcha WHERE captcha_time < " . $expiration);
            // Then see if a captcha exists:
            $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
            $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
            $query = $this->db->query($sql, $binds);
            $row = $query->row();
            $row_count = $row->count;
            if ($row_count == 0) {
                $this->form_validation->set_message('validate_captcha', lang('notcorrectcharacters'));
                return false;
            } else {
                return true;
            }
        }
    }





    public function validateusernameexistance()
    {
        $username = $this->input->post('username');
        $cond = array('username' => $username, 'deleted' => 0);
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $cond['id_user !='] = $user['id_user'];
        }
        $check = $this->fct->getonerow('user', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('validateusernameexistance', 'Username exists, please try another username.');
            return false;
        }
    }

    public function check_account_2()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $correlation_id = $this->input->post('correlation_id');
        $id_user = $this->input->post('id_user');
        $cond['correlation_id'] = $correlation_id;
        $cond['guest'] = 0;

        //echo 'aasa '.$this->session->userdata('login_id');exit;

        $cond['id_user'] = $id_user;
        $cond['completed'] = 0;

        //print_r($cond);exit;
        $check = $this->fct->getonerow('user', $cond);

        if (empty($check)) {
            $this->form_validation->set_message('check_account_2', lang('account_does_not_exists'));
            return false;
        } else {
            return true;
        }
    }

    public function required_categories()
    {


        if ($this->input->post('categories') && $this->input->post('categories') != "") {

            return true;
        } else {
            $this->form_validation->set_message('required_categories', 'The Categories field is required.');
            return false;

        }
    }


    public function check_if_username_exists()
    {
        $username = $this->input->post('username');
        $cond['username'] = $username;
        //echo 'aasa '.$this->session->userdata('login_id');exit;
        if ($this->session->userdata('login_id')) {
            $cond['id_user !='] = $this->session->userdata('login_id');
        }
        //print_r($cond);exit;
        $check = $this->fct->getonerow('user', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('check_if_username_exists', lang('username_exists'));
            return false;
        }
    }

    public function check_if_date_valid()
    {
        $day = intval($this->input->post('day'));
        $month = intval($this->input->post('month'));
        $year = intval($this->input->post('year'));
        $current_year = date("Y");
        $bool = true;
        $message = "";
        if (empty($month) && empty($day) && empty($year)) {
            return true;
        } else {
            if (!empty($year) && ($year < 1900 || $year > $current_year)) {
                $bool = false;
                $message = "Please enter a valid year (1900-2015).";
            } else {
                if (empty($year)) {
                    $bool = false;
                    $message = "Year field is required.";
                }
            }


            if (!empty($month) && ($month < 1 || $month > 12)) {
                $bool = false;
                $message = "Please enter a valid month (1-12).";
            } else {

                if (empty($month)) {
                    $bool = false;
                    $message = "Month field is required.";
                }
            }

            if (!empty($day) && ($day < 1 || $day > 31)) {
                $bool = false;
                $message = "Please enter a valid day (1-31).";
            } else {
                if (empty($day)) {
                    $bool = false;
                    $message = "Day field is required.";
                }
            }

//echo 'aasa '.$this->session->userdata('login_id');exit;
            if ($bool) {
                return true;
            } else {
                $this->form_validation->set_message('check_if_date_valid', $message);
                return false;
            }
        }
        //print_r($cond);exit;

    }



    /**
     * This looks like a function that isn't used now (some old registration form)
     * @param int $id_user
     * @param int $token
     */
    function pre_register($id_user = 0, $token = 0)
    {
        $cond_user['id_user'] = $id_user;
        $cond_user['token'] = $token;
        $cond_user['pre_registration'] = 1;
        $userData = $this->fct->getonerecord('user', $cond_user);
        if (!checkUserIfLogin() && !empty($userData)) {

            $userData = $this->fct->getonerecord('user', $cond_user);
            $data['userData'] = $userData;

            $lang = "";
            if ($this->config->item("language_module")) {
                $lang = getFieldLanguage($this->lang->lang());
            }
            $data['lang'] = $lang;

            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 9));
            $data['private_policy'] = $this->fct->getonerow('dynamic_pages', array('id_dynamic_pages' => 5));
            $data['disclaimer'] = $this->fct->getonerow('dynamic_pages', array('id_dynamic_pages' => 6));
            $data['categories'] = $this->fct->getAll_cond('categories', 'sort_order', array('id_parent' => 0));
            $data['countries'] = $this->fct->getAll("countries", "title");
            $data['business_type'] = $this->fct->getAll("business_type", "title");
            $data['hear_about_us'] = $this->fct->getAll("how_did_you_hear_about_us", "title");

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/pre_register', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            redirect(route_to('user'));
        }

    }


    public function createCategroeis()
    {
        $users = $this->fct->getAll('user', 'sort_order');

        foreach ($users as $val) {
            $this->ecommerce_model->createChecklistCategories($val['id_user']);
        }

    }

    function loginSucces($user, $bool)
    {


        if ($bool) {

            $update['last_login'] = date('Y-m-d h:i:s');
            $this->db->where('id_user', $user['id_user']);
            $this->db->update('user', $update);


            $this->session->set_userdata('login_id', $user['id_user']);

            $this->session->set_userdata('id_roles', $user['id_roles']);
            $this->session->set_userdata('correlation_id', $user['correlation_id']);
            $this->session->set_userdata('account_type', $user['type']);

            $this->session->set_userdata('last_login', changeDate($update['last_login']));
            if (isset($user['admin'])) {
                $this->session->set_userdata('uid', $user['id_user']);
                $this->session->set_userdata('user_name', $user['first_name'] . ' ' . $user['last_name']);

                $this->session->set_userdata('username', $user['username']);
                $this->session->set_userdata('email', $user['email']);
                $role = $this->fct->getonecell('roles', 'title', array('id_roles' => $user["id_roles"]));
                $this->session->set_userdata('roles', array($role));
            }
            //$this->session->set_flashdata('success_message',lang('welcome').' '.$user['name'].' '.lang(',').' '.lang('you_have_successfully_login_with').' <a href="'.site_url().'">'.lang('spamiles').'</a>.');
        }
    }




    public function validatecaptcha()
    {
        include_once './captcha/securimage.php';
        $securimage = new Securimage();
        $captcha = $this->input->post('captcha');
        if (empty($captcha)) {
            $this->form_validation->set_message('validatecaptcha', lang('captcha_field_required'));
            return false;
        } elseif ($securimage->check($captcha) == false) {
            $this->form_validation->set_message('validatecaptcha', lang('security_code_incorrect'));
            /*return false;*/
            return true;
        } else {
            return true;
        }
    }



/////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * This is after registration thank you - I think it's not used anymore so it's a candidate to delete
     */
    public function thank_you()
    {
        $login = checkUserIfLogin();

        $data = array();
        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('content', 'blocks/thank_you', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);
        $this->template->render();
    }

/////////////////////////////////////////////////////////////////////////////////////////////

    public function checkUploadLicence()
    {
        if (isset($_FILES['trading_licence']['size'])) {
            $size = $_FILES['trading_licence']['size'];
            $size_arr = getMaxSize('file');
            $max_size = $size_arr['size'];

            if ($max_size > $size) {

                return true;
            } else {
                $this->form_validation->set_message('checkUploadLicence', 'Licence  exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
                return false;
            }
        }
    }


    public function checkLicenseFiles()
    {
        $bool = false;
        $trading_license = $this->input->post('trading_license');

        $trading_license_name = $this->input->post('trading_license_name');
        if (!empty($trading_license)) {
            foreach ($trading_license as $key => $val) {
                if ($_FILES["trading_license_" . $val]["name"] || isset($trading_license_name[$key])) {
                    $bool = true;
                }
            }
        }
        if ($bool) {
            return true;
        } else {
            $this->form_validation->set_message('checkLicenseFiles', "Please upload at least one.");
            return false;
        }

    }


    public function updateProfile()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;

        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $allPostedVals = $this->input->post(NULL, TRUE);
            $allPostedVals['trading_licence'] = "";
            //////////////Personal Information///////////////////////////
            $this->form_validation->set_rules('id_role', 'Role', 'trim|required|xss_clean');
            $this->form_validation->set_rules('trading_name', lang('trading_name'), 'trim|required|xss_clean');

            if ($this->input->post('change_password')) {
                $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|callback_check_password[]');
                $this->form_validation->set_rules('password', lang('password'), 'trim|required|xss_clean|min_length[8]|max_length[20]');
                $this->form_validation->set_rules('confirm_password', lang('confirm_Password'), 'trim|required|xss_clean|matches[password]|min_length[8]|max_length[20]');
            }

            /*$this->form_validation->set_rules('username',lang('username'),'trim|required|xss_clean');*/
            $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('salutation', lang('salutation'), 'trim|xss_clean');
            $this->form_validation->set_rules('position', lang('position'), 'trim|xss_clean');
            $this->form_validation->set_rules('website', lang('website'), 'trim|xss_clean');
            $this->form_validation->set_rules('license_number', lang('license_number'), 'trim|xss_clean');
            /*$this->form_validation->set_rules('username',lang('username'), 'trim|required|xss_clean|callback_validateusernameexistance[]');*/
            $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email|xss_clean|callback_check_if_email_exists[]');

            /*	if(checkIfSupplier()){
	$this->form_validation->set_rules('category',lang('category'),'trim|xss_clean|callback_required_categories[]');}*/


            //////////////Address///////////////////////////////////////
            $this->form_validation->set_rules('address_1', lang('address'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('address_2', lang('address'), 'trim|xss_clean');
            $this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('country', lang('country'), 'trim|required|xss_clean');
            /*$this->form_validation->set_rules('phone',lang('phone'),'trim|required|xss_clean');*/
            /*		$this->form_validation->set_rules('fax',lang('fax'), 'callback_fax[]');
		$this->form_validation->set_rules('mobile',lang('mobile'), 'callback_mobile[]');*/
            $this->form_validation->set_rules('phone', lang('phone'), 'trim|required|xss_clean]');
            //////////////Others///////////////////////////////////////
            $this->form_validation->set_rules('business_type', lang('business_type'), 'trim|required|xss_clean');
            /*$this->form_validation->set_rules('hear_about_us',lang('hear_about_us'), 'trim|required|xss_clean');*/

            /*$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');*/
            /*$this->form_validation->set_rules('services_categories',lang('services_categories'),'trim|required|xss_clean');*/
            $this->form_validation->set_rules('comments', lang('comments'), 'trim|xss_clean');
            /*$this->form_validation->set_rules('trading_licence','trading licence','trim|callback_checkUploadLicence[]');*/
            /*$this->form_validation->set_rules('trading_licence_f','trading licence','callback_checkLicenseFiles[]');*/

            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } else {

                $id_user = $user['id_user'];
                $cond['id_user'] = $id_user;
                $update['trading_name'] = $this->input->post("trading_name");
                $update['license_number'] = $this->input->post("license_number");
                $update['salutation'] = $this->input->post("salutation");
                $update['email'] = $this->input->post("email");
                $update['first_name'] = $this->input->post("first_name");
                $update['last_name'] = $this->input->post("last_name");
                $update['username'] = $this->input->post("username");
                $update['website'] = $this->input->post("website");
                $update['city'] = $this->input->post("city");
                $update['position'] = $this->input->post("position");

                if ($this->input->post('change_password')) {
                    $update['password'] = md5($this->input->post('password'));
                }


                $update['id_countries'] = $this->input->post("country");
                $update['business_type'] = $this->input->post("business_type");
                $update['hear_about_us'] = $this->input->post("hear_about_us");
                $update['comments'] = $this->input->post("comments");

                /*			$phone = $this->input->post("phone");
			$update['phone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];*/

                $update['phone'] = $this->input->post("phone");

                /*			if($this->fax()){

			$fax = $this->input->post("fax");
			$update['fax'] = $fax[0].'-'.$fax[1].'-'.$fax[2];}else{
			$update['fax'] = 0;	}*/

                $update['fax'] = $this->input->post("fax");

                /*			if($this->mobile()){
			$mobile = $this->input->post("mobile");
			$update['mobile'] = $mobile[0].'-'.$mobile[1].'-'.$mobile[2];}else{
			$update['mobile'] = 0;	}*/

                $update['mobile'] = $this->input->post("mobile");

                /*if($_FILES["trading_licence"]["name"]) {
if(!empty($id_user)){
$cond_image=array("id_user"=>$id_user);
$old_image=$this->fct->getonecell("user","trading_licence",$cond_image);
if(!empty($old_image) && file_exists('./uploads/user/'.$old_image)){
unlink("./uploads/user/".$old_image);
 } }


$image1= $this->fct->uploadImage("trading_licence","user");
$update["trading_licence"]=$image1;
}*/

                $update['name'] = $update['first_name'] . ' ' . $update['last_name'];

                $update['address_1'] = $this->input->post("address_1");
                $update['address_2'] = $this->input->post("address_2");

                if ($this->input->post("is_subscribed")) {
                    $update['is_subscribed'] = 1;
                } else {
                    $update['is_subscribed'] = 0;
                }


                $update['updated_date'] = date('Y-m-d h:i:s');
                //$insert['last_login'] = date('Y-m-d h:i:s');


                $this->db->where('id_user', $id_user);
                $this->db->update('user', $update);

                $this->fct->insert_user_license($id_user);


                $new_id = $id_user;
                $newsletter['id_user'] = $new_id;
                $newsletter['created_date'] = date('Y-m-d h:i:s');
                $newsletter['email'] = $user['email'];
                if ($this->input->post("is_subscribed") == 1) {
                    $check = $this->fct->getonerecord('newsletter', array('id_user' => $new_id));
                    if (empty($check)) {
                        $this->db->insert('newsletter', $newsletter);
                    }
                } else {
                    $newsletter['updated_date'] = date('Y-m-d h:i:s');
                    $newsletter['deleted'] = 1;
                    $this->db->where(array('id_user' => $new_id));
                    $this->db->update('newsletter', $newsletter);
                }

                $_data['id'] = $new_id;
                $c_user['id_user'] = $new_id;
                $user = $this->fct->getoneuser($c_user);
                if (checkIfSupplier()) {
                    $categories = array();
                    $categories_arr = array();
                    if (isset($_POST['categories']) && !empty($_POST['categories'])) {
                        $categories = $_POST['categories'];

                        foreach ($categories as $key => $val) {
                            array_push($categories_arr, $key);
                        }
                    }


                    $this->load->model("users_categories_m");

                    $this->users_categories_m->insert_user_categories($new_id, $categories_arr);
                }

                $return['result'] = 1;
                $return['redirect_link'] = route_to('user/profile');
                $return['message'] = lang('system_is_redirecting_to_page');
                $this->session->set_flashdata('success_message', lang('profile_updated'));
                die(json_encode($return));

            }
        } else {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Token is expired please login again.';
            $this->session->set_flashdata('error_message', lang('login_expired'));
            $redirect_link = site_url('user/login');
            $return['result'] = $redirect_link;
        }
        echo json_encode($return);
    }


    public function update_pre_register()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;

        if (!checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $allPostedVals = $this->input->post(NULL, TRUE);
            $allPostedVals['trading_licence'] = "";
            //////////////Personal Information///////////////////////////
            $this->form_validation->set_rules('id_role', 'Role', 'trim|required|xss_clean');

            $this->form_validation->set_rules('trading_name', lang('trading_name'), 'trim|required|xss_clean');
            /*$this->form_validation->set_rules('username',lang('username'),'trim|required|xss_clean');*/
            $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('salutation', lang('salutation'), 'trim|xss_clean');
            $this->form_validation->set_rules('position', lang('position'), 'trim|xss_clean');
            $this->form_validation->set_rules('website', lang('website'), 'trim|xss_clean');
            $this->form_validation->set_rules('password', lang('password'), 'trim|required|xss_clean|min_length[8]|max_length[20]');
            $this->form_validation->set_rules('confirm_password', lang('confirm_Password'), 'trim|required|xss_clean|matches[password]|min_length[8]|max_length[20]');
            /*$this->form_validation->set_rules('username',lang('username'), 'trim|required|xss_clean|callback_validateusernameexistance[]');*/
            $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email|xss_clean|callback_check_if_email_exists[]');

            if (checkIfSupplier()) {

                $this->form_validation->set_rules('category', lang('category'), 'trim|xss_clean|callback_required_categories[]');
            }


            //////////////Address///////////////////////////////////////
            $this->form_validation->set_rules('address_1', lang('address'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('address_2', lang('address'), 'trim|xss_clean');
            $this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');
            /*$this->form_validation->set_rules('phone',lang('phone'),'trim|required|xss_clean');*/
            /*		$this->form_validation->set_rules('fax',lang('fax'), 'callback_fax[]');
		$this->form_validation->set_rules('mobile',lang('mobile'), 'callback_mobile[]');*/
            $this->form_validation->set_rules('phone', lang('phone'), 'callback_phone[]');
            //////////////Others///////////////////////////////////////
            $this->form_validation->set_rules('business_type', lang('business_type'), 'trim|required|xss_clean');
            /*$this->form_validation->set_rules('hear_about_us',lang('hear_about_us'), 'trim|required|xss_clean');*/

            $this->form_validation->set_rules('captcha', lang('captcha'), 'trim|required|xss_clean|callback_validate_captcha[]');
            /*$this->form_validation->set_rules('services_categories',lang('services_categories'),'trim|required|xss_clean');*/
            $this->form_validation->set_rules('comments', lang('comments'), 'trim|xss_clean');
            /*$this->form_validation->set_rules('trading_licence','trading licence','trim|callback_checkUploadLicence[]');*/
            $this->form_validation->set_rules('trading_licence_f', 'trading licence', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } else {

                $cond['status'] = 1;
                $cond['deleted'] = 0;
                $cond['id_user'] = $this->input->post("id_user");
                $cond['token'] = $this->input->post("token");
                $user = $this->fct->getonerow('user', $cond);


                /*			echo "<pre>";
			print_r($user);exit;*/

                if (!empty($user)) {
                    $id_user = $user['id_user'];

                    $update['trading_name'] = $this->input->post("trading_name");

                    $update['salutation'] = $this->input->post("salutation");
                    $update['first_name'] = $this->input->post("first_name");
                    $update['last_name'] = $this->input->post("last_name");
                    $update['username'] = $this->input->post("username");
                    $update['website'] = $this->input->post("website");
                    $update['completed'] = 1;
                    $update['city'] = $this->input->post("city");
                    $update['position'] = $this->input->post("position");
                    $update['pre_registration'] = 1;

                    $update['password'] = $this->input->post("password");
                    $update['id_countries'] = $this->input->post("id_countries");
                    $update['business_type'] = $this->input->post("business_type");
                    $update['hear_about_us'] = $this->input->post("hear_about_us");
                    $update['comments'] = $this->input->post("comments");

                    $phone = $this->input->post("phone");
                    $update['phone'] = $phone[0] . '-' . $phone[1] . '-' . $phone[2];

                    if ($this->fax()) {

                        $fax = $this->input->post("fax");
                        $update['fax'] = $fax[0] . '-' . $fax[1] . '-' . $fax[2];
                    } else {
                        $update['fax'] = 0;
                    }

                    if ($this->mobile()) {
                        $mobile = $this->input->post("mobile");
                        $update['mobile'] = $mobile[0] . '-' . $mobile[1] . '-' . $mobile[2];
                    } else {
                        $update['mobile'] = 0;
                    }

                    if ($_FILES["trading_licence"]["name"]) {

                        if (!empty($id_user)) {
                            $cond_image = array("id_user" => $id_user);
                            $old_image = $this->fct->getonecell("user", "trading_licence", $cond_image);
                            if (!empty($old_image) && file_exists('./uploads/user/' . $old_image)) {
                                unlink("./uploads/user/" . $old_image);
                            }
                        }


                        $image1 = $this->fct->uploadImage("trading_licence", "user");
                        $update["trading_licence"] = $image1;
                    }

                    $update['name'] = $update['first_name'] . ' ' . $update['last_name'];

                    $update['address_1'] = $this->input->post("address_1");
                    $update['address_2'] = $this->input->post("address_2");

                    if ($this->input->post("is_subscribed")) {
                        $update['is_subscribed'] = 1;
                    } else {
                        $update['is_subscribed'] = 0;
                    }


                    $update['updated_date'] = date('Y-m-d h:i:s');
                    //$insert['last_login'] = date('Y-m-d h:i:s');
                    $update['status'] = 2;

                    $this->db->where('id_user', $id_user);
                    $this->db->update('user', $update);

                    $new_id = $id_user;
                    $newsletter['id_user'] = $new_id;
                    $newsletter['created_date'] = date('Y-m-d h:i:s');
                    $newsletter['email'] = $user['email'];
                    if ($this->input->post("is_subscribed") == 1) {
                        $check = $this->fct->getonerecord('newsletter', array('id_user' => $new_id));
                        if (empty($check)) {
                            $this->db->insert('newsletter', $newsletter);
                        }
                    } else {
                        $newsletter['updated_date'] = date('Y-m-d h:i:s');
                        $newsletter['deleted'] = 1;
                        $this->db->where(array('id_user' => $new_id));
                        $this->db->update('newsletter', $newsletter);
                    }

                    $_data['id'] = $new_id;
                    $c_user['id_user'] = $new_id;
                    $user = $this->fct->getoneuser($c_user);
                    if ($user['id_roles'] == 5) {
                        $categories = array();
                        $categories_arr = array();
                        if (isset($_POST['categories']) && !empty($_POST['categories'])) {
                            $categories = $_POST['categories'];

                            foreach ($categories as $key => $val) {
                                array_push($categories_arr, $key);
                            }
                        }


                        $this->load->model("users_categories_m");

                        $this->users_categories_m->insert_user_categories($new_id, $categories_arr);
                    }
                    $return['result'] = 1;
                    $this->loginSucces($user, true);
                    $return['redirect_link'] = route_to('user/profile');
                    $return['message'] = lang('system_is_redirecting_to_page');
                    $this->session->set_flashdata('success_message', lang('profile_updated'));
                    die(json_encode($return));

                }
            }

            $this->load->model('send_emails');
            $this->send_emails->inform_pre_registration_admin($user);
        } else {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Token is expired please login again.';
            $this->session->set_flashdata('error_message', lang('login_expired'));
            $redirect_link = site_url('user/login');
            $return['result'] = $redirect_link;
        }
        echo json_encode($return);
    }

    public function updatenewsletter()
    {

        $lang = "";
        $allPostedVals = $this->input->post(NULL, TRUE);
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;

        if (checkUserIfLogin()) {

            $this->form_validation->set_rules('captcha', lang('captcha'), 'trim|required|xss_clean|callback_validate_captcha[]');
            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } else {
                $user = $this->ecommerce_model->getUserInfo();

                if ($user['result'] != 0) {
                    $return['result'] = 1;
                    if ($this->input->post('is_subscribed')) {
                        if ($user['is_subscribed'] == 1) {
                        } else {

                            $update['is_subscribed'] = 1;
                            $update['updated_date'] = date('Y-m-d h:i:s');
                            $this->db->where('id_user', $user['id_user']);
                            $this->db->update('user', $update);
                            $newsletter = $this->fct->getonerecord('newsletter', array('id_user' => $user['id_user']));
                            if (!empty($newsletter)) {
                                $update_n['updated_date'] = date('Y-m-d h:i:s');
                                $update_n['deleted'] = 0;
                                $cond_newsletter = array('id_user' => $user['id_user']);
                                $this->db->where($cond_newsletter);
                                $this->db->update('newsletter', $update_n);
                            } else {
                                $newsletter['id_user'] = $user['id_user'];
                                $newsletter['email'] = $user['email'];
                                $newsletter['deleted'] = 0;
                                $newsletter['created_date'] = date('Y-m-d h:i:s');
                                $this->db->insert('newsletter', $newsletter);
                                $this->load->model('send_emails');
                                $subscribe = 1;
                                $this->send_emails->sendNewsLetter($subscribe);
                            }
                        }

                        $return['message'] = lang('the_subscription_has_been_saved');
                    } else {
                        $update['is_subscribed'] = 0;
                        $update['updated_date'] = date('Y-m-d h:i:s');
                        $this->db->where('id_user', $user['id_user']);
                        $this->db->update('user', $update);
                        $newsletter = $this->fct->getonerecord('newsletter', array('id_user' => $user['id_user']));
                        if (!empty($newsletter)) {
                            $update_n['deleted'] = 1;
                            $cond_newsletter = array('id_user' => $user['id_user'], 'email' => $user['email']);
                            $this->db->where($cond_newsletter);
                            $this->db->update('newsletter', $update_n);
                            $this->load->model('send_emails');
                            $subscribe = 0;
                            $this->send_emails->sendNewsLetter($subscribe);
                        }
                        $return['message'] = lang('the_subscription_has_been_removed');

                    }

                } else {
                    $return['result'] = 0;
                    $return['message'] = lang('access_denied');
                    $return['redirect_link'] = route_to('user');;
                }

            }
        }

        echo json_encode($return);
    }


    public function loginWithFacebook()
    {

        $allPostedVals = $this->input->post(NULL, TRUE);
        if ($this->session->userdata('redirect_link')) {
            $redirect_link = $this->session->userdata('redirect_link');
            $this->session->unset_userdata('redirect_link');
        } else {
            $redirect_link = site_url('user');
        }
        //$this->form_validation->set_rules('email_fb',lang('email'),'trim|required|valid_email|xss_clean|callback_check_if_email_exists[]');
        //$this->form_validation->set_rules('confirm_email_register','Confirm email', 'trim|required|xss_clean|matches[email_register]');
        //$this->form_validation->set_rules('mobile',lang('mobile'), 'trim|required|xss_clean');

        $bool = true;
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $cond = array(
            'facebook_id' => $id
        );
        //print_r($cond);exit;
        $user = $this->fct->getoneuser($cond);
        if (empty($user)) {

            $insert['correlation_id'] = rand();
            $insert['name'] = $name;
            $insert['username'] = $name;
            $insert['type'] = 'retail';
            $insert['facebook_id'] = $id;
            //$insert['email'] = $this->input->post('email_register');
            //$insert['company_name'] = $this->input->post('company_name');
            //$insert['fax'] = $this->input->post('fax');
            //$insert['mobile'] = $this->input->post('mobile');
            //$insert['password'] = md5($this->input->post('password_register'));
            $insert['created_date'] = date('Y-m-d h:i:s');
            //$insert['expire_date'] = date('Y-m-d h:i:s',strtotime('+30 days'));
            //$insert['last_login'] = date('Y-m-d h:i:s');
            $insert['id_roles'] = 2;
            $insert['status'] = 1;
            $this->db->insert('user', $insert);
            $_data = $insert;
            $_data['id'] = $this->db->insert_id();
            $user = $this->fct->getonerecord('user', array('id_user' => $_data['id']));
            //$_data['send_password'] = $this->input->post('password');
            // send emails
            /*			$this->load->model('send_emails');
			$this->send_emails->sendUserToAdmin($_data);
			$this->send_emails->sendUserReply($insert);*/

            $this->session->set_flashdata('success_message', 'Welcome ' . $name . ' , you have successfully registered with <a href="' . site_url() . '">saiid-kobeisy.com</a>');
            $return['message'] = "Your are registered successfully ! System is redirecting to page.";
            $return['redirect_link'] = $redirect_link;
        } else {
            if ($user['status'] == 0) {
                //	$this->form_validation->set_message('validate_user',lang('account_is_blocked'));
                $this->form_validation->set_message('validate_user', lang('account_is_blocked'));
                $bool = "false";
                return false;
            }


        }
        $update['last_login'] = date('Y-m-d h:i:s');
        $this->db->where('id_user', $user['id_user']);
        $this->db->update('user', $update);
        $this->db->where('id_user', $user['id_user']);
        $this->db->delete('compare_products');
        $this->session->set_userdata('login_id', $user['id_user']);
        $this->session->set_userdata('correlation_id', $user['correlation_id']);
        $this->session->set_userdata('account_type', $user['type']);
        $this->session->set_userdata('user_name', $user['first_name'] . ' ' . $user['last_name']);
        $this->session->set_userdata('username', $user['username']);
        $this->session->set_userdata('email', $user['email']);
        $this->session->set_userdata('last_login', changeDate($update['last_login']));
        $return['result'] = 1;
        $this->session->set_flashdata('success_message', 'Welcome ' . $name . ' , you have successfully login with <a href="' . site_url() . '">saiid-kobeisy.com</a> .');
        $return['message'] = "Your are registered successfully ! System is redirecting to page.";
        $return['redirect_link'] = $redirect_link;


        echo json_encode($return);
    }

    public function profile($val = "")
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $data['userData'] = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
            $data['seo']['meta_title'] = "Account Information";
            if ($this->session->userdata('login_id') == '') {
                $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $this->session->set_userdata('redirect_link', $url);
            }
            $data['changepass'] = $val;
            $data['countries'] = $this->fct->getAll("countries", "title");
            $data['business_type'] = $this->fct->getAll("business_type", "title");
            $data['hear_about_us'] = $this->fct->getAll("how_did_you_hear_about_us", "title");
            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/profile', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }


    public function addresses()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }

        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo']['meta_title'] = 'Address Book';


            $cond1['billing'] = 1;
            $cond1['id_user'] = $user['id_user'];
            $billing = $this->ecommerce_model->getUserAddresses($cond1, 1, 0);

            $cond2['shipping'] = 1;
            $cond2['id_user'] = $user['id_user'];
            $shipping = $this->ecommerce_model->getUserAddresses($cond2, 1, 0);


            $data['default_addresses'][0] = $billing;
            $data['default_addresses'][1] = $shipping;


            /*$cond3['billing']=0;
$cond3['shipping']=0;	*/
            if (!empty($billing['id_users_addresses'])) {
                $cond3['no_billing'] = $billing['id_users_addresses'];
            }
            if (!empty($shipping['id_users_addresses'])) {
                $cond3['no_shipping'] = $shipping['id_users_addresses'];
            }
            $cond3['id_user'] = $user['id_user'];
            $cond3['no_branch'] = 1;
            $data['additional_addresses'] = $this->ecommerce_model->getUserAddresses($cond3);


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/user_addresses', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function address($id = "")
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 16));
            $data['seo']['meta_title'] = "Add Address";


            if ($id != "") {
                $cond['id_user'] = $user['id_user'];
                $cond['id_users_addresses'] = $id;
                $address = $this->ecommerce_model->getUserAddresses($cond, 1, 0);

                if (!empty($address)) $data['seo']['meta_title'] = "Edit Address";;
                $data['address'] = $address;
            }

            $data['countries'] = $this->fct->getAll("countries", "title");
            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/add_address', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }


    public function addressCheckOut($id = "")
    {

        $user = $this->ecommerce_model->getUserInfo();
        $data['userData'] = $user;
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 16));
        $data['seo']['meta_title'] = "Add Address";
        if ($id == "" && $this->input->post('id') != "") {
            $id = $this->input->post('id');
        }
        $data['formType'] = $this->input->post('formType');
        if ($id != "") {
            $cond['id_user'] = $user['id_user'];
            $cond['id_users_addresses'] = $id;
            $address = $this->ecommerce_model->getUserAddresses($cond, 1, 0);

            if (!empty($address)) $data['seo']['meta_title'] = "Edit Address";;
            $data['address'] = $address;
        }

        $data['countries'] = $this->fct->getAll("countries", "title");

        $html = $this->load->view('user/add_checkout_address', $data, true);
        if (checkUserIfLogin()) {
            $return['html'] = $html;
        } else {
            $return['redirect_link'] = route_to('user');
        }
        $return['result'] = 1;
        die(json_encode($return));
    }

    public function miles()
    {
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 28));

            $cond = array();
            $cond['id_user'] = $user['id_user'];

            $miles = $this->custom_fct->getMilesUsed($cond);
            if (empty($miles)) {
                $miles = 0;
            }
            $data['miles'] = $miles;


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/my_miles', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function vouchers()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 16));
            $data['seo']['meta_title'] = "My Vouchers";

            $data['vouchers'] = $this->fct->getAll_cond('users_vouchers', 'sort_order', array('id_user' => $user['id_user']));

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/my_vouchers', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }


    public function voucherDetails($id = "", $rand = "")
    {

        if (!empty($id)) {
            $voucher = $this->fct->getonerecord('users_vouchers', array('id_users_vouchers' => $id, 'rand' => $rand));
            $data['voucher'] = $voucher;
        }
        // get product attribute options

        if (checkUserIfLogin()) {
            if (!empty($voucher)) {
                if ($this->session->userdata('currency') == "") {
                    $this->session->set_userdata('currency', $this->config->item("default_currency"));
                }
                if ($this->session->userdata('login_id') == '') {
                    $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                    $this->session->set_userdata('redirect_link', $url);
                }

                if (!empty($voucher['title']))
                    $data['seo']['meta_title'] = $voucher['title'];

                $data['vouchers'] = $this->fct->getAll('vouchers', 'sort_order');
                $this->template->write_view('header', 'blocks/header', $data);
                $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
                $this->template->write_view('content', 'user/voucher_details', $data);
                $this->template->write_view('footer', 'blocks/footer', $data);

                $this->template->render();
            } else {
                $this->session->set_flashdata('error_message', lang('access_denied'));
                redirect(route_to('vouchers'));
            }
        } else {
            $this->session->set_flashdata('error_message', lang('login_to_continue'));
            redirect(route_to('user/login'));
        }
    }

    ///////////////BRANDS////////////
    public function brands()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin() && checkIfSupplier()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 29));

            $dir = 'desc';
            $sort_order = 'created_date';
            $cond['id_user'] = $user['id_user'];
            $count_news = $this->custom_fct->getBrands($cond);

            $limit = $count_news;

            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            } else $page = 0;
            $data['credits'] = $this->custom_fct->getBrands($cond, $limit, $page, $sort_order, $dir);

            $data['show_items'] = $limit;
            $data['count'] = $count_news;
            $data['offset'] = $page;


            $offset = 0;
            $data['brands'] = $this->custom_fct->getBrands($cond, $count_news, $offset);


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/brands', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function brand($id = "")
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }


        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 30));


            if ($id != "") {
                $brand = $this->custom_fct->getOneBrand($id, $user['id_user']);
            }


            if (!empty($brand)) {
                $data['brand'] = $brand;


                $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 31));
            }


            $data['countries'] = $this->fct->getAll("countries", "title");
            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/add_brand', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function checkSlide()
    {

        $bool = true;
        for ($i = 1; $i < 5; $i++) {
            if ($this->input->post("slide_" . $i . "_f") != "") {

                $bool = false;
            }
        }
        if ($bool) {

            $this->form_validation->set_message('checkSlide', "Please upload at least one.");

            return false;
        } else {
            return true;
        }


    }

    public function phone2()
    {
        $bool = false;
        if ($this->input->post('country_code') != "" && $this->input->post('city_code') != "" && $this->input->post('number') != "") {
            $bool = true;
        }
        if (!$bool) {

            $this->form_validation->set_message('phone', "The phone fields is required.");
            return false;
        } else {
            return true;
        }


    }


    public function phone()
    {

        $phone = $this->input->post('phone');
        $bool = true;
        if (!empty($phone)) {
            foreach ($phone as $key => $val) {
                if (empty($val)) {
                    $bool = false;
                }
            }

            if (!$bool) {
                $this->form_validation->set_message('phone', "The phone fields is required.");
                return false;
            } else {
                return true;
            }
        }

    }

    public function mobile()
    {
        $mobile = $this->input->post('mobile');
        $bool = true;

        foreach ($mobile as $key => $val) {
            if (empty($val)) {
                $bool = false;
            }
        }

        if (!$bool) {
            $this->form_validation->set_message('mobile', "The mobile fields is required.");
            return false;
        } else {
            return true;
        }

    }

    public function fax()
    {
        $fax = $this->input->post('fax');
        $bool = true;
        foreach ($fax as $key => $val) {
            if (empty($val)) {
                $bool = false;
            }
        }

        if (!$bool) {
            $this->form_validation->set_message('fax', "The Fax fields is required.");
            return false;
        } else {
            return true;
        }

    }


    public function checkUploadSlide1()
    {
        if (isset($_FILES['slide_1']['size'])) {
            $size = $_FILES['slide_1']['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];

            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadSlide1', 'Slide exceeds the maximum upload size(' . $size_arr['size_mega'] . 'Mb).');
                return false;
            }
        }
    }

    public function checkUploadSlide2()
    {
        if (isset($_FILES['slide_2']['size'])) {
            $size = $_FILES['slide_2']['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];

            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadSlide2', 'Slide exceeds the maximum upload size(' . $size_arr['size_mega'] . 'Mb).');
                return false;
            }
        }
    }

    public function checkUploadSlide3()
    {
        if (isset($_FILES['slide_3']['size'])) {
            $size = $_FILES['slide_3']['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];

            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadSlide3', 'Slide exceeds the maximum upload size(' . $size_arr['size_mega'] . 'Mb).');
                return false;
            }
        }
    }

    public function checkUploadSlide4()
    {
        if (isset($_FILES['slide_4']['size'])) {
            $size = $_FILES['slide_4']['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];

            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadSlide4', 'Slide exceeds the maximum upload size(' . $size_arr['size_mega'] . 'Mb).');
                return false;
            }
        }
    }

    public function checkUploadCatalog()
    {
        if (isset($_FILES['catalog']['size'])) {
            $size = $_FILES['catalog']['size'];
            $size_arr = getMaxSize('file');
            $max_size = $size_arr['size'];

            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadCatalog', 'Catalog exceeds the maximum upload size(' . $size_arr['size_mega'] . 'Mb).');
                return false;
            }
        }
    }

    public function checkUploadLogo()
    {
        if (isset($_FILES['logo']['size'])) {
            $size = $_FILES['logo']['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];
            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadLogo', 'Logo exceeds the maximum upload size(' . $size_arr['size_mega'] . 'Mb).');
                return false;
            }
        }
    }

    public function checkUploadPhoto()
    {
        if (isset($_FILES['photo']['size'])) {
            $size = $_FILES['photo']['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];
            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadPhoto', 'Photo exceeds the maximum upload size(' . $size_arr['size_mega'] . 'Mb).');
                return false;
            }
        }
    }


    public function submitBrand()
    {

        if (checkUserIfLogin()) {
            $lang = "";
            if ($this->config->item("language_module")) {
                $lang = getFieldLanguage($this->lang->lang());
            }
            /*$data['lang']=$lang;*/
            $allPostedVals = $this->input->post(NULL, TRUE);
            $this->form_validation->set_rules('title', 'brand name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('logo_f', 'logo', 'trim|required|xss_clean|callback_checkUploadLogo[]');
            $this->form_validation->set_rules('catalog_f', 'catalog', 'trim|required|xss_clean|callback_checkUploadCatalog[]');
            $this->form_validation->set_rules('video', 'youtube link', 'trim|xss_clean');

            $this->form_validation->set_rules('url', 'Website Link', 'trim|required|xss_clean');
            $this->form_validation->set_rules('overview', 'Overview', 'trim|required|xss_clean');
            $this->form_validation->set_rules('access_code', 'Access Code', 'trim|xss_clean');
            /*Contact Person*/
            $this->form_validation->set_rules('name', lang('name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', lang('email'), 'trim|required|xss_clean|valid_email');
            $this->form_validation->set_rules('position', lang('position'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('phone', lang('phone'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('slide', 'slide', 'trim|xss_clean|callback_checkSlide[]');
            /*$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');*/
            $this->form_validation->set_rules("photo_f", "photo" . "(" . $this->lang->lang() . ")", "trim|callback_checkUploadPhoto[]");


            /*SLIDES*/
            $this->form_validation->set_rules("slide_1_f", "slide 1" . "(" . $this->lang->lang() . ")", "trim|callback_checkUploadSlide1[]");
            $this->form_validation->set_rules("slide_2_f", "slide 2" . "(" . $this->lang->lang() . ")", "trim|callback_checkUploadSlide2[]");
            $this->form_validation->set_rules("slide_3_f", "slide 3" . "(" . $this->lang->lang() . ")", "trim|callback_checkUploadSlide3[]");
            $this->form_validation->set_rules("slide_4_f", "sldie 4" . "(" . $this->lang->lang() . ")", "trim|callback_checkUploadSlide4[]");

            if ($this->form_validation->run() == FALSE) {

                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }

                echo json_encode($return);
            } else {

                $user = $this->ecommerce_model->getUserInfo();
                $id_user = $user['id_user'];
                $data['id_user'] = $id_user;

                $data['title'] = $this->input->post("title");

                $data['video'] = $this->input->post("video");
                $data['url'] = $this->input->post("url");
                /*$data['catalog_link'] = $this->input->post("catalog_link");*/
                $data['overview'] = $this->input->post("overview");

                $data['status'] = 2;
                $data['access_code'] = $this->input->post("access_code");
                $data['name'] = $this->input->post("name");
                $data['email'] = $this->input->post("email");
                $data['position'] = $this->input->post("position");
                $data['mobile'] = $this->input->post("mobile");


                /*			$phone = $this->input->post("phone");
			$data['phone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];*/

                $data['phone'] = $this->input->post("phone");

                if (!empty($_FILES["logo"]["name"])) {
                    if ($this->input->post("id_brands") != "") {
                        $cond_image = array("id_brands" => $this->input->post("id_brands"));
                        $old_image = $this->fct->getonecell("brands", "logo", $cond_image);
                        if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                            unlink("./uploads/brands/" . $old_image);
                        }
                    }
                    $image1 = $this->fct->uploadImage("logo", "brands");
                    $data["logo"] = $image1;
                }

                if (!empty($_FILES["photo"]["name"])) {
                    if ($this->input->post("id_brands") != "") {
                        $cond_image = array("id_brands" => $this->input->post("id_brands"));
                        $old_image = $this->fct->getonecell("brands", "photo", $cond_image);
                        if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                            unlink("./uploads/brands/" . $old_image);
                        }
                    }
                    $image1 = $this->fct->uploadImage("photo", "brands");
                    $data["photo"] = $image1;
                }


                if (!empty($_FILES["catalog"]["name"])) {
                    if ($this->input->post("id_brands") != "") {
                        $cond_image = array("id_brands" => $this->input->post("id_brands"));
                        $old_image = $this->fct->getonecell("brands", "catalog", $cond_image);
                        if (!empty($old_image) && file_exists('./uploads/catalog/' . $old_image)) {
                            unlink("./uploads/catalog/" . $old_image);
                        }
                    }
                    $image1 = $this->fct->uploadImage("catalog", "brands");
                    $data["catalog"] = $image1;
                }


                for ($i = 1; $i < 5; $i++) {
                    if (!empty($_FILES["slide_" . $i]["name"])) {
                        if ($this->input->post("id_brands") != "") {
                            $cond_image = array("id_brands" => $this->input->post("id_brands"));
                            $old_image = $this->fct->getonecell("brands", "slide_" . $i, $cond_image);
                            if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                                unlink("./uploads/brands/" . $old_image);
                                $sumb_val1 = explode(",", "735x290");
                                foreach ($sumb_val1 as $key => $value) {
                                    if (file_exists("./uploads/brands/" . $value . "/" . $old_image)) {
                                        unlink("./uploads/brands/" . $value . "/" . $old_image);
                                    }
                                }
                            }
                        }
                        $image1 = $this->fct->uploadImage("slide_" . $i, "brands");
                        $this->fct->createthumb($image1, "brands", "735x290");
                        $data["slide_" . $i] = $image1;
                    }
                }


                if ($this->input->post('id_brands') && $this->input->post('id_brands') != "") {
                    $id_brands = $this->input->post('id_brands');
                    $this->session->set_flashdata('success_message', 'The information has been updated.');
                    $cond['id_brands'] = $id_brands;
                    $cond['id_user'] = $id_user;
                    $data['updated_date'] = date('Y-m-d h:i:s');
                    $this->db->where($cond);
                    $this->db->update('brands', $data);
                    $new_id = $id_brands;


                } else {

                    $data['created_date'] = date('Y-m-d h:i:s');
                    $this->session->set_flashdata('success_message', 'The information has been saved.');
                    $this->db->insert('brands', $data);

                    $new_id = $this->db->insert_id();

                }

                $data['id_brands'] = $new_id;
                $data = $this->fct->getonerecord('brands', array('id_brands' => $new_id));
                if ($this->input->post('id_brands') && $this->input->post('id_brands') != "") {
                } else {
                    $this->load->model('send_emails');
                    $this->send_emails->sendBrandToAdmin($data, $user);
                    $this->send_emails->sendBrandToClient($data, $user);
                }
                if ($this->input->post("id_brands") != "") {
                    $redirect_link = route_to('user/brand/' . $this->input->post("id_brands"));
                } else {
                    $redirect_link = route_to('user/brands');
                }
                $return['redirect_link'] = $redirect_link;
                $return['result'] = 1;
                $return['message'] = lang('system_is_redirecting_to_page');
                echo json_encode($return);

            }
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }


    }

    public function delete_row($section, $id)
    {

        //////Delete row which related to user///
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data = $this->fct->getonerecord($section, array('id_' . $section => $id, "id_user" => $user['id_user']));
            if (!empty($data)) {
                $_data = array("deleted" => 1,
                    "deleted_date" => date("Y-m-d h:i:s"));
                $this->db->where("id_" . $section, $id);
                $this->db->update($section, $_data);

                if ($section == "checklist_branches") {
                    $_data2 = array("deleted" => 1,
                        "deleted_date" => date("Y-m-d h:i:s"));
                    $this->db->where("id_users_addresses", $data['id_users_addresses']);
                    $this->db->update('users_addresses', $_data2);
                }
                $this->session->set_flashdata("success_message", "Information was deleted successfully");
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('error_message', lang('access_denied'));
                redirect(route_to('user/login'));
            }
        } else {

        }

    }


///////////////CHECKLIST CATEGORIES////////////
    public function categories()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 32));

            $dir = 'desc';
            $sort_order = 'created_date';
            $cond['id_user'] = $user['id_user'];
            $count_news = $this->pages_fct->getChecklistCategories($cond);
            $limit = $count_news;

            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            } else $page = 0;
            $data['info'] = $this->pages_fct->getChecklistCategories($cond, $limit, $page, $sort_order, $dir);

            $data['show_items'] = $limit;
            $data['count'] = $count_news;
            $data['offset'] = $page;


            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>My List Disposables </li>';
            $breadcrumbs .= '</ul>';
            $data['breadcrumbs'] = $breadcrumbs;


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/checklist_categories', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function category($id = "")
    {


        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 32));


            $data['title'] = 'Add Category';
            if ($id != "") {
                $cond['id_checklist_categories'] = $id;
                $cond['id_user'] = $user['id_user'];
                $category = $this->fct->getonerecord('checklist_categories', $cond);
                $data['title'] = 'Edit Category(' . $category['title'] . ')';
            }

            $data['seo']['meta_title'] = $data['title'];
            $data['seo']['page_title'] = $data['title'];


            if (!empty($category)) {
                $data['category'] = $category;
            }


            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';

            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>' . $data['title'] . '</li>';
            $breadcrumbs .= '</ul>';
            $data['breadcrumbs'] = $breadcrumbs;


            /*		$this->template->write_view('header','blocks/header',$data);
		$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
		$this->template->write_view('content','user/add_categories',$data);
		$this->template->write_view('footer','blocks/footer',$data);
		$this->template->render();*/
            $this->load->view('user/add_categories', $data);
        } else {
            $this->session->set_flashdata('error_message', lang('login_to_continue'));
        }
    }

    public function quotation_email($id = 0, $rand = "")
    {
        $login = checkUserIfLogin();
        $data['login'] = $login;
        $cond['rand'] = $rand;
        $orderData = $this->ecommerce_model->getQuotation($id, $cond);
        $data['orderData'] = $orderData;
        if (!empty($orderData)) {
            if ($login) {
                $user = $this->ecommerce_model->getUserInfo();
            } else {
                $user = array();
            }
            $data['userData'] = $user;
            $this->load->view('user/send_quotation', $data);
        } else {

        }
    }

    public function checkEmails()
    {
        $emails = $this->input->post('emails');
        $bool = true;
        foreach ($emails as $val) {
            $email = $val;
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $bool = false;
                $result = "Invalid email format";
            }

            if (empty($email)) {
                $bool = false;
                $result = "Please fill email(s) field.";
            }


        }
        if (!$bool) {
            $this->form_validation->set_message('checkEmails', $result);
            return false;
        } else {
            return true;
        }

    }


    public function sendQuotation()
    {
        $login = checkUserIfLogin();
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $allPostedVals = $this->input->post(NULL, TRUE);
        $id_user = $this->input->post('id_user');

        $this->form_validation->set_rules('email', lang('email'), 'callback_checkEmails[]');
        if (!$login) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email_from', 'E-mail', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'trim');
        }


        if ($this->form_validation->run() == FALSE) {

            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }

            die(json_encode($return));
        } else {

            $cond['rand'] = $this->input->post('rand');
            $id = $this->input->post('id');

            if (!$login) {
                $insert['name'] = $this->input->post('name');
                $insert['phone'] = $this->input->post('phone');
                $insert['email_from'] = $this->input->post('email_from');
            }
            $insert['email_to'] = implode(',', $this->input->post('emails'));
            $insert['created_date'] = date('Y-m-d h:i:s');
            $this->db->insert('record_submit_quotation', $insert);


            $orderData = $this->ecommerce_model->getQuotation($id, $cond);
            if (!$login) {
                $orderData['user']['name'] = $insert['name'];
                $orderData['user']['email'] = $insert['email_from'];
            }
            $this->load->model('send_emails');
            $this->send_emails->sendQuotation($orderData);

            $return['message'] = 'Yout quotation has been sent.';
            $return['result'] = 1;
            die(json_encode($return));
        }


    }

    public function submitCategory()
    {


        if (checkUserIfLogin()) {
            $lang = "";
            if ($this->config->item("language_module")) {
                $lang = getFieldLanguage($this->lang->lang());
            }
            /*$data['lang']=$lang;*/
            $allPostedVals = $this->input->post(NULL, TRUE);
            $this->form_validation->set_rules('title', 'Category name', 'trim|required|xss_clean');

            if ($this->form_validation->run() == FALSE) {

                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                /*$return['captcha'] = $this->fct->createNewCaptcha();*/
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }

                echo json_encode($return);
            } else {


                $user = $this->ecommerce_model->getUserInfo();
                $id_user = $user['id_user'];
                $data['id_user'] = $id_user;
                $title = $this->input->post("title");
                $data['title'] = $title;
                $data["title_url"] = $this->fct->cleanURL("checklist_categories", url_title($title));


                if ($this->input->post('id') != "") {

                    $cond2['id_checklist_categories'] = $this->input->post('id');
                    $data['created_date'] = date('Y-m-d h:i:s');
                    $this->db->where($cond2);
                    $this->db->update('checklist_categories', $data);
                    $id_category = $this->input->post('id');
                    $this->session->set_flashdata('success_message', $title . ' has been updated.');
                } else {
                    ;
                    $data['created_date'] = date('Y-m-d h:i:s');
                    $this->db->insert('checklist_categories', $data);
                    $id_category = $this->db->insert_id();
                    if ($this->input->post('product_stocklist') == "") {
                        $this->session->set_flashdata('success_message', $title . ' has been added to your stock-list categoreis.');
                    }
                }

                if ($this->input->post('product_stocklist') == 1) {
                    $return['message'] = $title . ' has been added to your stock-list categoreis.';
                    $return['html'] = '<div class="stockList_row mod"><label><span><input type="checkbox" name="stocklist[]" class="stocklist_category"  value="' . $id_category . '"/>' . $title . '</span></label></div>';
                    $return['html_append'] = '<li class="dropdown-li checklist-li"><a class="dropdown-toggle" href="' . route_to('user/checklist_categories/' . $id_category) . '">' . $title . '</a>';
                    $return['html_appendTo_before'] = "#sub-dropdown-7 li:last-child";
                } else {
                    $return['redirect_link'] = route_to('user/checklist_categories/' . $id_category);
                    $return['message'] = lang('system_is_redirecting_to_page');
                }
                $return['result'] = 1;

                die(json_encode($return));
            }
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }


    }


///////////////CHECKLIST BRANCHES////////////
    /*public function branches()
	{	if($this->session->userdata('login_id') == '') {
			$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$this->session->set_userdata('redirect_link',$url);
		}
		if(checkUserIfLogin()) {
		$user = $this->ecommerce_model->getUserInfo();
		$data['userData']=$user;
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>33));

$dir='desc';
$sort_order='created_date';
$cond['id_user']=$user['id_user'];
$cond['branch']=1;
$count_news = $this->pages_fct->getChecklistBranches($cond);
$limit=$count_news;

if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->pages_fct->getChecklistBranches($cond,$limit,$page,$sort_order,$dir);

$data['show_items']=$limit;
$data['count']=$count_news;
$data['offset']=$page;


                $breadcrumbs = "";
                $breadcrumbs .= '<ul>';
                $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
				$breadcrumbs .= '<li>My List Branches</li>';
                $breadcrumbs .= '</ul>';
				$data['breadcrumbs']=$breadcrumbs;



		$this->template->write_view('header','blocks/header',$data);
		$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
		$this->template->write_view('content','user/checklist_branches',$data);
		$this->template->write_view('footer','blocks/footer',$data);
		$this->template->render();
		}else {
			$this->session->set_flashdata('error_message',lang('access_denied'));
			redirect(route_to('user/login'));
		}
	}*/

    public function branches($operations = "")
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['user'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 33));

            $dir = 'desc';
            $sort_order = 'created_date';
            $cond['id_user'] = $user['id_user'];
            $cond['branch'] = 1;
            $count_news = $this->pages_fct->getChecklistBranches($cond);
            $limit = $count_news;
            if ($operations != "") {
                $limit = 1;
                $data['my_branch'] = "my-branch";
            }
            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            } else $page = 0;


            $data['info'] = $this->pages_fct->getChecklistBranches($cond, $limit, $page, $sort_order, $dir);


            $data['user_favorites'] = $this->ecommerce_model->getUserFavorites($this->session->userdata('login_id'));

            $data['show_items'] = $limit;
            $data['count'] = $count_news;
            $data['offset'] = $page;


            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            if ($operations != "") {
                $breadcrumbs .= '<li>My branch</li>';
            } else {
                $breadcrumbs .= '<li>My List Branches</li>';
            }
            $breadcrumbs .= '</ul>';
            $data['breadcrumbs'] = $breadcrumbs;


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/checklist_branches', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function branch($id = "")
    {
        $my_branch = 0;
        if ($id == "my-branch") {
            $my_branch = 1;
            $id = "";
        }
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 30));

            $data['my_branch'] = $my_branch;
            $data['title'] = 'Add Branch';
            if ($id != "") {
                $cond['id_checklist_branches'] = $id;
                $cond['id_user'] = $user['id_user'];
                $branch_arr = $this->fct->getonerecord('checklist_branches', $cond);

                $cond2['id_users_addresses'] = $branch_arr['id_users_addresses'];
                $branch = $this->ecommerce_model->getUserAddresses($cond, 1, 0);

                $branch['branch_name'] = $branch_arr['title'];
                $branch['id_checklist_branches'] = $branch_arr['id_checklist_branches'];
                $data['title'] = 'Edit branch(' . $branch_arr['title'] . ')';
            }


            $data['seo']['page_title'] = $data['title'];
            $data['seo']['meta_title'] = $data['title'];

            if (!empty($branch)) {
                $data['branch'] = $branch;
                $data['categories'] = $this->fct->getAll_cond('checklist_categories', 'id_checklist_categories', array('id_checklist_branches' => $id, 'id_user' => $user['id_user']));
            }


            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li><a href=' . route_to('user/branches') . '>My List Branches</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li>' . $data['title'] . '</li>';
            $breadcrumbs .= '</ul>';
            $data['breadcrumbs'] = $breadcrumbs;


            $data['countries'] = $this->fct->getAll("countries", "title");
            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/add_branches', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }


    public function submitBranch()
    {

        if (checkUserIfLogin()) {
            $lang = "";
            if ($this->config->item("language_module")) {
                $lang = getFieldLanguage($this->lang->lang());
            }
            /*$data['lang']=$lang;*/
            $allPostedVals = $this->input->post(NULL, TRUE);
            $this->form_validation->set_rules('branch_name', 'branch name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');

            /*$this->form_validation->set_rules('fax',lang('fax'), 'trim|xss_clean');*/
            $this->form_validation->set_rules('phone', lang('phone'), 'callback_phone[]');
            $this->form_validation->set_rules('street_one', lang('street'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('id_countries', lang('country'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('captcha', lang('captcha'), 'trim|required|xss_clean|callback_validate_captcha[]');
            if ($this->form_validation->run() == FALSE) {

                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }

                echo json_encode($return);
            } else {


                $user = $this->ecommerce_model->getUserInfo();
                $id_user = $user['id_user'];
                $data['id_user'] = $id_user;
                $data['first_name'] = $this->input->post("first_name");
                $data['last_name'] = $this->input->post("last_name");
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['company'] = $this->input->post("company");

                /*			$data['phone'] = $this->input->post("telephone");
			$data['fax'] = $this->input->post("fax");*/


                $phone = $this->input->post("phone");
                $data['phone'] = $phone[0] . '-' . $phone[1] . '-' . $phone[2];

                if ($this->fax()) {

                    $fax = $this->input->post("fax");
                    $data['fax'] = $fax[0] . '-' . $fax[1] . '-' . $fax[2];
                } else {
                    $data['fax'] = "";
                }

                $data['street_one'] = $this->input->post("street_one");
                $data['street_two'] = $this->input->post("street_two");
                $data['city'] = $this->input->post("city");
                $data['state'] = $this->input->post("state");
                $data['postal_code'] = $this->input->post("postal_code");
                $data['id_countries'] = $this->input->post("id_countries");
                $data['updated_date'] = date('Y-m-d h:i:s');
                $data['branch'] = 1;


                if ($this->input->post('id_users_addresses') && $this->input->post('id_users_addresses') != "") {
                    $id_address = $this->input->post('id_users_addresses');
                    $cond['id_users_addresses'] = $id_address;
                    $cond['id_user'] = $id_user;
                    $this->db->where($cond);
                    $this->db->update('users_addresses', $data);
                    $new_id = $id_address;
                    $return['message'] = 'The branch has been updated.';

                } else {
                    $data['created_date'] = date('Y-m-d h:i:s');
                    $this->session->set_flashdata('success_message', 'The branch has been saved.');
                    $this->db->insert('users_addresses', $data);
                    $new_id = $this->db->insert_id();
                    $redirect_link = route_to('user/branches');
                    $return['redirect_link'] = $redirect_link;
                    $return['message'] = lang('system_is_redirecting_to_page');
                }

                ////////////////////////BRANCH//////////////////////
                $branch_name = $this->input->post("branch_name");
                $branch_data["title_url"] = $this->fct->cleanURL("branches", url_title($branch_name));
                $cond2['id_users_addresses'] = $new_id;
                $cond2['id_user'] = $id_user;
                $branch_data['updated_date'] = date('Y-m-d h:i:s');
                $branch_data['title'] = $branch_name;
                $branch_data['id_users_addresses'] = $new_id;
                $branch_data['id_user'] = $id_user;
                $branch = $this->fct->getonerecord('checklist_branches', $cond2);
                if (!empty($branch)) {
                    $cond2['id_checklist_branches'] = $branch['id_checklist_branches'];
                    $this->db->where($cond2);
                    $this->db->update('checklist_branches', $branch_data);
                    $id_branch = $branch['id_checklist_branches'];

                } else {
                    $branch_data['created_date'] = date('Y-m-d h:i:s');
                    $this->db->insert('checklist_branches', $branch_data);
                    $id_branch = $this->db->insert_id();
                }

                //////////////////INSERT BRANCHES/////////////

                $categories = $this->input->post('categories');

                if (!empty($categories)) {
                    foreach ($categories as $key => $val) {
                        if (!empty($val)) {
                            $category = $this->fct->getonerecord('checklist_categories', array('id_checklist_branches' => $id_branch, 'id_checklist_categories' => $key));
                            $data2['title'] = $val;
                            $data2['id_checklist_branches'] = $id_branch;
                            $data2['id_user'] = $id_user;
                            $data2["title_url"] = $this->fct->cleanURL("branches", url_title($val));
                            if (!empty($category)) {
                                $data2['updated_date'] = date('Y-m-d h:i:s');
                                $this->db->where(array('id_checklist_categories' => $key));
                                $this->db->update('checklist_categories', $data2);
                            } else {
                                $data2['created_date'] = date('Y-m-d h:i:s');
                                $this->db->insert('checklist_categories', $data2);
                            }
                        }

                    }
                }


                $return['redirect_link'] = route_to('user/branch/' . $id_branch);
                $return['result'] = 1;
                echo json_encode($return);

            }
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }


    }

////////////////////////Credits////////////////////////////////////
    public function credits()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 24));


            /*		$limit=20;
		$offset=0;
		$cond['id_user']=$user['id_user'];
		$data['credits'] = $this->custom_fct->getCredits($cond,$limit,$offset);*/

            $limit = 6;
            $dir = 'desc';
            $sort_order = 'created_date';
            $cond['id_user'] = $user['id_user'];
            $count_news = $this->custom_fct->getCredits($cond);

            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            } else $page = 0;
            $data['credits'] = $this->custom_fct->getCredits($cond, $limit, $page, $sort_order, $dir);
            $data['show_items'] = $limit;
            $data['count'] = $count_news;
            $data['offset'] = $page;


            $cond['group_by'] = 'currency';
            $offset = 0;
            $data['currencies'] = $this->custom_fct->getCredits($cond, $count_news, $offset);


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/my_credits', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function add_credits()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 25));


            $data['countries'] = $this->fct->getAll('countries', 'title');
            $data['vouchers'] = $this->fct->getAll_cond('users_vouchers', 'sort_order', array('id_user' => $user['id_user']));

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/add_credits', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }


    public function submitAddress()
    {

        if (checkUserIfLogin()) {
            $lang = "";
            if ($this->config->item("language_module")) {
                $lang = getFieldLanguage($this->lang->lang());
            }
            /*$data['lang']=$lang;*/
            $allPostedVals = $this->input->post(NULL, TRUE);
            $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');

            $this->form_validation->set_rules('phone', lang('telephone'), 'trim|required|xss_clean');


            $this->form_validation->set_rules('street_one', lang('street'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('id_countries', lang('country'), 'trim|required|xss_clean');
            /*$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');*/
            if ($this->form_validation->run() == FALSE) {

                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }

                echo json_encode($return);
            } else {

                $user = $this->ecommerce_model->getUserInfo();
                $id_user = $user['id_user'];
                $data['id_user'] = $id_user;
                $data['first_name'] = $this->input->post("first_name");
                $data['last_name'] = $this->input->post("last_name");

                /*			$mobile = $this->input->post("mobile");
			$data['mobile'] = $mobile[0].'-'.$mobile[1].'-'.$mobile[2];*/
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['company'] = $this->input->post("company");
                /*			$phone = $this->input->post("phone");
			$data['phone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];*/

                $data['phone'] = $this->input->post("phone");
                $data['mobile'] = $this->input->post("mobile");
                /*			$fax = $this->input->post("fax");
			$data['fax'] = $fax[0].'-'.$fax[1].'-'.$fax[2];*/

                $data['fax'] = $this->input->post("fax");

                $data['street_one'] = $this->input->post("street_one");
                $data['street_two'] = $this->input->post("street_two");
                $data['city'] = $this->input->post("city");
                $data['state'] = $this->input->post("state");
                $data['postal_code'] = $this->input->post("postal_code");
                $data['id_countries'] = $this->input->post("id_countries");
                $data['updated_date'] = date('Y-m-d h:i:s');


                if ($this->input->post("billing") && $this->input->post("billing") != "") {
                    $data['billing'] = 1;
                } else {
                    $data['billing'] = 0;
                }
                $return['billing'] = $data['billing'];
                if ($this->input->post("shipping") && $this->input->post("shipping") != "") {
                    $data['shipping'] = 1;
                } else {
                    $data['shipping'] = 0;
                }

                /*		if($data['shipping']==1){
		$shipping_update['shipping']=0;
		$cond_shipping['id_user']=$id_user;
		$this->db->where($cond_shipping);
		$this->db->update('users_addresses',$shipping_update);
		}*/

                /*	if($data['billing']==1){
		$billing_update['billing']=0;
		$cond_billing['id_user']=$id_user;
		$this->db->where($cond_billing);
		$this->db->update('users_addresses',$billing_update);
		}	*/

                if ($this->input->post('id_users_addresses') && $this->input->post('id_users_addresses') != "") {
                    $id_address = $this->input->post('id_users_addresses');
                    $message = "The address has been updated.";
                    if ($this->input->post('checkout') == "") {
                        $this->session->set_flashdata('success_message', $message);
                    }
                    $cond['id_users_addresses'] = $id_address;
                    $cond['id_user'] = $id_user;

                    $this->db->where($cond);
                    $this->db->update('users_addresses', $data);
                    $new_id = $id_address;

                } else {
                    $message = "The address has been saved.";
                    $data['created_date'] = date('Y-m-d h:i:s');
                    if ($this->input->post('checkout') != "") {
                        $this->session->set_flashdata('success_message', $message);
                    }
                    $this->db->insert('users_addresses', $data);
                    $new_id = $this->db->insert_id();
                    $data['new'] = 1;
                }

                $data['submit_address'] = 1;


                $redirect_link = route_to('user/addresses');
                $return['id'] = $new_id;
                $return['result'] = 1;
                if ($this->input->post('checkout') == "") {
                    $return['redirect_link'] = $redirect_link;
                    $return['message'] = lang('system_is_redirecting_to_page');
                } else {
                    $address = $this->fct->getonerecord('users_addresses', array('id_users_addresses' => $new_id));
                    $data['address'] = $address;
                    $data['address']['countries_title'] = $this->fct->getonecell('countries', 'title', array('id_countries' => $data['id_countries']));
                    $data['formType'] = 'billing';
                    $billing_html = $this->load->view('blocks/address_box', $data, true);
                    $data['formType'] = 'shipping';
                    $shipping_html = $this->load->view('blocks/address_box', $data, true);
                    $return['message'] = $message;

                    $return['billing_html'] = $billing_html;
                    $return['shipping_html'] = $shipping_html;
                }
                die(json_encode($return));

            }
        } else {
            if ($this->input->post('checkout') != "") {
                $this->session->set_flashdata('error_message', lang('access_denied'));
                redirect(route_to('user/login'));
            }
        }


    }

    public function submit_crdits()
    {

        if (checkUserIfLogin()) {
            $lang = "";
            if ($this->config->item("language_module")) {
                $lang = getFieldLanguage($this->lang->lang());
            }
            /*$data['lang']=$lang;*/
            $allPostedVals = $this->input->post(NULL, TRUE);
            $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('telephone', lang('telephone'), 'trim|xss_clean');
            $this->form_validation->set_rules('fax', lang('fax'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('street_one', lang('street'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('id_countries', lang('country'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('captcha', lang('captcha'), 'trim|required|xss_clean|callback_validate_captcha[]');
            if ($this->form_validation->run() == FALSE) {

                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }

                echo json_encode($return);
            } else {

                $user = $this->ecommerce_model->getUserInfo();
                $id_user = $user['id_user'];
                $data['id_user'] = $id_user;
                $data['first_name'] = $this->input->post("first_name");
                $data['last_name'] = $this->input->post("last_name");
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['company'] = $this->input->post("company");
                $data['phone'] = $this->input->post("telephone");
                $data['fax'] = $this->input->post("fax");
                $data['street_one'] = $this->input->post("street_one");
                $data['street_two'] = $this->input->post("street_two");
                $data['city'] = $this->input->post("city");
                $data['state'] = $this->input->post("state");
                $data['postal_code'] = $this->input->post("postal_code");
                $data['id_countries'] = $this->input->post("id_countries");
                $data['updated_date'] = date('Y-m-d h:i:s');


                if ($this->input->post("billing") && $this->input->post("billing") != "") {
                    $data['billing'] = 1;
                } else {
                    $data['billing'] = 0;
                }

                if ($this->input->post("shipping") && $this->input->post("shipping") != "") {
                    $data['shipping'] = 1;
                } else {
                    $data['shipping'] = 0;
                }

                /*		if($data['shipping']==1){
		$shipping_update['shipping']=0;
		$cond_shipping['id_user']=$id_user;
		$this->db->where($cond_shipping);
		$this->db->update('users_addresses',$shipping_update);
		}*/

                /*	if($data['billing']==1){
		$billing_update['billing']=0;
		$cond_billing['id_user']=$id_user;
		$this->db->where($cond_billing);
		$this->db->update('users_addresses',$billing_update);
		}	*/

                if ($this->input->post('id_users_addresses') && $this->input->post('id_users_addresses') != "") {
                    $id_address = $this->input->post('id_users_addresses');
                    $this->session->set_flashdata('success_message', 'The address has been updated.');
                    $cond['id_users_addresses'] = $id_address;
                    $cond['id_user'] = $id_user;

                    $this->db->where($cond);
                    $this->db->update('users_addresses', $data);

                } else {

                    $data['created_date'] = date('Y-m-d h:i:s');
                    $this->session->set_flashdata('success_message', 'The address has been saved.');
                    $this->db->insert('users_addresses', $data);
                }

                $redirect_link = route_to('user/addresses');
                $return['redirect_link'] = $redirect_link;
                $return['result'] = 1;
                $return['message'] = lang('system_is_redirecting_to_page');

                die(json_encode($return));

            }
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }


    }

    public function deleteAddress($id_address = "")
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {

            if ($id_address == "") {
                $this->session->set_flashdata('error_message', lang('access_denied'));
                redirect(route_to('user/addresses'));
            } else {
                $user = $this->ecommerce_model->getUserInfo();
                $id_user = $user['id_user'];
                $cond['id_users_addresses'] = $id_address;
                $cond['id_user'] = $id_user;
                $getAddress = $this->fct->getonerecord('users_addresses', $cond);
                if (!empty($getAddress)) {
                    $this->db->where($cond);
                    $this->db->delete('users_addresses');
                    if ($this->input->post('ajax') == "") {
                        $this->session->set_flashdata('success_message', 'The address has been deleted.');
                        redirect(route_to('user/addresses'));
                    }
                } else {
                    if ($this->input->post('ajax') == "") {
                        $this->session->set_flashdata('error_message', lang('access_denied'));
                        redirect(route_to('user/addresses'));
                    }
                }
            }
        } else {
            if ($this->input->post('ajax') == "") {
                $this->session->set_flashdata('error_message', lang('access_denied'));
                redirect(route_to('user/addresses'));
            }
        }

        if ($this->input->post('ajax') != "") {
            $return['result'] = 1;
            die(json_encode($return));
        }

    }


    public function dropfiles()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['userData'] = $user;
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 27));


            $data['drop_files'] = $this->fct->getonerecord('user_drop_file', array('id_user' => $user['id_user']));


            if (empty($data["drop_files"])) {
                $_data["id_user"] = $user['id_user'];
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert('user_drop_file', $_data);
                $new_id = $this->db->insert_id();
                $cond2 = array("id_user_drop_file" => $new_id);
                $data["drop_files"] = $this->fct->getonerecord('user_drop_file', $cond2);
            }

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/drop_file', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function checkDropFiles()
    {
        $bool = true;
        for ($i = 1; $i < 3; $i++) {
            if ($this->input->post("file_" . $i . "_f") != "") {
                $bool = false;
            }
        }
        if ($bool) {
            $this->form_validation->set_message('checkDropFiles', "Please upload at least one.");
            return false;
        } else {
            return true;
        }

    }

    public function checkUploadFile1()
    {
        if (isset($_FILES['file_1']['size'])) {
            $size = $_FILES['file_1']['size'];
            $size_arr = getMaxSize('file');
            $max_size = $size_arr['size'];
            if ($max_size > $size) {
                return true;
            } else {

                $this->form_validation->set_message('checkUploadFile1', 'File exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
                return false;
            }
        }
    }

    public function checkUploadFile2()
    {
        if (isset($_FILES['file_2']['size'])) {
            $size = $_FILES['file_2']['size'];
            $size_arr = getMaxSize('file');
            $max_size = $size_arr['size'];

            if ($max_size > $size) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadFile2', 'File exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
                return false;
            }
        }
    }


    public function submitFiles()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $user = $this->ecommerce_model->getUserInfo();
        if (checkUserIfLogin()) {

            $allPostedVals = $this->input->post(NULL, TRUE);
            $allPostedVals['file_1'] = "";
            $allPostedVals['file_2'] = "";
            //////////////Personal Information///////////////////////////

            $this->form_validation->set_rules('file', 'file', 'trim|xss_clean|callback_checkDropFiles[]');
            $this->form_validation->set_rules('file_1', 'file', 'trim|xss_clean|callback_checkUploadFile1[]');
            $this->form_validation->set_rules('file_2', 'file', 'trim|xss_clean|callback_checkUploadFile2[]');

            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } else {

                $id_user = $user['id_user'];
                $id = $this->input->post('id');
                $update["updated_date"] = date('Y-m-d h:i:s');;

                if (isset($_FILES["file_1"]["name"])) {
                    if (!empty($id)) {
                        $cond_image = array("id_user" => $id_user);
                        $old_image = $this->fct->getonecell("user_drop_file", "file_1", $cond_image);
                        if (!empty($old_image) && file_exists('./uploads/user_drop_file/' . $old_image)) {
                            unlink("./uploads/user_drop_file/" . $old_image);
                        }
                    }
                    $image1 = $this->fct->uploadImage("file_1", "user_drop_file");
                    $update["file_1"] = $image1;
                }

                if (isset($_FILES["file_2"]["name"])) {
                    if (!empty($id)) {
                        $cond_image = array("id_user" => $id_user);
                        $old_image = $this->fct->getonecell("user_drop_file", "file_2", $cond_image);
                        if (!empty($old_image) && file_exists('./uploads/user_drop_file/' . $old_image)) {
                            unlink("./uploads/user_drop_file/" . $old_image);
                        }
                    }
                    $image1 = $this->fct->uploadImage("file_2", "user_drop_file");
                    $update["file_2"] = $image1;
                }


                $cond2['id_user_drop_file'] = $id;

                $this->db->where($cond2);
                $this->db->update('user_drop_file', $update);


                $return['result'] = 1;
                $return['redirect_link'] = route_to('user/dropfiles');
                $return['message'] = lang('system_is_redirecting_to_page');
                $this->session->set_flashdata('success_message', lang('files_updated'));
                die(json_encode($return));

            }
        } else {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Token is expired please login again.';
            $this->session->set_flashdata('error_message', lang('login_expired'));
            $redirect_link = site_url('user/login');
            $return['result'] = $redirect_link;
        }
        echo json_encode($return);
    }

    public function newsletter()
    {
        if (!checkUserIfLogin()) {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $data['userData'] = $this->ecommerce_model->getUserInfo();

            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 15));
            if ($this->session->userdata('login_id') == '') {
                $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $this->session->set_userdata('redirect_link', $url);
            }
            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('content', 'user/newsletter', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function change_password()
    {
        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }
        if (checkUserIfLogin()) {
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 8));
            if ($this->session->userdata('login_id') == '') {
                $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $this->session->set_userdata('redirect_link', $url);
            }

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('content', 'user/change_password', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);

            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            $redirect_link = site_url('user/login');
            $return['result'] = $redirect_link;
        }
    }


/////////////////////////////////////////////////////////////////////////////////////////////


    /*	public function password()
	{
		if(isset($_POST) && !empty($_POST)) {
			//echo 'test';exit;
			$this->form_validation->set_rules('email',lang('email'),'trim|required|valid_email|xss_clean|callback_validate_email[]');
			if($this->form_validation->run() == FALSE) {
				$data['error_messages'] = validation_errors();
				$this->passwordTemplate($data);
			}
			else {
				redirect(route_to('user'));
			}
		}
		else {
			$this->passwordTemplate();
		}
	}*/




    function loginbypassword()
    {
        if (isset($_GET['token'])) {
            $token = $_GET['token'];
            $cond = array('token' => $token);
            $check_token = $this->fct->getonerow('password_requests', $cond);
            /*print '<pre>';
		print_r($check_token);
		exit;*/
            if (empty($check_token)) {
                $this->session->set_flashdata('error_message', lang('invalid_token'));
                redirect(route_to('user/password'));
            } else {
                if ($check_token['expiration_date'] < date('Y-m-d h:i:s')) {
                    $this->session->set_flashdata('error_message', lang('token_is_expired'));
                    redirect(route_to('user/password'));
                } else {
                    if ($check_token['logged'] == 1) {
                        $this->session->set_flashdata('error_message', lang('token_was_used'));
                        redirect(route_to('user/password'));
                    } else {
                        $data['logged'] = 1;
                        $data['used_date'] = date('Y-m-d h:i:s');
                        $this->db->where('id_password_requests', $check_token['id_password_requests']);
                        $this->db->update('password_requests', $data);
                        //print_r($check_token);exit;
                        $user = $this->fct->getonerow('user', array('id_user' => $check_token['id_user']));
                        $this->session->set_userdata('login_id', $user['id_user']);
                        $this->session->set_userdata('user_name', $user['name']);
                        $this->session->set_userdata('email', $user['email']);
                        $this->session->set_userdata('last_login', changeDate($user['last_login']));

                        $this->load_loginbypassword($check_token['id_user']);
                    }
                }
            }
        } else {
            reirect(site_url());
        }
    }


    function setPassword($id_user = 0, $rand)
    {
        $user = $this->fct->getonerecord('user', array('id_user' => $id_user, 'correlation_id' => $rand, 'login_guest' => 0));

        if (!empty($user)) {


            $data['login_guest'] = 1;
            $data['updated_date'] = date('Y-m-d h:i:s');
            $this->db->where('id_user', $id_user);
            $this->db->update('user', $data);

            $data['setpassword'] = 1;
            $this->load_loginbypassword($id_user, $data);

        } else {
            $this->session->set_flashdata('error_message', lang('invalid_token'));
            redirect(route_to('user/login'));
        }
    }

    function load_loginbypassword($id_user, $data = array())
    {

        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 17));
        $data['page_title'] = $data['seo']['title' . getFieldLanguage()];

        if (isset($data['setpassword'])) {
            $data['page_title'] = 'Set Password';
            $data['seo']['page_title'] = 'Set Password';
            $data['seo']['meta_title'] = 'Set Password';
        }
        // breadcrumbs
        $breadcrumbs = array();
        $breadcrumbs[0]['title'] = lang('home');
        $breadcrumbs[0]['link'] = site_url();
        $breadcrumbs[1]['title'] = $data['page_title'];
        $crumbs['breadcrumbs'] = $breadcrumbs;
        // end breadcrumbs

        if (!isset($data['error_messages'])) {
            $data['success_messages'] = lang('password_logged_in_success');
        }

        $data['id_user'] = $id_user;


        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'user/fillnewpassword', $data);


        /*$this->template->write_view('bottom','blocks/bottomtabs',$data);*/
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function check_password()
    {
        $id = $this->session->userdata('login_id');
        $password = $this->input->post('current_password');

        $cond = array(
            'id_user' => $id,
            'password' => md5($password)
        );
        //print_r($cond);exit;
        $user = $this->fct->getoneuser($cond);

        //print '<pre>';print_r($user);exit;
        if (empty($user)) {
            $this->form_validation->set_message('check_password', 'Current  password not correct');
            return false;
        } else {
            return true;
        }
    }


    function update_new_password()
    {
        if (isset($_POST) && !empty($_POST)) {
            $allPostedVals = $this->input->post(NULL, TRUE);
            $this->form_validation->set_rules('id_user', lang('user'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', lang('password'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('confirm_password', lang('confirm_password'), 'trim|required|xss_clean|matches[password]');
            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } else {
                $id_user = $this->input->post('id_user');
                $password = $this->input->post('password');
                $password = md5($password);
                $data['password'] = $password;
                $data['updated_date'] = date('Y-m-d h:i:s');
                $data['last_login'] = date('Y-m-d h:i:s');
                $this->db->where('id_user', $id_user);
                $this->db->update('user', $data);

                $user = $this->fct->getonerecord('user', array('id_user' => $id_user));

                if ($user['status'] == 1) {
                    $update['last_login'] = date('Y-m-d h:i:s');
                    $this->db->where('id_user', $user['id_user']);
                    $this->db->update('user', $update);
                    $this->db->where('id_user', $user['id_user']);
                    $this->db->delete('compare_products');
                    $this->loginSucces($user, true);

                    $return['result'] = 1;
                    $return['errors'] = array();
                    $return['message'] = lang('password_updated');
                    $return['redirect_link'] = route_to('user');
                    $this->session->set_flashdata('success_message', lang('password_updated'));
                } else {
                    $return['result'] = 0;
                    $return['errors']['confirm_password'] = '';
                    $return['message'] = 'Srry! your account is blocked.';
                    die(json_encode($return));
                }
                //redirect(route_to('user'));

            }
        } else {
            $this->session->set_flashdata('error_message', lang('using_fake_link'));
            $return['result'] = 0;
            $return['message'] = '';
            $return['redirect_link'] = route_to('user/password');
        }
        echo json_encode($return);

    }

    function update_password()
    {
        if (isset($_POST) && !empty($_POST)) {
            $allPostedVals = $this->input->post(NULL, TRUE);
            $this->form_validation->set_rules('id_user', lang('user'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('current_password', 'Current Password', 'trim|required|callback_check_password[]');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('confirm_new_password', 'Retype New Password', 'trim|required|xss_clean|matches[new_password]');
            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } else {
                $id_user = $this->input->post('id_user');
                $password = $this->input->post('new_password');
                $password = md5($password);
                $data['password'] = $password;
                $data['updated_date'] = date('Y-m-d h:i:s');
                $data['last_login'] = date('Y-m-d h:i:s');
                $this->db->where('id_user', $id_user);
                $this->db->update('user', $data);
                $return['result'] = 1;
                $return['errors'] = array();
                $return['message'] = lang('password_updated');
                //$this->session->set_flashdata('success_message',lang('password_updated'));
                //redirect(route_to('user'));
                $return['redirect_link'] = route_to('user');
            }
        } else {
            $this->session->set_flashdata('error_message', lang('using_fake_link'));
            $return['result'] = 0;
            $return['message'] = '';
            $return['redirect_link'] = route_to('user/password');
        }
        echo json_encode($return);

    }

/////////////////////////////////////////////////////////////////////////////////////////////

    public function orders($id_orders = '', $rand = "")
    {
        $bool = true;
        if (empty($id_orders) && !checkUserIfLogin()) {
            $bool = false;
        }
        if (isset($_GET['vpc_TxnResponseCode'])) {
        }

        if ($bool) {

            $data['seo']['meta_title'] = 'My Orders';
            $data['page_title'] = 'My Orders';
            if (checkUserIfLogin()) {
                $data['userData'] = $this->ecommerce_model->getUserInfo();
            }


            // breadcrumbs
            $breadcrumbs = array();
            $breadcrumbs[0]['title'] = lang('home');
            $breadcrumbs[0]['link'] = site_url();
            $breadcrumbs[1]['title'] = $data['page_title'];
            $breadcrumbs[1]['link'] = route_to('user');
            // the function is processed below
            // end breadcrumbs

            $list = 1;
            if ($id_orders != '') {

                $cond['rand'] = $rand;
                $data['order_details'] = $this->ecommerce_model->getOrder($id_orders, $cond);


                if (!empty($data['order_details'])) {

                    $list = 0;
                    $data['seo']['meta_title'] = 'Order Details#' . $data['order_details']['id_orders'];
                    $breadcrumbs[2]['title'] = lang('orders');
                    $breadcrumbs[2]['link'] = route_to('user/orders');
                    $breadcrumbs[3]['title'] = lang('order_details');
                }

            }
            if ($list == 1) {

                $data['user_orders'] = $this->ecommerce_model->getUserOrders($this->session->userdata('login_id'));

                $breadcrumbs[2]['title'] = lang('orders');
            }
            $crumbs['breadcrumbs'] = $breadcrumbs;

            $cart_items = $this->cart->contents();
            $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            /*	$this->template->write_view('content','blocks/breadcrumbs',$crumbs);*/
            if ($list == 1) {
                $this->template->write_view('content', 'user/orders', $data);
            } else {
                $this->template->write_view('content', 'user/order_details', $data);
            }

            /*$this->template->write_view('bottom','blocks/bottomtabs',$data);*/
            $this->template->write_view('footer', 'blocks/footer', $data);

            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////

    public function quotation($id_orders = '', $rand = "")
    {
        $data['login'] = checkUserIfLogin();

        if (isset($_GET['vpc_TxnResponseCode'])) {
        }

        if ($this->session->userdata('login_id') || ($this->input->get('gateway') && $id_orders != '') || ($this->input->get('gateway2') && $id_orders != '') || ($rand != '' && $id_orders != '')) {

            $data['seo']['meta_title'] = 'My Quotations';
            $data['page_title'] = 'My Quotations';
            if ($this->session->userdata('login_id')) {
                $data['userData'] = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
            }


            // breadcrumbs
            $breadcrumbs = array();
            $breadcrumbs[0]['title'] = lang('home');
            $breadcrumbs[0]['link'] = site_url();
            $breadcrumbs[1]['title'] = $data['page_title'];
            $breadcrumbs[1]['link'] = route_to('user');
            // the function is processed below
            // end breadcrumbs

            $list = 1;

            $cond['rand'] = $rand;
            $data['order_details'] = $this->ecommerce_model->getQuotation($id_orders, $cond);
            if (!empty($data['order_details'])) {
                $list = 0;
                $data['seo']['meta_title'] = 'Quotation Details#' . $data['order_details']['id_quotation'];
                $breadcrumbs[2]['title'] = 'Quotations';
                $breadcrumbs[2]['link'] = route_to('user/orders');
                $breadcrumbs[3]['title'] = 'Quotation Details';
            } else {
                $list = 1;
            }


            if ($list == 1) {
                $data['user_quotation'] = $this->ecommerce_model->getUserQuotations($this->session->userdata('login_id'));

                $breadcrumbs[2]['title'] = lang('orders');
            }
            $crumbs['breadcrumbs'] = $breadcrumbs;


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            /*	$this->template->write_view('content','blocks/breadcrumbs',$crumbs);*/
            if ($list == 1) {
                $this->template->write_view('content', 'user/quotation', $data);
            } else {
                $this->template->write_view('content', 'user/quotation_details', $data);
            }

            /*$this->template->write_view('bottom','blocks/bottomtabs',$data);*/
            $this->template->write_view('footer', 'blocks/footer', $data);

            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function checklist($section = "", $id = "")
    {

        $data['section'] = $section;
        $data['breadcrumbs'] = "";
        $data['formID'] = "updateChecklist";
        $data['formAction'] = route_to('user/updateChecklist');
        $data['btn_name'] = "Update My List";
        $data['type_section'] = "";
        $data['selected_section'] = array();
        $data['info_section'] = "";

        $data['table'] = '';
        $data['title'] = "My List";
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 14));
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['bool'] = true;
            if ($section == "categories") {
                $data['bool'] = true;
                $data['section'] = $section;

                $data['info_section'] = $this->fct->getonerecord('checklist_categories', array('id_checklist_categories' => $id, 'id_user' => $user['id_user']));
                $data['title'] = "Checklist for category " . $data['info_section']['title'];
                $data['table'] = 'checklist_categories';
                $cond['id_checklist_categories'] = $id;

                $cond['id_user'] = $user['id_user'];
                $data['id'] = $id;
                $selected_checklist_categories = array();
                $selected_checklist_categories = $this->pages_fct->select_user_checklist_categories($cond);

                if (isset($val_categories)) {
                    $selected_checklist_categories = $val_categories;
                }
                $data['selected_section'] = $selected_checklist_categories;
                $cond1['id_user'] = $user['id_user'];
                $categories = $this->fct->getAll_cond('checklist_categories', 'sort_order', $cond1);
                $data['section_data'] = $categories;

                $data['seo']['meta_title'] = $data['info_section']['title'];


                $breadcrumbs = "";
                $breadcrumbs .= '<ul>';
                $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
                $breadcrumbs .= '<li><a href=' . route_to('user/categories') . '>My List Disposables </a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';

                $breadcrumbs .= '<li>' . $data['info_section']['title'] . '</li>';
                $breadcrumbs .= '</ul>';
                $data['breadcrumbs'] = $breadcrumbs;

            }

            if ($section == "branches") {
                $data['bool'] = true;
                $data['section'] = $section;

                $branch_arr = $this->fct->getonerecord('checklist_branches', array('id_checklist_branches' => $id, 'id_user' => $user['id_user']));
                $cond2['id_users_addresses'] = $branch_arr['id_users_addresses'];
                $branch = $this->ecommerce_model->getUserAddresses($cond2, 1, 0);

                $branch['branch_name'] = $branch_arr['title'];
                $branch['id_checklist_branches'] = $branch_arr['id_checklist_branches'];
                $data['info_section'] = $branch;

                $data['title'] = "Checklist for branch " . $data['info_section']['branch_name'];
                $data['table'] = 'checklist_branches';
                $cond['id_checklist_branches'] = $id;
                $cond['id_user'] = $user['id_user'];
                $data['id'] = $id;
                $selected_checklist_branches = array();
                $selected_checklist_branches = $this->pages_fct->select_user_checklist_branches($cond);
                $data['seo']['meta_title'] = $data['info_section']['branch_name'];
                $data['seo']['page_title'] = $data['info_section']['branch_name'];
                if (isset($val_branches)) {
                    $selected_checklist_branches = $val_branches;
                }
                $data['selected_section'] = $selected_checklist_branches;
                $cond1['id_user'] = $user['id_user'];
                $branches = $this->fct->getAll_cond('checklist_branches', 'sort_order', $cond1);

                $data['section_data'] = $branches;


                $breadcrumbs = "";
                $breadcrumbs .= '<ul>';
                $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
                $breadcrumbs .= '<li><a href=' . route_to('user/branches') . '>My List Branches</a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';

                $breadcrumbs .= '<li>' . $data['info_section']['branch_name'] . '</li>';
                $breadcrumbs .= '</ul>';
                $data['breadcrumbs'] = $breadcrumbs;

            }


            $data['user'] = $user;
            $data['lang'] = $this->lang->lang();


            $data['user_favorites'] = $this->ecommerce_model->getUserFavorites($this->session->userdata('login_id'));


            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'user/favorites', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

    public function checklist_categories($id = "")
    {
        $data['breadcrumbs'] = "";
        $data['title'] = "Checklist";
        $data['popup_id'] = "checklist_categories_p";
        $data['formID'] = 'updateChecklist';
        $data['formAction'] = route_to('user/updateChecklist');
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 14));

        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['info_section'] = $this->fct->getonerecord('checklist_categories', array('id_checklist_categories' => $id, 'id_user' => $user['id_user']));
            if (!empty($data['info_section'])) {
                $data['bool'] = false;

                $data['bool'] = true;
                $data['info_section'] = $this->fct->getonerecord('checklist_categories', array('id_checklist_categories' => $id, 'id_user' => $user['id_user']));
                $data['title'] = "Checklist for category " . $data['info_section']['title'];
                $data['table'] = 'checklist_categories';
                $data['checklist_products'] = 1;
                $data['type_section'] = 'checklist_selected_products';
                $cond['id_checklist_categories'] = $id;
                $cond['id_user'] = $user['id_user'];

                $data['id'] = $id;
                $selected_checklist_categories = array();
                $selected_checklist_categories = $this->pages_fct->select_user_checklist_categories($cond);


                if (isset($val_categories)) {
                    $selected_checklist_categories = $val_categories;
                }
                $data['selected_section'] = $selected_checklist_categories;
                $cond1['id_user'] = $user['id_user'];
                $categories = $this->fct->getAll_cond('checklist_categories', 'sort_order', $cond1);
                $data['section_data'] = $categories;

                $data['seo']['meta_title'] = $data['info_section']['title'];
                $data['seo']['page_title'] = $data['info_section']['title'];
                $data['section'] = 'categories';


                $breadcrumbs = "";
                $breadcrumbs .= '<ul>';
                $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
                $breadcrumbs .= '<li><a href=' . route_to('user/categories') . '>My List Disposables </a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';

                $breadcrumbs .= '<li>' . $data['info_section']['title'] . '</li>';
                $breadcrumbs .= '</ul>';
                $data['breadcrumbs'] = $breadcrumbs;


                $data['user'] = $user;
                $data['lang'] = $this->lang->lang();

                $data['user_favorites'] = $this->ecommerce_model->getUserFavorites($this->session->userdata('login_id'), "", $data['info_section']['id_checklist_categories'], 'select');

                //print '<pre>';print_r($data['favorites']);exit;


                $this->template->write_view('header', 'blocks/header', $data);
                $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
                $this->template->write_view('content', 'user/checklist_categories', $data);
                $this->template->write_view('footer', 'blocks/footer', $data);
                $this->template->render();
            } else {
                $this->session->set_flashdata('error_message', lang('please_login'));
                redirect(route_to('user/login'));
            }
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function checklist_products($id = "")
    {
        $data['breadcrumbs'] = "";
        $data['btn_name'] = "Add";
        $data['title'] = "Checklist";
        $data['formID'] = 'updatePopUpForm';
        $data['formAction'] = route_to('user/updateChecklist');
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 14));
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();

            $bool = true;


            $data['info_section'] = $this->fct->getonerecord('checklist_categories', array('id_checklist_categories' => $id, 'id_user' => $user['id_user']));
            $data['title'] = "Checklist for category " . $data['info_section']['title'];
            $data['table'] = 'checklist_categories';
            $data['checklist_products'] = 2;
            $data['type_section'] = 'checklist_unselect_products';
            $cond['id_checklist_categories'] = $id;
            $cond['id_user'] = $user['id_user'];


            $data['id'] = $id;
            $selected_checklist_categories = array();
            $selected_checklist_categories = $this->pages_fct->select_user_checklist_categories($cond);


            if (isset($val_categories)) {
                $selected_checklist_categories = $val_categories;
            }
            $data['selected_section'] = $selected_checklist_categories;
            $cond1['id_user'] = $user['id_user'];
            $categories = $this->fct->getAll_cond('checklist_categories', 'sort_order', $cond1);
            $data['section_data'] = $categories;

            $data['seo']['meta_title'] = $data['info_section']['title'];

            $data['section'] = 'categories';

            $breadcrumbs = "";
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href=' . route_to('user/checklist') . '>My List</a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
            $breadcrumbs .= '<li><a href=' . route_to('user/categories') . '>My List Disposables </a></li>';
            $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';

            $breadcrumbs .= '<li>' . $data['info_section']['title'] . '</li>';
            $breadcrumbs .= '</ul>';
            $data['breadcrumbs'] = $breadcrumbs;


            $data['bool'] = $bool;
            $data['user'] = $user;
            $data['lang'] = $this->lang->lang();


            $data['user_favorites'] = $this->ecommerce_model->getUserFavorites($this->session->userdata('login_id'), "", $id, 'unselect');


            //print '<pre>';print_r($data['favorites']);exit;


            $this->load->view('user/favorites_cart', $data);


        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

    public function removeCategory($id = 0)
    {
        $bool = false;

        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $category_checklist = $this->fct->getonerecord('checklist_categories', array('id_user' => $user['id_user'], 'id_checklist_categories' => $id));
            if (!empty($category_checklist)) {
                $bool = true;
            }
        }

        if ($bool) {
            $cond['id_checklist_categories'] = $id;
            $this->db->delete('checklist_categories', $cond);
            $this->session->set_flashdata('success_message', 'Category has been deleted.');
            redirect(route_to('user/checklist'));
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }


    }


    public function updateChecklist()
    {
        $type_section = $this->input->post('type_section');
        $id = $this->input->post('id');
        $section = $this->input->post('section');
        $user = $this->ecommerce_model->getUserInfo();


        $lang = "";
        $type_section = $this->input->post('type_section');
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;

        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $allPostedVals = $this->input->post(NULL, TRUE);

            //////////////Personal Information///////////////////////////
            $this->form_validation->set_rules('section', 'section', 'trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } else {

                $cond['id_user'] = $user['id_user'];
                $cond['id_checklist_categories'] = $this->input->post('id');
                $section = $this->input->post('section');


                if ($section == "branches") {
                    $cond['id_checklist_branches'] = $this->input->post('id');
                    $this->pages_fct->insert_user_checklist_branches($cond, $checklist);
                }

                $submit = $this->input->post('submit');

                if ($this->input->post('bulk_option') == "add-to-cart") {
                    $result = $this->ecommerce_model->multi_add();

                    if ($result) {
                        $message = $result;
                    } else {
                        $message = "";
                    }
                    $this->session->set_flashdata('success_message', '');
                    $this->session->set_flashdata('error_message', '');
                    $return['result'] = 1;
                    $return['mod_message'] = 'mod1';
                    $return['message'] = $message;
                    die(json_encode($return));

                } else {
                    $return['result'] = 1;
                    /*$return['message'] = lang('system_is_redirecting_to_page');*/
                    $message = 'My List has been updated.';
                    $return['message'] = $message;

                    if ($type_section == "") {
                        $checklist = array();
                        if (isset($_POST['checklist']) && !empty($_POST['checklist'])) {
                            $checklist = $_POST['checklist'];
                        }
                        $cond['id_checklist_categories'] = $this->input->post('id');
                        $this->pages_fct->updateStockList($cond, $checklist);
                    }


                    /*$this->session->set_flashdata('success_message','The information has been updated.');*/
                    $redirect_link = site_url('user/checklist/' . $section . '/' . $this->input->post('id'));
                    /*$return['redirect_link'] = $redirect_link;*/

                    if ($type_section == "checklist_selected_products" || $type_section == "checklist_unselect_products") {

                        $checklist = array();
                        if (isset($_POST['checklist']) && !empty($_POST['checklist'])) {
                            $checklist = $_POST['checklist'];
                        }

                        if ($section == "categories") {
                            $cond['id_checklist_categories'] = $this->input->post('id');
                            $this->pages_fct->insert_user_checklist_categories($cond, $checklist);
                        }

                        $message = "";
                        $cond['id_' . $section] = $id;
                        $cond['id_user'] = $user['id_user'];
                        $data['user'] = $user;
                        $data['bool'] = true;
                        $data['title'] = "Checklist";
                        $data['formID'] = 'updateChecklist';
                        $data['formAction'] = route_to('user/updateChecklist');
                        $data['id'] = $id;
                        $data['section'] = $section;
                        $data['type_section'] = $type_section;

                        $selected_checklist_categories = array();
                        $select = "";


                        $selected_checklist_categories = $this->pages_fct->select_user_checklist_categories($cond);


                        $currrent_selected_checklist_categories = array();

                        $data['checklist_products'] = 1;
                        $currrent_selected_checklist_categories = $this->input->post('checklist');

                        if (empty($currrent_selected_checklist_categories)) {
                            $currrent_selected_checklist_categories = array();
                        }

                        $data['current_selected_section'] = $currrent_selected_checklist_categories;
                        $data['selected_section'] = $selected_checklist_categories;
                        $cond1['id_user'] = $user['id_user'];
                        $categories = $this->fct->getAll_cond('checklist_categories', 'sort_order', $cond1);
                        $data['section_data'] = $categories;
                        $data['table'] = 'checklist_categories';
                        $data['user_favorites'] = $this->ecommerce_model->getUserFavorites($this->session->userdata('login_id'), "", $id, 'select');

                        if ($this->input->post('bulk_option') == "remove-all-list" && !empty($checklist)) {
                            $message = 'My List has been updated.';
                        }
                        $data['info_section'] = $this->fct->getonerecord('checklist_categories', array('id_checklist_categories' => $id, 'id_user' => $user['id_user']));
                        $return['checklist'] = $this->load->view('user/f_cart', $data, true);
                        $return['favorites_form'] = $this->load->view('blocks/favorites_forms', $data, true);
                        $return['message'] = $message;


                    }

                    die(json_encode($return));
                }
            }
        } else {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Token is expired please login again.';
            $this->session->set_flashdata('error_message', lang('login_expired'));
            $redirect_link = site_url('user/login');
            $return['result'] = $redirect_link;
        }


        echo json_encode($return);
    }

    function purchases_checklist()
    {
        if (!checkUserIfLogin()) {
            $this->session->set_userdata('redirect_link', $_SERVER['HTTP_REFERER']);
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        } else {
            $user_favorites = array();
            $user = $this->ecommerce_model->getUserInfo();
            $cond_fav['id_user'] = $user['id_user'];
            $section = $this->input->post('section');
            if ($section == "categories") {
                $cond_fav['id_checklist_categories'] = $this->input->post('id');

                $user_favorites = $this->pages_fct->getUserWishlist_byCategories($cond_fav);
            }

            if ($section == "branches") {
                $cond_fav['id_checklist_branches'] = $this->input->post('id');
                $user_favorites = $this->pages_fct->getUserWishlist_byBranches($cond_fav);
            }


            // load cart content after days
            $cart_items = $this->cart->contents();
            if (empty($cart_items))
                $cart_items = $this->ecommerce_model->loadUserCart();
            //print '<pre>';print_r($_POST);exit;
            $lang = 'en';

            foreach ($user_favorites as $fav) {

                $id = $fav['id_products'];
                $stock_text = '';
                $product = $this->fct->getonerow('products', array('id_products' => $id));
                //	print '<pre>';print_r($product);exit;
                $list_price = $product['list_price'];
                $discount_expiration = $product['discount_expiration'];
                $price = $product['price'];
                $redeem_miles = $product['redeem_miles'];
                $miles = $this->fct->getonecell('miles', 'miles', array('id_miles' => $product['id_miles']));

                $inhouse_quantity = $product['quantity'];
                $options = array();
                $options_p = array();
                $options['redeem_miles'] = $redeem_miles;
                $options['miles'] = $miles;

                if (isset($fav['options']) && !empty($fav['options'])) {

                    $options_p = explode(",", $fav['options']);

                    $results = $this->ecommerce_model->getOptionsByIds($options_p);

                    $combination = serializecartOptions($results);


                    //echo $combination;exit;
                    $stock = $this->fct->getonerow('products_stock', array('id_products' => $id, 'combination' => $combination));

                    if (!empty($stock)) {
                        $list_price = $stock['list_price'];
                        $discount_expiration = $stock['discount_expiration'];
                        $price = $stock['price'];

                        $redeem_miles = $stock['redeem_miles'];
                        $miles = $this->fct->getonecell('miles', 'miles', array('id_miles' => $stock['id_miles']));

                        $options['miles'] = $miles;

                        $options['redeem_miles'] = $redeem_miles;
                        $options['product_options'] = $options_p;
                        $inhouse_quantity = $stock['quantity'];
                        $combination_arr = unSerializeStock($stock['combination']);
                        $i = 0;

                        foreach ($combination_arr as $ar) {
                            $stock_text .= $results[$i]['title' . getFieldLanguage($lang)] . ' ';
                            $i++;
                        }
                    }
                }


                //echo $inhouse_quantity;exit;
                //echo $price;exit;
                $qty = 1;
                if (isset($fav['qty'])) {
                    $qty = $fav['qty'];
                }

                $price = displayCustomerPrice($list_price, $discount_expiration, $price);


                //echo $qty;exit;
                $flag = TRUE;
                $dataTmp = $this->cart->contents();

                foreach ($dataTmp as $item) {
                    $ok = 0;
                    if (!empty($options_p)) {

                        if ($item['id'] == $id && isset($item['options']['product_options']) && $item['options']['product_options'] == $options_p) {

                            $ok = 1;
                        }
                    } else {
                        if ($item['id'] == $id) {
                            $ok = 1;
                        }
                    }
                    if ($ok == 1) {
                        $newQTY = $qty;
                        $flag = FALSE;
                        $data = array(
                            'rowid' => $item['rowid'],
                            'qty' => $newQTY
                        );
                        if (!empty($options)) {
                            $data['options'] = $options;
                        }
                        // check price segment
                        $id_stock = 0;
                        if (isset($stock) && !empty($stock))
                            $id_stock = $stock['id_products_stock'];
                        $price_segment = $this->ecommerce_model->checkPriceSegment($id, $id_stock, $newQTY);

                        if ($price_segment != '' && $price_segment < $price) {
                            $price = $price_segment;
                            //$data['price'] = $price;
                        }
                        $data['price'] = $price;
                        // end check price segment
                        if ($inhouse_quantity < $qty) {
                            $this->session->set_flashdata('error_message', lang('stock_not_available') . ' ' . $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text);
                        } else {

                            $this->cart->update($data);
                            // update in database: shopping cart
                            $update['updated_date'] = date('Y-m-d H:i:s');
                            $update['qty'] = $newQTY;
                            $update['price'] = $price;
                            $cond_uu = array();
                            $cond_uu['rowid'] = $item['rowid'];
                            if ($this->session->userdata('login_id') == '') {
                                $cond_uu['sessionid'] = $this->session->userdata('session_id');
                            } else {
                                $cond_uu['id_user'] = $this->session->userdata('login_id');
                            }
                            $this->db->update('shopping_cart', $update, $cond_uu);

                            // end update in database: shopping cart
                            // update in database: stock
                            $comb = '';
                            if (isset($combination)) $comb = $combination;
                            //echo $newQTY;exit;
                            /*$this->ecommerce_model->UpdateProductStockInDatabase($id,$inhouse_quantity,$item['qty'],$newQTY,$comb);*/
                            // end update in database: stock
                            $this->session->set_flashdata('success_message', $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . lang('cart_added'));
                        }
                        break;
                    }
                }

                ///echo 'aaa'.$price;exit;
                if ($flag) {

                    $newQTY = $qty;
                    $data = array(
                        'id' => $id,
                        'qty' => $newQTY,
                        'price' => $price,
                        'name' => 'product'
                    );
                    if (!empty($options)) {
                        $data['options'] = $options;
                    }

                    // check price segment
                    $id_stock = 0;
                    if (isset($stock) && !empty($stock))
                        $id_stock = $stock['id_products_stock'];
                    $price_segment = $this->ecommerce_model->checkPriceSegment($id, $id_stock, $newQTY);

                    if ($price_segment != '' && $price_segment < $price) {
                        $price = $price_segment;
                        $data['price'] = $price;
                    }

                    // end check price segment
                    if ($inhouse_quantity < $newQTY) {
                        $this->session->set_flashdata('error_message', lang('stock_not_available') . ' ' . $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text);
                        $return['result'] = 1;
                        $return['message'] = lang('system_is_redirecting_to_page');
                        $return['redirect_link'] = $_SERVER['HTTP_REFERER'];
                        die(json_encode($return));
                    } else {


                        if ($miles == "") {
                            $miles = 0;
                        }
                        if ($redeem_miles == "") {
                            $redeem_miles = 0;
                        }

                        $newrowid = $this->cart->insert($data);


                        // insert in database: shopping cart
                        $insert['rowid'] = $newrowid;
                        $insert['created_date'] = date('Y-m-d H:i:s');
                        $insert['id_products'] = $id;
                        $insert['sessionid'] = $this->session->userdata('session_id');
                        $insert['qty'] = $newQTY;
                        $insert['price'] = $price;
                        $insert['miles'] = $miles;
                        $insert['redeem_miles'] = $redeem_miles;
                        $insert['name'] = 'product';
                        if ($this->session->userdata('login_id') != '') {
                            $insert['id_user'] = $this->session->userdata('login_id');
                        }

                        if (!empty($options_p)) {
                            $results = $this->ecommerce_model->getOptionsByIds($options_p);
                            $insert['options'] = serializecartOptions($results);
                        }


                        $this->db->insert('shopping_cart', $insert);
                        // end insert in database: shopping cart
                        // update in database: stock
                        $comb = '';
                        if (isset($combination)) $comb = $combination;
                        /*$this->ecommerce_model->UpdateProductStockInDatabase($id,$inhouse_quantity,0,$newQTY,$comb);*/
                        // end update in database: stock
                        $this->session->set_flashdata('success_message', $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . ' ' . lang('cart_added'));
                    }
                }
            }


            $return['result'] = 1;
            $return['message'] = lang('system_is_redirecting_to_page');
            $this->session->set_flashdata('success_message', 'You products has been added to your cart, please proceed with checkout below.');
            $return['redirect_link'] = route_to('cart/checkout');
            die(json_encode($return));

        }
    }


    public function removeFavorite($id_favorites)
    {
        if ($this->session->userdata('login_id')) {
            $cond = array(
                'id_user' => $this->session->userdata('login_id'),
                'id_favorites' => $id_favorites
            );
            $check = $this->fct->getonerow('favorites', $cond);

            if (empty($check)) {
                $this->session->set_flashdata('error_message', lang('no_product_selected'));
                redirect(route_to('user/checklist'));
            } else {
                $product = $this->fct->getonerecord('products', array('id_products' => $check['id_products']));
                $cond1 = array(
                    'id_user' => $this->session->userdata('login_id'),
                    'id_favorites' => $id_favorites
                );
                $this->db->delete('favorites', $cond1);
                $this->session->set_flashdata('success_message', $product['title'] . ' ' . lang('favorite_removed'));
                redirect(route_to('user/checklist'));
            }
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }


    public function removeUserCategory($id_checklist_categories, $id_favorites)
    {
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $cond = array(
                'id_user' => $user['id_user'],
                'id_favorites' => $id_favorites,
                'id_checklist_categories' => $id_checklist_categories
            );
            $check = $this->fct->getonerow('user_checklist_categories', $cond);

            if (empty($check)) {
                $this->session->set_flashdata('error_message', lang('access_denied'));
                redirect(route_to('user/checklist'));
            } else {
                $this->db->delete('user_checklist_categories', $cond);
                $this->session->set_flashdata('success_message', 'Removed Successfully');
                redirect(route_to('user/checklist_categories/' . $id_checklist_categories));
            }
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function compareProducts()
    {
        //if($this->session->userdata('login_id')) {


        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 13));
        $data['page_title'] = $data['seo']['title' . getFieldLanguage()];
        $id_user = 0;
        if ($this->session->userdata('login_id')) {
            $id_user = $this->session->userdata('login_id');
        } else {
            $data['page_title'] = "Compare Products";
            $data['seo']['meta_title'] = "Compare Products";
        }
        $session_id = $this->session->userdata('session_id');
        $data['user_compare_products'] = $this->ecommerce_model->getUserCompareProducts($id_user, $session_id);

        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('content', 'user/compare_products', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
        /*	}
	else {
		    $redirect_link = route_to('user/compareProducts');
			$this->session->set_userdata('redirect_link',$redirect_link);
			//echo $redirect_link;exit;
			$this->session->set_flashdata('error_message',lang('please_login'));
			redirect(route_to('user/login'));
	}*/
    }

    public function removecompareProducts($id_products)
    {
        //if($this->session->userdata('login_id')) {
        /*$cond = array(
			'id_user'=>$this->session->userdata('login_id'),
			'id_products'=>$id_products
		);
		$check = $this->fct->getonerow('compare_products',$cond);
		if(empty($check)) {
			$this->session->set_flashdata('error_message',lang('no_product_selected'));
			redirect(route_to('user/compareProducts'));
		}
		else {
			$cond1 = array(
				'id_user'=>$this->session->userdata('login_id'),
				'id_products'=>$id_products
			);
			$this->db->delete('compare_products',$cond1);*/

        $id_user = 0;
        if ($this->session->userdata('login_id')) {
            $id_user = $this->session->userdata('login_id');
        }
        $cond['id_products'] = $id_products;
        $product = $this->fct->getonerecord('products', $cond);
        /*$this->db->where("session_id",$this->session->userdata('session_id'));
		if($id_user != 0)
		$this->db->or_where("id_user",$id_user);
		$this->db->delete('compare_products');*/
        $q = 'DELETE FROM compare_products WHERE id_products = ' . $id_products . ' AND ( session_id = "' . $this->session->userdata('session_id') . '"';
        if ($id_user != 0)
            $q .= ' OR id_user = ' . $id_user;
        $q .= ' )';
        $this->db->query($q);
        $this->session->set_flashdata('success_message', $product['title'] . ' ' . lang('compare_removed'));
        redirect($_SERVER['HTTP_REFERER']);
        //}
        /*}
	else {
		$this->session->set_flashdata('error_message',lang('please_login'));
		redirect(route_to('user/login'));
	}*/
    }

    public function removeAllcompareProducts()
    {


        $id_user = 0;
        if (checkUserIfLogin()) {
            $id_user = $this->session->userdata('login_id');
        }
        $cond['id_products'] = $id_products;
        $product = $this->fct->getonerecord('products', $cond);
        /*$this->db->where("session_id",$this->session->userdata('session_id'));
		if($id_user != 0)
		$this->db->or_where("id_user",$id_user);
		$this->db->delete('compare_products');*/
        $q = 'DELETE FROM compare_products WHERE ( session_id = "' . $this->session->userdata('session_id') . '"';
        if ($id_user != 0)
            $q .= ' OR id_user = ' . $id_user;
        $q .= ' )';
        $this->db->query($q);
        $this->session->set_flashdata('success_message', lang('compare_list_cleared'));
        redirect($_SERVER['HTTP_REFERER']);

    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function getUserAddressBook()
    {
        $address_id = $this->input->post('address_id');
        $address = "";
        if ($address_id != "") {
            $address = $this->fct->getonerow('users_addresses', array('id_users_addresses' => $address_id));
        }
        if (empty($address)) {
            $userData = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
            $address['first_name'] = $userData['first_name'];
            $address['last_name'] = $userData['last_name'];
            $address['email'] = $userData['email'];
            $address['phone'] = $userData['mobile'];
        }

        echo json_encode($address);
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function quotation_print($id, $rand = "")
    {
        $cond['rand'] = $rand;
        $orderData = $this->ecommerce_model->getQuotation($id, $cond);

        $data['orderData'] = $orderData;
        $data['page_title'] = 'Spamiles Quotation';
        if (!empty($orderData)) {
            $template = $this->load->view('emails/quotation', $data, true);;
            $data['template'] = $template;
            $this->load->view('back_office/orders/invoice', $data);
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function invoice($id, $rand = "")
    {

        $cond['rand'] = $rand;
        $orderData = $this->ecommerce_model->getOrder($id, $cond);

        $data['orderData'] = $orderData;
        if (!empty($orderData)) {

            $template = $this->load->view('emails/order', $data, true);;
            $data['template'] = $template;
            $this->load->view('back_office/orders/invoice', $data);
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

    public function check_newsletter()
    {
        $cond['email'] = $this->input->post('email_newsletter');
        $cond['deleted'] = 0;
        $this->db->where($cond);
        $query = $this->db->get('newsletter');
        $res = $query->row_array();


        if (count($res) > 0) {
            $this->form_validation->set_message('check_newsletter', 'This email already exist .');
            return false;
        } else {
            return true;
        }
    }

    public function validate_google_captcha($recaptcha_response_value)
    {
        $captcha = $this->serviceLocator->captcha();
        if ($captcha->validate($recaptcha_response_value)) {
            return true;
        }

        $this->form_validation->set_message('validate_google_captcha', "Captcha code is not valid - try reloading page");
        return false;
    }

    public function add_newsletter()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);

        $this->form_validation->set_rules('g-recaptcha-response',"Recaptcha" , 'trim|required|xss_clean|callback_validate_google_captcha[]');
        $this->form_validation->set_rules('email_newsletter', 'Email', 'trim|required|valid_email|callback_check_newsletter[]');

        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {

            $_data['email'] = $this->input->post('email_newsletter');
            if (checkUserIfLogin()) {
                $_data['login'] = 1;
            } else {
                $_data['login'] = 0;
            }
            $_data["created_date"] = date("Y-m-d h:i:s");
            $this->db->insert('newsletter', $_data);

            $return['result'] = 1;
            $return['message'] = 'Thank you for subscribing!';

            $this->load->model('send_emails');
            $subscribe = 1;
            /*$this->send_emails->sendNewsLetter($_data);*/

        }
        die(json_encode($return));
    }

    /*********************************************CREDITS SUBMIT***************************************************/

    public function check_amount()
    {
        $amount = $this->input->post('amount');

        $threshold_amount = 20;

        $amount_default_currency = changeCurrency($amount, "", $this->config->item('default_currency'), $this->session->userdata('currency'));
        if ($threshold_amount > $amount_default_currency) {
            $threshold_amount_current = changeCurrency($threshold_amount, true, $this->session->userdata('currency'), $this->config->item('default_currency'));
            //	$this->form_validation->set_message('validate_user',lang('account_is_blocked'));
            $this->form_validation->set_message('check_amount', "Srry! you can't add amount less than " . $threshold_amount_current . ".");

            return false;
        } else {
            return true;
        }


    }


    public function submit_credits()
    {

        $allPostedVals = $this->input->post(NULL, TRUE);

        if (checkUserIfLogin()) {

            $this->form_validation->set_rules('amount', lang('amount'), 'trim|required|xss_clean|callback_check_amount[]');
            $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email|xss_clean');
            $this->form_validation->set_rules('phone', lang('phone'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('company', lang('company'), 'trim|xss_clean');
            $this->form_validation->set_rules('postal_code', lang('postal_code'), 'trim|xss_clean');
            $this->form_validation->set_rules('country', lang('country'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('state', lang('state'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('street_one', lang('street'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $return['result'] = 0;
                $return['errors'] = array();
                $return['message'] = 'Error!';
                $return['captcha'] = $this->fct->createNewCaptcha();
                $find = array('<p>', '</p>');
                $replace = array('', '');
                foreach ($allPostedVals as $key => $val) {
                    if (form_error($key) != '') {
                        $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                    }
                }
            } // if success
            else {

                $user = $this->ecommerce_model->getUserInfo();
                $data['id_user'] = $user['id_user'];
                $data['first_name'] = $this->input->post('first_name');
                $data['last_name'] = $this->input->post('last_name');
                $data['email'] = $this->input->post('email');


                /*				$phone = $this->input->post("phone");
			$update['telephone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];*/

                $data['telephone'] = $this->input->post('phone');

                /*			if($this->fax()){

			$fax = $this->input->post("fax");
			$update['fax'] = $fax[0].'-'.$fax[1].'-'.$fax[2];}else{
			$update['fax'] = 0;	}*/

                $data['fax'] = $this->input->post('fax');

                $data['postal_code'] = $this->input->post('postal_code');
                $data['id_countries'] = $this->input->post('country');
                $data['state'] = $this->input->post('state');
                $data['city'] = $this->input->post('city');
                $data['street_one'] = $this->input->post('street_one');
                $data['street_two'] = $this->input->post('street_two');
                $data['amount'] = $this->input->post('amount');

                $amount_default_currency = changeCurrency($data['amount'], "", $this->config->item('default_currency'), $this->session->userdata('currency'));

                $data['amount_default_currency'] = $amount_default_currency;
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['status'] = '0';
                $data['default_currency'] = $this->config->item('default_currency');
                $data['currency'] = $this->session->userdata('currency');

                //print '<pre>';print_r($order);
                $this->db->insert('users_credits', $data);
                $id_users_credits = $this->db->insert_id();

                $paymentData = $this->fct->getonerecord('users_credits', array('id_users_credits' => $id_users_credits));
                $paymentData['billing'] = $data;
                $paymentData['billing']['country'] = $this->fct->getonerecord('countries', array('id_countries' => $data['id_countries']));
                $paymentData['id_orders'] = $id_users_credits;

                $paymentData['user']['email'] = $data['email'];

                $return['message'] = "<span class='vSuccess'>" . lang("payment_redirection_ni") . "</span>" . "" . $this->fct->load_ni_payment_form1($paymentData);
                $return['result'] = 1;
                /*$return['message'] = lang('system_is_redirecting_to_page');*/

                /*$return['redirect_link'] = route_to('user/credits/success');*/
            }
            die(json_encode($return));


        } else {
            $this->session->set_flashdata('error_message', lang('login_to_continue'));
            $this->session->set_userdata('redirect_link', route_to('cart/credits'));
            redirect(route_to('user/login'));
        }
    }


    public function delete_file_m()
    {
        $user = $this->ecommerce_model->getUserInfo();
        $table = $this->input->post('section');
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/" . $table . "/" . $image)) {
            unlink("./uploads/" . $table . "/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = '" . $table . "')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/" . $table . "/" . $value . "/" . $image)) {
                    unlink("./uploads/" . $table . "/" . $value . "/" . $image);
                }
            }
        }

        $_data[$field] = "";
        $cond['id_user'] = $user['id_user'];
        $cond["id_" . $table] = $id;
        $this->db->where($cond);
        $this->db->update($table, $_data);
        echo 'Done!';
    }

    public function delete_file()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/supplier_info/" . $image)) {
            unlink("./uploads/supplier_info/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'user')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/user/" . $value . "/" . $image)) {
                    unlink("./uploads/user/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_user", $id);
        $this->db->update("user", $_data);
        echo 'Done!';
    }

    public function deleteDropFile()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/user_drop_file/" . $image)) {
            unlink("./uploads/user_drop_file/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'user_drop_file')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/user_drop_file/" . $value . "/" . $image)) {
                    unlink("./uploads/user_drop_file/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_user_drop_file", $id);
        $this->db->update("user_drop_file", $_data);
        echo 'Done!';
    }

    public function getPrivatePolicy($title_url = "")
    {
        $data['result'] = $this->fct->getonerecord('dynamic_pages', array('title_url' => $title_url));
        if (!empty($data['result'])) {
            $html = $this->load->view('load/private_policy', $data, true);

        } else {
            $html = "<span class='empty'>No data found</span>";
        }
        echo $html;
        exit;
    }
    public function country_dropdown(){

        $title=$this->input->post("title");
        $sql= "SELECT * FROM countries where title like '$title%' and status=0";
        $result=$this->db->query($sql);
        $found=$result->result();
        $count=count($found);
        if($count>0){
            foreach($found as $val){
                echo "<li class='dropdown-title' id-countries='$val->id_countries' data-country='$val->title' >$val->title</li>";
            }
        }else{
            echo "<li>No Country Found<li>";
        }

    }


}


function payment_error($id)
{

    if (checkUserIfLogin()) {
        if (!isset($_REQUEST['responseParameter'])) redirect(route_to("user"));
        $responseParameter = $_REQUEST['responseParameter'];
        $merchantId = $_REQUEST['merchantId'];
        $enc = MCRYPT_RIJNDAEL_128;
        $key = $this->config->item("payment_encryption_key");
        $mode = MCRYPT_MODE_CBC;
        $iv = $this->config->item("payment_iv");
        $rand = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
        $merchantId = $this->config->item("payment_merchant_id");
        $crypt = base64_decode($responseParameter);
        $padtext = mcrypt_decrypt($enc, base64_decode($key), $crypt, $mode, $iv);
        $pad = ord($padtext{strlen($padtext) - 1});
        if ($pad > strlen($padtext)) return false;
        if (strspn($padtext, $padtext{strlen($padtext) - 1}, strlen($padtext) - $pad) != $pad) {
            $text = "Error";
        }
        $text = substr($padtext, 0, -1 * $pad);
        $arr = explode("|", $text);

        //// After Success

        $credits_update['updated_date'] = date("Y-m-d H:i:s");
        $credits_update['status'] = "refused";
        $this->db->where("id_users_credits", $id);
        $this->db->update("users_credits", $credits_update);

        $this->session->set_flashdata('error_message', $arr[9]);
        redirect(route_to('user/credits'));

    } else {
        //$this->session->set_userdata("redirect_link",route_to("user/ni_payment_success"));
        $this->session->set_flashdata('error_message', lang('access_denied'));
        redirect(route_to('user/login'));
    }
}


