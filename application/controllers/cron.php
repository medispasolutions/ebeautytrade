<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends BaseController
{

    protected $validIps = [
        '127.0.0.1', //localhost
        '138.197.186.146', //spamiles.com server
        '54.37.68.206', //spamiles.bidev.pl server
        '89.76.108.36', //PW - home
        '79.188.184.233', //BI - office
    ];

    public function __construct()
    {
        parent::__construct();

        //look for a valid IP
        if (!in_array($_SERVER['REMOTE_ADDR'], $this->validIps)) {
            die('BAD IP: '.$_SERVER['REMOTE_ADDR']);
        }
    }

    public function daily()
    {
        $counts = [
            'expired_discounts' => 0,
        ];

        //end discounts that have expiration date
        $product = $this->serviceLocator->product();
        $collection = $product->find(
            [
                'discount_expiration is not null' => null,
                'discount_expiration <=' => date("Y-m-d"),
                'discount_active' => 1,
            ],
        10000
        );
        /** @var \spamiles\products\Product $product */
        foreach ($collection as $product) {
            $product->expireDiscount();
            $counts['expired_discounts'] ++;
        }

        echo "DONE".PHP_EOL;
        foreach ($counts as $name => $count) {
            echo $name.": ".$count.PHP_EOL;
        }
    }
}