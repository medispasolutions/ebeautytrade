<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class is loaded via application/core/MY_Controller.php file
 *
 * We can get requested controller either by get_class($this) or $this->router->fetch_class()
 * We can get request method by $this->router->fetch_method()
 */

abstract class BaseShopController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $contentTemplate Name of template to be rendered as content
     * @param [] $data Template data
     */
    protected function renderStandardTemplate($contentTemplate, $data)
    {
        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', $contentTemplate, $data);

        $footerData = $data;
        if ($this->input->get('after_registration')) {
            $footerData['afterRegistration'] = true;
        }
        $this->template->write_view('footer', 'blocks/footer', $footerData);

        $this->template->render();
    }
}