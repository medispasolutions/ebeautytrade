<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question_and_answer extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}
	public function index()
	{	
	
		
		////TRACING//////	
		$insert_log['section']='events';
		$this->fct->insertNewLog($insert_log);
		
		$url=route_to('events').'?pagination=on';
		$data['new_url']=$url;
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>34));
		$data['page_title'] = $data['seo']['title'.getFieldLanguage()];
		$cond=array();
		
		
////////////////////////////////CONTIDIONS//////////////		
		$cond['status']=4;
		
		$show_items=20;
	    $count= $this->pages_fct->getQuestionsAndAnswers($cond);
		
        $data['show_items']          = $show_items;
        $show_items                  = ($show_items == 'All') ? $count_news : $show_items;
        $cc                          = $count;
        $config['base_url']          = $url;
        $config['total_rows']        = $cc;
        $config['num_links']         = '8';
        $config['use_page_numbers']  = TRUE;
        $config['page_query_string'] = TRUE;
        
        $config['per_page'] = $show_items;
        
        $this->pagination->initialize($config);
        if ($this->input->get('per_page')) {
            if ($this->input->get('per_page')!="")
                $page = $this->input->get('per_page');
            else
                $page = 0;
        } else
            $page = 0;
        $data['count']    = $cc;
        $data['per_page'] = $page; 
        $num_page_prev    = 1;
        if ($page > 0) {
            $num_page = intval($page / $show_items);
        } else {
            $num_page = 0;
        }
   
		$data['answers'] = $this->pages_fct->getQuestionsAndAnswers($cond, $show_items, $page,"created_date","desc");
		
		$data['show_items']=$show_items;	
		$data['count']=$count;
		$data['offset']=$page;

				$breadcrumbs = "";
                $breadcrumbs .= '<ul>';
                $breadcrumbs .= '<li><a href=' . site_url() . '>Home</a></li>';
                $breadcrumbs .= '<li class="divider"><i class="fa fa-arrow-right"></i></li>';
       			$breadcrumbs .= '<li> Question & Answer </li>';
                $breadcrumbs .= '</ul>';
				
		$data['breadcrumbs']=$breadcrumbs;		

		$this->template->write_view('header','blocks/header',$data);
		$this->template->write_view('quarter_left_sideBar','blocks/quarter_left_sideBar',$data);
		$this->template->write_view('content', 'blocks/breadcrumbs', $data);
	
		$this->template->write_view('content','content/question_and_answer',$data);
		$this->template->write_view('footer','blocks/footer',$data);
		$this->template->render();
	}
	
function validate_captcha()
{
	$row_count = 1;
	if(!isset($_POST['no_captcha'])) {
		$expiration = time()-7200; // Two hour limit
		$this->db->query("DELETE FROM captcha WHERE captcha_time < ".$expiration);
		// Then see if a captcha exists:
		$sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
		$binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
		$query = $this->db->query($sql, $binds);
		$row = $query->row();
		$row_count = $row->count;
		if($row_count == 0) {
			$this->form_validation->set_message('validate_captcha',lang('notcorrectcharacters'));
			return false;
		}
		else {
			return true;
		}
	}
}
public function question()
{
		//print '<pre>';
		//print_r($_POST);exit;
		$allPostedVals = $this->input->post(NULL, TRUE);
		$this->form_validation->set_rules('name',lang('name'),'trim|required');
		$this->form_validation->set_rules('phone',lang('phone'),'trim|required');
		$this->form_validation->set_rules('email',lang('email'),'trim|required|valid_email');
		$this->form_validation->set_rules('question',lang('question'),'trim|required');
		//$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');
		if($this->form_validation->run() == FALSE) {
			$return['result'] = 0;
			$return['errors'] = array();
			$return['message'] = 'Error!';
			//$return['captcha'] = $this->fct->createNewCaptcha();
			$find =array('<p>','</p>');
			$replace =array('','');
			foreach($allPostedVals as $key => $val) {
				if(form_error($key) != '') {
					$return['errors'][$key] = str_replace($find,$replace,form_error($key));
				}
			}
		}
		else {
		
			
			$_data['name'] = $this->input->post('name');
			$_data['email'] = $this->input->post('email');
			$_data['phone'] = $this->input->post('phone');
			$_data['status'] = 2;
			$_data['question'] = $this->input->post('question');
			$_data['created_date'] = date('Y-m-d h:i:s');
			/*$_data['lang'] = $this->lang->lang();*/
			$this->db->insert('question_and_answer',$_data);
			$new_id=$this->db->insert_id();
			$_data['id_question_and_answer']=$new_id;
			/*$_data['lang'] = $this->lang->lang();*/
	
			// send emails

			
     		$this->load->model('send_emails');
			$this->send_emails->sendQuestionToAdmin($_data);
			$this->send_emails->sendQuestionToClient($_data);
			$return['result'] = 1;
			$return['message'] = 'Your question has been successfully submitted and received.';
		}
		echo json_encode($return);
	}	
	

}