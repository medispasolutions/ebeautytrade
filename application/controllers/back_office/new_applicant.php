<?
class New_applicant extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "supplier_application";
        $this->load->model("new_applicant_m");
        $this->lang->load("admin");
    }
    
    public function index()
    {
        $cond = array();
        $order = "sort_order";
        $this->session->unset_userdata("admin_redirect_url");
        if ($this->acl->has_permission('categories_task', 'index')) {
            $url = site_url("back_office/new_applicant/index?pagination=on");
            $data["title"] = "List categories task";
            $data["content"] = "back_office/new_applicant/list";

            $this->session->unset_userdata("back_link");

            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "25";
            }
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
            $data["show_items"] = $show_items;
            
            // pagination  start :
            if (isset($_GET["keyword"])) {
                $keyword = $_GET["keyword"];
                $url .= "&keyword=" . $keyword;
                if (!empty($keyword))
                    $cond["keyword"] = $keyword;
            }

            $show_items = ($show_items == "All") ? $count_news : $show_items;
            $this->load->library("pagination");

            $count_news = $this->new_applicant_m->getRecords($cond);
            $config["base_url"] = $url;
            $config["total_rows"] = $count_news;
            $config["per_page"] = $show_items;
            $config["use_page_numbers"] = TRUE;
            $config["page_query_string"] = TRUE;
            //print "<pre>";print_r($config);exit;
            $this->pagination->initialize($config);
            if (isset($_GET["per_page"])) {
                if ($_GET["per_page"] != "") $page = $_GET["per_page"];
                else $page = 0;
            } else $page = 0;
            $data["info"] = $this->new_applicant_m->getRecords($cond, $show_items, $page);
            // end pagination .
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/home/dashboard"));
        }
    }
    
    public function add()
    {
        if ($this->acl->has_permission('categories_task', 'add')) {
            $data["title"] = "Add categories task";
            $data["content"] = "back_office/new_applicant/add";
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/home/dashboard"));
        }
    }

    public function view($id, $obj = "")
    {
        if ($this->acl->has_permission('categories_task', 'index')) {
            $data["title"] = "View categories task";
            $data["content"] = "back_office/new_applicant/add";
            $cond = array("id_supplier_application" => $id);
            $data["id"] = $id;
            
        $this->db->where('deleted', 0);
        $this->db->where($cond);
        //$this->db->join('application_brand ', 'application_brand.random_number = supplier_application.random_nmber', 'left');
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            $data["info"] = $query->row_array();
        }

            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function edit($id, $obj = "")
    {
        if ($this->acl->has_permission('categories_task', 'edit')) {
            $data["title"] = "Edit categories task";
            $data["content"] = "back_office/new_applicant/add";
            $cond = array("id_supplier_application" => $id);
            $data["id"] = $id;
        $this->db->where('deleted', 0);
        $this->db->where($cond);
        //$this->db->join('application_brand ', 'application_brand.random_number = supplier_application.random_number', 'left');
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            $data["info"] = $query->row_array();
        }
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/home/dashboard"));
        }
    }
    
    /*
    public function delete($id)
    {
        if ($this->acl->has_permission('supplier_application', 'delete')) {
            $_data = array("deleted" => 1,
                "deleted_date" => date("Y-m-d h:i:s"));
            $this->db->where("id_supplier_application", $id);
            $this->db->update($this->table, $_data);
            $this->session->set_userdata("success_message", "Information was deleted successfully");
            redirect(site_url("back_office/new_applicant/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_all()
    {
        if ($this->acl->has_permission('supplier_application', 'delete_all')) {
            $cehcklist = $this->input->post("cehcklist");
            $check_option = $this->input->post("check_option");
            if ($check_option == "delete_all") {
                if (count($cehcklist) > 0) {
                    for ($i = 0; $i < count($cehcklist); $i++) {
                        if ($cehcklist[$i] != "") {
                            $_data = array("deleted" => 1,
                                "deleted_date" => date("Y-m-d h:i:s"));
                            $this->db->where("id_supplier_application", $cehcklist[$i]);
                            $this->db->update($this->table, $_data);
                        }
                    }
                }
                $this->session->set_userdata("success_message", "Informations were deleted successfully");
            }
            redirect(site_url("back_office/new_applicant/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }
*/
    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i + 1);
            $this->db->where("id_supplier_application", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit categories task";
        $this->form_validation->set_rules("title" . $lang, "TITLE", "trim|required");
        $this->form_validation->set_rules("meta_title" . $lang, "PAGE TITLE", "trim|max_length[65]");
        $this->form_validation->set_rules("title_url" . $lang, "TITLE URL", "trim");
        $this->form_validation->set_rules("meta_description" . $lang, "META DESCRIPTION", "trim|max_length[160]");
        $this->form_validation->set_rules("meta_keywords" . $lang, "META KEYWORDS", "trim|max_length[160]");
        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post("id") != "")
                $this->edit($this->input->post("id"));
            else
                $this->add();
        } else {
            $_data["id_user"] = $this->session->userdata("uid");
            $_data["title" . $lang] = $this->input->post("title" . $lang);
            $_data["meta_title" . $lang] = $this->input->post("meta_title" . $lang);
            if ($this->input->post("title_url" . $lang) == "")
                $title_url = $this->input->post("title" . $lang);
            else
                $title_url = $this->input->post("title_url" . $lang);
            if ($this->input->post("lang") == "ar") {
                $this->load->model("title_url_ar");
                $_data["title_url" . $lang] = $this->title_url_ar->cleanURL($this->table, $title_url, $this->input->post("id"), "title_url" . $lang);
            } else {
                $_data["title_url" . $lang] = $this->fct->cleanURL($this->table, url_title($title_url), $this->input->post("id"));
            }
            $_data["meta_description" . $lang] = $this->input->post("meta_description" . $lang);
            $_data["meta_keywords" . $lang] = $this->input->post("meta_keywords" . $lang);

            if ($this->input->post("id") != "") {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_supplier_application", $this->input->post("id"));
                $this->db->update($this->table, $_data);
                $new_id = $this->input->post("id");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert($this->table, $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }
            if ($this->session->userdata("admin_redirect_url")) {
                redirect($this->session->userdata("admin_redirect_url"));
            } else {
                redirect(site_url("back_office/new_applicant/" . $this->session->userdata("back_link")));
            }
        }
    }
    /*
    public function delete_file()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/new_applicant/" . $image)) {
            unlink("./uploads/new_applicant/" . $image);
        }
        $q = " SELECT thumb,thumb_val FROM `content_type_attr` WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'categories task') AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if ($res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/new_applicant/" . $value . "/" . $image)) {
                    unlink("./uploads/new_applicant/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_supplier_application", $id);
        $this->db->update("supplier_application", $_data);
        echo 'Done!';
    }
    */
}