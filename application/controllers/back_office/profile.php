<?php

class Profile extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Edit profile (currently only for suppliers - twig)
     */
    public function edit()
    {
        $this->view->error = $this->session->flashdata('errors');
        $this->view->success = $this->session->flashdata('success_message');
        $this->view->context = \views\suppliersBackoffice\Contexts::PROFILE_MANAGE;
        $this->view->title = 'Edit Profile';

        $user = $this->serviceLocator->user($this->acl->getCurrentUserId());
        $this->view->info = $user->toArray();

        $countries = $this->fct->getAll('countries', 'title');
        $this->view->countries = [];
        $this->view->delivery_shipping_from_countries = [];
        foreach ($countries as $country) {
            $this->view->countries[] = array(
                'TEXT' => $country['title'],
                'VALUE' => $country['id_countries'],
                'IS_SELECTED' => $this->view->info['id_countries'] == $country['id_countries'] ? true : false
            );

            $this->view->delivery_shipping_from_countries[] = array(
                'TEXT' => $country['title'],
                'VALUE' => $country['title'],
                'IS_SELECTED' => $this->view->info['delivery_shipping_from'] == $country['title'] ? true : false
            );
        }
        
        $this->view->usertype = $this->acl->getCurrentUserType();

        $this->load->twigView("profileAddEditPage/index", $this->view);
    }

    /**
     * Edit profile - for main backoffice users (admins)
     */
    public function index()
    {
        if (!$this->acl->isCurrentUserAAdmin()) {
            redirect(site_url('back_office/profile/edit'));
        }

        $data["title"] = "Edit Profile";
        $data["content"] = "back_office/profile";
        $data["id"] = $this->session->userdata('user_id');
        $data["info"] = $this->fct->getonerow('user', array('id_user' => $this->session->userdata('user_id')));

        $this->load->view('back_office/template', $data);
    }

    /**
     * Save profile (currently only for suppliers - twig)
     */
    public function save()
    {
        //we don't save the given data, only when we have no or wrong email
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|callback_validateemailexistance[]');

        if ($this->input->post('oldpassword') || $this->input->post('newpassword')) {
            $this->form_validation->set_rules('oldpassword', 'Password', 'callback_validatecurrentpassword[]');
            $this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('repeatnewpassword', 'Confirm Password', 'trim|required|min_length[5]|max_length[15]|matches[newpassword]');
        }

        if ($this->form_validation->run() == FALSE) {
            $obj = _get_validation_object();
            $this->session->set_flashdata('errors', implode('<br>', $obj->_error_array));
            redirect(site_url('back_office/profile/edit'));
        } else {
            //clean rule for old password, as we check formvalidation later in the code again
            $this->form_validation->set_rules('oldpassword', 'Password', 'trim');
        }

        //let's validate the rest of the data required to have a full profile
        $this->form_validation->set_rules('trading_name', 'Trade Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('address_1', 'Business Address', 'trim|required|xss_clean');
        $this->form_validation->set_rules('id_countries', 'Country', 'trim|required|xss_clean');
        $this->form_validation->set_rules('company_email', 'Corporate Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('company_phone', 'Company Phone', 'trim|required');
        $this->form_validation->set_rules('bank_name', 'Company Bank Name', 'trim');
        $this->form_validation->set_rules('bank_account', 'Company Bank Account Details', 'trim');
        $this->form_validation->set_rules('note', 'note', 'trim');
        
        $this->form_validation->set_rules('company_bank_ban', 'BAN', 'trim');
        $this->form_validation->set_rules('company_bank_version_name', 'Bank Name', 'trim');
        $this->form_validation->set_rules('company_bank_branch_name', 'Branch', 'trim');
        
        $this->form_validation->set_rules('company_location_map', 'Share Location Map', 'trim');

        $sentData = $this->input->post(null, true);
        //save
        $_data = array(
            'company_name' => $sentData['trading_name'],
            'trading_name' => $sentData['trading_name'],
            'company_vat_tax_no' => $sentData['company_vat_tax_no'],
            'address_1' => trimEmptyLines($sentData['address_1']),
            'city' => $sentData['city'],
            'id_countries' => $sentData['id_countries'],
            'company_email' => $sentData['company_email'],
            'company_phone' => $sentData['company_phone'],
            'company_bank_name' => $sentData['bank_name'],
            'company_bank_account_no' => $sentData['bank_account'],
            'note' => $sentData['note'],
            
            'company_bank_branch_name' => $sentData['company_bank_branch_name'],
            'company_bank_version_name' => $sentData['company_bank_version_name'],
            'company_bank_ban' => $sentData['company_bank_ban'],
            
            'lng' => $sentData['lng'],
            'lat' => $sentData['lat'],
            'company_location_map' => $sentData['company_location_map'],

            'delivery_shipping_from' => $sentData['delivery_shipping_from'],
            'delivery_time' => $sentData['delivery_time'],
            'delivery_time_period' => $sentData['delivery_time_period'],
            'delivery_time_period_type' => $sentData['delivery_time_period_type'],
            'delivery_fee' => $sentData['delivery_fee'],
            'delivery_fee_price' => $sentData['delivery_fee_price'],
            'credit_terms' => $sentData['credit_terms'],
            'payment_methods' => $sentData['payment_methods'] ? $sentData['payment_methods'] : [],

            'first_name' => $sentData['first_name'],
            'last_name' => $sentData['last_name'],
            'position' => $sentData['position'],
            'phone' => $sentData['phone'],
            'email' => $sentData['email'],
            'mobile' => $sentData['mobile'],
            'direct_phone' => $sentData['direct_phone'],
            'phone_extension' => $sentData['phone_extension'],
            'whatsapp' => $sentData['whatsapp'],

            'logo' => 'logo', //$_FILES field
            'trading_licence' => 'trading_licence', //$_FILES field
            'photo' => 'photo', //$_FILES field

            'updated_date' => date("Y-m-d h:i:s"),
        );

        if ($sentData['newpassword']) {
            $this->session->set_flashdata('success_message', 'Password has been changed');
            $_data['password'] = $sentData['newpassword'];
        }

        $user = $this->serviceLocator->user($this->acl->getCurrentUserId());
        $user->save($_data);

        //check whether we have logo
        $other_errors = [];
        $userData = $user->toArray();
        if (!$userData['logo']) {
            $other_errors[] = "Logo is required";
        }

        //if we have additional errors, we only want to show them to the user and not to block saving
        if ($this->form_validation->run() == FALSE || !empty($other_errors)) {
            $obj = _get_validation_object();
            $this->session->set_flashdata('errors', implode('<br>', array_merge($obj->_error_array, $other_errors)));
        }

        redirect(site_url('back_office/profile/edit'));
    }

    /**
     * Save profile - for main backoffice users (admins)
     */
    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
//$this->form_validation->set_rules('name', 'Full Name', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('username', 'User Name', 'trim|min_length[5]|max_length[15]|callback_validateusernameexistance[]');
        if ($this->input->post('password') != '') {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[5]|max_length[15]|matches[password]');
        }
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|callback_validateemailexistance[]');
        $this->form_validation->set_rules('country', 'Country', 'trim');
        $this->form_validation->set_rules('state', 'State', 'trim');
        $this->form_validation->set_rules('city', 'City', 'trim');
        $this->form_validation->set_rules('address', 'address', 'trim');
        $this->form_validation->set_rules('phone', 'phone', 'trim');
        $this->form_validation->set_rules('roles', 'Level', 'trim');
        
        $this->form_validation->set_rules('company_bank_ban', 'company_bank_ban', 'trim');
        $this->form_validation->set_rules('company_bank_version_name', 'company_bank_version_name', 'trim');
        $this->form_validation->set_rules('company_bank_branch_name', 'company_bank_branch_name', 'trim');
        
        if ($this->form_validation->run() == FALSE) {
            $data["title"] = "Edit Profile";
            $data["content"] = "back_office/profile";
            $data["id"] = $this->session->userdata('user_id');
            $data["info"] = $this->fct->getonerow('user', array('id_user' => $this->session->userdata('user_id')));
            $this->load->view('back_office/template', $data);
        } else {
            $cond = array('username' => $this->input->post('username'), 'password' => $this->input->post('password'));
            $check_user = $this->fct->getonerow('user', $cond);
            if (count($check_user) > 0 && $this->input->post('id') == '') {
                $data["title"] = "Edit Profile";
                $data["content"] = "back_office/profile";
                if ($this->input->post('id') != '') {
                    $cond = array('id_user' => $this->input->post('id'));
                    $data["id"] = $this->input->post('id');
                    $data["info"] = $this->fct->getonerecord('user', $cond);
                }
                $this->session->set_userdata('error_message', 'This username and password are already exist .');
                $this->load->view('back_office/template', $data);
            } else {
                $_data = array(
                    'name' => $this->input->post('name'),
                    'username' => $this->input->post('username'),
                    //'password'=>$this->input->post('password'),
                    'email' => $this->input->post('email'),
                    'phone' => $this->input->post('phone'),
                    'address' => $this->input->post('address'));
                $_data["updated_date"] = date("Y-m-d h:i:s");
                if ($this->input->post('password') != '') {
                    $_data['password'] = md5($this->input->post('password'));
                }
                $_data['company_name'] = $this->input->post("company_name");
                $_data['id_countries'] = $this->input->post("country");
                $_data['state'] = $this->input->post("state");
                $_data['city'] = $this->input->post("city");
                $this->db->where('id_user ', $this->input->post('id'));
                $this->db->update('user', $_data);
                $this->session->set_userdata('success_message', 'Information was updated successfully');
                redirect(site_url('back_office/profile'));
            }
        }
    }


    public function validateemailexistance()
    {
        $email = $this->input->post('email');
        $cond = array('email' => $email);
        $cond['id_user !='] = $this->acl->getCurrentUserId();

        $check = $this->fct->getonerow('user', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('validateemailexistance', 'Email exists, please try another email.');
            return false;
        }
    }

    public function validatecurrentpassword()
    {
        $password = $this->input->post('oldpassword');
        $user = $this->serviceLocator->user($this->acl->getCurrentUserId());
        if ($user->verifyPassword($password)) {
            return true;
        } else {
            $this->form_validation->set_message('validatecurrentpassword', 'Incorrect current password');
            return false;
        }
    }

    public function validateusernameexistance()
    {
        $username = $this->input->post('username');
        $cond = array('username' => $username);
        if ($this->input->post('id') != '') {
            $cond['id_user !='] = $this->input->post('id');
        }
        $check = $this->fct->getonerow('user', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('validateusernameexistance', 'Username exists, please try another username.');
            return false;
        }
    }
}
