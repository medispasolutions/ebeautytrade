<?
class Attribute_options extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="attribute_options";
$this->load->model("attribute_options_m");
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('attribute_options','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List attribute options";
$data["content"]="back_office/attribute_options/list";
//
$id_attribute = 0;
$cond = array();
if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute'])) {
	$id_attribute = $_GET['id_attribute'];
	$cond['id_attributes'] = $id_attribute;
	$data['attribute'] = $this->fct->getonerow('attributes',$cond);
}
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$data['info'] = $this->attribute_options_m->list_All($order,$cond);
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function add(){
if ($this->acl->has_permission('attribute_options','add')){	
$data["title"]="Add attribute options";
$data["content"]="back_office/attribute_options/add";
if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute'])) {
	$cond['id_attributes'] = $_GET['id_attribute'];
	$data['attribute'] = $this->fct->getonerow('attributes',$cond);
}
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ''){
if ($this->acl->has_permission('attribute_options','index')){	
$data["title"]="View attribute options";
$data["content"]="back_office/attribute_options/add";
$cond=array("id_attribute_options"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute'])) {
	$cond1['id_attributes'] = $_GET['id_attribute'];
	$data['attribute'] = $this->fct->getonerow('attributes',$cond1);
}
if($this->config->item("language_module")) {
  $data["language"]=$this->fct->getonerecord("languages",array("index"=>$data["info"]["lang"]));
  $data["lang"] = $data["info"]["lang"];
  if($obj == 'translate') {
	  $data["id_parent_translate"] = $this->translation_lib->getDefaultRowLangID("attribute_options",$id,TRUE);
  }
}
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ''){
if ($this->acl->has_permission('attribute_options','edit')){	
$data["title"]="Edit attribute options";
$data["content"]="back_office/attribute_options/add";
$cond=array("id_attribute_options"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute'])) {
	$cond1['id_attributes'] = $_GET['id_attribute'];
	$data['attribute'] = $this->fct->getonerow('attributes',$cond1);
}
if($this->config->item("language_module")) {
  $data["language"]=$this->fct->getonerecord("languages",array("index"=>$data["info"]["lang"]));
  $data["lang"] = $data["info"]["lang"];
  if($obj == 'translate') {
	  $data["id_parent_translate"] = $this->translation_lib->getDefaultRowLangID("attribute_options",$id,TRUE);
  }
}
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('attribute_options','delete')){

/*$check_in_orders = $this->fct->getAll_cond('line_items','sort_order',array('id_attribute_options'=>$id));
if(empty($check_in_orders)) {*/
$check_in_products = $this->fct->getAll_cond('product_options','sort_order',array('id_attribute_options'=>$id));
if(empty($check_in_products)) {
$this->db->where("id_attribute_options",$id);
$this->db->delete($this->table);
if($this->config->item("language_module")) {
	$this->db->where("id_parent_translate",$id);
	$this->db->delete($this->table);
}
	$this->session->set_userdata('success_message','Option is removed successfully.');
} 
else {
	$this->session->set_userdata('error_message','This option is in use, please remove it from selected products.');
}
//}
/*else {
	$this->session->set_userdata('error_message','You are permanently not allowed to delete this option, it was already selected for some orders.');
}*/
if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute']))
redirect(site_url("back_office/attribute_options")."?id_attribute=".$_GET['id_attribute']);	
else
redirect(site_url("back_office/attribute_options/".$this->session->userdata("back_link")));	
}
else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('attribute_options','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_attribute_options",$cehcklist[$i]);
$this->db->update($this->table,$_data);
if($this->config->item("language_module")) {
	$this->db->where("id_parent_translate",$cehcklist[$i]);
	$this->db->update($this->table,$_data);
}	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute']))
redirect(site_url("back_office/attribute_options")."?id_attribute=".$_GET['id_attribute']);	
else
redirect(site_url("back_office/attribute_options/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_attribute_options",$sort[$i]);
$this->db->update($this->table,$_data);	
if($this->config->item("language_module")) {
	$this->db->where("id_parent_translate",$sort[$i]);
	$this->db->update($this->table,$_data);
}	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit attribute options";
$this->form_validation->set_rules("title", "TITLE", "trim|required");
$this->form_validation->set_rules("option", "OPTION", "trim|option");
//$this->form_validation->set_rules("title_en", "TITLE (arabic)", "trim|required");
$this->form_validation->set_rules("meta_title", "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url", "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description", "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords", "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("attribute", "attribute", "trim|required");
if ($this->form_validation->run() == FALSE){	
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{
if($this->config->item("language_module")) {
$_data["lang"]=$this->input->post("lang");
}
$_data["id_user"]=$this->session->userdata("uid");
if($this->config->item("language_module") && isset($_POST["id_parent_translate"])) {
	if($this->input->post("id") != "" && $this->input->post("id") == $this->input->post("id_parent_translate")) {
		$_data["id_parent_translate"] = 0;
	}
	else {
		$_data["id_parent_translate"]=$this->input->post("id_parent_translate");
	}
}
$_data["title"]=$this->input->post("title");
$_data["option"]=$this->input->post("option");
//$_data["title_en"]=$this->input->post("title_en");
$_data["meta_title"]=$this->input->post("meta_title");
if($this->input->post("title_url") == "")
$title_url = $this->input->post("title");
else
$title_url = $this->input->post("title_url");
$_data["title_url"]=$this->fct->cleanURL("attribute_options",url_title($title_url),$this->input->post("id"));
$_data["meta_description"]=$this->input->post("meta_description");
$_data["meta_keywords"]=$this->input->post("meta_keywords");	
$_data["id_attributes"]=$this->input->post("attribute"); 

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_attribute_options",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	if($this->config->item("language_module") && isset($_POST["id_parent_translate"])) {
		$parent_record = $this->fct->getonerow($this->table,array("id_".$this->table=>$_POST["id_parent_translate"]));
		$_data["sort_order"]= $parent_record["sort_order"];
	}
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if(isset($_GET['id_attribute']) && !empty($_GET['id_attribute']))
	redirect(site_url("back_office/attribute_options")."?id_attribute=".$_GET['id_attribute']);
	else {
		if($this->config->item("language_module") && isset($_POST["id_parent_translate"])) {
			redirect(site_url("back_office/translation/section/attribute_options/".$this->input->post("id_parent_translate")));
		}
		else {
			redirect(site_url("back_office/attribute_options/".$this->session->userdata("back_link")));
		}
	}
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/attribute_options/".$image)){
unlink("./uploads/attribute_options/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'attribute options')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if(isset($res["thumb"]) && $res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/attribute_options/".$value."/".$image)){
unlink("./uploads/attribute_options/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_attribute_options",$id);
$this->db->update("attribute_options",$_data);
echo 'Done!';		
}

}