<?
class Brand_certified_training extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="brand_certified_training";
$this->load->model("brand_certified_training_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List brand certified training";
$data["content"]="back_office/brand_certified_training/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$count_news = $this->brand_certified_training_m->getAll($this->table,$order);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = site_url("back_office/brand_certified_training/index/".$order);
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['uri_segment'] = 5;
$this->pagination->initialize($config);
$data['info'] = $this->brand_certified_training_m->list_paginate($order,$config['per_page'],$this->uri->segment(5));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function getProducts(){
$keyword = $_POST['term'];
$cond['title']=$keyword;
$cond['brand']=$_POST['id_brand'];
$brands_ids=array();
$cond2=array();
$user = $this->ecommerce_model->getUserInfo();
if($this->input->post('id_supplier')!=""){
$cond2['id_user']=$this->input->post('id_supplier');	}
$brands=$this->fct->getAll_cond('brands','title asc',$cond2);

foreach($brands as $brand){
	 array_push($brands_ids, $brand['id_brands']);
	}

	
$cond['brands_ids']=$brands_ids;


$show_items = $this->ecommerce_model->getProducts($cond);  
$results = $this->ecommerce_model->getProducts($cond, $show_items, 0, 'products.sort_order', 'asc');  


//$results=$this->fct->getimagegallery($keyword);

$cc = count($results);
$i=0;
echo '[';
if(!empty($results)){
foreach($results as $result){
$i++;
if($i == $cc) $coma = '';
else $coma = ',';
//$res = preg_replace('/\.[^.]+$/','',$result['title']);
//$res =str_replace(".jpg","",$result['image']);
echo '{"label" : "'.str_replace("'","",$result['title']).'", "value": "'.$result['id_products'].'_'.$result['title'].'"}'.$coma;
//echo '"'.str_replace("'","",$result['title']).'"'.$coma;
}}
echo ']';

}

public function add(){
if ($this->acl->has_permission('products','add')){	
$data["title"]="Add brand certified training";
$data["content"]="back_office/brand_certified_training/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('products','index')){	
$data["title"]="View brand certified training";
$data["content"]="back_office/brand_certified_training/add";
$cond=array("id_brand_certified_training"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

/*public function edit($id,$obj = ""){
if ($this->acl->has_permission('brand_certified_training','edit')){	
$data["title"]="Edit brand certified training";
$data["content"]="back_office/brand_certified_training/add";
$cond=array("id_brand_certified_training"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}*/

public function edit($id,$obj = ""){
if ($this->acl->has_permission('products','edit')){	
$data["title"]="Edit Brand Certified Training";
$data["content"]="back_office/brand_certified_training/add";

if(checkIfSupplier_admin()){
	$cond2['id_user']=$this->session->userdata("uid");
	}

$cond=array("id_brand_certified_training"=>$id);
$data["info"]=$this->fct->getonerecord($this->table,$cond);
/*echo "<pre>";
print_r($data["info"]);exit;*/

/*if(empty($data["info"])){
	$_data["id_products"]=$id;
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();
	$cond2=array("id_brand_certified_training"=>$new_id);
	$data["info"]=$this->fct->getonerecord($this->table,$cond2);
	}*/
	$data["id"]=$data["info"]["id_brand_certified_training"];

    $this->load->view("back_office/template",$data);}else{
	redirect(site_url("home/dashboard"));	}

}


public function delete($id){
if ($this->acl->has_permission('brand_certified_training','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_brand_certified_training",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/brand_certified_training/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('brand_certified_training','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_brand_certified_training",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/brand_certified_training/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_brand_certified_training",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}
public function checkUploadImage(){

$size=$_FILES['image']['size'];
$size_arr=getMaxSize('image');
$max_size=$size_arr['size'];

if($max_size>$size) {
		return true;
	}
	else {
		$this->form_validation->set_message('checkUploadImage','Image size exceeds the maximum upload size('.$size_arr['size_mega'].' Mb).');
		return false;
	}
}
public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit brand certified training";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("image", "Image", "trim|callback_checkUploadImage[]");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");

$this->form_validation->set_rules("description", "description"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("id_products", "id_products"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
			if(isset($_POST['set_as_news'])) {
$_data["set_as_news"]=$this->input->post("set_as_news");
	}
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	

$_data["title".$lang]=$this->input->post("title".$lang);
$_data["duration".$lang]=$this->input->post("duration".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
if(!empty($_FILES["image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_brand_certified_training"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("brand_certified_training","image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/brand_certified_training/'.$old_image)){
unlink("./uploads/brand_certified_training/".$old_image);
$sumb_val1=explode(",","300x228");
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/brand_certified_training/".$value."/".$old_image)){
unlink("./uploads/brand_certified_training/".$value."/".$old_image);	 }							
} 
 } }
$image1= $this->fct->uploadImage("image","brand_certified_training");
$this->fct->createthumb1($image1,"brand_certified_training","300x228");$_data["image"]=$image1;	
}
$_data["description"]=$this->input->post("description");
$_data["id_products"]=$this->input->post("id_products");
$_data["set_as_training"]=1;

$training = array();
if(isset($_POST['training']) && !empty($_POST['training'])) {
		$training = $_POST['training'];
}



	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_brand_certified_training",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	
	

$this->brand_certified_training_m->insert_product_training($new_id,$training);
	//////////////////INFORM ADMIN//////	
	$product=$this->fct->getonerecord('products',array('id_products'=>$_data["id_products"]));
	$this->custom_fct->informProductModificationsToAdmin($product);

   	    redirect(site_url("back_office/brand_certified_training/edit/".$new_id));
	
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/brand_certified_training/".$image)){
unlink("./uploads/brand_certified_training/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'brand certified training')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if(isset($res["thumb"]) && $res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/brand_certified_training/".$value."/".$image)){
unlink("./uploads/brand_certified_training/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_brand_certified_training",$id);
$this->db->update("brand_certified_training",$_data);
echo 'Done!';
}

}