<?
class Opening_a_new_spa extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="opening_a_new_spa";
$this->load->model("opening_a_new_spa_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('opening_a_new_spa','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List opening a new spa";
$data["content"]="back_office/opening_a_new_spa/list";

$url = site_url('back_office/opening_a_new_spa?pagination=on');
$cond=array();
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));

if(isset($_GET['keyword'])) {
	$keyword = $_GET['keyword'];
	$url .= '&keyword='.$keyword;
	if(!empty($keyword))
	$cond['keyword'] = $keyword;
}

if(isset($_GET['status'])) {
	$status = $_GET['status'];
	$url .= '&status='.$status;
	if(!empty($status)){
		
	$cond['status'] = $status;}
}
if(isset($_GET['name'])) {
	$name = $_GET['name'];
	$url .= '&name='.$name;
	if(!empty($name))
	$cond['name'] = $name;
}

if(isset($_GET['project'])) {
	$project = $_GET['project'];
	$url .= '&project='.$project;
	if(!empty($project))
	$cond['id_projects'] = $project;
}
if(isset($_GET['service'])) {
	$service = $_GET['service'];
	$url .= '&service='.$service;
	if(!empty($service))
	$cond['id_services'] = $service;
}

if(isset($_GET['email'])) {
	$email = $_GET['email'];
	$url .= '&email='.$email;
	if(!empty($email))
	$cond['email'] = $email;
}

$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
// pagination  start :
$count_news =$this->pages_fct->getSpa($cond);

$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->pages_fct->getSpa($cond,$config['per_page'],$page);
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('opening_a_new_spa','add')){	
$data["title"]="Add opening a new spa";
$data["content"]="back_office/opening_a_new_spa/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('opening_a_new_spa','index')){	
$data["title"]="View opening a new spa";
$data["content"]="back_office/opening_a_new_spa/add";
$cond=array("id_opening_a_new_spa"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('opening_a_new_spa','edit')){	
$data["title"]="Edit opening a new spa";
$data["content"]="back_office/opening_a_new_spa/add";
$cond=array("id_opening_a_new_spa"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);

$_data['read'] = 1;
$cond['id_opening_a_new_spa']=$id;
$this->db->where($cond);
$this->db->update('opening_a_new_spa',$_data);

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('opening_a_new_spa','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_opening_a_new_spa",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/opening_a_new_spa/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('opening_a_new_spa','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_opening_a_new_spa",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/opening_a_new_spa/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_opening_a_new_spa",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit opening a new spa";

$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("name", "name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("first_name", "first name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("last_name", "last name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("position", "position"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("email", "email"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("mobile", "mobile"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("phone", "phone"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("trade_name", "trade name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("website", "website"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("address", "address"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("city", "city"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("country", "country"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("company_email", "company email"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("company_phone", "company phone"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("salutation", "salutation"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	

$_data["first_name"]=$this->input->post("first_name");
$_data["last_name"]=$this->input->post("last_name");
$_data['name'] = $_data['first_name'].' '.$_data['last_name'];
$_data["position"]=$this->input->post("position");
$_data["email"]=$this->input->post("email");
$_data["mobile"]=$this->input->post("mobile");
$_data["phone"]=$this->input->post("phone");
$_data["trade_name"]=$this->input->post("trade_name");
$_data["website"]=$this->input->post("website");
$_data["address"]=$this->input->post("address");
$_data["city"]=$this->input->post("city");
$_data["id_countries"]=$this->input->post("country"); 
$_data["company_email"]=$this->input->post("company_email");
$_data["company_phone"]=$this->input->post("company_phone");
$_data["salutation"]=$this->input->post("salutation");

$_data['comments'] = $this->input->post('comments');

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_opening_a_new_spa",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	
	$projects = array();
	if(isset($_POST['projects']) && !empty($_POST['projects'])) {
		$projects = $_POST['projects'];
	}
	$this->fct->insert_spa_projects($new_id,$projects);
	
	$services = array();
	if(isset($_POST['services']) && !empty($_POST['services'])) {
		$services = $_POST['services'];
	}
	$this->fct->insert_spa_services($new_id,$services);
	
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/opening_a_new_spa/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/opening_a_new_spa/".$image)){
unlink("./uploads/opening_a_new_spa/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'opening a new spa')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/opening_a_new_spa/".$value."/".$image)){
unlink("./uploads/opening_a_new_spa/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_opening_a_new_spa",$id);
$this->db->update("opening_a_new_spa",$_data);
echo 'Done!';
}

}