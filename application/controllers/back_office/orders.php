<?php

use \spamiles\orders\OrderInterface;

class Orders extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "orders";
        $this->load->model("orders_m");
        $this->lang->load('statics');
    }

    //==================================================================================================================
    // helpers

    /**
     * Checks whether the order with supplied id belongs to the current user
     * @param $order_id
     * @param string $permission_type
     * @return bool
     */
    protected function checkOwner($order_id, $permission_type = 'edit')
    {
        //check permission
        if (!$this->acl->has_permission('orders', $permission_type)) {
            return false;
        }
        
        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }

        //check owner
        $cond = array("id_orders" => $order_id);
        $data = $this->fct->getonerecord($this->table, $cond);
        if ($data['id_supplier'] != $parentId) {
            return false;
        }

        return true;
    }

    protected function export($orders)
    {
        $this->export_fct->salesHistoryExportToExcel($orders);
    }

    /**
     * This functions prepares filters
     * @return array Current filters settings
     */
    protected function getFilters()
    {
        $searchData = [
            'id' => '',
            'buyer_name' => '',
            'buyer_city' => '',
            'buyer_country' => '',
            'status' => '',
            'payment_status' => '',
            'date_from' => '',
            'date_to' => '',
        ];

        $currentSessionData = $this->session->userdata('orders-filters');
        if (!$currentSessionData) {
            $currentSessionData = [];
        }
        $currentPostedFilters = $this->input->post('Search');
        if (!$currentPostedFilters) {
            $currentPostedFilters = [];
        }
        if ($this->input->get('resetFilters') == 1) {
            $currentPostedFilters = $searchData;
        }

        foreach ($searchData as $key => $_) {
            if (isset($currentPostedFilters[$key])) {
                $searchData[$key] = $currentPostedFilters[$key];
            } else {
                $searchData[$key] = $currentSessionData[$key];
            }
        }

        $this->session->set_userdata('orders-filters', $searchData);

        //prepare possible filters
        $possibleFilters = [];
        //countries
        $countries = $this->fct->getAll('countries', 'title');
        foreach ($countries as $country) {
            $possibleFilters['buyer_country'][] = array(
                'TEXT' => $country['title'],
                'VALUE' => $country['id_countries'],
                'IS_SELECTED' => $searchData['buyer_country'] == $country['id_countries'] ? true : false
            );
        }
        //statuses
        $possibleFilters['status'] = [];
        $tmp = [
            OrderInterface::STATUS_UNREAD,
            OrderInterface::STATUS_PENDING,
            OrderInterface::STATUS_PREPARED,
            OrderInterface::STATUS_COMPLETED,
            OrderInterface::STATUS_CANCELED,
        ];
        foreach ($tmp as $i) {
            $possibleFilters['status'][] = array(
                'TEXT' => $this->serviceLocator->dictionary()->orders($i),
                'VALUE' => $i,
                'IS_SELECTED' => $searchData['status'] == $i ? true : false
            );
        }
        //payment statuses
        //statuses
        $possibleFilters['payment_status'] = [];
        $tmp = [
            OrderInterface::PAYMENT_STATUS_UNPAID,
            OrderInterface::PAYMENT_STATUS_PAID,
        ];
        foreach ($tmp as $i) {
            $possibleFilters['payment_status'][] = array(
                'TEXT' => $this->serviceLocator->dictionary()->orders($i),
                'VALUE' => $i,
                'IS_SELECTED' => $searchData['payment_status'] == $i ? true : false
            );
        }

        return [
            'current' => $searchData,
            'possible' => $possibleFilters
        ];
    }

    /**
     * This function searches for relevant orders
     * @param $filters
     * @return array[int|array]
     */
    protected function search($filters)
    {
        $cond = array();

        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        
        $cond['id_supplier'] = $parentId;

        if ($filters['id']) {
            $cond['orders.id_orders'] = $filters['id'];
        }

        if ($filters['buyer_name']) {
            $cond['user.name'] = $filters['buyer_name'];
        }

        if ($filters['buyer_country']) {
            $cond['user.id_countries'] = $filters['buyer_country'];
        }

        if ($filters['buyer_city']) {
            $cond['user.city'] = $filters['buyer_city'];
        }

        if ($filters['status']) {
            $cond['orders.status'] = $filters['status'];
        }

        if ($filters['payment_status']) {
            $cond['orders.payment_status'] = $filters['payment_status'];
        }

        if ($filters['date_from']) {
            //rework date
            $dt = DateTime::createFromFormat("d/m/Y", $filters['date_from']);
            $cond['orders.created_date >='] = $dt->format("Y-m-d")." 00:00:00";
        }

        if ($filters['date_to']) {
            //rework date
            $dt = DateTime::createFromFormat("d/m/Y", $filters['date_to']);
            $cond['orders.created_date <='] = $dt->format("Y-m-d")." 00:00:00";
        }

        $noOfElements = $this->ecommerce_model->getOrders($cond);
        $elements = $this->ecommerce_model->getOrders($cond, 1000, 0);

        return [$noOfElements, $elements];
    }

    //==================================================================================================================
    // views

    public function listing()
    {
        if (!$this->acl->has_permission('orders', 'index')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::ORDERS_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Orders',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
            ],
        ];

        $filters = $this->getFilters();

        $filtersForSearch = $filters['current'];

        list($noOfElements, $orders) = $this->search($filtersForSearch);
        if ($this->input->post('filtersRequestType') == "export") {
            return $this->export($orders);
        }

        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/products/live_on_site');

        $orders_array = array_splice(
            $orders,
            ($this->paginator->page-1)*$this->paginator->per_page,
            $this->paginator->per_page
        );

        $finalData = [];
        foreach ($orders_array as $order) {
            $finalData[] = [
                'rowId' => $order['id_orders'],
                'columns' => [
                    $order['id_orders'],
                    $order['created_date'],
                    $order['trading_name'],
                    $order['amount_currency'].' '.$order['currency'],
                    $this->serviceLocator->dictionary()->orders($order['payment_method']),
                    $order['status'],
                    $order['payment_status'],
                ],
            ];
        }


        $data['filters'] = $filters;
        $data['info'] = $finalData;


        $this->load->twigView("ordersListingPage/index", $data);
    }

    public function revise($order_id, $formData = null, $errors = null)
    {
        if (!$this->checkOwner($order_id)) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::ORDER_VIEW,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Edit order',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
                'MODE' => 'edit',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        //get details
        $order = $this->serviceLocator->order($order_id);
        $orderData = $order->toArray();
        $orderData['payment_method_readable'] = $this->serviceLocator->dictionary()->orders($orderData['payment_method']);
        $orderDetails = $this->fct->getOrderDetails($order_id);
        $buyerData = $this->serviceLocator->user($orderData['id_user'])->toArray();

        //order and buyer
        $data["order"] = $orderData;
        $data["buyer"] = $buyerData;

        //ordered products
        $data["details"] = [];
        foreach ($orderDetails as $i => $details) {
            $data["details"][] = [
                'rowId' => $details['product']['id_products'],
                'columns' => [
                    $i+1,
                    $details['sku'],
                    $details['barcode'],
                    $details['product']['title'],
                    $details['price_currency'],
                    $details['quantity'],
                    $details['total_price_currency'],
                ],
            ];
        }


        //dictionaries for selects
        $data['dictionary'] = [];
        foreach ($order->listStatuses() as $i) {
            $data['dictionary']['status'][] = [
                'TEXT' => $this->serviceLocator->dictionary()->orders($i),
                'VALUE' => $i,
                'IS_SELECTED' => $orderData['status'] == $i ? true : false
            ];
        }
        foreach ($order->listPaymentMethods() as $i) {
            $data['dictionary']['payment_method'][] = [
                'TEXT' => $this->serviceLocator->dictionary()->orders($i),
                'VALUE' => $i,
                'IS_SELECTED' => $orderData['payment_method'] == $i ? true : false
            ];
        }
        foreach ($order->listPaymentStatuses() as $i) {
            $data['dictionary']['payment_status'][] = [
                'TEXT' => $this->serviceLocator->dictionary()->orders($i),
                'VALUE' => $i,
                'IS_SELECTED' => $orderData['payment_status'] == $i ? true : false
            ];
        }


        if ($formData) {
            $data['info'] = array_merge($orderData, $formData);
            if (!empty($errors)) {
                $data['globalErrors'] = $errors;
            }
        }

        $this->load->twigView("orderViewPage/index", $data);
    }

    /**
     * This is the endpoint to save an order as a supplier
     */
    public function save()
    {
        $order_id = $this->input->post("id");

        //check owner
        if ($order_id) {
            if (!$this->checkOwner($order_id)) {
                redirect(site_url("back_office"));
            }
            $order = $this->serviceLocator->order($order_id);
            $currentData = $order->toArray();

        //there is no add funcionality yet
        } else {
            $order = $this->serviceLocator->order();
            $currentData = [];
        }

        $this->form_validation->set_rules("discount", "discount", "trim");
        $this->form_validation->set_rules("shipping_charge_currency", "shipping charge", "trim");
        $this->form_validation->set_rules("comments", "comments", "trim|xss_clean");
        $this->form_validation->set_rules("payment_method", "payment method", "trim");
        $this->form_validation->set_rules("status", "status", "trim|required");
        $this->form_validation->set_rules("payment_status", "payment status", "trim|required");

        if ($this->form_validation->run() == FALSE) {
            if ($order_id) {
                return $this->revise($product_id, $this->input->post(null), $this->form_validation->_error_array);
            } else {
                //no add yet
                throw new Exception("No add");
            }
        }

        $sentData = $this->input->post(null, true);

        $_data["discount"] = $sentData['discount'];
        $_data["shipping_charge_currency"] = $sentData['shipping_charge_currency'];
        $_data["comments"] = $sentData['comments'];
        $_data["payment_method"] = $sentData['payment_method'];
        $_data["status"] = $sentData['status'];
        $_data["payment_status"] = $sentData['payment_status'];

        if ($order_id) {
            $_data["updated_date"] = date("Y-m-d h:i:s");
            $order->save($_data);

        //no add yet
        } else {
            throw new Exception("No add");
        }

        redirect('back_office/orders/revise/'.$order_id);
    }

    //==================================================================================================================
    // main backoffice

    public function index($order = "")
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/listing');
        }

        $supplier = checkIfSupplier_admin();
        $user = $this->ecommerce_model->getUserInfo();
        $this->session->unset_userdata("admin_redirect_url");
        if ($this->acl->has_permission('orders', 'index')) {
            if ($order == "")
                $order = "sort_order";
            $data["title"] = "List orders";
            $data["content"] = "back_office/orders/list";
//
            $this->session->unset_userdata("back_link");
//
            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "25";
            }
            $data["show_items"] = $show_items;
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));


            $url = site_url('back_office/orders') . '?pagination=on';
            $cond = array();
            if (isset($_GET['order_id'])) {
                $order_id = $_GET['order_id'];
                $url .= '&order_id=' . $order_id;
                if (!empty($order_id))
                    $cond['orders.id_orders'] = $order_id;
            }


            if (isset($_GET['delivered'])) {
                $delivered = $_GET['delivered'];
                $url .= '&delivered=' . $delivered;
                if (!empty($delivered))
                    $cond['orders.delivered'] = $delivered;
            }

            if (isset($_GET['printed'])) {
                $printed = $_GET['printed'];
                $url .= '&printed=' . $printed;
                if (!empty($printed))
                    $cond['orders.printed'] = $printed;
            }

            if (isset($_GET['customer_first_name'])) {
                $customer_first_name = $_GET['customer_first_name'];
                $url .= '&customer_first_name=' . $customer_first_name;
                if (!empty($customer_first_name))
                    $cond['user.first_name'] = $customer_first_name;
            }
            if (isset($_GET['customer_surname'])) {
                $customer_surname = $_GET['customer_surname'];
                $url .= '&customer_surname=' . $customer_surname;
                if (!empty($customer_surname))
                    $cond['user.surname'] = $customer_surname;
            }

            if (isset($_GET['status'])) {
                $status = $_GET['status'];
                $url .= '&status=' . $status;
                if (!empty($status))
                    $cond['orders.status'] = $status;
            }


            if ($this->input->get("account_manager") != "") {
                $cond['user.id_account_manager'] = $this->input->get("account_manager");
                $url .= '&account_manager=' . $this->input->get("account_manager");
            }

            if ($this->input->get("id_users_areas") != "") {
                $cond['user.id_users_areas'] = $this->input->get("id_users_areas");
                $url .= '&id_users_areas=' . $this->input->get("id_users_areas");
            }

            if ($this->input->get("city") != "") {
                $cond['UPPER(user.city)'] = strtoupper($this->input->get("city"));
                $url .= '&city=' . $this->input->get("city");
            }


            if (isset($_GET['payment_method'])) {
                $payment_method = $_GET['payment_method'];
                $url .= '&payment_method=' . $payment_method;
                if (!empty($payment_method))
                    $cond['orders.payment_method'] = $payment_method;
            }

            if (isset($_GET['id_address'])) {
                $id_address = $_GET['id_address'];
                $url .= '&id_address=' . $id_address;
                $url .= '&address=' . $_GET['address'];
                if (!empty($id_address))
                    $cond['orders.billing_id'] = $id_address;
            }

            if (isset($_GET['from_date'])) {
                $from_date = $_GET['from_date'];
                $f_date = $this->fct->date_in_formate($from_date);

                $url .= '&from_date=' . $from_date;
                if (!empty($from_date))
                    $cond['orders.created_date >='] = $f_date;
            }

            if (isset($_GET['to_date'])) {
                $to_date = $_GET['to_date'];
                $t_date = $this->fct->date_in_formate($to_date);
                $url .= '&to_date=' . $to_date;
                if (!empty($to_date))
                    $cond['orders.created_date <='] = $t_date;
            }
            $data['supplier'] = $supplier;
            if ($supplier) {
                $parentUser = $this->acl->getCurrentUserId();
                $condition = array("id_user" => $parentUser);
                $companyData = $this->fct->getonerecord('user', $condition);
                $parentId = $companyData['id_parent'];
        
                //set conditions
                if($parentId){
                    $parentId = $parentId;
                } else {
                    $parentId = $this->acl->getCurrentUserId();
                }
                $cond['orders.id_supplier'] = $parentId;
            }
            $cond['group_products'] = 0;
            $cond['orders.deleted'] = 0;
// pagination  start :
            $count_news = $this->ecommerce_model->getOrders($cond);
            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $this->load->library('pagination');
            $config['base_url'] = $url;
            $config['num_links'] = '8';
            $config['total_rows'] = $count_news;
            $config['per_page'] = $show_items;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
            $this->pagination->initialize($config);
            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            } else $page = 0;
            $data['info'] = $this->ecommerce_model->getOrders($cond, $config['per_page'], $page);

            if (isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {
                $this->export_fct->salesHistoryExportToExcel($data['info']);
            }
// end pagination .
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function getAddresses()
    {
        $keyword = $_POST['term'];
        $cond['keyword'] = $keyword;

        $brands_ids = array();
        $user = $this->ecommerce_model->getUserInfo();
        if (checkIfSupplier_admin()) {
            $cond2['id_user'] = $user['id_user'];
        }


        $results = $this->ecommerce_model->getAddressBook("", "num", $cond);

        /*$results = $this->ecommerce_model->getUserAddresses($cond,$count,0);*/


//$results=$this->fct->getimagegallery($keyword);

        $cc = count($results);
        $i = 0;
        echo '[';
        if (!empty($results)) {
            foreach ($results as $result) {
                $i++;
                if ($i == $cc) $coma = '';
                else $coma = ',';
                $address = $result['street_one'] . ', ' . $result['city'] . ', ' . $result['state'] . ', ' . $result['title'];
//$res = preg_replace('/\.[^.]+$/','',$result['title']);
//$res =str_replace(".jpg","",$result['image']);
                echo '{"label" : "' . str_replace("'", "", $address) . '", "value": "' . $result['id_address_book'] . '_' . $address . '"}' . $coma;
//echo '"'.str_replace("'","",$result['title']).'"'.$coma;
            }
        }
        echo ']';
    }

    public function add()
    {
        //no add for suppliet
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/listing');
        }

        if ($this->acl->has_permission('orders', 'add')) {
            $data["title"] = "Add orders";
            $data["content"] = "back_office/orders/add";
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function view($id, $rand)
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/revise/'.$id);
        }

        if ($this->acl->has_permission('orders', 'index')) {
            $data["title"] = "View orders";
            $data["content"] = "back_office/orders/add";
            $cond = array("id_orders" => $id);
            $updata['readed'] = 1;
            $this->db->where($cond);
            $this->db->update('orders', $updata);
            $data["id"] = $id;
            $cond3['rand'] = $rand;
            $data["info"] = $this->ecommerce_model->getOrder($id, $cond3);
            $data["userData"] = $data["info"]['user'];
            $data['order_details'] = $data["info"];
            /*print '<pre>';
            print_r($data['order_details']);
            exit;*/
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function invoice($id, $rand = "")
    {
        //no invoice for supplier
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/listing');
        }

        $orderData = $this->ecommerce_model->getOrder($id);
        $data['print'] = 1;
        $data['orderData'] = $orderData;
        if ($this->input->get('order_return')) {
            $data['page_title'] = ' MR-ORDER #' . $id;
        }
        if (!empty($orderData)) {

            $template = $this->load->view('emails/order', $data, true);;
            $data['template'] = $template;
            $this->load->view('back_office/orders/invoice', $data);
        } else {
            $this->session->set_flashdata('error_message', lang('please_login'));
            redirect(route_to('user/login'));
        }
    }

    public function updatestatus()
    {
        //not for supplier
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/listing');
        }

        //print '<pre>';print_r($_POST);exit;
        $id_orders = $this->input->post('id_orders');
        $rand = $this->input->post('rand');
        $status = $this->input->post('status');

        $currentData = $this->fct->getonerecord($this->table, ['id_orders' => $id_orders]);

        $update['payment_status'] = $this->input->post('payment_status');
        $update["comments"] = $this->input->post("comments");
        $update["printed"] = $this->input->post("printed");
        $update["delivered"] = $this->input->post("delivered");


        $update['delivery_time'] = $this->input->post("delivery_time");
        $update['delivery_fee'] = $this->input->post("delivery_fee");
        $update['min_order'] = $this->input->post("min_order");
        $update['credit'] = $this->input->post("credit");

        $customer_shipping_charge = $this->input->post('customer_shipping_charge');
        $update['updated_date'] = date('Y-m-d h:i:s');
        if (
            ($status == 'paid' && $currentData['status'] != 'paid') //legacy
            || (
                $update['payment_status'] == 'paid'
                && $currentData['payment_status'] != 'paid'
            )
        ) {
            $update['payment_date'] = date('Y-m-d h:i:s');
        } else {
            $update['payment_date'] = null;
        }

        ////////////////////UPDATE MILES//////////////////////
        $order = $this->fct->getonerecord('orders', array('id_orders' => $id_orders));
        ////////////////////PAYMENT METHOD//////////////////////
        if ($this->input->post('payment_method') != "") {
            $update['payment_method'] = $this->input->post('payment_method');
            $update['bank'] = $this->input->post('bank');
            $update['number'] = $this->input->post('number');
        }


        if (($order['status'] != 'paid' && $status == "paid") || ($order['status'] != 'completed' && $status == "completed")) {

            $total_miles = $order['miles'];
            $total_redeem_miles = $order['redeem_miles'];
            $user = $this->ecommerce_model->getUserInfo($order['id_user']);

            $new_user_miles = $user['miles'] + $total_miles;
            $update_miles['miles'] = $new_user_miles;
            $this->db->where('id_user', $user['id_user']);
            $this->db->update('user', $update_miles);


        }

        ////////////////////UPDATE MILES//////////////////////
        if ($order['status'] != 'paid' && $this->input->post('status') != "" && $order['status'] != 'completed') {
            $update['status'] = $status;
        }

        $update['shipping_charge_currency'] = $this->input->post('shipping');
        $update['shipping'] = changeCurrency($update['shipping_charge_currency'], "", $this->config->item('default_currency'), $order['currency']);

        $amount = $order['amount_currency'] - $order['shipping_charge_currency'] + $update['shipping_charge_currency'];

        if ($amount > $this->input->post('discount')) {
            $update['discount'] = $this->input->post('discount');
            $amount = $amount + $order['discount'] - $update['discount'];
        }

        /*$update['customer_shipping_charge'] = $customer_shipping_charge;*/

        $update['amount'] = changeCurrency($amount, "", $this->config->item('default_currency'), $order['currency']);
        $update['amount_currency'] = $amount;


        $this->db->where('id_orders', $id_orders);
        $this->db->update('orders', $update);
        $order = $this->fct->getonerecord('orders', array('id_orders' => $id_orders));
        /*if($order['shipping']==0){
        $order=$this->fct->getOrder($order['id_orders'],$order['rand']);
        $total_weight=$order['weight'];
        $charge_per_kg=round($customer_shipping_charge/$total_weight);
        $charge_per_kg=5.3;
        $line_items=$order['line_items'];
        foreach($line_items as $val){
        $qty=$val['quantity'];
        $weight=$val['weight'];
        $charge=$charge_per_kg*$weight;
        $update2['customer_shipping_charge'] = $charge;
        $this->db->where('id_line_items',$val['id_line_items']);
        $this->db->update('line_items',$update2);
            }

        }*/

        if (isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {

            $orderData = $this->ecommerce_model->getOrder($id_orders, array('rand' => $rand));
            $this->load->model("send_emails");
            $this->send_emails->sendOrderToUser($orderData, 1);
        }

        $this->session->set_userdata("success_message", "Order status is updated.");
        redirect(site_url("back_office/orders/view/" . $id_orders . '/' . $rand));
    }

    /*public function setpaid()
    {
        $id_orders = $this->input->post('id_orders');
        $update['updated_date'] = date('Y-m-d h:i:s');
        $update['payment_date'] = date('Y-m-d h:i:s');
        $update['status'] = 'paid';
        $this->db->where('id_orders',$id_orders);
        $this->db->update('orders',$update);
        $this->session->set_userdata("success_message","Order is set as Paid.");
        redirect(site_url("back_office/orders/view/".$id_orders));
    }*/

    public function edit($id)
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/revise/'.$id);
        }

        if ($this->acl->has_permission('orders', 'edit')) {
            $data["title"] = "Edit orders";
            $data["content"] = "back_office/orders/add";
            $cond = array("id_orders" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete($id)
    {
        if ($this->acl->has_permission('orders', 'delete')) {
            $_data = array(
                "deleted" => 1,
                "deleted_date" => date("Y-m-d h:i:s")
            );
            $cnd = array('id_orders' => $id);
            $this->db->where($cnd);
            $this->db->update($this->table, $_data);
            $cnd = array('id_orders' => $id);
            $this->db->where($cnd);
            $this->db->update('line_items', $_data);
            $this->session->set_userdata("success_message", "Information was deleted successfully");
            redirect(site_url("back_office/orders/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_all()
    {
        //not for supplier
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/listing');
        }

        if ($this->acl->has_permission('orders', 'delete_all')) {
            $cehcklist = $this->input->post("cehcklist");
            $check_option = $this->input->post("check_option");
            if ($check_option == "delete_all") {
                if (count($cehcklist) > 0) {
                    for ($i = 0; $i < count($cehcklist); $i++) {
                        if ($cehcklist[$i] != "") {
                            $_data = array("deleted" => 1,
                                "deleted_date" => date("Y-m-d h:i:s"));
                            $this->db->where("id_orders", $cehcklist[$i]);
                            $this->db->update($this->table, $_data);
                        }
                    }
                }
                $this->session->set_userdata("success_message", "Informations were deleted successfully");
            }
            redirect(site_url("back_office/orders/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i);
            $this->db->where("id_orders", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function submit()
    {
        //not for supplier
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/listing');
        }

        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit orders";
        $this->form_validation->set_rules("title", "TITLE", "trim|required");
        $this->form_validation->set_rules("meta_title", "PAGE TITLE", "trim|max_length[65]");
        $this->form_validation->set_rules("title_url", "TITLE URL", "trim");
        $this->form_validation->set_rules("meta_description", "META DESCRIPTION", "trim|max_length[160]");
        $this->form_validation->set_rules("meta_keywords", "META KEYWORDS", "trim|max_length[160]");
        $this->form_validation->set_rules("user", "user", "trim");
        $this->form_validation->set_rules("amount", "amount", "trim");
        $this->form_validation->set_rules("comments", "comments", "trim");
        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post("id") != "")
                $this->edit($this->input->post("id"));
            else
                $this->add();
        } else {
            $_data["title"] = $this->input->post("title");
            $_data["meta_title"] = $this->input->post("meta_title");
            if ($this->input->post("title_url") == "")
                $title_url = $this->input->post("title");
            else
                $title_url = $this->input->post("title_url");
            $_data["title_url"] = $this->fct->cleanURL("orders", url_title($title_url), $this->input->post("id"));
            $_data["meta_description"] = $this->input->post("meta_description");
            $_data["meta_keywords"] = $this->input->post("meta_keywords");
            $_data["user"] = $this->input->post("user");
            $_data["amount"] = $this->input->post("amount");
            $_data["comments"] = $this->input->post("comments");

            if ($this->input->post("id") != "") {


                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_orders", $this->input->post("id"));
                $this->db->update($this->table, $_data);
                $new_id = $this->input->post("id");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert($this->table, $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }
            redirect(site_url("back_office/orders/" . $this->session->userdata("back_link")));
        }

    }

    public function delete_image($field, $image, $id)
    {
        //not for supplier
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/orders/listing');
        }

        if (file_exists("./uploads/orders/" . $image)) {
            unlink("./uploads/orders/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'orders')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/orders/" . $value . "/" . $image)) {
                    unlink("./uploads/orders/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_orders", $id);
        $this->db->update("orders", $_data);
        redirect(site_url("back_office/orders"));
    }

}