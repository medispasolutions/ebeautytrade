<?
class Categories_sub extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="categories_sub";
$this->load->model("categories_sub_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('categories_sub','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List categories sub";
$data["content"]="back_office/categories_sub/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$cond = array();
$like = array();
$url = site_url("back_office/categories_sub").'?pagination=on';
if($this->input->get("sub_category_name") != "") {
	$sub_category_name = $this->input->get("sub_category_name");
	$url .= '&sub_category_name='.$sub_category_name;
	$like['UPPER(categories_sub.title)'] = strtoupper($sub_category_name);
}
if($this->input->get("category") != "") {
	$category = $this->input->get("category");
	$url .= '&category='.$category;
	$cond['categories_sub_categories.id_categories'] = $category;
}
$count_news = $this->categories_sub_m->getSubcategories_list($cond,$like);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['uri_segment'] = 5;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
$this->pagination->initialize($config);
$data['info'] = $this->categories_sub_m->getSubcategories_list($cond,$like,$config['per_page'],$this->uri->segment(5));

$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$this->session->set_userdata('admin_redirect_url',$url);
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('categories_sub','add')){	
$data["title"]="Add categories sub";
$data["content"]="back_office/categories_sub/add";
if(isset($_POST['categories']))
$data['selected_options'] = $_POST['categories'];
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('categories_sub','index')){	
$data["title"]="View categories sub";
$data["content"]="back_office/categories_sub/add";
$cond=array("id_categories_sub"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if(isset($_POST['categories']))
$data['selected_options'] = $_POST['categories'];
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('categories_sub','edit')){	
$data["title"]="Edit categories sub";
$data["content"]="back_office/categories_sub/add";
$cond=array("id_categories_sub"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if(isset($_POST['categories']))
$data['selected_options'] = $_POST['categories'];
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('categories_sub','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_categories_sub",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
if($this->session->userdata("admin_redirect_url") != "")
redirect($this->session->userdata("admin_redirect_url"));
else
redirect(site_url("back_office/categories_sub/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('categories_sub','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_categories_sub",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
if($this->session->userdata("admin_redirect_url") != "")
redirect($this->session->userdata("admin_redirect_url"));
else
redirect(site_url("back_office/categories_sub/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_categories_sub",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit categories sub";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("banner_image", "banner image"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
/*if(!empty($_FILES["banner_image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_categories_sub"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("categories_sub","banner_image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/categories_sub/'.$old_image)){
unlink("./uploads/categories_sub/".$old_image);
$sumb_val1=explode(",","312x232");
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/categories_sub/".$value."/".$old_image)){
unlink("./uploads/categories_sub/".$value."/".$old_image);	 }							
} 
 } }
$image1= $this->fct->uploadImage("banner_image","categories_sub");
$this->fct->createthumb1($image1,"categories_sub","819x229");$_data["banner_image"]=$image1;	
}*/

if(!empty($_FILES["image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_categories_sub"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("categories_sub","image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/categories_sub/'.$old_image)){
unlink("./uploads/categories_sub/".$old_image);
$sumb_val1=explode(",","312x232");
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/categories_sub/".$value."/".$old_image)){
unlink("./uploads/categories_sub/".$value."/".$old_image);	 }							
} 
 } }
$image1= $this->fct->uploadImage("image","categories_sub");
$this->fct->createthumb1($image1,"categories_sub","183x168");$_data["image"]=$image1;	
}


	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_categories_sub",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if(isset($_POST['categories']) && !empty($_POST['categories'])) {
		//$this->fct->insert_product_categories($new_id,$_POST['categories']);
		$this->fct->insert_many_to_many('id_categories_sub',$new_id,$_POST['categories'],'categories','categories_sub_categories');
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/categories_sub/".$this->session->userdata("back_link")));
	}
	
}
	
}
public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/categories_sub/".$image)){
unlink("./uploads/categories_sub/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'categories sub')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if(isset($res["thumb"]) && $res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/categories_sub/".$value."/".$image)){
unlink("./uploads/categories_sub/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_categories_sub",$id);
$this->db->update("categories_sub",$_data);
echo 'Done!';
}

public function load_sub()
{
	$ids = $this->input->post("ids");
	$sub_categories = $this->fct->getSubCategoriesByMultipleCategories($ids);
	$content = '';
	if(!empty($sub_categories)) {
		foreach($sub_categories as $sub) {
			$content .= '<option value="'.$sub['id_categories_sub'].'">'.$sub['title'].'</option>';
		}
	}
	echo $content;
}

public function load_sub_filter()
{
	$id = $this->input->post("id");
	//echo $id;exit;
	$category = $this->fct->getonerow("categories",array("title_url".getUrlFieldLanguage($this->lang->lang())=>$id));
	//print_r($category);exit;
	$content = '<option value="">- All -</option>';
	if(!empty($category)) {
	$ids = array($category['id_categories']);
	$sub_categories = $this->fct->getSubCategoriesByMultipleCategories($ids);
	if(!empty($sub_categories)) {
		foreach($sub_categories as $sub) {
			$content .= '<option value="'.$sub['title_url'.getUrlFieldLanguage($this->lang->lang())].'">'.$sub['title'.getFieldLanguage($this->lang->lang())].'</option>';
			//$content .= '<option value="'.$sub['title_url'].'">'.$sub['title'].'</option>';
		}
	}
	}
	echo $content;
}

public function load_sub_filter2()
{
	$id = $this->input->post("id");
	//echo $id;exit;
	
	$category = $this->fct->getonerow("categories",array("id_categories"=>$id));
	//print_r($category);exit;
	$content = '<option value="">- All -</option>';
	if(!empty($category)) {
	$ids = array($category['id_categories']);

	/*$sub_categories = $this->ecommerce_model->getSubCategoriesByMultipleCategories2($ids);*/
	$selected_options=array();
	$limit = $this->custom_fct->getCategoriesPages($category['id_categories']);
	$sub_categories = $this->custom_fct->getCategoriesPages($category['id_categories'],array(),array(),$limit,0);
	
$s_id = '';
if(isset($id)) {
$s_id = $info['id_parent'];
}
echo displayLevels($sub_categories,0,$s_id,$selected_options);
$content = '</select>';}
echo $content;
}


}