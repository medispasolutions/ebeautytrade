<?php

class Home extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!$this->acl->isCurrentUserLoggedIn()) {
            $this->view->context = \views\suppliersBackoffice\Contexts::LOGIN_VIEW;
            $this->view->title = 'Login';
            $this->load->twigView('loginPage/index', $this->view);
        } else {
            if ($this->acl->isCurrentUserASupplier()) {
                redirect(site_url('back_office/dashboard'));
            } else {
                redirect(site_url('back_office/home/dashboard'));
            }
        }
    }

    public function login_validate()
    {
        if ($this->acl->isCurrentUserLoggedIn()) {
            redirect(site_url('back_office/home'));
        }

        $this->view->context = \views\suppliersBackoffice\Contexts::LOGIN_VIEW;
        $this->view->title = 'Login';
        $this->view->username = $this->input->post('username');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|xss_clean');
        $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[5]|max_length[15]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->load->twigView('loginPage/index', $this->view);

        } else {
            $bool = false;
            $username = $this->input->post('username');
            $password = $this->input->post('pass');
            // check in user table
            $this->db->where('username', $username);
            $this->db->where('password', md5($password));
            $this->db->where('deleted', 0);
            /* $this->db->where('status',1); */
            $query = $this->db->get('user');
            $user = $query->row_array();
            if (empty($user)) {
                $this->db->where('email', $username);
                $this->db->where_in('id_roles', array(5, 3, 1));
                $this->db->where('password', md5($password));
                $this->db->where('deleted', 0);
                $query = $this->db->get('user');
                $user = $query->row_array();
                if (!empty($user)) {
                    $bool = true;
                }
            } else {
                $bool = true;
            }
            if ($bool) {
                $role = $this->fct->getonecell('roles', 'title', array('id_roles' => $user["id_roles"]));
                $this->session->set_userdata('user_id', $user["id_user"]);
                $this->session->set_userdata('login_id', $user["id_user"]);
                $this->session->set_userdata('correlation_id', $user["correlation_id"]);
                $this->session->set_userdata('user_name', $user["name"]);
                $this->session->set_userdata('level', $user["level"]);
                $this->session->set_userdata('uid', $user["id_user"]);
                $this->session->set_userdata('roles', array($role));
                $this->session->set_userdata('login_date', date("d M Y - h:i:s A"));
                if (checkIfSupplier_admin()) {
                    redirect(site_url('back_office/dashboard'));
                } else {
                    redirect(site_url('back_office/home/dashboard'));
                }
            } else {
                $this->view->error = 'Wrong login or password';
                $this->load->twigView('loginPage/index', $this->view);
            }
        }
    }

    public function dashboard()
    {

        $data["title"] = "Dashboard";
        $data["content"] = "back_office/dashboard";
        $data["content_type"] = $this->fct->getAll('content_type', 'sort_order');

        $cc = $this->fct->getMessages(array());
        $data["messages"] = $this->fct->getMessages(array(), $cc, 0);
        $data["new_message"] = $this->fct->get_unreaded_emails();
        $data["newsletter_emails"] = $this->fct->getAll_orderdate('newsletter');
        $data["users"] = $this->fct->getAll_orderdate('user');

        if (checkIfSupplier_admin()) {
            redirect(site_url('back_office/products'));
        } else {
            $this->load->view('back_office/template', $data);
        }
    }

    public function complete_todo($id)
    {
        $cond = array('id_do' => $id);
        $do_it = $this->fct->getonerow('do_it', $cond);
        echo '<a class="cur" onclick="completed(' . $id . ');">';
        if ($do_it["completed"] == 1) {
            $action = 0;
            echo "<span class='label label-important'>Pending</span>";
        } else {
            echo "<span class='label label-success'>Completed</span>";
            $action = 1;
        }
        echo '</a>';
        $_data["completed"] = $action;
        $this->db->where('id_do', $id);
        $this->db->update('do_it', $_data);
    }

    public function do_it_pop_up($id)
    {
        $cond = array('id_do' => $id);
        $do_it = $this->fct->getonerow('do_it', $cond);
        echo '
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">' . $do_it["title"] . '<small>' . $do_it["Deadline"] . '</small></h3>
        </div>
        <div class="modal-body" id="modal-body">
            <p>' . $do_it["description"] . '</p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
        ';
    }

    public function message_popup($id)
    {
        $cond = array('id' => $id);
        $do_it = $this->fct->getonerow('contactform', $cond);
        echo '<div style="height:auto;"><h2 style="font-size:160%; font-weight:bold; margin:10px 0;">' . $do_it["subject"] . '&nbsp;&nbsp;<span style="color:#AE9A62">Date:' . $do_it["created_date"] . '</span> </h2>
<p>' . $do_it["message"] . '</p></div>';
    }

    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('uid');
        $this->session->unset_userdata('user_name');
        redirect(site_url('back_office/home'));
    }

///////////////////////////////////////////////////////////////////////////////////////////////////

    public function validate_email()
    {
        $email = $this->input->post('email');
        $cond = array(
            'email' => $email
        );
        $user = $this->fct->getonerow('user', $cond);

        if (empty($user)) {
            $this->form_validation->set_message('validate_email', "Account does not exist.");
            return false;
        } else {
            if ($user['status'] == 0) {
                $this->form_validation->set_message('validate_email', "Account is blocked.");
                return false;
            } else {
                $this->load->model('send_emails');
                $request = $this->fct->create_password_request($user);
                $this->send_emails->sendPasswordRequest($user, $request, true);
                $this->session->set_flashdata('success_message', 'Password request was sent to: ' . '<a href="mailto:' . $user['email'] . '">' . $user['email'] . '</a>');
                return true;
            }
        }
    }

    public function password()
    {
        if ($this->acl->isCurrentUserLoggedIn()) {
            redirect(site_url('back_office/home'));
        }

        $this->view->context = \views\suppliersBackoffice\Contexts::FORGOTTEN_PASSWORD_VIEW;
        $this->view->title = "Forgotten password";

        if (isset($_POST) && !empty($_POST)) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|callback_validate_email[]');
            if ($this->form_validation->run() == FALSE) {
                $this->load->twigView('forgottenPasswordPage/index', $this->view);
            } else {
                redirect(site_url('back_office/home/password'));
            }
        } else {
            $error = $this->session->flashdata('error_message');
            if ($error) {
                $this->view->error = $error;
            }
            $success = $this->session->flashdata('success_message');
            if ($success) {
                $this->view->success = $success;
            }
            $this->load->twigView('forgottenPasswordPage/index', $this->view);
        }
    }

    public function loginbypassword()
    {
        if ($this->acl->isCurrentUserLoggedIn()) {
            redirect(site_url('back_office/home'));
        }

        if (isset($_GET['token'])) {
            $token = $_GET['token'];
            $cond = array('token' => $token);
            $check_token = $this->fct->getonerow('password_requests', $cond);
            
            if (empty($check_token)) {
                $this->session->set_flashdata('error_message', 'Invalid token');
                redirect(site_url('back_office/home/password'));
            } else {
                if ($check_token['expiration_date'] < date('Y-m-d h:i:s')) {
                    $this->session->set_flashdata('error_message', 'Token has expired');
                    redirect(site_url('back_office/home/password'));
                } else {
                    if ($check_token['logged'] == 1) {
                        $this->session->set_flashdata('error_message', 'Token has expired');
                        redirect(site_url('back_office/home/password'));
                    } else {
                        $data['logged'] = 1;
                        $data['used_date'] = date('Y-m-d h:i:s');
                        $this->db->where('id_password_requests', $check_token['id_password_requests']);
                        $this->db->update('password_requests', $data);

                        $user = $this->fct->getonerow('user', array('id_user' => $check_token['id_user']));
                        $role = $this->fct->getonecell('roles', 'title', array('id_roles' => $user["id_roles"]));

                        $this->session->set_userdata('user_id', $user["id_user"]);
                        $this->session->set_userdata('user_name', $user["name"]);
                        $this->session->set_userdata('level', $user["level"]);
                        $this->session->set_userdata('uid', $user["id_user"]);
                        $this->session->set_userdata('roles', array($role));
                        $this->session->set_userdata('login_date', date("d M Y - h:i:s A"));

                        $this->load_loginbypassword(true);
                    }
                }
            }
        } else {
            redirect(site_url());
        }
    }

    public function load_loginbypassword($showInfo = false)
    {
        $this->view->context = \views\suppliersBackoffice\Contexts::RESET_PASSWORD_VIEW;
        $this->view->title = "Reset password";
        if ($showInfo) {
            $this->view->success = 'You are now logged in, you can change your password below.';
        }
        $this->load->twigView('resetPasswordPage/index', $this->view);
    }

    public function update_password()
    {
        if (isset($_POST) && !empty($_POST)) {
            $this->form_validation->set_rules('password', "password", 'trim|required|min_length[5]|max_length[15]|xss_clean');
            $this->form_validation->set_rules('confirm_password', "confirm password", 'trim|required|xss_clean|matches[password]');

            if ($this->form_validation->run() == FALSE) {
                $this->load_loginbypassword();
            } else {
                $id_user = $this->acl->getCurrentUserId();
                $password = $this->input->post('password');
                $password = md5($password);
                $data['password'] = $password;
                $data['updated_date'] = date('Y-m-d h:i:s');
                $data['last_login'] = date('Y-m-d h:i:s');
                $this->db->where('id_user', $id_user);
                $this->db->update('user', $data);

                $this->session->set_flashdata('success_message', 'Password was updated successfully.');
                redirect(site_url('back_office/profile'));
            }
        } else {
            $this->session->set_flashdata('error_message', 'The link has expired.');
            redirect(site_url('back_office/home/password'));
        }
    }

    public function import()
    {
        $this->load->view('back_office/import');
    }
}
