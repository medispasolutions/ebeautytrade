<?php

class Products extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "products";
        $this->load->model("products_m");
    }

    //==================================================================================================================
    // helpers

    protected function checkOwner($product_id, $permission_type = 'edit')
    {
        //check permission
        if (!$this->acl->has_permission('products', $permission_type)) {
            return false;
        }

        //check owner
        $cond = array("id_products" => $product_id);
        $productData = $this->fct->getonerecord($this->table, $cond);
        if ($productData['id_user'] != $this->acl->getCurrentUserId()) {
            return false;
        }

        return true;
    }

    protected function export($products)
    {
        $this->export_fct->productsExportToExcel($products);
    }


    /**
     * This functions prepares filters
     * @return array Current filters settings
     */
    protected function getFilters()
    {
        $searchData = [
            'title' => '',
            'sku' => '',
            'barcode' => '',
            'id_brands' => '',
            'category' => '',
            'properties' => '',
            'specials' => '',
        ];

        $currentSessionData = $this->session->userdata('products-filters');
        if (!$currentSessionData) {
            $currentSessionData = [];
        }
        $currentPostedFilters = $this->input->post('Search');
        if (!$currentPostedFilters) {
            $currentPostedFilters = [];
        }
        if ($this->input->get('resetFilters') == 1) {
            $currentPostedFilters = $searchData;
        }

        foreach ($searchData as $key => $_) {
            if (isset($currentPostedFilters[$key])) {
                $searchData[$key] = $currentPostedFilters[$key];
            } else {
                $searchData[$key] = $currentSessionData[$key];
            }
        }

        $this->session->set_userdata('products-filters', $searchData);

        //prepare possible filters
        $possibleFilters = [];
        //categories
        $categories = $this->fct->getAll_cond('categories','title',array('id_parent'=>0));
        foreach ($categories as $cat) {
            $possibleFilters['categories'][] = [
                'TEXT' => $cat['title'],
                'VALUE' => $cat['id_categories'],
                'IS_SELECTED' => $searchData['category'] == $cat['id_categories']
            ];
        }
        //brands
        $cond['id_user'] = $this->acl->getCurrentUserId();
        $brands = $this->fct->getAll_cond('brands','title', $cond);
        foreach ($brands as $brand) {
            $possibleFilters['brands'][] = [
                'TEXT' => $brand['title'],
                'VALUE' => $brand['id_brands'],
                'IS_SELECTED' => $searchData['id_brands'] == $brand['id_brands']
            ];
        }
        
        return [
            'current' => $searchData,
            'possible' => $possibleFilters
        ];
    }

    /**
     * This function searches for relevant products
     * @param $filters
     */
    protected function search($filters)
    {
        $cond = array();

        $cond['id_user'] = $this->acl->getCurrentUserId();
        $cond['admin'] = true;

        if ($filters['title']) {
            $cond['title'] = $filters['title'];
        }

        if ($filters['sku']) {
            $cond['sku'] = $filters['sku'];
        }

        if ($filters['barcode']) {
            $cond['barcode'] = $filters['barcode'];
        }

        if ($filters['id_brands']) {
            $cond['id_brands'] = $filters['id_brands'];
        }

        if ($filters['category']) {
            $cond['category'] = $filters['category'];
        }

        if ($filters['properties']) {
            $cond[$filters['properties']] = 1;
        }

        if ($filters['specials']) {
            $cond['specials'] = $filters['specials'];
        }

        if (isset($filters['status'])) {
            $cond['status'] = $filters['status'];
        }

        $noOfElements = $this->ecommerce_model->getProducts($cond);
        $elements = $this->ecommerce_model->getProducts($cond, 1000, 0);
        //$this->export_fct->productsExportToExcel($data['info']);
        return [$noOfElements, $elements];
    }

    /**
     * Creates array of products digested by live_on_site and ubpublished view
     * @param $products
     */
    protected function prepareProductsForListView($products, $addStatus = false)
    {
        $ret = [];

        foreach ($products as $product) {
            $setAsSet = false;
            if (
                $product['set_as_new']
                || $product['set_as_special']
                || $product['set_as_clearance']
                || $product['set_as_soon']
            ) {
                $setAsSet = true;
            }
            $setAsText = null;
            if ($product['set_as_new']) {
                $setAsText = "New Arrival";
            }
            if ($product['set_as_special']) {
                $setAsText = "Offer";
            }
            if ($product['set_as_clearance']) {
                $setAsText = "Clearance";
            }
            if ($product['set_as_soon']) {
                $setAsText = "Soon";
            }

            //bulk pricing
            $hasBulk = false;
            $priceSegments = $this->fct->getAll_cond(
                'product_price_segments',
                null,
                array('id_products' => $product['id_products'])
            );
            if (!empty($priceSegments)) {
                $hasBulk = true;
            }

            $tmp = [
                'rowId' => $product['id_products'],
                'columns' => [
                    //preview image
                    '/uploads/products/gallery/' . $product['gallery'][0]['image'],
                    //sku
                    [
                        'NAME' => 'sku',
                        'VALUE' => $product['sku'],
                        'EASY_EDIT_URL' => '/back_office/products/easyedit/'.$product['id_products'],
                    ],
                    //product name
                    $product['title'],
                    //last change date
                    $product['updated_date'],
                    //retail price
                    [
                        'NAME' => 'retailprice',
                        'VALUE' => $product['retail_price'],
                        'EASY_EDIT_URL' => '/back_office/products/easyedit/'.$product['id_products'],
                    ],
                    //trade price
                    [
                        'NAME' => 'tradeprice',
                        'VALUE' => $product['list_price'],
                        'EASY_EDIT_URL' => '/back_office/products/easyedit/'.$product['id_products'],
                    ],
                    //actions
                    [
                        'LABEL' => $product['discount'] > 0 ? 'Edit/Remove' : 'Manage',
                        'VARIANT' => $product['discount'] > 0 ? 'btn--primary' : 'btn--secondary',
                        'classes' => ['js-md-open'],
                        'data' => [
                            'md-id' => 'productDiscount',
                            'md-ac-url' => '/back_office/products/editDiscount/'.$product['id_products'],
                        ]
                    ],
                    [
                        'LABEL' => $hasBulk ? 'Edit/Remove' : 'Manage',
                        'VARIANT' => $hasBulk ? 'btn--primary' : 'btn--secondary',
                        'classes' => ['js-md-open'],
                        'data' => [
                            'md-id' => 'multiBuy',
                            'md-ac-url' => '/back_office/products/editBulkPricing/'.$product['id_products'],
                        ]
                    ],
                    [
                        'LABEL' => $setAsSet ? $setAsText.' (Edit)' : 'Manage',
                        'VARIANT' => $setAsSet ? 'btn--primary' : 'btn--secondary',
                        'classes' => ['js-md-open'],
                        'data' => [
                            'md-id' => 'productProperties',
                            'md-ac-url' => '/back_office/products/editProperties/'.$product['id_products'],
                        ]
                    ]
                ]
            ];
            if ($addStatus) {
                $tmp['columns'][] = $product['status'];
            }
            $ret[] = $tmp;
        }

        return $ret;
    }

    /**
     * Prepares brand options array digested by view - should be in view object, but we dont have those
     * @return []
     */
    public function getBrandOptions($selected = null, $user_id = null)
    {
        if (!$user_id) {
            $user_id = $this->acl->getCurrentUserId();
        }
        //brands
        $brands = $this->fct->getAll_cond('brands','title asc', ['id_user' => $user_id]);
        $options = [];
        foreach ($brands as $brand) {
            $options[] = [
                'VALUE' => $brand['id_brands'],
                'TEXT' => $brand['title'],
                'IS_SELECTED' => $brand['id_brands'] ==  $selected,
            ];
        }

        return $options;
    }

    //==================================================================================================================
    // easy edits and modal edits

    /**
     * This function is responsible for discount edit modal
     * @param $product_id
     */
    public function editDiscount($product_id)
    {
        if (!$this->checkOwner($product_id)) {
            echo "It seems you don't have permission to edit this";
            return;
        }

        $product = $this->serviceLocator->product($product_id);

        //check if save
        if ($this->input->post('discount') !== false) {
            header('Content-Type: application/json');

            $saveData['discount'] = (int)str_replace(",", '.', $this->input->post('discount'));

            $saveData['discount_expiration'] = $this->input->post('discount_expiration');
            if ($saveData['discount_expiration']) {
                $saveData['discount_expiration'] = explode('/', $saveData['discount_expiration']);
                $saveData['discount_expiration'] = $saveData['discount_expiration'][2]."-".$saveData['discount_expiration'][1]."-".$saveData['discount_expiration'][0];
            } else {
                $saveData['discount_expiration'] = null;
            }

            $ret = [
                'success' => true,
                'message' => '',
                'data' => [],
            ];

            if ($saveData['discount'] < 0 || $saveData['discount'] >= 100) {
                $ret['success'] = false;
                $ret['message'] = "Discount has to be > 0 and < 100";
                echo json_encode($ret);
                return;
            }

            if ($saveData['discount_expiration'] && $saveData['discount_expiration'] < date("Y-m-d")) {
                $ret['success'] = false;
                $ret['message'] = "Discount expiration cannot be a past date";
                echo json_encode($ret);
                return;
            }

            //apply discount
            $product->applyDiscount($saveData['discount'], $saveData['discount_expiration']);

            echo json_encode($ret);
            return;
        }

        $data = $product->toArray();
        if ($data['discount_expiration'] == '0000-00-00') {
            $data['discount_expiration'] = null;
        }
        if ($data['discount'] == '0') {
            $data['discount'] = null;
        }

        $this->load->twigView('modalContents/product-discount', $data);
    }

    /**
     * This function is responsible for properties edit modal
     * @param $product_id
     */
    public function editProperties($product_id)
    {
        if (!$this->checkOwner($product_id)) {
            echo "It seems you don't have permission to edit this";
            return;
        }

        //check if save
        if ($this->input->post('setas') !== false) {
            header('Content-Type: application/json');

            $saveData = [
                'set_as_new' => 0,
                'set_as_special' => 0,
                'set_as_clearance' => 0,
                'set_as_soon' => 0,
            ];

            if (array_key_exists($this->input->post('setas'), $saveData)) {
                $saveData[$this->input->post('setas')] = 1;
            }

            $ret = [
                'success' => true,
                'message' => '',
                'data' => [],
            ];

            $this->db->where("id_products", $product_id);
            $this->db->update($this->table, $saveData);

            echo json_encode($ret);
            return;
        }

        $cond = array("id_products" => $product_id);
        $data = $this->fct->getonerecord($this->table, $cond);

        $this->load->twigView('modalContents/product-properties', $data);
    }

    /**
     * This function is responsible for properties edit modal
     * @param $product_id
     */
    public function editBulkPricing($product_id)
    {
        if (!$this->checkOwner($product_id)) {
            echo "It seems you don't have permission to edit this";
            return;
        }

        //check if save
        if ($this->input->post('bulk') !== false) {
            header('Content-Type: application/json');
            $bulk = $this->input->post('bulk');
            if (!is_array($bulk)) {
                $bulk = [];
            }

            $ret = [
                'success' => true,
                'message' => '',
                'data' => [],
            ];

            $toSave = [];
            foreach ($bulk as $data) {
                $min_qty = (int)$data['min_qty'];
                if ($min_qty < 0) {
                    $ret['success'] = false;
                    $ret['message'] = "Quantity has to be > 0";
                    echo json_encode($ret);
                    return;
                }
                $price = (float)str_replace(",", '.', $data['price']);
                if ($price < 0) {
                    $ret['success'] = false;
                    $ret['message'] = "Price has to be > 0";
                    echo json_encode($ret);
                    return;
                }
                $toSave[] = [
                    'min_qty' => $min_qty,
                    'price' => $price,
                ];
            }

            $updateData = [
                'deleted' => 1,
                'deleted_date' => date("Y-m-d H:i:s"),
            ];
            $this->db->where("id_products", $product_id);
            $this->db->update("product_price_segments", $updateData);

            foreach ($toSave as $v) {
                if ($v['min_qty']>0 && $v['price']>0) {
                    $addData = [
                        'created_date' => date("Y-m-d H:i:s"),
                        'min_qty' => $v['min_qty'],
                        'price' => $v['price'],
                        'id_products' => $product_id,
                        'id_user' => $this->acl->getCurrentUserId()
                    ];
                    $this->db->insert("product_price_segments", $addData);
                }
            }

            echo json_encode($ret);
            return;
        }

        $cond = array("id_products" => $product_id);
        $data = $this->fct->getonerecord($this->table, $cond);
        $priceSegments = $this->fct->getAll_cond(
            'product_price_segments',
            'min_qty',
            array('id_products' => $product_id)
        );

        $data['bulk'] = [
            [
                'min_qty' => $priceSegments[0] ? $priceSegments[0]['min_qty'] : '',
                'price' => $priceSegments[0] ? $priceSegments[0]['price'] : '',
            ],
            [
                'min_qty' => $priceSegments[1] ? $priceSegments[1]['min_qty'] : '',
                'price' => $priceSegments[1] ? $priceSegments[1]['price'] : '',
            ],
        ];

        $this->load->twigView('modalContents/multi-buy', $data);
    }

    /**
     * This function manages easy edits
     * @param $type
     * @param $product_id
     */
    public function easyedit($product_id){
        $ret = [
            'success' => true,
            'message' => '',
            'data' => [],
        ];

        if (!$this->checkOwner($product_id)) {
            $ret['success'] = false;
            $ret['message'] = "It seems you don't have permissions to edit this product";
            echo json_encode($ret);
            return;
        }

        $cond = array("id_products" => $product_id);
        $data = $this->fct->getonerecord($this->table, $cond);

        $fields = $this->input->post('fields');
        $type = key($fields);
        $value = $fields[$type];

        $error = null;
        $saveData = [];
        $returnValue = [];

        switch ($type) {
            case "sku":
                if (trim($value) == "") {
                    $error = "SKU cannot be empty";
                } else {
                    $saveData['sku'] = trim($value);
                    $returnValue['sku'] = $saveData['sku'];
                }
                break;
            case "retailprice":
                $value = (float)str_replace(",", '.', $value);
                if ((float)$value <= 0) {
                    $error = "Retail price cannot be <= 0";
                } else {
                    $saveData['retail_price'] = $value;
                    $returnValue['retailprice'] = $saveData['retail_price'];
                }
                break;
            case "tradeprice":
                if ((float)$value <= 0) {
                    $error = "Trade price cannot be <= 0";
                } else {
                    $saveData['list_price'] = $value;
                    //apply discount
                    if ($data['discount_active']) {
                        $saveData['price'] = round($saveData['list_price'] * (1 - $data['discount'] / 100), 2);
                    } else {
                        $saveData['price'] = $saveData['list_price'];
                    }
                    $returnValue['tradeprice'] = $saveData['list_price'];
                }
                break;
            case "discount":
                $value = (int)str_replace(",", '.', $value);
                if ((float)$value <= 0 || (float)$value >= 100) {
                    $error = "Discount must be between 0 and 100";
                } else {
                    $saveData['discount'] = $value;
                    //apply discount
                    if ($data['discount_active']) {
                        $saveData['price'] = round($data['list_price'] * (1 - $saveData['discount'] / 100), 2);
                    } else {
                        $saveData['price'] = $data['list_price'];
                    }
                    $returnValue['discount'] = $saveData['discount'];
                }
                break;
        }

        if (empty($saveData)) {
            $error = "I don't know what to save";
        }

        if ($error) {
            $ret['success'] = false;
            $ret['message'] = $error;
            echo json_encode($ret);
            return;
        }

        $this->db->where("id_products", $product_id);
        $this->db->update($this->table, $saveData);

        $ret['data'] = $returnValue;
        echo json_encode($ret);
        return;
    }

    //==================================================================================================================
    // Listings functions

    /**
     * This functions lists supplier's products live on site
     */
    public function live_on_site()
    {
        if (!$this->acl->has_permission('products', 'index')) {
            redirect(site_url("back_office"));
        }
        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PRODUCTS_LISTING_LIVE_ON_SITE,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Products list: Live on site',
                ],
            ],
        ];
        $filters = $this->getFilters();
        $filtersForSearch = $filters['current'];
        $filtersForSearch['status'] = 0;
        //$filtersForSearch['status'] = [1,2];
        list($noOfElements, $products) = $this->search($filtersForSearch);
        if ($this->input->post('filtersRequestType') == "export") {
            return $this->export($products);
        }
        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/products/live_on_site');
        $products_array = $this->prepareProductsForListView(
            array_splice(
                $products,
                ($this->paginator->page-1)*$this->paginator->per_page,
                $this->paginator->per_page
            )
        );
        $data['filters'] = $filters;
        $data['totalSKU'] = $noOfElements;
        $data['info'] = $products_array;
        $this->load->twigView("productsListingPage/live-on-site", $data);
    }

    /**
     * This functions lists supplier's products that are unpublished
     */
    public function unpublished()
    {
        if (!$this->acl->has_permission('products', 'index')) {
            redirect(site_url("back_office"));
        }
        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PRODUCTS_LISTING_UNPUBLISHED,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Products list: Unpublished',
                ],
            ],
        ];
        $filters = $this->getFilters();
        $filtersForSearch = $filters['current'];
        $filtersForSearch['status'] = [1,2,3];
        list($noOfElements, $products) = $this->search($filtersForSearch);
        if ($this->input->post('filtersRequestType') == "export") {
            return $this->export($products);
        }
        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/products/unpublished');
        $products_array = $this->prepareProductsForListView(
            array_splice(
                $products,
                ($this->paginator->page-1)*$this->paginator->per_page,
                $this->paginator->per_page
            ),
            true
        );
        $data['filters'] = $filters;
        $data['totalSKU'] = $noOfElements;

        $data['info'] = $products_array;
        $this->load->twigView("productsListingPage/unpublished", $data);
    }

    /**
     * This functions list all products that have discounts
     */
    public function specials()
    {
        if (!$this->acl->has_permission('products', 'index')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PRODUCTS_LISTING_SPECIALS,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Products list: Specials',
                ],
            ],
        ];

        $filters = $this->getFilters();
        $filters['current']['specials'] = 'all';

        $filtersForSearch = $filters['current'];

        list($noOfElements, $products) = $this->search($filtersForSearch);
        if ($this->input->post('filtersRequestType') == "export") {
            return $this->export($products);
        }

        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/products/specials');

        $products = array_splice(
                $products,
                ($this->paginator->page-1)*$this->paginator->per_page,
                $this->paginator->per_page
            );

        $products_array = [];
        foreach ($products as $product) {

            //set as
            $setAs = [];
            if ($product['set_as_new']) {
                $setAs[] = "New Arrival";
            }
            if ($product['set_as_special']) {
                $setAs[] = "Offer";
            }
            if ($product['set_as_clearance']) {
                $setAs[] = "Clearance";
            }
            if ($product['set_as_soon']) {
                $setAs[] = "Soon";
            }

            //bulk pricing
            $priceSegments = $this->fct->getAll_cond(
                'product_price_segments',
                'min_qty',
                array('id_products' => $product['id_products'])
            );

            $products_array[] = [
                'rowId' => $product['id_products'],
                'columns' => [
                    //preview image
                    '/uploads/products/gallery/' . $product['gallery'][0]['image'],
                    //sku
                    $product['sku'],
                    //product name
                    $product['title'],
                    //trade price
                    $product['list_price'],
                    //discount
                    $product['discount'] . (!$product['discount_active'] ? " (EXPIRED)" : ""),
                    //price after discount
                    $product['price'],
                    //discount until
                    $product['discount_expiration'] != '0000-00-00' ? $product['discount_expiration'] : null,
                    //bulk #1 qty
                    $priceSegments[0] ? $priceSegments[0]['min_qty'] : null,
                    //bulk #1 price
                    $priceSegments[0] ? $priceSegments[0]['price'] : null,
                    //bulk #2 qty
                    $priceSegments[1] ? $priceSegments[1]['min_qty'] : null,
                    //bulk #2 price
                    $priceSegments[1] ? $priceSegments[1]['price'] : null,
                    //set as
                    implode(', ',$setAs),
                ]
            ];
        }

        $data['filters'] = $filters;
        $data['totalSKU'] = $noOfElements;

        $data['info'] = $products_array;
        $this->load->twigView("productsListingPage/specials", $data);
    }

    //==================================================================================================================
    // create and edit functions

    /**
     * This is a function to create a brand as a supplier
     * @var [] $formData Data to be used in the form (bad save)
     * @var [] $errors Data to be used in the form (bad save)
     */
    public function create($formData = null, $errors = null)
    {
        if (!$this->acl->has_permission('products', 'add')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PRODUCT_ADD,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Add product',
                ],
                'MODE' => 'add',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        if ($formData) {
            $data['info'] = $formData;
        }
        $data['canAskForReview'] = false;
        $data['id_user'] = $this->acl->getCurrentUserId();
        $data['brandOptions'] = $this->getBrandOptions();
        $data['tabs'] = [];

        $this->load->twigView("productAddEditPage/index", $data);
    }
    /**
     * This is a function to edit a brand as a supplier
     * @var [] $formData Data to be used in the form (bad save)
     * @var [] $errors Data to be used in the form (bad save)
     */
    public function revise($product_id, $formData = null, $errors = null)
    {
        if (!$this->checkOwner($product_id)) {
            redirect(site_url("back_office"));
        }

        //i don't have time for this
        if ($this->session->flashdata('datasheet_error')) {
            $errors['datasheet_error'] = $this->session->flashdata('datasheet_error');
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PRODUCT_EDIT,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Edit product',
                ],
                'MODE' => 'edit',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        //get details
        $cond = array("id_products" => $product_id);
        $productData = $this->fct->getonerecord($this->table, $cond);

        $data['id_user'] = $this->acl->getCurrentUserId();
        $data['brandOptions'] = $this->getBrandOptions($productData['id_brands']);

        $tabs = $this->fct->getAll_cond("product_information", "id_product_information", ['id_products' => $product_id, 'deleted' => 0]);
        $data['tabs'] = [];
        foreach ($tabs as $tab) {
            $data['tabs'][$tab["id_product_information"]] = $tab;
        }

        $gallery = $this->fct->getAll_cond("products_gallery", "sort_order", ['id_products' => $product_id]);
        $data['gallery'] = [];
        foreach ($gallery as $image) {
            $data['gallery'][$image["id_gallery"]] = $image;
        }

        $data["info"] = $productData;
        $data['canAskForReview'] = !$errors && ($productData['status'] == 1 || $productData['status'] == 3);

        if ($formData) {
            $data['info'] = $formData;
        } else {
            //additional errors
            $errors = [];
            if (empty($data['gallery'])) {
                $errors[] = "Product is added, but at least one product image is required";
                $data['canAskForReview'] = false;
            }
            if (!empty($errors)) {
                $data['globalErrors'] = $errors;
            }
        }

        $this->load->twigView("productAddEditPage/index", $data);
    }

    /**
     * This is the endpoint to save a product as a supplier
     */
    public function save()
    {
        $product_id = $this->input->post("id_products");
        $currentData = [];

        //check owner
        if ($product_id) {
            if (!$this->checkOwner($product_id)) {
                redirect(site_url("back_office"));
            }
            $currentData = $this->fct->getonerecord($this->table, ['id_products' => $product_id]);
        }

        $this->form_validation->set_rules("brief", "brief", "trim|required");
        $this->form_validation->set_rules("title" . "Title", "trim|required");
        $this->form_validation->set_rules("description", "description", "trim");
        $this->form_validation->set_rules("id_brands", "brand", "trim|required");
        $this->form_validation->set_rules("sku", "sku", "trim|required|callback_validate_sku[]");
        $this->form_validation->set_rules("barcode", "barcode", "trim|callback_validate_barcode[]");
        $this->form_validation->set_rules("list_price", "price", "trim|required");
        $this->form_validation->set_rules("retail_price", "Retail Price", "trim");
        $this->form_validation->set_rules("weight", "weight", "trim");
        $this->form_validation->set_rules("length", "length", "trim");
        $this->form_validation->set_rules("width", "width", "trim");
        //tabs
        $this->form_validation->set_rules("youtube", "youtube", "trim");
        $this->form_validation->set_rules("datasheet", "datasheet", "trim");
        $this->form_validation->set_rules("catalog", "catalog", "trim");
        $this->form_validation->set_rules("set_as_pro", "set_as_pro", "trim");
        $this->form_validation->set_rules("set_as_non_exportable", "set_as_non_exportable", "trim");

        if ($this->form_validation->run() == FALSE) {
            if ($product_id) {
                return $this->revise($product_id, $this->input->post(null), $this->form_validation->_error_array);
            } else {
                return $this->create($this->input->post(null), $this->form_validation->_error_array);
            }
        }

        $_data["id_user"] = $this->acl->getCurrentUserId();
        $_data["brief"] = $this->input->post("brief", true);
        $_data["title"] = $this->input->post("title", true);
        $_data["description"] = $this->input->post("description");
        $_data["id_brands"] = $this->input->post("id_brands", true);
        $_data["sku"] = $this->input->post("sku", true);
        $_data["barcode"] = $this->input->post("barcode", true);
        $_data["list_price"] = $this->input->post("list_price", true);
        $_data["retail_price"] = $this->input->post("retail_price", true);
        if ($currentData['discount_active']) {
            $_data['price'] = round($_data['list_price'] * (1 - $currentData['discount'] / 100), 2);
        } else {
            $_data['price'] = $_data['list_price'];
        }
        $_data['categories_comments'] = $this->input->post("categories_comments", true);
        $_data["attributes_comments"] = $this->input->post("attributes_comments", true);
        $_data["weight"] = $this->input->post("weight", true) ? $this->input->post("weight", true) : null;
        $_data["length"] = $this->input->post("length", true) ? $this->input->post("length", true) : null;
        $_data["width"] = $this->input->post("width", true) ? $this->input->post("width", true) : null;

        //tabs

        $_data["youtube"] = $this->input->post("youtube", true);

        if (!empty($_FILES["datasheet"]["name"])) {
            $size = $_FILES['datasheet']['size'];
            $size_arr = getMaxSize('file');
            $max_size = $size_arr['size'];
            if ($max_size > $size) {
                if ($product_id) {
                    $old_image = $currentData["datasheet"];
                    if (!empty($old_image) && file_exists('./uploads/products/' . $old_image)) {
                        unlink("./uploads/products/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("datasheet", "products");
                $_data["datasheet"] = $image1;
            } else {
                $this->session->set_flashdata('datasheet_error', 'Datasheet file exceeds maximum size');
            }
        }

        $_data["catalog"] = $this->input->post("catalog", true);
        $_data["set_as_pro"] = $this->input->post("set_as_pro") == 1 ? 1 : 0;
        $_data["set_as_non_exportable"] = $this->input->post("set_as_non_exportable") == 1 ? 1 : 0;


        if ($product_id) {
            $_data["updated_date"] = date("Y-m-d h:i:s");
            $this->db->where("id_products", $product_id);
            $this->db->update($this->table, $_data);
        } else {
            //status for new products is always 'draft'
            $_data['status'] = 3;
            $_data["created_date"] = date("Y-m-d h:i:s");
            $this->db->insert($this->table, $_data);
            $product_id = $this->db->insert_id();
        }

        //save tabs, quite long

        $tabs = $this->input->post("tabs", true);
        if (!is_array($tabs)) {
            $tabs = [];
        }

        //existing tabs (read before new, as new tabs add sth new)
        $currentTabsData = $this->fct->getAll_cond("product_information", "id_product_information", ['id_products' => $product_id, 'deleted' => 0]);
        $currentTabs = [];
        foreach ($currentTabsData as $tab) {
            $currentTabs[$tab["id_product_information"]] = $tab;
        }

        //new tabs
        if (isset($tabs['new'])) {
            //we need to rebuild part of $_FILES table, as CI uploader does not know how to do manage files under arrays
            if (isset($_FILES['tabFile1']) && is_array($_FILES['tabFile1']['name'])) {
                foreach ($_FILES['tabFile1']['name'] as $key => $value) {
                    $_FILES['tabFile1-'.$key]['name'] = $value;
                    $_FILES['tabFile1-'.$key]['type'] = $_FILES['tabFile1']['type'][$key];
                    $_FILES['tabFile1-'.$key]['tmp_name'] = $_FILES['tabFile1']['tmp_name'][$key];
                    $_FILES['tabFile1-'.$key]['error'] = $_FILES['tabFile1']['error'][$key];
                    $_FILES['tabFile1-'.$key]['size'] = $_FILES['tabFile1']['size'][$key];
                }
            }
            if (isset($_FILES['tabFile2']) && is_array($_FILES['tabFile2']['name'])) {
                foreach ($_FILES['tabFile2']['name'] as $key => $value) {
                    $_FILES['tabFile2-'.$key]['name'] = $value;
                    $_FILES['tabFile2-'.$key]['type'] = $_FILES['tabFile2']['type'][$key];
                    $_FILES['tabFile2-'.$key]['tmp_name'] = $_FILES['tabFile2']['tmp_name'][$key];
                    $_FILES['tabFile2-'.$key]['error'] = $_FILES['tabFile2']['error'][$key];
                    $_FILES['tabFile2-'.$key]['size'] = $_FILES['tabFile2']['size'][$key];
                }
            }

            $no_of_new_tabs = count($tabs['new'])/2;
            $newTabs = [];
            for ($i = 0; $i < $no_of_new_tabs; $i++) {
                $_newTab['id_products'] = $product_id;
                $_newTab['id_user'] = $this->acl->getCurrentUserId();
                $_newTab['created_date'] = date("Y-m-d H:i:s");
                $_newTab['title'] = $tabs['new'][$i*2]['title'];
                $_newTab['description'] = $tabs['new'][($i*2)+1]['description'];

                //upload files
                $file1Key = 'tabFile1-'.$i;
                if ($_FILES[$file1Key]['name']) {
                    $file1= $this->fct->uploadImage($file1Key, "product_information");
                    $_newTab["file"]=$file1;
                }

                $file2Key = 'tabFile2-'.$i;
                if ($_FILES[$file2Key]['name']) {
                    $file2= $this->fct->uploadImage($file2Key, "product_information");
                    $_newTab["file2"]=$file2;
                }

                $newTabs[] = $_newTab;
            }

            foreach ($newTabs as $tabData) {
                $this->db->insert("product_information", $tabData);
            }

            unset($tabs['new']);
        }

        //update tabs
        foreach ($tabs as $tab_id => $tabData) {
            $tabData['updated_date'] = date("Y-m-d H:i:s");

            $file1Key = 'tab'.$tab_id.'File1';
            if ($_FILES[$file1Key]['name']) {
                $old_image = $currentTabs[$tab_id]["file"];
                if (!empty($old_image) && file_exists('./uploads/product_information/' . $old_image)) {
                    unlink("./uploads/product_information/" . $old_image);
                }
                $file1= $this->fct->uploadImage($file1Key, "product_information");
                $tabData["file"]=$file1;
            }

            $file2Key = 'tab'.$tab_id.'File2';
            if ($_FILES[$file2Key]['name']) {
                $old_image = $currentTabs[$tab_id]["file2"];
                if (!empty($old_image) && file_exists('./uploads/product_information/' . $old_image)) {
                    unlink("./uploads/product_information/" . $old_image);
                }
                $file2= $this->fct->uploadImage($file2Key, "product_information");
                $tabData["file2"]=$file2;
            }

            $this->db->where("id_product_information", $tab_id);
            $this->db->where("id_products", $product_id);
            $this->db->update("product_information", $tabData);

            unset($currentTabs[$tab_id]);
        }

        //delete tabs
        foreach ($currentTabs as $tab_id => $tabData) {
            $image = $tabData["file"];
            if (!empty($image) && file_exists('./uploads/product_information/' . $image)) {
                unlink("./uploads/product_information/" . $image);
            }
            $saveData["file"] = null;

            $image = $tabData["file2"];
            if (!empty($image) && file_exists('./uploads/product_information/' . $image)) {
                unlink("./uploads/product_information/" . $image);
            }
            $saveData["file2"] = null;

            $saveData['deleted'] = 1;
            $saveData['deleted_date'] = date("Y-m-d H:i:s");
            $this->db->where("id_product_information", $tab_id);
            $this->db->update("product_information", $saveData);
        }

        redirect('back_office/products/revise/'.$product_id);
    }

    public function unpublish($product_id)
    {
        if (!$this->checkOwner($product_id)) {
            redirect(site_url("back_office"));
        }
        $cond = array("id_products" => $product_id);
        $productData = $this->fct->getonerecord($this->table, $cond);

        if ($productData['status'] == 0) {
            $productData['published'] = $_data["published"] = 1;
            $productData['status'] = $_data["status"] = 1;
            $productData['updated_date'] = $_data["updated_date"] = date("Y-m-d h:i:s");
            $this->db->where("id_products", $product_id);
            $this->db->update($this->table, $_data);
            $this->notifyAdmin($productData);
        }

        redirect('back_office/products/unpublished');
    }

    public function send_to_review($product_id)
    {
        if (!$this->checkOwner($product_id)) {
            redirect(site_url("back_office"));
        }

        $cond = array("id_products" => $product_id);
        $productData = $this->fct->getonerecord($this->table, $cond);

        if ($productData['status'] == 1 || $productData['status'] == 3) {
            $productData['status'] = $_data["status"] = 2;
            $productData['updated_date'] = $_data["updated_date"] = date("Y-m-d h:i:s");
            $this->db->where("id_products", $product_id);
            $this->db->update($this->table, $_data);
            $this->notifyAdmin($productData);
        }

        redirect('back_office/products/unpublished');
    }

    protected function notifyAdmin($productData)
    {
        $this->load->model('send_emails');
        $this->send_emails->sendProductStatusChangeToAdmin($this->acl->getCurrentUserData()->toArray(), $productData);
    }

    public function gallery_new_image($product_id)
    {
        $ret = [
            'success' => true,
        ];

        if (!$this->checkOwner($product_id)) {
            $ret['success'] = false;
            echo json_encode($ret);
            return;
        }

        //gallery frontend plugin sends us an uuid for new images, so we have to use it during session
        $uuid = $this->input->post('uuid', true);

        //this is copied from some other part controllers/control.php -> upload_gal_image()
        $content_type = "products";
        $type_record = $this->fct->getonerow('content_type',array('name'=>$content_type));
        $thumb_vals = array();
        if(!empty($type_record['thumb_val_gal'])) {
            $thumb_vals = explode(',', $type_record['thumb_val_gal']);
        }
        $image_name = $this->fct->uploadImage("file",$content_type."/gallery");
        $this->fct->createthumb($image_name,$content_type."/gallery","120x120");

        if (!empty($thumb_vals)) {
            foreach($thumb_vals as $val) {
                if($val != '') {
                    $this->fct->createthumb($image_name,$content_type."/gallery",$val);
                }
            }
        }

        $this->db->select_max('sort_order');
        $this->db->where('id_'.$content_type, $product_id);
        $res = $this->db->get($content_type.'_gallery')->row_array();
        $max_sort = $res["sort_order"];

        $data['image'] = $image_name;
        $data['created_date'] = date('Y-m-d h:i:s');
        $data['id_'.$content_type] = $product_id;
        $data['sort_order'] = $max_sort+1;
        $this->db->insert($content_type.'_gallery', $data);
        $image_id = $this->db->insert_id();

        //store db image id in session with sent uuid
        $sessionProductImages = $this->session->userdata('products-new-images');
        if (!is_array($sessionProductImages)) {
            $sessionProductImages = [];
        }
        $sessionProductImages[$uuid] = $image_id;
        $this->session->set_userdata('products-new-images', $sessionProductImages);

        echo json_encode($ret);
        return;
    }

    public function gallery_delete_image($product_id)
    {
        $ret = [
            'success' => true,
        ];

        if (!$this->checkOwner($product_id)) {
            $ret['success'] = false;
            echo json_encode($ret);
            return;
        }

        //this is copied from some other part controllers/control.php -> delete_gal_image()
        $content_type = "products";
        $id_image = $this->input->post('uuid');
        //this image can be an uuid of new image
        if (!is_numeric($id_image)) {
            $sessionProductImages = $this->session->userdata('products-new-images');
            if (!is_array($sessionProductImages)) {
                $sessionProductImages = [];
            }
            $id_image = $sessionProductImages[$id_image];
        }

        $type_record = $this->fct->getonerow('content_type',array('name'=>$content_type));
        $imageData = $this->fct->getonerow($content_type.'_gallery',array('id_gallery'=>$id_image));
        $image = $imageData['image'];
        $thumb_vals = array();
        if(!empty($type_record['thumb_val_gal'])) {
            $thumb_vals = explode(',',$type_record['thumb_val_gal']);
        }
        if(file_exists("./uploads/".$content_type."/gallery/".$image)){
            unlink("./uploads/".$content_type."/gallery/".$image);
        }
        if(file_exists("./uploads/".$content_type."/gallery/120x120/".$image)){
            unlink("./uploads/".$content_type."/gallery/120x120/".$image);
        }
        if(!empty($thumb_vals)) {
            foreach($thumb_vals as $val) {
                if(!empty($val)) {
                    if(file_exists("./uploads/".$content_type."/gallery/".$val."/".$image)){
                        unlink("./uploads/".$content_type."/gallery/".$val."/".$image);
                    }
                }
            }
        }

        $this->db->where("id_products",$product_id);
        $this->db->where("id_gallery",$id_image);
        $this->db->delete($content_type.'_gallery');

        echo json_encode($ret);
        return;
    }

    public function gallery_order_images($product_id)
    {
        $ret = [
            'success' => true,
        ];

        if (!$this->checkOwner($product_id)) {
            $ret['success'] = false;
            echo json_encode($ret);
            return;
        }

        //get session new images uuids
        $sessionProductImages = $this->session->userdata('products-new-images');
        if (!is_array($sessionProductImages)) {
            $sessionProductImages = [];
        }

        //this is copied from some other part controllers/control.php -> delete_gal_image()
        $content_type = "products";
        $newOrder = $this->input->post('order');
        $newOrder = explode('|',$newOrder);
        if (!empty($newOrder)) {
            $i=0;
            foreach($newOrder as $image_id) {
                //this image can be an uuid of new image
                if (!is_numeric($image_id)) {
                    $image_id = $sessionProductImages[$image_id];
                }

                $data['updated_date'] = date('Y-m-d h:i:s');
                $data['sort_order'] = $i + 1;

                $this->db->where("id_products",$product_id);
                $this->db->where('id_gallery', $image_id);
                $this->db->update($content_type.'_gallery', $data);
                $i++;
            }
        }

        echo json_encode($ret);
        return;
    }

    //==================================================================================================================
    // validators

    public function validate_sku()
    {
        $sku = $this->input->post('sku');
        $cond['sku'] = $sku;
        if ($this->input->post('id') != '') {
            $cond['id_products !='] = $this->input->post('id');
        } elseif ($this->input->post('id_products') != '') {
            $cond['id_products !='] = $this->input->post('id_products');
        }
        $check = $this->fct->getonerecord('products', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('validate_sku', 'SKU exists, please use another SKU value.');
            return false;
        }
    }

    public function validate_barcode()
    {
        $barcode = $this->input->post('barcode');
        $cond['barcode'] = $barcode;
        if ($this->input->post('id') != '') {
            $cond['id_products !='] = $this->input->post('id');
        } elseif ($this->input->post('id_products') != '') {
            $cond['id_products !='] = $this->input->post('id_products');
        }
        $check = $this->fct->getonerecord('products', $cond);
        if (empty($check) || empty($barcode)) {
            return true;
        } else {
            $this->form_validation->set_message('validate_barcode', 'Barcode exists, please use another Barcode value.');
            return false;
        }
    }

    public function checkUploadFile()
    {
        $size = $_FILES['datasheet']['size'];
        $size_arr = getMaxSize('file');
        $max_size = $size_arr['size'];

        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadFile', 'File ' . $_FILES['datasheet']['name'] . ' exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function checkUploadFile2()
    {
        if (isset($_POST['description_product_info'])) {
            $bool = true;
            $product_descriptions = $_POST['description_product_info'];
            $size_arr = getMaxSize('file');
            $max_size = $size_arr['size'];
            foreach ($product_descriptions as $key => $info) {
                if (!empty($_FILES["file_product_info_" . $key]["name"])) {
                    $size = $_FILES["file_product_info_" . $key]["size"];

                    if ($max_size > $size) {

                    } else {
                        $bool = false;
                    }
                }
            }

            if ($bool) {
                return true;
            } else {
                $this->form_validation->set_message('checkUploadFile2', 'File ' . $_FILES["file_product_info_" . $key]["name"] . ' exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
                return false;
            }
        }
    }

    //==================================================================================================================
    // old, main back office functions

    public function index($order = "")
    {
        /* 	$delivery_order_line_items=$this->fct->getAll_cond('delivery_order_line_items','id_delivery_order_line_items',array('id_orders'=>250));
          foreach($delivery_order_line_items as $line_item){
          if($line_item['type']=="product"){
          $product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
          $cond['id_products']=$line_item['id_products'];
          $update['quantity']=$product['quantity']-$line_item['quantity'];
          $this->db->where($cond);
          $this->db->update('products',$update);
          }else{
          $product=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));
          $cond4['id_products_stock']=$line_item['id_products'];
          $update['quantity']=$product['quantity']-$line_item['quantity'];
          $this->db->where($cond4);
          $this->db->update('products_stock',$update);
          }

          } */

        /* 		$delivery_order_line_items=$this->fct->getAll_cond('delivery_order_line_items','id_delivery_order_line_items',array('id_orders'=>250));
          foreach($delivery_order_line_items as $line_item){
          $product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
          $cond['id_products']=$product['id_products'];
          $update['quantity']=$product['quantity']-$line_item['quantity'];
          $this->db->where($cond);
          $this->db->update('products',$update);


          } */

        $this->session->unset_userdata("admin_redirect_url");
//echo  $this->session->userdata('uid').' and '.$this->session->userdata('roles');exit;
        if ($this->acl->has_permission('products', 'index')) {

            if ($this->acl->isCurrentUserASupplier()) {
                redirect('back_office/products/live_on_site');
            }

            if ($order == "")
                $order = "sort_order";
            $data["title"] = "List products";
            $data["content"] = "back_office/products/list";

//
            $this->session->unset_userdata("back_link");
//    
            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "32";
            }
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
            $data["show_items"] = $show_items;

// organize data filtration
            $cond = array();
            $cond['All'] = "All";
            $cond['admin'] = "admin";
            $url = site_url('back_office/products?pagination=on');

            if (isset($_GET['product_id'])) {
                $product_id = $_GET['product_id'];
                $url .= '&product_id=' . $product_id;
                if (!empty($product_id))
                    $cond['id_products'] = $product_id;
            }

            if (isset($_GET['product_name'])) {
                $product_name = $_GET['product_name'];
                $url .= '&product_name=' . $product_name;
                if (!empty($product_name))
                    $cond['title' . getFieldLanguage($this->lang->lang())] = $product_name;
            }

            if (isset($_GET['product_sku'])) {
                $product_sku = $_GET['product_sku'];
                $url .= '&product_sku=' . $product_sku;
                if (!empty($product_sku))
                    $cond['sku'] = $product_sku;
            }

            if (isset($_GET['barcode'])) {
                $barcode = $_GET['barcode'];
                $url .= '&barcode=' . $barcode;
                if (!empty($barcode))
                    $cond['barcode'] = $barcode;
            }

            if (isset($_GET['group_category'])) {
                $group_category = $_GET['group_category'];
                $url .= '&group_category=' . $group_category;
                if (!empty($group_category))
                    $cond['group_category'] = $group_category;
            }


            if ($this->acl->isCurrentUserASupplier()) {
                $cond['id_admin'] = $this->session->userdata("uid");
                $cond['with_products_stock'] = 1;
                $data['supplier'] = 1;
                /* $cond["id_user_m"]=$this->session->userdata("uid"); */
                $brands = $this->fct->getAll_cond('brands', 'sort_order asc', array('id_user' => $this->session->userdata("uid")));
                $ids_brands = $this->ecommerce_model->getIds('id_brands', $brands);
                if (!empty($ids_brands)) {
                    $cond['brands_ids'] = $ids_brands;
                } else {
                    $cond['brands_ids'] = array(-1);
                }
            }
            if (isset($_GET['category'])) {
                $category = $_GET['category'];
                $url .= '&category=' . $category;
                if (!empty($category))
                    $cond['category'] = $category;
            }
            if (isset($_GET['brand'])) {
                $brand = $_GET['brand'];
                $url .= '&brand=' . $brand;
                if (!empty($brand))
                    $cond['id_brands'] = $brand;
            }

            if (isset($_GET['sub_category'])) {
                $sub_category = $_GET['sub_category'];
                $url .= '&sub_category=' . $sub_category;
                if (!empty($sub_category))
                    $cond['category'] = $sub_category;
            }


            if (isset($_GET['classifications'])) {
                $classifications = $_GET['classifications'];
                $url .= '&classifications=' . $classifications;
                if (!empty($classifications))
                    $cond['classifications'] = $classifications;
            }

            if (isset($_GET['designer'])) {
                $designer = $_GET['designer'];
                $url .= '&designer=' . $designer;
                if (!empty($designer))
                    $cond['products.designer'] = $designer;
            }


            if ($this->input->get('news') != "" && $this->input->get('news') != 0) {

                $news = $_GET['news'];
                $url .= '&news=' . $news;
                $cond['set_as_news'] = $news;
            }

            if ($this->input->get('status') != "") {

                $status = $_GET['status'];
                $url .= '&status=' . $status;

                $cond['status'] = $status;
            }


            if (isset($_GET['display_in_home']) && $_GET['display_in_home'] != 0) {
                $display_in_home = $_GET['display_in_home'];
                $url .= '&display_in_home=' . $display_in_home;
                if (!empty($display_in_home))
                    $cond['display_in_home'] = 1;
            }

            if (isset($_GET['availability']) && $_GET['availability'] != 2) {

                $availability = $_GET['availability'];

                $url .= '&availability=' . $availability;
                if (!empty($availability) || $availability == 0)
                    $cond['availability'] = $availability;
            }

            if (isset($_GET['sales']) && $_GET['sales'] != 2) {
                $sales = $_GET['sales'];
                $url .= '&sales=' . $sales;
                if (!empty($sales))
                    $cond['sales'] = $sales;
                unset($cond['All']);
            }

            if (isset($_GET['published']) && $_GET['published'] == 1) {
                $published = $_GET['published'];
                $url .= '&published=' . $published;
                if (!empty($published))
                    $cond['published'] = $published;
            }

            $cond['group_products'] = 99;


// pagination  start :
            $count_news = $this->ecommerce_model->getProducts($cond);

            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $this->load->library('pagination');
            $config['base_url'] = $url;
            $config['num_links'] = '8';
            $config['total_rows'] = $count_news;
            $config['per_page'] = $show_items;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
            $this->pagination->initialize($config);
            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '')
                    $page = $_GET['per_page'];
                else
                    $page = 0;
            } else
                $page = 0;
            $data['info'] = $this->ecommerce_model->getProducts($cond, $config['per_page'], $page);

            $url .= '&per_page=' . $page;

            $data['url'] = $url;
            $this->session->set_userdata("back_link", $url);
            /* echo "<pre>";
              print_r($data['info']);exit; */
            if (isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {
                $this->export_fct->productsExportToExcel($data['info']);
            }

////////////////GET Results////
            if (isset($data['supplier'])) {
                $products = $this->ecommerce_model->getProducts($cond, $count_news, 0);
                $total = $this->admin_fct->getProductsTotal($products);
                $data['total'] = $total;
            }


            if (isset($_GET['report_submit']) && $_GET['report_submit'] == 'import') {
                redirect(site_url("back_office/excel_c/import?section=" . $_GET['section']));
            }
// end pagination .
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office"));
        }
    }

    public function getRelatedProducts()
    {
        $keyword = $_POST['term'];
        $cond = array();

        if (checkIfSupplier_admin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $cond['id_user'] = $user['id_user'];
        }


        $cond['title'] = $keyword;
        $count_news = $this->ecommerce_model->getProducts($cond);
        $results = $this->ecommerce_model->getProducts($cond, $count_news, 0);


//$results=$this->fct->getimagegallery($keyword);

        $cc = count($results);
        $i = 0;
        echo '[';
        if (!empty($results)) {
            foreach ($results as $result) {
                $i++;
                if ($i == $cc)
                    $coma = '';
                else
                    $coma = ',';
                $order_name = $result['title'];
//$res = preg_replace('/\.[^.]+$/','',$result['title']);
//$res =str_replace(".jpg","",$result['image']);
                echo '{"label" : "' . $order_name . $result['id_products'] . '", "value": "' . $result['id_products'] . '_' . $order_name . $result['id_products'] . '"}' . $coma;
//echo '"'.str_replace("'","",$result['title']).'"'.$coma;
            }
        }
        echo ']';
    }

    public function clone_product($id)
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $old_product = $this->fct->getonerow("products", array("id_products" => $id));
        $new_product = $old_product;
        unset($new_product['id_products']);
        unset($new_product['sort_order']);
        unset($new_product['created_date']);
        unset($new_product['deleted_date']);
        unset($new_product['updated_date']);
        unset($new_product['image']);
        unset($new_product['pdf']);
        $new_product["title_url" . $lang] = $this->fct->cleanURL("products", url_title($new_product['title_url' . $lang]));
        unset($new_product['promote_to_front']);
        $new_product['created_date'] = date("Y-m-d h:i:s");
        $this->db->insert("products", $new_product);
        $new_id = $this->db->insert_id();
        redirect(site_url("back_office/products/edit/" . $new_id));
    }

    public function add($data = array())
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/products/create');
        }

        if ($this->acl->has_permission('products', 'add')) {
            $data["title"] = "Add products";
            $data["content"] = "back_office/products/add";

            if (checkIfSupplier_admin()) {
                $data['supplier'] = true;
            } else {
                $data['supplier'] = false;
            }

            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function do_import()
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/products/live_on_site');
        }

        include './Classes/PHPExcel/IOFactory.php';
        $inputFileName = $_FILES['tmp_name']['tmp_name'];

        $section = 'uploadProducts';


        try {
            $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch (Exception $e) {
//die();


            $data['error'] = 'Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage();

            $this->load->view("back_office/import_excel", $data);
            /* break; */
        }

        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $arrayCount = count($allDataInSheet);  // Here get total count of row in that Excel sheet


        $this->load->model("title_url_ar");
        $this->load->model("import_model");

//echo '<pre>';print_r($allDataInSheet);exit;
        $data = array();

        $result = $this->import_model->import($section, $allDataInSheet);

        $this->session->set_flashdata("success_message", "File is imported successfully.");

        $this->load->view("back_office/import_excel", $data);
        /* for($i=3;$i<=$arrayCount;$i++){
          $userName = trim($allDataInSheet[$i]["A"]);
          $userMobile = trim($allDataInSheet[$i]["B"]);
          } */
    }

    public function view($id, $obj = '')
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/products/live_on_site');
        }

        if ($this->acl->has_permission('products', 'index')) {
            $data["title"] = "View products";
            $data["content"] = "back_office/products/add";
            $cond = array("id_products" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function edit($id, $obj = '', $data = array())
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/products/revise/'.$id);
        }
        if ($this->acl->has_permission('products', 'edit')) {
            $data["title"] = "Edit products";
            $data["content"] = "back_office/products/add";
            $cond['id_products'] = $id;
            if (checkIfSupplier_admin()) {
                $cond['id_user'] = $this->session->userdata("uid");
                $data['supplier'] = true;
            } else {
                $data['supplier'] = false;
            }
            $data["id"] = $id;
            $data["info"] = $this->fct->getOneProduct_admin($cond);


            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete($id)
    {
        if ($this->acl->has_permission('products', 'delete')) {
            $product = $this->ecommerce_model->getOneProduct($id);
            $user = $this->fct->getonerecord('user', array('id_user' => $this->session->userdata("uid")));
            if ($user['id_roles'] == 5) {
                $_data = array("deleted_supplier" => 1,
                    "deleted_date" => date("Y-m-d h:i:s"));
            } else {
                $_data = array("deleted" => 1,
                    "deleted_date" => date("Y-m-d h:i:s"));
            }


            $this->db->where("id_products", $id);
            $this->db->update($this->table, $_data);
            if ($user['id_roles'] == 5 && $product['deleted_supplier'] != 1) {
                $data['user'] = $user;
                $data['products'][0] = $product;
                $this->load->model('send_emails');
                $this->send_emails->sendDeleteProductsToAdmin($data);
                $this->send_emails->sendDeleteProductsToClient($data);
            }

            if ($user['id_roles'] != 5 && $product['deleted_supplier'] == 1) {
                $data['user'] = $product['user'];
                $data['products'][0] = $product;
                $this->load->model('send_emails');
                $this->send_emails->sendProductsStatusToClient($data);
            }
            $this->session->set_userdata("success_message", "Information was deleted successfully");
            redirect(site_url("back_office/products"));
        } else {
            redirect(site_url("back_office/products"));
        }
    }

    public function delete_segement()
    {
        $id_product = $this->input->post('id_product');
        $id_segement = $this->input->post('id_segement');
        $cond['id_products'] = $id_product;
        $cond['id_product_price_segments'] = $id_segement;
        $_data = array("deleted" => 1,
            "deleted_date" => date("Y-m-d h:i:s"));
        $this->db->where($cond);
        $this->db->update('product_price_segments', $_data);
        echo "done";
        exit;
    }

    public function update_quantity()
    {
        $product_ids = $this->input->post('product_id');
        $rand_arr = $this->input->post('rand');
        $quantities = $this->input->post('stock_status_quantity_threshold');
        $list_prices = $this->input->post('list_price');
        $retail_prices = $this->input->post('retail_price');
        $discount = $this->input->post('discount');
        $return_message = "";

        $dataTmp = $this->cart->contents();
        $flag = 0;
        $i = 0;
        $redeem_bool = true;
        foreach ($product_ids as $key => $val) {
            $i++;

            $id = $product_ids[$key];
            $rand = $rand_arr[$key];
            $qty = $quantities[$key];
            $list_price = $list_prices[$key];
            $retail_price = $retail_prices[$key];
            $disc = $discount[$key];

            if ($disc > 0) {

                $testprice = round($list_price - (($list_price * $disc) / 100));
                $price = $testprice;
            } else {
                $price = $list_price;
            }

            if ($list_price > $price) {
                $update["discount_status"] = 1;
            } else {
                $update["discount_status"] = 0;
            }

            $update['stock_status_quantity_threshold'] = $qty;
            $update['list_price'] = $list_price;
            $update['price'] = $price;
            $update['retail_price'] = $retail_price;
            $update['discount'] = $disc;
            $update['updated_date'] = date('Y-m-d h:i:s');


            $this->db->where(array('id_products' => $id));
            $this->db->update('products', $update);
        }


        $return['message'] = 'Done!';
        $return['result'] = 1;

        die(json_encode($return));
    }

    public function submit_products()
    {
        if ($this->input->post('method') == "update_quantity") {
            $this->update_quantity();
        } else {

            $this->delete_all();
        }
    }

    public function delete_all()
    {
        if ($this->acl->has_permission('products', 'delete_all')) {
            $user = $this->fct->getonerecord('user', array('id_user' => $this->session->userdata("uid")));
            $cehcklist = $this->input->post("cehcklist");
            $check_option = $this->input->post("check_option");
            if ($check_option == "delete_all") {
                $k = 0;
                if (count($cehcklist) > 0) {
                    for ($i = 0; $i < count($cehcklist); $i++) {
                        if ($cehcklist[$i] != "") {
                            $product = $this->fct->getonerecord('products', array('id_products' => $cehcklist[$i]));
                            if ($user['id_roles'] == 5) {
                                if ($product['deleted_supplier'] != 1) {
                                    $products[$k] = $product;
                                    $_data = array("deleted_supplier" => 1,
                                        "deleted_date" => date("Y-m-d h:i:s"));
                                    $k++;
                                }
                            } else {
                                $product = $this->ecommerce_model->getOneProduct($cehcklist[$i]);
                                if ($product['deleted_supplier'] == 1) {

                                    $products[$k] = $product;
                                    $k++;
                                }
                                $_data = array("deleted" => 1,
                                    "deleted_date" => date("Y-m-d h:i:s"));
                            }
                            $this->db->where("id_products", $cehcklist[$i]);
                            $this->db->update($this->table, $_data);
                        }
                    }
                }

                if ($user['id_roles'] == 5 && !empty($products)) {
                    $data['user'] = $user;
                    $data['products'] = $products;
                    $this->load->model('send_emails');
                    $this->send_emails->sendDeleteProductsToAdmin($data);
                    $this->send_emails->sendDeleteProductsToClient($data);
                }

                if ($user['id_roles'] != 5 && !empty($products)) {
                    $data['user'] = $products[0]['user'];
                    $data['products'] = $products;
                    $this->load->model('send_emails');
                    $this->send_emails->sendProductsStatusToClient($data);
                }

                $this->session->set_userdata("success_message", "Informations were deleted successfully");
            }
            $return['result'] = 1;
            /* redirect(site_url("back_office/products".$this->session->userdata("back_link")));	 */
            $return['redirect_link'] = site_url("back_office/products");
        } else {
            $return['result'] = 0;
            $return['redirect_link'] = site_url("home/dashboard");
        }
        die(json_encode($return));
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i + 1);
            $this->db->where("id_products", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function submit()
    {
        if ($this->acl->isCurrentUserASupplier()) {
            redirect('back_office/products/live_on_site');
        }

        $lang = "";
        /* $products=$this->fct->getAll('products','sort_order');
          foreach($products as $product){
          if(empty($product['brief_one'])){
          $product['brief_one']="";}
          if(empty($product['brief_two'])){
          $product['brief_two']="";}

          $brief=$product['brief_one'].' '.$product['brief_two'];
          $update['brief']=$brief;
          $this->db->where(array('id_products'=>$product['id_products']));
          $this->db->update('products',$update);

          } */
        $bool_supplier = false;
        if (checkIfSupplier_admin()) {
            $bool_supplier = true;
        }
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit products";

        $this->form_validation->set_rules("access_code", "Access Code", "trim");
        $this->form_validation->set_rules("dimensions", "dimensions", "trim");
        $this->form_validation->set_rules("stock_status_quantity_threshold", "stock_status_quantity_threshold", "trim");
        $this->form_validation->set_rules("stock_status", "Stock Status", "trim");
        $this->form_validation->set_rules("catalog", "catalog", "trim");
        $this->form_validation->set_rules("youtube", "youtube", "trim");
        $this->form_validation->set_rules("id_delivery_terms", "Delivery Terms", "trim");
        /* $this->form_validation->set_rules("categories", "categories", "trim|required"); */
        $this->form_validation->set_rules("id_brands", "brand", "trim");
        $this->form_validation->set_rules("id_group_categories", "Group Categories", "trim");
//$this->form_validation->set_rules("category", "CATEGORY", "trim");
        $this->form_validation->set_rules("sub_category", "SUB CATEGORY", "trim");
        $this->form_validation->set_rules("exclude_from_shipping_calculation", "exclude from shipping calculation", "trim");
        $this->form_validation->set_rules("delivery_day", "Delivery Day", "trim");
        $this->form_validation->set_rules("delivery_days", "DELIVERY DAY(S)", "trim|integer");
        $this->form_validation->set_rules("type", "TYPE", "trim");
        $this->form_validation->set_rules("title" . $lang, "TITLE", "trim|required");
        $this->form_validation->set_rules("fabric", "fabric", "trim");
        $this->form_validation->set_rules("location_code", "Location Code", "trim");
        $this->form_validation->set_rules("work", "work", "trim|max_length[65]");
        $this->form_validation->set_rules("washing_instructions", "washing instructions", "trim|max_length[450]");
        $this->form_validation->set_rules("meta_title" . $lang, "PAGE TITLE", "trim");
        $this->form_validation->set_rules("title_url" . $lang, "TITLE URL", "trim");
        $this->form_validation->set_rules("meta_description" . $lang, "META DESCRIPTION", "trim|max_length[450]");
        $this->form_validation->set_rules("meta_keywords" . $lang, "META KEYWORDS", "trim|max_length[450]");
        $this->form_validation->set_rules("sku", "sku", "trim|required|callback_validate_sku[]");
        $this->form_validation->set_rules("barcode", "barcode", "trim|callback_validate_barcode[]");

        $this->form_validation->set_rules("quantity", "quantity", "trim");
        /* $this->form_validation->set_rules("discount", "discount", "trim|integer"); */


        if ($this->input->post('set_as_clearance') == "" && !$bool_supplier) {
            $this->form_validation->set_rules("length", "length", "trim|required");
            //$this->form_validation->set_rules("height", "height", "trim|required");
            $this->form_validation->set_rules("width", "width", "trim|required");
            $this->form_validation->set_rules("weight", "weight", "trim|required");
        }


        $this->form_validation->set_rules("retail_price", "Retail Price", "trim");
        $this->form_validation->set_rules("list_price", "price", "trim|required");

        /* $this->form_validation->set_rules("brief_one", "weight", "trim");
          $this->form_validation->set_rules("brief_two", "weight", "trim"); */
        $this->form_validation->set_rules("brief", "brief", "trim");

        $this->form_validation->set_rules("discount", "discount", "trim");
        $this->form_validation->set_rules("price", "price", "trim");
        $this->form_validation->set_rules("description" . $lang, "description", "trim");
        $this->form_validation->set_rules("brief" . $lang, "brief", "trim|max_length[220]");
        $this->form_validation->set_rules("features" . $lang, "features", "trim");
        $this->form_validation->set_rules("specifications" . $lang, "specifications", "trim");
        $this->form_validation->set_rules("related_products" . $lang, "related_products", "trim");
        $this->form_validation->set_rules("downloads" . $lang, "downloads", "trim");
        $this->form_validation->set_rules("why_and_when" . $lang, "why_and_when", "trim");
        $this->form_validation->set_rules("video", "video", "trim");
        $this->form_validation->set_rules("status", "status", "trim");

////////////////////////////RETAIL PRICE////////////////////////////////////////
        $this->form_validation->set_rules("retail2_price", "dimensions", "trim");
        $this->form_validation->set_rules("retail_list_price", "dimensions", "trim");
        $this->form_validation->set_rules("retail_discount", "dimensions", "trim");
        $this->form_validation->set_rules("retail_discount_expiration", "discount expiration", "trim");

        $this->form_validation->set_rules("brand", "brand", "trim");
        $this->form_validation->set_rules("unit", "unit", "trim");
        $this->form_validation->set_rules("datasheet", "datasheet", "trim|callback_checkUploadFile[]");
        $this->form_validation->set_rules("tabs_files", "datasheet", "trim|callback_checkUploadFile2[]");
        if ($this->input->post("set_general_miles")) {
            $this->form_validation->set_rules("general_miles", "General Miles", "trim|required");
        }

        if ($this->input->post("set_as_redeemed_by_miles")) {
            $this->form_validation->set_rules("redeem_miles", " Amount of Miles", "trim|required");
        }


        if ($this->input->post("discount") != "" && $this->input->post("discount") != 0)
            $this->form_validation->set_rules("discount_expiration", "discount expiration" . "(" . $this->lang->lang() . ")", "trim");
        else
            $this->form_validation->set_rules("discount_expiration", "discount expiration" . "(" . $this->lang->lang() . ")", "trim");

        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $data['errors'] = '<div class="alert alert-error">' . validation_errors() . '</div>';

            if (isset($_POST['title_product_info'])) {
                $infomrations1 = $_POST['title_product_info'];
                $infomrations2 = $_POST['description_product_info'];
                $infomrations3 = $_POST['video_product_info'];
                $i = 0;
                $product_informations = array();
                foreach ($infomrations1 as $key => $val) {
                    $product_informations[$i]['id_product_information'] = $key;
                    $product_informations[$i]['title'] = $val;
                    $product_informations[$i]['description'] = $infomrations2[$key];
                    $product_informations[$i]['video'] = $infomrations3[$key];

                    $product_informations[$i]['file'] = $this->input->post('file_product_info_label_' . $key);
                    $product_informations[$i]['file2'] = $this->input->post('file2_product_info_label_' . $key);
                    $i++;
                }

                $data['product_informations'] = $product_informations;
            }

            /* if(isset($_POST['categories'])) {
              $data['selected_options'] = $_POST['categories'];
              } */
            if (isset($_POST['categories'])) {
                $data["h_selected_options"] = $this->input->post("categories");
            }

            if (isset($_POST['categories'])) {
                $data["set_as_special"] = $this->input->post("set_as_special");
            }

            if (isset($_POST['related_products_arr'])) {
                $data["h_selected_options2"] = $this->input->post("related_products_arr");
            }

            $data["set_as_clearance"] = $this->input->post("set_as_clearance");


            if (isset($_POST['set_general_miles'])) {
                $data["set_general_miles"] = $this->input->post("set_general_miles");
            }
            if (isset($_POST['set_as_redeemed_by_miles'])) {
                $data["set_as_redeemed_by_miles"] = $this->input->post("set_as_redeemed_by_miles");
            }

            if (isset($_POST['set_as_new'])) {
                $data["set_as_new"] = $this->input->post("set_as_new");
            }
            $data["set_as_trends_updates"] = $this->input->post("set_as_trends_updates");
            $data["set_as_soon"] = $this->input->post("set_as_soon");
            $data["set_as_pro"] = $this->input->post("set_as_pro");
            $data["promote_to_front"] = $this->input->post("promote_to_front");
            if ($this->input->post("id") != "") {

                if (isset($_POST['promote_to_front'])) {
                    $data["promote_to_front"] = $this->input->post("promote_to_front");
                }
                if (isset($_POST['display_in_home'])) {
                    $data["display_in_home"] = $this->input->post("display_in_home");
                }

                if (isset($_POST['exclude_from_shipping_calculation'])) {
                    $data["exclude_from_shipping_calculation"] = $this->input->post("exclude_from_shipping_calculation");
                }

                if (isset($_POST['hide_price'])) {
                    $data["hide_price"] = $this->input->post("hide_price");
                }

                $this->edit($this->input->post("id"), '', $data);
            } else {
                if (isset($_POST['display_in_home'])) {
                    $_data["display_in_home"] = $this->input->post("display_in_home");
                }

                if (isset($_POST['set_as_news'])) {
                    $data["set_as_news"] = $this->input->post("set_as_news");
                }
                if (isset($_POST['set_as_clearance'])) {
                    $data["set_as_clearance"] = $this->input->post("set_as_clearance");
                }
                if (isset($_POST['promote_to_front'])) {
                    $data["promote_to_front"] = $this->input->post("promote_to_front");
                }
                if (isset($_POST['hide_price'])) {
                    $data["hide_price"] = $this->input->post("hide_price");
                }


                $this->add($data);
            }
        } else {

            $user = $this->fct->getonerecord('user', array('id_user' => $this->session->userdata("uid")));

            if (checkIfSupplier_admin()) {
                $supplier = 1;
                //$_data["id_user"] = $this->session->userdata("uid");
                $_data["price"] = $this->input->post("list_price");
            } else {
                $_data["price"] = $this->input->post("price");
            }
            $_data["catalog"] = $this->input->post("catalog");
            $_data["list_price"] = $this->input->post("list_price");

            $_data["stock_status_quantity_threshold"] = $this->input->post("stock_status_quantity_threshold");
            $_data["set_as_special"] = $this->input->post("set_as_special");
            $_data['categories_comments'] = $this->input->post("categories_comments");
            $_data['attributes_comments'] = $this->input->post("attributes_comments");

            if (!checkIfSupplier_admin()) {


                $data["set_as_non_exportable"] = $this->input->post("set_as_non_exportable");
                $_data["promote_to_front"] = $this->input->post("promote_to_front");
                $_data["stock_status"] = $this->input->post("stock_status");
                $_data["display_in_home"] = $this->input->post("display_in_home");
                $_data["availability"] = $this->input->post("availability");

                $_data["set_as_trends_updates"] = $this->input->post("set_as_trends_updates");

                /* $_data["id_categories"]=$this->input->post("category"); */
                $_data["display_in_home"] = $this->input->post("display_in_home");
                $_data["redeem_miles"] = $this->input->post("redeem_miles");


                $_data["set_general_miles"] = $this->input->post("set_general_miles");
                $_data["general_miles"] = $this->input->post("general_miles");

                $_data["exclude_from_shipping_calculation"] = $this->input->post("exclude_from_shipping_calculation");

                if ($this->input->post("id_delivery_terms") == 4) {
                    $_data["exclude_from_shipping_calculation"] = 1;
                }
                $_data["set_as_redeemed_by_miles"] = $this->input->post("set_as_redeemed_by_miles");
                $_data["amount_of_miles"] = $this->input->post("amount_of_miles");
                if (isset($_POST['display_in_home'])) {
                    $_data["display_in_home"] = $this->input->post("display_in_home");
                }
                $_data["meta_title"] = $this->input->post("meta_title");
                if ($this->input->post("title_url") == "")
                    $title_url = $this->input->post("title");
                else
                    $title_url = $this->input->post("title_url");
                if ($this->input->post("lang") == 'ar') {
                    $this->load->model('title_url_ar');
                    $_data["title_url" . $lang] = $this->title_url_ar->cleanURL($this->table, $title_url, $this->input->post("id"), 'title_url' . $lang);
                } else {
                    $_data["title_url" . $lang] = $this->fct->cleanURL("products", url_title($title_url), $this->input->post("id"));
                }
                $_data["meta_description" . $lang] = $this->input->post("meta_description" . $lang);
                $_data["meta_keywords" . $lang] = $this->input->post("meta_keywords" . $lang);


                /* 	if($this->input->post('set_general_miles')==1 && $_data["general_miles"]>0){
                  $price_miles=($_data["general_miles"]*$_data["price"])/100;
                  $miles=round(($price_miles/10),0);
                  $_data["miles"] = $miles;} */


                $_data["miles"] = $this->input->post("miles");

                /* $_data["brief_one"]=$this->input->post("brief_one");
                  $_data["brief_two"]=$this->input->post("brief_two"); */
                $_data["id_group_categories"] = $this->input->post("id_group_categories");
                $_data["id_delivery_terms"] = $this->input->post("id_delivery_terms");
                $_data["id_miles"] = $this->input->post("id_miles");
                $_data["delivery_day"] = $this->input->post("delivery_day");


                $_data["discount_expiration"] = $this->fct->date_in_formate($this->input->post("discount_expiration"));
                $_data["retail_discount_expiration"] = $this->fct->date_in_formate($this->input->post("retail_discount_expiration"));

                $_data["location_code"] = $this->input->post("location_code");

                $_data["id_brands"] = $this->input->post("id_brands");

                $_data["weight"] = $this->input->post("weight");
                $_data["width"] = $this->input->post("width");
                $_data["height"] = $this->input->post("height");
                $_data["length"] = $this->input->post("length");
                $_data["dimensions"] = $this->input->post("dimensions");
                $_data["delivery_days"] = $this->input->post("delivery_days");
                $_data["access_code"] = $this->input->post("access_code");
            }


            $_data["retail_price"] = $this->input->post("retail_price");
            $_data["retail_list_price"] = $this->input->post("retail_list_price");
            $_data["retail2_price"] = $this->input->post("retail2_price");
            $_data["discount"] = $this->input->post("discount");
            $_data["retail_discount"] = $this->input->post("retail_discount");
//if($_data["discount"] != 0 && $_data["price"] != 0 && $_data["price"] == $_data["list_price"])
//$_data["price"] = round($_data["list_price"] - (($_data["list_price"] * 10) / 100));

            if ($_data["discount"] > 0) {
                $testprice = round($_data["list_price"] - (($_data["list_price"] * $_data["discount"]) / 100));
                if ($_data["price"] != $testprice) {
                    $_data["price"] = $testprice;
                }
            } else {
                $_data["price"] = $_data["list_price"];
            }

            if ($_data["list_price"] > $_data["price"]) {
                $_data["discount_status"] = 1;
                $_data["discount_active"] = 1;
            } else {
                $_data["discount_status"] = 0;
                $_data["discount_active"] = 0;
            }

///////////RETAIL////////////////////
            if ($_data["retail_discount"] > 0) {
                $testprice = round($_data["retail_list_price"] - (($_data["retail_list_price"] * $_data["retail_discount"]) / 100));
                if ($_data["retail2_price"] != $testprice) {
                    $_data["retail2_price"] = $testprice;
                }
            } else {
                $_data["retail2_price"] = $_data["retail_list_price"];
            }

            if ($_data["retail_list_price"] > $_data["retail2_price"]) {
                $_data["retail_discount_status"] = 1;
            } else {
                $_data["retail_discount_status"] = 0;
            }


            $_data["threshold"] = $this->input->post("threshold");

            $_data["account_type"] = $this->input->post("account_type");

            $_data["id_units"] = $this->input->post("unit");
            $_data["youtube"] = $this->input->post("youtube");
//$_data["id_types"]=$this->input->post("type");
            $_data["title"] = $this->input->post("title");
            $_data["brief"] = $this->input->post("brief");


            $_data["sku"] = $this->input->post("sku");
            $_data["barcode"] = $this->input->post("barcode");
//$_data["quantity"]=$this->input->post("quantity");


            $_data["description"] = $this->input->post("description");
            $_data["set_as_new"] = $this->input->post("set_as_new");
            $_data["set_as_clearance"] = $this->input->post("set_as_clearance");
            $_data["set_as_non_exportable"] = $this->input->post("set_as_non_exportable");
            $_data["set_as_soon"] = $this->input->post("set_as_soon");
            $_data["set_as_pro"] = $this->input->post("set_as_pro");


            if (!empty($_FILES["datasheet"]["name"])) {
                if ($this->input->post("id") != "") {
                    $cond_image = array("id_products" => $this->input->post("id"));
                    $old_image = $this->fct->getonecell("products", "datasheet", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/products/' . $old_image)) {
                        unlink("./uploads/products/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("datasheet", "products");
                $_data["datasheet"] = $image1;
            }

            if (!empty($_FILES["image"]["name"])) {
                if ($this->input->post("id") != "") {
                    $cond_image = array("id_products" => $this->input->post("id"));
                    $old_image = $this->fct->getonecell("products", "image", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/products/' . $old_image)) {
                        unlink("./uploads/products/" . $old_image);
                        $sumb_val1 = explode(",", "800x590", "515x570", "342x494", "342x494", '114x165');
                        foreach ($sumb_val1 as $key => $value) {
                            if (file_exists("./uploads/products/" . $value . "/" . $old_image)) {
                                unlink("./uploads/products/" . $value . "/" . $old_image);
                            }
                        }
                    }
                }
                $image1 = $this->fct->uploadImage("image", "products");
                $this->fct->createthumb($image1, "products", "800x590,515x570,342x494,114x165");
                $_data["image"] = $image1;
            }


            $_data["hide_price"] = 0;
            if (isset($_POST['hide_price'])) {
                $_data["hide_price"] = $this->input->post("hide_price");
            }

            if (checkIfSupplier_admin()) {
                $user = $this->ecommerce_model->getUserInfo();

                $_data["published"] = 1;
                $_data["status"] = 2;
            } else {
                $_data["status"] = $this->input->post("status");

                if ($_data['status'] == 0) {
                    $_data["published"] = 0;
                    $_data["inform_admin"] = 0;
                }
            }


            if ($this->input->post("id") != "") {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_products", $this->input->post("id"));
                $this->db->update($this->table, $_data);
                $new_id = $this->input->post("id");
                $new = 0;

                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {


                $_data["sort_order"] = 0;
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert($this->table, $_data);
                $new_id = $this->db->insert_id();
                $new = 1;
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }
//////////////////INFORM Client//////
            if (isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
                $product = $this->fct->getonerecord('products', array('id_products' => $new_id));
                /* $user_brand=$this->fct->getonerecord('user',array('id_brands'=>$_data['id_brands'])); */
                $supplier = $this->fct->getonerecord('user', array('id_user' => $product['id_user']));
                $this->load->model("send_emails");
                $this->send_emails->informProductStatusToClient($supplier, $product);
            }
//////////////////INFORM ADMIN//////
            $product = $this->fct->getonerecord('products', array('id_products' => $new_id));
//////////////////Threshold//////	
            $this->admin_fct->checkThreshold($product);

            $product['new'] = $new;
//$this->custom_fct->informProductModificationsToAdmin($product);
            $product = $this->fct->getonerecord('products', array('id_products' => $new_id));
            if (!isset($supplier)) {
                $categories = array();
                if (isset($_POST['categories']) && !empty($_POST['categories'])) {
                    $categories = $_POST['categories'];
                }

                if (isset($_POST['categories']) && !empty($_POST['categories'])) {
                    $categories_b2c = $_POST['categories_b2c'];
                    foreach ($categories_b2c as $val) {
                        array_push($categories, $val);
                    }
                }

                $this->fct->insert_product_categories($new_id, $categories);
            }
            if (!isset($supplier)) {
                $related_products = array();
                if (isset($_POST['related_products_arr']) && !empty($_POST['related_products_arr'])) {
                    $related_products = $_POST['related_products_arr'];
                }

                $this->fct->insert_product_related($new_id, $related_products);


                $sub_categories = array();
                if (isset($_POST['sub_categories']) && !empty($_POST['sub_categories'])) {
                    $sub_categories = $_POST['sub_categories'];
                }

                $this->fct->insert_product_sub_categories($new_id, $sub_categories);
            }


///////////////////////////////////////////Brand Training//////////////////////////////////////////	
            /* 	if(isset($_POST['classifications']) && !empty($_POST['classifications'])) {
              $classifications = $_POST['classifications'];
              }
              $this->products_m->insert_product_classifications($new_id,$classifications); */;
            if (isset($_POST['description_product_info']) && !empty($_POST['description_product_info']) && isset($_POST['title_product_info']) && !empty($_POST['title_product_info'])) {

                $infomrations1 = $_POST['title_product_info'];
                $infomrations2 = $_POST['description_product_info'];
                $infomrations3 = $_POST['video_product_info'];
                $this->products_m->insert_product_informations($new_id, $infomrations1, $infomrations2, $infomrations3);
            }


            if ($this->session->userdata("admin_redirect_url")) {
                if ($this->session->userdata("back_link") != "") {
                    $back_link = $this->session->userdata("back_link");
                    redirect($back_link);
                } else {
                    redirect(site_url("back_office/" . $this->table));
                }
            } else {
                if ($this->input->post('submit') == 'Save and Continue') {
                    redirect(site_url("back_office/" . $this->table . "/edit/" . $new_id));
                }
                if ($this->input->post('submit') == 'Save Changes') {
                    redirect(site_url("back_office/" . $this->table));
                }


                if ($this->input->post('submit') == 'Save and manage gallery') {
                    redirect(site_url('back_office/control/manage_gallery/products/' . $new_id));
                }
            }
        }
    }

    public function submit_attribute()
    {
        if (!isset($_GET['id_product']) || (isset($_GET['id_product']) && empty($_GET['id_product']))) {
            redirect(site_url('back_office/products'));
        } else {
            $data["title"] = "Add / Edit product attributes";
            $this->form_validation->set_rules("product", "product", "trim|required");
            $this->form_validation->set_rules("attribute", "attribute", "trim|required");
            if ($this->form_validation->run() == FALSE) {
                if ($this->input->post('id')) {
                    $this->edit_attribute($this->input->post('id'));
                } else {
                    $this->add_attribute();
                }
            } else {
                $_data["id_products"] = $this->input->post("product");
                $_data["id_attributes"] = $this->input->post("attribute");
                if ($this->input->post("id") != "") {
                    $_data["updated_date"] = date("Y-m-d h:i:s");
                    $this->db->where("id_product_attributes", $this->input->post("id"));
                    $this->db->update('product_attributes', $_data);
                    $new_id = $this->input->post("id");
                    $this->session->set_userdata("success_message", "Information was updated successfully");
                } else {
                    $_data["created_date"] = date("Y-m-d h:i:s");
                    $this->db->insert('product_attributes', $_data);
                    $new_id = $this->db->insert_id();
                    $this->session->set_userdata("success_message", "Information was inserted successfully");
                }
                redirect(site_url('back_office/products/attributes') . "?id_product=" . $_GET['id_product']);
            }
        }
    }

    public function delete_file()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/products/" . $image)) {
            unlink("./uploads/products/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'products')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/products/" . $value . "/" . $image)) {
                    unlink("./uploads/products/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_products", $id);
        $this->db->update("products", $_data);
        echo 'Done!';
    }

//////////////////////////////////////////////////////////////////////
    public function attributes()
    {
        if ($this->acl->has_permission('product_attributes', 'index')) {
            if (isset($_GET['id_product']) && !empty($_GET['id_product'])) {
                $order = "sort_order";
                $data["title"] = "List Product Attributes";
                $data["content"] = "back_office/products/attributes";
                $data['id_product'] = $_GET['id_product'];
                $data['product'] = $this->fct->getonerow('products', array('id_products' => $data['id_product']));
                $data['selected_product_attributes'] = $this->fct->getNotSelectedAttributes_byProduct($data['id_product']);
                $data['product_attributes'] = $this->fct->getSelectedAttributes_byProduct($data['id_product']);
                $this->load->view("back_office/template", $data);
            } else {
                redirect(site_url("back_office/products"));
            }
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function add_attribute()
    {
        if ($this->acl->has_permission('product_attributes', 'add')) {
            $data["title"] = "Add Product Attribute";
            $data["content"] = "back_office/products/add_attribute";
            $data['id_product'] = $_GET['id_product'];
            $data['product'] = $this->fct->getonerow('products', array('id_products' => $data['id_product']));
            $data['product_attributes'] = $this->fct->getNotSelectedAttributes_byProduct($data['id_product']);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function edit_attribute($id)
    {
        if ($this->acl->has_permission('product_attributes', 'add')) {
            $data["title"] = "Add Product Attribute";
            $data["content"] = "back_office/products/add_attribute";
            $data['id_product'] = $_GET['id_product'];
            $data['product'] = $this->fct->getonerow('products', array('id_products' => $data['id_product']));
            $data['product_attributes'] = $this->fct->getAll('attributes', 'sort_order');
            $data['id'] = $id;
            $data['info'] = $this->fct->getonerow('product_attributes', array('id_product_attributes' => $id));
//print '<pre>';print_r($data['product_attributes']);exit;
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_attribute($id)
    {
        $cond = array('id_product_attributes' => $id);
        $product_attribute = $this->fct->getonerow('product_attributes', $cond);
        $id_product = $product_attribute['id_products'];
        $delete_cond_2 = array(
            'id_products' => $id_product,
            'id_attributes' => $product_attribute['id_attributes']
        );
// delete from product attributes table
        $this->db->delete('product_attributes', $cond);
// delete from product options table
        $this->db->delete('product_options', $delete_cond_2);
        $this->session->set_userdata("success_message", "Attribute is deleted successfully");
        redirect(site_url('back_office/products/attributes') . '?id_product=' . $id_product);
    }

    public function options()
    {
        if ($this->acl->has_permission('product_options', 'index')) {
            if (isset($_GET['id_product']) && !empty($_GET['id_product']) && isset($_GET['id_attribute']) && !empty($_GET['id_attribute'])) {
                $order = "sort_order";
                $data["title"] = "List Product Options";
                $data["content"] = "back_office/products/options";
                $data['id_product'] = $_GET['id_product'];
                $data['id_attribute'] = $_GET['id_attribute'];
                $data['product'] = $this->fct->getonerow('products', array('id_products' => $data['id_product']));
                $data['attribute'] = $this->fct->getonerow('attributes', array('id_attributes' => $data['id_attribute']));
                $data['product_options'] = $this->ecommerce_model->getAllOptions_byProductAttribute($data['id_product'], $data['id_attribute']);
//print '<pre>';print_r($data['product_options']);exit;
                $this->load->view("back_office/template", $data);
            } else {
                redirect(site_url("back_office/products"));
            }
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function update_options()
    {
//print '<pre>';print_r($_POST);exit;
        $id_product = $this->input->post('id_product');
        $id_attribute = $this->input->post('id_attribute');
        $product = $this->fct->getonerow('products', array('id_products' => $id_product));
//print_r($product);exit;
        $options_ids = $this->input->post('checklist_ids');
        $cehcklist = array();
        if (isset($_POST['cehcklist']))
            $cehcklist = $this->input->post('cehcklist');;
        $product_option_sku = $this->input->post('product_option_sku');
        $product_option_qty = $this->input->post('product_option_qty');
        $product_option_price = $this->input->post('product_option_price');


        foreach ($options_ids as $key => $oid) {
            $insert = array();
            $update = array();
            $cond = array();

            if (!empty($product_option_sku[$key]))
                $sku = $product_option_sku[$key];
            else
                $sku = $product['sku'];

            if (!empty($product_option_qty[$key]))
                $qty = $product_option_qty[$key];
            else
                $qty = $product['quantity'];

            if (!empty($product_option_price[$key]))
                $price = $product_option_price[$key];
            else
                $price = $product['price'];


            if (isset($cehcklist[$oid]) && $cehcklist[$oid] == 1) {
                $status = 1;
            } else {
                $status = 0;
            }
            $cond = array(
                'id_products' => $id_product,
                'id_attributes' => $id_attribute,
                'id_attribute_options' => $oid
            );
            $check = $this->fct->getonerow('product_options', $cond);
            if (empty($check)) {
                $insert = $cond;
                $insert['status'] = $status;
                $insert['sku'] = $sku;
                $insert['quantity'] = $qty;
                $insert['price'] = $price;
                $insert['created_date'] = date('Y-m-d h:i:s');
                $this->db->insert('product_options', $insert);
            } else {
                $update['status'] = $status;
                $update['sku'] = $sku;
                $update['quantity'] = $qty;
                $update['price'] = $price;
                $update['updated_date'] = date('Y-m-d h:i:s');
                $this->db->update('product_options', $update, $cond);
            }
        }
        $this->session->set_userdata("success_message", "Options are updated successfully");
        redirect(site_url('back_office/products/options?id_product=' . $id_product . '&id_attribute=' . $id_attribute));
    }

    public function stock($id_products = '')
    {
//echo $id_products;exit;
        if ($this->acl->has_permission('products', 'index')) {
            if ($id_products != '') {
                $order = "sort_order";
                $data["title"] = "List Product Stock";
                $data["content"] = "back_office/products/stock";
                $data['id_product'] = $id_products;
                $data['product'] = $this->fct->getOneProduct($data['id_product']);


                $attributes = $data['product']['attributes'];

                /* print '<pre>';
                  print_r($attributes);
                  exit; */
                $data['stock'] = array();
                if (!empty($attributes))
                    $data['stock'] = PrepareAttributes($attributes, $attributes[0]['options'], count($attributes));
//print '<pre>';print_r( $data['stock'] );exit;
                $this->load->view("back_office/template", $data);
            } else {
                redirect(site_url("back_office/products"));
            }
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function update_stock()
    {
        $sku_arr = $this->input->post('sku');
        $barcode_arr = $this->input->post('barcode');
        $miles_arr = $this->input->post('miles');


        $cehcklist_ids = $this->input->post('cehcklist_ids');
        $id_product = $this->input->post('id_product');
        $error = "";
        $bool = true;
        $current_sku = array();
        $current_barcode = array();
        foreach ($cehcklist_ids as $key => $val) {
            $sku = $sku_arr[$key];
            $barcode = $barcode_arr[$key];

            $id = $val;
            $cond = array('sku' => $sku);


//////////////////////VALIDATE BARCODE//////////////
            if (!empty($sku)) {
                if (!empty($check) || in_array($sku, $current_sku)) {
                    $id_1 = $id . '-' . $id_product;
                    $id_2 = $check['combination_id'] . '-' . $check['id_products'];
                    if ($id_1 != $id_2 || in_array($sku, $current_sku)) {
                        $bool = false;
                        $error .= $sku . " exists, please use another SKU value </br>";
                    }
                    array_push($current_sku, $sku);
                }
            } else {
                $bool = false;
                $error .= " All sku fields are required. </br>";
            }

//////////////////////VALIDATE BARCODE//////////////
            $cond2 = array('barcode' => $barcode);
            $check2 = $this->fct->getonerecord('products_stock', $cond2);
            if (!empty($barcode) && !empty($check2) || in_array($barcode, $current_barcode)) {

                $id_1 = $id . '-' . $id_product;
                $id_2 = $check2['combination_id'] . '-' . $check2['id_products'];
                if ($id_1 != $id_2 || in_array($barcode, $current_barcode)) {
                    $bool = false;
                    $error .= $barcode . " exists, please use another barcode value </br>";
                }
                array_push($current_barcode, $barcode);
            }
        }


        if ($bool) {
//print '<pre>';print_r($_POST);exit;

            $id_attribute = $this->input->post('id_attribute');
            $product = $this->fct->getonerow('products', array('id_products' => $id_product));
            $old_stock = $this->fct->getAll_cond('products_stock', 'id_products_stock', array('id_products' => $id_product));
            $new_ids = array();
            $options_ids = $this->input->post('checklist_ids');
            $cehcklist = array();
            if (isset($_POST['cehcklist']))
                $cehcklist = $this->input->post('cehcklist');
            $cehcklist_ids = $this->input->post('cehcklist_ids');
            $arr_items = $this->input->post('items');
            $arr_sku = $this->input->post('sku');
            $arr_barcode = $this->input->post('barcode');
            $arr_threshold = $this->input->post('threshold');
            $arr_retail_price = $this->input->post('retail_price');


            $arr_quantity = $this->input->post('quantity');
            $arr_price = $this->input->post('price');

            $arr_set_general_miles = $this->input->post('set_general_miles');
            $arr_general_miles = $this->input->post('general_miles');

            $arr_set_as_redeemed_by_miles = $this->input->post('set_as_redeemed_by_miles');
            $arr_redeem_miles = $this->input->post('redeem_miles');


            $arr_list_price = $this->input->post('list_price');
            $arr_discount = $this->input->post('discount');
            $arr_discount_expiration = $this->input->post('discount_expiration');
            $arr_show_hide_prices = $this->input->post('hide_price');

            $arr_retail2_price = $this->input->post('retail2_price');
            $arr_retail_list_price = $this->input->post('retail_list_price');
            $arr_retail_discount = $this->input->post('retail_discount');
            $arr_retail_discount_expiration = $this->input->post('retail_discount_expiration');


            foreach ($cehcklist_ids as $key => $val) {
                $insert = array();
                $update = array();
                $cond = array();

                $sku = $arr_sku[$key];
                $miles = $miles_arr[$key];

                $barcode = $arr_barcode[$key];
                $retail_price = $arr_retail_price[$key];

                $qty = $arr_quantity[$key];
                $threshold = $arr_threshold[$key];
                $list_price = $arr_list_price[$key];
                $retail_list_price = $arr_retail_list_price[$key];

                $discount = $arr_discount[$key];
                $retail_discount = $arr_retail_discount[$key];
                if (isset($arr_set_general_miles[$key])) {
                    $set_general_miles = $arr_set_general_miles[$key];
                } else {
                    $set_general_miles = 0;
                }
                $general_miles = $arr_general_miles[$key];
                if (isset($arr_set_as_redeemed_by_miles[$key])) {
                    $set_as_redeemed_by_miles = $arr_set_as_redeemed_by_miles[$key];
                } else {
                    $set_as_redeemed_by_miles = 0;
                }
                $redeem_miles = $arr_redeem_miles[$key];


                $price = $arr_price[$key];
                $retail2_price = $arr_retail2_price[$key];


                $hide_price = $arr_show_hide_prices[$key];

//$hide_price = 0;
//if(isset($))

                if ($discount > 0) {
                    $testprice = round($list_price - (($list_price * $discount) / 100));
                    if ($price != $testprice) {
                        $price = $testprice;
                    }
                } else {
                    $price = $list_price;
                }

                if ($retail_discount > 0) {
                    $testprice = round($retail_list_price - (($retail_list_price * $retail_discount) / 100));
                    if ($retail_price != $testprice) {
                        $retail_price = $testprice;
                    }
                } else {
                    /* $retail_price = $retail_list_price; */
                    /* $retail_price = $retail_list_price; */
                }

//if($discount > 0 && $price != $list_price)
//$price = round($list_price - (($list_price * $discount) / 100),2);
//if($price1 != $price) $price = $price1;

                $discount_expiration = $arr_discount_expiration[$key];
                $retail_discount_expiration = $arr_retail_discount_expiration[$key];

                $item = $arr_items[$key];

                $id = $val;
                array_push($new_ids, $id);

                $cond = array(
                    'id_products' => $id_product,
                    'combination_id' => $id
                );


                /* 	if($set_general_miles==1 && $general_miles>0){
                  $price_miles=($general_miles*$price)/100;
                  $miles=round(($price_miles/10),0);
                  } */

                if (isset($cehcklist[$id]) && $cehcklist[$id] == 1) {
                    $status = 1;
                } else {
                    $status = 0;
                }
// let us make it checked all the time, the admin will set the quantity to 0 to disable it
                $status = 1;
//	print '<pre>';print_r($cond);echo '<br />';
                $check = $this->fct->getonerow('products_stock', $cond);
//print '<pre>';print_r($check);echo '<br />';
                if (empty($check)) {
                    $insert = $cond;
                    $insert['status'] = $status;
                    //$insert['id_user'] = $this->session->userdata('uid');
                    $insert['lang'] = $this->lang->lang();
                    $insert['id_products'] = $id_product;
                    $insert['combination'] = $item;
                    $insert['sku'] = $sku;
                    $insert['barcode'] = $barcode;
                    $insert['quantity'] = $qty;
                    $insert['threshold'] = $threshold;


                    $insert["set_general_miles"] = $set_general_miles;
                    $insert["general_miles"] = $general_miles;
                    $insert["set_as_redeemed_by_miles"] = $set_as_redeemed_by_miles;
                    $insert["redeem_miles"] = $redeem_miles;
                    $insert["miles"] = $miles;

                    $insert['retail_price'] = $retail_price;

                    /* $insert['id_miles'] = $mile; */
                    $insert['list_price'] = $list_price;
                    $insert['retail_list_price'] = $retail_list_price;
                    $insert['price'] = $price;
                    $insert['retail2_price'] = $retail2_price;
                    if ($insert["list_price"] > $insert["price"]) {
                        $insert["discount_status"] = 1;
                        $insert["discount_active"] = 0;
                    } else {
                        $insert["discount_status"] = 0;
                        $insert["discount_active"] = 0;
                    }

                    if ($insert["retail_list_price"] > $insert["retail2_price"]) {
                        $insert["retail_discount_status"] = 1;
                    } else {
                        $insert["retail_discount_status"] = 0;
                    }

                    $insert['discount'] = $discount;
                    $insert['retail_discount'] = $retail_discount;
                    $insert['hide_price'] = $hide_price;
                    $insert['discount_expiration'] = $this->fct->date_in_formate($discount_expiration);
                    $insert['retail_discount_expiration'] = $this->fct->date_in_formate($retail_discount_expiration);
                    $insert['created_date'] = date('Y-m-d h:i:s');
                    $insert['updated_date'] = date('Y-m-d h:i:s');
                    $this->db->insert('products_stock', $insert);
                    $new_id = $this->db->insert_id();
                    $stock = $this->fct->getonerecord('products_stock', array('id_products_stock' => $new_id));
                } else {

                    $update['status'] = $status;
                    //$update['id_user'] = $this->session->userdata('uid');
                    $update['lang'] = $this->lang->lang();
                    $update['sku'] = $sku;
                    $update['barcode'] = $barcode;
                    $update['quantity'] = $qty;
                    $update['price'] = $price;
                    $update['retail_price'] = $price;
                    $update['updated_date'] = date('Y-m-d h:i:s');
                    $update['threshold'] = $threshold;
                    /* $update['id_miles'] = $mile; */
                    $update["set_general_miles"] = $set_general_miles;
                    $update["general_miles"] = $general_miles;
                    $update["set_as_redeemed_by_miles"] = $set_as_redeemed_by_miles;
                    $update["redeem_miles"] = $redeem_miles;
                    $update["miles"] = $miles;


                    $update['retail_price'] = $retail_price;
                    $update['retail2_price'] = $retail2_price;
                    $update['list_price'] = $list_price;
                    $update['retail_list_price'] = $retail_list_price;
                    if ($update["list_price"] > $update["price"]) {
                        $update["discount_status"] = 1;
                        $update["discount_active"] = 1;
                    } else {
                        $update["discount_status"] = 0;
                        $update["discount_active"] = 0;
                    }

                    if ($update["retail_list_price"] > $update["retail2_price"]) {
                        $update["retail_discount_status"] = 1;
                    } else {
                        $update["retail_discount_status"] = 0;
                    }

                    $update['discount'] = $discount;
                    $update['retail_discount'] = $retail_discount;
                    $update['hide_price'] = $hide_price;
                    $update['discount_expiration'] = $this->fct->date_in_formate($discount_expiration);
                    $update['retail_discount_expiration'] = $this->fct->date_in_formate($retail_discount_expiration);


                    $update['updated_date'] = date('Y-m-d h:i:s');
                    $this->db->update('products_stock', $update, $cond);


                    $stock = $this->fct->getonerecord('products_stock', $cond);
                    /* echo $this->db->last_query().'<br />';exit; */
                }

//////////////////Threshold//////	
                $this->admin_fct->checkThreshold($stock);
//  delete old stock
            }
            foreach ($old_stock as $ostk) {
                $oid = $ostk['combination_id'];
                if (!in_array($oid, $new_ids)) {
                    $cnd1 = array(
                        'combination_id' => $oid,
                        'id_products' => $id_product
                    );
                    $this->db->delete('products_stock', $cnd1);
                }
            }


//////////////////INFORM ADMIN//////	
            $this->custom_fct->informProductModificationsToAdmin($product);
            $this->session->set_userdata("success_message", "Stock is updated successfully");
        } else {
            $this->session->set_userdata("error_message", $error);
        }


        redirect(site_url('back_office/products/stock/' . $id_product));
    }

////////////////////////////////////////////////////////////////////////////////////
    public function loadsubcategories()
    {
        $id_cat = $this->input->post('id_cat');
        $sub_categories = $this->fct->getSubCategoriesByCategory($id_cat);
        $content = '<option value="">Select Sub Category</option>';
        if (!empty($sub_categories)) {
            foreach ($sub_categories as $sub_category) {
                $options[$sub_category['id_categories_sub']] = $sub_category['title'];
                $content .= '<option value="' . $sub_category['id_categories_sub'] . '">' . $sub_category['title'] . '</option>';
            }
        }
        echo $content;
    }

    public function loadtypes()
    {
        $id_cat = $this->input->post('id_cat');
        $types = $this->fct->getTypesByCategory($id_cat);
        $content = '<option value="">Select Type</option>';
        if (!empty($types)) {
            foreach ($types as $type) {
                $options[$type['id_types']] = $type['title'];
                $content .= '<option value="' . $type['id_types'] . '">' . $type['title'] . '</option>';
            }
        }
        echo $content;
    }

////////////////////////////////////////////////////////////////////////
    public function getQuantity($section, $id)
    {
        /* $id = $this->input->post('id'); */

        if ($section == "stock") {
            $quantity = $this->fct->getonecell('products_stock', 'quantity', array('id_products_stock' => $id));
        } else {
            $quantity = $this->fct->getonecell('products', 'quantity', array('id_products' => $id));
        }


        die($quantity);
    }

////////////////////////////////////////////////////////////////////////
    public function stock_price_segments($id_products, $id_stock = 0)
    {
        $data['price_segments'] = $this->fct->getAll_cond('product_price_segments', 'sort_order', array('id_products' => $id_products, 'id_stock' => $id_stock));
        $data['stock'] = $this->fct->getonerow('products_stock', array('id_products_stock' => $id_stock));
        $data['product'] = $this->fct->getonerow('products', array('id_products' => $id_products));
        $data['id_products'] = $id_products;
        $data['id_stock'] = $id_stock;
        $this->load->view('back_office/products/stock_price_segments', $data);
    }

    public function add_more_stock_price_segments($id_products, $id_stock)
    {
        $data['id_products'] = $id_products;
        $data['id_stock'] = $id_stock;
        $data['stock'] = $this->fct->getonerow('products_stock', array('id_products_stock' => $id_stock));
        $data['product'] = $this->fct->getonerow('products', array('id_products' => $id_products));
        $this->load->view('back_office/products/stock_price_segments_add_more', $data);
    }

    public function edit_stock_price_segments($id, $id_products, $id_stock)
    {
        $data['id_products'] = $id_products;
        $data['id_stock'] = $id_stock;
        $data['id'] = $id;
        $data['stock'] = $this->fct->getonerow('products_stock', array('id_products_stock' => $id_stock));
        $data['product'] = $this->fct->getonerow('products', array('id_products' => $id_products));
        $data['info'] = $this->fct->getonerow('product_price_segments', array('id_product_price_segments' => $id));
        $this->load->view('back_office/products/stock_price_segments_add_more', $data);
    }

    public function submit_stock_price_segments()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $id_products = $this->input->post('id_products');
        $id_stock = $this->input->post('id_stock');
        $data['id_products'] = $id_products;
        $data['id_stock'] = $id_stock;
        $data['stock'] = $this->fct->getonerow('products_stock', array('id_products_stock' => $id_stock));
        $data['product'] = $this->fct->getonerow('products', array('id_products' => $id_products));
        /* $this->form_validation->set_rules("title".$lang, "TITLE", "trim|required"); */
        $this->form_validation->set_rules("min_qty", "min qty" . "(" . $this->lang->lang() . ")", "trim|required");
        $this->form_validation->set_rules("price", "price" . "(" . $this->lang->lang() . ")", "trim|required");
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('back_office/products/stock_price_segments_add_more', $data);
        } else {
            $_data["id_products"] = $id_products;
            $_data["id_stock"] = $id_stock;
            //$_data["id_user"] = $this->session->userdata("uid");
            $_data["title" . $lang] = $this->input->post("title" . $lang);
            $_data["min_qty"] = $this->input->post("min_qty");
            $_data["price"] = $this->input->post("price");

            if ($this->input->post("id") != "") {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_product_price_segments", $this->input->post("id"));
                $this->db->update("product_price_segments", $_data);
                $new_id = $this->input->post("id");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert("product_price_segments", $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }

//////////////////INFORM ADMIN//////	
            $product = $this->fct->getonerecord('products', array('id_products' => $id_products));
            $this->custom_fct->informProductModificationsToAdmin($product);
            redirect(site_url("back_office/products/stock_price_segments/" . $id_products . '/' . $id_stock));
        }
    }

    public function delete_stock_price_segment($id, $id_products, $id_stock)
    {
        if ($this->acl->has_permission('product_price_segments', 'delete') && !empty($id)) {
            $this->db->where("id_product_price_segments", $id);
            $this->db->delete("product_price_segments");

            $this->session->set_userdata("success_message", "Informations is deleted successfully");
        }
        redirect(site_url('back_office/products/stock_price_segments/' . $id_products . '/' . $id_stock));
    }

    public function delete_all_stock_price_segments()
    {
        if ($this->acl->has_permission('product_price_segments', 'delete_all')) {
            $id_products = $this->input->post("id_products");
            $id_stock = $this->input->post("id_stock");
            $cehcklist = $this->input->post("cehcklist");
            $check_option = $this->input->post("check_option");
            if ($check_option == "delete_all") {
                if (count($cehcklist) > 0) {
                    for ($i = 0; $i < count($cehcklist); $i++) {
                        if ($cehcklist[$i] != "") {
                            $this->db->where("id_product_price_segments", $cehcklist[$i]);
                            $this->db->delete("product_price_segments");
                        }
                    }
                }
                $this->session->set_userdata("success_message", "Informations were deleted successfully");
            }
        }
        redirect(site_url('back_office/products/stock_price_segments/' . $id_products . '/' . $id_stock));
    }

///////////////////////////////////////Add Purchases////////////////
    public function purchases($order = "")
    {

        $this->session->unset_userdata("admin_redirect_url");
        if ($this->acl->has_permission('products', 'index')) {
            if ($order == "")
                $order = "sort_order";
            $data["title"] = "List purchases";
            $data["content"] = "back_office/products/list_purchases";


            $this->load->model('purchases_m');
//
            $this->session->unset_userdata("back_link");
//
            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "25";
            }
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
            $data["show_items"] = $show_items;
// pagination  start :

            if ($this->input->get('id_stock')) {
                $cond['id_products'] = $this->input->get('id_stock');
                $data['id_stock'] = $this->input->get('id_stock');
                $cond['type'] = 'stock';
                $data['type'] = $cond['type'];
                $data['product'] = $this->fct->getonerecord('products_stock', array('id_products_stock' => $this->input->get('id_stock')));
                $data['sku'] = $data['product']['sku'];
                $url = site_url("back_office/products/purchases") . '?id_stock=' . $this->input->get('id_product');
            }

            if ($this->input->get('id_product')) {
                $cond['id_products'] = $this->input->get('id_product');
                $cond['type'] = 'product';
                $data['type'] = $cond['type'];
                $data['id_stock'] = "";
                $data['product'] = $this->fct->getonerecord('products', array('id_products' => $this->input->get('id_product')));
                $data['sku'] = $data['product']['sku'];
                $url = site_url("back_office/products/purchases") . '?id_product=' . $this->input->get('id_product');
            }

            $data['id_products'] = $data['product']['id_products'];
            $count_news = $this->purchases_m->getAll('purchases', $order, $cond);

            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $this->load->library('pagination');
            $config['base_url'] = $url;
            $config['total_rows'] = $count_news;
            $config['per_page'] = $show_items;
            $config['uri_segment'] = 5;
            $this->pagination->initialize($config);
            $data['info'] = $this->purchases_m->list_paginate($order, $config['per_page'], $this->uri->segment(5), $cond);


// end pagination .
            $this->load->view("back_office/products/list_purchases", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_row()
    {
        $field = $this->input->post('field');
        $table = $this->input->post('section');
        $id = $this->input->post('id');


        $_data['deleted'] = 1;
        $this->db->where("id_" . $table, $id);
        $this->db->update($table, $_data);
        echo 'Done!';
    }

    public function add_purchases($data = array())
    {


        if ($this->acl->has_permission('purchases', 'add')) {
            $data["title"] = "Add Purchases";
            $data["content"] = "back_office/products/add_purchases";

            if ($this->input->get('id_product') || isset($data['id_products'])) {

                if ($this->input->get('id_product') != "") {
                    $data['id_product'] = $this->input->get('id_product');
                } else {
                    $data['id_product'] = $data['id_products'];
                }
                $data['product'] = $this->fct->getonerow('products', array('id_products' => $data['id_product']));
                $data['type'] = 'product';
                $data['sku'] = $data['product']['sku'];
            }
            if ($this->input->get('id_stock')) {
                $data['id_stock'] = $this->input->get('id_stock');
                $data['type'] = 'stock';
                $data['product_stock'] = $this->fct->getonerecord('products_stock', array('id_products_stock' => $data['id_stock']));
                $data['sku'] = $data['product_stock']['sku'];
            }


            $this->load->view("back_office/products/add_purchases", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function edit_purchases($id, $obj = '', $data = array())
    {
        if ($this->acl->has_permission('product_attributes', 'add')) {
            $data["title"] = "Add Product Attribute";
            $data["content"] = "back_office/products/add_purchases";

            if ($this->input->get('id_product')) {
                $data['id_product'] = $this->input->get('id_product');
                $data['product'] = $this->fct->getonerow('products', array('id_products' => $data['id_product']));
                $data['type'] = 'product';
                $data['sku'] = $data['product']['sku'];
            }
            if ($this->input->get('id_stock')) {
                $data['id_stock'] = $this->input->get('id_stock');
                $data['type'] = 'stock';
                $data['product_stock'] = $this->fct->getonerecord('products_stock', array('id_products_stock' => $data['id_stock']));
                $data['sku'] = $data['product_stock']['sku'];
            }


            $data['id'] = $id;
            $data['info'] = $this->fct->getonerow('purchases', array('id_purchases' => $id));

//print '<pre>';print_r($data['product_attributes']);exit;
            $this->load->view("back_office/products/add_purchases", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function submitPurchases()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit purchases";

        $this->form_validation->set_rules("meta_title" . $lang, "PAGE TITLE", "trim|max_length[65]");

        $this->form_validation->set_rules("meta_description" . $lang, "META DESCRIPTION", "trim|max_length[160]");
        $this->form_validation->set_rules("meta_keywords" . $lang, "META KEYWORDS", "trim|max_length[160]");
        $this->form_validation->set_rules("id_products", "id_products" . "(" . $this->lang->lang() . ")", "trim");
        $this->form_validation->set_rules("quantity", "quantity" . "(" . $this->lang->lang() . ")", "trim|required");
        $this->form_validation->set_rules("price", "price" . "(" . $this->lang->lang() . ")", "trim|required");
        $this->form_validation->set_rules("cost", "cost" . "(" . $this->lang->lang() . ")", "trim|required");
        $this->form_validation->set_rules("id_user", "id_user" . "(" . $this->lang->lang() . ")", "trim");
        if ($this->form_validation->run() == FALSE) {
            if ($this->input->post('type') == "product") {
                $data['id_product'] = $this->input->post('id_products');
                $data['product'] = $this->fct->getonerow('products', array('id_products' => $data['id_product']));
                $data['type'] = 'product';
                $data['sku'] = $data['product']['sku'];
            }
            if ($this->input->post('type') == "stock") {
                $data['id_products'] = $this->input->post('id_products');
                $data['type'] = 'stock';
                $data['product_stock'] = $this->fct->getonerecord('products_stock', array('id_products_stock' => $data['id_products']));
                $data['sku'] = $data['product_stock']['sku'];
            }


            if ($this->input->post("id") != "") {
                $this->edit_purchases($this->input->post("id"), '', $data);
            } else {

                $this->add_purchases($data);
            }
        } else {
            $_data["id_user"] = $this->session->userdata("uid");
            $_data["title" . $lang] = $this->input->post("title" . $lang);
            $_data["meta_title" . $lang] = $this->input->post("meta_title" . $lang);
            if ($this->input->post("title_url" . $lang) == "")
                $title_url = $this->input->post("title" . $lang);
            else
                $title_url = $this->input->post("title_url" . $lang);
            if ($this->input->post("lang") == "ar") {
                $this->load->model("title_url_ar");
                $_data["title_url" . $lang] = $this->title_url_ar->cleanURL($this->table, $title_url, $this->input->post("id"), "title_url" . $lang);
            } else {
                $_data["title_url" . $lang] = $this->fct->cleanURL($this->table, url_title($title_url), $this->input->post("id"));
            }

            $_data["meta_description" . $lang] = $this->input->post("meta_description" . $lang);
            $_data["meta_keywords" . $lang] = $this->input->post("meta_keywords" . $lang);
            $_data["id_products"] = $this->input->post("id_products");
            $_data["quantity"] = $this->input->post("quantity");
            $_data["price"] = $this->input->post("price");
            $_data["status"] = $this->input->post("status");
            $_data["type"] = $this->input->post("type");
            $_data["sku"] = $this->input->post("sku");
            $_data["cost"] = $this->input->post("cost");
            $_data["id_user"] = $this->input->post("id_user");


            $delete_qty = 0;
            $delete_price = 0;

            if ($this->input->post("id") != "") {
                $purchases = $this->fct->getonerecord('purchases', array('id_purchases' => $this->input->post("id")));
                $delete_qty = $purchases['quantity'];
                $delete_price = $purchases['price'];
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_purchases", $this->input->post("id"));
                $this->db->update('purchases', $_data);
                $new_id = $this->input->post("id");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert('purchases', $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }
//////////UPDATE PRODUCT//////////////////////////
            if ($_data['type'] == "product") {
                $product = $this->fct->getonerecord('products', array('id_products' => $this->input->post("id_products")));
                $update_product['quantity'] = $product['quantity'] + $_data["quantity"] - $delete_qty;
                $this->db->where("id_products", $_data["id_products"]);
                $this->db->update('products', $update_product);
            }

            if ($_data['type'] == "stock") {
                $product = $this->fct->getonerecord('products_stock', array('id_products_stock' => $this->input->post("id_products")));
                $update_product['quantity'] = $product['quantity'] + $_data["quantity"] - $delete_qty;

                $this->db->where("id_products_stock", $_data["id_products"]);
                $this->db->update('products_stock', $update_product);
            }

            if ($_data['type'] == "stock") {
                $product_stock = $this->fct->getonerecord('products_stock', array('id_products_stock' => $this->input->post("id_products")));
                $product = $this->fct->getonerecord('products', array('id_products' => $product_stock['id_products']));
            }

            if ($_data['type'] == "product") {
                $product = $this->fct->getonerecord('products', array('id_products' => $this->input->post("id_products")));
            }
//////////////////INFORM ADMIN//////	
            $this->custom_fct->informProductModificationsToAdmin($product);

            if ($this->session->userdata("admin_redirect_url")) {
                redirect($this->session->userdata("admin_redirect_url"));
            } else {

                if ($_data["type"] == "product") {
                    $link = site_url('back_office/products/purchases?id_product=' . $_data['id_products']);
                } else {
                    $link = site_url('back_office/products/purchases?id_stock=' . $_data['id_products']);
                }
                redirect($link);
            }
        }
    }

    public function delete_purchases($id)
    {
        $cond = array('id_purchases' => $id);
        $purchase = $this->fct->getonerow('purchases', $cond);
        $id_product = $purchase['id_products'];
        $delete_cond_2 = array(
            'id_purchases' => $purchase['id_purchases']
        );


// delete from product options table

        if ($purchase['type'] == "product") {
            $product = $this->fct->getonerecord('products', array('id_products' => $purchase["id_products"]));
            $id_pro = $product['id_products'];
        } else {
            if ($purchase['type'] == "stock") {
                $product = $this->fct->getonerecord('products_stock', array('id_products_stock' => $purchase["id_products"]));
                $id_pro = $product['id_products'];
            }
        }

        $update_product['quantity'] = $product['quantity'] - $purchase["quantity"];

        if ($update_product['quantity'] >= 0) {
            if ($purchase['type'] == "product") {
                $this->db->where("id_products", $purchase["id_products"]);
                $this->db->update('products', $update_product);
            }

            if ($purchase['type'] == "stock") {
                $this->db->where("id_products_stock", $purchase["id_products"]);
                $this->db->update('products_stock', $update_product);
            }
            $this->db->delete('purchases', $delete_cond_2);

            $product = $this->fct->getonerecord('products', array('id_products' => $id_pro));
            $this->custom_fct->informProductModificationsToAdmin($product);

            $this->session->set_userdata("success_message", "Parchases is deleted successfully");
        } else {

            $this->session->set_userdata("error_message", "Srry! you can't  delete this purchase.");
        }
        redirect(site_url('back_office/products/purchases') . '?id_' . $purchase['type'] . '=' . $id_product);
    }

}
