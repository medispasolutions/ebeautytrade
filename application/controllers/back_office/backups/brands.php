<?php

class Brands extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "brands";
        $this->load->model("brands_m");
        $this->lang->load("admin");
    }

    /**
     * This is a function to list brands for the supplier
     */
    public function listing()
    {
        if (!$this->acl->has_permission('brands', 'index')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::BRANDS_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'List brands',
                ],
            ],
        ];

        //set conditions
        $cond['id_user'] = $this->acl->getCurrentUserId();

        //paginator
        $noOfElements = $this->brands_m->getRecords($cond);
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/brands');

        //get elements
        $brands = $this->brands_m->getRecords($cond, $this->paginator->per_page, $this->paginator->offset);

        foreach ($brands as $info) {
            $data['brands'][] = [
                'rowId' => $info['id_brands'],
                'columns' => [
                    '/uploads/brands/' . $info['logo'],
                    $info['title'],
                    $info['status']
                ]
            ];
        }

        $this->load->twigView("brandsListingPage/index", $data);
    }

    /**
     * This is a function to create a brand as a supplier
     * @var [] $formData Data to be used in the form (bad save)
     * @var [] $errors Data to be used in the form (bad save)
     */
    public function create($formData = null, $errors = null)
    {
        if (!$this->acl->has_permission('brands', 'add')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::BRAND_ADD,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Add brand',
                ],
                'MODE' => 'add',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        if ($formData) {
            $data['info'] = $formData;
        }
        $data['id_user'] = $this->acl->getCurrentUserId();

        $this->load->twigView("brandAddEditPage/index", $data);
    }

    /**
     * This is a function to edit a brand as a supplier
     * @var [] $formData Data to be used in the form (bad save)
     * @var [] $errors Data to be used in the form (bad save)
     */
    public function revise($id, $formData = null, $errors = null)
    {
        if (!$this->acl->has_permission('brands', 'edit')) {
            redirect(site_url("back_office"));
        }

        //i don't have time for this
        if ($this->session->flashdata('logo_error')) {
            $errors['logo_error'] = $this->session->flashdata('logo_error');
        }
        if ($this->session->flashdata('catalog_error')) {
            $errors['catalog_error'] = $this->session->flashdata('catalog_error');
        }
        if ($this->session->flashdata('slide_error')) {
            $errors['slide_error'] = $this->session->flashdata('slide_error');
        }
        if ($this->session->flashdata('photo_error')) {
            $errors['photo_error'] = $this->session->flashdata('photo_error');
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::BRAND_EDIT,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Edit brand',
                ],
                'MODE' => 'edit',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        //get brand details
        $cond = array("id_brands" => $id);
        $data["info"] = $this->fct->getonerecord($this->table, $cond);

        //check owner
        if ($data['info']['id_user'] != $this->acl->getCurrentUserId()) {
            redirect(site_url("back_office"));
        }

        if ($formData) {
            $data['info'] = $formData;
        } else {
            //additional errors
            $errors = [];
            if (!$data['info']['logo']) {
                $errors[] = "Brand logo is required";
            }
            if (!$data['info']['photo']) {
                $errors[] = "Brand contact person photo is required";
            }
            if (!empty($errors)) {
                $data['globalErrors'] = $errors;
            }
        }
        $data['id_user'] = $this->acl->getCurrentUserId();




        $this->load->twigView("brandAddEditPage/index", $data);
    }

    /**
     * This is the endpoint to save a brand as a supplier
     */
    public function save()
    {
        $brandId = $this->input->post("id_brands");

        //check owner
        if ($brandId) {
            $cond = array("id_brands" => $brandId);
            $brandData = $this->fct->getonerecord($this->table, $cond);
            if ($brandData['id_user'] != $this->acl->getCurrentUserId()) {
                redirect(site_url("back_office"));
            }
        }

        $this->form_validation->set_rules("logo", "logo", "trim");
        $this->form_validation->set_rules("title", "Brand name", "trim|required");
        $this->form_validation->set_rules("url", "Website", "trim");
        $this->form_validation->set_rules("logo", "logo", "trim");
        $this->form_validation->set_rules("video", "video", "trim");
        $this->form_validation->set_rules("access_code", "video", "trim");

        $this->form_validation->set_rules("slide_1", "slide 1", "trim");
        $this->form_validation->set_rules("slide_2", "slide 2", "trim");
        $this->form_validation->set_rules("slide_3", "slide 3", "trim");
        $this->form_validation->set_rules("slide_4", "sldie 4", "trim");

        $this->form_validation->set_rules("photo", "photo", "trim");
        $this->form_validation->set_rules("name", "First name", "trim|required");
        $this->form_validation->set_rules("last_name", "Family name", "trim|required");
        $this->form_validation->set_rules("position", "position", "trim|required");
        $this->form_validation->set_rules("phone", "phone", "trim|required");
        $this->form_validation->set_rules("email", "email", "trim|required|valid_email");

        if ($this->form_validation->run() == FALSE) {
            if ($brandId) {
                return $this->revise($brandId, $this->input->post(null), $this->form_validation->_error_array);
            } else {
                return $this->create($this->input->post(null), $this->form_validation->_error_array);
            }
        }


        $_data["id_user"] = $this->acl->getCurrentUserId();
        $_data["title"] = $this->input->post("title", true);
        $_data["title_url"] = $this->fct->cleanURL($this->table, url_title($title_url), $brandId);

        if (!empty($_FILES["catalog"]["name"])) {
            $size = $_FILES['catalog']['size'];
            $size_arr = getMaxSize('file');
            $max_size = $size_arr['size'];
            if ($max_size > $size) {
                if ($brandId) {
                    $cond_image = array("id_brands" => $brandId);
                    $old_image = $this->fct->getonecell("brands", "catalog", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                        unlink("./uploads/catalog/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("catalog", "brands");
                $_data["catalog"] = $image1;
            } else {
                $this->session->set_flashdata('catalog_error', 'Catalog file exceeds maximum size');
            }
        }

        if (!empty($_FILES["logo"]["name"])) {
            $size = $_FILES['logo']['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];
            if ($max_size > $size) {
                if ($brandId) {
                    $cond_image = array("id_brands" => $brandId);
                    $old_image = $this->fct->getonecell("brands", "logo", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                        unlink("./uploads/brands/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("logo", "brands");
                $this->fct->createthumb($image1, "brands", "112x122");
                $_data["logo"] = $image1;
            } else {
                $this->session->set_flashdata('logo_error', 'Logo file exceeds maximum size');
            }
        }

        $_data["video"] = $this->input->post("video", true);
        for ($i = 1; $i < 5; $i++) {

            if (!empty($_FILES["slide_" . $i]["name"])) {
                $size = $_FILES["slide_" . $i]['size'];
                $size_arr = getMaxSize('image');
                $max_size = $size_arr['size'];
                if ($max_size > $size) {
                    if ($brandId) {
                        $cond_image = array("id_brands" => $brandId);
                        $old_image = $this->fct->getonecell("brands", "slide_" . $i, $cond_image);
                        if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                            unlink("./uploads/brands/" . $old_image);
                            $sumb_val1 = explode(",", "735x290");
                            foreach ($sumb_val1 as $key => $value) {
                                if (file_exists("./uploads/brands/" . $value . "/" . $old_image)) {
                                    unlink("./uploads/brands/" . $value . "/" . $old_image);
                                }
                            }
                        }
                    }
                    $image1 = $this->fct->uploadImage("slide_" . $i, "brands");
                    $this->fct->createthumb($image1, "brands", "735x290");
                    $_data["slide_" . $i] = $image1;
                } else {
                    $this->session->set_flashdata('slide_error', 'Slide file exceeds maximum size');
                }
            }
        }

        if (!empty($_FILES["photo"]["name"])) {
            $size = $_FILES["photo"]['size'];
            $size_arr = getMaxSize('image');
            $max_size = $size_arr['size'];
            if ($max_size > $size) {
                if ($brandId) {
                    $cond_image = array("id_brands" => $brandId);
                    $old_image = $this->fct->getonecell("brands", "photo", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                        unlink("./uploads/brands/" . $old_image);
                        $sumb_val1 = explode(",", "112x122");
                        foreach ($sumb_val1 as $key => $value) {
                            if (file_exists("./uploads/brands/" . $value . "/" . $old_image)) {
                                unlink("./uploads/brands/" . $value . "/" . $old_image);
                            }
                        }
                    }
                }
                $image1 = $this->fct->uploadImage("photo", "brands");
                $this->fct->createthumb($image1, "brands", "112x122");
                $_data["photo"] = $image1;
            } else {
                $this->session->set_flashdata('photo_error', 'Slide file exceeds maximum size');
            }
        }

        $_data["access_code"] = $this->input->post("access_code", true);
        $_data["name"] = $this->input->post("name", true);
        $_data["last_name"] = $this->input->post("last_name", true);
        $_data["position"] = $this->input->post("position", true);
        $_data['phone'] = $this->input->post("phone", true);;
        $_data["email"] = $this->input->post("email", true);

        $_data["url"] = $this->input->post("url", true);

        if ($this->acl->isCurrentUserASupplier()) {
            //do not unpublish brands
            //$_data["status"] = 2;
        }

        if ($brandId) {
            $_data["updated_date"] = date("Y-m-d h:i:s");
            $this->db->where("id_brands", $brandId);
            $this->db->update($this->table, $_data);
        } else {
            $_data["created_date"] = date("Y-m-d h:i:s");
            $this->db->insert($this->table, $_data);
            $brandId = $this->db->insert_id();

            $this->load->model('send_emails');
            $brandData = $this->fct->getonerecord($this->table, ["id_brands" => $brandId]);
            $this->send_emails->sendBrandToAdmin($brandData, $this->acl->getCurrentUserData()->toArray());
        }

        redirect('back_office/brands/revise/'.$brandId);
    }

    /**
     * This is the function to list brands for admin (and starting point for list for supplier)
     * @param string $id_user
     */
    public function index($id_user = "")
    {
        $url = site_url("back_office/brands/index?pagination=on");
        $this->session->unset_userdata("admin_redirect_url");

        if (!$this->acl->has_permission('brands', 'index')) {
            redirect(site_url("home/dashboard"));
        }

        if ($this->acl->isCurrentUserASupplier()) {
            redirect(site_url("back_office/brands/listing"));
        }


        $data["title"] = "List brands";
        $data["content"] = "back_office/brands/list";
//
        $this->session->unset_userdata("back_link");
//
        if ($this->input->post('show_items')) {
            $show_items = $this->input->post('show_items');
            $this->session->set_userdata('show_items', $show_items);
        } elseif ($this->session->userdata('show_items')) {
            $show_items = $this->session->userdata('show_items');
        } else {
            $show_items = "50";
        }

        $show_items = "50";
        $user = $this->ecommerce_model->getUserInfo();
        $cond = array();
        if ($this->input->get('supplier') != "" && is_numeric($this->input->get('supplier'))) {
            $cond['id_user'] = $this->input->get('supplier');
            $url .= '&supplier=' . $cond['id_user'];
        }
        if (checkIfSupplier_admin()) {
            $cond['id_user'] = $user['id_user'];
        }

        if (isset($_GET['brand_name'])) {
            $brand_name = $_GET['brand_name'];
            $url .= '&brand_name=' . $brand_name;
            if (!empty($brand_name))
                $cond['brand_name'] = $brand_name;
        }

        if (isset($_GET['brand'])) {
            $brand = $_GET['brand'];
            $url .= '&brand=' . $brand;
            if (!empty($brand))
                $cond['brand'] = $brand;
        }


        $show_items = ($show_items == "All") ? $count_news : $show_items;
        $data['show_items'] = $show_items;
        $this->load->library("pagination");

        $count_news = $this->brands_m->getRecords($cond);
        $config["base_url"] = $url;
        $config["total_rows"] = $count_news;
        $config["per_page"] = $show_items;
        $config["use_page_numbers"] = TRUE;
        $config["page_query_string"] = TRUE;
//print "<pre>";print_r($config);exit;
        $this->pagination->initialize($config);
        if (isset($_GET["per_page"])) {
            if ($_GET["per_page"] != "")
                $page = $_GET["per_page"];
            else
                $page = 0;
        } else
            $page = 0;
        $data["info"] = $this->brands_m->getRecords($cond, $show_items, $page);
        $url .= '&per_page=' . $page;

        $this->session->set_userdata("back_link", $url);

// end pagination .
        $this->load->view("back_office/template", $data);

    }

    /**
     * I don't know what's that - probably unused copy
     * @param string $id_user
     */
    public function index2($id_user = "")
    {
        $order = "";
        $userData = "";
        if ($id_user != "") {
            $userData = $this->fct->getonerecord('user', array('id_user' => $id_user));
        }

        if (!empty($userData)) {
            $data['userData'] = $userData;
            $this->session->unset_userdata("admin_redirect_url");
            if ($this->acl->has_permission('brands', 'index')) {
                if ($order == "")
                    $order = "sort_order";
                $data["title"] = "List brands";
                $data["content"] = "back_office/brands/list";
//
                $this->session->unset_userdata("back_link");
//
                if ($this->input->post('show_items')) {
                    $show_items = $this->input->post('show_items');
                    $this->session->set_userdata('show_items', $show_items);
                } elseif ($this->session->userdata('show_items')) {
                    $show_items = $this->session->userdata('show_items');
                } else {
                    $show_items = "25";
                }

                $cond['id_user'] = $id_user;
                $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
                $data["show_items"] = $show_items;
// pagination  start :
                $count_news = $this->brands_m->getAll($this->table, $order, $cond);
                $show_items = ($show_items == 'All') ? $count_news : $show_items;
                $this->load->library('pagination');
                $config['base_url'] = site_url("back_office/brands/index/" . $id_user);
                $config['total_rows'] = $count_news;
                $config['per_page'] = $show_items;
                $config['uri_segment'] = 5;
                $this->pagination->initialize($config);
                $data['info'] = $this->brands_m->list_paginate($order, $config['per_page'], $this->uri->segment(5), $cond);
// end pagination .
                $this->load->view("back_office/template", $data);
            } else {
                redirect(site_url("home/dashboard"));
            }
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function view($id, $obj = "")
    {
        if ($this->acl->has_permission('brands', 'index')) {
            $data["title"] = "View brands";
            $data["content"] = "back_office/brands/add";
            $cond = array("id_brands" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    /**
     * This is a function to add brand as admin
     * @param string $id_user
     * @param string $obj
     */
    public function add($id_user = "", $obj = "")
    {
        if (
            !$this->acl->has_permission('brands', 'add')
            || $this->acl->isCurrentUserASupplier()
        ) {
            redirect(site_url("home/dashboard"));
        }

        if ($this->acl->has_permission('brands', 'add')) {
            $data["title"] = "Add brands";
            $data["content"] = "back_office/brands/add";
            if (isset($obj['info']['phone'])) {
                $data["info"]['phone'] = $obj['info']['phone'];
            }
            $this->load->view("back_office/template", $data);

        } else {
            redirect(site_url("home/dashboard"));
        }/* }else{
          redirect(site_url("home/dashboard"));} */
    }

    public function edit($id, $id_user = "", $obj = "")
    {
        $userData = "";

        $data = $obj;
        if ($this->acl->has_permission('brands', 'edit')) {
            $data["title"] = "Edit brands";
            $data["content"] = "back_office/brands/add";
            $cond = array("id_brands" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            //var_dump(set_value("position",''));
            //die();
            if (isset($obj['info']['phone'])) {
                $data["info"]['phone'] = $obj['info']['phone'];
            }

            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete($id)
    {
        $brand = $this->fct->getonerecord('brands', array('id_brands' => $id));
        if ($this->acl->has_permission('brands', 'delete')) {
            $_data = array("deleted" => 1,
                "deleted_date" => date("Y-m-d h:i:s"));
            $this->db->where("id_brands", $id);
            $this->db->update($this->table, $_data);
            $this->session->set_userdata("success_message", "Information was deleted successfully");
            redirect(site_url("back_office/brands/index/" . $brand['id_user']));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_all()
    {
        if ($this->acl->has_permission('brands', 'delete_all')) {
            $cehcklist = $this->input->post("cehcklist");
            $check_option = $this->input->post("check_option");
            if ($check_option == "delete_all") {
                if (count($cehcklist) > 0) {
                    for ($i = 0; $i < count($cehcklist); $i++) {
                        if ($cehcklist[$i] != "") {
                            $_data = array("deleted" => 1,
                                "deleted_date" => date("Y-m-d h:i:s"));
                            $this->db->where("id_brands", $cehcklist[$i]);
                            $this->db->update($this->table, $_data);
                        }
                    }
                }
                $this->session->set_userdata("success_message", "Informations were deleted successfully");
            }
            redirect(site_url("back_office/brands/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i + 1);
            $this->db->where("id_brands", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function checkUploadSlide1()
    {

        $size = $_FILES['slide_1']['size'];
        $size_arr = getMaxSize('image');
        $max_size = $size_arr['size'];

        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadImage1', 'Slide #1 exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function checkUploadSlide2()
    {

        $size = $_FILES['slide_2']['size'];
        $size_arr = getMaxSize('image');
        $max_size = $size_arr['size'];

        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadSlide2', 'Slide #2 exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function checkUploadSlide3()
    {

        $size = $_FILES['slide_3']['size'];
        $size_arr = getMaxSize('image');
        $max_size = $size_arr['size'];

        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadSlide3', 'Slide #4 exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function checkUploadSlide4()
    {

        $size = $_FILES['slide_4']['size'];
        $size_arr = getMaxSize('image');
        $max_size = $size_arr['size'];

        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadSlide4', 'Slide #4 exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function checkUploadCatalog()
    {

        $size = $_FILES['catalog']['size'];
        $size_arr = getMaxSize('file');
        $max_size = $size_arr['size'];

        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadCatalog', 'Catalog exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function checkUploadLogo()
    {

        $size = $_FILES['logo']['size'];
        $size_arr = getMaxSize('image');
        $max_size = $size_arr['size'];
        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadLogo', 'Logo exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function checkUploadPhoto()
    {

        $size = $_FILES['photo']['size'];
        $size_arr = getMaxSize('image');
        $max_size = $size_arr['size'];
        if ($max_size > $size) {
            return true;
        } else {
            $this->form_validation->set_message('checkUploadPhoto', 'Photo exceeds the maximum upload size(' . $size_arr['size_mega'] . ' Mb).');
            return false;
        }
    }

    public function phone()
    {
        $phone = $this->input->post('phone');
        $bool = true;
        foreach ($phone as $key => $val) {
            if (empty($val)) {
                $bool = false;
            }
        }

        if (!$bool) {
            $this->form_validation->set_message('phone', "The phone fields is required.");
            return false;
        } else {
            return true;
        }
    }

    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit brands";
        $this->form_validation->set_rules("title" . $lang, "TITLE", "trim|required");
        $this->form_validation->set_rules("supplier", "Supplier", "trim|required");
        //$this->form_validation->set_rules("button_text", "Button Text", "trim");
        //$this->form_validation->set_rules("deliver_text", "deliver Text", "trim");
        $this->form_validation->set_rules("phone", "phone", "trim|required");
        //$this->form_validation->set_rules("meta_title" . $lang, "PAGE TITLE", "trim|max_length[65]");
        //$this->form_validation->set_rules("title_url" . $lang, "TITLE URL", "trim");
        //$this->form_validation->set_rules("meta_description" . $lang, "META DESCRIPTION", "trim|max_length[160]");
        //$this->form_validation->set_rules("meta_keywords" . $lang, "META KEYWORDS", "trim|max_length[160]");
        $this->form_validation->set_rules("logo", "logo", "trim|callback_checkUploadLogo[]");
        //$this->form_validation->set_rules("overview", "overview", "trim|required");
        //$this->form_validation->set_rules("image", "image", "trim");
        //$this->form_validation->set_rules("link", "LINK", "trim");
        $this->form_validation->set_rules("video", "video", "trim");
        //$this->form_validation->set_rules("set_as_featured", "set as featured", "trim");
        $this->form_validation->set_rules("slide_1", "slide 1", "trim|callback_checkUploadSlide1[]");
        $this->form_validation->set_rules("slide_2", "slide 2", "trim|callback_checkUploadSlide2[]");
        $this->form_validation->set_rules("slide_3", "slide 3", "trim|callback_checkUploadSlide3[]");
        $this->form_validation->set_rules("slide_4", "sldie 4", "trim|callback_checkUploadSlide4[]");
//$this->form_validation->set_rules("catalog", "Catalog", "trim|callback_checkUploadCatalog[]");
        $this->form_validation->set_rules("photo", "photo", "trim|callback_checkUploadPhoto[]");
        $this->form_validation->set_rules("position", "position", "trim|required");
        $this->form_validation->set_rules("name", "name", "trim|required");
        /* $this->form_validation->set_rules("brand", "brand", "trim|required"); */
        /* $this->form_validation->set_rules("country_code", "Zip Code", "trim|required");
          $this->form_validation->set_rules("city_code", "Area Code", "trim|required");
          $this->form_validation->set_rules("number", "phone number", "trim|required"); */
        $this->form_validation->set_rules("email", "email", "trim|required");
        $this->form_validation->set_rules("status", "status", "trim");
        //$this->form_validation->set_rules("catalog_link", "catalog link", "trim");
        $this->form_validation->set_rules("url", "url", "trim");
        //$this->form_validation->set_rules("promote_to_front", "promote to front", "trim");
        if ($this->form_validation->run() == FALSE) {
            die('no validate');
            $data = array();
            /* 	if($this->input->post('phone')!=""){
              $data["info"]["phone"]=implode('-',$this->input->post('phone'));} */
            $data['deliver_by_supplier'] = $this->input->post('deliver_by_supplier');

            if ($this->input->post("id") != "")
                $this->edit($this->input->post("id"), $this->input->post("id_user"), $data);
            else
                $this->add($this->input->post("id_user"), $data);
        } else {
            $_data["id_user"] = $this->session->userdata("uid");
            $_data["title" . $lang] = $this->input->post("title" . $lang);
            $_data["button_text" . $lang] = $this->input->post("button_text" . $lang);
            if ($this->input->post("title_url" . $lang) == "")
                $title_url = $this->input->post("title" . $lang);
            else
                $title_url = $this->input->post("title_url" . $lang);
            if ($this->input->post("lang") == "ar") {
                $this->load->model("title_url_ar");
                $_data["title_url" . $lang] = $this->title_url_ar->cleanURL($this->table, $title_url, $this->input->post("id"), "title_url" . $lang);
            } else {
                $_data["title_url" . $lang] = $this->fct->cleanURL($this->table, url_title($title_url), $this->input->post("id"));
            }

            if (!checkIfSupplier_admin()) {
                $_data["meta_title" . $lang] = $this->input->post("meta_title" . $lang);
                $_data["meta_description" . $lang] = $this->input->post("meta_description" . $lang);
                $_data["meta_keywords" . $lang] = $this->input->post("meta_keywords" . $lang);
            }

            $_data['expire_date'] = $this->fct->date_in_formate($this->input->post('expire_date'));
            if (!empty($_FILES["catalog"]["name"])) {
                if ($this->input->post("id") != "") {
                    $cond_image = array("id_brands" => $this->input->post("id"));
                    $old_image = $this->fct->getonecell("brands", "catalog", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/catalog/' . $old_image)) {
                        unlink("./uploads/catalog/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("catalog", "brands");
                $_data["catalog"] = $image1;
            }

            if (!empty($_FILES["logo"]["name"])) {
                if ($this->input->post("id") != "") {
                    $cond_image = array("id_brands" => $this->input->post("id"));
                    $old_image = $this->fct->getonecell("brands", "logo", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                        unlink("./uploads/brands/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("logo", "brands");
                $this->fct->createthumb($image1, "brands", "112x122");
                $_data["logo"] = $image1;
            }
            $_data["overview"] = $this->input->post("overview");
            if (!empty($_FILES["image"]["name"])) {
                if ($this->input->post("id") != "") {
                    $cond_image = array("id_brands" => $this->input->post("id"));
                    $old_image = $this->fct->getonecell("brands", "image", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                        unlink("./uploads/brands/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("image", "brands");
                $_data["image"] = $image1;
            }
            $_data["video"] = $this->input->post("video");
            if (!checkIfSupplier_admin()) {
                $_data["set_as_featured"] = $this->input->post("set_as_featured");
            }


            for ($i = 1; $i < 5; $i++) {

                if (!empty($_FILES["slide_" . $i]["name"])) {
                    if ($this->input->post("id") != "") {
                        $cond_image = array("id_brands" => $this->input->post("id"));
                        $old_image = $this->fct->getonecell("brands", "slide_" . $i, $cond_image);
                        if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                            unlink("./uploads/brands/" . $old_image);
                            $sumb_val1 = explode(",", "735x290");
                            foreach ($sumb_val1 as $key => $value) {
                                if (file_exists("./uploads/brands/" . $value . "/" . $old_image)) {
                                    unlink("./uploads/brands/" . $value . "/" . $old_image);
                                }
                            }
                        }
                    }
                    $image1 = $this->fct->uploadImage("slide_" . $i, "brands");

                    $this->fct->createthumb($image1, "brands", "735x290");
                    $_data["slide_" . $i] = $image1;
                }
            }


            if (!empty($_FILES["photo"]["name"])) {
                if ($this->input->post("id") != "") {
                    $cond_image = array("id_brands" => $this->input->post("id"));
                    $old_image = $this->fct->getonecell("brands", "photo", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/brands/' . $old_image)) {
                        unlink("./uploads/brands/" . $old_image);
                        $sumb_val1 = explode(",", "112x122");
                        foreach ($sumb_val1 as $key => $value) {
                            if (file_exists("./uploads/brands/" . $value . "/" . $old_image)) {
                                unlink("./uploads/brands/" . $value . "/" . $old_image);
                            }
                        }
                    }
                }
                $image1 = $this->fct->uploadImage("photo", "brands");
                $this->fct->createthumb($image1, "brands", "112x122");
                $_data["photo"] = $image1;
            }
            $_data["id_user"] = $this->input->post("supplier");

            $_data["deliver_text"] = $this->input->post("deliver_text");
            $_data["access_code"] = $this->input->post("access_code");
            $_data["promote_to_front"] = $this->input->post("promote_to_front");
            $_data["position"] = $this->input->post("position");
            $_data["name"] = $this->input->post("name");
            $_data["status"] = $this->input->post("status");
            $_data["link"] = $this->input->post("link");

            $phone = $this->input->post("phone");

            /* $_data["country_code"]=$this->input->post("country_code");
              $_data["city_code"]=$this->input->post("city_code");
              $_data["number"]=$this->input->post("number"); */
            /* $_data['phone']=$phone[0].'-'.$phone[1].'-'.$phone[2]; */
            $_data['phone'] = $this->input->post("phone");;
            $_data["email"] = $this->input->post("email");
            $_data["deliver_by_supplier"] = $this->input->post("deliver_by_supplier");
            $_data["catalog_link"] = $this->input->post("catalog_link");
            $_data["url"] = $this->input->post("url");
            if (checkIfSupplier_admin()) {
                $_data["status"] = 2;
            }

            if ($this->input->post("id") != "") {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_brands", $this->input->post("id"));
                $this->db->update($this->table, $_data);
                $new_id = $this->input->post("id");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert($this->table, $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }

            if (isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
                $brand = $this->fct->getonerow("brands", array("id_brands" => $new_id));
                $user = $this->fct->getonerow("user", array("id_user" => $this->input->post("supplier")));
                $this->load->model("send_emails");
                $this->send_emails->inform_client_brand($brand, $user);
            }
            if ($this->session->userdata("admin_redirect_url")) {
                redirect($this->session->userdata("admin_redirect_url"));
            } else {
                if ($this->session->userdata("back_link") != "") {
                    $back_link = $this->session->userdata("back_link");
                    redirect($back_link);
                } else {
                    redirect(site_url("back_office/brands/index/" . $this->input->post("id_user")));
                }
            }
        }
    }

    public function delete_file()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/brands/" . $image)) {
            unlink("./uploads/brands/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'brands')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/brands/" . $value . "/" . $image)) {
                    unlink("./uploads/brands/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_brands", $id);
        $this->db->update("brands", $_data);
        echo 'Done!';
    }

}
