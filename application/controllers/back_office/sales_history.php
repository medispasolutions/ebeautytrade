<?
class Sales_history extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="sales_history";
$this->load->model("sales_history_m");
 $this->lang->load("admin"); 
}


public function index($order=""){

$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('sales_history','index')){
		
if($order == "")
$order ="sort_order";
$data["title"]="List sales history";
$data["content"]="back_office/sales_history/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$data["show_items"] = $show_items;
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));


$url = site_url('back_office/sales_history').'?pagination=on';
$cond = array();
/*if(isset($_GET['customer_name'])) {
	$customer_name = $_GET['customer_name'];
	$url .= '&customer_first_name='.$customer_name;
	if(!empty($customer_name))
	$cond['user.name like'] = '%'.$customer_name.'%';
}*/


if(isset($_GET['first_name'])) {
	$first_name = $_GET['first_name'];
	$url .= '&first_name='.$first_name;
	if(!empty($first_name)){
	$cond['address_book.first_name like'] = $first_name;
	}
	
}
if(isset($_GET['last_name'])) {
	$last_name = $_GET['last_name'];
	$url .= '&last_name='.$last_name;
	if(!empty($last_name)){
	$cond['address_book.last_name like'] = $last_name;
	}
	
}

if(isset($_GET['country_id'])) {
	$country_id = $_GET['country_id'];
	$url .= '&country_id='.$country_id;
	if(!empty($country_id))
	$cond['address_book.id_countries'] = $country_id;
	$cond['billing']=1;
}



if(isset($_GET['payment_method'])) {
	$payment_method = $_GET['payment_method'];
	$url .= '&payment_method='.$payment_method;
	if(!empty($payment_method))
	$cond['orders.payment_method'] = $payment_method;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);

	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['orders.created_date >='] = $f_date;
}

if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
    $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$to_date;
	if(!empty($to_date))
	$cond['orders.created_date <='] = $t_date;
}

if(isset($_GET['status'])) {
	$status = $_GET['status'];
	$url .= '&status='.$status;
	if(!empty($status))
	$cond['orders.status'] = $status;
}

// pagination  start :
$count_news = $this->ecommerce_model->getOrders($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->ecommerce_model->getOrders($cond,$config['per_page'],$page);

if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {
	
$this->export_fct->salesHistoryExportToExcel($data['info']);
}

$data['countries'] = $this->fct->getAll_cond('countries','title',array("status"=>0));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('sales_history','add')){	
$data["title"]="Add sales history";
$data["content"]="back_office/sales_history/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('sales_history','index')){	
$data["title"]="View sales history";
$data["content"]="back_office/sales_history/add";
$cond=array("id_sales_history"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('sales_history','edit')){	
$data["title"]="Edit sales history";
$data["content"]="back_office/sales_history/add";
$cond=array("id_sales_history"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('sales_history','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_sales_history",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/sales_history/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('sales_history','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_sales_history",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/sales_history/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_sales_history",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit sales history";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_sales_history",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/sales_history/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/sales_history/".$image)){
unlink("./uploads/sales_history/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'sales history')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/sales_history/".$value."/".$image)){
unlink("./uploads/sales_history/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_sales_history",$id);
$this->db->update("sales_history",$_data);
echo 'Done!';
}

}