<?
class Task extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="task";
$this->load->model("task_m");
 $this->lang->load("admin"); 
}


public function index(){
$cond=array();
$order ="sort_order";
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('task','index')){	
$url=site_url("back_office/task/index?pagination=on");
$data["title"]="List task";
$data["content"]="back_office/task/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :



if(isset($_GET["keyword"])) {
	$keyword = $_GET["keyword"];
	$url .= "&keyword=".$keyword;
	if(!empty($keyword))
	$cond["keyword"] = $keyword;
}

if(isset($_GET["location"])) {
	$location = $_GET["location"];
	$url .= "&location=".$location;
	if(!empty($location))
	$cond["location"] = $location;
}


if(isset($_GET["status"])) {
	$status = $_GET["status"];
	$url .= "&status=".$status;
	if(!empty($status))
	$cond["status"] = $status;
}


if(isset($_GET["priority"])) {
	$priority = $_GET["priority"];
	$url .= "&priority=".$priority;
	if(!empty($priority))
	$cond["priority"] = $priority;
}

if(isset($_GET["category"])) {
	$category = $_GET["category"];
	$url .= "&category=".$category;
	if(!empty($category))
	$cond["category"] = $category;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	$data['from_date']=$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}

if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	 $data['to_date']=$to_date;
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}

$show_items = ($show_items == "All") ? $count_news : $show_items;
$this->load->library("pagination");

$count_news = $this->task_m->getRecords($cond);
$config["base_url"] = $url;
$config["total_rows"] = $count_news;
$config["per_page"] =$show_items;
$config["use_page_numbers"] = TRUE;
$config["page_query_string"] = TRUE;
//print "<pre>";print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET["per_page"])) {
	if($_GET["per_page"] != "") $page = $_GET["per_page"];
	else $page = 0;
}
else $page = 0;
$data["info"] = $this->task_m->getRecords($cond,$show_items,$page);
$url .='&per_page='.$page;


$data['url']=$url;
$this->session->set_userdata("back_link",$url);

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('task','add')){	
$data["title"]="Add task";
$data["content"]="back_office/task/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function getComments(){
	$lang="";
	if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
	$id_task=$this->input->post('id_task');
	$comments=$this->fct->getAll_cond('task_comments','id_task_comments desc',array('id_task'=>$id_task)); 
	$data['comments']=$comments;
	$data['lang']=$lang;
	$return['html']=$this->load->view('back_office/task/task_comments',$data,true);
	die(json_encode($return));
	} 


public function view($id,$obj = ""){
if ($this->acl->has_permission('task','index')){	
$data["title"]="View task";
$data["content"]="back_office/task/add";
$cond=array("id_task"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('task','edit')){	
$data["title"]="Edit task";
$data["content"]="back_office/task/add";
$cond=array("id_task"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('task','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_task",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
	if($this->session->userdata("back_link")!=""){
		$back_link=$this->session->userdata("back_link");
		redirect($back_link);}else{
   	    redirect(site_url("back_office/".$this->table));}
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('task','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_task",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
	if($this->session->userdata("back_link")!=""){
		$back_link=$this->session->userdata("back_link");
		redirect($back_link);}else{
   	    redirect(site_url("back_office/".$this->table));}
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_task",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit task";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim");
$this->form_validation->set_rules("id_categories_task", "Category Task", "trim");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("date", "date"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("description", "description"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("enquiry", "enquiry"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("location", "location"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("spamiles_note", "spamiles note"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("priority", "priority"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("dow_group_notes", "dow group notes"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("status", "status"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("task_category", "task category"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["id_categories_task"]=$this->input->post("id_categories_task");
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	

$_data["date"]=$this->fct->date_in_formate($this->input->post("date"));
$_data["description"]=$this->input->post("description");
$_data["enquiry"]=$this->input->post("enquiry");
$_data["location"]=$this->input->post("location");
$_data["spamiles_note"]=$this->input->post("spamiles_note");
$_data["priority"]=$this->input->post("priority");
$_data["dow_group_notes"]=$this->input->post("dow_group_notes");
$_data["status"]=$this->input->post("status");
$_data["task_category"]=$this->input->post("task_category");

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_task",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect(site_url("back_office/task/edit/".$new_id));
	}
	else {
		if($this->session->userdata("back_link")!=""){
		$back_link=$this->session->userdata("back_link");
		redirect($back_link);}else{
   	    redirect(site_url("back_office/task/edit/".$new_id));}
	}
	
}
	
}

public function delete_comment(){
	$id_task=$this->input->post('id_task');
	$id_task_comments=$this->input->post('id_task_comments');
	$cond['id_task']=$id_task;
	$cond['id_task_comments']=$id_task_comments;
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where($cond);
$this->db->update('task_comments',$_data);
echo "done";exit;
	}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/task/".$image)){
unlink("./uploads/task/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'task')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/task/".$value."/".$image)){
unlink("./uploads/task/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_task",$id);
$this->db->update("task",$_data);
echo 'Done!';
}

}