<?
class Products_stock extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="products_stock";
$this->load->model("products_stock_m");
 $this->lang->load("admin"); 
}


public function index(){
$cond=array();
$order ="sort_order";
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products_stock','index')){	
$url=site_url("back_office/products_stock/index?pagination=on");
$data["title"]="List products stock";
$data["content"]="back_office/products_stock/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :



if(isset($_GET["keyword"])) {
	$keyword = $_GET["keyword"];
	$url .= "&keyword=".$keyword;
	if(!empty($keyword))
	$cond["keyword"] = $keyword;
}

$show_items = ($show_items == "All") ? $count_news : $show_items;
$this->load->library("pagination");

$count_news = $this->products_stock_m->getRecords($cond);
$config["base_url"] = $url;
$config["total_rows"] = $count_news;
$config["per_page"] =$show_items;
$config["use_page_numbers"] = TRUE;
$config["page_query_string"] = TRUE;
//print "<pre>";print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET["per_page"])) {
	if($_GET["per_page"] != "") $page = $_GET["per_page"];
	else $page = 0;
}
else $page = 0;
$data["info"] = $this->products_stock_m->getRecords($cond,$show_items,$page);
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('products_stock','add')){	
$data["title"]="Add products stock";
$data["content"]="back_office/products_stock/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('products_stock','index')){	
$data["title"]="View products stock";
$data["content"]="back_office/products_stock/add";
$cond=array("id_products_stock"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('products_stock','edit')){	
$data["title"]="Edit products stock";
$data["content"]="back_office/products_stock/add";
$cond=array("id_products_stock"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('products_stock','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_products_stock",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/products_stock/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('products_stock','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_products_stock",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/products_stock/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_products_stock",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit products stock";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("miles", "miles"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("retail2_price", "retail2 price"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("retail_list_price", "retail list price"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("retail_discount_status", "retail discount status"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("retail_discount_expiration", "retail discount expiration"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("retail_discount", "retail discount"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("return_quantity", "return quantity"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("threshold", "threshold"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("threshold_flag", "threshold_flag"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("barcode", "barcode"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("set_as_redeemed_by_miles", "set_as_redeemed_by_miles"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("set_general_miles", "set_general_miles"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("general_miles", "general_miles"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("product", "product"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("combination", "combination"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("sku", "sku"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("quantity", "quantity"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("price", "price"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["miles"]=$this->input->post("miles");
$_data["retail2_price"]=$this->input->post("retail2_price");
$_data["retail_list_price"]=$this->input->post("retail_list_price");
$_data["retail_discount_status"]=$this->input->post("retail_discount_status");

$_data["retail_discount_expiration"]=$this->fct->date_in_formate($this->input->post("retail_discount_expiration"));
$_data["retail_discount"]=$this->input->post("retail_discount");
$_data["return_quantity"]=$this->input->post("return_quantity");
$_data["threshold"]=$this->input->post("threshold");
$_data["threshold_flag"]=$this->input->post("threshold_flag");
$_data["barcode"]=$this->input->post("barcode");
$_data["set_as_redeemed_by_miles"]=$this->input->post("set_as_redeemed_by_miles");
$_data["set_general_miles"]=$this->input->post("set_general_miles");
$_data["general_miles"]=$this->input->post("general_miles");
$_data["id_products"]=$this->input->post("product"); 
$_data["combination"]=$this->input->post("combination");
$_data["sku"]=$this->input->post("sku");
$_data["quantity"]=$this->input->post("quantity");
$_data["price"]=$this->input->post("price");

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_products_stock",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/products_stock/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/products_stock/".$image)){
unlink("./uploads/products_stock/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'products stock')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/products_stock/".$value."/".$image)){
unlink("./uploads/products_stock/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_products_stock",$id);
$this->db->update("products_stock",$_data);
echo 'Done!';
}

}