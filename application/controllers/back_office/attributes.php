<?
class Attributes extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="attributes";
$this->load->model("attributes_m");
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('attributes','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List attributes";
$data["content"]="back_office/attributes/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
//$count_news = $this->attributes_m->getAll($this->table,$order);
$data['info'] = $this->fct->getAll_cond('attributes','sort_order',array('id_parent_translate'=>0));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('attributes','add')){	
$data["title"]="Add attributes";
$data["content"]="back_office/attributes/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ''){
if ($this->acl->has_permission('attributes','index')){	
$data["title"]="View attributes";
$data["content"]="back_office/attributes/add";
$cond=array("id_attributes"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if($this->config->item("language_module")) {
  $data["language"]=$this->fct->getonerecord("languages",array("index"=>$data["info"]["lang"]));
  $data["lang"] = $data["info"]["lang"];
  if($obj == 'translate') {
	  $data["id_parent_translate"] = $this->translation_lib->getDefaultRowLangID($this->table,$id,TRUE);
  }
}
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ''){
if ($this->acl->has_permission('attributes','edit')){	
$data["title"]="Edit attributes";
$data["content"]="back_office/attributes/add";
$cond=array("id_attributes"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if($this->config->item("language_module")) {
  $data["language"]=$this->fct->getonerecord("languages",array("index"=>$data["info"]["lang"]));
  $data["lang"] = $data["info"]["lang"];
  if($obj == 'translate') {
	  $data["id_parent_translate"] = $this->translation_lib->getDefaultRowLangID($this->table,$id,TRUE);
  }
}
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('attributes','delete')){
/*$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));*/
/*$check_in_orders = $this->fct->getAll_cond('line_items','sort_order',array('id_attributes'=>$id));
if(empty($check_in_orders)) {*/
$check_in_products = $this->fct->getAll_cond('product_attributes','sort_order',array('id_attributes'=>$id));
if(empty($check_in_products)) {
$cnd = array('id_attributes'=>$id);
$this->db->delete($this->table,$cnd);
$this->db->delete('attribute_options',$cnd);
	$this->session->set_userdata('success_message','Attribute is removed successfully.');
} 
else {
	$this->session->set_userdata('error_message','This attribute is in use, please remove it from selected products.');
}
//}
/*else {
	$this->session->set_userdata('error_message','You are permanently not allowed to delete this attribute, it was already selected for some orders.');
}
*/
redirect(site_url("back_office/attributes/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('attributes','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_attributes",$cehcklist[$i]);
$this->db->update($this->table,$_data);
if($this->config->item("language_module")) {
	$this->db->where("id_parent_translate",$cehcklist[$i]);
	$this->db->update($this->table,$_data);
}	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/attributes/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_attributes",$sort[$i]);
$this->db->update($this->table,$_data);	
if($this->config->item("language_module")) {
	$this->db->where("id_parent_translate",$sort[$i]);
	$this->db->update($this->table,$_data);
}
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit attributes";
$this->form_validation->set_rules("title", "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title", "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url", "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description", "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords", "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("label", "label", "trim|required");
//$this->form_validation->set_rules("label_en", "label (english)", "trim|required");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{
	if($this->config->item("language_module")) {
$_data["lang"]=$this->input->post("lang");
}
$_data["id_user"]=$this->session->userdata("uid");
if($this->config->item("language_module") && isset($_POST["id_parent_translate"])) {
	if($this->input->post("id") != "" && $this->input->post("id") == $this->input->post("id_parent_translate")) {
		$_data["id_parent_translate"] = 0;
	}
	else {
		$_data["id_parent_translate"]=$this->input->post("id_parent_translate");
	}
}
$_data["title"]=$this->input->post("title");
$_data["meta_title"]=$this->input->post("meta_title");
if($this->input->post("title_url") == "")
$title_url = $this->input->post("title");
else
$title_url = $this->input->post("title_url");
$_data["title_url"]=$this->fct->cleanURL("attributes",url_title($title_url),$this->input->post("id"));
$_data["meta_description"]=$this->input->post("meta_description");
$_data["meta_keywords"]=$this->input->post("meta_keywords");	
$_data["label"]=$this->input->post("label");
$_data["label_en"]=$this->input->post("label_en");

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_attributes",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	if($this->config->item("language_module") && isset($_POST["id_parent_translate"])) {
		$parent_record = $this->fct->getonerow($this->table,array("id_".$this->table=>$_POST["id_parent_translate"]));
		$_data["sort_order"]= $parent_record["sort_order"];
	}
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->config->item("language_module") && isset($_POST["id_parent_translate"])) {
		redirect(site_url("back_office/translation/section/".$this->table."/".$this->input->post("id_parent_translate")));
	}
	else {
		redirect(site_url("back_office/".$this->table."/".$this->session->userdata("back_link")));
	}
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/attributes/".$image)){
unlink("./uploads/attributes/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'attributes')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if(isset($res["thumb"]) && $res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/attributes/".$value."/".$image)){
unlink("./uploads/attributes/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_attributes",$id);
$this->db->update("attributes",$_data);
echo 'Done!';
}

}