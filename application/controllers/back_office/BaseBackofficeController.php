<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class is loaded via application/core/MY_Controller.php file
 * 
 * We can get requested controller either by get_class($this) or $this->router->fetch_class()
 * We can get request method by $this->router->fetch_method()
 */
 
abstract class BaseBackofficeController extends BaseController
{

    /** @var \views\suppliersBackoffice\ViewData */
    protected $view;

    /** @var  \spamiles\utils\paginators\TwigPaginator */
    protected $paginator;
    
    public function __construct()
    {
        parent::__construct();

        $this->view = new \views\suppliersBackoffice\ViewData();
        $this->paginator = new \spamiles\utils\paginators\TwigPaginator();

        $this->checkLoginRestrictions();

        if ($this->acl->isCurrentUserLoggedIn()) {
            $this->checkACLRestrictions();
            $this->checkProfileCompletionRestrictions();
        }
    }
    
    /**
     * Checks whether the user can access this page without login, 
     * if not then redirects him to home
     * @return void
     */
    protected function checkLoginRestrictions()
    {
        $actionsAllowedWithoutLogin = array(
            'home' => array(
                'index', 'login_validate', 'validate_email', 'password', 
                'loginbypassword', 'load_loginbypassword', 'update_password',
            ),
        );
    
        $restricted = false;

        if (!$this->acl->isCurrentUserLoggedIn()) {
            if (!isset($actionsAllowedWithoutLogin[strtolower(get_class($this))])) {
                $restricted = true;
            } elseif (
                !in_array(
                    strtolower($this->router->fetch_method()), 
                    $actionsAllowedWithoutLogin[strtolower(get_class($this))]
                )
            ) {
                $restricted = true;
            }
        }
        
        if ($restricted) {
            redirect(site_url("back_office"));
        }
    }
    
    /**
     * TODO: move all acl checks here for automatization
     * Checks whether the user can access requested method according to ACL,
     * if not then redirects him to home
     * @return void Checks whether the user can access requested method according to ACL
     */
    protected function checkACLRestrictions()
    {
    
    }

    /**
     * Checks whether user can access some pages without giving us, fundamental information
     */
    protected function checkProfileCompletionRestrictions()
    {
        //we only check it for suppliers
        if ($this->acl->isCurrentUserAAdmin()) {
            return;
        }

        //which controllers and methods you can access with no data given
        $actionsAllowedWithoutCompletedProfile = array(
            'dashboard' => [
                'index',
            ],
            'profile' => [
                'edit', 'save',
            ],
            'home' => [
                'index', 'logout',
            ],
        );

        //which controllers and methods you can access with only personal data given
        $actionsAllowedWithoutCompletedBrand = [
            'brands' => [
                'listing', 'create', 'delete', 'revise', 'save',
            ],
        ];
        $actionsAllowedWithoutCompletedBrand = array_merge(
            $actionsAllowedWithoutCompletedBrand, 
            $actionsAllowedWithoutCompletedProfile
        );

        //let's find whether you can access current page
        $restricted = false;

        if ($this->acl->isCurrentUserLoggedIn()) {
            if ($this->acl->profileCompletionPercent() == 100) {
                //do nothing
            } else {

                $aclList = $actionsAllowedWithoutCompletedProfile;
                if ($this->acl->personalDataCompletionPercent() == 100) {
                    $aclList = $actionsAllowedWithoutCompletedBrand;
                }

                if (!isset($aclList[strtolower(get_class($this))])) {
                    $restricted = true;
                } elseif (
                    !in_array(
                        strtolower($this->router->fetch_method()),
                        $aclList[strtolower(get_class($this))]
                    )
                ) {
                    $restricted = true;
                }
            }
        }

        if ($restricted) {
            redirect(site_url("back_office"));
        }
    }
}
