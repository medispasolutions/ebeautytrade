<?

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
// Your own constructor code
//$this->headers='back_office/includes/header';
//$this->footer='back_office/includes/footer';

    }


    public function index()
    {
        $data["title"] = "List Users";
        $data["content"] = "back_office/users";
        $cond = array();
        $like = array();
        $cond['guest'] = 0;
        if ($this->acl->has_permission('users', 'index')) {
            $url = site_url("back_office/users/index?pagination=on");
            
            if ($this->input->get("user_id") != "") {
                $cond['user.id_user'] = $this->input->get("user_id");
                $url .= '&user_id=' . $this->input->get("user_id");
            }

            if ($this->input->get("name") != "") {
                $like['UPPER(user.name)'] = strtoupper($this->input->get("name"));
                $url .= '&name=' . $this->input->get("name");
            }

            if ($this->input->get("person_name") != "") {
                $like['UPPER(brands.name)'] = strtoupper($this->input->get("person_name"));
                $url .= '&person_name=' . $this->input->get("person_name");
            }

            if ($this->input->get("guest") != "") {
                $cond['guest'] = $this->input->get("guest");
                $url .= '&guest=' . $this->input->get("guest");
            }
            
            if ($this->input->get("surname") != "") {
                $like['UPPER(user.surname)'] = strtoupper($this->input->get("surname"));
                $url .= '&surname=' . $this->input->get("surname");
            }

            if ($this->input->get("account_manager") != "") {
                $cond['user.id_account_manager'] = $this->input->get("account_manager");
                $url .= '&account_manager=' . $this->input->get("account_manager");
            }

            if ($this->input->get("id_users_areas") != "") {
                $cond['user.id_users_areas'] = $this->input->get("id_users_areas");
                $url .= '&id_users_areas=' . $this->input->get("id_users_areas");
            }

            if ($this->input->get("city") != "") {
                $like['UPPER(user.city)'] = strtoupper($this->input->get("city"));
                $url .= '&city=' . $this->input->get("city");
            }

            if ($this->input->get("trading_name") != "") {
                $like['UPPER(user.trading_name)'] = strtoupper($this->input->get("trading_name"));
                $url .= '&trading_name=' . $this->input->get("trading_name");
            }
            
            if ($this->input->get("user_email") != "") {
                $like['UPPER(user.email)'] = strtoupper($this->input->get("user_email"));
                $url .= '&user_email=' . $this->input->get("user_email");
            }

            if ($this->input->get("account_type") != "") {
                $like['UPPER(user.type)'] = strtoupper($this->input->get("account_type"));
                $url .= '&account_type=' . $this->input->get("account_type");
            }
            
            if ($this->input->get("role") != "") {
                $cond['user.id_roles'] = $this->input->get("role");
                $url .= '&role=' . $this->input->get("role");
            }

            if ($this->input->get("first_step") == 1) {
                $cond['user.completed'] = 0;
                $url .= '&first_step=' . $this->input->get("first_step");
            }

            if ($this->input->get("country") != "") {
                $cond['user.id_countries'] = $this->input->get("country");
                $url .= '&country=' . $this->input->get("country");
            }

            if ($this->input->get("discount_group") != "") {
                if ($this->input->get("discount_group") == 'not_assigned')
                    $cond['user.id_discount_groups'] = 0;
                else
                    $cond['user.id_discount_groups'] = $this->input->get("discount_group");
                    $url .= '&discount_group=' . $this->input->get("discount_group");
            }
            
            if ($this->input->get("status") != "") {
                $url .= '&status=' . $this->input->get("status");
                if ($this->input->get("status") == 'blocked') {
                    $cond['user.status'] = 0;
                } else {
                    if ($this->input->get("status") == 'under_review') {
                        $cond['user.status'] = 2;
                    } else {
                        if ($this->input->get("status") == 'incomplete') {
                            $cond['user.status'] = 3;
                        } else {
                            $cond['user.status'] = 1;
                        }
                    }
                }
            }

            $data['countries'] = $this->fct->getAll('countries', 'id_countries');
            $this->session->set_userdata("back_link", $url);
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            /*$this->session->set_userdata('admin_redirect_link',$url);*/

            $data["info"] = $this->fct->getUsers($cond, $like);

            if (isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {
                $this->export_fct->usersExportToExcel($data['info']);
            }
            
            if (isset($_GET['report_submit']) && $_GET['report_submit'] == 'subscribers') {
                $this->export_fct->subscribedExportToExcel($data['info']);
            }
            
            if (isset($_GET['report_submit']) && $_GET['report_submit'] == 'supplier') {
                $this->export_fct->supplierExportToExcel($data['info']);
            }

            $this->load->view('back_office/template', $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function checkUser()
    {
        $id_user = $this->input->post('id_user');
        $checkBrand = $this->fct->getonerecord('brands', array('id_user' => $id_user));
        $result = 1;
        if (!empty($checkBrand)) {
            $result = 0;
        }

        $checkOrders = $this->fct->getonerecord('orders', array('id_user' => $id_user));
        if (!empty($checkOrders)) {
            $result = 0;
        }

        $deliveryOrders = $this->fct->getonerecord('delivery_order', array('id_user' => $id_user));
        if (!empty($deliveryOrders)) {
            $result = 0;
        }
        echo $result;
        exit;

    }

    public function add()
    {
        $data["title"] = "Add New User";
        $data["content"] = "back_office/add_users";
        $data["roles"] = $this->fct->getAll('roles', 'sort_order');
        $data["discount_groups"] = $this->fct->getAll('discount_groups', 'sort_order');
        $this->load->view('back_office/template', $data);
    }

    public function edit($id)
    {
        if (($id == 1 || $id == 2) && $this->session->userdata('level') == 0)
            redirect(site_url('back_office/users'));
        $data["title"] = "Edit User";
        $data["content"] = "back_office/add_users";
        $cond = array('id_user' => $id);
        $updata['readed'] = 1;
        $this->db->where($cond);
        $this->db->update('user', $updata);
        $data["id"] = $id;
        $data["info"] = $this->fct->getonerecord('user', $cond);
        $data["roles"] = $this->fct->getAll('roles', 'sort_order');
        $data["discount_groups"] = $this->fct->getAll('discount_groups', 'sort_order');
        $this->load->view('back_office/template', $data);
    }

    public function priviledge($id)
    {
        $data["title"] = "Set User's Privileges";
        $data["content"] = "back_office/priviledge";
        $data["id"] = $id;
        $data["attr"] = $this->fct->getAll('content_type', 'sort_order');
        $this->load->view('back_office/template', $data);
    }

    public function validateemailexistance()
    {
        $email = $this->input->post('email');
        $cond = array('email' => $email, 'deleted' => 0);
        if ($this->input->post('id') != '') {

            $cond['id_user !='] = $this->input->post('id');
        }

        $cond['guest'] = $this->input->post('guest');
        $check = $this->fct->getonerow('user', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('validateemailexistance', 'Email exists, please try another email.');
            return false;
        }
    }

    public function validateusernameexistance()
    {
        $username = $this->input->post('username');
        $cond = array('username' => $username, 'deleted' => 0);
        if ($this->input->post('id') != '') {
            $cond['id_user !='] = $this->input->post('id');
        }
        $check = $this->fct->getonerow('user', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('validateusernameexistance', 'Username exists, please try another username.');
            return false;
        }
    }

    public function phone()
    {
        $phone = $this->input->post('phone');
        $bool = true;
        foreach ($phone as $key => $val) {
            if (empty($val)) {
                $bool = false;
            }
        }

        if (!$bool) {
            $this->form_validation->set_message('phone', "The phone fields is required.");
            return false;
        } else {
            return true;
        }

    }

    public function mobile()
    {
        $mobile = $this->input->post('mobile');
        $bool = true;
        foreach ($mobile as $key => $val) {
            if (empty($val)) {
                $bool = false;
            }
        }

        if (!$bool) {
            $this->form_validation->set_message('mobile', "The mobile fields is required.");
            return false;
        } else {
            return true;
        }

    }

    public function fax()
    {
        $fax = $this->input->post('fax');
        $bool = true;
        foreach ($fax as $key => $val) {
            if (empty($val)) {
                $bool = false;
            }
        }

        if (!$bool) {
            $this->form_validation->set_message('fax', "The Fax fields is required.");
            return false;
        } else {
            return true;
        }

    }

    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }

        if ($this->input->post('id') != '') {
            //////////////Personal Information///////////////////////////

            if ($this->input->post('password') != '') {
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[20]');
                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[8]|max_length[20]|matches[password]');
            }
        } else {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[8]|max_length[20]|matches[password]');
        }
        //////////////Address///////////////////////////////////////
        if ($this->input->post('roles') == 4 || $this->input->post('roles') == 5) {
            /*$this->form_validation->set_rules('trading_name',lang('trading_name'),'trim|required|xss_clean');*/
        }

        $this->form_validation->set_rules('address_2', lang('address'), 'trim|xss_clean');
        $this->form_validation->set_rules('id_countries', lang('country'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('id_account_manager', 'Account Manager', 'trim|xss_clean');
        //$this->form_validation->set_rules('city', lang('city'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('id_users_areas', lang('areas'), 'trim|xss_clean');
        $this->form_validation->set_rules('phone', lang('phone'), 'trim|required|xss_clean');

        /*$this->form_validation->set_rules('fax',lang('fax'), 'trim|xss_clean');
        $this->form_validation->set_rules('mobile',lang('mobile'), 'trim|xss_clean');*/
        //////////////Personal Information///////////////////////////
        $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('salutation', lang('salutation'), 'trim|xss_clean');
        $this->form_validation->set_rules('license_number', lang('license_number'), 'trim|xss_clean');
        $this->form_validation->set_rules('position', lang('position'), 'trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|callback_validateemailexistance[]');
        //$this->form_validation->set_rules('username', 'Username', 'trim|required|callback_validateusernameexistance[]');
        $this->form_validation->set_rules('status', 'Status', 'trim');
        $this->form_validation->set_rules('mobile', 'Mobile', 'trim');
        $this->form_validation->set_rules('fax', 'Fax', 'trim');
        //////////////Others///////////////////////////////////////
        /*		$this->form_validation->set_rules('business_type',lang('business_type'),'trim|required|xss_clean');
                $this->form_validation->set_rules('hear_about_us',lang('hear_about_us'), 'trim|required|xss_clean');*/

        /*$this->form_validation->set_rules('services_categories',lang('services_categories'),'trim|required|xss_clean');*/
        $this->form_validation->set_rules('comments', lang('comments'), 'trim|xss_clean');
        $this->form_validation->set_rules('roles', lang('roles'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('completed', lang('completed'), 'trim');

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
            exit;
            $data["title"] = "Add / Edit New User";
            $data["content"] = "back_office/add_users";
            $data["pre_registration"] = $this->input->post("pre_registration");
            $data["featured_supplier"] = $this->input->post("featured_supplier");
            
            $data["fulfilled_by"] = $this->input->post("fulfilled_by");

            if ($this->input->post('id') != '') {
                $cond = array('id_user' => $this->input->post('id'));
                $data["id"] = $this->input->post('id');
                $data["info"] = $this->fct->getonerecord('user', $cond);
            }
            $data["roles"] = $this->fct->getAll('roles', 'sort_order');
            $data["discount_groups"] = $this->fct->getAll('discount_groups', 'sort_order');
            /*if($this->input->post('phone')!=""){
            $data["info"]["phone"]=implode('-',$this->input->post('phone'));}
            if($this->input->post('fax')!=""){
            $data["info"]["fax"]=implode('-',$this->input->post('fax'));}
            if($this->input->post('mobile')!=""){
            $data["info"]["mobile"]=implode('-',$this->input->post('mobile'));}*/

            $this->load->view('back_office/template', $data);
        } else {
            $_data = array(
                'id_discount_groups' => $this->input->post('discount_group'),
                'id_roles' => $this->input->post('roles'));
            $_data['email'] = $this->input->post("email");

            $_data['username'] = $this->input->post("username");
            $_data['license_number'] = $this->input->post("license_number");
            if ($this->input->post('roles') == 4 || $this->input->post('roles') == 5) {
                $_data['trading_name'] = $this->input->post("trading_name");
            }
            $_data['completed'] = $this->input->post("completed");
            $_data['salutation'] = $this->input->post("salutation");
            $_data['first_name'] = $this->input->post("first_name");
            $_data['last_name'] = $this->input->post("last_name");
            $_data['city'] = $this->input->post("city");
            $_data['position'] = $this->input->post("position");
            $_data['id_countries'] = $this->input->post("id_countries");
            $_data['id_brands'] = $this->input->post("id_brands");
            $token = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
            $_data['token'] = $token;
            $_data["pre_registration"] = $this->input->post("pre_registration");
            $_data["featured_supplier"] = $this->input->post("featured_supplier");
            $_data["fulfilled_by"] = $this->input->post("fulfilled_by");
            

            /*$_data['website_name'] = website_name();*/
            $_data['agree'] = 1;
            $_data['name'] = $_data['first_name'] . ' ' . $_data['last_name'];
            $_data['address_1'] = trimEmptyLines($this->input->post("address_1"));
            $_data['id_users_areas'] = $this->input->post("id_users_areas");
            $_data['id_account_manager'] = $this->input->post("id_account_manager");
            $_data['address_2'] = $this->input->post("address_2");
            $_data['fax'] = $this->input->post("fax");
            $_data['status'] = $this->input->post("status");

            /*			$phone = $this->input->post("phone");
                        $_data['phone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];*/

            $phone = $this->input->post("phone");
            $_data['phone'] = $phone;

            /*			if($this->fax()){

                        $fax = $this->input->post("fax");
                        $_data['fax'] = $fax[0].'-'.$fax[1].'-'.$fax[2];}else{
                        $_data['fax'] = 0;	}*/

            $_data['fax'] = $this->input->post("fax");


            /*			if($this->mobile()){
                        $mobile = $this->input->post("mobile");
                        $_data['mobile'] = $mobile[0].'-'.$mobile[1].'-'.$mobile[2];}else{
                        $_data['mobile'] = 0;	}*/

            $_data['mobile'] = $this->input->post("mobile");

            if ($this->input->post('id') != '') {
                if ($this->input->post('password') != '') {
                    $_data['password'] = md5($this->input->post('password'));
                }
            } else {
                $_data['password'] = md5($this->input->post('password'));
            }
            ////////////////////////////////SUPPLIER INFO//////////////////////////////


            if ($this->input->post('id') != '') {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $getUserInfo = $this->fct->getonerecord('user', array('id_user' => $this->input->post('id')));
                /*	if($getUserInfo['status']!=$this->input->post('status')){
                    if($this->input->post('status')==1){
                        $_data['expire_date'] = date('Y-m-d',strtotime('+'.getExpireDate().' days'));}
                    }else{
                        $_data['expire_date']=$this->fct->date_in_formate($this->input->post('expire_date'));
                        }*/

                $this->db->where('id_user ', $this->input->post('id'));
                $this->db->update('user', $_data);
                $new_id = $this->input->post('id');
                $this->session->set_userdata('success_message', 'Information was updated successfully');
                if (isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
                    $user = $this->fct->getonerow("user", array("id_user" => $new_id));
                    /*		if($this->input->post('status') == 1) {
                                $id = 8;
                            }
                            else {
                                $id = 7;
                            }*/
                    $this->load->model("send_emails");
                    $this->send_emails->inform_client($user);
                }
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert('user', $_data);
                $new_id = $this->db->insert_id();
                $this->ecommerce_model->createChecklistCategories($new_id);
                $this->session->set_userdata('success_message', 'Information was inserted successfully');
            }


            if ((isset($_POST['pre_registration']) && $_POST['pre_registration'] == 1 && !isset($getUserInfo['pre_registration'])) || (isset($_POST['pre_registration']) && $_POST['pre_registration'] == 1 && isset($getUserInfo['pre_registration']) && $getUserInfo['pre_registration'] == 0)) {
                $user = $this->fct->getonerow("user", array("id_user" => $new_id));
                $this->load->model("send_emails");
                $this->send_emails->inform_pre_registration($user);
            }

            /*	if(isset($_POST['categories']) && !empty($_POST['categories'])) {
                    $categories = $_POST['categories'];
                }

                $this->users_categories_m->insert_user_categories_admin($new_id,$categories);*/

            if ($this->input->post('submit') == 'Save and Continue') {

                redirect(site_url("back_office/users/edit/" . $new_id));
            }
            if ($this->input->post('submit') == 'Save Changes') {
                if ($this->session->userdata("back_link") != "") {
                    $back_link = $this->session->userdata("back_link");
                    redirect($back_link);
                } else {
                    redirect(site_url("back_office/users"));
                }
            }
        }
    }

    public function delete($id)
    {

        /*$_data=array('deleted'=>1,
        'deleted_date'=>date("Y-m-d h:i:s"));
        $this->db->where('id_user',$id);
        $this->db->update('user',$_data);*/
        $this->deleteUser($id);
        $this->session->set_userdata('success_message', 'Information was deleted successfully');
        redirect(site_url('back_office/users'));

    }

    public function delete_all()
    {
        $cehcklist = $this->input->post('cehcklist');
        $check_option = $this->input->post('check_option');

        if ($check_option == "delete_all") {
            if (count($cehcklist) > 0) {
                for ($i = 0; $i < count($cehcklist); $i++) {
                    if ($cehcklist[$i] != '') {
                        /*$_data=array('deleted'=>1,
                        'deleted_date'=>date("Y-m-d h:i:s"));
                        $this->db->where('id_user',$cehcklist[$i]);
                        $this->db->update('user',$_data);	*/
                        $this->deleteUser($cehcklist[$i]);
                    }
                }
            }
            $this->session->set_userdata('success_message', 'Informations were deleted successfully');
        }
        redirect(site_url('back_office/users'));
    }

    function deleteUser($id_user)
    {
        $cond = array("id_user" => $id_user);
        $updata['deleted'] = 1;
        $this->db->where($cond);
        $this->db->update("user", $updata);
        $q = "DELETE FROM line_items WHERE id_orders IN (SELECT id_orders FROM orders WHERE id_user = " . $id_user . ")";
        $this->db->query($q);
        /*	$this->db->delete("orders",$cond);
            $this->db->delete("address_book",$cond);*/
        $this->db->delete("compare_products", $cond);
        $this->db->delete("favorites", $cond);
        $this->db->delete("shopping_cart", $cond);
        return TRUE;
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get('table-1') as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array('sort_order' => $i);
            $this->db->where('id_user', $sort[$i]);
            $this->db->update('user', $_data);
        }
    }

    public function submit_brand()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit supplier info";
        $this->form_validation->set_rules('name', lang('name'), 'trim|required|xss_clean');
        /*$this->form_validation->set_rules('trading_licence',lang('trading_licence'), 'trim|required|xss_clean');*/
        //////////////Supplier Inff///////////////////////////////////////


        if ($this->form_validation->run() == FALSE) {

            $this->brand_info($this->input->post("id_user"));

        } else {


/////////////////////USER//////////////////////
            $user_id = $this->input->post("id_user");


            ////////////////////////////////SUPPLIER INFO//////////////////////////////
            $_data["id_user"] = $this->input->post("id_user");
            $_data["name"] = $this->input->post("name");
            $_data["mobile"] = $this->input->post("mobile");
            $_data["position"] = $this->input->post("position");
            $_data["overview"] = $this->input->post("overview");

            if (!empty($_FILES["image"]["name"])) {

                if ($this->input->post("id_supplier_info") != "") {
                    $cond_image = array("id_supplier_info" => $this->input->post("id_supplier_info"));
                    $old_image = $this->fct->getonecell("supplier_info", "supplier_image", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/supplier_info/' . $old_image)) {
                        unlink("./uploads/supplier_info/" . $old_image);
                        $sumb_val1 = explode(",", "112x122");
                        foreach ($sumb_val1 as $key => $value) {
                            if (file_exists("./uploads/supplier_info/" . $value . "/" . $old_image)) {
                                unlink("./uploads/supplier_info/" . $value . "/" . $old_image);
                            }
                        }
                    }
                }
                $image1 = $this->fct->uploadImage("image", "supplier_info");
                $this->fct->createthumb($image1, "supplier_info", "112x122");
                $_data["image"] = $image1;
            }
            if (!empty($_FILES["logo"]["name"])) {
                if ($this->input->post("id_supplier_info") != "") {
                    $cond_image = array("id_supplier_info" => $this->input->post("id_supplier_info"));
                    $old_image = $this->fct->getonecell("supplier_info", "supplier_logo", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/supplier_info/' . $old_image)) {
                        unlink("./uploads/supplier_info/" . $old_image);
                    }
                }
                $image1 = $this->fct->uploadImage("logo", "supplier_info");
                $_data["logo"] = $image1;
            }
            if (!empty($_FILES["banner_image"]["name"])) {

                if ($this->input->post("id_supplier_info") != "") {

                    $cond_image = array("id_supplier_info" => $this->input->post("id_supplier_info"));
                    $old_image = $this->fct->getonecell("supplier_info", "banner_image", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/supplier_info/' . $old_image)) {
                        unlink("./uploads/supplier_info/" . $old_image);
                        $sumb_val1 = explode(",", "850x160");
                        foreach ($sumb_val1 as $key => $value) {
                            if (file_exists("./uploads/supplier_info/" . $value . "/" . $old_image)) {
                                unlink("./uploads/supplier_info/" . $value . "/" . $old_image);
                            }
                        }
                    }
                }
                $image1 = $this->fct->uploadImage("banner_image", "supplier_info");
                $this->fct->createthumb($image1, "supplier_info", "850x160");
                $_data["banner_image"] = $image1;
            }
            $_data["set_as_featured"] = $this->input->post("set_as_featured");
            $_data["video"] = $this->input->post("video");

            for ($i = 1; $i < 5; $i++) {
                $image = "slide_" . $i;

                if (!empty($_FILES[$image]["name"])) {
                    $image1 = $this->fct->uploadImage($image, "supplier_info");
                    $this->fct->createthumb($image1, "supplier_info", "850x160");

                    $_data['slide_' . $i] = $image1;
                }
            }
            if ($this->input->post("id_supplier_info") != "") {

                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_supplier_info", $this->input->post("id_supplier_info"));
                $this->db->update('supplier_info', $_data);
                $new_id = $this->input->post("id_supplier_info");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert('supplier_info', $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }

            $update_user["id_brands"] = $this->input->post("id_brands");
            $update_user["id_supplier_info"] = $new_id;

            $this->db->where("id_user", $this->input->post("id_user"));
            $this->db->update('user', $update_user);

            if ($this->session->userdata("admin_redirect_url")) {
                redirect($this->session->userdata("admin_redirect_url"));
            } else {
                redirect(site_url("back_office/users/brand_info/" . $user_id));
            }

        }

    }

    public function brand_info($id, $obj = "")
    {
        if ($this->acl->has_permission('supplier_info', 'edit')) {
            $data["title"] = "Edit brand info";
            $data["content"] = "back_office/supplier_info/brand_info";

            $user = $this->fct->getonerecord('user', array('id_user' => $id));
            $data['info'] = $user;

            $cond = array("id_supplier_info" => $user['id_supplier_info']);
            $data["supplier_info"] = $this->fct->getonerecord('supplier_info', $cond);


            if (empty($data["supplier_info"])) {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert('supplier_info', $_data);
                $new_id = $this->db->insert_id();
                $cond2 = array("id_supplier_info" => $new_id);
                $data["supplier_info"] = $this->fct->getonerecord('supplier_info', $cond2);
                $user_update['id_supplier_info'] = $new_id;
                $this->db->where(array('id_user' => $user['id_user']));
                $this->db->update('user', $user_update);
            }
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_file()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/supplier_info/" . $image)) {
            unlink("./uploads/supplier_info/" . $image);
        }
        $q = " SELECT thumb,thumb_val
        FROM `content_type_attr`
        WHERE id_content = (SELECT id_content FROM user WHERE name = 'user')
        AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/user/" . $value . "/" . $image)) {
                    unlink("./uploads/user/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_user", $id);
        $this->db->update("user", $_data);
        echo 'Done!';
    }

    public function submit_privi()
    {
        $id = $this->input->post('id');
        $cehcklist = $this->input->post('cehcklist');
        $content_type = $this->fct->getAll_1('content_type', 'sort_order');
        if (count($cehcklist) > 0) {
            for ($j = 0; $j < 11; $j++) {
        //if($cehcklist[$i] != ''){

                //$j= $cehcklist[$i];
                $section = $this->input->post('section' . $j);
                $read = $this->input->post('read' . $j);
                $write = $this->input->post('write' . $j);
                $delete = $this->input->post('delete' . $j);

                $_data = array(
                    'read_p' => $read,
                    'write_p' => $write,
                    'delete_p' => $delete);
                $this->db->where("`id_user` = '" . $id . "' AND `id_content` = '" . $section . "'");
                $this->db->update('priviledge', $_data);
//}
            }
            $this->session->set_userdata('success_message', 'Informations were updated successfully');
        }
        redirect(site_url('back_office/users/priviledge/' . $id));
    }

}