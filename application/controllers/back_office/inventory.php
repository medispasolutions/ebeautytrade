<?
class Inventory extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="inventory";
$this->load->model("inventory_m");
 $this->lang->load("admin"); 
}


public function index($page=""){
	
	
	
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	
$order="";
if($order == "")
$order ="sort_order";
$data["title"]="List Inventory";
if(!empty($page)){
$data["title"]="List Inventory Reports";}
$data["content"]="back_office/inventory/list";
//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}


$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$cond = array();
$cond['All']="All";
if($page==""){
$url = site_url('back_office/inventory?pagination=on');}else{
$url = site_url('back_office/inventory/index/'.$page.'?pagination=on');	}

$data['page']=$page;
if(isset($_GET['product_id'])) {
	$product_id = $_GET['product_id'];
	$url .= '&product_id='.$product_id;
	if(!empty($product_id))
	$cond['id_products'] = $product_id;
}
$cond['order_type'] = 'all';
if(isset($_GET['order_type'])) {
	$order_type = $_GET['order_type'];
	$url .= '&order_type='.$order_type;
	if(!empty($order_type))
	$cond['order_type'] = $order_type;
}

if(isset($_GET['brand'])) {
	$brand = $_GET['brand'];
	$url .= '&brand='.$brand;
	if(!empty($brand)){
		
	$cond['id_brands'] = $brand;}
}

if(isset($_GET['search'])) {
	$search = $_GET['search'];
	$url .= '&search='.$search;
}

if(isset($_GET['brands'])) {
	$brands = $_GET['brands'];
	$url .= '&brands='.$brands;
	if(!empty($brands)){
		
	$cond['id_brands'] = $brands;}
}


if(isset($_GET['product_name'])) {
	$product_name = $_GET['product_name'];
	$url .= '&product_name='.$product_name;
	if(!empty($product_name))
	$cond['product_name'] = $product_name;
}

if(isset($_GET['sku'])) {
	$product_sku = $_GET['sku'];
	$url .= '&sku='.$product_sku;
	if(!empty($product_sku))
	$cond['sku'] = $product_sku;
}

if(isset($_GET['supplier']) && $_GET['supplier']!="all") {
	$id_supplier = $_GET['supplier'];
	$url .= '&supplier='.$id_supplier;
	if(!empty($id_supplier)){
	$brands=$this->fct->getAll_cond('brands','sort_order asc',array('id_user'=>$id_supplier));
	$ids=array();
foreach($brands as $val){
	array_push($ids,$val['id_brands']);}
	$cond['brands']=implode(',',$ids);}
}

if(isset($_GET['category'])) {
	$category = $_GET['category'];
	$url .= '&category='.$category;
	if(!empty($category))
	$cond['category'] = $category;
}

if(isset($_GET['sub_category'])) {
	$sub_category = $_GET['sub_category'];
	$url .= '&sub_category='.$sub_category;
	if(!empty($sub_category))
	$cond['category'] = $sub_category;	
}

if($this->input->get('sales')!="" && $this->input->get('sales')!="2") {
	$sales = $_GET['sales'];
	$url .= '&sales='.$sales;
	$cond['sales'] = $sales;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}

if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}

$cond['admin']=1;
$cond['inventory']=1;

// pagination  start :
$count_news = $this->ecommerce_model->getInventory($cond);

$show_items = ($show_items == 'All') ? $count_news : $show_items;

$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {
$config['per_page']=$count_news;
$page=0;}
$data['info'] = $this->ecommerce_model->getInventory($cond,$config['per_page'],$page);
$data['supplier_admin']=checkIfSupplier_admin();

$data['supplier_admin']=1;



if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {	
$data['info']['order_type']=$cond['order_type'];
$this->export_fct->inventoryExportToExcel($data['info']);
}

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function profits($page=""){

$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	
$order="";
if($order == "")
$order ="sort_order";
$data["title"]="List Sales Report";
$data["content"]="back_office/inventory/profits/list";
//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$cond = array();
$cond['All']="All";

$url = site_url('back_office/inventory/profits?pagination=on');

$cond['status'] = "paid";

if(isset($_GET['product_id'])) {
	$product_id = $_GET['product_id'];
	$url .= '&product_id='.$product_id;
	if(!empty($product_id))
	$cond['id_products'] = $product_id;
}

if(isset($_GET['supplier'])) {
	$supplier = $_GET['supplier'];
	$url .= '&supplier='.$supplier;
	if(!empty($supplier))
	$cond['supplier'] = $supplier;
}

if(isset($_GET['account_manager'])) {
	$account_manager = $_GET['account_manager'];
	$url .= '&account_manager='.$account_manager;
	if(!empty($account_manager))
	$cond['account_manager'] = $account_manager;
}

if(isset($_GET['country'])) {
	$country = $_GET['country'];
	$url .= '&country='.$country;
	if(!empty($country))
	$cond['country'] = $country;
}

if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo();
	$cond['supplier']=$user['id_user'];
	}

if(isset($_GET['id_brands'])) {
	$id_brands = $_GET['id_brands'];
	$url .= '&id_brands='.$id_brands;
	if(!empty($id_brands))
	$cond['id_brands'] = $id_brands;
}

if(isset($_GET['brands'])) {
	$brands = $_GET['brands'];
	$url .= '&brands='.$brands;
	if(!empty($brands))
	$cond['id_brands'] = $brands;
}

if(isset($_GET['payment_method'])) {
	$payment_method = $_GET['payment_method'];
	$url .= '&payment_method='.$payment_method;
	if(!empty($payment_method))
	$cond['payment_method'] = $payment_method;
}

if(isset($_GET['status'])) {
	$status = $_GET['status'];
	$url .= '&status='.$status;
	if(!empty($status))
	$cond['status'] = $status;
}


$cond['from_date'] = date("Y-m-d",strtotime(date("Y-m-d")." - 1 month"));

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}
$data['from_date']=$this->fct->date_out_formate($cond['from_date']);

$cond['to_date'] = date("Y-m-d",strtotime(date("Y-m-d")." + 1 day"));
if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$to_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
	
	
}

$data['to_date']=$this->fct->date_out_formate($cond['to_date']);



// pagination  start :
$count_news = $this->ecommerce_model->getProfits($cond);

$show_items = ($show_items == 'All') ? $count_news : $show_items;
$show_items=$count_news;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;

$data['info'] = $this->ecommerce_model->getProfits($cond,$config['per_page'],$page);




if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {	
$this->export_fct->salesReportExportToExcel($data['info']);
}

if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'invoice') {	

if(!empty($data['info']['brands'])){
if($this->input->get('supplier')!=""){
	
$new_order=$this->admin_fct->generateInvocie($data['info']);
$order=$this->fct->getonerecord('invoices',array('id_invoices'=>$new_order));
$this->session->set_userdata('success_message',"Generate invoice is completed");
redirect(site_url('back_office/inventory/view_invoice_details/'.$order['id_invoices'].'/'.$order['rand']));}else{

	$this->session->set_userdata('error_message',"You should choose a supplier.");
	redirect($url);}}else{
	
	$this->session->set_userdata('error_message',"Srry!Your order is empty.");
	redirect($url);	}
}

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function invoices($page=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	
$order="";
if($order == "")
$order ="sort_order";
$data["title"]="List Invoices";
$data["content"]="back_office/inventory/invoices/list";
//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$cond = array();
$cond['All']="All";

$url = site_url('back_office/inventory/invoices?pagination=on');



if(isset($_GET['product_id'])) {
	$product_id = $_GET['product_id'];
	$url .= '&product_id='.$product_id;
	if(!empty($product_id))
	$cond['id_products'] = $product_id;
}

if(isset($_GET['supplier'])) {
	$supplier = $_GET['supplier'];
	$url .= '&supplier='.$supplier;
	if(!empty($supplier))
	$cond['supplier'] = $supplier;
}

if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo();
	$cond['supplier']=$user['id_user'];
	}

if(isset($_GET['id_brands'])) {
	$id_brands = $_GET['id_brands'];
	$url .= '&id_brands='.$id_brands;
	if(!empty($id_brands))
	$cond['id_brands'] = $id_brands;
}

if(isset($_GET['status'])) {
	$status = $_GET['status'];
	$url .= '&status='.$status;
	if(!empty($status))
	$cond['status'] = $status;
}


if(isset($_GET['invoice'])) {
	$invoice = $_GET['invoice'];
	$url .= '&invoice='.$invoice;
	if(!empty($invoice))
	$cond['invoice'] = $invoice;
}



if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}



if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}





// pagination  start :
$count_news = $this->ecommerce_model->getInvoices($cond);

$show_items = ($show_items == 'All') ? $count_news : $show_items;
$show_items=$count_news;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;

$data['info'] = $this->ecommerce_model->getInvoices($cond,$config['per_page'],$page);


if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {	
$this->export_fct->invoicesExportToExcel($data);
}

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function invoice($id,$rand="")
{
$cond['id_invoices']=$id;
$cond['rand']=$rand;
$data["info"]=$this->ecommerce_model->getInvoice($cond);
$data["userData"]=$data["info"]['user'];
$data['orderData'] = $data["info"]['invoice'];


		if(!empty($data["info"])) {
			$data['print']=1;
		$template = $this->load->view('emails/supplier_invoice',$data,true);;
		$data['template'] = $template;
		$this->load->view('back_office/inventory/invoices/invoice',$data);
		}else {
		$this->session->set_flashdata('error_message',lang('please_login'));
		redirect(route_to('user/login'));
	}
}

public function view_invoice_details($id,$rand){
if ($this->acl->has_permission('orders','index')){	
$data["title"]="details";
$data["content"]="back_office/inventory/invoices/add";
$cond=array("id_orders"=>$id);
$updata['readed']=1;
$this->db->where($cond);
$this->db->update('orders',$updata);
$data["id"]=$id;
$cond['id_invoices']=$id;
$cond['rand']=$rand;
$data["info"]=$this->ecommerce_model->getInvoice($cond);

$data["userData"]=$data["info"]['user'];
$data['order_details'] = $data["info"];
$data['order_details'] = $data["info"]['invoice'];
/*print '<pre>';
print_r($data['order_details']);
exit;*/
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function updateInvoice()
{
	//print '<pre>';print_r($_POST);exit;
	$id_invoices = $this->input->post('id_invoices');
	$rand = $this->input->post('rand');
	$status = $this->input->post('status');
	
	$update['updated_date'] = date('Y-m-d h:i:s');
	$update['status'] = $status;
	$this->db->where(array('id_invoices'=>$id_invoices,'rand'=>$rand));
	$this->db->update('invoices',$update);


		if(isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
					
	$cond['id_invoices']=$id_invoices;
	$cond['rand']=$rand;
	$orderData=$this->ecommerce_model->getInvoice($cond);
	
				$this->load->model('send_emails');
				$this->send_emails->sendUpdateInvoiceToUser($orderData);}

	$this->session->set_userdata("success_message","Order status is updated.");
	redirect(site_url("back_office/inventory/view_invoice_details/".$id_invoices.'/'.$rand));
}

public function supplier(){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	
$order="";
if($order == "")
$order ="sort_order";
$data["title"]="List Inventory";
$data["content"]="back_office/inventory/list";

//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$cond = array();
$cond['All']="All";

$url = site_url('back_office/inventory/supplier?pagination=on');

$data['page']=$page;

if(isset($_GET['product_id'])) {
	$product_id = $_GET['product_id'];
	$url .= '&product_id='.$product_id;
	if(!empty($product_id))
	$cond['id_products'] = $product_id;
}

if(isset($_GET['brand'])) {
	$brand = $_GET['brand'];
	$url .= '&brand='.$brand;
	if(!empty($brand)){
		
	$cond['id_brands'] = $brand;}
}

if(isset($_GET['product_name'])) {
	$product_name = $_GET['product_name'];
	$url .= '&product_name='.$product_name;
	if(!empty($product_name))
	$cond['product_name'] = $product_name;
}

if(isset($_GET['sku'])) {
	$product_sku = $_GET['sku'];
	$url .= '&sku='.$product_sku;
	if(!empty($product_sku))
	$cond['sku'] = $product_sku;
}

if(isset($_GET['id_supplier'])) {
	$id_supplier = $_GET['id_supplier'];
	$url .= '&id_supplier='.$id_supplier;
	if(!empty($id_supplier))
	$cond['id_user'] = $id_supplier;
}


if(isset($_GET['category'])) {
	$category = $_GET['category'];
	$url .= '&category='.$category;
	if(!empty($category))
	$cond['category'] = $category;
}

if(isset($_GET['sub_category'])) {
	$sub_category = $_GET['sub_category'];
	$url .= '&sub_category='.$sub_category;
	if(!empty($sub_category))
	$cond['category'] = $sub_category;	
}

if($this->input->get('sales')!="" && $this->input->get('sales')!="2") {
	$sales = $_GET['sales'];
	$url .= '&sales='.$sales;
	$cond['sales'] = $sales;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}

if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}

$cond['admin']=1;
$cond['inventory']=1;

// pagination  start :
$count_news = $this->ecommerce_model->getSupplierInventory($cond);

$show_items = ($show_items == 'All') ? $count_news : $show_items;

$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;

$data['info'] = $this->ecommerce_model->getInventory($cond,$config['per_page'],$page);

if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {	
$this->export_fct->inventoryExportToExcel($data['info']);
}

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delivery_order($order=""){
$this->session->unset_userdata("admin_redirect_url");
$order="";
if ($this->acl->has_permission('products','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="Delivery Order";
$data["content"]="back_office/inventory/delivery_order/list";

//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$url = site_url('back_office/inventory/delivery_order').'?pagination=on';
$cond = array();
if(isset($_GET['delivery_order'])) {
	$delivery_order = $_GET['delivery_order'];
	$url .= '&delivery_order='.$delivery_order;
	if(!empty($delivery_order))
	$cond['delivery_order'] = $delivery_order;
}

if(isset($_GET['name'])) {

	$name = $_GET['name'];
	$url .= '&name='.$name;
	if(!empty($name))
	$cond['name'] = $name;
}

if(isset($_GET['status'])) {
	$status = $_GET['status'];
	$url .= '&status='.$status;
	if(!empty($status))
	$cond['status'] = $status;	
}

if(isset($_GET['type'])) {
	$type = $_GET['type'];
	$url .= '&type='.$type;
	if(!empty($type))
	$cond['type'] = $type;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}

if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo();
	$cond['id_user']=$user['id_user'];}
	$cond['id_role']=5;
	

if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}
$cond['return']=0;
// pagination  start :
$count_news = $this->admin_fct->getDeliveryOrders($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->admin_fct->getDeliveryOrders($cond,$config['per_page'],$page);


$data['url']=$url;
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delivery_request($order=""){
$this->session->unset_userdata("admin_redirect_url");
$order="";
if ($this->acl->has_permission('products','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List inventory";
$data["content"]="back_office/inventory/delivery_request/list";

//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$url = site_url('back_office/inventory/delivery_request').'?pagination=on';
$cond = array();
if(isset($_GET['delivery_order'])) {
	$delivery_order = $_GET['delivery_order'];
	$url .= '&delivery_order='.$delivery_order;
	if(!empty($delivery_order))
	$cond['delivery_order'] = $delivery_order;
}

if(isset($_GET['name'])) {

	$name = $_GET['name'];
	$url .= '&name='.$name;
	if(!empty($name))
	$cond['name'] = $name;
}

if(isset($_GET['status'])) {

	$status = $_GET['status'];

	$url .= '&status='.$status;
	if(!empty($status))
	$cond['status'] = $status;
	
}

if(isset($_GET['type'])) {
	$type = $_GET['type'];
	$url .= '&type='.$type;
	if(!empty($type))
	$cond['type'] = $type;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}



if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}
$cond['id_role']=1;
$cond['id_role_1']=1;
$cond['return']=0;
// pagination  start :
$count_news = $this->admin_fct->getDeliveryOrders($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->admin_fct->getDeliveryOrders($cond,$config['per_page'],$page);

$data['url']=$url;

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function request($section=""){

$cond = array();
$data['section']=$section;

if($section=="stock"){
$type=1;	
$export=0;
$page_title='Stock Request';
$order_name='R-Q';
$form='export';
$section_name='Stock Request';
}

if($section=="delivery_note"){
$type=1;	
$export=1;
$page_title='Delivery Note';
$order_name='D-N';
$form='updateOrder';
$section_name='Delivery Note';
}

if($section=="lpo"){
$type=2;	
$export=0;
$page_title='LPO';
$order_name='D-P';
$form='export';
$section_name='LPO';
}

if($section=="invoices"){
$type=0;	
$export=1;
$page_title='Purchases Invoices';
$order_name='D-I';
$form='updateOrder';
$section_name='Purchase Invoice';
}



$data['type']=$type;
$data['form']=$form;
$data['export']=$export;
$data['page_title']=$page_title;
$data['order_name']=$order_name;
$data['section_name']=$section_name;
$cond['type']=$type;
$cond['export']=$export;
$cond['section']=$section;
		
$this->session->unset_userdata("admin_redirect_url");
$order="";
if ($this->acl->has_permission('products','index')){	
if($order == "")
$order ="sort_order";
$data["title"]=$page_title;
$data["content"]="back_office/inventory/delivery_request/list";

//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$url = site_url('back_office/inventory/request/'.$section).'?pagination=on';

if(isset($_GET['delivery_order'])) {
	$delivery_order = $_GET['delivery_order'];
	$url .= '&delivery_order='.$delivery_order;
	if(!empty($delivery_order))
	$cond['delivery_order'] = $delivery_order;
}

if(isset($_GET['name'])) {

	$name = $_GET['name'];
	$url .= '&name='.$name;
	if(!empty($name))
	$cond['name'] = $name;
}

if(isset($_GET['trading_name'])) {
	$trading_name = $_GET['trading_name'];
	$url .= '&trading_name='.$trading_name;
	if(!empty($trading_name))
	$cond['trading_name'] = $trading_name;
}

if(isset($_GET['status'])) {

	$status = $_GET['status'];

	$url .= '&status='.$status;
	if(!empty($status))
	$cond['status'] = $status;
	
}



if(isset($_GET['exported'])) {

	$exported = $_GET['exported'];

	$url .= '&exported='.$exported;
	if(!empty($exported))
	$cond['exported'] = $exported;
	
}

if(isset($_GET['type'])) {
	$type = $_GET['type'];
	$url .= '&type='.$type;
	if(!empty($type))
	$cond['type'] = $type;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}



if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}
/*$cond['id_role']=1;*/
$cond['id_role_1']=1;
$cond['return']=0;


// pagination  start :
$count_news = $this->admin_fct->getDeliveryOrders($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->admin_fct->getDeliveryOrders($cond,$config['per_page'],$page);

$data['url']=$url;

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function getDeliveryOrders($order=""){
$count_news = $this->admin_fct->getDeliveryOrders($cond);
$results = $this->admin_fct->getDeliveryOrders($cond,$count_news,0);

	}
	
public function getDeliveryOrdersToReturn(){

$return['result']=5;
$return['html']="";
$brands_arr=array();
if($this->input->post('id_user')!=""){
	$cond['id_user']=$this->input->post('id_user');
if($this->input->post('id_brands')==""){	

$brands = $this->fct->getAll_cond('brands','title asc',$cond);
foreach($brands as $val){
array_push($brands_arr,$val['id_brands']);}}
$cond['brands']=$brands_arr;
	}
	
if($this->input->post('brands')!=""){
	$cond['id_brands']=$this->input->post('brands');
	}
	
if($this->input->post('id_delivery_order')!=""){
	$cond['delivery_order']=$this->input->post('id_delivery_order');
	}	
	
if($this->input->post('trading_name')!=""){
	$cond['trading_name']=$this->input->post('trading_name');
	}		
	
if($this->input->post('from_date')!=""){
	$cond['from_date']=$this->input->post('from_date');
	}
	
if($this->input->post('to_date')!=""){
	$cond['to_date']=$this->input->post('to_date');
	}
	
if($this->input->post('order_type')!=""){
	$cond['order_type']=$this->input->post('order_type');
	}				

$cond['available_orders']=1;
$cond['join']=2;
/*$cond['threshold']=1;*/
$cc= $this->admin_fct->getDeliveryOrders($cond);
$data['info'] = $this->admin_fct->getDeliveryOrders($cond,$cc,0);

$data['id_stock_return']=$this->input->post('id_stock_return');

$return['result']=1;
$return['html']=$this->load->view('back_office/blocks/delivery_orders',$data,true);


die(json_encode($return));
 
}
	
	
public function order_return($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('orders','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List orders";
$data["content"]="back_office/inventory/order_return/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$data["show_items"] = $show_items;
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));

$url = site_url('back_office/inventory/order_return').'?pagination=on';
$cond = array();
if(isset($_GET['order_id'])) {
	$order_id = $_GET['order_id'];
	$url .= '&order_id='.$order_id;
	if(!empty($order_id))
	$cond['orders.id_orders'] = $order_id;
}

if(isset($_GET['name'])) {
	$name = $_GET['name'];
	$url .= '&name='.$name;
	if(!empty($name))
	$cond['user.name'] = $name;
}

if(isset($_GET['trading_name'])) {
	$trading_name = $_GET['trading_name'];
	$url .= '&trading_name='.$trading_name;
	if(!empty($name))
	$cond['user.trading_name'] = $trading_name;
}


if((isset($_GET['name']) && !empty($_GET['name'])) || (isset($_GET['order_id']) && !empty($_GET['order_id']))){
// pagination  start :
$count_news = $this->ecommerce_model->getOrders($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->ecommerce_model->getOrders($cond,$config['per_page'],$page);}else{
	$data['info']=array();
	}
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}



public function manage_order($id){
if ($this->acl->has_permission('orders','index')){	
$data["title"]="MR-Order#".$id;
$data["content"]="back_office/inventory/order_return/add";
$cond=array("id_orders"=>$id);
$updata['readed']=1;
$this->db->where($cond);
$this->db->update('orders',$updata);
$data["id"]=$id;

$data["info"]=$this->ecommerce_model->getOrder($id);

$data["userData"]=$data["info"]['user'];
$data['order_details'] = $data["info"];
/*print '<pre>';
print_r($data['order_details']);
exit;*/
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}




public function checkQuantity()
	{
		
		
	$errors="<b>Can't process the changes!!</b>";	
	$bool=true;
	
	$ids_arr=$this->input->post('ids');


foreach($ids_arr as $key=>$val){


$line_item=$this->admin_fct->getDeliveryLineItem($val);
$type=$line_item['type'];
$stock=$line_item['stock'];
$product=$line_item['product'];
$purchase=$line_item['purchase'];
$qty=$line_item['quantity'];
if($type=="option"){$total_qty=$stock['quantity'];}else{$total_qty=$product['quantity'];}



if($total_qty<$qty){
	$bool=false;

	$errors .="<p>The Quantity '".$qty."' exceed the total available quantity '".$total_qty."' for ".$product['title']."</p>";
	}


}
	
if($bool) {
		return true;
}
else {
		$this->form_validation->set_message('checkQuantity',$errors);
		return false;
}
	}

public function checkReturnQuantity2()
	{
		

	}
	
	
public function checkReturnQuantity()
	{ 
		
	
	$errors="<b>Can't process the changes!!</b>";	
	$bool=true;
	
$ids_arr=$this->input->post('checklist');
	
$qty_return_arr=$this->input->post('qty');

foreach($ids_arr as $key=>$val){


$line_item=$this->admin_fct->getLineItem($val);

$stock=$line_item['stock'];
$product=$line_item['product'];
$qty_return=$qty_return_arr[$val];
	
/*$qty_line_item=$line_item['quantity']+$line_item['return_quantity'];*/
$qty_line_item=$line_item['quantity'];


if($qty_line_item<$qty_return){
	$bool=false;
	$errors .="<p>The Quantity '".$qty_return."' exceed the total available quantity '".$qty_line_item."' for ".$product['title']."</p>";
	}
}

if($bool) {
	$errors="<b>Can't process the changes!!</b>";	
	$bool=true;
	
$ids_arr=$this->input->post('checklist');
	
$return_qty_arr=$this->input->post('qty');
foreach($ids_arr as $key=>$val){


$line_item=$this->admin_fct->getLineItem($val);

$stock=$line_item['stock'];
$product=$line_item['product'];

$qty=$line_item['quantity'];
if(!empty($line_item['option'])){$total_qty=$stock['quantity'];}else{$total_qty=$product['quantity'];}
/*$remaining_qty=$total_qty-$line_item['return_quantity']+$return_qty_arr[$val];*/
$remaining_qty=$qty-$return_qty_arr[$val];


if($remaining_qty<0){
	
	$bool=false;
	$errors .="<p>The Quantity '".$qty."' exceed the total available quantity '".$total_qty."' for ".$product['title']."</p>";
	}
}}
	

	
if($bool) {
		return true;
}
else {
	
		$this->form_validation->set_message('checkReturnQuantity',$errors);
		return false;
}
	}
//////////////////////////////////////////UpdateReturnORder////////////////////////////
public function updateReturnOrder()
{	

  $allPostedVals = $this->input->post(NULL,TRUE);
  $id_order=$this->input->post('id_order');
  $status=$this->input->post('status');
  $rand=$this->input->post('rand');
  $ids_arr=$this->input->post('checklist');
  $qty_arr=$this->input->post('qty');
  $orderData=$this->fct->getOrder($id_order,$rand);
 
		$this->form_validation->set_rules('quantity','Quantities','callback_checkReturnQuantity[]');
		$this->form_validation->set_rules('quantity2','Quantities','callback_checkReturnQuantity2[]');	
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
					$this->manage_order($id_order,$rand);
					}else {
	



						// created order
					
						$order=$this->fct->getonerecord('orders',array('id_orders'=>$id_order,'rand'=>$rand));
						$checkOrder=$this->fct->getonerecord('return_orders',array('id_orders'=>$id_order));
						if(empty($checkOrder)){
						$order['created_date']=date('Y-m-d h:i:s');
						$this->db->insert('return_orders',$order);}

					 ////////////////////BACKUP////////////////
						foreach($ids_arr as $key=>$val) {
						$checkLinkItem=$this->fct->getonerecord('return_orders_line_items',array('id_line_items'=>$val));
						if(empty($checkLinkItem)){
							if(isset($qty_arr[$key])){
						    $qty=$qty_arr[$key];
							$line_item=$this->admin_fct->getLineItem($val);
							$update_line_item['quantity'] = $line_item['quantity']+$line_item['return_quantity'];
							$update_line_item['price'] = $line_item['price'];
							$update_line_item['total_price'] = $line_item['total_price'];
							$this->db->insert('return_orders_line_items',$update_line_item);}}
						}

				$this->admin_fct->returnOrderProductsQuantities();
	
				$cond3['id_order']=$id_order;
				$cond3['rand']=$rand;
				$old_order=$order;
				$this->admin_fct->updateUserCredits($cond3,$order);
				//generate invoice templat
	/*			$orderData = $this->ecommerce_model->getOrder($order_id);
				$d['orderData']=$orderData;*/
				
				$this->ecommerce_model->updateAllUsersMiles($orderData['id_user']);
				
				$orderData=$this->fct->getOrder($id_order,$rand);
				if(isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
				$cond=array();
				$this->load->model('send_emails');
				$this->send_emails->sendUpdateOrderToUser($orderData);}
		
$this->session->set_userdata("success_message","Information was updated successfully");		

redirect(site_url("back_office/inventory/manage_order/".$id_order."/".$rand));
		}

}

//////////////////////////////////////////UpdateORder////////////////////////////
public function updateOrder()
{	

$cond = array();
$section=$this->input->post('section');
$data['section']=$section;

if($section=="stock"){
$type=1;	
$export=0;
$page_title='Stock Request';
$order_name='R-Q';
$form='export';
$section_name='Stock Request';
}

if($section=="delivery_note"){
$type=1;	
$export=1;
$page_title='Delivery Note';
$order_name='D-N';
$form='updateOrder';
$section_name='Delivery Note';
}

if($section=="lpo"){
$type=2;	
$export=0;
$page_title='LPO';
$order_name='D-P';
$form='export';
$section_name='Purchase Order';
}

if($section=="invoices"){
$type=0;	
$export=1;
$page_title='Purchases Invoices';
$order_name='D-I';
$form='updateOrder';
$section_name='Purchase Invoice';
}






  $allPostedVals = $this->input->post(NULL,TRUE);
  $id_delivery_order=$this->input->post('id_delivery_order');
  $section=$this->input->post('section');
  $payment_terms=$this->input->post('payment_terms');
  $order_conditions=$this->input->post('order_conditions');
  $delivery_date=$this->input->post('delivery_date');
  $status=$this->input->post('status');
  $rand=$this->input->post('rand');
  $cond['rand']=$rand;
  $orderData=$this->admin_fct->getDeliveryOrder($id_delivery_order,$cond);
  
 		$this->form_validation->set_rules('ids','ids','xss_clean');
  		if($this->input->post('status')!="" && ($orderData['status']==1 && $status!=1))
		$this->form_validation->set_rules('qty','Quantities','trim|xss_clean|callback_checkQuantity[]');
		if($section=="invoices"){
		$this->form_validation->set_rules('id_invoice','invoice#','trim|xss_clean|required');
		}
			
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
					$return['errors']='<div class="alert alert-error">'.validation_errors().'</div>';
					$this->view_delivery_request($section,$id_delivery_order,$rand,$return);
					}else {
		$orderData=$this->admin_fct->getDeliveryOrder($id_delivery_order,$cond);
	
		if($this->input->post('submit')=="Send to Supplier") {
$cond=array();
			
$orderData['section']=$section;
$orderData['form']=$form;
$orderData['type']=$type;
$orderData['export']=$export;
$orderData['page_title']=$page_title;
$orderData['order_name']=$order_name;
$orderData['section_name']=$section_name;

			$this->load->model('send_emails');
			$this->send_emails->inform_supplier_deliveryOrder($orderData);
			$this->session->set_userdata("success_message","Information was sent successfully");	}else{

	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$expire_date_arr=$this->input->post('expire_date');
	$total_cost=0;
	$total_price=0;
	
						// created order
						$order['updated_date'] = date('Y-m-d h:i:s');
						if($this->input->post('status')!=""){
						$order['status'] = $this->input->post('status');}
						if($this->input->post('type')!=""){
						$order['type'] = $this->input->post('type');}
						
						$cond['id_delivery_order']=$id_delivery_order;
						$cond['rand'] = $this->input->post('rand');

						//print '<pre>';print_r($order);
						$this->db->where($cond);
						$this->db->update('delivery_order',$order);
	if(isset($orderData['parent']['status']) && $orderData['parent']['status']==5){}else{
if($this->input->post('status')!="" && ($orderData['status']==1 && $status!=1)){
	$this->admin_fct->removeProductsQuantities();
	}
	
if($this->input->post('status')!="" && ($orderData['status']!=1 && $status==1)){
	$this->admin_fct->updateProductsQuantities();}}	
	
	if($this->input->post('submit')=="Export to Invoice") {
		
		
 $export_order['created_date']=$orderData['created_date'];
  $export_order['rand']=$orderData['rand'];
  $export_order['id_old_delivery_order']=$orderData['id_delivery_order'];
  $export_order['status']=2;
  if($this->input->post('type')!=""){
  $export_order['type']=$this->input->post('type');}else{
	$export_order['type']=0;  }
  $export_order['payment_terms']=$orderData['payment_terms'];
  $export_order['order_conditions']=$orderData['order_conditions'];
  $export_order['delivery_date']=$orderData['delivery_date'];
  $export_order['export']=1;
  $export_order['currency']=$orderData['currency'];
  $export_order['total_cost']=$orderData['total_cost'];
  $export_order['total_price']=$orderData['total_price'];
  $export_order['id_user']=$orderData['id_user'];
  $export_order['id_role']=$orderData['id_role'];		
  $export_order['return']=$orderData['return'];
  $export_order['invoice']=1;

 $this->db->insert('delivery_order',$export_order);
 $new_id = $this->db->insert_id();
		}

						$j=0;
						if(($this->input->post('status')!="" && ($orderData['status']!=1)) || $this->input->post('submit')=="Export to Invoice"){ 
						foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];
						$price=$price_arr[$key];
						$cost=$cost_arr[$key];
						
						$expire_date=$this->fct->date_in_formate($expire_date_arr[$key]);
						$total_cost=$total_cost+($qty*$cost);
						$total_price=$total_price+($qty*$price);
						$line_item=$this->admin_fct->getDeliveryLineItem($val);
	if($this->input->post('submit')=="Export to Invoice") {
	
	                     	$insert_line_item['created_date'] = date('Y-m-d h:i:s');
							$insert_line_item['quantity'] = $line_item['quantity'];
							$insert_line_item['cost'] = $line_item['cost'];;
							$insert_line_item['expire_date'] = $line_item['expire_date'];
							$insert_line_item['price'] = $line_item['price'];
							$insert_line_item['id_orders'] = $new_id;
							$insert_line_item['combination'] = $line_item['combination'];
							$insert_line_item['options'] = $line_item['options'];
							$insert_line_item['type'] = $line_item['type'];
							$insert_line_item['id_products'] = $line_item['id_products'];
							$insert_line_item['id_brands'] = $line_item['id_brands'];
							$insert_line_item['brands_title'] = $line_item['brands_title'];
							$insert_line_item['id_purchases'] = $line_item['id_purchases'];
							$insert_line_item['brands_title'] = $line_item['brands_title'];
							$insert_line_item['sku'] = $line_item['sku'];
							$insert_line_item['flag'] = $line_item['flag'];
							$this->db->insert('delivery_order_line_items',$insert_line_item);					
}else{
							$update_line_item['updated_date'] = date('Y-m-d h:i:s');
							$update_line_item['quantity'] = $qty;
							
					    	$update_line_item['cost'] = $cost;
							$update_line_item['price'] = $price;
							$update_line_item['expire_date'] = $expire_date;

						$this->db->where("id_delivery_order_line_items",$val);
						$this->db->update('delivery_order_line_items',$update_line_item);
							}
						
						
						
						}}
						if($this->input->post('id_old_delivery_order')!=""){
						$update_order['id_old_delivery_order']=$this->input->post('id_old_delivery_order');	
						}
						if($this->input->post('id_invoice')!=""){
						$update_order['id_invoice']=$this->input->post('id_invoice');	
						}
						
						$cond['id_delivery_order']=$id_delivery_order;
						$cond['rand'] = $this->input->post('rand');
						$update_order['total_cost']=$total_cost;
						$update_order['total_price']=$total_price;
						$update_order['status']=$this->input->post('status');
						if($this->input->post('submit')=="Export to Invoice"){
						$update_order['status']=1;	}
						$update_order['delivery_date']=$this->fct->date_in_formate($delivery_date);
						$update_order['payment_terms']=$payment_terms;
						$update_order['order_conditions']=$order_conditions;

						//print '<pre>';print_r($order);
						$this->db->where($cond);
						$this->db->update('delivery_order',$update_order);			
						
$this->session->set_userdata("success_message","Information was updated successfully");	
	}

				//generate invoice templat
	/*			$orderData = $this->ecommerce_model->getOrder($order_id);
					
				
				$d['orderData']=$orderData;*/
				
			


		
	
//if($orderData['id_role']==5){	
//redirect(site_url("back_office/inventory/view_delivery_order/".$export_section."/".$id_delivery_order."/".$rand));}else{
	if($this->input->post('submit')=="Export to Invoice") {	
redirect(site_url("back_office/inventory/view_delivery_request/invoices/".$new_id."/".$orderData['rand']));
		}else{
redirect(site_url("back_office/inventory/view_delivery_request/".$section."/".$id_delivery_order."/".$rand));	}
//}
		}

}

//////////////////////////////////////////Export To////////////////////////////
public function export()
{	
$cond = array();
$section=$this->input->post('section');

$data['section']=$section;

if($section=="stock"){
$type=1;	
$export=0;
$page_title='Stock Request';
$order_name='R-Q';
$form='export';
$section_name='Stock Request';
}

if($section=="delivery_note"){
$type=1;	
$export=1;
$page_title='Delivery Note';
$order_name='D-N';
$form='updateOrder';
$section_name='Delivery Note';
}

if($section=="lpo"){
$type=2;	
$export=0;
$page_title='LPO';
$order_name='D-P';
$form='export';
$section_name='LPO';
}

if($section=="invoices"){
$type=0;	
$export=1;
$page_title='Purchases Invoices';
$order_name='D-I';
$form='updateOrder';
$section_name='Purchase Invoice';
}



  $allPostedVals = $this->input->post(NULL,TRUE);
  $id_delivery_order=$this->input->post('id_delivery_order');
  $status=$this->input->post('status');
  $payment_terms=$this->input->post('payment_terms');

  $order_conditions=$this->input->post('order_conditions');
  $delivery_date=$this->input->post('delivery_date');
 
  $section=$this->input->post('section');
  $rand=$this->input->post('rand');
  $cond['rand']=$rand;
  $orderData=$this->admin_fct->getDeliveryOrder($id_delivery_order,$cond);

  if(!empty($orderData)){
	  

	  	if($this->input->post('submit')=="Send to Supplier") {
		$this->load->model('send_emails');
		$orderData['type']=$type;
$orderData['form']=$form;
$orderData['export']=$export;
$orderData['page_title']=$page_title;
$orderData['order_name']=$order_name;
$orderData['section_name']=$section_name;

$this->send_emails->inform_supplier_deliveryOrder($orderData);
$this->session->set_userdata("success_message","Information was sent successfully");

redirect(site_url("back_office/inventory/view_delivery_request/".$section."/".$id_delivery_order."/".$rand));
}
		
	if($this->input->post('submit')=="Save Changes") {
			$j=0;
	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$total_cost=0;
	$total_price=0;
	
	$expire_date_arr=$this->input->post('expire_date');
						foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];
						$price=$price_arr[$key];
						$cost=$cost_arr[$key];
						$total_cost=$total_cost+($qty*$cost);
						$total_price=$total_price+($qty*$price);
						$expire_date=$this->fct->date_in_formate($expire_date_arr[$key]);
		
						$line_item=$this->admin_fct->getDeliveryLineItem($val);
	
							$update_line_item['updated_date'] = date('Y-m-d h:i:s');
							$update_line_item['quantity'] = $qty;
						    $update_line_item['cost'] = $cost;
							$update_line_item['price'] = $price;
							$update_line_item['expire_date'] = $expire_date;

						$this->db->where("id_delivery_order_line_items",$val);
						$this->db->update('delivery_order_line_items',$update_line_item);
						
						}
						
									
						$cond['id_delivery_order']=$id_delivery_order;
						$cond['rand'] = $this->input->post('rand');
						$update_order['total_cost']=$total_cost;
						$update_order['total_price']=$total_price;
						$update_order['delivery_date']=$this->fct->date_in_formate($delivery_date);
						$update_order['payment_terms']=$payment_terms;
						$update_order['order_conditions']=$order_conditions;
				

						//print '<pre>';print_r($order);
						$this->db->where($cond);
						$this->db->update('delivery_order',$update_order);
						
						$this->session->set_userdata("success_message","Information was updated successfully");
						redirect(site_url("back_office/inventory/view_delivery_request/".$section."/".$id_delivery_order.'/'.$rand));
					 
			}
	
		if($this->input->post('submit')=="Export to Delivery Note" || $this->input->post('submit')=="Export to Invoice") {
	
 		$this->form_validation->set_rules('ids','ids','xss_clean');
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
					$this->view_delivery_order($id_delivery_order,$rand);
					}else {
						
/*  $old_order['status']=1;
  $this->db->where($cond);
  $this->db->update('delivery_order',$old_order);*/

  $export_order['created_date']=$orderData['created_date'];
  $export_order['rand']=$orderData['rand'];
  $export_order['id_old_delivery_order']=$id_delivery_order;
 if($this->input->post("status_m")!=""){
	 $export_order['status']=$this->input->post("status_m");}else{
		$export_order['status']=2; }
  
  $export_order['type']=$orderData['type'];
  $export_order['payment_terms']=$this->input->post('payment_terms');
  $export_order['order_conditions']=$this->input->post('order_conditions');
  $export_order['delivery_date']=$this->fct->date_in_formate($this->input->post('delivery_date'));
  $export_order['export']=1;
  $export_order['currency']=$orderData['currency'];
  $export_order['id_user']=$orderData['id_user'];
  $export_order['id_role']=$orderData['id_role'];		
  $export_order['return']=$orderData['return'];
  
  if($export_order['type']==2){
	$export_order['invoice']=1;
	 }

  $this->db->insert('delivery_order',$export_order);
 


  $new_id = $this->db->insert_id();
					
	
	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$expire_date_arr=$this->input->post('expire_date');
	$total_cost=0;
	$total_price=0;
	



						$j=0;

						foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];
						$expiry_date=$expire_date_arr[$key];
						$price=$price_arr[$key];
						$cost=$cost_arr[$key];
						
						$line_item=$this->admin_fct->getDeliveryLineItem($val);
						
						$total_cost=$total_cost+($qty*$cost);
						$total_price=$total_price+($qty*$price);
						
						$expire_date=$this->fct->date_in_formate($expire_date_arr[$key]);
							$insert_line_item['updated_date'] = date('Y-m-d h:i:s');
							$insert_line_item['quantity'] = $qty;
							$insert_line_item['cost'] = $cost;
							$insert_line_item['expire_date'] = $expire_date;
							$insert_line_item['price'] = $price;
							$insert_line_item['id_orders'] = $new_id;
							
							$insert_line_item['combination'] = $line_item['combination'];
							$insert_line_item['options'] = $line_item['options'];
							$insert_line_item['type'] = $line_item['type'];
							$insert_line_item['id_products'] = $line_item['id_products'];
							$insert_line_item['id_brands'] = $line_item['id_brands'];
							$insert_line_item['brands_title'] = $line_item['brands_title'];
							$insert_line_item['id_purchases'] = $line_item['id_purchases'];
							$insert_line_item['brands_title'] = $line_item['brands_title'];
							$insert_line_item['sku'] = $line_item['sku'];
							$insert_line_item['flag'] = $line_item['flag'];
							
						$this->db->insert('delivery_order_line_items',$insert_line_item);
					
						}
						
						$cond2['id_delivery_order']=$new_id;
						$update_order['total_cost']=$total_cost;
						$update_order['total_price']=$total_price;
						//print '<pre>';print_r($order);
						$this->db->where($cond2);
						$this->db->update('delivery_order',$update_order);
	
$this->session->set_userdata("success_message","Information was exported successfully");
if($section=="stock"){
redirect(site_url("back_office/inventory/view_delivery_request/delivery_note/".$new_id."/".$rand));}
if($section=="lpo"){
redirect(site_url("back_office/inventory/view_delivery_request/invoices/".$new_id."/".$rand));}
	}	

		}}else{
	$this->session->set_flashdata('error_message',lang('access_denied'));
  redirect(site_url("back_office/inventory/request/".$section));}

}



public function print_delivery_order($section="",$id,$rand="")
{
		
$cond = array();
$data['section']=$section;

if($section=="stock"){
$type=1;	
$export=0;
$page_title='Stock Request';
$order_name='R-Q';
$form='export';
$section_name='Stock Request';
}

if($section=="delivery_note"){
$type=1;	
$export=1;
$page_title='Delivery Note';
$order_name='D-N';
$form='updateOrder';
$section_name='Delivery Note';
}

if($section=="lpo"){
$type=2;	
$export=0;
$page_title='LPO';
$order_name='D-P';
$form='export';
$section_name=' Purchase Order';
}

if($section=="invoices"){
$type=0;	
$export=1;
$page_title='Purchase Invoice';
$order_name='D-I';
$form='updateOrder';
$section_name='Purchase Invoice';
$data['print']=1;
}

$data['type']=$type;
$data['form']=$form;
$data['export']=$export;
$data['page_title']=$page_title;
$data['order_name']=$order_name;
$data['section_name']=$section_name;




	
		$cond['rand']=$rand;
		$orderData=$this->admin_fct->getDeliveryOrder($id,$cond);


	   $data['site_info']=$this->fct->getonerow('settings',array('id_settings'=>1));

		$data['orderData']=$orderData;
		if(!empty($orderData)) {
		
		$template = $this->load->view('emails/delivery_order',$data,true);;
		$data['template'] = $template;
		$this->load->view('back_office/inventory/print',$data);
		}else {
		$this->session->set_flashdata('error_message',lang('please_login'));
		redirect(route_to('back_office/inventory'));
	}
}

public function print_stock_return($id,$rand="")
{
	
	
		$cond=array('rand'=>$rand);
$orderData=$this->admin_fct->getReturnStockOrder($id,$cond);

		$data['orderData']=$orderData;
		$data['hide_price']=1;
		if(!empty($orderData)) {
		
		$template = $this->load->view('emails/stock_return',$data,true);;
		$data['template'] = $template;
		$this->load->view('back_office/inventory/delivery_order/print',$data);
		}else {
		$this->session->set_flashdata('error_message',lang('please_login'));
		redirect(route_to('back_office/inventory'));
	}
}


public function getDeliveryItems(){
$keyword = $_POST['term'];
$cond['title']=$keyword;
$cond['brand']=$_POST['id_brand'];
$brands_ids=array();
$user = $this->ecommerce_model->getUserInfo();
if(checkIfSupplier_admin()){
$cond2['id_user']=$user['id_user'];	
$brands=$this->fct->getAll_cond('brands','title asc',$cond2);

foreach($brands as $brand){
	 array_push($brands_ids, $brand['id_brands']);
	}
	}
	
$cond['brands_ids']=$brands_ids;


$show_items = $this->ecommerce_model->getProducts($cond);  
$results = $this->ecommerce_model->getProducts($cond, $show_items, 0, 'products.sort_order', 'asc');  


//$results=$this->fct->getimagegallery($keyword);

$cc = count($results);
$i=0;
echo '[';
if(!empty($results)){
foreach($results as $result){
$i++;
if($i == $cc) $coma = '';
else $coma = ',';
//$res = preg_replace('/\.[^.]+$/','',$result['title']);
//$res =str_replace(".jpg","",$result['image']);
echo '{"label" : "'.str_replace("'","",$result['title']).'", "value": "'.$result['id_products'].'_'.$result['title'].'"}'.$coma;
//echo '"'.str_replace("'","",$result['title']).'"'.$coma;
}}
echo ']';}
public function getStock(){
$id = $_POST['id'];
$return['result']=5;
$return['html']="";
$product=$this->fct->getonerecord('products',array('id_products'=>$id));

$stock=$this->ecommerce_model->getProductStock(array('id_products'=>$id));


/*$stock = $this->fct->getAll_cond('products_stock','sort_order',array('id_products'=>$id));*/
$data['stock']=$stock;
$data['product']=$product;
if(!empty($stock)){
$return['result']=1;
$return['html']=$this->load->view('back_office/blocks/product_stock',$data,true);	
	}else{
		if(!empty($product)){
$return['result']=0;		
$return['html']=$this->load->view('back_office/blocks/product_stock',$data,true);}}



die(json_encode($return));
 
}

public function getStockReachedThreshold(){
$id_brand = $_POST['id_brand'];
$return['result']=5;
$return['html']="";


$cond['id_brands']=$id_brand;
/*$cond['threshold']=1;*/
$cc= $this->ecommerce_model->getInventory($cond);
$data['info'] = $this->ecommerce_model->getInventory($cond,$cc,0);


if(!empty($data['info'])){
$return['result']=1;
$return['html']=$this->load->view('back_office/blocks/product_stock_threshold',$data,true);}else{
$return['result']=0;
$return['html']="";}


die(json_encode($return));
 
}



public function getOrders(){
$keyword = $_POST['term'];
$cond['order_id_keyword']=$keyword;

if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo();
	$cond['id_user']=$user['id_user'];
	$cond['id_role']=5;}
	
	
$cond['return']=0;
	$cond['flag']=4;	
$brands_ids=array();
$count_news = $this->admin_fct->getDeliveryOrders($cond);
$results = $this->admin_fct->getDeliveryOrders($cond,$count_news,0);



//$results=$this->fct->getimagegallery($keyword);

$cc = count($results);
$i=0;
echo '[';
if(!empty($results)){
foreach($results as $result){
$i++;
if($i == $cc) $coma = '';
else $coma = ',';
  if($result["id_role"] == 5) { $order_name="D-ORDER#"; }else{ $order_name="D-RQ#";}
  $order_name=""; 
//$res = preg_replace('/\.[^.]+$/','',$result['title']);
//$res =str_replace(".jpg","",$result['image']);
echo '{"label" : "'.$order_name.$result['id_delivery_order'].'", "value": "'.$result['id_delivery_order'].'_'.$order_name.$result['id_delivery_order'].'"}'.$coma;
//echo '"'.str_replace("'","",$result['title']).'"'.$coma;
}}
echo ']';}

public function getOrdersLineItems($id,$id_stock_return){

$cond=array();
$cond['flag']=4;
$data['order_details']=$this->admin_fct->getDeliveryOrder($id,$cond);

$data['id_stock_return']=$id_stock_return;
$this->load->view("back_office/blocks/order_line_items_section",$data);


/*die(json_encode($return));*/
 
}

public function getBrands(){
	$cond=array();
	$cond2=array();
$id = $this->input->post('id_supplier');

$return['result']=5;
$return['html']="";
if($id!="all" && $id!=""){
$cond['id_user']=$id;}
$brands_arr=array(-1);
$brands = $this->fct->getAll_cond('brands','title',$cond);

$html ="<label>Brand *</label>";

$html .="<select name='brands' id='brands'>";
$html .="<option value=''>-Select All-</option>";

foreach($brands as $val){

	array_push($brands_arr,$val['id_brands']);
$html .="<option value=".$val['id_brands'].">".$val['title']."</option>";
 } 
$html .="</select>";
	
$return['result']=1;


///////////////////////////////////////GET PRODUCTS By Supplier/////////////////

$cond2['brands']=implode(',',$brands_arr);

/*$cond['threshold']=1;*/
$cc= $this->ecommerce_model->getInventory($cond2);
$data['info'] = $this->ecommerce_model->getInventory($cond2,$cc,0);

if(!empty($data['info']) && $id!="all" && $id!=""){
$return['result2']=1;
$return['html2']=$this->load->view('back_office/blocks/product_stock_threshold',$data,true);
}else{
$return['result2']=0;
$return['html2']="";}


$return['html']=$html;

die(json_encode($return));
 
}

public function getStoreKeeper(){
$id = $this->input->post('id_line_items_return');
$line_item=$this->fct->getonerecord('delivery_order_line_items',array('id_delivery_order_line_items'=>$id));
$return['result']=5;
$return['html']="";
$cond['product_type']=$line_item['type'];
$cond['id_products']=$line_item['id_products'];
$storeKeeper = $this->admin_fct->getStoreKeeper($cond);

$data['line_items']=$storeKeeper;
$this->load->view('back_office/blocks/store_keeper',$data,true);	

/*$return['result']=1;
$return['html']=$html;

die(json_encode($return));*/
 
}

public function addToOrder(){
$data['checklist']=$this->input->post('checklist');

$type=$this->input->post('type');
$data['threshold']=$this->input->post('threshold');
$data['type']=$type;
$data['qty']=$this->input->post('qty');
$data['price']=$this->input->post('price');
$data['cost']=$this->input->post('cost');
$ids=$this->input->post('ids');
$data['ids']=$ids;
$data['id_brands']=$this->input->post('brands');
$result=0;

$return['message']="";	
if($this->input->post('ids')!=""){
	$result=1;
foreach($ids as $key=>$val){
	$type_m=$type[$key];

if($type_m=="option" && empty($data['checklist'])){
	
$result=0;
$return['message']="Please select at least one item.";
}}

$return['html']=$this->load->view('back_office/blocks/product_stock_order',$data,true);

}else{
$return['message']="error!";}
$return['id_brands']=$data['id_brands'];
$return['brand_name']=$this->fct->getonecell('brands',"title",array("id_brands"=>$data['id_brands']));
$return['table_head']='  <tr><td width="25%">Product Name</td><td width="15%">Sku</td><td width="10%">Quantity</td><td width="15%">Price('.$this->config->item('default_currency').')</td><td width="15%">Cost('.$this->config->item('default_currency').')</td><td width="15%"></td></tr>';
$return['result']=$result;	

die(json_encode($return));

}

public function addToReturnStock(){
	

$data['checklist']=$this->input->post('checklist');

$type=$this->input->post('type');
$data['type']=$type;
$data['qty']=$this->input->post('qty');
$data['price']=$this->input->post('price');
$data['cost']=$this->input->post('cost');
$data['ids']=$this->input->post('ids');

$result=0;
$return['message']="";	
if($this->input->post('ids')!=""){
$result=1;
if(isset($data['checklist']) && empty($data['checklist'])){
$result=0;
$return['message']="Please select at least one item.";
}

$return['html']=$this->load->view('back_office/blocks/stock_line_items',$data,true);

}else{
$return['message']="error!";}


$return['table_head']='  <tr><td width="30%">Product Name</td> <td width="15%">Sku</td><td width="10%">Quantity</td><td class="price_p" width="15%">Price('.$this->config->item('default_currency').')</td><td width="15%" class="price_p">Cost('.$this->config->item('default_currency').')</td><td width="10%"></td></tr>';
$return['result']=$result;	

die(json_encode($return));

}

public function remove_product($section,$id_line_item,$id_order,$rand){
		$check=$this->fct->getonerecord('delivery_order',array('id_delivery_order'=>$id_order,'rand'=>$rand));
	if(!empty($check)){
	$cond['id_orders']=$id_order;
	$cond['id_delivery_order_line_items']=$id_line_item;
	$this->db->delete('delivery_order_line_items',$cond);
	$this->updateOrderPrices($id_order,$rand);
	$this->session->set_userdata("success_message","Item was deleted successfully");
	redirect(site_url("back_office/inventory/view_delivery_request/".$section."/".$id_order."/".$rand));}
	
	}
	
public function remove_product_stock_return($id_line_item,$id_order,$rand){
	$check=$this->fct->getonerecord('stock_return',array('id_stock_return'=>$id_order,'rand'=>$rand));
	if(!empty($check)){
	$cond['id_orders']=$id_order;
	$cond['id_stock_return_line_items']=$id_line_item;
	$this->db->delete('stock_return_line_items',$cond);
	$this->session->set_userdata("success_message","Item was deleted successfully");
	redirect(site_url("back_office/inventory/view_stock_return/".$id_order."/".$rand));}
	
	}
	
	public function updateOrderPrices($id_delivery_order,$rand){


  	$cond['id_delivery_order']=$id_delivery_order;
	$orderData=$this->admin_fct->getDeliveryOrder($id_delivery_order,$cond);


	$total_cost=0;
	$total_price=0;
	$j=0;
	$brands_line_items=$orderData['line_items'];
foreach($brands_line_items as $brand) {
 $line_items=$brand['line_items'];

			foreach($line_items as $line_item) {$j++;
					$total_cost=$total_cost+($line_item['quantity']*$line_item['cost']);	
					$total_price=$total_price+($line_item['quantity']*$line_item['price']);	
						}}
						
					
						$update_order['total_cost']=$total_cost;
						$update_order['total_price']=$total_price;
			

						//print '<pre>';print_r($order);
						$this->db->where($cond);
						$this->db->update('delivery_order',$update_order);	
					
return true;
}

public function add(){
if ($this->acl->has_permission('inventory','add')){	
$data["title"]="Add inventory";
$data["content"]="back_office/inventory/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

//////////////////////////////////////////Checkout////////////////////////////
public function insertOrder()
{	
			$allPostedVals = $this->input->post(NULL,TRUE);

			$this->form_validation->set_rules('id_role','Role','trim|xss_clean');
				
					
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
							
				$return['result'] = 0;
				$return['errors'] = array();
				$return['message'] = 'Error!';
			    $return['captcha'] = $this->fct->createNewCaptcha();
				$find =array('<p>','</p>');
				$replace =array('','');
				foreach($allPostedVals as $key => $val) {
					if(form_error($key) != '') {
						$return['errors'][$key] = str_replace($find,$replace,form_error($key));
					}
				}
				die(json_encode($return));
					}else {
	
	$user = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
	$brands_arr=$this->input->post('brands');
	$id_brand=$brands_arr[0];


	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$section=$this->input->post('section');
	$type_arr=$this->input->post('type');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$expire_date_arr=$this->input->post('expire_date');
	$total_cost=0;
	$total_price=0;
	

						// created order
						$site_info=$this->fct->getonerow('settings',array('id_settings'=>1));
						$order['created_date'] = date('Y-m-d h:i:s');
						$order['id_user'] = $this->input->post('id_user');
						$order['order_conditions'] = $site_info['order_conditions'];
						$order['payment_terms'] = 45;
						$order['rand'] = rand();
						$order['type'] = $this->input->post('order_type');
					
	if($section=="invoices"){
	$order['invoice']=1;
	$order['type']=0;
	}
	
	
		
						$order['id_role'] = $this->input->post('id_role');
						$order['return'] = $this->input->post('return');
						$order['id_brands'] = $id_brand;
						/*$order['status'] = $this->input->post('status');*/
						$order['status'] =2;
						$order['currency'] = $this->config->item('default_currency');
						/*$order['lang'] = $this->lang->lang();*/

						//print '<pre>';print_r($order);
						$this->db->insert('delivery_order',$order);
						$order_id = $this->db->insert_id();
				
						
			
					$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
     						// Miles
						$id_products=$val;
						$qty=$qty_arr[$key];	
					    $id_brands=$brands_arr[$key];
						$price=$price_arr[$key];
						if(empty($price)){
							$price=0;}
						$cost=$cost_arr[$key];
						if(empty($cost)){
							$cost=0;}
						$expire_date=$expire_date_arr[$key];
						$brand_title=$this->fct->getonecell('brands','title',array('id_brands'=>$id_brands));
						$type=$type_arr[$key];
						$combination="";
						if($type=="option"){
							$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$id_products));
							$combination=$stock['combination'];
						}
						
						$total_cost=$total_cost+($qty*$cost);
					$total_price=$total_price+($qty*$price);
							$line_item['created_date'] = date('Y-m-d h:i:s');
							$line_item['id_products'] = $id_products;
							$line_item['quantity'] = $qty;
							$line_item['expire_date'] = $this->fct->date_in_formate($expire_date);
							$line_item['price'] = $price;
							$line_item['cost'] = $cost;
							$line_item['id_orders'] = $order_id;
							$line_item['options'] = $combination;
							$line_item['brands_title'] = $brand_title;
							$line_item['id_brands'] = $id_brands;
							$line_item['type'] = $type;
							
						
							// options
					$this->db->insert('delivery_order_line_items',$line_item);
				
					}
					
						$cond2['id_delivery_order']=$order_id;
						$update_order['total_cost']=$total_cost;
						$update_order['total_price']=$total_price;
						//print '<pre>';print_r($order);
						$this->db->where($cond2);
						$this->db->update('delivery_order',$update_order);	
						
						

					

				//generate invoice templat
	/*			$orderData = $this->ecommerce_model->getOrder($order_id);
				$d['orderData']=$orderData;*/
			$cond=array('rand'=>$order['rand']);
			$orderData=$this->admin_fct->getDeliveryOrder($order_id,$cond);
			
			$this->load->model('send_emails');
/*if($orderData['id_role']==5){
			$this->send_emails->sendDeliveryOrderToAdmin($orderData);
			$this->send_emails->sendDeliveryOrderToUser($orderData);}else{
			$this->send_emails->sendRequestOrderToUser($orderData);	
			$this->send_emails->sendRequestOrderToAdmin($orderData);}*/	
$return['message'] = lang('system_is_redirecting_to_page');
if($this->input->post('return')==1){
$return['redirect_link'] =  site_url("back_office/inventory/view_stock_return/".$section."/".$order_id."/".$order['rand']);	}else{
if($orderData['id_role']==5){
$return['redirect_link'] =  site_url("back_office/inventory/view_delivery_order/".$section."/".$order_id."/".$order['rand']);}else{
$return['redirect_link'] =  site_url("back_office/inventory/view_delivery_request/".$section."/".$order_id."/".$order['rand']);	}}

$this->session->set_userdata("success_message","Your order is saved");	
			$return['result'] = 1;
			die(json_encode($return));
		}
		
		
		

}


//////////////////////////////////////////Checkout////////////////////////////
public function insertNewProducts()
{	
$order_id=$this->input->post('id_delivery_order');
$rand=$this->input->post('rand');
$section=$this->input->post('section');
			$allPostedVals = $this->input->post(NULL,TRUE);

			$this->form_validation->set_rules('id_role','Role','trim|xss_clean');
				
			$this->form_validation->set_rules('id_user',lang('trading_name'),'trim|required|xss_clean');		
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
							
				$return['result'] = 0;
				$return['errors'] = array();
				$return['message'] = 'Error!';
			    $return['captcha'] = $this->fct->createNewCaptcha();
				$find =array('<p>','</p>');
				$replace =array('','');
				foreach($allPostedVals as $key => $val) {
					if(form_error($key) != '') {
						$return['errors'][$key] = str_replace($find,$replace,form_error($key));
					}
				}
				die(json_encode($return));
					}else {
	
	$user = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
	
/*	$brands_arr=$this->input->post('brands');
	$id_brand=$brands_arr[0];*/
	$id_brands=$this->input->post('brands');
	


	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$checklist=$this->input->post('checklist');
	$section=$this->input->post('section');
	$type_arr=$this->input->post('type');
	$brands_arr=$this->input->post('brands');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$expire_date_arr=$this->input->post('expire_date');
	
	$total_cost=0;
	$total_price=0;

		$order=$this->fct->getonerecord('delivery_order',array('id_delivery_order'=>$order_id));
		
		if(!empty($order) && $this->input->post('id_user')=="all" || $this->input->post('id_user')!=$order['id_user']){
			$this->db->delete('delivery_order_line_items',array('id_orders'=>$order['id_delivery_order']));
			}		
				
				
				$id_user=$this->input->post('id_user');
			
					$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
						if(isset($checklist[$key]) || count($ids_arr)<2){
     						// Miles
						$id_products=$val;
						$qty=$qty_arr[$key];	
					    $id_brands=$brands_arr[$key];
						$price=$price_arr[$key];
						if(empty($price)){
							$price=0;}
						$cost=$cost_arr[$key];
						if(empty($cost)){
							$cost=0;}
						$expire_date=$expire_date_arr[$key];
						$brand_title=$this->fct->getonecell('brands','title',array('id_brands'=>$id_brands));
						$type=$type_arr[$key];
						$combination="";
						if($type=="option"){
							$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$id_products));
							$combination=$stock['combination'];
							$line_items3=$this->fct->getAll_cond_1('delivery_order_line_items','id_delivery_order_line_items',array('type'=>'option','id_products'=>$id_products),'desc');
	 					if(!empty($line_items3)){
	 					$cost=$line_items3[0]['cost'];}
						$price=$stock['price'];
						}else{
						$product=$this->fct->getonerecord('products',array('id_products'=>$id_products));
	 					$line_items3=$this->fct->getAll_cond_1('delivery_order_line_items','id_delivery_order_line_items',array('type'=>'product','id_products'=>$id_products),'desc');
						if(!empty($line_items3)){
						$cost=$line_items3[0]['cost'];}
						$price=$product['price'];
							}
						
						$total_cost=$total_cost+($qty*$cost);
					$total_price=$total_price+($qty*$price);
							$line_item['created_date'] = date('Y-m-d h:i:s');
							$line_item['id_products'] = $id_products;
							$line_item['quantity'] = $qty;
							$line_item['expire_date'] = $this->fct->date_in_formate($expire_date);
							$line_item['price'] = $price;
							$line_item['cost'] = $cost;
							$line_item['id_orders'] = $order_id;
							$line_item['options'] = $combination;
							$line_item['brands_title'] = $brand_title;
							$line_item['id_brands'] = $id_brands;
							$line_item['type'] = $type;
							
						
					// options
					   $this->db->insert('delivery_order_line_items',$line_item);}
				         }
					
						$cond2['id_delivery_order']=$order_id;
						$update_order['id_user']=$id_user;
						$update_order['total_cost']=$total_cost;
						$update_order['total_price']=$total_price;
						//print '<pre>';print_r($order);
						$this->db->where($cond2);
						$this->db->update('delivery_order',$update_order);	
						
						

					

				//generate invoice templat
	/*			$orderData = $this->ecommerce_model->getOrder($order_id);
				$d['orderData']=$orderData;*/

			//$return['redirect_link'] =  site_url("back_office/inventory/view_delivery_request/".$section."/".$order_id."/".$rand);
			$this->session->set_userdata("success_message","Your order was updated successfully");	
			// $return['message'] = lang('system_is_redirecting_to_page');
		
			$this->updateOrderPrices($order_id,$rand);
			$return['result'] = 1;
			$return['redirect_link'] = site_url("back_office/inventory/view_delivery_request/".$section."/".$order_id."/".$rand);
			$return['message'] = lang('system_is_redirecting_to_page');
			
			die(json_encode($return));
		}
		
		
		

}



public function insertStockOrder()
{

$checklist=$this->input->post('checklist');		
$result=0;
$return['message']="";	
if($this->input->post('ids')!=""){
$bool=true;
$result=1;
if(isset($checklist) && empty($checklist)){
$bool=false;
$result=0;
$return['message']="Please select at least one item.";
}$return['result']=$result;
	}
	if($bool){
	
	$user = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$checklist=$this->input->post('checklist');
	$order_id=$this->input->post('id_stock_return');
	$id_delivery_order=$this->input->post('id_delivery_order');
	$delivery_order=$this->fct->getonerecord('delivery_order',array('id_delivery_order'=>$id_delivery_order));		
	$stock_return=$this->fct->getonerecord('stock_return',array('id_stock_return'=>$order_id));	
	if($delivery_order['id_user']!=$stock_return['id_user'] && !empty($stock_return)){
	$cond_delete['id_orders']=$order_id;
	$this->db->delete('stock_return_line_items',$cond_delete);
	
			}
	
	if(!empty($stock_return)){
	$cond_update['id_stock_return']=$order_id;
	$update['id_user']=$delivery_order['id_user'];
	$this->db->where($cond_update);
	$this->db->update('stock_return',$update);	}		
					
					$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
						if(isset($checklist[$key])){
							
     						// Miles
						$id_line_items=$val;
						$qty=$qty_arr[$key];	
				
							$line_item['created_date'] = date('Y-m-d h:i:s');
							$line_item['id_line_items'] = $id_line_items;
							$line_item['quantity'] = $qty;
							$line_item['id_orders'] = $order_id;
	
							// options

						$this->db->insert('stock_return_line_items',$line_item);}
						}
						

	

				//generate invoice templat
	/*			$orderData = $this->ecommerce_model->getOrder($order_id);
				$d['orderData']=$orderData;*/
$cond=array();
$orderData=$this->admin_fct->getReturnStockOrder($order_id,$cond);		
$data['order_details']=$orderData;
$return['message']="Products was added successfully to stock return.";	
$return['html'] =  $this->load->view('back_office/blocks/stock_line_items',$data,true);
$return['result'] = 1;
			die(json_encode($return));}else{
			die(json_encode($return));	}
	

}

public function getStockLineItems()
{
	}

//////////////////////////////////////////Checkout////////////////////////////
public function updateDeliveryOrder()
{	
	$allPostedVals = $this->input->post(NULL,TRUE);

					$this->form_validation->set_rules('id_delivery_order','id_delivery_order','trim|xss_clean');
				
					
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
							
				$return['result'] = 0;
				$return['errors'] = array();
				$return['message'] = 'Error!';
			    $return['captcha'] = $this->fct->createNewCaptcha();
				$find =array('<p>','</p>');
				$replace =array('','');
				foreach($allPostedVals as $key => $val) {
					if(form_error($key) != '') {
						$return['errors'][$key] = str_replace($find,$replace,form_error($key));
					}
				}
				die(json_encode($return));
					}else {
	
	

	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');

						// created order
						$order_id = $this->input->post('id_delivery_order');
						$rand = $this->input->post('rand');
						$order['updated_date'] = date('Y-m-d h:i:s');
						
					
						$order['type'] = $this->input->post('type');
						$order['status'] =$this->input->post('status');
						/*$order['lang'] = $this->lang->lang();*/

						//print '<pre>';print_r($order);
						$this->db->where(array('id_delivery_order'=>$order_id,'rand'=>$rand));
						$this->db->update('delivery_order',$order);
						
						
			
					$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
     					$qty=$qty_arr[$key];	

							$line_item['updated_date'] = date('Y-m-d h:i:s');
							$line_item['quantity'] = $qty;

						$this->db->where(array('id_delivery_order_line_items'=>$val));
						$this->db->update('delivery_order_line_items',$line_item);
						}
						

					

$this->session->set_userdata("success_message","Information was updated successfully");	

 redirect(site_url("back_office/inventory/view_delivery_order/".$order_id."/".$rand));
			die(json_encode($return));
		}
		
		
		

}


public function add_delivery_order($id_deliver_order=0){
$cond=array("id_delivery_order"=>$id_deliver_order);
$info=$this->fct->getonerecord('delivery_order',$cond);
if ($this->acl->has_permission('products','add')){
if(!empty($info)){
$data["id"]=$id_deliver_order;
$data["info"]=$info;
$data["title"]="Edit Order";
}else{
$data["title"]="Add New Order";
}
$data["content"]="back_office/inventory/delivery_order/add";

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view_delivery_order($id_deliver_order=0,$rand,$return=array()){
$cond=array('rand'=>$rand);
$info=$this->admin_fct->getDeliveryOrder($id_deliver_order,$cond);

$data=$return;
if ($this->acl->has_permission('products','add')){
if(!empty($info)){
$data['order_details']=$info;
$data["id"]=$id_deliver_order;
$data["info"]=$info;
$data["title"]="Order Details#".$id_deliver_order;
}else{
$data["title"]="Order Details";
}
$data["content"]="back_office/inventory/delivery_order/edit";

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 


public function add_delivery_request($section,$id_deliver_order=0){
	
$cond = array();
$data['section']=$section;

if($section=="stock"){
$type=1;	
$export=0;
$page_title='Stock Request';
$order_name='R-Q';
$form='export';
}

if($section=="delivery_note"){
$type=1;	
$export=1;
$page_title='Delivery Note';
$order_name='D-N';
$form='updateOrder';
}

if($section=="lpo"){
$type=2;	
$export=0;
$page_title='LPO';
$order_name='D-P';
$form='export';
}

if($section=="invoices"){
$type=2;	
$export=1;
$page_title='Purchases Invoices';
$order_name='D-I';
$form='updateOrder';

}



$data['type']=$type;
$data['form']=$form;
$data['export']=$export;
$data['page_title']=$page_title;
$data['order_name']=$order_name;
$cond['type']=$type;
$cond['export']=$export;
	
/*	
$cond=array("id_delivery_order"=>$id_deliver_order);
$info=$this->fct->getonerecord('delivery_order',$cond);
if ($this->acl->has_permission('inventory','add')){
if(!empty($info)){
$data["id"]=$id_deliver_order;
$data["info"]=$info;
$data["title"]="Edit Order";
}else{
$data["title"]="Add New Order";
}
$data["content"]="back_office/inventory/delivery_request/add";

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}*/


						// created order
						$rand=rand();
						$site_info=$this->fct->getonerow('settings',array('id_settings'=>1));
						$order['created_date'] = date('Y-m-d h:i:s');
						$order['id_user'] = 0;
						$order['order_conditions'] = $site_info['order_conditions'];
						$order['payment_terms'] = 45;
						$order['rand'] = $rand;
						$order['type'] = $type;
					
	if($section=="invoices"){
	$order['invoice']=1;
	$order['type']=0;
	}
	
						$user=$this->ecommerce_model->getUserInfo();
						$order['id_role'] = $user['id_roles'];
						
						/*$order['status'] = $this->input->post('status');*/
						$order['status'] =2;
						$order['currency'] = $this->config->item('default_currency');
						/*$order['lang'] = $this->lang->lang();*/

					
						
						$this->db->insert('delivery_order',$order);
						$order_id = $this->db->insert_id();
						
						redirect(site_url('back_office/inventory/view_delivery_request/'.$section.'/'.$order_id.'/'.$rand));
} 

public function view_delivery_request($section,$id_deliver_order=0,$rand,$return=array()){
$data=$return;	
	$cond = array();
$data['section']=$section;

if($section=="stock"){
$type=1;	
$export=0;
$page_title='Stock Request';
$order_name='R-Q';
$form='export';
$section_name='Stock Request';
}

if($section=="delivery_note"){
$type=1;	
$export=1;
$page_title='Delivery Note';
$order_name='D-N';
$form='updateOrder';
$section_name='Delivery Note';
}

if($section=="lpo"){
$type=2;	
$export=0;
$page_title='LPO';
$order_name='D-P';
$form='export';
$section_name='LPO';
}

if($section=="invoices"){
$type=0;	
$export=1;
$page_title='Purchases Invoices';
$order_name='D-I';
$form='updateOrder';
$section_name='Purchase Invoice';
}



$data['type']=$type;
$data['form']=$form;
$data['export']=$export;
$data['page_title']=$page_title;
$data['order_name']=$order_name;
$data['section_name']=$section_name;
$cond['type']=$type;
$cond['export']=$export;
$cond['section']=$section;
	
$data['site_info']=$this->fct->getonerow('settings',array('id_settings'=>1));	
$cond=array('rand'=>$rand);
$info=$this->admin_fct->getDeliveryOrder($id_deliver_order,$cond);




if ($this->acl->has_permission('inventory','add')){
if(!empty($info)){
$data['order_details']=$info;
$data["id"]=$id_deliver_order;
$data["info"]=$info;
$data["title"]="Order Details#".$id_deliver_order;
}else{
$data["title"]="Order Details";
}
$data["content"]="back_office/inventory/delivery_request/edit";

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 


public function add_stock_return2($id_deliver_order=0){
$cond=array("id_delivery_order"=>$id_deliver_order);
$info=$this->fct->getonerecord('delivery_order',$cond);
if ($this->acl->has_permission('inventory','add')){
if(!empty($info)){
$data["id"]=$id_deliver_order;
$data["info"]=$info;
$data["title"]="Edit Order";
}else{
$data["title"]="Add New Order";
}
$data["content"]="back_office/inventory/stock_return/add";

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function add_stock_return(){
	
$user=$this->ecommerce_model->getUserInfo($this->session->userdata('user_id'));
if(checkIfSupplier_admin()){ 
 	$id_role=5;
         } 
		 $return=1;
		 $id_user= $user['id_user'];

	$user = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));


						// created order
						$order['created_date'] = date('Y-m-d h:i:s');
						$order['id_user'] = 0;
						$order['rand'] = rand();
						$order['id_role'] =0;
						$order['status'] = 2;
						$this->db->insert('stock_return',$order);
						$order_id = $this->db->insert_id();

	

$return['message'] = lang('system_is_redirecting_to_page');
$redirect_link =  site_url("back_office/inventory/view_stock_return/".$order_id."/".$order['rand']);
redirect($redirect_link);
$this->session->set_userdata("success_message","");	

}
public function checkReturnQuantity3()
	{
		
	
	$errors="<b>Can't process the changes!!</b>";	
	$bool=true;
	
	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');

foreach($ids_arr as $key=>$val){


$stock_line_items=$this->fct->getonerecord('stock_return_line_items',array('id_stock_return_line_items'=>$val));
$line_item=$this->admin_fct->getDeliveryLineItem($stock_line_items['id_line_items']);

$qty_return=$stock_line_items['quantity'];
$remaining_quantity=$line_item-$stock_line_items['sold_quantity'];	



if($remaining_quantity<$qty_return){
	$bool=false;
	$errors .="<p>The Quantity '".$qty_return."' exceed the total available quantity '".$remaining_quantity."' for ".$$line_item['product']['title']."</p>";
	}
}
	
if($bool) {
		return true;
}
else {
	
		$this->form_validation->set_message('checkReturnQuantity3',$errors);
		return false;
}
	}
public function updateStockReturn()
{	
  $allPostedVals = $this->input->post(NULL,TRUE);
  $id_stock_return=$this->input->post('id_stock_return');
  $status=$this->input->post('status');
  $rand=$this->input->post('rand');
  $cond['rand']=$rand;
  $orderData=$this->admin_fct->getReturnStockOrder($id_stock_return,$cond);
  
 		$this->form_validation->set_rules('ids','ids','xss_clean');
			if($this->input->post('status')!="" && ($orderData['status']!=1))
  		$this->form_validation->set_rules('qty','Quantities','callback_checkQuantity3[]');
			
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
					$this->view_delivery_order($id_stock_return,$rand);
					}else {
	

	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');

	
						// created order
						$order['updated_date'] = date('Y-m-d h:i:s');
						if($this->input->post('status')!=""){
						$order['status'] = $this->input->post('status');}
						
						
						$cond['id_stock_return']=$id_stock_return;
						$cond['rand'] = $this->input->post('rand');

						//print '<pre>';print_r($order);
						$this->db->where($cond);
						$this->db->update('stock_return',$order);


	
if($this->input->post('status')!="" && ($orderData['status']!=1 && $status==1)){
	$this->admin_fct->updateReturnStocks2();}	

						$j=0;
						if($this->input->post('status')!="" && ($orderData['status']!=1)){
						foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];
			
						$price=$price_arr[$key];
						$cost=$cost_arr[$key];
						$stock_line_items=$this->fct->getonerecord('stock_return_line_items',array('id_stock_return_line_items'=>$val));
						$line_item=$this->admin_fct->getDeliveryLineItem($stock_line_items['id_line_items']);
	
							$update_line_item['updated_date'] = date('Y-m-d h:i:s');
							$update_line_item['quantity'] = $qty;
				

						$this->db->where("id_stock_return_line_items",$val);
						$this->db->update('stock_return_line_items',$update_line_item);
						}}
						

					

				//generate invoice templat
	/*			$orderData = $this->ecommerce_model->getOrder($order_id);
					
				
				$d['orderData']=$orderData;*/
				/*$orderData=$this->admin_fct->getDeliveryOrder($id_delivery_order,$cond);*/
				if(isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
			$cond=array();
			$this->load->model('send_emails');
			$data_return["id_stock_return"]=$id_stock_return;
			$data_return["rand"]=$rand;
			$this->send_emails->stock_return($data_return);}
		
$this->session->set_userdata("success_message","Information was updated successfully");		

redirect(site_url("back_office/inventory/view_stock_return/".$id_stock_return."/".$rand));
		}

}

public function view_stock_return($id_deliver_order=0,$rand){
$cond=array('rand'=>$rand);
$info=$this->admin_fct->getReturnStockOrder($id_deliver_order,$cond);

if ($this->acl->has_permission('inventory','add')){
if(!empty($info)){
$data['order_details']=$info;
$data["id"]=$id_deliver_order;
$data["info"]=$info;
$data["title"]="Order Details#".$id_deliver_order;
}else{
$data["title"]="Order Details";
}
$data["content"]="back_office/inventory/stock_return/edit";

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function stock_return($order=""){
$this->session->unset_userdata("admin_redirect_url");
$order="";
if ($this->acl->has_permission('products','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List inventory";
$data["content"]="back_office/inventory/stock_return/list";

//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$url = site_url('back_office/inventory/stock_return').'?pagination=on';
$cond = array();
if(isset($_GET['stock_return'])) {
	$stock_return = $_GET['stock_return'];
	$url .= '&stock_return='.$stock_return;
	if(!empty($stock_return))
	$cond['stock_return'] = $stock_return;
}

if(isset($_GET['name'])) {

	$name = $_GET['name'];
	$url .= '&name='.$name;
	if(!empty($name))
	$cond['name'] = $name;
}

if(isset($_GET['trading_name'])) {
	
	$trading_name = $_GET['trading_name'];
	$url .= '&trading_name='.$trading_name;
	if(!empty($trading_name)){
	$cond['trading_name'] = $trading_name;
	}
}

if(isset($_GET['status'])) {

	$status = $_GET['status'];

	$url .= '&status='.$status;
	if(!empty($status))
	$cond['status'] = $status;
	
}

if(isset($_GET['type'])) {
	$type = $_GET['type'];
	$url .= '&type='.$type;
	if(!empty($type))
	$cond['type'] = $type;
}

if(isset($_GET['from_date'])) {
	$from_date = $_GET['from_date'];
	$f_date=$this->fct->date_in_formate($from_date);
	$url .= '&from_date='.$from_date;
	if(!empty($from_date))
	$cond['from_date'] =  $f_date;
}



if(isset($_GET['to_date'])) {
	$to_date = $_GET['to_date'];
	 $t_date=$this->fct->date_in_formate($to_date);
	$url .= '&to_date='.$from_date;
	if(!empty($to_date))
	$cond['to_date'] = $t_date;
}


$cond['return']=1;


if(checkIfSupplier_admin()){
$user=$this->ecommerce_model->getUserInfo();
$cond['id_user']=$user['id_user'];
$cond['id_role']=5;
}

// pagination  start :
$count_news = $this->admin_fct->getStockReturnOrders($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->admin_fct->getStockReturnOrders($cond,$config['per_page'],$page);

$data['url']=$url;

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}



public function view($section="",$id){
	$table="";
	if($section=="stock"){
		$table="products_stock";
		$product['product_type']='option';
		}
	if($section=="product"){
		$product['product_type']='product';
		$table="products";}	
if ($this->acl->has_permission('inventory','index') && !empty($table)){	
$data["title"]="View Report";
$data["content"]="back_office/inventory/add";


$cond["id_products"]=$id;
$cond["type"]=$section;
$inventory=$this->custom_fct->getProductsInventoryDetails($cond);

		$product['id_products']=$id;
		if($cond['type']=="product"){
			$product['product_type']='product';
			}else{
			$product['product_type']='option';}
			
		$product_quantities=$this->admin_fct->getProductsStock($product);
		$data['product_quantities']= $product_quantities;

$data['inventory']=$inventory;

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}



public function edit($id,$obj = ""){
if ($this->acl->has_permission('inventory','edit')){	
$data["title"]="Edit inventory";
$data["content"]="back_office/inventory/add";
$cond=array("id_inventory"=>$id);
$data["id"]=$id;
$data["info"]=$this->custom_fct->getProductsInventoryDetails($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('inventory','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_inventory",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/inventory/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_delivery_order($id=0,$rand=0,$section="stock"){
	$orderData=$this->fct->getonerecord('delivery_order',array('id_delivery_order'=>$id,'rand'=>$rand));

	$exported_order=$this->fct->getonerecord('delivery_order',array('id_old_delivery_order'=>$id));
	
$bool=true;
if ($this->acl->has_permission('delivery_order','delete') && !empty($orderData)){
	if($orderData['status']!=1){
		if(!empty($exported_order)){
	
$this->session->set_userdata("error_message","Sorry! you can't remove the order#".$id." since its exported. ");

$bool=false;
			}
			if($bool){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_delivery_order",$id);
$this->db->update("delivery_order",$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");}}else{
$this->session->set_userdata("error_message","Sorry! you can't remove the order#".$id." since its approved. ");	}

redirect(site_url("back_office/inventory/request/".$section));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_stock_return($id=0,$rand=0,$page="delivery_order"){
	$orderData=$this->fct->getonerecord('stock_return',array('id_stock_return'=>$id,'rand'=>$rand));

if ($this->acl->has_permission('stock_return','delete') && !empty($orderData)){
	if($orderData['status']!=1){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_stock_return",$id);
$this->db->update("stock_return",$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");}else{
$this->session->set_userdata("error_message","Sorry! you can't remove the order#".$id." since its approved. ");	}

redirect(site_url("back_office/inventory/".$page));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('inventory','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_inventory",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/inventory/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_inventory",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit inventory";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_inventory",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/inventory/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/inventory/".$image)){
unlink("./uploads/inventory/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'inventory')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/inventory/".$value."/".$image)){
unlink("./uploads/inventory/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_inventory",$id);
$this->db->update("inventory",$_data);
echo 'Done!';
}

}