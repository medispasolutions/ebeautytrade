<?

class Promocode extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "promo_code";
        $this->load->model("promocode_m");
        $this->lang->load("admin");
    }

    /**
     * This is a function to list categories for the supplier
     */
    public function listing()
    {
        if (!$this->acl->has_permission('brands', 'index')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PROMOCODE_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'List Promocode',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
            ],
        ];

        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];

        //set conditions
        if (!$user_id) {
            if ($parentId) {
                $cond['id_user'] = $parentId;
            } else {
                $cond['id_user'] = $this->acl->getCurrentUserId();
            }
        }

        $query = $this->db->query("SELECT pc.id_promo_code as id_promo_code,pc.code AS code, b.title AS brandName,p.title AS ProductName , pc.status AS status FROM promo_code as pc LEFT JOIN brands as b ON b.id_brands = pc.id_brands LEFT JOIN products as p ON p.id_products = pc.id_products WHERE pc.deleted = 0 AND pc.id_user =" . $cond['id_user']);

        foreach ($query->result_array() as $info) {
            $data['promocode'][] = [
                'rowId' => $info['id_promo_code'],
                'columns' => [
                    // $info['ProductName'],
                    $info['brandName'],
                    $info['code'],
                    $info['status']
                ]
            ];
        }

        $this->load->twigView("promoCodeListingPage/index", $data);
    }

    public function index($order = "")
    {

        $this->session->unset_userdata("admin_redirect_url");
        if ($this->acl->isCurrentUserASupplier()) {
            redirect(site_url("back_office/promocode/listing"));
        }
        if ($this->acl->has_permission('promocode', 'index')) {
            if ($order == "")
                $order = "sort_order";
            $data["title"] = "List Promocode";
            $data["content"] = "back_office/promocode/list";

            $this->session->unset_userdata("back_link");

            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "25";
            }
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
            $data["show_items"] = $show_items;
            // pagination  start :

            $cond = array();
            $like = array();
            $url = site_url("back_office/promocode") . "?pagination=on";
            $id_category = 0;
            $data['current_category'] = array();
            $data['parent_levels'] = array();

            $this->load->view("back_office/template", $data);

        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function add($data = array())
    {
        if ($this->acl->has_permission('promocode', 'add')) {
            $data["title"] = "Add promocode";
            $data["content"] = "back_office/promocode/add";

            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function view($id, $obj = "")
    {
        if ($this->acl->has_permission('promocode', 'index')) {
            $data["title"] = "View promocode";
            $data["content"] = "back_office/promocode/add";
            $cond = array("id_promo_code" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function edit($id, $obj = "")
    {
        $data = $obj;
        if ($this->acl->has_permission('promocode', 'edit')) {
            $data["title"] = "Edit promocode";
            $data["content"] = "back_office/promocode/add";
            $cond = array("id_promo_code" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $data["section"] = $data["info"]['section'];
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function delete($id)
    {
        if ($this->acl->has_permission('promo_code', 'delete')) {
            $_data = array("deleted" => 1,
                "deleted_date" => date("Y-m-d h:i:s"));
            $this->db->where("id_promo_code", $id);
            $this->db->update($this->table, $_data);
            $this->session->set_userdata("success_message", "Information was deleted successfully");
            redirect(site_url("back_office/promocode/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i + 1);
            $this->db->where("id_promo_code", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    /**
     * This is a function to edit a brand as a supplier
     * @var [] $formData Data to be used in the form (bad save)
     * @var [] $errors Data to be used in the form (bad save)
     */
    public function revise($id, $formData = null, $errors = null)
    {
        if (!$this->acl->has_permission('brands', 'edit')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PROMOCODE_EDIT,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Edit Promocode',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
                'MODE' => 'edit',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        $cond = array("id_promo_code" => $id);
        $categoryData = $this->fct->getonerecord($this->table, $cond);
        $data["info"] = $categoryData;
        $data['id_promo_code'] = $categoryData['id_promo_code'];
        $data['id_brands'] = $this->getBrandOptions($categoryData['id_brands']);
        $data['id_products'] = $this->getProductOptions($categoryData['id_products']);
        //$data["expire_date"] = $categoryData['expire_date'];

        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];
        
        $data['revisesection'] = 1;

        //check owner
        if ($parentId) {
            if ($data['info']['id_user'] != $parentId) {
                redirect(site_url("back_office"));
            }
        } else {
            if ($data['info']['id_user'] != $this->acl->getCurrentUserId()) {
                redirect(site_url("back_office"));
            }
        }

        if ($formData) {
            $data['info'] = $formData;
        } else {
            //additional errors
            $errors = [];
            if (!empty($errors)) {
                $data['globalErrors'] = $errors;
            }
        }

        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];

        if ($parentId) {
            $cond['id_user'] = $parentId;
        } else {
            $cond['id_user'] = $this->acl->getCurrentUserId();
        }

        $this->load->twigView("promoCodeAddEditPage/index", $data);
    }

    /**
     * This is the endpoint to save a brand as a supplier
     */
    public function save()
    {

        $promocodeId = $this->input->post("id_promo_code");

        //check owner
        if ($promocodeId) {
            $cond = array("id_promo_code" => $promocodeId);
            $promoCodeData = $this->fct->getonerecord($this->table, $cond);

            $parentUser = $this->acl->getCurrentUserId();
            $conduser = array("id_user" => $parentUser);
            $companyData = $this->fct->getonerecord('user', $conduser);
            $parentId = $companyData['id_parent'];

            if ($parentId) {
                if ($promoCodeData['id_user'] != $parentId) {
                    redirect(site_url("back_office"));
                }
            } else {
                if ($promoCodeData['id_user'] != $this->acl->getCurrentUserId()) {
                    redirect(site_url("back_office"));
                }
            }
        }

        $this->form_validation->set_rules("code", "code", "trim|required");
        $this->form_validation->set_rules("amount", "amount", "trim|required");
        $this->form_validation->set_rules("expire_date", "expire_date", "trim|required");

        if ($this->form_validation->run() == FALSE) {
            if ($promocodeId) {
                return $this->revise($promocodeId, $this->input->post(null), $this->form_validation->_error_array);
            } else {
                return $this->create($this->input->post(null), $this->form_validation->_error_array);
            }
        }

        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];

        if ($parentId) {
            $userid = $parentId;
        } else {
            $userid = $this->acl->getCurrentUserId();
        }
    
        if ($promocodeId) {
            $_data["code"] = $this->input->post("code", true);
            $_data["id_brands"] = $this->input->post("id_brands");
            $_data["id_products"] = 0;
            $_data["expire_date"] = $this->input->post("expire_date", true);
            $_data["amount"] = $this->input->post("amount", true);
            $_data["updated_date"] = date("Y-m-d h:i:s");
            $this->db->where("id_promo_code", $promocodeId);
            $this->db->update($this->table, $_data);
        } else {
            $number = $this->input->post('brand_amount');
        	for($i=1; $i<=$number; $i++)
        	{
        	    $brands = $this->input->post("id_brands".$i);
        	    if($brands){
                    $_data["code"] = $this->input->post("code", true);
                    $_data["id_brands"] = $brands;
                    $_data["id_products"] = $this->input->post("id_products", true);
                    $_data["expire_date"] = $this->input->post("expire_date", true);
                    $_data["amount"] = $this->input->post("amount", true);
                    $_data["id_user"] = $userid;
                    $_data["status"] = 1;
                    if ($this->acl->isCurrentUserASupplier()) {
                        //do not unpublish brands
                        //$_data["status"] = 2;
                    }
                    $_data["created_date"] = date("Y-m-d h:i:s");
                    $this->db->insert($this->table, $_data);
        	    }
        	}
        }
        redirect('back_office/promocode/listing/' . $promocodeId);
    }

    /**
     * This is a function to create a brand as a supplier
     * @var [] $formData Data to be used in the form (bad save)
     * @var [] $errors Data to be used in the form (bad save)
     */
    public function create($formData = null, $errors = null)
    {
        if (!$this->acl->has_permission('brands', 'add')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::PROMOCODE_ADD,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Add Promo Code',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
                'MODE' => 'add',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        if ($formData) {
            $data['info'] = $formData;
        }

        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];

        if ($parentId) {
            $data['id_user'] = $parentId;
        } else {
            $data['id_user'] = $this->acl->getCurrentUserId();
        }

        $data['id_brands'] = $this->getBrandOptions();
        $data['id_count'] = $this->getBrandCount();
        $data['id_products'] = $this->getProductOptions();

        $this->load->twigView("promoCodeAddEditPage/index", $data);
    }

    public function getBrandCount($selected = null, $user_id = null)
    {
        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];

        if ($parentId) {
            if (!$user_id) {
                $user_id = $parentId;
            }
        } else {
            if (!$user_id) {
                $user_id = $this->acl->getCurrentUserId();
            }
        }

        //brands
        $brands = $this->fct->getAll_cond('brands', 'title asc', ['id_user' => $user_id]);
        $options = 0;
        foreach ($brands as $brand) {
            $options++;
        }

        return $options;
    }

    public function getBrandOptions($selected = null, $user_id = null)
    {
        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];

        if ($parentId) {
            if (!$user_id) {
                $user_id = $parentId;
            }
        } else {
            if (!$user_id) {
                $user_id = $this->acl->getCurrentUserId();
            }
        }

        //brands
        $brands = $this->fct->getAll_cond('brands', 'title asc', ['id_user' => $user_id]);
        $options = [];
        foreach ($brands as $brand) {
            $options[] = [
                'VALUE' => $brand['id_brands'],
                'TEXT' => $brand['title'],
                'IS_SELECTED' => $brand['id_brands'] == $selected,
            ];
        }

        return $options;
    }

    public function getProductOptions($selected = null, $user_id = null)
    {
        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $parentId = $companyData['id_parent'];

        if ($parentId) {
            if (!$user_id) {
                $user_id = $parentId;
            }
        } else {
            if (!$user_id) {
                $user_id = $this->acl->getCurrentUserId();
            }
        }
        $products = $this->fct->getAll_cond('products', 'title asc', ['id_user' => $user_id]);
        $options = [];
        foreach ($products as $product) {
            $options[] = [
                'VALUE' => $product['id_products'],
                'TEXT' => $product['title'],
                'IS_SELECTED' => $product['id_products'] == $selected,
            ];
        }

        return $options;
    }
}