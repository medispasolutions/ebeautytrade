<?
class Group_products extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="orders";
$this->load->model("orders_m");
 $this->lang->load("admin"); 
}


/*public function index(){
$cond=array();
$order ="sort_order";
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('orders','index')){	
$url=site_url("back_office/orders/index?pagination=on");
$data["title"]="List group products";
$data["content"]="back_office/orders/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :



if(isset($_GET["keyword"])) {
	$keyword = $_GET["keyword"];
	$url .= "&keyword=".$keyword;
	if(!empty($keyword))
	$cond["keyword"] = $keyword;
}

$show_items = ($show_items == "All") ? $count_news : $show_items;
$this->load->library("pagination");

$count_news = $this->orders_m->getRecords($cond);
$config["base_url"] = $url;
$config["total_rows"] = $count_news;
$config["per_page"] =$show_items;
$config["use_page_numbers"] = TRUE;
$config["page_query_string"] = TRUE;
//print "<pre>";print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET["per_page"])) {
	if($_GET["per_page"] != "") $page = $_GET["per_page"];
	else $page = 0;
}
else $page = 0;
$data["info"] = $this->orders_m->getRecords($cond,$show_items,$page);
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}
*/
public function index($section=""){

		
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	

$order ="barcode";
$data["title"]="List products";
$data["content"]="back_office/products/list";

//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$cond = array();
$cond['All']="All";
$cond['admin']="admin";
$url = site_url('back_office/products?pagination=on');

if(isset($_GET['product_id'])) {
	$product_id = $_GET['product_id'];
	$url .= '&product_id='.$product_id;
	if(!empty($product_id))
	$cond['id_products'] = $product_id;
}

if(isset($_GET['product_name'])) {
	$product_name = $_GET['product_name'];
	$url .= '&product_name='.$product_name;
	if(!empty($product_name))
	$cond['title'.getFieldLanguage($this->lang->lang())] = $product_name;
}

if(isset($_GET['product_sku'])) {
	$product_sku = $_GET['product_sku'];
	$url .= '&product_sku='.$product_sku;
	if(!empty($product_sku))
	$cond['sku'] = $product_sku;
}

if(checkIfSupplier_admin()){
$cond["id_user"]=$this->session->userdata("uid");}
if(isset($_GET['category'])) {
	$category = $_GET['category'];
	$url .= '&category='.$category;
	if(!empty($category))
	$cond['category'] = $category;
}
if(isset($_GET['brand'])) {
	$brand = $_GET['brand'];
	$url .= '&brand='.$brand;
	if(!empty($brand))
	$cond['id_brands'] = $brand;
}

if(isset($_GET['sub_category'])) {
	$sub_category = $_GET['sub_category'];
	$url .= '&sub_category='.$sub_category;
	if(!empty($sub_category))
	$cond['category'] = $sub_category;	
}




if(isset($_GET['classifications'])) {
	$classifications = $_GET['classifications'];
	$url .= '&classifications='.$classifications;
	if(!empty($classifications))
	$cond['classifications'] = $classifications;
}

if(isset($_GET['designer'])) {
	$designer = $_GET['designer'];
	$url .= '&designer='.$designer;
	if(!empty($designer))
	 $cond['products.designer'] = $designer;
	
}



if($this->input->get('news')!="" && $this->input->get('news')!=0) {

$news = $_GET['news'];
$url .= '&news='.$news;
$cond['set_as_news'] = $news;
	
}

if($this->input->get('status')!="") {

	$status = $_GET['status'];
	$url .= '&status='.$status;

	$cond['status'] = $status;
}

if(isset($_GET['barcode'])) {
	$barcode = $_GET['barcode'];
	$url .= '&barcode='.$barcode;
	if(!empty($barcode))
	$cond['barcode'] = $barcode;
}

if(isset($_GET['display_in_home']) && $_GET['display_in_home']!=0) {
	$display_in_home = $_GET['display_in_home'];
	$url .= '&display_in_home='.$display_in_home;
	if(!empty($display_in_home))
	$cond['display_in_home'] = 1;
}

if(isset($_GET['availability']) && $_GET['availability']!=2) {

	$availability = $_GET['availability'];
	
	$url .= '&availability='.$availability;
	if(!empty($availability) || $availability==0)
	$cond['availability'] = $availability;
}

if(isset($_GET['sales']) && $_GET['sales']!=2) {
    $sales = $_GET['sales'];
	$url .= '&sales='.$sales;
	if(!empty($sales))
	$cond['sales'] = $sales;
	unset($cond['All']);
}

if(isset($_GET['published']) && $_GET['published']==1) {
    $published = $_GET['published'];
	$url .= '&published='.$published;
	if(!empty($published))
	$cond['published'] = $published;

}


$cond['group_products'] = 1;
$data['bool_kits'] = true;

// pagination  start :
$count_news = $this->ecommerce_model->getProducts($cond);

$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->ecommerce_model->getProducts($cond,$config['per_page'],$page);

$url .='&per_page='.$page;

$data['url']=$url;
$this->session->set_userdata("back_link",$url);
/*echo "<pre>";
print_r($data['info']);exit;*/
if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'export') {
$this->export_fct->productsExportToExcel($data['info']);
}

if(isset($_GET['report_submit']) && $_GET['report_submit'] == 'import') {
	redirect(site_url("back_office/excel_c/import?section=".$_GET['section']));}
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


//////////////////////////////////////////Submit////////////////////////////

public function checkQuantities()
{  $error=false;
	$success=false;
	$error_txt="";	
	$cond = array();

  $allPostedVals = $this->input->post(NULL,TRUE);
  $id_orders=$this->input->post('id_orders');
  $status=$this->input->post('status');
  $rand=$this->input->post('rand');
  
  $quantity_kits=$this->input->post('quantity_kits');
  $cond['rand']=$rand;
  $orderData=$this->admin_fct->getGroupProductsOrder($id_orders,$cond);
  $order=$orderData;
  
	if(!empty($orderData)){		
	$j=0;
	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
		foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];

					///////////////////////////CHECK QUANTITIES//////////////////////////////////////
						$line_item=$this->admin_fct->getGroupProductsLineItem($val);
						$id_products=$line_item['id_products'];
						$type=$line_item['type'];
						if($type=="option"){
							$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$id_products));
							$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
							$combination=$stock['combination'];
		                $options=unSerializeStock($product['combination']);
						 $j=0; foreach($options as $opt){$j++;
							if($j==1){
							echo $opt;
							}else{
						echo ", ".$opt;
							}}
						$price=$stock['price'];
						$product_name=$product['title'].' "'.$option.'"';
						$qty_product=$stock['quantity'];
						}else{
						$product=$this->fct->getonerecord('products',array('id_products'=>$id_products));
						$price=$product['price'];
						$product_name=$product['title'];
						$qty_product=$product['quantity'];
							}

				        $new_qty=($qty_product+($line_item['quantity']*$order['quantity']))-($qty*$quantity_kits);	
						if($new_qty>=0){
							$success=true;
						 }else{
							$error=true;
							$error_txt .='<p>The Quantity you are asking for "'.$product_name.'" is not available.</p>';
					  		 } }
						}else{
						$error=true;
		                   }
		


	if(!$error) {
		return true;
	}
	else {
		$this->form_validation->set_message('checkQuantities',$error_txt);
		return false;
	}
}
public function submit()
{	
  $id_orders=$this->input->post('id_orders');
  $status=$this->input->post('status');
  $rand=$this->input->post('rand');
 $this->form_validation->set_rules('quantity_kits',lang('trading_name'),'trim|required|callback_checkQuantities[]');		

				if($this->form_validation->run() == FALSE) {
							
				$return['result'] = 0;
				$return['errors'] = array();
				$return['message'] = 'Error!';
			    
				$find =array('<p>','</p>');
				$replace =array('','');
			
			   $this->session->set_userdata("error_message",validation_errors());
			   redirect(site_url("back_office/group_products/view/".$id_orders.'/'.$rand));
		
					}else {
	$error=false;
	$success=false;
	$error_txt="";	
$cond = array();
$section=$this->input->post('section');

$data['section']=$section;

if($section=="stock"){
$type=1;	
$export=0;
$page_title='Stock Request';
$order_name='R-Q';
$form='export';
$section_name='Stock Request';
}

  $allPostedVals = $this->input->post(NULL,TRUE);

  
  
$quantity_kits=$this->input->post('quantity_kits');



  
  $cond['rand']=$rand;
  $orderData=$this->admin_fct->getGroupProductsOrder($id_orders,$cond);
  
  
  
$order=$orderData;
if(!empty($orderData)){		
if($this->input->post('submit')=="Save Changes" && $orderData['status']!='paid' && $orderData['status']!='completed') {
	$update_order['id_user']=$this->input->post('id_users');
	$j=0;
	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	if($this->input->post('status')!=""){
		$update_order['status']=$this->input->post('status');
		}
	$total_cost=0;
	$total_price=0;
	$expire_date_arr=$this->input->post('expire_date');
						foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];
						$price=$price_arr[$key];
						$cost=$cost_arr[$key];
						$total_cost=$total_cost+($qty*$cost);
						$total_price=$total_price+($qty*$price);
						$expire_date=$this->fct->date_in_formate($expire_date_arr[$key]);
						
						///////////////////////////CHECK QUANTITIES//////////////////////////////////////
						$line_item=$this->admin_fct->getGroupProductsLineItem($val);
						$id_products=$line_item['id_products'];
						$type=$line_item['type'];
						if($type=="option"){
							$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$id_products));
							$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
							$combination=$stock['combination'];
		                $options=unSerializeStock($product['combination']);
						 $j=0; foreach($options as $opt){$j++;
							if($j==1){
							echo $opt;
							}else{
						echo ", ".$opt;
							}}
						$price=$stock['price'];
						$product_name=$product['title'].' "'.$option.'"';
						$qty_product=$stock['quantity'];
						}else{
						$product=$this->fct->getonerecord('products',array('id_products'=>$id_products));
						$price=$product['price'];
						$product_name=$product['title'];
						$qty_product=$product['quantity'];
							}
		
	
						$update_line_item['updated_date'] = date('Y-m-d h:i:s');
						$update_line_item['quantity'] = $qty;

					
				        $new_qty=($qty_product+($line_item['quantity']*$order['quantity']))-($qty*$quantity_kits);	
						if($new_qty>=0){
							$success=true;
							$this->updateQty($id_products,$type,$new_qty);
							$this->db->where("id_group_products_line_items",$val);
						   $this->db->update('group_products_line_items',$update_line_item);
						 }else{
							$error=true;
							$error_txt .='<p>The Quantity you are asking for "'.$product_name.'" is not available.</p>';
					  		 } }
							 
				
$update_kits['quantity']=$quantity_kits;
$cond_order['rand']=$rand;
$cond_order['id_products']=$id_orders;

$this->db->where($cond_order);
$this->db->update('products',$update_kits);			 


			if($success){
			$this->session->set_userdata("success_message","Your order was updated successfully");}
			
			if($error){
			$this->session->set_userdata("error_message",$error_txt);}	

	
					 redirect(site_url("back_office/group_products/view/".$id_orders.'/'.$rand));
			}
		

			
}else{
  $this->session->set_flashdata('error_message',lang('access_denied'));
  redirect(site_url("back_office/group_products"));}

}}
//////////////////////////////////////////Submit Details////////////////////////////
public function validate_sku()
{
	$sku = $this->input->post('sku');
	$cond['sku'] = $sku;
	if($this->input->post('id') != '') {
		$cond['id_products !='] = $this->input->post('id');
	}
	$check = $this->fct->getonerecord('products',$cond);
	if(empty($check)) {
		return true;
	}
	else {
		$this->form_validation->set_message('validate_sku','SKU exists, please use another SKU value.');
		return false;
	}
}

public function validate_barcode()
{
	$barcode = $this->input->post('barcode');
	$cond['barcode'] = $barcode;
	if($this->input->post('id') != '') {
		$cond['id_products !='] = $this->input->post('id');
	}
	$check = $this->fct->getonerecord('products',$cond);
	if(empty($check) || empty($barcode)) {
		return true;
	}
	else {
		$this->form_validation->set_message('validate_barcode','Barcode exists, please use another Barcode value.');
		return false;
	}
}

public function checkUploadFile(){
$size=$_FILES['datasheet']['size'];
$size_arr=getMaxSize('file');
$max_size=$size_arr['size'];

	if($max_size>$size) {
		return true;
	}
	else {
		$this->form_validation->set_message('checkUploadFile','File exceeds the maximum upload size('.$size_arr['size_mega'].' Mb).');
		return false;
	}
}

public function checkUploadFile2()
{
	if(isset($_POST['description_product_info'])){
$bool=true;
$product_descriptions = $_POST['description_product_info'];
$size_arr=getMaxSize('file');
$max_size=$size_arr['size'];
foreach($product_descriptions as $key=>$info) {
if(!empty($_FILES["file_product_info_".$key]["name"])) {
$size=$_FILES["file_product_info_".$key]["size"];

if($max_size>$size) {
	}else {
$bool=false;
	}}}

if($bool) {
		return true;
	}else {
		$this->form_validation->set_message('checkUploadFile2','File exceeds the maximum upload size('.$size_arr['size_mega'].' Mb).');
		return false;
	}}
}


public function submit_details(){
$lang = "";

/*$products=$this->fct->getAll('products','sort_order');
foreach($products as $product){
	if(empty($product['brief_one'])){
		$product['brief_one']="";}
		if(empty($product['brief_two'])){
		$product['brief_two']="";}
		
	$brief=$product['brief_one'].' '.$product['brief_two'];
	$update['brief']=$brief;
	$this->db->where(array('id_products'=>$product['id_products']));
	$this->db->update('products',$update);
	
	}*/

if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit products";
$this->form_validation->set_rules("weight", "weight", "trim|required");
$this->form_validation->set_rules("dimensions", "dimensions", "trim");
$this->form_validation->set_rules("id_delivery_terms", "Delivery Terms", "trim");
/*$this->form_validation->set_rules("categories", "categories", "trim|required");*/
$this->form_validation->set_rules("id_brands", "brand", "trim");
//$this->form_validation->set_rules("category", "CATEGORY", "trim");
$this->form_validation->set_rules("sub_category", "SUB CATEGORY", "trim");
$this->form_validation->set_rules("exclude_from_shipping_calculation", "exclude from shipping calculation", "trim");
$this->form_validation->set_rules("delivery_day", "Delivery Day", "trim");
$this->form_validation->set_rules("delivery_days", "DELIVERY DAY(S)", "trim|integer");
$this->form_validation->set_rules("type", "TYPE", "trim");
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("fabric", "fabric", "trim");
$this->form_validation->set_rules("location_code", "Location Code", "trim");
$this->form_validation->set_rules("work", "work", "trim|max_length[65]");
$this->form_validation->set_rules("washing_instructions", "washing instructions", "trim|max_length[160]");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("sku", "sku", "trim|required|callback_validate_sku[]");
$this->form_validation->set_rules("barcode", "barcode", "trim|callback_validate_barcode[]");

$this->form_validation->set_rules("quantity", "quantity", "trim");
/*$this->form_validation->set_rules("discount", "discount", "trim|integer");*/
$this->form_validation->set_rules("list_price", "price", "trim|required");

if($this->input->post('set_as_clearance')==""){	
$this->form_validation->set_rules("length", "length", "trim|required");
$this->form_validation->set_rules("height", "height", "trim|required");
$this->form_validation->set_rules("width", "width", "trim|required");}

$this->form_validation->set_rules("weight", "weight", "trim|required");
$this->form_validation->set_rules("retail_price", "Retail Price", "trim");

/*$this->form_validation->set_rules("brief_one", "weight", "trim");
$this->form_validation->set_rules("brief_two", "weight", "trim");*/
$this->form_validation->set_rules("brief", "brief", "trim");

$this->form_validation->set_rules("discount", "discount", "trim");
$this->form_validation->set_rules("price", "price", "trim");
$this->form_validation->set_rules("description".$lang, "description", "trim");
$this->form_validation->set_rules("brief".$lang, "brief", "trim|max_length[220]");
$this->form_validation->set_rules("features".$lang, "features", "trim");
$this->form_validation->set_rules("specifications".$lang, "specifications", "trim");
$this->form_validation->set_rules("related_products".$lang, "related_products", "trim");
$this->form_validation->set_rules("downloads".$lang, "downloads", "trim");
$this->form_validation->set_rules("why_and_when".$lang, "why_and_when", "trim");
$this->form_validation->set_rules("video", "video", "trim");
$this->form_validation->set_rules("status", "status", "trim");
$this->form_validation->set_rules("brand", "brand", "trim");
$this->form_validation->set_rules("unit", "unit", "trim");
$this->form_validation->set_rules("datasheet", "datasheet", "trim|callback_checkUploadFile[]");
$this->form_validation->set_rules("tabs_files", "datasheet", "trim|callback_checkUploadFile2[]");
if($this->input->post("set_general_miles")){
$this->form_validation->set_rules("general_miles", "General Miles", "trim|required");	
}

if($this->input->post("set_as_redeemed_by_miles")){
$this->form_validation->set_rules("redeem_miles", " Amount of Miles", "trim|required");	
	}	

	
if($this->input->post("discount") != "" && $this->input->post("discount") != 0)
$this->form_validation->set_rules("discount_expiration", "discount expiration"."(".$this->lang->lang().")", "trim");
else 
$this->form_validation->set_rules("discount_expiration", "discount expiration"."(".$this->lang->lang().")", "trim");

if ($this->form_validation->run() == FALSE){ 


	$data = array();
	$data['tab']=2;
	$data['errors']='<div class="alert alert-error">'.validation_errors().'</div>';
	
if(isset($_POST['title_product_info'])){
		$infomrations1 = $_POST['title_product_info'];
		$infomrations2 = $_POST['description_product_info'];
		$infomrations3=  $_POST['video_product_info'];
		$i=0;
		$product_informations=array();
foreach($infomrations1 as $key=>$val){
	$product_informations[$i]['id_product_information']=$key;
	$product_informations[$i]['title']=$val;
	$product_informations[$i]['description']=$infomrations2[$key];
	$product_informations[$i]['video']=$infomrations3[$key];

	$product_informations[$i]['file']=$this->input->post('file_product_info_label_'.$key);
	$product_informations[$i]['file2']=$this->input->post('file2_product_info_label_'.$key);
	$i++;
	}
	
	$data['product_informations']=$product_informations;}

	/*if(isset($_POST['categories'])) {
		$data['selected_options'] = $_POST['categories'];
	}*/
if(isset($_POST['categories'])) {
	$data["h_selected_options"]=$this->input->post("categories");
	}
	
if(isset($_POST['related_products_arr'])) {
	$data["h_selected_options2"]=$this->input->post("related_products_arr");
	}	
	
$data["set_as_clearance"]=$this->input->post("set_as_clearance");

$data["set_as_non_exportable"]=$this->input->post("set_as_non_exportable");

	
if(isset($_POST['set_general_miles'])) {
$data["set_general_miles"]=$this->input->post("set_general_miles");
	}
if(isset($_POST['set_as_redeemed_by_miles'])) {
$data["set_as_redeemed_by_miles"]=$this->input->post("set_as_redeemed_by_miles");

	}	
	
if(isset($_POST['set_as_new'])) {
$data["set_as_new"]=$this->input->post("set_as_new");
	}
	
$data["set_as_soon"]=$this->input->post("set_as_soon");
$data["set_as_pro"]=$this->input->post("set_as_pro");	
if($this->input->post("id")!="") {
	
	if(isset($_POST['promote_to_front'])) {
		$data["promote_to_front"]=$this->input->post("promote_to_front");
	}
	if(isset($_POST['display_in_home'])) {
$data["display_in_home"]=$this->input->post("display_in_home");
	}
	
		if(isset($_POST['exclude_from_shipping_calculation'])) {
$data["exclude_from_shipping_calculation"]=$this->input->post("exclude_from_shipping_calculation");
	}
	
	if(isset($_POST['hide_price'])) {
		$data["hide_price"]=$this->input->post("hide_price");
	}

$this->view($this->input->post("id"),$this->input->post("rand"),$data);
}
else {
		if(isset($_POST['display_in_home'])) {
$_data["display_in_home"]=$this->input->post("display_in_home");
	}
	
		if(isset($_POST['set_as_news'])) {
$data["set_as_news"]=$this->input->post("set_as_news");
	}
		if(isset($_POST['set_as_clearance'])) {
$data["set_as_clearance"]=$this->input->post("set_as_clearance");
	}
	if(isset($_POST['promote_to_front'])) {
		$data["promote_to_front"]=$this->input->post("promote_to_front");
	}
	if(isset($_POST['hide_price'])) {
		$data["hide_price"]=$this->input->post("hide_price");
	}
	

	$this->add($data);
}
}
else
{
$this->load->model('products_m');
$rand=$this->input->post('rand');	
$user=$this->fct->getonerecord('user',array('id_user'=>$this->session->userdata("uid")));

if(checkIfSupplier_admin()){
$_data["id_user"]=$this->session->userdata("uid");

}

$_data["list_price"]=$this->input->post("list_price");
$_data["price"]=$this->input->post("price");

if(!checkIfSupplier_admin()){
	$_data["availability"]=$this->input->post("availability");
$_data["set_as_clearance"]=$this->input->post("set_as_clearance");

$_data["set_as_non_exportable"]=$this->input->post("set_as_non_exportable");
/*$_data["id_categories"]=$this->input->post("category");*/
$_data["display_in_home"]=$this->input->post("display_in_home");
$_data["redeem_miles"]=$this->input->post("redeem_miles");
$_data["set_as_new"]=$this->input->post("set_as_new");
$_data["set_as_soon"]=$this->input->post("set_as_soon");
$_data["set_as_pro"]=$this->input->post("set_as_pro");
$_data["set_general_miles"]=$this->input->post("set_general_miles");
$_data["general_miles"]=$this->input->post("general_miles");
$_data["exclude_from_shipping_calculation"]=$this->input->post("exclude_from_shipping_calculation");

if($this->input->post("id_delivery_terms")==4){
$_data["exclude_from_shipping_calculation"]=1;	
	}
$_data["set_as_redeemed_by_miles"]=$this->input->post("set_as_redeemed_by_miles");
$_data["amount_of_miles"]=$this->input->post("amount_of_miles");
if(isset($_POST['display_in_home'])) {
$_data["display_in_home"]=$this->input->post("display_in_home");
	}
/*	if($this->input->post('set_general_miles')==1 && $_data["general_miles"]>0){
	$price_miles=($_data["general_miles"]*$_data["price"])/100;
	$miles=round(($price_miles/10),0);
	$_data["miles"] = $miles;}*/
}
$_data["miles"] = $this->input->post("miles");
$_data["retail_price"]=$this->input->post("retail_price");
/*$_data["brief_one"]=$this->input->post("brief_one");
$_data["brief_two"]=$this->input->post("brief_two");*/

$_data["id_brands"]=$this->input->post("id_brands");
$_data["id_delivery_terms"]=$this->input->post("id_delivery_terms");
$_data["id_miles"]=$this->input->post("id_miles");
$_data["weight"]=$this->input->post("weight");
$_data["delivery_day"]=$this->input->post("delivery_day");
$_data["width"]=$this->input->post("width");
$_data["height"]=$this->input->post("height");
$_data["length"]=$this->input->post("length");
$_data["threshold"]=$this->input->post("threshold");
$_data["dimensions"]=$this->input->post("dimensions");
$_data["account_type"]=$this->input->post("account_type");
$_data["delivery_days"]=$this->input->post("delivery_days");
$_data["id_units"]=$this->input->post("unit");

//$_data["id_types"]=$this->input->post("type");
$_data["title"]=$this->input->post("title");
$_data["brief"]=$this->input->post("brief");
$_data["location_code"]=$this->input->post("location_code");
$_data["meta_title"]=$this->input->post("meta_title");
if($this->input->post("title_url") == "")
$title_url = $this->input->post("title");
else
$title_url = $this->input->post("title_url");
if($this->input->post("lang") == 'ar') {
	$this->load->model('title_url_ar');
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL('products',$title_url,$this->input->post("id"),'title_url'.$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL("products",url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["sku"]=$this->input->post("sku");
$_data["barcode"]=$this->input->post("barcode");
//$_data["quantity"]=$this->input->post("quantity");

$_data["discount"]=$this->input->post("discount");


//if($_data["discount"] != 0 && $_data["price"] != 0 && $_data["price"] == $_data["list_price"])
//$_data["price"] = round($_data["list_price"] - (($_data["list_price"] * 10) / 100));

if($_data["discount"] > 0) {
  $testprice = round($_data["list_price"] - (($_data["list_price"] * $_data["discount"]) / 100));
  if($_data["price"] != $testprice) {
	  $_data["price"] = $testprice;
  }
}
else {
  $_data["price"] = $_data["list_price"];
}

if($_data["list_price"]>$_data["price"]){
	$_data["discount_status"]=1;
	}else{
	$_data["discount_status"]=0;
	}

$_data["description"]=$this->input->post("description");


if(!empty($_FILES["datasheet"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_products"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("products","datasheet",$cond_image);
if(!empty($old_image) && file_exists('./uploads/products/'.$old_image)){
unlink("./uploads/products/".$old_image); 
 } }
$image1= $this->fct->uploadImage("datasheet","products");
$_data["datasheet"]=$image1;	
}

if(!empty($_FILES["image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_products"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("products","image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/products/'.$old_image)){
unlink("./uploads/products/".$old_image);
$sumb_val1=explode(",","800x590","515x570","342x494","342x494",'114x165');
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/products/".$value."/".$old_image)){
unlink("./uploads/products/".$value."/".$old_image);	 }							
} 
 } }
$image1= $this->fct->uploadImage("image","products");
$this->fct->createthumb($image1,"products","800x590,515x570,342x494,114x165");
$_data["image"]=$image1;	
}
	

$_data["discount_expiration"]=$this->fct->date_in_formate($this->input->post("discount_expiration"));


	
$_data["hide_price"] = 0;
if(isset($_POST['hide_price'])) {
$_data["hide_price"]=$this->input->post("hide_price");}

if(checkIfSupplier_admin()){
	$user=$this->ecommerce_model->getUserInfo();

$_data["published"] = 1;
$_data["status"] = 2;

}else{
	$_data["status"]=$this->input->post("status");
	
	if($_data['status']==0){
		$_data["published"] = 0;
		$_data["inform_admin"] = 0;
	}
}


	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_products",$this->input->post("id"));
	$this->db->update('products',$_data);
	$new_id = $this->input->post("id");
	$new=0;
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	

	$_data["sort_order"]=0;
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert('products',$_data); 
	$new_id = $this->db->insert_id();
	$new=1;
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	//////////////////INFORM Client//////
	if(isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
		$product=$this->fct->getonerecord('products',array('id_products'=>$new_id));
		/*$user_brand=$this->fct->getonerecord('user',array('id_brands'=>$_data['id_brands']));*/
		$supplier=$this->fct->getonerecord('user',array('id_user'=>$product['id_user']));
		$this->load->model("send_emails");
		$this->send_emails->informProductStatusToClient($supplier,$product);
	}
	//////////////////INFORM ADMIN//////
	$product=$this->fct->getonerecord('products',array('id_products'=>$new_id));
//////////////////Threshold//////	
$this->admin_fct->checkThreshold($product);

$product['new']=$new;
	$this->custom_fct->informProductModificationsToAdmin($product);
	$product=$this->fct->getonerecord('products',array('id_products'=>$new_id));

	$categories = array();
	if(isset($_POST['categories']) && !empty($_POST['categories'])) {
		$categories = $_POST['categories'];
	}
	


	$this->fct->insert_product_categories($new_id,$categories);
	
		$related_products = array();
	if(isset($_POST['related_products_arr']) && !empty($_POST['related_products_arr'])) {
		$related_products = $_POST['related_products_arr'];
	}


	$this->fct->insert_product_related($new_id,$related_products);
	


	$sub_categories = array();
	if(isset($_POST['sub_categories']) && !empty($_POST['sub_categories'])) {
		$sub_categories = $_POST['sub_categories'];
	}

	$this->fct->insert_product_sub_categories($new_id,$sub_categories);





///////////////////////////////////////////Brand Training//////////////////////////////////////////	

if(isset($_POST['description_product_info']) && !empty($_POST['description_product_info']) && isset($_POST['title_product_info']) && !empty($_POST['title_product_info'])) {

		$infomrations1 = $_POST['title_product_info'];
		$infomrations2 = $_POST['description_product_info'];
		$infomrations3=  $_POST['video_product_info'];
	
	

		$this->products_m->insert_product_informations($new_id,$infomrations1,$infomrations2,$infomrations3);
	}
$redirect_link=site_url('back_office/group_products/view/'.$new_id.'/'.$rand).'#tab-2';

redirect($redirect_link);
		
	

}
	
}



public function view($id=0,$rand,$return=array()){
	$data=$return;
if ($this->acl->has_permission('orders','index')){	
$data["title"]="View Group Product(#".$id.")";
$data["content"]="back_office/group_products/edit";
$data['section_name']=lang('group_products');
$data['page_title']=lang('group_products');
$data["id"]=$id;
$cond=array('rand'=>$rand);
$data["info"]=$this->admin_fct->getGroupProductsOrder($id,$cond);
$data["id_products"]=$id;



$data["userData"]=$data["info"]['user'];
$data['order_details'] = $data["info"];
/*print '<pre>';
print_r($data['order_details']);
exit;*/
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}

} 


public function add($id=0){
	
$cond = array();
$page_title=lang('group_products') ;
$data['page_title']=$page_title;

						// created order
						$rand=rand();
						$site_info=$this->fct->getonerow('settings',array('id_settings'=>1));
						$order['created_date'] = date('Y-m-d h:i:s');
						$order['id_user'] = 0;
						$order['rand'] = $rand;
						$order['quantity'] = 1;
						$order['group_products'] = 1;
						$user=$this->ecommerce_model->getUserInfo();
					
						/*$order['status'] = $this->input->post('status');*/
						$order['status'] ='pending';
						/*$order['lang'] = $this->lang->lang();*/
						$this->db->insert('products',$order);
					
						$order_id = $this->db->insert_id();
						redirect(site_url('back_office/group_products/view/'.$order_id.'/'.$rand));
} 


//////////////////////////////////////////Checkout////////////////////////////
public function insertNewProducts()
{
	$error=false;
	$success=false;
	$error_txt="";	
	
$order_id=$this->input->post('id_orders');
$rand=$this->input->post('rand');
$cond=array('rand'=>$rand);
$order=$this->admin_fct->getGroupProductsOrder($order_id,$cond);
$user = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
			$allPostedVals = $this->input->post(NULL,TRUE);

			$this->form_validation->set_rules('id_role','Role','trim|xss_clean');
				
			$this->form_validation->set_rules('id_user',lang('trading_name'),'trim|required|xss_clean');		
					// run form validation
					// if error
				if($this->form_validation->run() == FALSE) {
							
				$return['result'] = 0;
				$return['errors'] = array();
				$return['message'] = 'Error!';
			    $return['captcha'] = $this->fct->createNewCaptcha();
				$find =array('<p>','</p>');
				$replace =array('','');
				foreach($allPostedVals as $key => $val) {
					if(form_error($key) != '') {
						$return['errors'][$key] = str_replace($find,$replace,form_error($key));
					}
				}
				die(json_encode($return));
					}else {
	
	
	
/*	$brands_arr=$this->input->post('brands');
	$id_brand=$brands_arr[0];*/
	$id_brands=$this->input->post('brands');
	

	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$checklist=$this->input->post('checklist');
	$section=$this->input->post('section');
	$type_arr=$this->input->post('type');
	$brands_arr=$this->input->post('brands');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$expire_date_arr=$this->input->post('expire_date');
	
	$total_cost=0;
	$total_price=0;


				$id_user=$this->input->post('id_user');
			
					$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
						if(isset($checklist[$key]) || count($ids_arr)<2){
     						// Miles
						$id_products=$val;
						$qty=$qty_arr[$key];	
					    $id_brands=$brands_arr[$key];
						$price=$price_arr[$key];
						if(empty($price)){
							$price=0;}
						$cost=$cost_arr[$key];
						if(empty($cost)){
							$cost=0;}
						$expire_date=$expire_date_arr[$key];
						$brand_title=$this->fct->getonecell('brands','title',array('id_brands'=>$id_brands));
						$type=$type_arr[$key];
						$combination="";
						if($type=="option"){
							$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$id_products));
							$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
							$combination=$stock['combination'];
		                $options=unSerializeStock($combination);
						 $j=0; foreach($options as $opt){$j++;
							if($j==1){
							$option_txt=$opt;
							}else{
						    $option_txt.", ".$opt;
							}}
						$price=$stock['price'];
						$product_name=$product['title'].' "'.$option_txt.'"';
						$qty_product=$stock['quantity'];
						}else{
						$product=$this->fct->getonerecord('products',array('id_products'=>$id_products));
	 					$group_products_line_items3=$this->fct->getAll_cond_1('group_products_line_items','id_group_products_line_items',array('type'=>'product','id_products'=>$id_products),'desc');
						/*if(!empty($group_products_line_items3)){
						$cost=$group_products_line_items3[0]['cost'];}*/
						$price=$product['price'];
						$product_name=$product['title'];
						$qty_product=$product['quantity'];
							}
						
						    $total_cost=$total_cost+($qty*$cost);
					        $total_price=$total_price+($qty*$price);
							$line_item['created_date'] = date('Y-m-d h:i:s');
							$line_item['id_products'] = $id_products;
							$line_item['quantity'] = $qty;

							$line_item['id_orders'] = $order_id;
							$line_item['options'] = $combination;
							$line_item['brands_title'] = $brand_title;
							$line_item['id_brands'] = $id_brands;
							$line_item['type'] = $type;
							
						$total_qty=$qty*$order['quantity'];	
						if($qty_product>=$total_qty){
							$success=true;
							$new_qty=$qty_product-$total_qty;
							$this->updateQty($id_products,$type,$new_qty);
							$this->db->insert('group_products_line_items',$line_item);}else{
							$error=true;
							$error_txt .='<p>The Quantity you are asking for "'.$product_name.'" is not available.</p>';
					  		 } } }

			if(!$error){
			$this->session->set_userdata("success_message","Your order was updated successfully");}
			
			if($error){
			$this->session->set_userdata("error_message",$error_txt);}	
			// $return['message'] = lang('system_is_redirecting_to_page');
			$return['result'] = 1;
			$return['redirect_link'] = site_url("back_office/group_products/view/".$order_id."/".$rand);
			$return['message'] = lang('system_is_redirecting_to_page');
			die(json_encode($return));
		}
		
		
		

}
public function updateQty($id_product,$type,$new_qty){
	$update['quantity']=$new_qty;
	if($type=="option"){
	$cond['id_products_stock']=$id_product;	
	$this->db->where($cond);
	$this->db->update('products_stock',$update);
		}else{
	$cond['id_products']=$id_product;
	$this->db->where($cond);
	$this->db->update('products',$update);			}
	
	}
public function getBrands(){
	$cond=array();
	$cond2=array();
$id = $this->input->post('id_supplier');

$return['result']=5;
$return['html']="";
if($id!="all" && $id!=""){
$cond['id_user']=$id;}
$brands_arr=array(-1);
$brands = $this->fct->getAll_cond('brands','title asc',$cond);

$html ="<label>Brand *</label>";

$html .="<select name='brands' id='brands'>";
$html .="<option value=''>-Select All-</option>";

foreach($brands as $val){

	array_push($brands_arr,$val['id_brands']);
$html .="<option value=".$val['id_brands'].">".$val['title']."</option>";
 } 
$html .="</select>";
	
$return['result']=1;


///////////////////////////////////////GET PRODUCTS By Supplier/////////////////

$cond2['brands']=implode(',',$brands_arr);

/*$cond['threshold']=1;*/
$cc= $this->ecommerce_model->getInventory($cond2);
$data['info'] = $this->ecommerce_model->getInventory($cond2,$cc,0);

if(!empty($data['info']) && $id!="all" && $id!=""){
$return['result2']=1;
$return['html2']=$this->load->view('back_office/blocks/product_stock_threshold',$data,true);
}else{
$return['result2']=0;
$return['html2']="";}


$return['html']=$html;

die(json_encode($return));
 
}

	public function updateOrderPrices($id_order,$rand){


  	$cond['rand']=$rand;
	$orderData=$this->admin_fct->getGroupProductsOrder($id_order,$cond);
	$order=$orderData;

	$total_cost=0;
	$total_price=0;
	$j=0;
	$brands_line_items=$orderData['line_items'];
foreach($brands_line_items as $brand) {
 $line_items=$brand['line_items'];

			foreach($line_items as $line_item) {$j++;
					//$total_cost=$total_cost+($line_item['quantity']*$line_item['cost']);	
					$total_price=$total_price+($line_item['quantity']*$line_item['price']);	
						}}
						
						/*$update_order['total_cost']=$total_cost;*/
						$update_order['total_price']=$total_price;
						$update_order['total_price_currency']=$total_price;

		$amount=$total_price;	
		
/*		$update_order['shipping_charge_currency'] = $this->input->post('shipping');
		$update_order['shipping']=changeCurrency($update_order['shipping_charge_currency'],"",$this->config->item('default_currency'),$order['currency']);
		$amount = $amount-$order['shipping_charge_currency']+$update_order['shipping_charge_currency'];
	
		if($amount>$this->input->post('discount')){	
		$update_order['discount'] = $this->input->post('discount');
		$amount = $amount+$order['discount']-$update_order['discount'];
		}
		*/
		/*$update['customer_shipping_charge'] = $customer_shipping_charge;*/
		
		$update_order['amount']=changeCurrency($amount,"",$this->config->item('default_currency'),$order['currency']);
		$update_order['amount_currency']=$amount;

						//print '<pre>';print_r($order);
					 $this->db->where($cond);
					 $this->db->update('orders',$update_order);
					 
						
					
					

						//print '<pre>';print_r($order);
						$this->db->where($cond);
						$this->db->update('orders',$update_order);	
					
return true;
}

public function remove_product($id_line_item,$id_order,$rand){
	$order=$this->fct->getonerecord('products',array('id_products'=>$id_order,'rand'=>$rand));
	
	
	if(!empty($order)){
	$cond['id_orders']=$id_order;
	$cond['id_group_products_line_items']=$id_line_item;
	$line_item=$this->fct->getonerow('group_products_line_items',$cond);
	if(!empty($line_item)){

						$id_products=$line_item['id_products'];
						$type=$line_item['type'];
						if($type=="option"){
							$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$id_products));
							$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
							$qty_product=$stock['quantity'];
						}else{
							$product=$this->fct->getonerecord('products',array('id_products'=>$id_products));
							$qty_product=$product['quantity'];
							}
							
	$new_qty=$qty_product+($line_item['quantity']*$order['quantity']);		
						
	$this->updateQty($id_products,$type,$new_qty);	
		
	$this->db->delete('group_products_line_items',$cond);}
	
	$check=$this->fct->getAll_cond('group_products_line_items','sort_order',array('id_orders'=>$id_order));

	if(empty($check)){
$update_kits['quantity']=1;
$cond_order['rand']=$rand;
$cond_order['id_products']=$id_order;
$this->db->where($cond_order);
$this->db->update('products',$update_kits);}
	/*$this->updateOrderPrices($id_order,$rand);*/
	$this->session->set_userdata("success_message","Item was deleted successfully");
	redirect(site_url("back_office/group_products/view/".$id_order."/".$rand));}
	
	
	}
	
/////added for plupload plugin////////////////////////////////////////////////////////////////////////////////////////
public function loadGallery(){
		$content_type=$this->input->post('content_type');
		$id=$this->input->post('id');
		$type=$this->input->post('type');
		$data['content_type'] = $content_type;
		$data['id'] = $id;
		//$data['gallery']=$this->fct->getAll_cond($content_type.'_gallery','sort_order',array('id_'.$content_type=>$id,"type"=>$type));
		$data['gallery']=$this->site_model->getFileGallery($content_type,$id,array("image","masterkey"));
		$this->load->view('back_office/group_products/gallery/ajax_gallery',$data);
}
	
public function upload_gal_image($content_type,$id_gallery,$type)
{
	$type_record = $this->fct->getonerow('content_type',array('name'=>$content_type));
	$thumb_vals = array();
	if(!empty($type_record['thumb_val_gal'])) {
		$thumb_vals = explode(',',$type_record['thumb_val_gal']);
	}
	$image_name = $this->fct->uploadImage("file",$content_type."/gallery");
	if($type == "image") {
		$this->fct->createthumb1($image_name,$content_type."/gallery","120x120");
		//print '<pre>';print_r($thumb_vals);exit;
		if(!empty($thumb_vals)) {
			foreach($thumb_vals as $val) {
				if($val != '') {
					$this->fct->createthumb1($image_name,$content_type."/gallery",$val);
				}
			}
		}
	}
	$data['type'] = $type;
	$data['image'] = $image_name;
	$data['created_date'] = date('Y-m-d h:i:s');
	$data['id_'.$content_type] = $id_gallery;
	$this->db->insert($content_type.'_gallery',$data);
	//echo 'done';
}

public function delete_gal_image(){
	$content_type = $this->input->post('content_type');
	$id = $this->input->post('id');
	$id_image = $this->input->post('id_image');
	$type_record = $this->fct->getonerow('content_type',array('name'=>$content_type));
	$imageData = $this->fct->getonerow($content_type.'_gallery',array('id_gallery'=>$id_image));
	if($imageData['type'] == 'image') {
		$image = $imageData['image'];
		$thumb_vals = array();
		if(!empty($type_record['thumb_val_gal'])) {
			$thumb_vals = explode(',',$type_record['thumb_val_gal']);
		}
		if(file_exists("./uploads/".$content_type."/gallery/".$image)){
			unlink("./uploads/".$content_type."/gallery/".$image); 
		}
		if(file_exists("./uploads/".$content_type."/gallery/120x120/".$image)){
						unlink("./uploads/".$content_type."/gallery/120x120/".$image); 
		}
		if(!empty($thumb_vals)) {
			foreach($thumb_vals as $val) {
				if(!empty($val)) {
					if(file_exists("./uploads/".$content_type."/gallery/".$val."/".$image)){
						unlink("./uploads/".$content_type."/gallery/".$val."/".$image); 
					}
				}
			}
		}
	}
	$this->db->where("id_gallery",$id_image);
	$this->db->delete($content_type.'_gallery');
	echo '';
}
	
public function sort_order_gallery(){
	$newOrder = array();
	$content_type= $this->input->post('content_type');
	$newOrder=$this->input->post('newOrder');
	$newOrder = explode(',',$newOrder);	
	if(!empty($newOrder)) {
		$i=0;
		foreach($newOrder as $order) {
			$data['updated_date'] = date('Y-m-d h:i:s');
			$data['sort_order'] = $i + 1;
			$this->db->where('id_gallery',$order);
			$this->db->update($content_type.'_gallery',$data);
			$i++;
		}
	}
	echo '';
}	
	
public function insertStockOrder()
{
$checklist=$this->input->post('checklist');		
$result=0;
$return['message']="";	
if($this->input->post('ids')!=""){
$bool=true;
$result=1;
if(isset($checklist) && empty($checklist)){
$bool=false;
$result=0;
$return['message']="Please select at least one item.";
}$return['result']=$result;
	}
	if($bool){
	
	$user = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$checklist=$this->input->post('checklist');
	$order_id=$this->input->post('id_stock_return');
	$id_orders=$this->input->post('id_orders');
	$orders=$this->fct->getonerecord('orders',array('id_orders'=>$id_orders));		
	$stock_return=$this->fct->getonerecord('stock_return',array('id_stock_return'=>$order_id));	
	if($orders['id_user']!=$stock_return['id_user'] && !empty($stock_return)){
	$cond_delete['id_orders']=$order_id;
	$this->db->delete('stock_return_line_items',$cond_delete);
	
			}
	
	if(!empty($stock_return)){
	$cond_update['id_stock_return']=$order_id;
	$update['id_user']=$orders['id_user'];
	$this->db->where($cond_update);
	$this->db->update('stock_return',$update);	}		
					
					$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
						if(isset($checklist[$key])){
							
     						// Miles
						$id_line_items=$val;
						$qty=$qty_arr[$key];	
				
							$line_item['created_date'] = date('Y-m-d h:i:s');
							$line_item['id_line_items'] = $id_line_items;
							$line_item['quantity'] = $qty;
							$line_item['id_orders'] = $order_id;
	
							// options

						$this->db->insert('stock_return_line_items',$line_item);}
						}
						

	

				//generate invoice templat
	/*			$orderData = $this->ecommerce_model->getOrder($order_id);
				$d['orderData']=$orderData;*/
$cond=array();
$orderData=$this->admin_fct->getReturnStockOrder($order_id,$cond);		
$data['order_details']=$orderData;
$return['message']="Products was added successfully to stock return.";	
$return['html'] =  $this->load->view('back_office/blocks/stock_line_items',$data,true);
$return['result'] = 1;
			die(json_encode($return));}else{
			die(json_encode($return));	}
	

}


/*public function view($id,$obj = ""){
if ($this->acl->has_permission('orders','index')){	
$data["title"]="View group products";
$data["content"]="back_office/orders/add";
$cond=array("id_orders"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}
*/
public function edit($id,$obj = ""){
if ($this->acl->has_permission('orders','edit')){	
$data["title"]="Edit group products";
$data["content"]="back_office/orders/add";
$cond=array("id_orders"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('group_products','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_orders",$id);
$this->db->update('orders',$_data);

$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/group_products"));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('orders','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_orders",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/orders/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_orders",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

/*public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit group products";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_orders",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/orders/".$this->session->userdata("back_link")));
	}
	
}
	
}*/

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/orders/".$image)){
unlink("./uploads/orders/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'group products')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/orders/".$value."/".$image)){
unlink("./uploads/orders/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_orders",$id);
$this->db->update("orders",$_data);
echo 'Done!';
}

}