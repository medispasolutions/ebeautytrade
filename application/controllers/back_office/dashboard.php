<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        //only for suppliers
        if ($this->acl->isCurrentUserASupplier()) {
            //dashboard when the user has not completed all profile data
            if ($this->acl->profileCompletionPercent() < 100) {
                $data = [
                    'mergeConfig' => [
                        'BIACONFIG' => [
                            'context' => [
                                'id' => \views\suppliersBackoffice\Contexts::DASHBOARD,
                                'ENABLE_UNLOAD_PREVENTION' => true,
                            ],
                        ],
                        'seo' => [
                            'PAGE_TITLE' => 'Dashboard',
                        ],
                        'user' => [
                            'type' => $this->acl->getCurrentUserType(),
                        ],
                    ],
                ];
 
                $parentUser = $this->acl->getCurrentUserId();
                $cond = array("id_user" => $parentUser);
                $companyData = $this->fct->getonerecord('user', $cond);
                $parentId = $companyData['id_parent'];
    print_r($parentId);
                if($parentId){
                    return $this->dashboard();
                    $this->load->twigView('dashboardPage/index', $data);
                } else {
                    $this->load->twigView('welcomePage/index', $data);
                }
                
            //full dashboard
            } else {
                return $this->dashboard();
            }
        } else {
            redirect(site_url("back_office"));
        }
    }

    protected function dashboard()
    {
        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::DASHBOARD,
                        'ENABLE_UNLOAD_PREVENTION' => false,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Dashboard',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
            ],
        ];

        $data['counters'] = [
            'buyers' => 0,
            'messages' => 0,
            'orders' => 0,
            'products' => 0,
            'specials' => 0,
            'clicks_on_brands' => 0,
            'clicks_on_products' => 0,
            'clicks_on_call' => 0,
        ];

        $data['lists'] = [
            'buyers' => '',
            'orders' => '',
            'products' => '',
            'specials' => '',
        ];
        
        $parentUser = $this->acl->getCurrentUserId();
        $conduser = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $conduser);
        $parentId = $companyData['id_parent'];

        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        
        //list user brands
        $this->db->where(['id_user' => $parentId]);
        $tmp = $this->db->get("brands")->result_array();
        $user_brands_ids = [];
        foreach ($tmp as $row) {
            $user_brands_ids[] = $row['id_brands'];
        }
        //just in case
        if (count($user_brands_ids) == 0) {
            $user_brands_ids[] = -1;
        }

        //products
        $filters = [];
        list($data['counters']['products'], $data['lists']['products']) = $this->searchForProducts($filters);

        //specials
        $filters = ['specials' => 'has_active_discount'];
        list($data['counters']['specials'], $data['lists']['specials']) = $this->searchForProducts($filters);

        //clicks
        $clicks = $this->serviceLocator->clicksService()->getCountedClicksForSupplier($parentId);
        $data['counters']['clicks_on_brands'] = $clicks['clicks_on_brands'];
        $data['counters']['clicks_on_products'] = $clicks['clicks_on_products'];
        $data['counters']['clicks_on_call'] = $clicks['clicks_on_call'];

        //messages
        $this->db->where_in('id_brands', $user_brands_ids);
        $data['counters']['messages'] = $this->db->get('contactform')->num_rows();

        //buyers
        $buyersService = $this->serviceLocator->buyersService();
        $data['counters']['buyers'] = $buyersService->getCountedBuyersForSupplier($parentId);
        $buyersList = $buyersService->find(
            [
                'id_supplier' => $parentId
            ],
            10,
            0,
            'created_date',
            'desc'
        );
        foreach ($buyersList as $buyer) {
            /** @var \spamiles\users\buyers\Buyer $buyer */
            $data['lists']['buyers'][] = $buyer->getUser()->toArray();
        }

        //orders
        $cond['id_supplier'] = $parentId;
        $data['counters']['orders'] = $this->ecommerce_model->getOrders($cond);
        $orders = $this->ecommerce_model->getOrders($cond, 10, 0);
        foreach ($orders as $order) {
            $order['status_readable'] = $this->serviceLocator->dictionary()->orders($order['status']);
            $data['lists']['orders'][] = $order;
        }
        
        $parentUser = $parentId;
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $cond);
        $data['type'] = $companyData['id_user_type'];

        $this->load->twigView('dashboardPage/index', $data);
    }

    protected function searchForProducts($filters)
    {
        $cond = array();
        
        $parentUser = $this->acl->getCurrentUserId();
        $conduser = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $conduser);
        $parentId = $companyData['id_parent'];

        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }

        $cond['id_user'] = $parentId;
        $cond['admin'] = true;

        if ($filters['specials']) {
            $cond['specials'] = $filters['specials'];
        }

        if (isset($filters['status'])) {
            $cond['status'] = $filters['status'];
        }

        $noOfElements = $this->ecommerce_model->getProducts($cond);
        $elements = $this->ecommerce_model->getProducts($cond, 10, 0, 'products.updated_date', 'desc');

        return [$noOfElements, $elements];
    }
}
