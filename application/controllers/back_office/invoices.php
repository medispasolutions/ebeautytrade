<?
class Invoices extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="invoices";
$this->load->model("invoices_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('invoices','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List invoices";
$data["content"]="back_office/invoices/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$count_news = $this->invoices_m->getAll($this->table,$order);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = site_url("back_office/invoices/index/".$order);
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['uri_segment'] = 5;
$this->pagination->initialize($config);
$data['info'] = $this->invoices_m->list_paginate($order,$config['per_page'],$this->uri->segment(5));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('invoices','add')){	
$data["title"]="Add invoices";
$data["content"]="back_office/invoices/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('invoices','index')){	
$data["title"]="View invoices";
$data["content"]="back_office/invoices/add";
$cond=array("id_invoices"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('invoices','edit')){	
$data["title"]="Edit invoices";
$data["content"]="back_office/invoices/add";
$cond=array("id_invoices"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('invoices','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_invoices",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/invoices/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('invoices','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_invoices",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/invoices/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_invoices",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit invoices";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("default_currency", "default_currency"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("status", "status"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("total_amount_credits", "total amount credits"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("total_customer_charge", "total_customer_charge"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("id_user", "id_user"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("currency", "currency"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("total_amount", "total amount"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("total_amount_currency", "total amount currency"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("shipping_charge", "shipping charge"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("net_price", "net price"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("discount", "discount"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("from_date", "from_date"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("to_date", "to_date"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("rand", "rand"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["default_currency"]=$this->input->post("default_currency");
$_data["status"]=$this->input->post("status");
$_data["total_amount_credits"]=$this->input->post("total_amount_credits");
$_data["total_customer_charge"]=$this->input->post("total_customer_charge");
$_data["id_user"]=$this->input->post("id_user");
$_data["currency"]=$this->input->post("currency");
$_data["total_amount"]=$this->input->post("total_amount");
$_data["total_amount_currency"]=$this->input->post("total_amount_currency");
$_data["shipping_charge"]=$this->input->post("shipping_charge");
$_data["net_price"]=$this->input->post("net_price");
$_data["discount"]=$this->input->post("discount");

$_data["from_date"]=$this->fct->date_in_formate($this->input->post("from_date"));

$_data["to_date"]=$this->fct->date_in_formate($this->input->post("to_date"));
$_data["rand"]=$this->input->post("rand");

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_invoices",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/invoices/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/invoices/".$image)){
unlink("./uploads/invoices/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'invoices')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/invoices/".$value."/".$image)){
unlink("./uploads/invoices/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_invoices",$id);
$this->db->update("invoices",$_data);
echo 'Done!';
}

}