<?
class Banner extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="banner";
$this->load->model("banner_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('banner','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List banner";
$data["content"]="back_office/banner/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$count_news = $this->banner_m->getAll($this->table,$order);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = site_url("back_office/banner/index/".$order);
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['uri_segment'] = 5;
$this->pagination->initialize($config);
$data['info'] = $this->banner_m->list_paginate($order,$config['per_page'],$this->uri->segment(5));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('banner','add')){	
$data["title"]="Add banner";
$data["content"]="back_office/banner/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('banner','index')){	
$data["title"]="View banner";
$data["content"]="back_office/banner/add";
$cond=array("id_banner"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('banner','edit')){	
$data["title"]="Edit banner";
$data["content"]="back_office/banner/add";
$cond=array("id_banner"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('banner','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_banner",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/banner/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('banner','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_banner",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/banner/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_banner",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit banner";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("id_brands", "Brands", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("background", "background"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("link", "link"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("featured_image", "featured image"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("featured_title", "featured title"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("featured_url", "featured url"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("banner_link", "banner link"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("slide_delay", "slide delay"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("version", "version"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("pages", "pages"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["id_brands"]=$this->input->post("id_brands");
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
if(!empty($_FILES["background"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_banner"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("banner","background",$cond_image);
if(!empty($old_image) && file_exists('./uploads/banner/'.$old_image)){
unlink("./uploads/banner/".$old_image);
 } }
$image1= $this->fct->uploadImage("background","banner");
$_data["background"]=$image1;	
}
$_data["link"]=$this->input->post("link");
if(!empty($_FILES["featured_image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_banner"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("banner","featured_image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/banner/'.$old_image)){
unlink("./uploads/banner/".$old_image);
 } }
$image1= $this->fct->uploadImage("featured_image","banner");
$_data["featured_image"]=$image1;	
}
$_data["featured_title"]=$this->input->post("featured_title");
$_data["featured_url"]=$this->input->post("featured_url");
$_data["banner_link"]=$this->input->post("banner_link");
$_data["slide_delay"]=$this->input->post("slide_delay");
$_data["version"]=$this->input->post("version");
$_data["pages"]=$this->input->post("pages");

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_banner",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/banner/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/banner/".$image)){
unlink("./uploads/banner/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'banner')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/banner/".$value."/".$image)){
unlink("./uploads/banner/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_banner",$id);
$this->db->update("banner",$_data);
echo 'Done!';
}

}