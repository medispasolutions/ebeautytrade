<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_import extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('supplierimport_model', 'import');
		$this->load->helper('form');
		$this->load->helper('url');
	}
	public function index() {
		$data['page'] = 'import';
		$data['title'] = 'Import XLSX | EBeautyTrade_Supplier';
        // $data['content'] = "back_office/supplier_import/index";
		
		$this->load->view('back_office/supplier_import/index', $data);
        // $this->load->view("back_office/template",$data);
	}
	
	
	// import excel data
	public function save() {
	    if (!$this->acl->has_permission('brands', 'add')) {
            redirect(site_url("back_office"));
        }
        $data['success'] = 1;
        $user=$this->ecommerce_model->getUserInfo();
	    $userid=$user['id_user'];
	
        if ($this->input->post('importfile')) {
            $path = '/home/ebeautyadmin/public_html/uploads/excel/supplier_excel/';

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }

            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
                $error = 0;
            } else {
                $import_xls_file = 0;
                $error = 1;
            }
            if($error == 0){
                $inputFileName = $path . $import_xls_file;
                try {
                    PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch (Exception $e) {
                    $data['success'] = 0;
                    $errors =  $e->getMessage();
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
                    $this->load->twigView("productsListingPage/live-on-site", $data);
                }
                
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
    
                $arrayCount = count($allDataInSheet);
                $flag = 0;
                $createArray = array('Title', 'Trade_Price',  'Set_as_PRO', 'YouTube_Link', 'Retail_Price','Weight','Set_as_Non-Exportable','Length','Width','Barcode', 'Description', 'SKU', 'Brief', 'Quantity','Image','Brands_ID');
                $makeArray = array('Title' => 'Title', 'Trade_Price' => 'Trade_Price','Set_as_PRO' => 'Set_as_PRO','YouTube_Link' => 'YouTube_Link', 'Retail_Price' => 'Retail_Price','Weight' => 'Weight','Set_as_Non-Exportable' => 'Set_as_Non-Exportable','Length' => 'Length', 'Width' => 'Width', 'Barcode' => 'Barcode', 'Description' => 'Description', 'SKU' => 'SKU', 'Brief' => 'Brief', 'Quantity' => 'Quantity', 'Image' => 'Image', 'Brands_ID' => 'Brands_ID');
                $SheetDataKey = array();
                foreach ($allDataInSheet as $dataInSheet) {
                    foreach ($dataInSheet as $key => $value) {
                        if (in_array(trim($value), $createArray)) {
                            $value = preg_replace('/\s+/', '', $value);
                            $SheetDataKey[trim($value)] = $key;
                            
                        } 
                    }
                }
                
                $data = array_diff_key($makeArray, $SheetDataKey);
                if (empty($data)) {
                    $flag = 1;
                }
    
                if ($flag == 1) {
                    for ($i = 2; $i <= $arrayCount; $i++) {
                        $addresses = array();
                        $Title = $SheetDataKey['Title'];
                        $List_Price = $SheetDataKey['Trade_Price'];
                        $Retail_Price = $SheetDataKey['Retail_Price'];
                        $Youtube_Link = $SheetDataKey['YouTube_Link'];
                        $Barcode = $SheetDataKey['Barcode'];
                        $Image = $SheetDataKey['Image'];
                        $Weight = $SheetDataKey['Weight'];
                        $Length = $SheetDataKey['Length'];
                        $Width = $SheetDataKey['Width'];
                        $sku = $SheetDataKey['SKU'];
                        $Description = $SheetDataKey['Description'];
                        $Brief = $SheetDataKey['Brief'];
                        $Set_As_Pro = $SheetDataKey['Set_as_PRO'];
                        $Set_As_Non_Exportable = $SheetDataKey['Set_as_Non-Exportable'];
                        $Quantity = $SheetDataKey['Quantity'];
                        $Brands_ID = $SheetDataKey['Brands_ID'];
                        
                        $Image = $allDataInSheet[$i][$Image];
                        $Title = filter_var(trim($allDataInSheet[$i][$Title]), FILTER_SANITIZE_STRING);
                        $List_Price = filter_var(trim($allDataInSheet[$i][$List_Price]), FILTER_SANITIZE_STRING);
                        $Youtube_Link = filter_var(trim($allDataInSheet[$i][$Youtube_Link]), FILTER_SANITIZE_STRING);
                        $Retail_Price = filter_var(trim($allDataInSheet[$i][$Retail_Price]), FILTER_SANITIZE_STRING);
                        $Barcode = filter_var(trim($allDataInSheet[$i][$Barcode]), FILTER_SANITIZE_STRING);
                        $Weight = filter_var(trim($allDataInSheet[$i][$Weight]), FILTER_SANITIZE_STRING);
                        $Length = filter_var(trim($allDataInSheet[$i][$Length]), FILTER_SANITIZE_STRING);
                        $Width = filter_var(trim($allDataInSheet[$i][$Width]), FILTER_SANITIZE_STRING);
                        $sku = filter_var(trim($allDataInSheet[$i][$sku]), FILTER_SANITIZE_STRING);
                        $Set_As_Pro = filter_var(trim($allDataInSheet[$i][$Set_As_Pro]), FILTER_SANITIZE_STRING);
                        $Set_As_Non_Exportable = filter_var(trim($allDataInSheet[$i][$Set_As_Non_Exportable]), FILTER_SANITIZE_STRING);
                        $Description = filter_var(trim($allDataInSheet[$i][$Description]), FILTER_SANITIZE_STRING);
                        $Brief = filter_var(trim($allDataInSheet[$i][$Brief]), FILTER_SANITIZE_STRING);
                        $Quantity = filter_var(trim($allDataInSheet[$i][$Quantity]), FILTER_SANITIZE_STRING);
                        $Brands_ID = filter_var(trim($allDataInSheet[$i][$Brands_ID]), FILTER_SANITIZE_STRING);
                        
                        if (!empty($sku)) {
                            $List_Price = (float)str_replace(",", '.', $List_Price);
                            if ((float)$List_Price <= 0) {
                                $data['success'] = 0;
                                $error = "List price cannot be <= 0";
                                break;
                            }
                            $filecount = 0;
                            $getSKUifExists = $this->import->skuisAlreadyExists($sku);
                            
                            if ($getSKUifExists > 0) {
                                $data['successduplicate'] = 0;
                                $errorsduplicatesku[] =  $sku;
                            }else{
                                if (!empty($Title) || !empty($List_Price) || !empty($Retail_Price) || !empty($sku) || !empty($Description) || !empty($Brief) || !empty($Quantity)) {
                                    $fetchData = array();
                                    $fetchData[] = array('title' => $Title, 'status' => 2 ,'id_user'=> $userid, 
                                        'list_price' => $List_Price, 'retail_price' => $Retail_Price, 'barcode' => $Barcode,
                                        'width' => $Width, 'length' => $Length, 'weight' => $Weight,'description' => $Description,
                                        'set_as_pro' => $Set_As_Pro, 'set_as_non_exportable' => $Set_As_Non_Exportable,
                                        'sku' => $sku, 'brief' => $Brief, 'youtube' => $Youtube_Link, 'quantity' => $Quantity,'id_brands' => $Brands_ID);
                                    $data['employeeInfo'] = $fetchData;
                                    $this->import->setBatchImport($fetchData);
                                    $this->import->importData();
                                    if(!empty($Image)){
                                        $nameimage = $Title.'-'.$sku.'.jpg';
                                        //Get the file
                                        $saveto='/home/ebeautyadmin/public_html/uploads/products/gallery/'.$nameimage;
                                        $imagefilee = $this->import->getimg($Image, $saveto);
                                        $saveto='/home/ebeautyadmin/public_html/uploads/products/gallery/120x120/'.$nameimage;
                                        $imagefilee = $this->import->getimg($Image, $saveto);
                                        $product_id = $this->import->getproductidfromsku($sku);
                                        $dataimage = array();
                                        $dataimage[] = array('image' => $nameimage, 'id_products' => $product_id);
                		                $this->db->insert_batch('products_gallery', $dataimage);
                                    }
                                }
                                $previousTITLE= $Title;
                                $previoussku = $sku;
                                $filecount++;
                            }
                        }elseif(!empty($previoussku) && !empty($Image)){
                            $nameimage = $previousTITLE.'-'.$previoussku.'-'.$filecount.'.jpg';
                            $saveto='/home/ebeautyadmin/public_html/uploads/products/gallery/'.$nameimage;
                            $imagefilee = $this->import->getimg($Image, $saveto);
                            $saveto='/home/ebeautyadmin/public_html/uploads/products/gallery/120x120/'.$nameimage;
                            $imagefilee = $this->import->getimg($Image, $saveto);
                            $product_id = $this->import->getproductidfromsku($previoussku);
                            $dataimage = array();
                            $dataimage[] = array('image' => $nameimage, 'id_products' => $product_id);
    		                $this->db->insert_batch('products_gallery', $dataimage);
                        }
                    }
                   
                    if ($data['successduplicate']==0) {
                        $errors = 'You have a duplicated SKU in your excel please remove all duplication, Import Failed for the following SKU failed:';
                        foreach($errorsduplicatesku as $errorsduplicateskus):
                            $errors .= $errorsduplicateskus.' / ';
                        endforeach;
                        $data['success'] = 0;
                    }
                } else {
                    $data['success'] = 0;
                    $errors =  'The imported excel is not correct, please upload the correct excel template. the followign fields do not match';
                    foreach($data as $dataval){$errors.= $dataval.' / ';}
                }
            }else {
                $data['success'] = 0;
                $errors =  'The imported excel is not correct, please upload the correct excel template.';
            }
        }
        if($data['success']==1){
            $this->session->set_userdata("success_message", "Product were imported succesfully");
        }else{
            $data['globalErrors'] = $errors;
            $this->session->set_userdata("success_message", $errors);
        }
        redirect(site_url("back_office/products/live_on_site"));
	}
}
