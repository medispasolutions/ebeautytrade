<?
class Banner_ads extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="banner_ads";
$this->load->model("banner_ads_m");
}


public function index($order=""){
if ($this->acl->has_permission('banner_ads','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List banner ads";
$data["content"]="back_office/banner_ads/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$count_news = $this->banner_ads_m->getAll($this->table,$order);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = site_url("back_office/banner_ads/index/".$order);
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['uri_segment'] = 5;
$this->pagination->initialize($config);
$data['info'] = $this->banner_ads_m->list_paginate($order,$config['per_page'],$this->uri->segment(5));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add($data = array()){
if ($this->acl->has_permission('banner_ads','add')){	
$data["title"]="Add banner ads";
$data["content"]="back_office/banner_ads/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$data = array()){
if ($this->acl->has_permission('banner_ads','index')){	
$data["title"]="View banner ads";
$data["content"]="back_office/banner_ads/add";
$cond=array("id_banner_ads"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$data = array()){
if ($this->acl->has_permission('banner_ads','edit')){	
$data["title"]="Edit banner ads";
$data["content"]="back_office/banner_ads/add";
$cond=array("id_banner_ads"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('banner_ads','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_banner_ads",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(base_url()."back_office/banner_ads/".$this->session->userdata("back_link"));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('banner_ads','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_banner_ads",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(base_url()."back_office/banner_ads/".$this->session->userdata("back_link"));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i);
$this->db->where("id_banner_ads",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$data["title"]="Add / Edit banner ads";
$this->form_validation->set_rules("title", "TITLE", "trim|required");
$this->form_validation->set_rules("target", "target", "trim");
$this->form_validation->set_rules("link", "link", "trim");
$this->form_validation->set_rules("meta_title", "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url", "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description", "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords", "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("user", "user", "trim");
$this->form_validation->set_rules("image", "image", "trim");
$this->form_validation->set_rules("expiry_date", "expiry date", "trim|required");
//$this->form_validation->set_rules("pages", "pages", "trim");
//$this->form_validation->set_rules("positions", "positions", "trim");
//$this->form_validation->set_rules("categories", "categories", "trim");
if ($this->form_validation->run() == FALSE){
	$data = array();
	if($this->input->post("pages") != "")
$data['selected_pages'] = $this->input->post("pages");
if($this->input->post("blocks") != "")
$data['selected_blocks'] = $this->input->post("blocks");

if($this->input->post("categories") != "")
$data['selected_categories'] = $this->input->post("categories");

if($this->input->post("id")!="")
$this->edit($this->input->post("id"),$data);
else
$this->add($data);
}
else
{
$_data["title"]=$this->input->post("title");
$_data["target"]=$this->input->post("target");
$_data["link"]=$this->input->post("link");
$_data["meta_title"]=$this->input->post("meta_title");
if($this->input->post("title_url") == "")
$title_url = $this->input->post("title");
else
$title_url = $this->input->post("title_url");
$_data["title_url"]=$this->fct->cleanURL("banner_ads",url_title($title_url),$this->input->post("id"));
$_data["meta_description"]=$this->input->post("meta_description");
$_data["meta_keywords"]=$this->input->post("meta_keywords");	
$_data["id_user"]=$this->input->post("user"); 
if(!empty($_FILES["image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_banner_ads"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("banner_ads","image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/banner_ads/'.$old_image)){
unlink("./uploads/banner_ads/".$old_image);
 } }
$image1= $this->fct->uploadImage("image","banner_ads");
$_data["image"]=$image1;	
}
$_data["video"]=$this->input->post("video");
$_data["type"]=$this->input->post("type");
$_data["expiry_date"]=$this->fct->date_in_formate($this->input->post("expiry_date"));

if($this->input->post("pages") != ""){
$_data['pages'] = implode("|",$this->input->post("pages"));}else{
$_data['pages'] = "";	}
if($this->input->post("blocks") != ""){
$_data['positions'] = implode("|",$this->input->post("blocks"));}else{
$_data['positions'] = "";	}

if($this->input->post("categories") != ""){
$_data['categories'] = implode("|",$this->input->post("categories"));}else{
$_data['categories'] = "";	}

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_banner_ads",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	redirect(base_url()."back_office/banner_ads/".$this->session->userdata("back_link"));
}
	
}

public function delete_image($field,$image,$id){
if(file_exists("./uploads/banner_ads/".$image)){
unlink("./uploads/banner_ads/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'banner ads')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/banner_ads/".$value."/".$image)){
unlink("./uploads/banner_ads/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_banner_ads",$id);
$this->db->update("banner_ads",$_data);
redirect(base_url()."back_office/banner_ads");	
}

}