<?
class Users_credits extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="users_credits";
$this->load->model("users_credits_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('users_credits','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List users Petty Cash";
$data["content"]="back_office/users_credits/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$count_news = $this->users_credits_m->getAll($this->table,$order);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = site_url("back_office/users_credits/index/".$order);
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['uri_segment'] = 5;
$this->pagination->initialize($config);
$data['info'] = $this->users_credits_m->list_paginate($order,$config['per_page'],$this->uri->segment(5));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('users_credits','add')){	
$data["title"]="Add users credits";
$data["content"]="back_office/users_credits/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('users_credits','index')){	
$data["title"]="View users credits";
$data["content"]="back_office/users_credits/add";
$cond=array("id_users_credits"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('users_credits','edit')){	
$data["title"]="Edit users credits";
$data["content"]="back_office/users_credits/add";
$cond=array("id_users_credits"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('users_credits','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_users_credits",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/users_credits/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('users_credits','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_users_credits",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/users_credits/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_users_credits",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit users credits";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("id_user", "id_user"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("fax", "fax"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("payment_date", "payment_date"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("gateway_response_id", "gateway_response_id"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("default_currency", "default currency"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("currency", "currency"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("amount_default_currency", "amount_default_currency"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("id_countries", "id_countries"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("postal_code", "postal code"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("telephone", "telephone"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("status", "status"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("first_name", "first name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("last_name", "last name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("amount", "amount"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("email", "email"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("street_one", "street one"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("street_two", "street two"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("city", "city"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("state", "state"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["id_user"]=$this->input->post("id_user");
$_data["fax"]=$this->input->post("fax");

$_data["payment_date"]=$this->fct->date_in_formate($this->input->post("payment_date"));
$_data["gateway_response_id"]=$this->input->post("gateway_response_id");
$_data["default_currency"]=$this->input->post("default_currency");
$_data["currency"]=$this->input->post("currency");
$_data["amount_default_currency"]=$this->input->post("amount_default_currency");
$_data["id_countries"]=$this->input->post("id_countries");
$_data["postal_code"]=$this->input->post("postal_code");
$_data["telephone"]=$this->input->post("telephone");
$_data["status"]=$this->input->post("status");
$_data["first_name"]=$this->input->post("first_name");
$_data["last_name"]=$this->input->post("last_name");
$_data["amount"]=$this->input->post("amount");
$_data["email"]=$this->input->post("email");
$_data["street_one"]=$this->input->post("street_one");
$_data["street_two"]=$this->input->post("street_two");
$_data["city"]=$this->input->post("city");
$_data["state"]=$this->input->post("state");

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_users_credits",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/users_credits/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/users_credits/".$image)){
unlink("./uploads/users_credits/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'users credits')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/users_credits/".$value."/".$image)){
unlink("./uploads/users_credits/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_users_credits",$id);
$this->db->update("users_credits",$_data);
echo 'Done!';
}

}