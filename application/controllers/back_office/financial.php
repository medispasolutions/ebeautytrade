<?php

use \spamiles\orders\OrderInterface;

class Financial extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "orders";
        $this->load->model("orders_m");
        $this->lang->load('statics');
    }

    //==================================================================================================================
    // helpers

    /**
     * Checks whether the order with supplied id belongs to the current user
     * @param $order_id
     * @param string $permission_type
     * @return bool
     */
    protected function checkOwner($order_id, $permission_type = 'edit')
    {
        //check permission
        if (!$this->acl->has_permission('orders', $permission_type)) {
            return false;
        }
        
        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }

        //check owner
        $cond = array("id_orders" => $order_id);
        $data = $this->fct->getonerecord($this->table, $cond);
        if ($data['id_supplier'] != $parentId) {
            return false;
        }

        return true;
    }

    protected function export($orders)
    {
        $this->export_fct->salesHistoryExportToExcel($orders);
    }

    /**
     * This functions prepares filters
     * @return array Current filters settings
     */
    protected function getFilters()
    {
        $searchData = [
            'id' => '',
            'buyer_name' => '',
            'buyer_city' => '',
            'buyer_country' => '',
            'status' => '',
            'payment_status' => '',
            'date_from' => '',
            'date_to' => '',
        ];

        $currentSessionData = $this->session->userdata('orders-filters');
        if (!$currentSessionData) {
            $currentSessionData = [];
        }
        $currentPostedFilters = $this->input->post('Search');
        if (!$currentPostedFilters) {
            $currentPostedFilters = [];
        }
        if ($this->input->get('resetFilters') == 1) {
            $currentPostedFilters = $searchData;
        }

        foreach ($searchData as $key => $_) {
            if (isset($currentPostedFilters[$key])) {
                $searchData[$key] = $currentPostedFilters[$key];
            } else {
                $searchData[$key] = $currentSessionData[$key];
            }
        }

        $this->session->set_userdata('orders-filters', $searchData);

        //prepare possible filters
        $possibleFilters = [];
        //countries
        $countries = $this->fct->getAll('countries', 'title');
        foreach ($countries as $country) {
            $possibleFilters['buyer_country'][] = array(
                'TEXT' => $country['title'],
                'VALUE' => $country['id_countries'],
                'IS_SELECTED' => $searchData['buyer_country'] == $country['id_countries'] ? true : false
            );
        }
        //statuses
        $possibleFilters['status'] = [];
        $tmp = [
            OrderInterface::STATUS_UNREAD,
            OrderInterface::STATUS_PENDING,
            OrderInterface::STATUS_PREPARED,
            OrderInterface::STATUS_COMPLETED,
            OrderInterface::STATUS_CANCELED,
        ];
        foreach ($tmp as $i) {
            $possibleFilters['status'][] = array(
                'TEXT' => $this->serviceLocator->dictionary()->orders($i),
                'VALUE' => $i,
                'IS_SELECTED' => $searchData['status'] == $i ? true : false
            );
        }
        //payment statuses
        //statuses
        $possibleFilters['payment_status'] = [];
        $tmp = [
            OrderInterface::PAYMENT_STATUS_UNPAID,
            OrderInterface::PAYMENT_STATUS_PAID,
        ];
        foreach ($tmp as $i) {
            $possibleFilters['payment_status'][] = array(
                'TEXT' => $this->serviceLocator->dictionary()->orders($i),
                'VALUE' => $i,
                'IS_SELECTED' => $searchData['payment_status'] == $i ? true : false
            );
        }

        return [
            'current' => $searchData,
            'possible' => $possibleFilters
        ];
    }

    /**
     * This function searches for relevant orders
     * @param $filters
     * @return array[int|array]
     */
    protected function search($filters)
    {
        $cond = array();

        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        
        $cond['id_supplier'] = $parentId;
        $cond['payment_method'] = 'credit_cards';

        if ($filters['id']) {
            $cond['orders.id_orders'] = $filters['id'];
        }

        if ($filters['buyer_name']) {
            $cond['user.name'] = $filters['buyer_name'];
        }

        if ($filters['buyer_country']) {
            $cond['user.id_countries'] = $filters['buyer_country'];
        }

        if ($filters['buyer_city']) {
            $cond['user.city'] = $filters['buyer_city'];
        }

        if ($filters['status']) {
            $cond['orders.status'] = $filters['status'];
        }

        if ($filters['payment_status']) {
            $cond['orders.payment_status'] = $filters['payment_status'];
        }

        if ($filters['date_from']) {
            //rework date
            $dt = DateTime::createFromFormat("d/m/Y", $filters['date_from']);
            $cond['orders.created_date >='] = $dt->format("Y-m-d")." 00:00:00";
        }

        if ($filters['date_to']) {
            //rework date
            $dt = DateTime::createFromFormat("d/m/Y", $filters['date_to']);
            $cond['orders.created_date <='] = $dt->format("Y-m-d")." 00:00:00";
        }

        $noOfElements = $this->ecommerce_model->getOrders($cond);
        $elements = $this->ecommerce_model->getOrders($cond, 1000, 0);

        return [$noOfElements, $elements];
    }

    protected function search2($filters)
    {
        $cond = array();

        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        
        $cond['id_supplier'] = $parentId;
        $cond['payment_status'] = 'paid';

        if ($filters['id']) {
            $cond['orders.id_orders'] = $filters['id'];
        }

        if ($filters['buyer_name']) {
            $cond['user.name'] = $filters['buyer_name'];
        }

        if ($filters['buyer_country']) {
            $cond['user.id_countries'] = $filters['buyer_country'];
        }

        if ($filters['buyer_city']) {
            $cond['user.city'] = $filters['buyer_city'];
        }

        if ($filters['status']) {
            $cond['orders.status'] = $filters['status'];
        }

        if ($filters['payment_status']) {
            $cond['orders.payment_status'] = $filters['payment_status'];
        }

        if ($filters['date_from']) {
            //rework date
            $dt = DateTime::createFromFormat("d/m/Y", $filters['date_from']);
            $cond['orders.created_date >='] = $dt->format("Y-m-d")." 00:00:00";
        }

        if ($filters['date_to']) {
            //rework date
            $dt = DateTime::createFromFormat("d/m/Y", $filters['date_to']);
            $cond['orders.created_date <='] = $dt->format("Y-m-d")." 00:00:00";
        }

        $noOfElements = $this->ecommerce_model->getpaymentNotOnline($cond);
        $elements = $this->ecommerce_model->getpaymentNotOnline($cond, 1000, 0);

        return [$noOfElements, $elements];
    }
    
    //==================================================================================================================
    // views

    public function listing()
    {
        if (!$this->acl->has_permission('orders', 'index')) {
            redirect(site_url("back_office"));
        }
        
        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        $paymentAmount = $this->fct->get_ordersAmount($parentId);

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::FINANCIAL_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Financial',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
                'Payment' => [
                    'Amount' => number_format($paymentAmount['amount_currency']-($paymentAmount['amount_currency']*0.05+5), 2),
                    'Commission' => number_format($paymentAmount['amount_currency']*0.05+5, 2),
                ]
            ],
        ];

        $filters = $this->getFilters();

        $filtersForSearch = $filters['current'];

        list($noOfElements, $orders) = $this->search($filtersForSearch);
        if ($this->input->post('filtersRequestType') == "export") {
            return $this->export($orders);
        }

        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/products/live_on_site');

        $orders_array = array_splice(
            $orders,
            ($this->paginator->page-1)*$this->paginator->per_page,
            $this->paginator->per_page
        );

        $finalData = [];
        foreach ($orders_array as $order) {
            $finalData[] = [
                'rowId' => $order['id_orders'],
                'columns' => [
                    $order['created_date'],
                    $order['trading_name'],
                    $order['id_orders'],
                    $order['amount_currency'].' '.$order['currency'],
                    $order['amount_currency']*0.05.' '.$order['currency']
                ]
            ];
        }

        $data['filters'] = $filters;
        $data['info'] = $finalData;

        $this->load->twigView("financialAvailableListingPage/index", $data);
    }

    public function order()
    {
        if (!$this->acl->has_permission('orders', 'index')) {
            redirect(site_url("back_office"));
        }
        
        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        $paymentAmount = $this->fct->get_ordersAmount($parentId);

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::FINANCIAL_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Financial',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
                'Payment' => [
                    'Amount' => number_format($paymentAmount['amount_currency']-($paymentAmount['amount_currency']*0.05+5), 2),
                    'Commission' => number_format($paymentAmount['amount_currency']*0.05+5, 2),
                ]
            ],
        ];

        $filters = $this->getFilters();

        $filtersForSearch = $filters['current'];

        list($noOfElements, $orders) = $this->search2($filtersForSearch);
        if ($this->input->post('filtersRequestType') == "export") {
            return $this->export($orders);
        }

        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/products/live_on_site');

        $orders_array = array_splice(
            $orders,
            ($this->paginator->page-1)*$this->paginator->per_page,
            $this->paginator->per_page
        );

        $finalData = [];
        foreach ($orders_array as $order) {
            $finalData[] = [
                'rowId' => $order['id_orders'],
                'columns' => [
                    $order['created_date'],
                    $order['trading_name'],
                    $order['id_orders'],
                    $order['amount_currency'].' '.$order['currency'],
                    $order['amount_currency']*0.05.' '.$order['currency']
                ]
            ];
        }

        $data['filters'] = $filters;
        $data['info'] = $finalData;

        $this->load->twigView("financialAvailableListingPage/index", $data);
    }


    public function request()
    {
        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        $paymentAmount = $this->fct->get_ordersAmount($parentId);

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::FINANCIAL_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Request Withdrawal',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ]
            ],
        ];
        
        $data["amount"] = number_format($paymentAmount['amount_currency']-($paymentAmount['amount_currency']*0.05+5), 2);

        $this->load->model('user_m');
        $test = array();
        $test = $this->user_m->get_user($parentId);
        print_r($test);
        echo $test['name'];
        echo $test['email'];
        
        
        $this->load->twigView("requestWithdrawal/index", $data);
    }
    public function send()
    {
        
        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        $paymentAmount = $this->fct->get_ordersAmount($parentId);

        $_data['name'] = "";
        $_data['email'] = "";
        $_data['created_date'] = date('Y-m-d h:i:s');
        $_data['amount'] = number_format($paymentAmount['amount_currency']*0.05+5, 2);
        
        $this->db->insert('financial_user_details', $_data);
        $new_id = $this->db->insert_id();
        
        $this->load->model('send_emails');
        /*$this->send_emails->sendRequestToAdmin($_data);*/
            
        $this->load->twigView("requestWithdrawal/index", $data);
    }

}