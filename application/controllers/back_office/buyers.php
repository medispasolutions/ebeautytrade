<?php

class Buyers extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
    }

    //==================================================================================================================
    // helpers

    protected function checkOwner($user_id)
    {
        $buyersService = $this->serviceLocator->buyersService();

        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        
        $buyer = $buyersService->findFirst([
            "id_user" => $user_id,
            "id_supplier" => $parentId,
        ]);

        return $buyer->exists();
    }

    protected function export($buyers)
    {
        $this->export_fct->buyersExportToExcel($buyers);
    }

    /**
     * This functions prepares filters
     * @return array Current filters settings
     */
    protected function getFilters()
    {
        $searchData = [
            'trading_name' => '',
            'name' => '',
            'email' => '',
            'id_countries' => '',
            'city' => '',
        ];

        $currentSessionData = $this->session->userdata('buyers-filters');
        if (!$currentSessionData) {
            $currentSessionData = [];
        }
        $currentPostedFilters = $this->input->post('Search');
        if (!$currentPostedFilters) {
            $currentPostedFilters = [];
        }
        if ($this->input->get('resetFilters') == 1) {
            $currentPostedFilters = $searchData;
        }

        foreach ($searchData as $key => $_) {
            if (isset($currentPostedFilters[$key])) {
                $searchData[$key] = $currentPostedFilters[$key];
            } else {
                $searchData[$key] = $currentSessionData[$key];
            }
        }

        $this->session->set_userdata('buyers-filters', $searchData);

        //prepare possible filters
        $possibleFilters = [];
        //countries
        $countries = $this->fct->getAll('countries', 'title');
        foreach ($countries as $country) {
            $possibleFilters['countries'][] = array(
                'TEXT' => $country['title'],
                'VALUE' => $country['id_countries'],
                'IS_SELECTED' => $searchData['id_countries'] == $country['id_countries'] ? true : false
            );
        }

        return [
            'current' => $searchData,
            'possible' => $possibleFilters
        ];
    }

    /**
     * This function searches for relevant products
     * @param $filters
     */
    protected function search($filters)
    {
        $cond = array();

        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        
        $cond['id_supplier'] = $parentId;

        if ($filters['trading_name']) {
            $cond['trading_name'] = $filters['trading_name'];
        }

        if ($filters['name']) {
            $cond['name'] = $filters['name'];
        }

        if ($filters['email']) {
            $cond['email'] = $filters['email'];
        }

        if ($filters['id_countries']) {
            $cond['id_countries'] = $filters['id_countries'];
        }

        if ($filters['city']) {
            $cond['city'] = $filters['city'];
        }

        $noOfElements = $this->fct->getBuyers($cond);
        $elements = $this->fct->getBuyers($cond, 1000, 0);

        return [$noOfElements, $elements];
    }

    //==================================================================================================================
    // suppliers functions

    public function listing()
    {
        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::BUYERS_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Buyers',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
            ],
        ];

        $filters = $this->getFilters();

        $filtersForSearch = $filters['current'];
        list($noOfElements, $users) = $this->search($filtersForSearch);
        if ($this->input->post('filtersRequestType') == "export") {
            return $this->export($users);
        }

        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/buyers/listing');

        $users = array_splice(
            $users,
            ($this->paginator->page-1)*$this->paginator->per_page,
            $this->paginator->per_page
        );
        $data['filters'] = $filters;

        $buyers = [];
        foreach ($users as $user) {
            $buyers[] = [
                'rowId' => $user['id_user'],
                'columns' => [
                    $user['trading_name'],
                    $user['name'],
                    $user['email'],
                    $this->fct->getCountryNameById($user['id_countries']),
                    $user['city'],
                    $user['entry_created']
                ]
            ];
        }

        $data['info'] = $buyers;
        $this->load->twigView("buyersListingPage/index", $data);
    }

    public function view($user_id)
    {
        if (!$this->checkOwner($user_id)) {
            echo "You can't view this message as it's not yours";
        }

        $buyersService = $this->serviceLocator->buyersService();

        $parentUser = $this->acl->getCurrentUserId();
        $condition = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord('user', $condition);
        $parentId = $companyData['id_parent'];

        //set conditions
        if($parentId){
            $parentId = $parentId;
        } else {
            $parentId = $this->acl->getCurrentUserId();
        }
        
        $buyer = $buyersService->findFirst([
            "id_user" => $user_id,
            "id_supplier" => $parentId,
        ]);

        $userData = $buyer->getUser()->toArray();

        $data = [
            'entry' => $buyer->toArray(),
            'user' => $userData,
            'user_country' => $this->fct->getCountryNameById($userData['id_countries'])
        ];

        $this->load->twigView('modalContents/buyer-view', $data);
    }

}
