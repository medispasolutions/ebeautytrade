<?
class Banner_items extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="banner_items";
$this->load->model("banner_items_m");
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('banner_items','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List banner items";
$data["content"]="back_office/banner_items/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :

$cond = array();
$url = site_url('back_office/banner_items?pagination=on');
if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {
	$cond['id_banner'] = $_GET['id_banner'];
	$url .= '&id_banner='.$_GET['id_banner'];
}
$count_news = $this->banner_items_m->getItems($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->banner_items_m->getItems($cond,$config['per_page'],$page);
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}



public function add(){
if ($this->acl->has_permission('banner_items','add')){	
$data["title"]="Add banner items";
$data["content"]="back_office/banner_items/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id){
if ($this->acl->has_permission('banner_items','index')){	
$data["title"]="View banner items";
$data["content"]="back_office/banner_items/add";
$cond=array("id_banner_items"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id){
if ($this->acl->has_permission('banner_items','edit')){	
$data["title"]="Edit banner items";
$data["content"]="back_office/banner_items/add";
$cond=array("id_banner_items"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('banner_items','delete')){
	$cond = array('id_banner_items'=>$id);
	$item = $this->fct->getonerow('banner_items',$cond);
	if(!empty($item['image'])) {
		if(file_exists('./uploads/banner_items/'.$item['image'])) {
			unlink('./uploads/banner_items/'.$item['image']);
		}
	}
$this->db->delete($this->table,$cond);
$this->session->set_userdata("success_message","Information was deleted successfully");
if(isset($_GET['id_banner']) && !empty($_GET['id_banner']))
redirect(site_url("back_office/banner_items")."?id_banner=".$_GET['id_banner']);
else
redirect(site_url("back_office/banner_items/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('banner_items','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
	$cond = array('id_banner_items'=>$cehcklist[$i]);
	$item = $this->fct->getonerow('banner_items',$cond);
	if(!empty($item['image'])) {
		if(file_exists('./uploads/banner_items/'.$item['image'])) {
			unlink('./uploads/banner_items/'.$item['image']);
		}
	}
/*$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_banner_items",$cehcklist[$i]);	*/
$this->db->delete($this->table,$cond);
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
if(isset($_GET['id_banner']) && !empty($_GET['id_banner']))
redirect(site_url("back_office/banner_items")."?id_banner=".$_GET['id_banner']);
else
redirect(site_url("back_office/banner_items/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i);
$this->db->where("id_banner_items",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit banner items";
$this->form_validation->set_rules("title", "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title", "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url", "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description", "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords", "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("banner", "banner", "trim|required");
$this->form_validation->set_rules("type", "type", "trim|required");
$this->form_validation->set_rules("link", "link", "trim");
$this->form_validation->set_rules("image", "image", "trim");
$this->form_validation->set_rules("text", "text", "trim");
//$this->form_validation->set_rules("color", "color", "trim");
//$this->form_validation->set_rules("font_size", "font size", "trim");
//$this->form_validation->set_rules("font_weight", "font weight", "trim");
$this->form_validation->set_rules("top", "top", "trim|required");
$this->form_validation->set_rules("left", "left", "trim|required");
$this->form_validation->set_rules("zindex", "zindex", "trim|required");
//$this->form_validation->set_rules("slide_in_direction", "slide in direction", "trim|required");
//$this->form_validation->set_rules("slide_out_direction", "slide out direction", "trim|required");
//$this->form_validation->set_rules("easingin", "easingin", "trim|required");
//$this->form_validation->set_rules("easingout", "easingout", "trim|required");
//$this->form_validation->set_rules("duration_in", "duration in", "trim|required");
//$this->form_validation->set_rules("duration_out", "duration out", "trim|required");
//$this->form_validation->set_rules("delay_in", "delay in", "trim|required");
//$this->form_validation->set_rules("rotate", "rotate", "trim");
if ($this->form_validation->run() == FALSE){

if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{
$_data["title"]=$this->input->post("title");
$_data["meta_title"]=$this->input->post("meta_title");
if($this->input->post("title_url") == "")
$title_url = $this->input->post("title");
else
$title_url = $this->input->post("title_url");
$_data["title_url"]=$this->fct->cleanURL("banner_items",url_title($title_url),$this->input->post("id"));
$_data["meta_description"]=$this->input->post("meta_description");
$_data["meta_keywords"]=$this->input->post("meta_keywords");	
$_data["id_banner"]=$this->input->post("banner"); 
$_data["type"]=$this->input->post("type");
$_data["link"]=$this->input->post("link");
if($_data["type"] == 'image') {

if(!empty($_FILES["image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_banner_items"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("banner_items","image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/banner_items/'.$old_image)){
unlink("./uploads/banner_items/".$old_image);
 } }
$image1= $this->fct->uploadImage("image","banner_items");
$_data["image"]=$image1;	
}
}
if($_data["type"] == 'text') {
$_data["text"]=$this->input->post("text");
/*$_data["color"]=$this->input->post("color");
$_data["font_size"]=$this->input->post("font_size");
$_data["font_weight"]=$this->input->post("font_weight");*/
}
$_data["top"]=$this->input->post("top");
$_data["left"]=$this->input->post("left");
$_data["zindex"]=$this->input->post("zindex");
/*$_data["slide_in_direction"]=$this->input->post("slide_in_direction");
$_data["slide_out_direction"]=$this->input->post("slide_out_direction");
$_data["easingin"]=$this->input->post("easingin");
$_data["easingout"]=$this->input->post("easingout");
$_data["duration_in"]=$this->input->post("duration_in");
$_data["duration_out"]=$this->input->post("duration_out");
$_data["delay_in"]=$this->input->post("delay_in");
$_data["rotate"]=$this->input->post("rotate");*/

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_banner_items",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if(isset($_GET['id_banner']) && !empty($_GET['id_banner'])) {
		redirect(site_url("back_office/banner_items")."?id_banner=".$_GET['id_banner']);
	}
	else {
		redirect(site_url("back_office/banner_items/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_image($field,$image,$id){
if(file_exists("./uploads/banner_items/".$image)){
unlink("./uploads/banner_items/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'banner items')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if(isset($res["thumb"]) && $res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/banner_items/".$value."/".$image)){
unlink("./uploads/banner_items/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_banner_items",$id);
$this->db->update("banner_items",$_data);
redirect(site_url("back_office/banner_items"));	
}

}