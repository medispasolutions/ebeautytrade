<?
class Supplier extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->table = "user";
        $this->load->model("adminsupplier_m");
        $this->lang->load("admin");
    }

    /**
     * This is a function to list categories for the supplier
    */
    public function listing()
    {
        if (!$this->acl->has_permission('brands', 'index')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::SUPPLIER_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'List User',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
            ],
        ];

        //set conditions
        if (!$user_id) {
            $cond['id_user'] = $this->acl->getCurrentUserId();
        }

        $query = $this->db->query("SELECT id_user, first_name, last_name, email FROM user WHERE deleted = 0 AND id_parent = " . $cond['id_user']);

        foreach ($query->result_array() as $info) {
            $data['supplier'][] = [
                'rowId' => $info['id_user'],
                'columns' => [
                    $info['first_name'],
                    $info['last_name'],
                    $info['email']
                ]
            ];
        }
        $this->load->twigView("suppliersListingPage/index", $data);
    }

    public function index($order = "")
    {
        $this->session->unset_userdata("admin_redirect_url");
        if ($this->acl->isCurrentUserASupplier()) {
            redirect(site_url("back_office/supplier/listing"));
        }
        if ($this->acl->has_permission('supplier', 'index')) {

            if ($order == "")
                $order = "sort_order";
            $data["title"] = "List Promo Code";
            $data["content"] = "back_office/supplier/list";

            $this->session->unset_userdata("back_link");

            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "25";
            }
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
            $data["show_items"] = $show_items;
            // pagination  start :

            $cond = array();
            $like = array();
            $url = site_url("back_office/supplier") . "?pagination=on";
            $id_category = 0;
            $data['current_category'] = array();
            $data['parent_levels'] = array();
            if ($this->input->get('section') != "") {
                $url .= "&category=" . $id_category;
                $data['section'] = $this->input->get('section');
                $cond['c.section'] = $this->input->get('section');
            } else {
                $data['section'] = 'b2b';
            }
            if ($this->input->get("category") != "") {
                $id_category = $this->input->get("category");
                $url .= "&category=" . $id_category;
                $data['parent_levels'] = $this->custom_fct->getCategoriesTreeParentLevels($id_category);
                $data['parent_levels'] = arrangeParentLevels($data['parent_levels']);
                $data['current_category'] = $this->fct->getonerow("promocode", array("id_promo_code" => $id_promocode));
                //print '<pre>'; print_r($data['parent_levels']); exit;
            }
            $count_news = $this->custom_fct->getCategoriesPages($id_category, $cond, $like);
            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $this->load->library('pagination');
            $config['base_url'] = $url;
            $config['total_rows'] = $count_news;
            $config['per_page'] = $this->config->item("admin_per_page");
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            //print '<pre>';print_r($config);exit;
            $this->pagination->initialize($config);
            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            } else $page = 0;
            $data['info'] = $this->custom_fct->getCategoriesPages($id_category, $cond, $like, $config['per_page'], $page);

            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata("admin_redirect_url", $url);

            //print '<pre>';print_r($data['info']);exit;
            // end pagination .

            $this->load->view("back_office/template", $data);

        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function add($data = array())
    {
        if ($this->acl->has_permission('supplier', 'add')) {
            $data["title"] = "Add supplier";
            $data["content"] = "back_office/supplier/add";

            if ($this->input->get('section') == "") {
                $data['section'] = 'b2b';
            }

            if ($this->input->get('section') != "") {
                $data['section'] = $this->input->get('section');
            }

            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function view($id, $obj = "")
    {
        if ($this->acl->has_permission('supplier', 'index')) {
            $data["title"] = "View supplier";
            $data["content"] = "back_office/supplier/add";
            $cond = array("id_promo_code" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function edit($id, $obj = "")
    {
        $data = $obj;
        if ($this->acl->has_permission('suppliers', 'edit')) {
            $data["title"] = "Edit suppliers";
            $data["content"] = "back_office/suppliers/add";
            $cond = array("id_user" => $id);
            $data["id"] = $id;


            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $data["section"] = $data["info"]['section'];
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("back_office/dashboard"));
        }
    }

    public function delete($id)
    {
        $_data = array("deleted" => 1,
            "deleted_date" => date("Y-m-d h:i:s"));
        $this->db->where("id_user", $id);
        $this->db->update($this->table, $_data);
        $this->session->set_userdata("success_message", "Information was deleted successfully");
        redirect(site_url("back_office/supplier/listing"));
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i + 1);
            $this->db->where("id_promo_code", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }

        if ($this->input->post('id') != '') {
            //////////////Personal Information///////////////////////////
            if ($this->input->post('password') != '') {
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[20]');
                $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[8]|max_length[20]|matches[password]');
            }
        } else {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[8]|max_length[20]|matches[password]');
        }

        $this->form_validation->set_rules('id_countries', lang('country'), 'trim|required|xss_clean');

        $this->form_validation->set_rules('phone', lang('phone'), 'trim|required|xss_clean');

        $this->form_validation->set_rules('first_name', lang('first_name'), 'trim|required|xss_clean');
        $this->form_validation->set_rules('last_name', lang('last_name'), 'trim|required|xss_clean');

        $this->form_validation->set_rules('position', lang('position'), 'trim|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|callback_validateemailexistance[]');

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
            exit;
            $data["title"] = "Add / Edit New User";
            $data["content"] = "back_office/add_users";
            $data["pre_registration"] = $this->input->post("pre_registration");

            if ($this->input->post('id') != '') {
                $cond = array('id_user' => $this->input->post('id'));
                $data["id"] = $this->input->post('id');
                $data["info"] = $this->fct->getonerecord('user', $cond);
            }
            $data["roles"] = $this->fct->getAll('roles', 'sort_order');
            $data["discount_groups"] = $this->fct->getAll('discount_groups', 'sort_order');

            $this->load->view('back_office/template', $data);
        } else {
            $_data = array(
                'id_discount_groups' => $this->input->post('discount_group'),
                'id_roles' => $this->input->post('roles'));
            

            $_data['username'] = $this->input->post("username");
            $_data['license_number'] = $this->input->post("license_number");
            if ($this->input->post('roles') == 4 || $this->input->post('roles') == 5) {
                $_data['trading_name'] = $this->input->post("trading_name");
            }
            $_data['completed'] = $this->input->post("completed");
            $_data['salutation'] = $this->input->post("salutation");
            
            $_data['id_countries'] = $this->input->post("id_countries");
            $_data['id_brands'] = $this->input->post("id_brands");
            $token = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
            $_data['token'] = $token;
            $_data["pre_registration"] = $this->input->post("pre_registration");

            /*$_data['website_name'] = website_name();*/
            $_data['agree'] = 1;

            $_data['id_users_areas'] = $this->input->post("id_users_areas");
            $_data['id_account_manager'] = $this->input->post("id_account_manager");
            $_data['address_2'] = $this->input->post("address_2");
            $_data['fax'] = $this->input->post("fax");
            $_data['status'] = $this->input->post("status");

            if ($this->input->post('id') != '') {
                if ($this->input->post('password') != '') {
                    $_data['password'] = md5($this->input->post('password'));
                }
            } else {
                $_data['password'] = md5($this->input->post('password'));
            }
            ////////////////////////////////SUPPLIER INFO//////////////////////////////

            if ($this->input->post('id') != '') {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $getUserInfo = $this->fct->getonerecord('user', array('id_user' => $this->input->post('id')));

                $this->db->where('id_user ', $this->input->post('id'));
                $this->db->update('user', $_data);
                $new_id = $this->input->post('id');
                $this->session->set_userdata('success_message', 'Information was updated successfully');
                if (isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
                    $user = $this->fct->getonerow("user", array("id_user" => $new_id));

                    $this->send_emails->inform_client($user);
                }
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert('user', $_data);
                $new_id = $this->db->insert_id();
                $this->ecommerce_model->createChecklistCategories($new_id);
                $this->session->set_userdata('success_message', 'Information was inserted successfully');
            }

            if ((isset($_POST['pre_registration']) && $_POST['pre_registration'] == 1 && !isset($getUserInfo['pre_registration'])) || (isset($_POST['pre_registration']) && $_POST['pre_registration'] == 1 && isset($getUserInfo['pre_registration']) && $getUserInfo['pre_registration'] == 0)) {
                $user = $this->fct->getonerow("user", array("id_user" => $new_id));
                $this->load->model("send_emails");
                $this->send_emails->inform_pre_registration($user);
            }

            if ($this->input->post('submit') == 'Save and Continue') {

                redirect(site_url("back_office/supplier/listing"));
            }
            if ($this->input->post('submit') == 'Save Changes') {
                if ($this->session->userdata("back_link") != "") {
                    $back_link = $this->session->userdata("back_link");
                    redirect($back_link);
                } else {
                    redirect(site_url("back_office/supplier"));
                }
            }
        }
    }

    public function revise($id, $formData = null, $errors = null)
    {
        if (!$this->acl->has_permission('brands', 'edit')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::SUPPLIER_EDIT,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Edit User',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
                'MODE' => 'edit',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        $cond = array("id_user" => $id);
        $categoryData = $this->fct->getonerecord($this->table, $cond);
        //print_r($categoryData);
        $data["info"] = $categoryData;
        //$data['id_parent'] = $categoryData['id_user'];

        //check owner
        if ($data['info']['id_parent'] != $this->acl->getCurrentUserId()) {
            redirect(site_url("back_office"));
        }

        $parentUser = $this->acl->getCurrentUserId();
        $cond = array("id_user" => $parentUser);
        $companyData = $this->fct->getonerecord($this->table, $cond);
        $data["maininfo"] = $companyData;

        if ($formData) {
            $data['info'] = $formData;
        } else {
            //additional errors
            $errors = [];
            if (!empty($errors)) {
                $data['globalErrors'] = $errors;
            }
        }
        
        $data['id_parent'] = $this->acl->getCurrentUserId();

        $this->load->twigView("supplierAddEditPage/index", $data);
    }
    
    public function save()
    {
        $userId = $this->input->post("id_user");

        //check owner
        if ($userId) {
            $cond = array("id_user" => $userId);
            $supplierData = $this->fct->getonerecord($this->table, $cond);
            if ($supplierData['id_parent'] != $this->acl->getCurrentUserId()) {
                redirect(site_url("back_office"));
            }
        }
        
        $parentDetails = "select * FROM `user` WHERE `id_user` = " . $this->acl->getCurrentUserId();
        $query = $this->db->query($parentDetails);
        foreach ($query->result() as $row){
            $parentDetailsName = $row->company_name;
            $parentDetailsphone = $row->phone;
            $parentDetailscountry = $row->country;
            $parentDetailsaddress = $row->address_1;
            $parentDetailscompany_email = $row->company_email;
            $parentDetailscompany_phone = $row->company_phone;
            $parentDetailsid_countries = $row->id_countries;
            $parentDetailscity = $row->city;
            $parentDetailstrading_name = $row->trading_name;
            $parentDetailswebsite = $row->website;
            $parentDetailslogo = $row->logo;
            $parentDetailsphoto = $row->photo;
            $parentDetailscorrelation_id = $row->correlation_id;
            $parentDetailsbusiness_type = $row->business_type;
            $parentDetailsdelivery_shipping_from = $row->delivery_shipping_from;
            $parentDetailsdelivery_time = $row->delivery_time;
            $parentDetailsdelivery_fee = $row->delivery_fee;
            $parentDetailslat = $row->lat;
            $parentDetailslng = $row->lng;
            $parentDetailscompany_location_map = $row->company_location_map;
            $parentDetailscredit_terms = $row->credit_terms;
            $parentDetailspayment_methods = $row->payment_methods;
            $parentDetailsid_user_type = '4';
        }
        
        $this->form_validation->set_rules("first_name", "first_name", "trim|required");
        $this->form_validation->set_rules("last_name", "last_name", "trim|required");
        $this->form_validation->set_rules("email", "email", "trim|required");

        if ($this->input->post('id') != '') {
            if ($this->input->post('password') != '') {
                $_data['password'] = md5($this->input->post('password'));
            }
        } else {
            $_data['password'] = md5($this->input->post('password'));
        }
            
        if ($this->form_validation->run() == FALSE) {
            if ($userId) {
                return $this->revise($userId, $this->input->post(null), $this->form_validation->_error_array);
            } else {
                return $this->create($this->input->post(null), $this->form_validation->_error_array);
            }
        }

        $_data['id_parent'] = $this->acl->getCurrentUserId();
        $_data['first_name'] = $this->input->post("first_name");
        $_data['last_name'] = $this->input->post("last_name");
        $_data['email'] = $this->input->post("email");
        $_data['status'] = 1;
        $_data['name'] = $parentDetailsName;
        $_data['position'] = '';
        $_data['phone'] = $parentDetailsphone;
        $_data['address_1'] = $parentDetailsaddress;
        $_data['company_email'] = $parentDetailscompany_email;
        $_data['company_phone'] = $parentDetailscompany_phone;
        $_data['id_roles'] = '5';
        $_data['id_countries'] = $parentDetailsid_countries;
        $_data['city'] = $parentDetailscity;
        $_data['trading_name'] = $parentDetailstrading_name;
        $_data['website'] = $parentDetailswebsite;
        $_data['logo'] = $parentDetailslogo;
        $_data['photo'] = $parentDetailsphoto;
        $_data['correlation_id'] = $parentDetailscorrelation_id;
        $_data['business_type'] = $parentDetailsbusiness_type;
        $_data['agree'] = 1;
        $_data['token'] = $this->input->post("token");
        $_data['delivery_shipping_from'] = $parentDetailsdelivery_shipping_from;
        $_data['delivery_time'] = $parentDetailsdelivery_time;
        $_data['delivery_fee'] = $parentDetailsdelivery_fee;
        $_data['lat'] = $parentDetailslat;
        $_data['lng'] = $parentDetailslng;
        $_data['company_location_map'] = $parentDetailscompany_location_map;
        $_data['credit_terms'] = $parentDetailscredit_terms;
        $_data['payment_methods'] = $parentDetailspayment_methods;
        $_data['encrypt_pass'] = $this->input->post("password");
        $_data['id_user_type'] = '4';
        

        if ($userId) {
            $_data["updated_date"] = date("Y-m-d h:i:s");
            $this->db->where("id_user", $userId);
            $this->db->update($this->table, $_data);
        } else {
            $_data["created_date"] = date("Y-m-d h:i:s");
            $this->db->insert($this->table, $_data);
            $userId = $this->db->insert_id();
        }

        $_data['encrypt_pass'] = $this->input->post('password');

        $this->load->model('send_emails');
        $this->send_emails->sendUserCredentialForNewApplicant($_data);

        redirect('back_office/supplier/listing');
    }

    public function create($formData = null, $errors = null)
    {
        if (!$this->acl->has_permission('brands', 'add')) {
            redirect(site_url("back_office"));
        }

        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::SUPPLIER_ADD,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Add user',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
                'MODE' => 'add',
            ],
            'globalErrors' => $errors ? $errors : null,
        ];

        if ($formData) {
            $data['info'] = $formData;
        }
        
        $data['id_user'] = $this->acl->getCurrentUserId();

        $this->load->twigView("supplierAddEditPage/index", $data);
    }

    public function getCountryOptions($selected = null, $user_id = null)
    {
        if (!$user_id) {
            $user_id = $this->acl->getCurrentUserId();
        }
        //brands
        $countries = $this->fct->getAll_cond('countries', 'title asc', []);
        $options = [];
        foreach ($countries as $country) {
            $options[] = [
                'VALUE' => $country['id_countries'],
                'TEXT' => $country['title'],
                'IS_SELECTED' => $country['id_countries'] == $selected,
            ];
        }
        return $options;
    }
}