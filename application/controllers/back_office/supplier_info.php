<?

class Supplier_info extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "supplier_info";
        $this->load->model("supplier_info_m");
        $this->lang->load("admin");
    }


    public function index($order = "")
    {
        $this->session->unset_userdata("admin_redirect_url");
        if ($this->acl->has_permission('supplier_info', 'index')) {
            if ($order == "")
                $order = "sort_order";
            $data["title"] = "List supplier info";
            $data["content"] = "back_office/supplier_info/list";
//
            $this->session->unset_userdata("back_link");
//
            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "25";
            }
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
            $data["show_items"] = $show_items;
// pagination  start :
            $count_news = $this->supplier_info_m->getAll($this->table, $order);
            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $this->load->library('pagination');
            $config['base_url'] = site_url("back_office/supplier_info/index/" . $order);
            $config['total_rows'] = $count_news;
            $config['per_page'] = $show_items;
            $config['uri_segment'] = 5;
            $this->pagination->initialize($config);
            $data['info'] = $this->supplier_info_m->list_paginate($order, $config['per_page'], $this->uri->segment(5));
// end pagination .
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }


    public function add()
    {
        if ($this->acl->has_permission('supplier_info', 'add')) {
            $data["title"] = "Add supplier info";
            $data["content"] = "back_office/supplier_info/add";
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function view($id, $obj = "")
    {
        if ($this->acl->has_permission('supplier_info', 'index')) {
            $data["title"] = "View supplier info";
            $data["content"] = "back_office/supplier_info/add";
            $cond = array("id_supplier_info" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function edit($id, $obj = "")
    {
        if ($this->acl->has_permission('supplier_info', 'edit')) {
            $data["title"] = "Edit info";
            $data["content"] = "back_office/supplier_info/add";

            $user = $this->fct->getonerecord('user', array('id_user' => $id));
            $data['info'] = $user;


            $cond = array("id_user" => $id);
            $data["supplier_info"] = $this->fct->getonerecord($this->table, $cond);
            if (empty($data["supplier_info"])) {
                $_data["id_user"] = $id;
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert($this->table, $_data);
                $new_id = $this->db->insert_id();
                $cond2 = array("id_supplier_info" => $new_id);
                $data["supplier_info"] = $this->fct->getonerecord($this->table, $cond2);
            }
            $data['id'] = $data["supplier_info"]['id_supplier_info'];
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete($id)
    {
        if ($this->acl->has_permission('supplier_info', 'delete')) {
            $_data = array("deleted" => 1,
                "deleted_date" => date("Y-m-d h:i:s"));
            $this->db->where("id_supplier_info", $id);
            $this->db->update($this->table, $_data);
            $this->session->set_userdata("success_message", "Information was deleted successfully");
            redirect(site_url("back_office/supplier_info/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_all()
    {
        if ($this->acl->has_permission('supplier_info', 'delete_all')) {
            $cehcklist = $this->input->post("cehcklist");
            $check_option = $this->input->post("check_option");
            if ($check_option == "delete_all") {
                if (count($cehcklist) > 0) {
                    for ($i = 0; $i < count($cehcklist); $i++) {
                        if ($cehcklist[$i] != "") {
                            $_data = array("deleted" => 1,
                                "deleted_date" => date("Y-m-d h:i:s"));
                            $this->db->where("id_supplier_info", $cehcklist[$i]);
                            $this->db->update($this->table, $_data);
                        }
                    }
                }
                $this->session->set_userdata("success_message", "Informations were deleted successfully");
            }
            redirect(site_url("back_office/supplier_info/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i + 1);
            $this->db->where("id_supplier_info", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit supplier info";
        $this->form_validation->set_rules('trading_name', lang('trading_name'), 'trim|required|xss_clean');
        /*$this->form_validation->set_rules('trading_licence',lang('trading_licence'), 'trim|required|xss_clean');*/
        //////////////Supplier Inff///////////////////////////////////////
        $this->form_validation->set_rules('name', lang('name'), 'trim|xss_clean');
        $this->form_validation->set_rules('mobile', lang('mobile'), 'trim|xss_clean');
        $this->form_validation->set_rules('business_type', lang('business_type'), 'trim|required|xss_clean');

        $this->form_validation->set_rules('delivery_time', lang('delivery_time'), 'trim|xss_clean');
        $this->form_validation->set_rules('delivery_fee', lang('delivery_fee'), 'trim|xss_clean');
        $this->form_validation->set_rules('min_order', lang('min_order'), 'trim|xss_clean');
        $this->form_validation->set_rules('credit_terms', lang('credit'), 'trim|xss_clean');

        /*$this->form_validation->set_rules('hear_about_us',lang('hear_about_us'), 'trim|required|xss_clean');*/

        if ($this->form_validation->run() == FALSE) {

            $this->edit($this->input->post("id_user"));

        } else {
/////////////////////USER//////////////////////
            $user_id = $this->input->post("id_user");
            $user['trading_name'] = $this->input->post("trading_name");
            $user['trading_licence'] = $this->input->post("trading_licence");
            $user['business_type'] = $this->input->post("business_type");
            $user['display_in_menu'] = $this->input->post("display_in_menu");
            $user['website'] = $this->input->post("website");
            $user['hear_about_us'] = $this->input->post("hear_about_us");

            $user['delivery_time'] = $this->input->post("delivery_time");
            $user['delivery_time_period'] = $this->input->post("delivery_time_period");
            $user['delivery_time_period_type'] = $this->input->post("delivery_time_period_type");
            $user['delivery_fee'] = $this->input->post("delivery_fee");
            $user['delivery_fee_price'] = $this->input->post("delivery_fee_price");
            $user['min_order'] = $this->input->post("min_order");
            $user['credit_terms'] = $this->input->post("credit_terms");
            $this->fct->insert_user_license($user_id);


            $categories = array();
            if (isset($_POST['categories']) && !empty($_POST['categories'])) {
                $categories = $_POST['categories'];
            }

            $this->users_categories_m->insert_user_categories_admin($user_id, $categories);

            if ($_FILES["trading_licence"]["name"]) {

                if (!empty($id_user)) {
                    $cond_image = array("id_user" => $id_user);
                    $old_image = $this->fct->getonecell("user", "trading_licence", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/user/' . $old_image)) {
                        unlink("./uploads/user/" . $old_image);
                    }
                }

                $image1 = $this->fct->uploadImage("trading_licence", "user");
                $user["trading_licence"] = $image1;
            }

            if (!empty($_FILES["logo"]["name"])) {
                if ($this->input->post("id") != "") {
                    $cond_image = array("id_user" => $this->input->post("id_user"));
                    $old_image = $this->fct->getonecell("user", "logo", $cond_image);
                    if (!empty($old_image) && file_exists('./uploads/user/' . $old_image)) {
                        unlink("./uploads/user/" . $old_image);
                    }
                }


                $image1 = $this->fct->uploadImage("logo", "user");
                $this->fct->createthumb($image1, "user", "112x122");
                $user["logo"] = $image1;
            }


            $user['comments'] = $this->input->post("comments");
            $_data["updated_date"] = date("Y-m-d h:i:s");
            $this->db->where("id_user", $user_id);
            $this->db->update('user', $user);


//////////////////////////////////////////////////////////////////////////////////////////////////////	

            if ($this->input->post("id") != "") {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_supplier_info", $this->input->post("id"));
                $this->db->update($this->table, $_data);
                $new_id = $this->input->post("id");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert($this->table, $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }


            if ($this->session->userdata("admin_redirect_url")) {
                redirect($this->session->userdata("admin_redirect_url"));
            } else {
                redirect(site_url("back_office/supplier_info/edit/" . $user_id));
            }

        }

    }

    public function delete_file()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/supplier_info/" . $image)) {
            unlink("./uploads/supplier_info/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'supplier info')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/supplier_info/" . $value . "/" . $image)) {
                    unlink("./uploads/supplier_info/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_supplier_info", $id);
        $this->db->update("supplier_info", $_data);
        echo 'Done!';
    }

}