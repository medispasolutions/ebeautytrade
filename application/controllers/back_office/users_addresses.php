<?
class Users_addresses extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="users_addresses";
$this->load->model("users_addresses_m");
 $this->lang->load("admin"); 
}


public function index(){
$cond=array();
$order ="sort_order";
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('users_addresses','index')){	
$url=site_url("back_office/users_addresses/index?pagination=on");
$data["title"]="List users addresses";
$data["content"]="back_office/users_addresses/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :



if(isset($_GET["keyword"])) {
	$keyword = $_GET["keyword"];
	$url .= "&keyword=".$keyword;
	if(!empty($keyword))
	$cond["keyword"] = $keyword;
}
$cond["pickup"] = 1;
$show_items = ($show_items == "All") ? $count_news : $show_items;
$this->load->library("pagination");

$count_news = $this->users_addresses_m->getRecords($cond);
$config["base_url"] = $url;
$config["total_rows"] = $count_news;
$config["per_page"] =$show_items;
$config["use_page_numbers"] = TRUE;
$config["page_query_string"] = TRUE;
//print "<pre>";print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET["per_page"])) {
	if($_GET["per_page"] != "") $page = $_GET["per_page"];
	else $page = 0;
}
else $page = 0;
$data["info"] = $this->users_addresses_m->getRecords($cond,$show_items,$page);
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add($data=""){
if ($this->acl->has_permission('users_addresses','add')){	
$data["title"]="Add users addresses";
$data["content"]="back_office/users_addresses/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('users_addresses','index')){	
$data["title"]="View users addresses";
$data["content"]="back_office/users_addresses/add";
$cond=array("id_users_addresses"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$data = ""){
	
if ($this->acl->has_permission('users_addresses','edit')){	
$data["title"]="Edit users addresses";
$data["content"]="back_office/users_addresses/add";
$cond=array("id_users_addresses"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('users_addresses','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_users_addresses",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/users_addresses/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('users_addresses','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_users_addresses",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/users_addresses/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_users_addresses",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}
		public function phone()
	{  
	
	$phone=$this->input->post('phone');
$bool=true;
foreach($phone as $key=>$val){
	if(empty($val)){
		$bool=false;}}

	if(!$bool) {
			$this->form_validation->set_message('phone',"The phone fields is required.");
		   return false;
			}else{
				return true;
			}
	
	}
	

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit users addresses";

$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("state", "state"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("phone", "Phone", "trim");
$this->form_validation->set_rules("fax", "Fax", "trim");
$this->form_validation->set_rules("mobile", "Mobile", "trim");
$this->form_validation->set_rules("city", "city"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("country", "country"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("billing", "billing"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("shipping", "shipping"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("branch", "branch"."(".$this->lang->lang().")", "trim");

$this->form_validation->set_rules("id_countries", "id_countries"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("pickup", "pickup"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("address", "address"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("type", "type"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("as_default", "as_default"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("street_one", "street one"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("street_two", "street two"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("place", "place"."(".$this->lang->lang().")", "trim");

$this->form_validation->set_rules("address_name", "address name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("floor_name", "floor name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("appartment_office", "appartment office"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("first_name", "first name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("last_name", "last name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("name", "name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("company_name", "company name"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
/*	$data['phone']=implode('-',$this->input->post('phone'));;
	$data['mobile']=implode('-',$this->input->post('mobile'));;
	$data['fax']=implode('-',$this->input->post('fax'));*/
if($this->input->post("id")!="")
$this->edit($this->input->post("id"),$data);
else
$this->add($data);
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");

$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["state"]=$this->input->post("state");
$_data["postal_code"]=$this->input->post("postal_code");
$_data["city"]=$this->input->post("city");
$_data["country"]=$this->input->post("country");



$_data["id_countries"]=$this->input->post("id_countries");
$_data["pickup"]=1;
$_data["address"]=$this->input->post("address");

$_data["street_one"]=$this->input->post("street_one");
$_data["street_two"]=$this->input->post("street_two");
$_data["place"]=$this->input->post("place");
/*if($this->input->post('phone')!=""){
$_data["phone"]=implode('-',$this->input->post('phone'));}
if($this->input->post('fax')!=""){
$_data["fax"]=implode('-',$this->input->post('fax'));}
if($this->input->post('mobile')!=""){
$_data["mobile"]=implode('-',$this->input->post('mobile'));}*/


$_data["phone"]=$this->input->post('phone');
$_data["mobile"]=$this->input->post('mobile');
$_data["fax"]=$this->input->post('fax');

$_data["address_name"]=$this->input->post("address_name");
$_data["floor_name"]=$this->input->post("floor_name");
$_data["appartment_office"]=$this->input->post("appartment_office");
$_data["first_name"]=$this->input->post("first_name");
$_data["last_name"]=$this->input->post("last_name");
$_data["name"]=$_data["first_name"].' '.$_data["last_name"];
$_data["company_name"]=$this->input->post("company_name");
 $country_name="";
 if(!empty($_data['id_countries'])){
$country_name=$this->fct->getonecell('countries','title',array('id_countries'=>$_data['id_countries']));
}
$_data["title".$lang]=$_data['street_one'].', '.$_data['city'].', '.$_data['state'].', '.$country_name; 


	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_users_addresses",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/users_addresses/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/users_addresses/".$image)){
unlink("./uploads/users_addresses/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'users addresses')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/users_addresses/".$value."/".$image)){
unlink("./uploads/users_addresses/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_users_addresses",$id);
$this->db->update("users_addresses",$_data);
echo 'Done!';
}

}