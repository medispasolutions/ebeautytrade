<?
class Product_price_segments extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="product_price_segments";
$this->load->model("product_price_segments_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List product price segments";
$data["content"]="back_office/product_price_segments/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
$cond = array();
if($this->input->get("id_product") != "") {
$cond['id_products'] = $this->input->get("id_product");
$data['product_Data'] = $this->fct->getonerow("products",array("id_products"=>$cond['id_products']));
}
// pagination  start :
$count_news = $this->product_price_segments_m->getAll_cond($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = site_url("back_office/product_price_segments/index/".$order);
$config['total_rows'] = $count_news;
$config['per_page'] = 200;
$config['uri_segment'] = 5;
$this->pagination->initialize($config);
$data['info'] = $this->product_price_segments_m->getAll_cond($cond,$config['per_page'],$this->uri->segment(5));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add($id_product=""){
if ($this->acl->has_permission('products','add')){	
$data["title"]="Add product price segments";
$data["content"]="back_office/product_price_segments/add";

	if($id_product != ""){
	
	$data['id_products'] = $id_product;	}else{
	$data['id_products'] = $this->input->get('id_product');}
	$data['product'] = $this->fct->getonerow('products',array('id_products'=>$data['id_products']));


$this->load->view("back_office/product_price_segments/add",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('products','index')){	
$data["title"]="View product price segments";
$data["content"]="back_office/product_price_segments/add";
$cond=array("id_product_price_segments"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if($this->input->get('id_product') != '') {
	$data['id_products'] = $this->input->get('id_product');
	$data['product'] = $this->fct->getonerow('products',array('id_products'=>$data['id_products']));
}
$this->load->view("back_office/product_price_segments/add",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('products','edit')){	
$data["title"]="Edit product price segments";
$data["content"]="back_office/product_price_segments/add";
$cond=array("id_product_price_segments"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
if($this->input->get('id_product') != '') {
	$data['id_products'] = $this->input->get('id_product');
	$data['product'] = $this->fct->getonerow('products',array('id_products'=>$data['id_products']));
}
$this->load->view("back_office/product_price_segments/add",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('product_price_segments','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_product_price_segments",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
if($this->input->get('id_product') != '') {
	redirect(site_url("back_office/product_price_segments?id_product=".$this->input->get('id_product')));
}
else {
	redirect(site_url("back_office/product_price_segments/".$this->session->userdata("back_link")));
}
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('product_price_segments','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_product_price_segments",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
if($this->input->get('id_product') != '') {
	redirect(site_url("back_office/product_price_segments?id_product=".$this->input->get('id_product')));
}
else {
	redirect(site_url("back_office/product_price_segments/".$this->session->userdata("back_link")));
}
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_product_price_segments",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
$allPostedVals = $this->input->post(NULL, TRUE);

if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit product price segments";
/*$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");*/
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("product", "product", "trim|required");
$this->form_validation->set_rules("min_qty", "min qty", "trim|required");
$this->form_validation->set_rules("price", "price", "trim|required");
//$this->form_validation->set_rules("discount", "discount"."(".$this->lang->lang().")", "trim|required");
//$this->form_validation->set_rules("discount_expiration", "discount expiration"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
	
		
			$return['result'] = 0;
			$return['errors'] = array();
			$return['message'] = 'Error!';
			$return['captcha'] = $this->fct->createNewCaptcha();
			$find =array('<p>','</p>');
			$replace =array('','');
			foreach($allPostedVals as $key => $val) {
				if(form_error($key) != '') {
					$return['errors'][$key] = str_replace($find,$replace,form_error($key));
				}
			}
		$return['message'] = validation_errors();
		echo json_encode($return);
		}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["id_products"]=$this->input->post("product"); 
$_data["min_qty"]=$this->input->post("min_qty");
$_data["price"]=$this->input->post("price");
//$_data["discount"]=$this->input->post("discount");

//$_data["discount_expiration"]=$this->fct->date_in_formate($this->input->post("discount_expiration"));
	$return['result']=1;
	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_product_price_segments",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	$return['message']="Information was updated successfully";
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	$return['message']="Information was inserted successfully";
	}
	
		//////////////////INFORM ADMIN//////
		$cond['id_user']=$this->session->userdata('user_id');
		$user=$this->fct->getonerecord('user',$cond);
		$product = $this->fct->getonerow('products',array('id_products'=>$_data["id_products"]));
	    if($user['id_roles']==5){
$update_product["published"] = 1;
$update_product["status"] = 1;
	if($product['inform_admin']==0){
	$update_product['inform_admin']=1;
	$this->load->model("send_emails");
	$this->send_emails->informProductModificationsToAdmin($user,$product);
		}
$this->db->where("id_products",$product['id_products']);
$this->db->update('products',$update_product);
	}

/*	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	   if($this->input->post('id_product') != '') {
			redirect(site_url("back_office/product_price_segments?id_product=".$this->input->post('id_product')));
		}
		else {
   		    redirect(site_url("back_office/product_price_segments/".$this->session->userdata("back_link")));
		}
	}*/
	die(json_encode($return));
}
	
}
public function getProductSegements(){
	$id_product=$this->input->post('id_product');
	$segements=$this->fct->getAll_cond('product_price_segments','id_product_price_segments desc',array('id_products'=>$id_product)); 
	$data['segements']=$segements;
	$return['html']=$this->load->view('back_office/blocks/products_segements',$data,true);
	die(json_encode($return));
	}
public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/product_price_segments/".$image)){
unlink("./uploads/product_price_segments/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'product price segments')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if(isset($res["thumb"]) && $res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/product_price_segments/".$value."/".$image)){
unlink("./uploads/product_price_segments/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_product_price_segments",$id);
$this->db->update("product_price_segments",$_data);
echo 'Done!';
}

}