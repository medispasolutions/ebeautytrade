<?

class Routes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "routes";
        $this->load->model("routes_m");
    }


    public function index($order = "")
    {
        $this->session->unset_userdata("admin_redirect_url");
        if ($this->acl->has_permission('routes', 'index')) {
            if ($order == "")
                $order = "sort_order";
            $data["title"] = "List routes";
            $data["content"] = "back_office/routes/list";
//
            $this->session->unset_userdata("back_link");
//
            if ($this->input->post('show_items')) {
                $show_items = $this->input->post('show_items');
                $this->session->set_userdata('show_items', $show_items);
            } elseif ($this->session->userdata('show_items')) {
                $show_items = $this->session->userdata('show_items');
            } else {
                $show_items = "200";
            }
            $this->session->set_userdata('back_link', 'index/' . $order . '/' . $this->uri->segment(5));
            $data["show_items"] = $show_items;
// pagination  start :
            $count_news = $this->routes_m->getAll($this->table, $order);
            $show_items = ($show_items == 'All') ? $count_news : $show_items;
            $this->load->library('pagination');
            $config['base_url'] = site_url("back_office/routes/index/" . $order);
            $config['total_rows'] = $count_news;
            $config['per_page'] = $show_items;
            $config['uri_segment'] = 5;
            $this->pagination->initialize($config);
            $data['info'] = $this->routes_m->list_paginate($order, $config['per_page'], $this->uri->segment(5));
// end pagination .
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }


    public function add()
    {
        if ($this->acl->has_permission('routes', 'add')) {
            $data["title"] = "Add routes";
            $data["content"] = "back_office/routes/add";
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function view($id, $obj = '')
    {
        if ($this->acl->has_permission('routes', 'index')) {
            $data["title"] = "View routes";
            $data["content"] = "back_office/routes/add";
            $cond = array("id_routes" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function edit($id, $obj = '')
    {
        if ($this->acl->has_permission('routes', 'edit')) {
            $data["title"] = "Edit routes";
            $data["content"] = "back_office/routes/add";
            $cond = array("id_routes" => $id);
            $data["id"] = $id;
            $data["info"] = $this->fct->getonerecord($this->table, $cond);
            $this->load->view("back_office/template", $data);
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete($id)
    {
        if ($this->acl->has_permission('routes', 'delete')) {
            $_data = array("deleted" => 1,
                "deleted_date" => date("Y-m-d h:i:s"));
            $this->db->where("id_routes", $id);
//$this->db->update($this->table,$_data);
            $this->db->delete($this->table);
            $this->session->set_userdata("success_message", "Information was deleted successfully");
            redirect(site_url("back_office/routes/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function delete_all()
    {
        if ($this->acl->has_permission('routes', 'delete_all')) {
            $cehcklist = $this->input->post("cehcklist");
            $check_option = $this->input->post("check_option");
            if ($check_option == "delete_all") {
                if (count($cehcklist) > 0) {
                    for ($i = 0; $i < count($cehcklist); $i++) {
                        if ($cehcklist[$i] != "") {
                            $_data = array("deleted" => 1,
                                "deleted_date" => date("Y-m-d h:i:s"));
                            $this->db->where("id_routes", $cehcklist[$i]);
//$this->db->update($this->table,$_data);	
                            $this->db->delete($this->table);
                        }
                    }
                }
                $this->session->set_userdata("success_message", "Informations were deleted successfully");
            }
            redirect(site_url("back_office/routes/" . $this->session->userdata("back_link")));
        } else {
            redirect(site_url("home/dashboard"));
        }
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i + 1);
            $this->db->where("id_routes", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data["title"] = "Add / Edit routes";
        $this->form_validation->set_rules("title", "TITLE", "trim|required");
        $this->form_validation->set_rules("meta_title", "PAGE TITLE", "trim|max_length[65]");
        $this->form_validation->set_rules("title_url", "TITLE URL", "trim");
        $this->form_validation->set_rules("meta_description", "META DESCRIPTION", "trim|max_length[160]");
        $this->form_validation->set_rules("meta_keywords", "META KEYWORDS", "trim|max_length[160]");
        $this->form_validation->set_rules("route_index", "route index", "trim|required");
        $this->form_validation->set_rules("route_rule", "route rule", "trim|required");
        if ($this->form_validation->run() == FALSE) {

            if ($this->input->post("id") != "")
                $this->edit($this->input->post("id"));
            else
                $this->add();

        } else {

            $_data["id_user"] = $this->session->userdata("uid");
            $_data["title"] = $this->input->post("title");
            $_data["meta_title"] = $this->input->post("meta_title");
            if ($this->input->post("title_url") == "")
                $title_url = $this->input->post("title");
            else
                $title_url = $this->input->post("title_url");
            $_data["title_url"] = $this->fct->cleanURL("routes", url_title($title_url), $this->input->post("id"));
            $_data["meta_description"] = $this->input->post("meta_description");
            $_data["meta_keywords"] = $this->input->post("meta_keywords");
            $_data["route_index"] = $this->input->post("route_index");
            $_data["route_rule"] = $this->input->post("route_rule");
            $_data["sub_page"] = 0;
            if (isset($_POST['sub_page']))
                $_data["sub_page"] = $this->input->post("sub_page");

            if ($this->input->post("id") != "") {
                $_data["updated_date"] = date("Y-m-d h:i:s");
                $this->db->where("id_routes", $this->input->post("id"));
                $this->db->update($this->table, $_data);
                $new_id = $this->input->post("id");
                $this->session->set_userdata("success_message", "Information was updated successfully");
            } else {
                $_data["created_date"] = date("Y-m-d h:i:s");
                $this->db->insert($this->table, $_data);
                $new_id = $this->db->insert_id();
                $this->session->set_userdata("success_message", "Information was inserted successfully");
            }
            if ($this->session->userdata("admin_redirect_url")) {
                redirect($this->session->userdata("admin_redirect_url"));
            } else {
                redirect(site_url("back_office/" . $this->table . "/" . $this->session->userdata("back_link")));
            }
        }

    }

    public function delete_file()
    {
        $field = $this->input->post('field');
        $image = $this->input->post('image');
        $id = $this->input->post('id');
        if (file_exists("./uploads/routes/" . $image)) {
            unlink("./uploads/routes/" . $image);
        }
        $q = " SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'routes')
AND name = '" . $field . "'";
        $query = $this->db->query($q);
        $res = $query->row_array();
        if (isset($res["thumb"]) && $res["thumb"] == 1) {
            $sumb_val1 = explode(",", $res["thumb_val"]);
            foreach ($sumb_val1 as $key => $value) {
                if (file_exists("./uploads/routes/" . $value . "/" . $image)) {
                    unlink("./uploads/routes/" . $value . "/" . $image);
                }
            }
        }
        $_data[$field] = "";
        $this->db->where("id_routes", $id);
        $this->db->update("routes", $_data);
        echo 'Done!';
    }

    public function updateAll()
    {


        $code = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');";


        $result = $this->fct->getAll("routes", "sort_order");
        if (!empty($result)) {
            foreach ($result as $row) {
                $code .= "
		 route[ '" . strtolower($row['route_rule']) . "' ]                        = '" . $row['route_index'] . "';";
                //$code .= " route[ '^en/'.row['route_rule'] ]                 = row['route_index'];";
            }
        }


        $cond = array();
        $result = $this->ecommerce_model->getClassifications($cond);
        $code .= " route[ 'register' ]                        = 'user/register';";
        $code .= " route[ 'login/(:any)' ]              = 'user/login_by_token/$1';";
        $code .= " route[ 'account' ]                        = 'user';";
        $code .= " route[ 'login' ]                        = 'user/login';";

        $code .= " route[ 'wishlist' ]                        = 'user/favorites';";
        $code .= " route[ 'disposables' ]                        = 'user/categories';";
        $code .= " route[ 'branches' ]                        = 'user/branches';";
        $code .= " route[ 'stock-list' ]                        = 'user/checklist';";
        $code .= " route[ 'brands' ]                        = 'user/brands';";
        $code .= " route[ 'profile' ]                        = 'user/profile';";
        $code .= " route[ 'addresses' ]                        = 'user/addresses';";
        $code .= " route[ 'address' ]                        = 'user/address';";
        $code .= " route[ 'orders' ]                        = 'user/orders';";
        $code .= " route[ 'thank-you' ]                        = 'user/thank_you';";
        $code .= " route[ 'user-miles' ]                        = 'user/miles';";
        $code .= " route[ 'petty-cash' ]                        = 'user/credits';";

        $code .= " route[ 'compare' ]                        = 'user/compareProducts';";
        $code .= " route[ 'logout' ]                        = 'user/logout';";
        $code .= " route[ 'checkout' ]                        = 'cart/checkout';";
        $code .= " route[ 'change_currency' ]                        = 'home/change_currency';";

        $code .= " route[ 'new-arrivals' ]                        = 'products/index/new-arrivals';";
        $code .= " route[ 'offers' ]                        = 'products/index/offers';";


        $code .= " route[ 'products-clearance' ]                        = 'products/index/clearance';";
        /*code .= "route[ 'customer-service/(:any)'] = 'pages/customer_service/$1';";*/
        /*$result = $this->fct->getAll('customer_service','sort_order');
            if(!empty($result)) {
            $code .= "/////////////////////////////////////////////////////////////////////////////////";
            foreach( $result as $row )
            {
            $find=array("'"," ");
            $replace=array("-","-");
            $title_url = str_replace($find,$replace,$row['title_url']);
                 $code .= "
                 route[ 'clients/(:any)'] = 'clients/index/$1';";
            }}	*/

        $result = $this->fct->getAll('about_spamiles', 'sort_order');

        if (!empty($result)) {
            $code .= "/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ 'about-spamiles/" . strtolower($title_url) . "' ]                        = 'pages/about_spamiles/" . $row['id_about_spamiles'] . "';";
            }
        }

        $result = $this->fct->getAll('customer_service', 'sort_order');
        if (!empty($result)) {
            $code .= "/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ 'customer-service/" . strtolower($title_url) . "' ]                        = 'pages/customer_service/" . $row['title_url'] . "';";
            }
        }

        $result = $this->fct->getAll('categories', 'sort_order');
        if (!empty($result)) {
            $code .= "/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ '" . strtolower($title_url) . "' ]                        = 'products/category/" . $row['id_categories'] . "';";
            }
        }


        $result = $this->fct->getAll('brands', 'title asc');
        if (!empty($result)) {
            $code .= "/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ 'brand/" . strtolower($title_url) . "' ]                        = 'brands/details/" . $row['id_brands'] . "';";
            }
        }

        $result = $this->fct->getAll('training_categories', 'sort_order');
        if (!empty($result)) {
            $code .= "/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ '" . strtolower($title_url) . "' ]                        = 'brands/training/" . $row['id_training_categories'] . "';";
            }
        }


        $result = $this->fct->getAll('guide', 'sort_order');
        if (!empty($result)) {
            $code .= "/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ '" . strtolower($title_url) . "' ]                        = 'contactlist/index/" . $row['id_guide'] . "';";
            }
        }


        $result = $this->fct->getAll('products', 'sort_order');
        if (!empty($result)) {
            $code .= "
	/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ 'product/" . strtolower($title_url) . "' ]            = 'products/details/" . $row['id_products'] . "';";
            }
        }


        $result = $this->fct->getAll('questions', 'sort_order');
        if (!empty($result)) {
            $code .= "
	/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ '" . strtolower($title_url) . "' ]                        = 'page/index/questions/" . $row['title_url'] . "';";
            }
        }

        $result = $this->fct->getAll('information', 'sort_order');
        if (!empty($result)) {
            $code .= "
	/////////////////////////////////////////////////////////////////////////////////";
            foreach ($result as $row) {
                $find = array("'", " ");
                $replace = array("-", "-");
                $title_url = str_replace($find, $replace, $row['title_url']);
                $code .= "
		 route[ '" . strtolower($title_url) . "' ] = 'page/index/information/" . $row['title_url'] . "';";
            }
        }
        //////////////////STATIC SEO///////////

        $code .= "
	/////////////////////////////////////////////////////////////////////////////////";

        $static_seo = $this->fct->getonecell('static_seo_pages', 'title_url', array('id_static_seo_pages' => 36));
        $code .= "
		 route[ '" . strtolower($static_seo) . "' ] = 'contactus';";

        $code .= "
	/////////////////////////////////////////////////////////////////////////////////";
        $static_seo = $this->fct->getonecell('static_seo_pages', 'title_url', array('id_static_seo_pages' => 21));
        $code .= "
		 route[ '" . strtolower($static_seo) . "' ] = 'products/index/miles';";

        $code .= "
	/////////////////////////////////////////////////////////////////////////////////";
        $static_seo = $this->fct->getonecell('static_seo_pages', 'title_url', array('id_static_seo_pages' => 35));
        $code .= "
		 route[ '" . strtolower($static_seo) . "' ] = 'opening_new_spa';";


        $code .= "
	/////////////////////////////////////////////////////////////////////////////////";
        $static_seo = $this->fct->getonecell('static_seo_pages', 'title_url', array('id_static_seo_pages' => 22));
        $code .= "
		 route[ '" . strtolower($static_seo) . "' ] = 'brands/training';";


        $code .= "
	/////////////////////////////////////////////////////////////////////////////////";
        $code .= "
	route['default_controller'] = 'home/index';";

        $code .= "
	route['back_office'] = 'back_office/home';";

        $code .= "
	route['404_override'] = 'error404';";

        $code .= "
	//route['^ar/(.+)$'] = '$1';
	//route['^fr/(.+)$'] = '$1';
	route['^en/(.+)$'] = '$1';
	//route['^fr$'] = route['default_controller'];
	//route['^ar$'] = route['default_controller'];
	route['^en$'] = route['default_controller'];";
        $code .= "
	route[''] = 'home';";


        $find = array("route[", "row[");
        $replace = array('$route[', '$row[');

        $path = "./application/config/routes.php";

        $code = str_replace($find, $replace, $code);
        $this->fct->write_file($path, $code);

        $this->session->set_userdata("success_message", "Routes are published successfully..");
        redirect(site_url("back_office/routes"));

    }

}