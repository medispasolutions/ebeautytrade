<?
class Categories extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="categories";
$this->load->model("categories_m");
 $this->lang->load("admin"); 
}


public function index($order=""){

$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('categories','index')){	

if($order == "")
$order ="sort_order";
$data["title"]="List categories";
$data["content"]="back_office/categories/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :

$cond = array();
$like = array();
$url = site_url("back_office/categories")."?pagination=on";
$id_category = 0;
$data['current_category'] = array();
$data['parent_levels'] = array();
if($this->input->get('section')!=""){
	$url .= "&category=".$id_category;
	$data['section']=$this->input->get('section');
	$cond['c.section']=$this->input->get('section');}else{
	$data['section']='b2b';	}
if($this->input->get("category") != "") {
	$id_category = $this->input->get("category");
	$url .= "&category=".$id_category;
	$data['parent_levels'] = $this->custom_fct->getCategoriesTreeParentLevels($id_category);
	$data['parent_levels'] = arrangeParentLevels($data['parent_levels']);
	$data['current_category'] = $this->fct->getonerow("categories",array("id_categories"=>$id_category));
    //print '<pre>'; print_r($data['parent_levels']); exit;
}
$count_news = $this->custom_fct->getCategoriesPages($id_category,$cond,$like);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['total_rows'] = $count_news;
$config['per_page'] =$this->config->item("admin_per_page");
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->custom_fct->getCategoriesPages($id_category,$cond,$like,$config['per_page'],$page);

$url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$this->session->set_userdata("admin_redirect_url",$url);

//print '<pre>';print_r($data['info']);exit;
// end pagination .

$this->load->view("back_office/template",$data);

} else {
	redirect(site_url("home/dashboard"));
}
}


public function add($data=array()){
if ($this->acl->has_permission('categories','add')){	
$data["title"]="Add categories";
$data["content"]="back_office/categories/add";

if($this->input->get('section')==""){
$data['section']='b2b';}


if($this->input->get('section')!=""){	
$data['section']=$this->input->get('section');	}
	

$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('categories','index')){	
$data["title"]="View categories";
$data["content"]="back_office/categories/add";
$cond=array("id_categories"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
	$data=$obj;
if ($this->acl->has_permission('categories','edit')){	
$data["title"]="Edit categories";
$data["content"]="back_office/categories/add";
$cond=array("id_categories"=>$id);
$data["id"]=$id;



$data["info"]=$this->fct->getonerecord($this->table,$cond);
$data["section"]=$data["info"]['section'];
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('categories','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_categories",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/categories/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('categories','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_categories",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/categories/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_categories",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit categories";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("id_group_categories", "Group Categories", "trim");
$this->form_validation->set_rules("status", "Status", "trim");
$this->form_validation->set_rules("position", "Position", "trim");
$this->form_validation->set_rules("section", "Section", "trim");
$this->form_validation->set_rules("color", "color", "trim");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[450]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[450]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[450]");
$this->form_validation->set_rules("category", "category"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("image", "image"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("description".$lang, "description"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
	$data=array();
$data["section"]=$this->input->post("section");			
$data["activate_view_all"]=$this->input->post("activate_view_all");
$data["bold"]=$this->input->post("bold");
$data["set_color"]=$this->input->post("set_color");
$data["enable_sort_by_brand"]=$this->input->post("enable_sort_by_brand");
if($this->input->post("id")!="")
$this->edit($this->input->post("id"),$data);
else
$this->add($data);
}
else
{
$_data["enable_sort_by_brand"]=$this->input->post("enable_sort_by_brand");	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["position"]=$this->input->post("position");

$_data["section"]=$this->input->post("section");

$_data["id_group_categories"]=$this->input->post("id_group_categories");
$_data["bold"]=$this->input->post("bold");
$_data["set_color"]=$this->input->post("set_color");
$_data["color"]=$this->input->post("color");
$_data["status"]=$this->input->post("status");
$_data["activate_view_all"]=$this->input->post("activate_view_all");
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_title"]=$this->input->post("meta_title");
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["id_parent"]=$this->input->post("category"); 
if(!empty($_FILES["image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_categories"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("categories","image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/categories/'.$old_image)){
unlink("./uploads/categories/".$old_image);
 } }
$image1= $this->fct->uploadImage("image","categories");
$this->fct->createthumb($image1,"categories","295x295");
$_data["image"]=$image1;	
}
$_data["description".$lang]=$this->input->post("description".$lang);




	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_categories",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}

/////
	if($this->input->post("list_id") != "") {

		$list_id = $this->input->post("list_id");
		$list_title = $this->input->post("list_title");
		$list_order = $this->input->post("list_order");
		foreach($list_id as $key => $v) {
			if(!empty($list_title[$key])) {
				$dt['title'] = $list_title[$key];
				$dt['sort_order'] = $list_order[$key];
				if(empty($v)) {
					$dt['id_categories'] = $new_id;
					$this->db->insert("categories_listing",$dt);
				}
				else {
					$this->db->where("id_categories_listing",$v);
					$this->db->update("categories_listing",$dt);
				}
			}
		}
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/categories/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/categories/".$image)){
unlink("./uploads/categories/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'categories')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/categories/".$value."/".$image)){
unlink("./uploads/categories/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_categories",$id);
$this->db->update("categories",$_data);
echo 'Done!';
}

}