<?
class Products_review extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="products_review";
$this->load->model("products_review_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('products','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List products";
$data["content"]="back_office/products/list";

//
$this->session->unset_userdata("back_link");
//    
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;

// organize data filtration
$cond = array();
$cond['All']="All";
$url = site_url('back_office/products?pagination=on');

if(isset($_GET['product_id'])) {
	$product_id = $_GET['product_id'];
	$url .= '&product_id='.$product_id;
	if(!empty($product_id))
	$cond['id_products'] = $product_id;
}

if(isset($_GET['product_name'])) {
	$product_name = $_GET['product_name'];
	$url .= '&product_name='.$product_name;
	if(!empty($product_name))
	$cond['title'.getFieldLanguage($this->lang->lang())] = $product_name;
}

if(isset($_GET['product_sku'])) {
	$product_sku = $_GET['product_sku'];
	$url .= '&product_sku='.$product_sku;
	if(!empty($product_sku))
	$cond['sku'] = $product_sku;
}


if(isset($_GET['category'])) {
	$category = $_GET['category'];
	$url .= '&category='.$category;
	if(!empty($category))
	$cond['category'] = $category;
}

if(isset($_GET['classifications'])) {
	$classifications = $_GET['classifications'];
	$url .= '&classifications='.$classifications;
	if(!empty($classifications))
	$cond['classifications'] = $classifications;
}

if(isset($_GET['designer'])) {
	$designer = $_GET['designer'];
	$url .= '&designer='.$designer;
	if(!empty($designer))
	 $cond['products.designer'] = $designer;
	
}



if(isset($_GET['news']) && $_GET['news']!=0) {
	$news = $_GET['news'];
	$url .= '&set_as_news='.$news;
	if(!empty($news))
	$cond['set_as_news'] = $news;
}


if(isset($_GET['display_in_home']) && $_GET['display_in_home']!=0) {
	$display_in_home = $_GET['display_in_home'];
	$url .= '&display_in_home='.$display_in_home;
	if(!empty($display_in_home))
	$cond['display_in_home'] = 1;
}

if(isset($_GET['availability']) && $_GET['availability']!=2) {

	$availability = $_GET['availability'];
	
	$url .= '&availability='.$availability;
	if(!empty($availability) || $availability==0)
	$cond['availability'] = $availability;
}

if(isset($_GET['sales']) && $_GET['sales']!=2) {
    $sales = $_GET['sales'];
	$url .= '&sales='.$sales;
	if(!empty($sales))
	$cond['sales'] = $sales;
	unset($cond['All']);
}


if(isset($_GET['sub_category'])) {
	$sub_category = $_GET['sub_category'];
	$url .= '&sub_category='.$sub_category;
	if(!empty($sub_category))
	$cond['categories_sub.title_url'.getUrlFieldLanguage($this->lang->lang())] = $sub_category;
}

// pagination  start :
$count_news = $this->ecommerce_model->getProducts($cond);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->ecommerce_model->getProducts($cond,$config['per_page'],$page);

// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}



public function add(){
if ($this->acl->has_permission('products_review','add')){	
$data["title"]="Add products review";
$data["content"]="back_office/products_review/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('products_review','index')){	
$data["title"]="View products review";
$data["content"]="back_office/products_review/add";
$cond=array("id_products_review"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('products_review','edit')){	
$data["title"]="Edit products review";
$data["content"]="back_office/products_review/add";
$cond=array("id_products_review"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('products_review','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_products_review",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/products_review/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('products_review','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_products_review",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/products_review/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_products_review",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit products review";
$this->form_validation->set_rules("title".$lang, "TITLE", "trim|required");
$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("description", "description"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("published", "published"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("brands", "brands"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("delivery_days", "delivery_days"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("id_units", "id_units"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("availability", "availability"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("display_in_home", "display_in_home"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("promote_to_front", "promote_to_front"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("brief", "brief"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("sku", "sku"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("hide_price", "hide price"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("delivery_date", "delivery date"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("account_type", "account type"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("set_as_new", "set as new"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("miles", "miles"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("featured", "featured"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("datasheet", "datasheet"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("set_as_training", "set as training"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("set_as_clearance", "set as clearance"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("list_price", "list price"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("quantity", "quantity"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("discount", "discount"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("discount_expiration", "discount expiration"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("price", "price"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("image", "image"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("id_products", "id_products"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("discount_status", "discount status"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["description"]=$this->input->post("description");
$_data["published"]=$this->input->post("published");
$_data["id_brands"]=$this->input->post("brands"); 

$_data["delivery_days"]=$this->fct->date_in_formate($this->input->post("delivery_days"));
$_data["id_units"]=$this->input->post("id_units");
$_data["availability"]=$this->input->post("availability");
$_data["display_in_home"]=$this->input->post("display_in_home");
$_data["promote_to_front"]=$this->input->post("promote_to_front");
$_data["brief"]=$this->input->post("brief");
$_data["sku"]=$this->input->post("sku");
$_data["hide_price"]=$this->input->post("hide_price");

$_data["delivery_date"]=$this->fct->date_in_formate($this->input->post("delivery_date"));
$_data["account_type"]=$this->input->post("account_type");
$_data["set_as_new"]=$this->input->post("set_as_new");
$_data["id_miles"]=$this->input->post("miles"); 
$_data["featured"]=$this->input->post("featured");

if(!empty($_FILES["datasheet"]["name"])) {
if($this->input->post("id")!=""){
$cond_file=array("id_products_review"=>$this->input->post("id"));
$old_file=$this->fct->getonecell("products_review","datasheet",$cond_file);
if(!empty($old_file))
unlink("./uploads/products_review/".$old_file);	
}
$file1= $this->fct->uploadImage("datasheet","products_review");
$_data["datasheet"]=$file1;	
}
$_data["set_as_training"]=$this->input->post("set_as_training");
$_data["set_as_clearance"]=$this->input->post("set_as_clearance");
$_data["list_price"]=$this->input->post("list_price");
$_data["quantity"]=$this->input->post("quantity");
$_data["discount"]=$this->input->post("discount");

$_data["discount_expiration"]=$this->fct->date_in_formate($this->input->post("discount_expiration"));
$_data["price"]=$this->input->post("price");
if(!empty($_FILES["image"]["name"])) {
if($this->input->post("id")!=""){
$cond_image=array("id_products_review"=>$this->input->post("id"));
$old_image=$this->fct->getonecell("products_review","image",$cond_image);
if(!empty($old_image) && file_exists('./uploads/products_review/'.$old_image)){
unlink("./uploads/products_review/".$old_image);
 } }
$image1= $this->fct->uploadImage("image","products_review");
$_data["image"]=$image1;	
}
$_data["id_products"]=$this->input->post("id_products");
$_data["discount_status"]=$this->input->post("discount_status");

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_products_review",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/products_review/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/products_review/".$image)){
unlink("./uploads/products_review/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'products review')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/products_review/".$value."/".$image)){
unlink("./uploads/products_review/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_products_review",$id);
$this->db->update("products_review",$_data);
echo 'Done!';
}

}