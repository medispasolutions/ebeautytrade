<?php

class Messages extends BaseBackofficeController
{

    public function __construct()
    {
        parent::__construct();
        $this->table = "contactform";
    }

    //==================================================================================================================
    // helpers

    protected function checkOwner($message_id)
    {
        //check owner
        $cond = array("id" => $message_id);
        $messageData = $this->fct->getonerecord($this->table, $cond);

        $cond = ["id_brands" => $messageData["id_brands"]];
        $brandData = $this->fct->getonerecord("brands", $cond);
        if ($brandData['id_user'] != $this->acl->getCurrentUserId()) {
            return false;
        }

        return true;
    }

    /**
     * This functions prepares filters
     * @return array Current filters settings
     */
    protected function getFilters()
    {
        $searchData = [
            'trading_name' => '',
            'name' => '',
            'email' => '',
            'id_brands' => '',
            'readed' => '',
        ];

        $currentSessionData = $this->session->userdata('messages-filters');
        if (!$currentSessionData) {
            $currentSessionData = [];
        }
        $currentPostedFilters = $this->input->post('Search');
        if (!$currentPostedFilters) {
            $currentPostedFilters = [];
        }
        if ($this->input->get('resetFilters') == 1) {
            $currentPostedFilters = $searchData;
        }

        foreach ($searchData as $key => $_) {
            if (isset($currentPostedFilters[$key])) {
                $searchData[$key] = $currentPostedFilters[$key];
            } else {
                $searchData[$key] = $currentSessionData[$key];
            }
        }

        $this->session->set_userdata('messages-filters', $searchData);

        //prepare possible filters
        $possibleFilters = [];
        //categories
        $possibleFilters['status'] = [
            [
                'TEXT' => 'Read',
                'VALUE' => '1',
                'IS_SELECTED' => $searchData['readed'] == 1
            ],
            [
                'TEXT' => 'Unread',
                'VALUE' => '0',
                'IS_SELECTED' => $searchData['readed'] === "0"
            ]
        ];
        //brands
        $cond['id_user'] = $this->acl->getCurrentUserId();
        $brands = $this->fct->getAll_cond('brands','title', $cond);
        foreach ($brands as $brand) {
            $possibleFilters['brands'][] = [
                'TEXT' => $brand['title'],
                'VALUE' => $brand['id_brands'],
                'IS_SELECTED' => $searchData['id_brands'] == $brand['id_brands']
            ];
        }

        return [
            'current' => $searchData,
            'possible' => $possibleFilters
        ];
    }

    /**
     * This function searches for relevant products
     * @param $filters
     */
    protected function search($filters)
    {
        $cond = array();

        //list user brands
        //not needed a fct->getMessages does that
        /*
        $this->db->where(['id_user' => $this->acl->getCurrentUserId()]);
        $tmp = $this->db->get("brands")->result_array();
        $user_brands_ids = [];
        foreach ($tmp as $row) {
            $user_brands_ids[] = $row['id_brands'];
        }
        $cond['id_brands'] = $user_brands_ids;
        */

        if ($filters['trading_name']) {
            $cond['trading_name'] = $filters['trading_name'];
        }

        if ($filters['name']) {
            $cond['name'] = $filters['name'];
        }

        if ($filters['email']) {
            $cond['email'] = $filters['email'];
        }

        if ($filters['id_brands']) {
            $cond['brand'] = $filters['id_brands'];
        }

        if (isset($filters['readed']) && is_numeric($filters['readed'])) {
            $cond['readed'] = $filters['readed'];
        }

        $noOfElements = $this->fct->getMessages($cond);
        $elements = $this->fct->getMessages($cond, 1000, 0);

        return [$noOfElements, $elements];
    }

    //==================================================================================================================
    // suppliers functions

    public function listing()
    {
        $data = [
            'mergeConfig' => [
                'BIACONFIG' => [
                    'context' => [
                        'id' => \views\suppliersBackoffice\Contexts::MESSAGES_LISTING,
                    ],
                ],
                'seo' => [
                    'PAGE_TITLE' => 'Messages',
                ],
                'user' => [
                    'type' => $this->acl->getCurrentUserType(),
                ],
            ],
        ];

        $filters = $this->getFilters();

        $filtersForSearch = $filters['current'];
        list($noOfElements, $messages) = $this->search($filtersForSearch);

        //paginator
        $this->paginator->setTotalElements($noOfElements);
        $this->paginator->setViewData($data, '/back_office/messages/listing');

        $messages_array = array_splice(
            $messages,
            ($this->paginator->page-1)*$this->paginator->per_page,
            $this->paginator->per_page
        );
        $data['filters'] = $filters;

        $messages = [];
        foreach ($messages_array as $msg) {
            $brandName = $this->fct->getBrandNameById($msg['id_brands']);
            $messages[] = [
                'rowId' => $msg['id'],
                'classes' => ['js-mark'],
                'data' => [
                    'mark-id' => "message{$msg['id']}",
                    'mark-state' => $msg['readed'] ? 'read' : 'unread',
                    'mark-url' => '/back_office/messages/changeState/'.$msg['id'],
                ],
                'columns' => [
                    $this->fct->getUserTradingNameById($msg['id_user']),
                    $msg['name'],
                    $msg['email'],
                    $this->fct->getBrandNameById($msg['id_brands']),
                    $msg['created_date'],
                    $msg['readed']
                ]
            ];
        }

        $data['info'] = $messages;
        $this->load->twigView("messagesListingPage/index", $data);
    }

    public function changeState($message_id)
    {
        $ret = [
            'success' => true,
            'message' => '',
            'data' => [],
        ];

        if (!$this->checkOwner($message_id)) {
            $ret['success'] = false;
            $ret['message'] = "It seems you don't have permissions to edit this message";
            echo json_encode($ret);
            return;
        }

        $state = $this->input->post('state');
        $saveData = [];
        if ($state == 'read') {
            $saveData['readed'] = 1;
        } else {
            $saveData['readed'] = 0;
        }

        $this->db->where("id", $message_id);
        $this->db->update($this->table, $saveData);


        $ret['data']['state'] = $state;
        echo json_encode($ret);
        return;
    }

    public function view($message_id)
    {
        if (!$this->checkOwner($message_id)) {
            echo "You can't view this message as it's not yours";
        }

        $message = $this->fct->getonerow($this->table, ['id' => $message_id]);

        $data = [
            'message' => $message,
            'brand' => $this->fct->getonerow("brands", ['id_brands' => $message['id_brands']]),
            'user' => $this->fct->getonerow("user", ['id_user' => $message['id_user']]),
        ];

        $this->load->twigView('modalContents/message-view', $data);
    }

    public function remove($message_id)
    {
        if (!$this->checkOwner($message_id)) {
            redirect('back_office/messages');
        }

        $_data = [
            "deleted" => 1,
            "deleted_date" => date("Y-m-d h:i:s")
        ];
        $this->db->where("id", $message_id);
        $this->db->update($this->table, $_data);

        redirect(site_url("back_office/messages/"));
    }

    //==================================================================================================================
    // main (old) back office functions

    public function index()
    {
        if ($this->acl->isCurrentUserASupplier()) {
            return $this->listing();
        }

        $data["title"] = "List All Messages";
        $data["content"] = "back_office/messages/list";
        $cond = array();
        if ($this->input->get('brand') != "") {
            $cond['brand'] = $this->input->get('brand');
        }

        if ($this->input->get('user') != "") {
            $cond['user'] = $this->input->get('user');
        }

        if ($this->input->get('type') != "") {
            $cond['type'] = $this->input->get('type');
        }

        $cc = $this->fct->getMessages($cond);
        $data["info"] = $this->fct->getMessages($cond, $cc, 0);
        $this->session->set_userdata('message_link', '');
        $this->load->view("back_office/template", $data);
    }

    public function opened()
    {
        $data["title"] = "List Archived Messages";
        $data["content"] = "back_office/messages/list";
        $q = "SELECT * FROM `" . $this->table . "` WHERE deleted = 0 AND readed = 1 ORDER BY created_date DESC";
        $query = $this->db->query($q);
        $data["info"] = $query->result_array();
        $this->session->set_userdata('message_link', 'opened');
        $this->load->view("back_office/template", $data);
    }

    public function unread()
    {
        $data["title"] = "List Unread Messages";
        $data["content"] = "back_office/messages/list";
        $q = "SELECT * FROM `" . $this->table . "` WHERE deleted = 0 AND readed = 0 ORDER BY created_date DESC";
        $query = $this->db->query($q);
        $data["info"] = $query->result_array();
        $this->session->set_userdata('message_link', 'unread');
        $this->load->view("back_office/template", $data);
    }

    public function add()
    {
        $data["title"] = "Add Messages";
        $cond1 = array('name' => "news");
        $id_content = $this->fct->getonecell("content_type", "id_content", $cond1);
        $cond = array('id_content' => 16);
        $data["attr"] = $this->fct->getAll_cond("content_type_attr", "sort_order", $cond);
        $data["content"] = "back_office/news/add";
        $this->load->view("back_office/template", $data);
    }

    public function read($id)
    {
        $data["title"] = "Read Message";
        $data["content"] = "back_office/messages/add";
        $cond = array("id" => $id);
        $data["id"] = $id;
        $data["info"] = $this->fct->getonerecord($this->table, $cond);
        $_data["readed"] = 1;
        $this->db->where('id', $id);
        $this->db->update($this->table, $_data);
        $this->load->view("back_office/template", $data);
    }

    public function delete($id)
    {
        $_data = array("deleted" => 1,
            "deleted_date" => date("Y-m-d h:i:s"));
        $this->db->where("id", $id);
        $this->db->update($this->table, $_data);
        $this->session->set_userdata("success_message", "Information was deleted successfully");
        redirect(site_url("back_office/messages/" . $this->session->userdata('message_link')));
    }

    public function delete_all()
    {
        $cehcklist = $this->input->post("cehcklist");
        $check_option = $this->input->post("check_option");
        if ($check_option == "delete_all") {
            if (count($cehcklist) > 0) {
                for ($i = 0; $i < count($cehcklist); $i++) {
                    if ($cehcklist[$i] != "") {
                        $_data = array("deleted" => 1,
                            "deleted_date" => date("Y-m-d h:i:s"));
                        $this->db->where("id", $cehcklist[$i]);
                        $this->db->update($this->table, $_data);
                    }
                }
            }
            $this->session->set_userdata("success_message", "Informations were deleted successfully");
        }
        redirect(site_url("back_office/messages/" . $this->session->userdata('message_link')));
    }

    public function sorted()
    {
        $sort = array();
        foreach ($this->input->get("table-1") as $key => $val) {
            if (!empty($val))
                $sort[] = $val;
        }
        $i = 0;
        for ($i = 0; $i < count($sort); $i++) {
            $_data = array("sort_order" => $i);
            $this->db->where("id", $sort[$i]);
            $this->db->update($this->table, $_data);
        }
    }

    public function submit()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $to = $this->input->post('to');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
        $cond = array('admin_id' => 2);
        $email = $this->fct->getonecell('admin', 'email', $cond);
        $id = $this->input->post('id');
// send email 
        $this->load->library('email');
        $config = Array(
            'mailtype' => 'html'
        );
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($email);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
//$this->email->attach('./uploads/apply_career/'.$career_file_new);
        $this->email->send();
        $this->session->set_userdata("success_message", "Email was sent successfully");
        redirect(site_url("back_office/messages/read/" . $id));
    }

}
