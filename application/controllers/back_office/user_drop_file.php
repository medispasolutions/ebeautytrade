<?
class User_drop_file extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="user_drop_file";
$this->load->model("user_drop_file_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('user_drop_file','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List user drop file";
$data["content"]="back_office/user_drop_file/list";
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}
$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
$count_news = $this->user_drop_file_m->getAll($this->table,$order);
$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = site_url("back_office/user_drop_file/index/".$order);
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['uri_segment'] = 5;
$this->pagination->initialize($config);
$data['info'] = $this->user_drop_file_m->list_paginate($order,$config['per_page'],$this->uri->segment(5));
// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('user_drop_file','add')){	
$data["title"]="Add user drop file";
$data["content"]="back_office/user_drop_file/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('user_drop_file','index')){	
$data["title"]="View user drop file";
$data["content"]="back_office/user_drop_file/add";
$cond=array("id_user_drop_file"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

/*public function edit($id,$obj = ""){
if ($this->acl->has_permission('user_drop_file','edit')){	
$data["title"]="Edit user drop file";
$data["content"]="back_office/user_drop_file/add";
$cond=array("id_user_drop_file"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}*/

public function edit($id,$obj = ""){
if ($this->acl->has_permission('supplier_info','edit')){	
$data["title"]="Edit supplier info";
$data["content"]="back_office/user_drop_file/add";

$user=$this->fct->getonerecord('user',array('id_user'=>$id));
$data['info']=$user;

$cond=array("id_user"=>$id);
$data["user_drop_file"]=$this->fct->getonerecord($this->table,$cond);

if(empty($data["user_drop_file"])){
	$_data["id_user"]=$id;
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();
	$cond2=array("id_user_drop_file"=>$new_id);
	$data["user_drop_file"]=$this->fct->getonerecord($this->table,$cond2);}
    $this->load->view("back_office/template",$data);

} else {
	redirect(site_url("home/dashboard"));
}
}


public function delete($id){
if ($this->acl->has_permission('user_drop_file','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_user_drop_file",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/user_drop_file/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('user_drop_file','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_user_drop_file",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/user_drop_file/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_user_drop_file",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function checkUploadFile1(){

$size=$_FILES['file_1']['size'];
$size_arr=getMaxSize('file');
$max_size=$size_arr['size'];

	if($max_size>$size) {
		return true;
	}
	else {
		$this->form_validation->set_message('checkUploadFile1','File exceeds the maximum upload size('.$size_arr['size_mega'].' Mb).');
		return false;
	}
}

public function checkUploadFile2(){

$size=$_FILES['file_2']['size'];
$size_arr=getMaxSize('file');
$max_size=$size_arr['size'];

	if($max_size>$size) {
		return true;
	}
	else {
		$this->form_validation->set_message('checkUploadFile2','File exceeds the maximum upload size('.$size_arr['size_mega'].' Mb).');
		return false;
	}
}


public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit user drop file";


$this->form_validation->set_rules("file_1", "file 1"."(".$this->lang->lang().")", "trim|callback_checkUploadFile1[]");
$this->form_validation->set_rules("file_2", "file 2"."(".$this->lang->lang().")", "trim|callback_checkUploadFile2[]");
$this->form_validation->set_rules("id_user", "id_user"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id_user")!="")
$this->edit($this->input->post("id_user"));
else
$this->add();
}
else
{	
$user_id=$this->input->post("id_user");


if(!empty($_FILES["file_1"]["name"])) {
if($this->input->post("id")!=""){
$cond_file=array("id_user_drop_file"=>$this->input->post("id"));
$old_file=$this->fct->getonecell("user_drop_file","file_1",$cond_file);
if(!empty($old_file))
unlink("./uploads/user_drop_file/".$old_file);	
}
$file1= $this->fct->uploadImage("file_1","user_drop_file");
$_data["file_1"]=$file1;	
}



if(!empty($_FILES["file_2"]["name"])) {
if($this->input->post("id")!=""){
$cond_file=array("id_user_drop_file"=>$this->input->post("id"));
$old_file=$this->fct->getonecell("user_drop_file","file_2",$cond_file);
if(!empty($old_file))
unlink("./uploads/user_drop_file/".$old_file);	
}
$file1= $this->fct->uploadImage("file_2","user_drop_file");
$_data["file_2"]=$file1;	
}


	if($this->input->post("id")!=""){
		
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_user_drop_file",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {

	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	       redirect(site_url("back_office/user_drop_file/edit/".$user_id));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/user_drop_file/".$image)){
unlink("./uploads/user_drop_file/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'user drop file')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if(isset($res["thumb"]) && $res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/user_drop_file/".$value."/".$image)){
unlink("./uploads/user_drop_file/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_user_drop_file",$id);
$this->db->update("user_drop_file",$_data);
echo 'Done!';
}

}