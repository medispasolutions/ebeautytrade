<?
class Question_and_answer extends CI_Controller{

public function __construct()
{
parent::__construct();
$this->table="question_and_answer";
$this->load->model("question_and_answer_m");
 $this->lang->load("admin"); 
}


public function index($order=""){
$this->session->unset_userdata("admin_redirect_url");
if ($this->acl->has_permission('question_and_answer','index')){	
if($order == "")
$order ="sort_order";
$data["title"]="List question and answer";
$data["content"]="back_office/question_and_answer/list";
$url = site_url('back_office/question_and_answer?pagination=on');
$cond=array();
//
$this->session->unset_userdata("back_link");
//
if($this->input->post('show_items')){
$show_items  =  $this->input->post('show_items');
$this->session->set_userdata('show_items',$show_items);
} elseif($this->session->userdata('show_items')) {
$show_items  = $this->session->userdata('show_items'); 	}
else {
$show_items = "25";	
}

if(isset($_GET['keyword'])) {
	$keyword = $_GET['keyword'];
	$url .= '&keyword='.$keyword;
	if(!empty($keyword))
	$cond['keyword'] = $keyword;
}

if(isset($_GET['status'])) {
	$status = $_GET['status'];
	$url .= '&status='.$status;
	if(!empty($status)){
		
	$cond['status'] = $status;}
}
if(isset($_GET['name'])) {
	$name = $_GET['name'];
	$url .= '&name='.$name;
	if(!empty($name))
	$cond['name'] = $name;
}

if(isset($_GET['email'])) {
	$email = $_GET['email'];
	$url .= '&email='.$email;
	if(!empty($email))
	$cond['email'] = $email;
}

$this->session->set_userdata('back_link','index/'.$order.'/'.$this->uri->segment(5));
$data["show_items"] = $show_items;
// pagination  start :
// pagination  start :
$count_news =$this->pages_fct->getQuestionsAndAnswers($cond);

$show_items = ($show_items == 'All') ? $count_news : $show_items;
$this->load->library('pagination');
$config['base_url'] = $url;
$config['num_links'] = '8';
$config['total_rows'] = $count_news;
$config['per_page'] = $show_items;
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = TRUE;
//print '<pre>';print_r($config);exit;
$this->pagination->initialize($config);
if(isset($_GET['per_page'])) {
	if($_GET['per_page'] != '') $page = $_GET['per_page'];
	else $page = 0;
}
else $page = 0;
$data['info'] = $this->pages_fct->getQuestionsAndAnswers($cond,$config['per_page'],$page);


// end pagination .
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}


public function add(){
if ($this->acl->has_permission('question_and_answer','add')){	
$data["title"]="Add question and answer";
$data["content"]="back_office/question_and_answer/add";
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
} 

public function view($id,$obj = ""){
if ($this->acl->has_permission('question_and_answer','index')){	
$data["title"]="View question and answer";
$data["content"]="back_office/question_and_answer/add";
$cond=array("id_question_and_answer"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function edit($id,$obj = ""){
if ($this->acl->has_permission('question_and_answer','edit')){	
$data["title"]="Edit question and answer";
$data["content"]="back_office/question_and_answer/add";
$cond=array("id_question_and_answer"=>$id);
$data["id"]=$id;
$data["info"]=$this->fct->getonerecord($this->table,$cond);

$_data['read'] = 1;
$cond['id_question_and_answer']=$id;
$this->db->where($cond);
$this->db->update('question_and_answer',$_data);
$this->load->view("back_office/template",$data);
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete($id){
if ($this->acl->has_permission('question_and_answer','delete')){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_question_and_answer",$id);
$this->db->update($this->table,$_data);
$this->session->set_userdata("success_message","Information was deleted successfully");
redirect(site_url("back_office/question_and_answer/".$this->session->userdata("back_link")));
} else {
	redirect(site_url("home/dashboard"));
}
}

public function delete_all(){
if ($this->acl->has_permission('question_and_answer','delete_all')){
$cehcklist= $this->input->post("cehcklist");
$check_option= $this->input->post("check_option");
if($check_option == "delete_all"){
if(count($cehcklist) > 0){
for($i = 0; $i < count($cehcklist); $i++){
if($cehcklist[$i] != ""){
$_data=array("deleted"=>1,
"deleted_date"=>date("Y-m-d h:i:s"));
$this->db->where("id_question_and_answer",$cehcklist[$i]);
$this->db->update($this->table,$_data);	
}
} } 
$this->session->set_userdata("success_message","Informations were deleted successfully");
}
redirect(site_url("back_office/question_and_answer/".$this->session->userdata("back_link")));	
} else {
	redirect(site_url("home/dashboard"));
}
}

public function sorted(){
$sort=array();
foreach($this->input->get("table-1") as $key => $val){
if(!empty($val))
$sort[]=$val;	
}
$i=0;
for($i=0; $i<count($sort); $i++){
$_data=array("sort_order"=>$i+1);
$this->db->where("id_question_and_answer",$sort[$i]);
$this->db->update($this->table,$_data);	
}
}

public function submit(){
$lang = "";
if($this->config->item("language_module")) {
	$lang = getFieldLanguage($this->lang->lang());
}
$data["title"]="Add / Edit question and answer";

$this->form_validation->set_rules("meta_title".$lang, "PAGE TITLE", "trim|max_length[65]");
$this->form_validation->set_rules("title_url".$lang, "TITLE URL", "trim");
$this->form_validation->set_rules("meta_description".$lang, "META DESCRIPTION", "trim|max_length[160]");
$this->form_validation->set_rules("meta_keywords".$lang, "META KEYWORDS", "trim|max_length[160]");
$this->form_validation->set_rules("phone", "phone"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("email", "email"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("question", "question"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("name", "name"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("answer", "answer"."(".$this->lang->lang().")", "trim");
$this->form_validation->set_rules("reply", "reply"."(".$this->lang->lang().")", "trim");
if ($this->form_validation->run() == FALSE){
if($this->input->post("id")!="")
$this->edit($this->input->post("id"));
else
$this->add();
}
else
{	
$_data["id_user"]=$this->session->userdata("uid");
$_data["title".$lang]=$this->input->post("title".$lang);
$_data["meta_title".$lang]=$this->input->post("meta_title".$lang);
if($this->input->post("title_url".$lang) == "")
$title_url = $this->input->post("title".$lang);
else
$title_url = $this->input->post("title_url".$lang);
if($this->input->post("lang") == "ar") {
	$this->load->model("title_url_ar");
	$_data["title_url".$lang] = $this->title_url_ar->cleanURL($this->table,$title_url,$this->input->post("id"),"title_url".$lang);
}
else {
$_data["title_url".$lang]=$this->fct->cleanURL($this->table,url_title($title_url),$this->input->post("id"));
}
$_data["meta_description".$lang]=$this->input->post("meta_description".$lang);
$_data["meta_keywords".$lang]=$this->input->post("meta_keywords".$lang);	
$_data["phone"]=$this->input->post("phone");
$_data["email"]=$this->input->post("email");
$_data["question"]=$this->input->post("question");
$_data["name"]=$this->input->post("name");
$_data["answer"]=$this->input->post("answer");
$_data["reply"]=$this->input->post("reply");
$_data['status']=$this->input->post("status");


if(isset($_POST['inform_client']) && $_POST['inform_client'] == 1) {
		$this->load->model("send_emails");
		$this->send_emails->question_inform_client($_data);
	}

	if($this->input->post("id")!=""){
	$_data["updated_date"]=date("Y-m-d h:i:s");
	$this->db->where("id_question_and_answer",$this->input->post("id"));
	$this->db->update($this->table,$_data);
	$new_id = $this->input->post("id");
	$this->session->set_userdata("success_message","Information was updated successfully");
	} else {
	$_data["created_date"]=date("Y-m-d h:i:s");
	$this->db->insert($this->table,$_data); 
	$new_id = $this->db->insert_id();	
	$this->session->set_userdata("success_message","Information was inserted successfully");
	}
	if($this->session->userdata("admin_redirect_url")) {
		redirect($this->session->userdata("admin_redirect_url"));
	}
	else {
   	    redirect(site_url("back_office/question_and_answer/".$this->session->userdata("back_link")));
	}
	
}
	
}

public function delete_file(){
$field = $this->input->post('field');
$image = $this->input->post('image');
$id = $this->input->post('id');
if(file_exists("./uploads/question_and_answer/".$image)){
unlink("./uploads/question_and_answer/".$image); }
$q=" SELECT thumb,thumb_val
FROM `content_type_attr`
WHERE id_content = (SELECT id_content FROM `content_type` WHERE name = 'question and answer')
AND name = '".$field."'";
$query=$this->db->query($q);
$res=$query->row_array();
if($res["thumb"] == 1){
$sumb_val1=explode(",",$res["thumb_val"]);
foreach($sumb_val1 as $key => $value){
if(file_exists("./uploads/question_and_answer/".$value."/".$image)){
unlink("./uploads/question_and_answer/".$value."/".$image);	 }								
} } 
$_data[$field]="";
$this->db->where("id_question_and_answer",$id);
$this->db->update("question_and_answer",$_data);
echo 'Done!';
}

}