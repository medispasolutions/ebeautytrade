<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Medical extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}
    public function index()
    {
        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        //$this->template->write_view('content', 'blocks/breadcrumbs', $data);
        $this->template->write_view('content', 'content/Medical', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
    }
}