<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Controller for devs
 *
 * Class Dev
 */
class Dev extends BaseController
{

    /** @var dev_mail_log $dev_mail_log*/
    public $dev_mail_log;

    protected $validIps = [
        '127.0.0.1', //localhost
        '89.76.108.36', //PW - home
        '79.188.184.233', //BI - office
    ];

    public function __construct()
    {
        parent::__construct();

        //look for a valid IP
        if (!in_array($_SERVER['REMOTE_ADDR'], $this->validIps)) {
            die('BAD IP: '.$_SERVER['REMOTE_ADDR']);
        }

        $this->template->set_master_template('dev/template.php');
    }

    public function index()
    {
        $this->template->render();
    }

    public function maillog($action = null, $id = null)
    {
        $this->load->model('dev/dev_mail_log');
        $data['log'] = $this->dev_mail_log->get();

        switch ($action) {
            case "getMailContent":
                echo $data['log'][$id]['message'];
                return;
            case "clear":
                $this->dev_mail_log->clear();
                redirect(site_url('dev/maillog'));
                return;
        }


        $this->template->write_view('content', 'dev/mail_log', $data);
        $this->template->render();
    }

}