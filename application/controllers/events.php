<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Events extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        //this page is only for main admins
        if (!$this->acl->isCurrentUserAAdmin()) {
            redirect(site_url());
        }


        ////TRACING//////
        $insert_log['section'] = 'events';
        $this->fct->insertNewLog($insert_log);
        $url = route_to('events') . '?pagination=on';
        $data['new_url'] = $url;
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 26));
        $data['page_title'] = $data['seo']['title' . getFieldLanguage()];
        $data['branches'] = $this->fct->getAll_1("branches", "id_branches");
        $data['settings'] = $this->fct->getonerow('settings', array('id_settings' => 1));
        $cond = array();


////////////////////////////////CONTIDIONS//////////////		
        $sort_order = "date";
        $desc = "asc";
        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            $url .= '&sort=' . $sort;
            if ($sort == "country") {
                $sort_order = "date";
                $data['list_as_country'] = "as_country";
                $cond['list_as_country'] = "country";
            }
            if ($sort == "name") {
                $sort_order = "title";
                $desc = "asc";
            }

            if ($sort == "date") {
                $sort_order = "date";
                $desc = "asc";
            }
        }

        $show_items = 21;


        $count = $this->pages_fct->getEvents($cond);


        $data['show_items'] = $show_items;
        $show_items = ($show_items == 'All') ? $count_news : $show_items;
        $cc = $count;
        $config['base_url'] = $url;
        $config['total_rows'] = $cc;
        $config['num_links'] = '8';
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;

        $config['per_page'] = $show_items;

        $this->pagination->initialize($config);
        if ($this->input->get('per_page')) {
            if ($this->input->get('per_page') != "") {
                $page = $this->input->get('per_page');
            } else {
                $page = 0;
            }
        } else {
            $page = 0;
        }
        $data['count'] = $cc;
        $data['per_page'] = $page;
        $num_page_prev = 1;
        if ($page > 0) {
            $num_page = intval($page / $show_items);
        } else {
            $num_page = 0;
        }

        $data['events'] = $this->pages_fct->getEvents($cond, $show_items, $page, $sort_order, $desc);


        /////////////////////////////////ADVERTISEMENTS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        /*	    $page_ads="directory_events";
                $block_ads="pages_top";
                $category_ads="";
                $id_user_ads="";
                $type="image";

                $data['advertisements'] = $this->custom_fct->getBannerAds($page_ads,$type,$block_ads,$category_ads,$id_user_ads);*/

        $page_ads = "directory";
        $block_ads = "directory_left";
        $category_ads = "";
        $id_user_ads = "";
        $type = "image";

        $data['home_bottom_ads_left'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads, $category_ads,
            $id_user_ads);

        $page_ads = "directory";
        $block_ads = "directory_right";
        $category_ads = "";
        $id_user_ads = "";
        $type = "image";

        $data['home_bottom_ads_right'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads, $category_ads,
            $id_user_ads);


        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'blocks/breadcrumbs', $data);

        $this->template->write_view('content', 'content/events', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);
        $this->template->render();
    }


}