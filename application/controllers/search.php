<? class Search extends CI_Controller
{
    public function index()
    {
        $url = route_to('search') . '?pagination=on';
        $data['info'] = $this->fct->getonerow('settings', array('id_settings' => 1));
        if ($this->input->get("keywords") != "") {

            $keywords = $this->input->get("keywords");
            $url .= '&keywords=' . $keywords;
            $keywords = urldecode($keywords);
            $data['seo'] = $this->fct->getonerecord('static_seo_pages', array('id_static_seo_pages' => 1));
            $data['page_title'] = 'Search result : "' . $keywords . '"';
            $data['seo']['meta_title'] = $data['page_title'];
            $data['keywords'] = $keywords;

            $this->load->library('pagination');

            $cc = $this->custom_fct->searchWebsite($keywords);
            $config['base_url'] = $url;
            $config['total_rows'] = $cc;
            $config['num_links'] = '8';
            $config['per_page'] = 30;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $this->pagination->initialize($config);
            if (isset($_GET['per_page'])) {
                if ($_GET['per_page'] != '') $page = $_GET['per_page'];
                else $page = 0;
            } else $page = 0;
            $data['count'] = $cc;
            $data['results'] = $this->custom_fct->searchWebsite($keywords, $config['per_page'], $page);


            /////////////////////////////////BreadCrumbes//////////////////////
            $breadcrumbs = ' <h1 class="h1_title">Search Results - ' . $keywords . '</h1>';
            $breadcrumbs .= '<ul>';
            $breadcrumbs .= '<li><a href="' . site_url("home") . '">Home</a></li>';
            $breadcrumbs .= '<li class="divider">/</li>';
            $breadcrumbs .= '<li>Search Results</li>';
            $breadcrumbs .= '</ul>';

            $data['breadcrumbs'] = $breadcrumbs;

            $data['banners'] = array();

            $this->template->write_view('header', 'blocks/header', $data);
            $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
            $this->template->write_view('content', 'content/search_results', $data);
            $this->template->write_view('footer', 'blocks/footer', $data);
            $this->template->render();
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /****AutoComplete***************************************************************************************/
    public function search_by_title()
    {
        /*		$keyword = $this->input->post("term");
                $type = $this->input->post("type");
                $results=$this->units_m->searchByTitle($keyword,$type,FALSE);
        
                */
        $keywords = $this->input->post("term");

        $keywords = urldecode($keywords);


        $results = $this->custom_fct->searchWebsite($keywords, 100, 0);


        $find_section = array("'", "_", '"');
        $replace_section = array("", " " . "");
        $cc = count($results);
        $i = 0;
        echo '[';
        foreach ($results as $result) {

            $i++;
            if ($i == $cc) $coma = '';
            else $coma = ',';
            //$res = preg_replace('/\.[^.]+$/','',$result['title']);
            //$res =str_replace(".jpg","",$result['image']);
            /*echo '{"label" : "'.ucfirst(str_replace($find_section,$replace_section,$result['section'])).': '.str_replace("'","",$result['title']).'","section":"'.str_replace("'","",$result['section']).'","id":"'.str_replace("'","",$result['id']).'"}'.$coma;*/

            echo '{"label" : "' . str_replace($find_section, $replace_section, $result['title']) . '"}' . $coma;

            //echo '"'.str_replace("'","",$result['title']).'"'.$coma;
        }
        echo ']';
    }
}