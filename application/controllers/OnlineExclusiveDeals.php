<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OnlineExclusiveDeals extends BaseShopController {
	public function __construct()
{
	  parent::__construct();
}
    public function index()
    {
        
        $account_status = $this->ecommerce_model->updateGroupCategoryProducts();
        if (checkUserIfLogin()) {
            $login = true;
        } else {
            $login = false;
        }
        $data['login'] = $login;
        if (empty($title_url)) {
            $url = route_to('products') . '?pagination=on';
        } else {
            $url = route_to('products/index/' . $title_url) . '?pagination=on';
        }

        $data['current_url'] = $url;
        $data['title'] = 'Products';
        $data['page'] = "";
        $cond['products.status'] = 0;
        $sort_order = 'products.sku';
        $dir = "";

        if ($this->input->get('show_items')) {
            $show_items = $this->input->get('show_items');
        } else {
            $show_items = "40";
        }

        $show_items = "40";

        if ($title_url == "new-arrivals") {
            $cond['set_as_news'] = 1;
            $data['title'] = 'New Arrivals';
            $data['page'] = "new-arrivals";
            $id_seo = 18;
        }

        if ($title_url == "offers") {
            $cond['offers_month'] = 1;
            $data['title'] = 'Offers of The Month';
            $data['page'] = "offers";
            $id_seo = 19;
        }

        if ($title_url == "clearance") {
            $cond['clearance'] = 1;
            $data['title'] = 'Clearance';
            $data['page'] = "clearance";
            $id_seo = 20;
        }

        if ($title_url == "miles") {
            $cond['miles'] = 1;
            $data['title'] = 'miles';
            $data['page'] = "miles";
            $sort_order = 'products.sku';
            $id_seo = 21;
        }

        if ($this->input->get('sort')) {
            $sort_order = $this->ecommerce_model->getSortOrder($this->input->get('sort'));
        }


        if (isset($id_seo) && !empty($id_seo)) {
            $data['seo'] = $this->fct->getonerow('static_seo_pages', array(
                'id_static_seo_pages' => $id_seo
            ));
        }
        
        $cond['set_as_oed'] = 1;
        
        $count = $this->ecommerce_model->getProducts($cond);

        $data['show_items'] = $show_items;
        $show_items = ($show_items == 'All') ? $count_news : $show_items;
        $cc = $count;
        $config['base_url'] = $url;
        $config['total_rows'] = $cc;
        $config['num_links'] = '8';
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['per_page'] = $show_items;

        $this->pagination->initialize($config);
        if ($this->input->get('per_page')) {
            if ($this->input->get('per_page') != "")
                $page = $this->input->get('per_page');
            else
                $page = 0;
        } else
            $page = 0;
        $data['count'] = $cc;
        $data['per_page'] = $page;
        $num_page_prev = 1;
        if ($page > 0) {
            $num_page = intval($page / $show_items);
        } else {
            $num_page = 0;
        }

        $data['products'] = $this->ecommerce_model->getProducts($cond, $show_items, $page, $sort_order, $dir);


        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'blocks/breadcrumbs', $data);
        $this->template->write_view('content', 'content/products', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);
        $this->template->render();
    }
}