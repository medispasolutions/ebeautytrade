<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class size_guide extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}

	public function index()
	{redirect(site_url());}
	
	public function woman()
	{
		
		if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		$data['seo']['meta_title'] ="Women's size guide";
		$data['seo']['page_title'] ="Women's size guide";
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>1));
		$this->template->add_css('front/css/size_guide.css');

		$this->template->write_view('content','size_guide/woman',$data);

		$this->template->render();
	}
	
	public function man()
	{
		
		if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		$data['seo'] = $this->fct->getonerow('static_seo_pages',array('id_static_seo_pages'=>1));
		$data['seo']['meta_title'] ="Mens's size guide";
		$data['seo']['page_title'] ="Mens's size guide";
		$this->template->add_css('front/css/size_guide.css');

		$this->template->write_view('content','size_guide/man',$data);

		$this->template->render();
	}
	
	public function unisex()
	{
		
		if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		$data['seo']['meta_title'] ="Unisex size guide";
		$data['seo']['page_title'] ="Unisex size guide";
		$this->template->add_css('front/css/size_guide.css');

		$this->template->write_view('content','size_guide/unisex',$data);

		$this->template->render();
	}
	
	public function lifestyle()
	{
		
		if($this->session->userdata('currency') == "") {
			$this->session->set_userdata('currency',$this->config->item("default_currency"));
		}
		$data['seo']['meta_title'] ="Lifestyle size guide";
		$data['seo']['page_title'] ="Lifestyle size guide";
		$this->template->add_css('front/css/size_guide.css');

		$this->template->write_view('content','size_guide/lifestyle',$data);

		$this->template->render();
	}
}