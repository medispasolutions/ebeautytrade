<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends BaseShopController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function emails()
    {
        $data = $this->fct->getonerecord('user', array('id_user' => 324));
        $this->load->model('send_emails');
        $this->send_emails->sendUserReply($data);
        $this->send_emails->sendUserToAdmin($data);
    }

    public function index()
    {
        //$this->cart->destroy();exit;
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 11));
        $data['page_title'] = $data['seo']['meta_title'];
        // breadcrumbs
        $breadcrumbs = array();
        $breadcrumbs[0]['title'] = lang('home');
        $breadcrumbs[0]['link'] = site_url();
        $breadcrumbs[1]['title'] = $data['page_title'];
        $crumbs['breadcrumbs'] = $breadcrumbs;
        // end breadcrumbs

        $cart_items = $this->cart->contents();


        if (empty($cart_items))
            $cart_items = $this->ecommerce_model->loadUserCart();

        $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);


        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'cart/cart', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);
        $this->template->render();
    }


    public function buy_verification()
    {

// echo $system_email = $this->fct->admin_email();
// $adminstrator_email = $this->fct->get_related_emails(2);
// echo $from = join(',',$adminstrator_email);
// echo "<pre>";
// print_r($adminstrator_email);
// exit;


        if (isset($_GET['vpc_TxnResponseCode'])) {
            $this->load->helper('audi_helper');
            //$SECURE_SECRET = "2F93B702D722AE819AC549582F0D3890";
            $SECURE_SECRET = $this->config->item("SecureHashSecret");

            $vpc_Txn_Secure_Hash = addslashes($_GET["vpc_SecureHash"]);
            //echo "<pre>";print_r($_GET);exit;
            unset($_GET["vpc_SecureHash"]);
            ksort($_GET);
            // set a flag to indicate if hash has been validated
            $errorExists = false;
            //check if the value of response code is valid
            if (strlen($SECURE_SECRET) > 0 && addslashes($_GET["vpc_TxnResponseCode"]) != "7" && addslashes($_GET["vpc_TxnResponseCode"]) != "No Value Returned") {
                //creat an md5 variable to be compared with the passed transaction secure hash to check if url has been tampered with or not
                $md5HashData = $SECURE_SECRET;

                //creat an md5 variable to be compared with the passed transaction secure hash to check if url has been tampered with or not
                $md5HashData_2 = $SECURE_SECRET;

                // sort all the incoming vpc response fields and leave out any with no value
                foreach ($_GET as $key => $value) {
                    if ($key != "vpc_SecureHash" && strlen($value) > 0 && $key != 'action') {
                        $hash_value = str_replace(" ", '+', $value);
                        $md5HashData_2 .= $hash_value;
                        $md5HashData .= $value;

                    }
                }

                //if transaction secure hash is the same as the md5 variable created
                if ((strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData)) || strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData_2)))) {
                    $hashValidated = "<b>CORRECT</b>";
                } else {
                    $hashValidated = "<b>INVALID HASH</b>";
                    $errorExists = true;
                }
            } else {
                $hashValidated = "<FONT color='orange'><b>Not Calculated - No 'SECURE_SECRET' present.</b></FONT>";
            }
            //the the fields passed from the url to be displayed
            $amount = null2unknown(addslashes($_GET["amount"]) / 100);
            $locale = null2unknown(addslashes($_GET["vpc_Locale"]));
            $batchNo = null2unknown(addslashes($_GET["vpc_BatchNo"]));
            $command = null2unknown(addslashes($_GET["vpc_Command"]));
            $message = null2unknown(addslashes($_GET["vpc_Message"]));
            $version = null2unknown(addslashes($_GET["vpc_Version"]));
            $cardType = null2unknown(addslashes($_GET["vpc_Card"]));
            $orderInfo = null2unknown(addslashes($_GET["orderInfo"]));
            $receiptNo = null2unknown(addslashes($_GET["vpc_ReceiptNo"]));
            $merchantID = null2unknown(addslashes($_GET["merchant"]));
            $authorizeID = null2unknown(addslashes($_GET["vpc_AuthorizeId"]));
            $merchTxnRef = null2unknown(addslashes($_GET["merchTxnRef"]));
            $transactionNo = null2unknown(addslashes($_GET["vpc_TransactionNo"]));
            $acqResponseCode = null2unknown(addslashes($_GET["vpc_AcqResponseCode"]));
            $txnResponseCode = null2unknown(addslashes($_GET["vpc_TxnResponseCode"]));


            // Show 'Error' in title if an error condition
            $errorTxt = "";


            // Show this page as an error page if vpc_TxnResponseCode equals '7'
            if ($txnResponseCode == "7" || $txnResponseCode == "No Value Returned" || $errorExists) {
                $errorTxt = "Error ";
            }

            //echo "<pre>";
// print_r($_GET);exit;
// echo "Error: ".$errorTxt;
// echo "<br />";
// echo "hashValidated:".$hashValidated;
// echo "<br />";
// echo $message;
// echo "<br />errorExists: ";
// echo $errorExists;
// echo "<br />txnResponseCode: ";
// echo $txnResponseCode;
// echo "<br />errorTxt: ";
// echo $errorTxt;


            if ($errorExists == true || $txnResponseCode != "0" || $txnResponseCode != 0 || $errorTxt != "") {
                if ($txnResponseCode == 0) $errorCode = 2;
                else $errorCode = $txnResponseCode;
                $this->session->set_flashdata('error_message', getResponseDescription($errorCode));
                $cond['id_orders'] = $merchTxnRef;
                $this->db->delete('orders', $cond);
                redirect(route_to('cart/checkout'));

            } else {

                $data_array = array(
                    'receiptNo' => $receiptNo,
                    //'status' => 'paid',
                    // 'payment_date' => date('Y-m-d h:i:s'),
                    'transactionNo' => $transactionNo,
                    'authorizeID' => $authorizeID,
                    'batchNo' => $batchNo,
                    'cardType' => $cardType,
                    'orderInfo' => $orderInfo,
                    'amount' => $amount,
                    'validate_status' => $txnResponseCode,
                    'created_date' => date("Y-m-d h:i:s")
                );
                if ($receiptNo == "No Value Returned" || $txnResponseCode != "0" || $txnResponseCode != 0) {
                    $data_array['status'] = 'ERROR';
                } else {
                    $data_array['status'] = 'paid';
                    $data_array['payment_date'] = date('Y-m-d h:i:s');
                }
                $this->db->where('id_orders', $merchTxnRef);
                $this->db->update('orders', $data_array);


                $orderData = $this->fct->getonerecord('orders', array('id_orders' => $merchTxnRef));
                $delivery = $this->fct->getonerecord('address_book', array('id_address_book' => $orderData['delivery_id']));
                /* $this->session->set_userdata("success_message",getResponseDescription($txnResponseCode));*/
                // send email to the client and Admin that the Transaction Successful.
                $this->load->model('send_emails');
                $this->send_emails->sendOrderToAdmin($orderData, $delivery);
                $this->send_emails->sendOrderToUser($merchTxnRef, $delivery);
                $this->cart->destroy();
                // success redirect
                $this->session->set_flashdata('success_message', lang('order_completed') . $merchTxnRef . '. One of our sales persons will review your order and contact you soon.');
                $return['result'] = 1;
                // echo 'wosil la honi';exit;
                redirect(route_to('user/orders/' . $merchTxnRef));
                $return['message'] = "System is redirecting to page.";
            }
            exit;
        }

    }

    public function print_c()
    {
        $cart_items = $this->cart->contents();
        $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);

        $this->load->view('cart/print_cart', $data);
    }

    /************************************************************************************************/
    public function request_quote()
    {
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 5));
        $data['page_title'] = $data['seo']['title' . getFieldLanguage()];

        $data['settings'] = $this->fct->getonerow('settings', array('id_settings' => 1));

        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('content', 'content/request_quote', $data);
        $this->template->write_view('footer', 'blocks/popup_r', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
    }

    function validate_captcha()
    {
        $row_count = 1;
        if (!isset($_POST['no_captcha'])) {
            $expiration = time() - 7200; // Two hour limit
            $this->db->query("DELETE FROM captcha WHERE captcha_time < " . $expiration);
            // Then see if a captcha exists:
            $sql = "SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?";
            $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
            $query = $this->db->query($sql, $binds);
            $row = $query->row();
            $row_count = $row->count;
            if ($row_count == 0) {
                $this->form_validation->set_message('validate_captcha', lang('notcorrectcharacters'));
                return false;
            } else {
                return true;
            }
        }
    }

    public function sendRequest()
    {
        //print '<pre>';
        //print_r($_POST);exit;
        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('name', lang('name'), 'trim|required');
        $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email');
        $this->form_validation->set_rules('message', lang('message'), 'trim');
        //$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {
            $_data['name'] = $this->input->post('name');
            $_data['email'] = $this->input->post('email');
            $_data['message'] = $this->input->post('message');
            $_data['msg_fr'] = $this->input->post('msg_fr');
            $_data['created_date'] = date('Y-m-d h:i:s');
            /*			$_data['lang'] = $this->lang->lang();
			$this->db->insert('contactform',$_data);*/
            // send emails
            $this->load->model('send_emails');
            $this->send_emails->sendRequestQuoteToUser($_data);

            $return['result'] = 1;
            $return['message'] = lang('request_sent');
        }
        echo json_encode($return);
    }

    /************************************************************************************************/
    function remove($rid, $redirect = "TRUE")
    {
        $dataTmp = $this->cart->contents();
        $flag = FALSE;
        foreach ($dataTmp as $item) {
            if ($item['rowid'] == $rid) {
                $data = array(
                    'rowid' => $item['rowid'],
                    'qty' => 0
                );
                $flag = TRUE;
                // update in database: stock
                $product = $this->fct->getonerow('products', array('id_products' => $item['id']));
                $inhouse_quantity = $product['quantity'];

                if (isset($item['options']['product_options']) && !empty($item['options']['product_options'])) {
                    $options = $item['options']['product_options'];
                    //$ids  = unSerializeStock(implode(',',$options));
                    //$ids = arrangeStockIds($ids);

                    $results = $this->ecommerce_model->getOptionsByIds($options);
                    $combination = serializecartOptions($results);

                    $stock = $this->fct->getonerow('products_stock', array('id_products' => $item['id'], 'combination' => $combination));
                    if (!empty($stock)) {
                        $inhouse_quantity = $stock['quantity'];
                    }
                }
                $comb = '';

                if (isset($combination)) $comb = $combination;
                //echo $inhouse_quantity;exit;
                /*			$this->ecommerce_model->UpdateProductStockInDatabase($product['id_products'],$inhouse_quantity,$item['qty'],0,$comb);*/
                // end update in database: stock
                // delete from cart

                $this->cart->update($data);
                // delete from database: shopping cart
                if ($redirect == "TRUE") {
                    $this->db->where('rowid', $item['rowid']);
                    $this->db->delete('shopping_cart');
                }
            }
        }
        if ($redirect == "TRUE") {
            if ($flag) {
                $message = $product['title'] . " " . lang('cart_item_removed');
                //$this->session->set_flashdata('success_message',$message);
            } else {
                $message = lang('cart_item_removed_error');
                //$this->session->set_flashdata('error_message',$message);
            }

            /*	$url = $_SERVER['HTTP_REFERER'];
	redirect($url);*/

            $return['result'] = 1;
            $return['return_popup_message'] = $message;

            $user = $this->ecommerce_model->getUserInfo();
            $cartPrices = $this->ecommerce_model->getCheckoutAttr();
            $user_miles = $user['miles'] - $cartPrices['total_redeem_miles'];
            if ($user_miles > 0) {
                $available_miles = " (" . $user_miles . ")";
            } else {
                $available_miles = " (0)";
            }
            $return['remaining_miles'] = $available_miles;

            $data['section'] = 'cart';
            $return['cart'] = $this->load->view('blocks/cart', $data, true);
            $return['total_price_cont'] = $this->load->view('blocks/total_price_cont', $data, true);
            $return['count_items'] = $this->cart->total_items();
            die(json_encode($return));

        }
    }

    /************************************************************************************************/
    function remove_products($rid)
    {
        $dataTmp = $this->cart->contents();
        $flag = FALSE;
        foreach ($dataTmp as $item) {
            if ($item['rowid'] == $rid) {
                $data = array(
                    'rowid' => $item['rowid'],
                    'qty' => 0
                );
                $flag = TRUE;
                // update in database: stock
                $product = $this->fct->getonerow('products', array('id_products' => $item['id']));
                $inhouse_quantity = $product['quantity'];
                if (isset($item['options']) && !empty($item['options'])) {
                    $options = $item['options'];
                    //$ids  = unSerializeStock(implode(',',$options));
                    //$ids = arrangeStockIds($ids);
                    $results = $this->ecommerce_model->getOptionsByIds($options);
                    $combination = serializecartOptions($results);

                    $stock = $this->fct->getonerow('products_stock', array('id_products' => $item['id'], 'combination' => $combination));
                    if (!empty($stock)) {
                        $inhouse_quantity = $stock['quantity'];
                    }
                }
                $comb = '';
                if (isset($combination)) $comb = $combination;
                //echo $inhouse_quantity;exit;
                /*			$this->ecommerce_model->UpdateProductStockInDatabase($product['id_products'],$inhouse_quantity,$item['qty'],0,$comb);*/
                // end update in database: stock
                // delete from cart
                $this->cart->update($data);
                // delete from database: shopping cart
                $this->db->where('rowid', $item['rowid']);
                $this->db->delete('shopping_cart');
            }
        }
        if ($flag) {
            $this->session->set_flashdata('success_message', lang('cart_item_removed'));
        } else {
            $this->session->set_flashdata('error_message', lang('cart_item_removed_error'));
        }
        return true;
    }

    /************************************************************************************************/

    function add()
    {
        $login = checkUserIfLogin();
        if ($login) {
            $post['product_id'] = $this->input->post('product_id');
            $post['redeem'] = $this->input->post('redeem');
            if ($this->input->post('options')) {
                $post['options'] = $this->input->post('options');
            }
            if ($this->input->post('qty')) {
                $post['qty'] = $this->input->post('qty');
            }
            $this->ecommerce_model->add_to_cart($post);
        } else {
            $actual_link = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

            $return['result'] = 0;
            $return['login'] = 1;
            die(json_encode($return));
        }
    }


    function sendInquiry($key)
    {
        if ($this->session->userdata('product_stock_inquiry') != "") {
            $arr = $this->session->userdata('product_stock_inquiry');

            $data = $arr[$key];
            $user = $this->ecommerce_model->getUserInfo();
            if (!empty($data['options']['product_options'])) {
                $results = $this->ecommerce_model->getOptionsByIds($data['options']['product_options']);
                $combination = serializecartOptions($results);
            } else {
                $combination = "";
            }
            $insert['options_en'] = $combination;
            $insert['id_user'] = $user['id_user'];
            $insert['quantity'] = $data['qty'];
            $insert['id_products'] = $data['id_products'];
            $this->db->insert('product_inquiry', $insert);
            $new_id = $this->db->insert_id();

            $this->load->model('send_emails');
            $this->send_emails->sendProductsInquiry($new_id);
            $message = 'Your inquiry is received and we will contact you shortly, thank you!';
            if ($this->input->post('ajax') != "") {
                $return['return_popup_message'] = $message;
                $return['result'] = 1;
            } else {
                $this->session->set_flashdata('success_message', $message);
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            $message = lang('access_denied');
            $this->session->set_flashdata('error_message', $message);
            if ($this->input->post('ajax') != "") {
                $return['redirect_link'] = $_SERVER['HTTP_REFERER'];
                $return['result'] = 0;
            } else {

                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    /************************************************************************************************/
    function update()
    {

        if ($this->input->post('rowid')) {
            // load cart content after days
            $cart_items = $this->cart->contents();


            if (empty($cart_items))
                $cart_items = $this->ecommerce_model->loadUserCart();
            //print '<pre>';print_r($_POST);exit;
            $lang = 'en';
            $id = $this->input->post('product_id');
            $rowid = $this->input->post('rowid');
            $stock_text = '';
            $product = $this->fct->getonerow('products', array('id_products' => $id));
            $general_miles = $product['general_miles'];
            $set_general_miles = $product['set_general_miles'];

            /*print '<pre>';print_r($product);exit;*/

            $list_price = $product['list_price'];

            $discount_expiration = $product['discount_expiration'];
            $price = $product['price'];
            $inhouse_quantity = $product['quantity'];
            /*	echo $inhouse_quantity ;exit; */

            $options = array();
            if (isset($_POST['options'])) {

                $options = $this->input->post('options');
                $results = $this->ecommerce_model->getOptionsByIds($options);
                $combination = serializecartOptions($results);
                //echo $combination;exit;
                $stock = $this->fct->getonerow('products_stock', array('id_products' => $id, 'combination' => $combination));
                $stock = "";
                if (!empty($stock)) {
                    $list_price = $stock['list_price'];
                    $general_miles = $stock['general_miles'];
                    $set_general_miles = $stock['set_general_miles'];
                    $discount_expiration = $stock['discount_expiration'];
                    $price = $stock['price'];

                    $inhouse_quantity = $stock['quantity'];
                    $combination_arr = unSerializeStock($stock['combination']);
                    $i = 0;
                    foreach ($combination_arr as $ar) {
                        $stock_text .= $results[$i]['title' . getFieldLanguage($lang)] . ' ';
                        $i++;
                    }
                }
            }
            //echo $inhouse_quantity;exit;
            //echo $price;exit;
            $qty = 1;
            if (isset($_POST['qty'])) {
                $qty = $this->input->post('qty');
            }
            $price = displayCustomerPrice($list_price, $discount_expiration, $price);

            //echo $qty;exit;
            $flag = TRUE;
            $dataTmp = $this->cart->contents();

            ///insert


            $newQTY = $qty;
            $data = array(
                'rowid' => $rowid,
                'id' => $id,
                'qty' => $newQTY,
                'price' => $price,
                'name' => 'product'
            );
            if (!empty($options)) {
                $data['options'] = $options;
            }
            // check price segment
            $id_stock = 0;
            if (isset($stock) && !empty($stock))
                $id_stock = $stock['id_products_stock'];
            $price_segment = $this->ecommerce_model->checkPriceSegment($id, $id_stock, $newQTY);

            if ($price_segment != '' && $price_segment < $price) {

                $price = $price_segment;
                // $data['price'] = $price;
            }

            // end check price segment
            /*	if($inhouse_quantity < $newQTY) {
				 $this->session->set_flashdata('error_message',lang('stock_not_available').' '.$product['title'.getFieldLanguage($lang)].' '.$stock_text);
			}
			else {*/
            if ($inhouse_quantity < $newQTY) {
                $flag = FALSE;
                $data3 = $data;
                $data3['id_products'] = $id;
                $this->input->session('product_stock_inquiry', $data);
                $this->session->set_flashdata('error_message', lang('stock_not_available'));
                redirect($_SERVER['HTTP_REFERER']);

            } else {
                //$data['note'] = "";


                $this->cart->update($data);

                // update in database: shopping cart
                $update['rowid'] = $rowid;
                $update['created_date'] = date('Y-m-d h:i:s');
                $update['id_products'] = $id;
                $update['sessionid'] = $this->session->userdata('session_id');
                $update['qty'] = $newQTY;
                $update['price'] = $price;
                $update['name'] = 'product';
                $cond_uu = array();
                $cond_uu['rowid'] = $rowid;
                if ($this->session->userdata('login_id') == '') {
                    $cond_uu['sessionid'] = $this->session->userdata('session_id');
                } else {
                    $cond_uu['id_user'] = $this->session->userdata('login_id');
                }
                $this->db->update('shopping_cart', $update, $cond_uu);
                if (!empty($options)) {
                    $results = $this->ecommerce_model->getOptionsByIds($options);
                    $insert['options'] = serializecartOptions($results);
                }

                /*$insert['note']="";*/
                $this->db->update('shopping_cart', $update, $cond_uu);
                // end insert in database: shopping cart
                // update in database: stock
                $comb = '';
                if (isset($combination)) $comb = $combination;
                /* $this->ecommerce_model->UpdateProductStockInDatabase($id,$inhouse_quantity,0,$newQTY,$comb);*/
                // end update in database: stock
                $this->session->set_flashdata('success_message', $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . ' ' . lang('cart_added'));
            }
            /*}*/
            if ($this->input->post('add_to_cart') && $this->input->post('add_to_cart') == "add_to_cart") {
                $this->session->set_flashdata('success_message', $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . ' is added to your shopping cart.');
            }

            if ($this->input->post('add_to_cart') && $this->input->post('add_to_cart') == "add_to_cart") {

                redirect(route_to('cart'));
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect($_SERVER['HTTP_REFERER']);
        }

    }


    /************************************************************************************************/

    /*function update_quantity()
{
	$product_ids = $this->input->post('product_id');
	$product_rowids = $this->input->post('product_rowid');
	$quantities = $this->input->post('product_quantity');
	$dataTmp = $this->cart->contents();
	$flag = 0;
	$i=0;
	foreach($product_rowids as $key => $prodrowid) {$i++;
		$id = $product_ids[$key];
		$product = $this->fct->getonerow('products',array('id_products'=>$id));
		//if($i == 2) { echo $product['list_price'].','.$product['discount_expiration'].','.$product['price'];exit;}
		$price = displayCustomerPrice($product['list_price'],$product['discount_expiration'],$product['price']);
		//echo $price;exit;
		//if($i == 2) {echo 'a'.$price;exit;}
		$inhouse_quantity = $product['quantity'];
		foreach($dataTmp as $item) {
			$stock = array();
			if($item['rowid'] == $prodrowid) {
				$data = array(
					'rowid' => $prodrowid,
					'qty' => $quantities[$key]
                 );
				 $options = array();
				 if(isset($item['options'])) {
				 	$options = $item['options'];
				 }
				 if(!empty($options)) {

					 $results = $this->ecommerce_model->getOptionsByIds($options);

					 $combination = serializecartOptions($results);
					 $stock = $this->fct->getonerow('products_stock',array('id_products'=>$id,'combination'=>$combination));
					// print '<pre>';print_r($stock);exit;
					if(!empty($stock)) {
						$inhouse_quantity = $stock['quantity'];
						$price = displayCustomerPrice($stock['list_price'],$stock['discount_expiration'],$stock['price']);
					}
				 }
				 if($inhouse_quantity < $quantities[$key] - $item['qty']) {
					 $flag = 1;
					 $this->session->set_flashdata('success_message',lang('cart_quantity_updated_note'));
				 }
				 else {
					// echo $price;exit;
					 //if($i == 2) {echo 'b'.$price;exit;}
					 $newQTY = $quantities[$key];
					 // check price segment
					$id_stock = 0;
					if(isset($stock) && !empty($stock))
					$id_stock = $stock['id_products_stock'];
				//	echo $id_stock;exit;
					$price_segment = $this->ecommerce_model->checkPriceSegment($id,$id_stock,$newQTY);
					//echo $price_segment;exit;
					// if($i == 2) {echo 'ps1: '.$price_segment;exit;}
					 if($price_segment != '' && $price_segment <= $price) {
						//echo 'aa';exit;
					   $price = $price_segment;
				 	 }
				     //echo 'p21: '.$price;exit;
					 $data['price'] = $price;
					 // end check price segment
					 $this->cart->update($data);
					 // update in database: shopping cart
					 $update['updated_date'] = date('Y-m-d H:i:s');
					 $update['qty'] = $newQTY;
					 $update['price'] = $price;
					 $cond_uu = array();
					 $cond_uu['rowid'] = $prodrowid;
					 if($this->session->userdata('login_id') == '') {
						 $cond_uu['sessionid'] = $this->session->userdata('session_id');
					 }
					 else {
						 $cond_uu['id_user'] = $this->session->userdata('login_id');
					 }
					 $this->db->update('shopping_cart',$update,$cond_uu);
					 // end update in database: shopping cart
					 // update in database: stock
					 $comb = '';
					 if(isset($combination)) $comb = $combination;
					 $this->ecommerce_model->UpdateProductStockInDatabase($id,$inhouse_quantity,$item['qty'],$newQTY,$comb);
					 // end update in database: stock
				 }
			}
		}
	}
	if($flag == 0) {
		$this->session->set_flashdata('success_message',lang('cart_quantity_updated'));
	}

	redirect(route_to('cart'));
}
*/
    function update_quantity()
    {
        if ($this->input->post('action_cart') == "checkout") {
                $this->sendOrder();
            } else {
                
            $promocode = $this->input->post('promocode', true);
            $brand = $this->input->post('brandid', true);
            $product_ids = $this->input->post('product_id');
            $product_rowids = $this->input->post('product_rowid');
            $quantities = $this->input->post('product_quantity');
            $return_message = "";
            
            if($promocode){
                
                $brandQty = $this->input->post('brand-qty', true);
                
                if($brandQty > 1){
                    for($i = 1; $i <= $brandQty; $i++){
                        $brand = $this->input->post('brandid'.$i, true);
                        if($i != $brandQty){
                            $brandconditions = "id_brands = " . $brand . " || " . $brandconditions ;
                        } else {
                            $brandconditions = $brandconditions . "id_brands = " . $brand;
                        }
                    }
                    $brandcondition = "(" . $brandconditions . ")";
                } else {
                    $brand = $this->input->post('brandid1', true);
                    $brandcondition = "id_brands = " . $brand . " " ;
                }
            
                $promocodeQuery = "SELECT * FROM `promo_code` Where `code` Like '" . $promocode . "' AND " . $brandcondition;
                $query = $this->db->query($promocodeQuery);
                foreach ($query->result() as $row)
                {
                    $brandid =  $row->id_brands;
                    $expireDate =  $row->expire_date;
                }
                $count = $query->num_rows();
                $today = date("Y-m-d");
                if($count == 1){
                    if($today < $expireDate){
                        $dataTmp = $this->cart->contents();
                        $flag = 0;
                        $i = 0;
                        $redeem_bool = true;
                        foreach ($product_rowids as $key => $prodrowid) {
                            $update['promo_code'] = $promocode;
                            $cond_uu = array();
                            $cond_uu['rowid'] = $prodrowid;
                            $cond_uu['id_brands'] = $brandid;
                            
                            if ($this->session->userdata('login_id') == '') {
                                $cond_uu['sessionid'] = $this->session->userdata('session_id');
                            } else {
                                $cond_uu['id_user'] = $this->session->userdata('login_id');
                            }
                            $this->db->update('shopping_cart', $update, $cond_uu);
                        }
                        
                        $return['message'] = "Your discount has been applied.";
                        $data['section'] = 'cart';
                        $return['cart'] = $this->load->view('blocks/cart', $data, true);
                        $return['total_price_cont'] = $this->load->view('blocks/total_price_cont', $data, true);
                        $return['result'] = 1;
                        $return['count_items'] = $this->cart->total_items();
                    } else {
                        $return['message'] = 'Promocode Expired';
                    }
                } else {
                    $return['message'] = 'The discount code you entered is invalid or not applicable to the contents of your cart.';
                }
                die(json_encode($return));
                exit;
            }

            $dataTmp = $this->cart->contents();
            $flag = 0;
            $i = 0;
            $redeem_bool = true;
            foreach ($product_rowids as $key => $prodrowid) {
                $i++;
                $id = $product_ids[$key];
                $product = $this->fct->getonerow('products', array('id_products' => $id));
                $stock_status = $product['stock_status'];
                $general_miles = $product['general_miles'];
                $set_general_miles = $product['set_general_miles'];
                //if($i == 2) { echo $product['list_price'].','.$product['discount_expiration'].','.$product['price'];exit;}
                $price = displayCustomerPrice($product['list_price'], $product['discount_expiration'], $product['price']);
                //echo $price;exit;
                //if($i == 2) {echo 'a'.$price;exit;}
                $inhouse_quantity = $product['quantity'];
                foreach ($dataTmp as $item) {
                    $stock = array();
                    if ($item['rowid'] == $prodrowid) {
                        $qty = 0;
                        if (!empty($quantities[$key])) {
                            $qty = $quantities[$key];
                        }
                        $data = array(
                            'rowid' => $prodrowid,
                            'qty' => $qty
                        );

                        $options = array();
                        if (isset($item['options']['product_options'])) {
                            $options = $item['options']['product_options'];
                        }
                        if (!empty($options)) {
                            $results = $this->ecommerce_model->getOptionsByIds($options);
                            $combination = serializecartOptions($results);
                            $stock = $this->fct->getonerow('products_stock', array('id_products' => $id, 'combination' => $combination));
                            $general_miles = $stock['general_miles'];
                            $set_general_miles = $stock['set_general_miles'];
                            // print '<pre>';print_r($stock);exit;
                            if (!empty($stock)) {

                                $inhouse_quantity = $stock['quantity'];
                                $price = displayCustomerPrice($stock['list_price'], $stock['discount_expiration'], $stock['price']);
                            }
                        }
                        if ((int)$item['options']['redeem_miles'] != "") {
                            $result_miles = $this->ecommerce_model->updateMilesRedeem($prodrowid, 1, $quantities[$key], $item['options']['redeem_miles']);
                            if ($result_miles['result'] == 0) {
                                $return_message = $result_miles['message'];
                                $redeem_bool = false;
                            }
                        }
                        
                        //if ((($inhouse_quantity < $quantities[$key]) && (empty($stock_status) || $stock_status == 0)) || !$redeem_bool) {
                        //no stock check, cause no stock
                        if (false) {
                            $flag = 1;
                            if ($redeem_bool) {
                                /*$this->session->set_flashdata('success_message',lang('cart_quantity_updated_note'));*/
                                $return_message = lang('cart_quantity_updated_note');
                            }
                        } else {
                            // echo $price;exit;
                            //if($i == 2) {echo 'b'.$price;exit;}
                            $newQTY = $quantities[$key];
                            if (empty($newQTY)) {
                                $newQTY = 0;
                            }
                            // check price segment
                            $id_stock = 0;
                            if (isset($stock) && !empty($stock))
                                $id_stock = $stock['id_products_stock'];
                            //	echo $id_stock;exit;
                            $price_segment = $this->ecommerce_model->checkPriceSegment($id, $id_stock, $newQTY);

                            //echo $price_segment;exit;
                            // if($i == 2) {echo 'ps1: '.$price_segment;exit;}
                            if ($price_segment != '' && $price_segment <= $price) {
                                //echo 'aa';exit;
                                $price = $price_segment;

                            }
                            $miles = 0;
                            if ($general_miles >= 0 && $general_miles <= 100 && $set_general_miles == 1) {
                                $miles = getMiles($general_miles, $price);
                            }

                            $options['miles'] = $miles;
                            if (!empty($options)) {
                                $data['options'] = $options;
                            }

                            //echo 'p21: '.$price;exit;
                            $data['price'] = $price;
                            // end check price segment
                            $this->cart->update($data);
                            // update in database: shopping cart
                            $update['updated_date'] = date('Y-m-d H:i:s');
                            $update['qty'] = $newQTY;
                            $update['price'] = $price;

                            $update['miles'] = $miles;
                            $cond_uu = array();
                            $cond_uu['rowid'] = $prodrowid;
                            if ($this->session->userdata('login_id') == '') {
                                $cond_uu['sessionid'] = $this->session->userdata('session_id');
                            } else {
                                $cond_uu['id_user'] = $this->session->userdata('login_id');
                            }
                            $this->db->update('shopping_cart', $update, $cond_uu);
                            // end update in database: shopping cart
                            // update in database: stock
                            $comb = '';
                            if (isset($combination)) $comb = $combination;
                            /*$this->ecommerce_model->UpdateProductStockInDatabase($id,$inhouse_quantity,$item['qty'],$newQTY,$comb);*/
                            // end update in database: stock
                        }
                    }
                }
            }
            if ($flag == 0) {
                /*$this->session->set_flashdata('success_message',lang('cart_quantity_updated'));*/
                $return_message = lang('cart_quantity_updated');
            }

            /*redirect(route_to('cart'));*/
            if (checkUserIfLogin()) {
                $user = $this->ecommerce_model->getUserInfo();
                $cartPrices = $this->ecommerce_model->getCheckoutAttr();
                $user_miles = $user['miles'] - $cartPrices['total_redeem_miles'];
                if ($user_miles > 0) {
                    $available_miles = " (" . $user_miles . ")";
                } else {
                    $available_miles = " (0)";
                }
                $return['remaining_miles'] = $available_miles;
            }
            $return['message'] = $return_message;
            $data['section'] = 'cart';
            $return['cart'] = $this->load->view('blocks/cart', $data, true);
            $return['total_price_cont'] = $this->load->view('blocks/total_price_cont', $data, true);
            $return['result'] = 1;
            $return['count_items'] = $this->cart->total_items();
            /*	if($flag==0){
	$return['result']=1;}else{
	$return['result']=0;	}*/


            die(json_encode($return));
        }
    }

    public function sendOrder()
    {

        $allPostedVals = $this->input->post(NULL, TRUE);
        if (checkUserIfLogin()) {
            $cart_items = $this->cart->contents();
            if (empty($cart_items)) {
                $this->session->set_flashdata('success_message', lang('cart_is_empty'));
                redirect(route_to('cart'));
            } else {
                if (isset($_POST) && !empty($_POST)) {
                    $this->form_validation->set_rules('action_cart', lang('action_cart'), 'trim|required|xss_clean');

                    // if error
                    if ($this->form_validation->run() == FALSE) {


                        $return['result'] = 0;
                        $return['errors'] = array();
                        $return['message'] = 'Error!';
                        $return['captcha'] = $this->fct->createNewCaptcha();
                        $find = array('<p>', '</p>');
                        $replace = array('', '');
                        foreach ($allPostedVals as $key => $val) {
                            if (form_error($key) != '') {
                                $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                            }
                        }

                        die(json_encode($return));
                    } // if success
                    else {

                        //this is where we really submit the cart

                        $user = $this->ecommerce_model->getUserInfo();

                        $cart_items = $this->cart->contents();

                        if (empty($cart_items))
                            $cart_items = $this->ecommerce_model->loadUserCart();
                        $ids_products = $this->ecommerce_model->getIds('id', $cart_items);

                        if (!empty($ids_products)) {
                            $suppliers = $this->ecommerce_model->getCartDeliveredByBrands(array('ids_products' => $ids_products));
                        } else {
                            $suppliers = array();
                        }

                        $cart_rows_ids = array();
                        
                        foreach ($suppliers as $supplier) {
                            if (($supplier['id_user'] == $this->input->post('id_supplier') || $this->input->post('id_supplier') == "all") || ($this->input->post('id_supplier') == "all" && empty($supplier['id_user']))) {
                                // created order
                                $order['created_date'] = date('Y-m-d h:i:s');
                                $order['id_user'] = $user['id_user'];
                                $order['id_supplier'] = $supplier['id_user'];
                                $order['currency'] = $this->session->userdata('currency');
                                $order['default_currency'] = $this->config->item('default_currency');
                                $order['rand'] = rand();
                                if ($this->input->post('exported')) {
                                    $order['exported'] = $this->input->post('exported');
                                }
                                $order['comments'] = $this->input->post('comments');
                                $order['status'] = 'pending';

                                /*$order['billing_id'] = $billing_id;*/
                                $order['billing_id'] = 0;
                                $order['delivery_id'] = 0;
                                $order['lang'] = $this->lang->lang();
                                $order['shipping_charge'] = 0;
                                $order['shipping_charge_currency'] = 0;

                                //print '<pre>';print_r($order);
                                $this->db->insert('orders', $order);
                                $order_id = $this->db->insert_id();

                                // arrange and insert line items
                                $cart_items = $this->cart->contents();
                                $ids_brands = array();
                                $brands = array();
                                if (!empty($supplier['id_user'])) {
                                    $brands = $this->fct->getAll_cond('brands', 'sort_order asc', array('id_user' => $supplier['id_user']));
                                    $ids_brands = $this->ecommerce_model->getIds('id_brands', $brands);
                                } else {
                                }
                                $cond['ids_brands'] = $ids_brands;
                                $products = $this->ecommerce_model->getCartProducts($cart_items, '', $cond);
                                $net_price = 0;
                                $net_discount_price = 0;
                                $net_redeem_miles_price = 0;
                                $net_discount_redeem_miles_price = 0;
                                $miles = 0;
                                $redeem_miles = 0;
                                $total_miles = 0;
                                $total_redeem_miles = 0;
                                $selected_miles_redeem = $this->input->post('miles_redeem');
                                $j = 0;

                                foreach ($products as $product) {
                                    array_push($cart_rows_ids, $product['rowid']);
                                    $j++;
                                    $cond_p = array();
                                    $line_item['redeem'] = 0;
                                    // Miles
                                    $qty = $product['qty'];
                                    $miles = $product['options']['miles'];
                                    $redeem_miles = $product['options']['redeem_miles'];

                                    if (!empty($product['options']['redeem_miles'])) {
                                        $line_item['redeem'] = 1;
                                        $miles = 0;
                                    }

                                    $total_price = 0;
                                    $line_item['created_date'] = date('Y-m-d h:i:s');
                                    $line_item['id_products'] = $product['product_info']['id_products'];
                                    if (empty($product['options']['id_brands'])) {
                                        $product['options']['id_brands'] = 0;
                                    }
                                    $line_item['id_brands'] = $product['options']['id_brands'];
                                    $line_item['stock_status'] = $product['product_info']['stock_status'];
                                    $line_item['service'] = $product['product_info']['set_as_service_product'];

                                    $line_item['exclude_from_shipping_calculation'] = $product['product_info']['exclude_from_shipping_calculation'];
                                    $cond_p['id_products'] = $line_item['id_products'];
                                    $product_tshold = $this->fct->getonerow('products', $cond_p);

                                    // Price
                                    $new_price = $product['price'];
                                    $list_price = $product['product_info']['list_price'];
                                    $total_price = $list_price * $product['qty'];
                                    $total_discount_price = $product['price'] * $product['qty'];
                                    $discount = $product['product_info']['price'] - $product['price'];

                                    $line_item['miles'] = $miles;
                                    $line_item['redeem_miles'] = $redeem_miles;

                                    $line_item['price_old'] = $list_price;
                                    $line_item['price'] = $product['price'];
                                    $line_item['discount'] = changeCurrency($discount, FALSE);
                                    $line_item['quantity'] = $product['qty'];

                                    $line_item['id_orders'] = $order_id;

                                    $line_item['currency'] = $this->session->userdata('currency');
                                    $line_item['default_currency'] = $this->config->item('default_currency');
                                    $line_item['price_old_currency'] = changeCurrency($list_price, FALSE);
                                    $line_item['price_currency'] = changeCurrency($new_price, FALSE);
                                    $line_item['delivery_days'] = $product['product_info']['delivery_days'];

                                    $sku = $product['product_info']['sku'];
                                    $barcode = $product['product_info']['barcode'];
                                    if ($product['product_info']['delivery_days'] != "") {
                                        $line_item['delivery_days'] = $product['product_info']['delivery_days'];
                                    } else {
                                        $line_item['delivery_days'] = 0;
                                    }
                                    $line_item['total_price_old'] = $total_price;
                                    $line_item['total_price_old_currency'] = changeCurrency($total_price, FALSE);

                                    $line_item['total_price_currency'] = changeCurrency($total_discount_price, FALSE);
                                    $line_item['total_price'] = $total_discount_price;
                                    /*$net_price = $net_price + $total_price;*/

                                    $line_item['weight'] = $product['product_info']['weight'];
                                    $line_item['group_products'] = $product['product_info']['group_products'];
                                    $line_item['options_en'] = "";
                                    // options
                                    if (!empty($product['cart_options'])) {
                                        //////////////////Threshold//////
                                        $line_item['options_en'] = serializecartOptions($product['cart_options']);
                                        $cond_p['combination'] = $line_item['options_en'];
                                        $cond_p['id_products'] = $line_item['id_products'];
                                        $product_tshold = $this->fct->getonerow('products_stock', $cond_p);
                                        $sku = $product_tshold['sku'];
                                        $barcode = $product_tshold['barcode'];
                                    }

                                    $line_item['sku'] = $sku;
                                    $line_item['barcode'] = $barcode;

                                    $this->db->insert('line_items', $line_item);

                                    $new_id = $this->db->insert_id();
                                }

                                $orderData = $this->ecommerce_model->getOrder($order_id);

                                $checkoutAttr = $this->ecommerce_model->getCheckoutAttr($supplier);
                                $sub_total = $checkoutAttr['sub_total'];
                                $net_discount_price = $checkoutAttr['net_discount_price'];
                                $net_price = $checkoutAttr['net_price'];
                                $discount = $checkoutAttr['discount'];
                                $weight = $checkoutAttr['weight'];
                                $miles = $checkoutAttr['miles'];
                                $total_miles = $checkoutAttr['total_miles'];
                                $total_redeem_miles = $checkoutAttr['total_redeem_miles'];
                                $net_redeem_miles_price = $checkoutAttr['net_redeem_miles_price'];
                                $net_discount_redeem_miles_price = $checkoutAttr['net_discount_redeem_miles_price'];
                                if (checkuserIfLogin()) {
                                    $q = 'DELETE FROM shopping_cart WHERE id_user = ' . $this->session->userdata('login_id');
                                    if (!empty($ids_brands)) {
                                        $q .= ' and id_brands IN (' . implode(',', $ids_brands) . ')';
                                    } else {
                                        $q .= ' and (id_brands ="" OR id_brands=0)';
                                    }
                                    $this->db->query($q);
                                }

                                if (isset($supplier['delivery_time'])) {
                                    $order_update_template['delivery_time'] = $supplier['delivery_time'];
                                    $order_update_template['delivery_fee'] = $supplier['delivery_fee'];
                                    $order_update_template['min_order'] = $supplier['min_order'];
                                    $order_update_template['credit'] = $supplier['credit'];
                                }
                                if($this->input->post('comments')){
                                    $order_update_template['comments'] = $this->input->post('comments');
                                }else {
                                    $order_update_template['comments'] = '';
                                }
                                $order_update_template['sub_total'] = $sub_total;
                                $order_update_template['weight'] = $weight;
                                $order_update_template['miles'] = $total_miles;
                                $order_update_template['redeem_miles'] = $total_redeem_miles;
                                $order_update_template['total_price'] = $net_price;
                                $order_update_template['redeem_discount_miles_amount'] = changeCurrency($net_discount_redeem_miles_price, false);
                                $order_update_template['redeem_miles_amount'] = changeCurrency($net_redeem_miles_price, false);
                                $order_update_template['payment_method'] = $this->input->post('payment_method');
                                $order_update_template['total_price_currency'] = changeCurrency($net_price, false);
                                $order_update_template['discount'] = changeCurrency($discount, false);
                                $order_update_template['amount'] = $net_discount_price + $order['shipping_charge'];
                                $order_update_template['amount_currency'] = changeCurrency($net_discount_price, false) + $order['shipping_charge_currency'];

                                $this->db->where('id_orders', $order_id);
                                $this->db->update('orders', $order_update_template);

                                //generate invoice templat
                                $orderData = $this->ecommerce_model->getOrder($order_id);


                                $d['orderData'] = $orderData;
                                $paymentData = $orderData;
                                $paymentData['id_orders'] = 'o-' . $order_id;

                                $bool_payment = true;

                                if ($this->input->post('payment_method') == "credits" || $this->input->post('payment_method') == "checque" || $this->input->post('payment_method') == "cash" || $this->input->post('payment_method') == "") {
                                    $cond_d['id_orders'] = $orderData['id_orders'];
                                    $cond_d['rand'] = $orderData['rand'];
                                    $this->ecommerce_model->updateOrderQuantities($cond_d);

                                ////////////////////////User Miles///////////////
                                    $new_user_miles = $user['miles'] - $total_redeem_miles;
                                    $update_miles['miles'] = $new_user_miles;
                                    $this->db->where('id_user', $user['id_user']);
                                    $this->db->update('user', $update_miles);
                                }

                                $paymentData['currency'] = $this->session->userdata('currency');
                                $paymentData['amount'] = $orderData['amount_currency'];
                                $paymentData['amount_default_currency'] = changeCurrency($orderData['amount_currency'], "", $this->config->item('default_currency'), $this->session->userdata('currency'));

                                ////////////CREDITS CART//////////
                                if ($this->input->post('payment_method') == "credit_cards") {
                                    $return['message'] = "<span class='vSuccess'>" . lang("payment_redirection_ni") . "</span>";
                                    $return['payment'] = $this->fct->load_ni_payment_form($paymentData);
                                    $return['result'] = 1;
                                    $bool_payment = false;
                                    die(json_encode($return));
                                    exit;
                                }
                                
                                ////////////PAYPAL//////////
                                if ($this->input->post('payment_method') == "paypal") {
                                    $return['message'] = "<span class='vSuccess'>" . lang("payment_redirection_ni") . "</span>";
                                    $return['payment'] = $this->load->view('blocks/paypal', $paymentData, true);
                                    $return['result'] = 1;
                                    $bool_payment = false;
                                    die(json_encode($return));
                                    exit;
                                }

                                if ($bool_payment) {
                                    $order_update_status['status'] = 'unread';

                                    if ($this->input->post('payment_method') == "credits") {
                                        $order_update_status['status'] = 'paid';

                                        ////////////////////////User Credits ///////////////
                                        $amount_credits_remove = $orderData['amount'];
                                        $user_update['available_balance'] = $user['available_balance'] - $amount_credits_remove;
                                        $this->db->where("id_user", $user['id_user']);
                                        $this->db->update("user", $user_update);
                                    }

                                    $this->db->where('id_orders', $order_id);
                                    $this->db->update('orders', $order_update_status);

                                    //$this->session->set_flashdata('success_message', 'Your order is complete! Your order number is ' . $order_id . '.A member of Spamiles team will review your order and may contact you for confirmation.');
                                    $orderData = $this->ecommerce_model->getOrder($order_id);
                                    $this->load->model('send_emails');

                                    $this->send_emails->sendOrderToSupplier($orderData);
                                    $this->send_emails->sendOrderToAdmin($orderData);
                                    $this->send_emails->sendOrderToUser($orderData);
                                    $this->serviceLocator->buyersService()->addBuyerForOrder(
                                        $this->session->userdata('login_id'),
                                        $orderData['id_supplier'],
                                        null,
                                        null
                                    );
                                }
                            }


                            $return['result'] = 1;
                            $return['checkout'] = 1;
                            $return['message'] = lang('system_is_redirecting_to_page');

                            $this->ecommerce_model->destroyItems($cart_rows_ids);
                            //$return['redirect_link'] = route_to('user/orders');
                            $return['redirect_link'] = route_to('?open=afterOrder');
                        }

                        $return['result'] = 1;
                        die(json_encode($return));
                    }

                } else {
                    $this->checkout_template();
                }
            }
        } else {
            $this->session->set_flashdata('error_message', lang('login_to_continue'));
            $this->session->set_userdata('redirect_link', route_to('cart/checkout'));
            redirect(route_to('user/login'));
        }
    }


    public function check_if_date_valid()
    {
        $day = intval($this->input->post('day'));
        $month = intval($this->input->post('month'));
        $year = intval($this->input->post('year'));
        $current_year = date("Y");
        $bool = true;
        $message = "";
        if (empty($month) && empty($day) && empty($year)) {
            return true;
        } else {
            if (!empty($year) && ($year < 1900 || $year > $current_year)) {
                $bool = false;
                $message = "Please enter a valid year (1900-2015).";
            } else {
                if (empty($year)) {
                    $bool = false;
                    $message = "Year field is required.";
                }
            }


            if (!empty($month) && ($month < 1 || $month > 12)) {
                $bool = false;
                $message = "Please enter a valid month (1-12).";
            } else {

                if (empty($month)) {
                    $bool = false;
                    $message = "Month field is required.";
                }
            }

            if (!empty($day) && ($day < 1 || $day > 31)) {
                $bool = false;
                $message = "Please enter a valid day (1-31).";
            } else {
                if (empty($day)) {
                    $bool = false;
                    $message = "Day field is required.";
                }
            }

//echo 'aasa '.$this->session->userdata('login_id');exit;
            if ($bool) {
                return true;
            } else {
                $this->form_validation->set_message('check_if_date_valid', $message);
                return false;
            }
        }
        //print_r($cond);exit;

    }


    public function check_if_user_credits_valid()
    {
        $lang = "";

        $checkoutAttr = $this->ecommerce_model->getCheckoutAttr();
        $net_discount_price = changeCurrency($checkoutAttr['net_discount_price'], "", $this->config->item('default_currency'), $this->session->userdata('currency'));
        $shipping_charge = changeCurrency(0, "", $this->config->item('default_currency'), $this->session->userdata('currency'));
        $amount_checkout = $net_discount_price + $shipping_charge;
        $user = $this->ecommerce_model->getUserInfo();
        $available_balance = $user['available_balance'];

        if ($available_balance >= $amount_checkout) {
            return true;
        } else {
            $this->form_validation->set_message('check_if_user_credits_valid', "The amount of your order exceeds the available balance in your Wallet.");
            return false;
        }
    }

    function loginSucces($user, $bool)
    {


        if ($bool) {
            $update['last_login'] = date('Y-m-d h:i:s');
            $this->db->where('id_user', $user['id_user']);
            $this->db->update('user', $update);

            $this->session->set_userdata('login_id', $user['id_user']);
            $this->session->set_userdata('correlation_id', $user['correlation_id']);
            $this->session->set_userdata('account_type', $user['type']);
            $this->session->set_userdata('user_name', $user['first_name'] . ' ' . $user['last_name']);
            $this->session->set_userdata('username', $user['username']);
            $this->session->set_userdata('email', $user['email']);
            $this->session->set_userdata('last_login', changeDate($update['last_login']));
            $this->session->set_flashdata('success_message', lang('welcome') . ' ' . $user['name'] . ' ' . lang(',') . ' ' . lang('you_have_successfully_login_with') . ' <a href="' . site_url() . '">' . lang('cartel') . '</a>.');
        }
    }

    /************************************************************************************************/
    public function check_if_exportable()
    {
        if ($this->input->post('exported') == 1) {
            return true;
        } else {
            $this->form_validation->set_message('check_if_exportable', "Please remove the non exportable products from your cart or confirm if you are willing to ship them at your own responsibility.");
            return false;
        }
    }

//////////////////////////////////////////Miles Redeem////////////////////////////
    public function removeRedeemMileRowId($arr, $id)
    {
        $product_quantities = $this->session->userdata('product_quantities');
        $new_product_quantities = array();
        $new_arr = array();
        foreach ($arr as $v) {
            if ($v != $id) {
                $new_product_quantities[$v] = $product_quantities[$v];
                array_push($new_arr, $v);
            }
        }
        $this->session->set_userdata('product_quantities', $new_product_quantities);
        return $new_arr;
    }

    public function checkQuantity($user_miles, $total_redeem_miles, $redeem_miles, $qty)
    {
        $total_redeem_miles = $total_redeem_miles + $redeem_miles * $qty;
        if ($total_redeem_miles > $user_miles && $qty > 1) {
            $qty = $qty - 1;
            $qty = $this->checkQuantity($user_miles, $total_redeem_miles, $redeem_miles, $qty);
        }
        return $qty;
    }

    public function updateMilesRedeem()
    {
        $this->ecommerce_model->updateMilesRedeem();
    }

    public function check_checkRedeemMiles()
    {
        $user = $this->ecommerce_model->getUserInfo();
        $user_miles = $user['miles'];
        $bool = true;
        $total_redeem_miles = 0;

        $miles_redeem = $this->input->post('miles_redeem');
        if ($this->input->post('miles_redeem') != "") {
            foreach ($miles_redeem as $r) {
                $total_redeem_miles = $total_redeem_miles + $r;
            }


            if ($user_miles >= $total_redeem_miles && $total_redeem_miles > 0) {
                $bool = true;
            } else {
                $bool = false;
            }
        }

        if ($bool) {
            return true;
        } else {
            $this->form_validation->set_message('check_checkRedeemMiles', '<ul class="messages"><li class="error-msg r-fullSide"><ul><li><span>Your selected miles is exceeded your miles.</span></li></ul></li></ul>');
            return false;
        }

    }

    public function check_if_email_exists()
    {
        $lang = "";
        if ($this->config->item("language_module")) {
            $lang = getFieldLanguage($this->lang->lang());
        }
        $data['lang'] = $lang;
        $email = $this->input->post('billing_email');
        $cond['email'] = $email;
        //$cond['guest'] = 0;
        $cond['deleted'] = 0;
        $cond['completed'] = 1;
        //echo 'aasa '.$this->session->userdata('login_id');exit;


        if ($this->input->post('id_user') != "") {
            $cond['id_user !='] = $this->input->post('id_user');
        }

        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $cond['id_user !='] = $user['id_user'];
        }

        if ($this->input->post('token') != "") {
            $cond['token !='] = $this->input->post('token');
        }
        //print_r($cond);exit;
        $check = $this->fct->getonerow('user', $cond);

        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('check_if_email_exists', lang('email_exists'));
            return false;
        }
    }

    public function check_if_username_exists()
    {
        $username = $this->input->post('billing_username');
        $cond['username'] = $username;
        //echo 'aasa '.$this->session->userdata('login_id');exit;
        if ($this->session->userdata('login_id')) {
            $cond['id_user !='] = $this->session->userdata('login_id');
        }

        //print_r($cond);exit;
        $check = $this->fct->getonerow('user', $cond);
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('check_if_username_exists', lang('username_exists'));
            return false;
        }
    }

//////////////////////////////////////////Checkout////////////////////////////

    public function phone()
    {

        $phone = $this->input->post('billing_phone');
        $bool = true;
        if (!empty($phone)) {
            foreach ($phone as $key => $val) {
                if (empty($val)) {
                    $bool = false;
                }
            }

            if (!$bool) {
                $this->form_validation->set_message('phone', "The phone fields is required.");
                return false;
            } else {
                return true;
            }
        }

    }

    public function phone2()
    {

        $phone = $this->input->post('billing_phone');
        $bool = true;
        if (!empty($phone)) {
            foreach ($phone as $key => $val) {
                if (empty($val)) {
                    $bool = false;
                }
            }

            if (!$bool) {
                $this->form_validation->set_message('phone2', "The phone fields is required.");
                return false;
            } else {
                return true;
            }
        }

    }

    public function validatecaptcha()
    {
        include_once './captcha/securimage.php';
        $securimage = new Securimage();
        $captcha = $this->input->post('captcha');
        if (empty($captcha)) {
            $this->form_validation->set_message('validatecaptcha', lang('captcha_field_required'));
            return false;
        } elseif ($securimage->check($captcha) == false) {
            $this->form_validation->set_message('validatecaptcha', lang('security_code_incorrect'));
            return false;
        } else {
            return true;
        }
    }

    public function checkout()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);
        if (checkUserIfLogin()) {
            $cart_items = $this->cart->contents();
            if (empty($cart_items)) {
                $this->session->set_flashdata('success_message', lang('cart_is_empty'));
                redirect(route_to('cart'));
            } else {
                if (isset($_POST) && !empty($_POST)) {

                    // billing information validation
                    // delivery information validation

                    $this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');

                    /*					if(isset($_POST['exported'])){
					$this->form_validation->set_rules('exported',"",'trim|xss_clean|callback_check_if_exportable[]');}	*/

                    if ($this->input->post('shipping_select') == "" && ($this->input->post('use_billing_address') == 0 || $this->input->post('use_billing_address') == 2)) {
                        if ($this->input->post('use_billing_address') == 2) {
                            $this->form_validation->set_rules('branch_name', ' branch name', 'trim|required|xss_clean');
                        }
                        $this->form_validation->set_rules('delivery_select', lang('delivery'), 'trim|xss_clean');
                        $this->form_validation->set_rules('delivery_first_name', lang('delivery') . ': ' . lang('first_name'), 'trim|required|xss_clean');
                        $this->form_validation->set_rules('delivery_last_name', lang('delivery') . ': ' . lang('last_name'), 'trim|required|xss_clean');
                        /*$this->form_validation->set_rules('delivery_phone',lang('delivery').': '.lang('phone'),'trim|required|xss_clean');*/
                        $this->form_validation->set_rules('delivery_phone', lang('delivery') . ': ' . lang('phone'), 'trim|required|xss_clean');
                        /*$this->form_validation->set_rules('delivery_company',lang('delivery').': '.lang('company'),'trim|xss_clean');*/
                        $this->form_validation->set_rules('delivery_postal_code', lang('delivery') . ': ' . lang('postal_code'), 'trim|xss_clean');
                        $this->form_validation->set_rules('delivery_country', lang('delivery') . ': ' . lang('country'), 'trim|required|xss_clean');
                        $this->form_validation->set_rules('delivery_state', lang('delivery') . ': ' . lang('state'), 'trim|xss_clean');
                        $this->form_validation->set_rules('delivery_street_one', lang('delivery') . ': street address', 'trim|required|xss_clean');
                        $this->form_validation->set_rules('delivery_street_two', lang('delivery') . ': street address', 'trim|xss_clean');
                        $this->form_validation->set_rules('delivery_city', lang('delivery') . ': ' . lang('city'), 'trim|required|xss_clean');
                    }

                    if ($this->input->post('billing_select') == "") {
                        if (!checkUserIfLogin()) {
                            $this->form_validation->set_rules('billing_email', lang('email'), 'trim|required|valid_email|xss_clean|callback_check_if_email_exists[]');
                            //$this->form_validation->set_rules('billing_username',lang('billing').':'.lang('username'),'trim|required|xss_clean|callback_check_if_username_exists[]');
                        }
                        $this->form_validation->set_rules('billing_select', lang('billing'), 'trim|xss_clean');
                        $this->form_validation->set_rules('billing_first_name', lang('billing') . ': ' . lang('first_name'), 'trim|required|xss_clean');
                        $this->form_validation->set_rules('billing_last_name', lang('billing') . ': ' . lang('last_name'), 'trim|required|xss_clean');/*$this->form_validation->set_rules('billing_phone',lang('billing').': '.lang('phone'),'trim|required|xss_clean')*/;
                        $this->form_validation->set_rules('billing_phone', lang('billing') . ': ' . lang('phone'), 'trim|required|xss_clean');
                        $this->form_validation->set_rules('billing_company', lang('billing') . ': ' . lang('company'), 'trim|xss_clean');
                        $this->form_validation->set_rules('billing_postal_code', lang('billing') . ': ' . lang('postal_code'), 'trim|xss_clean');
                        $this->form_validation->set_rules('billing_country', lang('billing') . ': ' . lang('country'), 'trim|required|xss_clean');
                        $this->form_validation->set_rules('billing_state', lang('billing') . ': ' . lang('state'), 'trim|xss_clean');
                        $this->form_validation->set_rules('billing_street_one', lang('billing') . ': street address', 'trim|required|xss_clean');
                        $this->form_validation->set_rules('billing_street_two', lang('billing') . ': street address', 'trim|xss_clean');
                        $this->form_validation->set_rules('billing_city', lang('billing') . ': ' . lang('city'), 'trim|required|xss_clean');
                    }

                    // payment methods
                    $this->form_validation->set_rules('payment_method', lang('payment_methods'), 'trim|required|xss_clean');
                    // comments
                    $this->form_validation->set_rules('comments', lang('comments'), 'trim|xss_clean');
                    /*$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validatecaptcha[]');*/
                    // run form validation
                    // if error
                    if ($this->form_validation->run() == FALSE) {
                        $return['result'] = 0;
                        $return['errors'] = array();
                        $return['message'] = 'Error!';
                        $return['captcha'] = $this->fct->createNewCaptcha();
                        $find = array('<p>', '</p>');
                        $replace = array('', '');
                        foreach ($allPostedVals as $key => $val) {
                            if (form_error($key) != '') {
                                $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                            }
                        }
                        $data['select_billing_country'] = $this->input->post('billing_country');
                        $data['select_billing_select'] = $this->input->post('billing_select');
                        if (isset($_POST['make_delivery_as_billing']) && $_POST['make_delivery_as_billing'] == 1) {
                            $data['delivery_checked'] = 1;
                        } else {
                            $data['select_delivery_country'] = $this->input->post('delivery_country');
                            $data['select_delivery_select'] = $this->input->post('delivery_select');
                        }
                        //print '<pre>';print_r($data);exit;
                        die(json_encode($return));
                    } // if success
                    else {

                        if (!checkUserIfLogin()) {
                            $guest_user['first_name'] = $this->input->post("billing_first_name");
                            $guest_user['correlation_id'] = rand();
                            $guest_user['last_name'] = $this->input->post("billing_last_name");
                            $guest_user['city'] = $this->input->post("city");
                            $guest_user['gender'] = $this->input->post('gender');
                            $guest_user['name'] = $guest_user['first_name'] . ' ' . $guest_user['last_name'];
                            $guest_user['username'] = $this->input->post('billing_username');

                            /*			$phone = $this->input->post("billing_phone");
			                    $guest_user['phone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];*/
                            $guest_user['phone'] = $this->input->post('billing_phone');
                            /*			$fax = $this->input->post("billing_fax");
			                    $guest_user['fax'] = $fax[0].'-'.$fax[1].'-'.$fax[2];*/
                            $guest_user['fax'] = $this->input->post('billing_fax');
                            /*	        $mobile = $this->input->post("billing_mobile");
			                    $guest_user['mobile'] = $mobile[0].'-'.$mobile[1].'-'.$mobile[2];*/
                            $guest_user['mobile'] = $this->input->post('billing_mobile');
                            $guest_user['state'] = $this->input->post('billing_state');
                            $guest_user['address_1'] = $this->input->post('billing_street_one');
                            $guest_user['address_2'] = $this->input->post('billing_street_two');
                            $guest_user['id_countries'] = $this->input->post('billing_country');
                            $guest_user['email'] = $this->input->post('billing_email');
                            $guest_user['is_subscribed'] = 0;
                            $guest_user['completed'] = 1;
                            $guest_user['agree'] = 1;
                            $guest_user['created_date'] = date('Y-m-d h:i:s');
                            $day = intval($this->input->post('day'));
                            $month = intval($this->input->post('month'));
                            $year = intval($this->input->post('year'));
                            $date = $year . '-' . $month . '-' . $day;
                            $guest_user['date_of_birth'] = $date;
                            $guest_user['status'] = 1;
                            $guest_user['id_roles'] = 2;
                            $guest_user['guest'] = 0;
                            $password = rand();
                            $guest_user['password'] = md5($password);
                            $this->db->insert('user', $guest_user);
                            $new_id = $this->db->insert_id();
                            $user = $this->ecommerce_model->getUserInfo($new_id);

                            $this->load->model('send_emails');
                            /*
                            $this->send_emails->sendUserToAdmin($user);
		                    $this->send_emails->sendGuestReply($user);
		                    */

                        } else {
                            $user = $this->ecommerce_model->getUserInfo();
                        }

                        //echo 'test';exit;
                        // address book
                        $billing_select = $this->input->post('billing_select');

                        // billing information
                        $billing_select = $this->input->post('billing_select');
                        $billing_user_address = "";
                        if ($billing_select != '') {
                            $billing_user_address = $this->fct->getonerecord('users_addresses', array('id_users_addresses' => $billing_select));
                        }

                        if ($billing_user_address == '') {
                            $billing['id_user'] = $user['id_user'];
                            $billing['first_name'] = $this->input->post("billing_first_name");
                            $billing['last_name'] = $this->input->post("billing_last_name");
                            $billing['name'] = $billing['first_name'] . ' ' . $billing['last_name'];
                            $billing['company'] = $this->input->post("billing_company");
                            $billing['billing'] = 1;
                            /*	$phone = $this->input->post("billing_phone");
                    			$billing['phone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];
                    			$fax = $this->input->post("billing_fax");
                    			$billing['fax'] = $fax[0].'-'.$fax[1].'-'.$fax[2];
                    			$mobile = $this->input->post("billing_mobile");
                    			$billing['mobile'] = $mobile[0].'-'.$mobile[1].'-'.$mobile[2];
                    		*/
                            $billing['phone'] = $this->input->post("billing_phone");
                            $billing['fax'] = $this->input->post("billing_fax");
                            $billing['mobile'] = $this->input->post("billing_mobile");
                            $billing['street_one'] = $this->input->post("billing_street_one");
                            $billing['street_two'] = $this->input->post("billing_street_two");
                            $billing['city'] = $this->input->post("billing_city");
                            $billing['state'] = $this->input->post("billing_state");
                            $billing['postal_code'] = $this->input->post("billing_postal_code");
                            $billing['id_countries'] = $this->input->post("billing_country");
                            $billing['updated_date'] = date('Y-m-d h:i:s');
                            $billing['created_date'] = date('Y-m-d h:i:s');
                        } else {
                            $billing['id_user'] = $billing_user_address['id_user'];
                            $billing['first_name'] = $billing_user_address['first_name'];
                            $billing['mobile'] = $billing_user_address['mobile'];
                            $billing['last_name'] = $billing_user_address['last_name'];
                            $billing['name'] = $billing_user_address['name'];
                            $billing['company'] = $billing_user_address['company'];
                            $billing['phone'] = $billing_user_address['phone'];
                            $billing['fax'] = $billing_user_address['fax'];
                            $billing['billing'] = 1;
                            $billing['street_one'] = $billing_user_address['street_one'];
                            $billing['street_two'] = $billing_user_address['street_two'];
                            $billing['city'] = $billing_user_address['city'];
                            $billing['state'] = $billing_user_address['state'];
                            $billing['postal_code'] = $billing_user_address['postal_code'];
                            $billing['id_countries'] = $billing_user_address['id_countries'];
                            $billing['updated_date'] = date('Y-m-d h:i:s');
                            $billing['created_date'] = date('Y-m-d h:i:s');
                        }
                        if ($this->input->post('use_billing_address') == 1) {
                            $billing['shipping'] = 1;
                        }
                        $this->db->insert('address_book', $billing);

                        $billing_id = $this->db->insert_id();
                        if ($this->input->post("save_billing") && $billing_user_address == '' && $this->input->post('checkout_method') != "guest") {
                            $this->db->insert('users_addresses', $billing);
                        }

                        // delivery information
                        if ($this->input->post('use_billing_address') == 0 || $this->input->post('use_billing_address') == 2) {
                            if ($this->input->post('use_billing_address') == 2) {
                                $delivery['branch'] = 1;
                            } else {
                                $delivery['shipping'] = 1;
                            }
                            $delivery_select = $this->input->post('shipping_select');
                            $deliver_user_address = "";
                            if ($delivery_select != '') {
                                $deliver_user_address = $this->fct->getonerecord('users_addresses', array('id_users_addresses' => $delivery_select));
                            }

                            if ($deliver_user_address == '') {
                                $delivery['id_user'] = $user['id_user'];
                                $delivery['first_name'] = $this->input->post("delivery_first_name");
                                $delivery['last_name'] = $this->input->post("delivery_last_name");
                                /*				$mobile = $this->input->post("delivery_mobile");
                        			$delivery['mobile'] = $mobile[0].'-'.$mobile[1].'-'.$mobile[2];;*/
                                $delivery['name'] = $delivery['first_name'] . ' ' . $delivery['last_name'];
                                $delivery['company'] = $this->input->post("deliver_company");
                                $delivery['branch_name'] = $this->input->post("branch_name");

                                /*			$phone = $this->input->post("delivery_phone");
                        			$delivery['phone'] = $phone[0].'-'.$phone[1].'-'.$phone[2];
                        			$fax = $this->input->post("delivery_fax");
                        			$delivery['fax'] = $fax[0].'-'.$fax[1].'-'.$fax[2];*/
                                $delivery['phone'] = $this->input->post("delivery_phone");
                                $delivery['fax'] = $this->input->post("delivery_fax");
                                $delivery['mobile'] = $this->input->post("delivery_mobile");
                                $delivery['street_one'] = $this->input->post("delivery_street_one");
                                $delivery['street_two'] = $this->input->post("delivery_street_two");
                                $delivery['city'] = $this->input->post("delivery_city");
                                $delivery['state'] = $this->input->post("delivery_state");
                                $delivery['postal_code'] = $this->input->post("delivery_postal_code");
                                $delivery['id_countries'] = $this->input->post("delivery_country");
                                $delivery['updated_date'] = date('Y-m-d h:i:s');
                                $delivery['created_date'] = date('Y-m-d h:i:s');
                            } else {
                                $delivery['id_user'] = $deliver_user_address['id_user'];
                                $delivery['first_name'] = $deliver_user_address['first_name'];
                                $delivery['pickup'] = $deliver_user_address['pickup'];
                                $delivery['mobile'] = $deliver_user_address['mobile'];
                                $delivery['last_name'] = $deliver_user_address['last_name'];
                                $delivery['name'] = $deliver_user_address['name'];
                                $delivery['company'] = $deliver_user_address['company'];
                                $delivery['phone'] = $deliver_user_address['phone'];
                                $delivery['fax'] = $deliver_user_address['fax'];
                                if ($this->input->post('use_billing_address') == 2) {
                                    $branch = $this->fct->getonerecord('checklist_branches', array('id_users_addresses' => $delivery_select));
                                    $delivery['branch_name'] = $branch['title'];
                                }
                                $delivery['street_one'] = $deliver_user_address['street_one'];
                                $delivery['street_two'] = $deliver_user_address['street_two'];
                                $delivery['city'] = $deliver_user_address['city'];
                                $delivery['state'] = $deliver_user_address['state'];
                                $delivery['postal_code'] = $deliver_user_address['postal_code'];
                                $delivery['id_countries'] = $deliver_user_address['id_countries'];
                                $delivery['updated_date'] = date('Y-m-d h:i:s');
                                $delivery['created_date'] = date('Y-m-d h:i:s');
                            }
                            $this->db->insert('address_book', $delivery);
                            $shipping_id = $this->db->insert_id();
                        }

                        if ($this->input->post("save_shipping") && isset($deliver_user_address) && $deliver_user_address == '' && $this->input->post('checkout_method') != "guest") {
                            $insert_delivery_address = $delivery;
                            unset($insert_delivery_address['branch_name']);
                            $this->db->insert('users_addresses', $insert_delivery_address);
                            $id_user_address_ship = $this->db->insert_id();
                        }

                        if ($this->input->post('use_billing_address') == 1) {
                            $delivery_id = $billing_id;
                            $delivery = $billing;
                        } else {
                            $delivery_id = $shipping_id;

                            ///////////////BRANCH////////////////
                            if ($this->input->post('use_billing_address') == 2 && (($this->input->post("save_shipping") && isset($deliver_user_address) && $deliver_user_address == '') || ($delivery_select != ""))) {
                                if ($delivery_select != '') {
                                    $id_user_address = $delivery_select;
                                } else {
                                    $id_user_address = $id_user_address_ship;
                                }
                                $branch_name = $this->input->post('branch_name');
                                $branch = $this->fct->getonerecord('checklist_branches', array('id_users_addresses' => $id_user_address));
                                $branch_data['updated_date'] = date('Y-m-d h:i:s');
                                $branch_data['title'] = $branch_name;
                                $branch_data['id_users_addresses'] = $id_user_address;
                                $branch_data['id_user'] = $user['id_user'];
                                $branch_data['created_date'] = date('Y-m-d h:i:s');
                                if (empty($branch)) {
                                    $this->db->insert('checklist_branches', $branch_data);
                                } else {
                                    $cond2['id_checklist_branches'] = $branch['id_checklist_branches'];
                                    $this->db->where($cond2);
                                    $this->db->update('checklist_branches', $branch_data);
                                }
                            }
                        }

                        // end order billing and delivery information
                        //echo 'here great';exit;
                        // created order
                        $order['created_date'] = date('Y-m-d h:i:s');
                        $order['id_user'] = $user['id_user'];
                        $order['rand'] = rand();
                        if ($this->input->post('exported')) {
                            $order['exported'] = $this->input->post('exported');
                        }
                        $order['comments'] = $this->input->post('comments');
                        $order['status'] = 'pending';

                        $order['payment_method'] = $this->input->post('payment_method');
                        if ($order['payment_method'] == 0) {
                            $order['payment_method'] = null;
                        }
                        /*$order['billing_id'] = $billing_id;*/
                        $order['billing_id'] = $billing_id;
                        $order['delivery_id'] = $delivery_id;
                        $order['lang'] = $this->lang->lang();
                        $order['shipping_charge'] = 0;
                        $order['shipping_charge_currency'] = 0;
                        //$order['id_supplier'] = $supplier['id_user'];

                        $delivery_address = $this->fct->getonerecord('address_book', array('id_address_book' => $delivery_id));

                        $aramex = $this->checkAramex(true, $delivery_address);
                        if (isset($delivery_address['pickup']) && $delivery_address['pickup'] == 1) {
                            $order['shipping'] = 0;
                        } else {
                            $order['shipping'] = 1;
                        }
                        if (!empty($aramex) && $aramex != 1) {

                            $order['shipping_charge'] = $aramex['shippingCharge'];
                            $order['shipping_charge_currency'] = changeCurrency($aramex['shippingCharge'], false);
                        }

                        $order['default_currency'] = $this->config->item('default_currency');
                        $order['currency'] = $this->session->userdata('currency');

                        //print '<pre>';print_r($order);
                        $this->db->insert('orders', $order);
                        $order_id = $this->db->insert_id();

                        // arrange and insert line items
                        $cart_items = $this->cart->contents();
                        $products = $this->ecommerce_model->getCartProducts($cart_items);
                        $net_price = 0;
                        $net_discount_price = 0;
                        $net_redeem_miles_price = 0;
                        $net_discount_redeem_miles_price = 0;
                        $miles = 0;
                        $redeem_miles = 0;
                        $total_miles = 0;
                        $total_redeem_miles = 0;
                        $selected_miles_redeem = $this->input->post('miles_redeem');
                        $j = 0;
                        foreach ($products as $product) {
                            $j++;
                            $cond_p = array();
                            $line_item['redeem'] = 0;
                            // Miles
                            $qty = $product['qty'];
                            $miles = $product['options']['miles'];
                            $redeem_miles = $product['options']['redeem_miles'];

                            if (!empty($product['options']['redeem_miles'])) {
                                $line_item['redeem'] = 1;
                                $miles = 0;
                            }

                            $total_price = 0;
                            $line_item['created_date'] = date('Y-m-d h:i:s');
                            $line_item['id_products'] = $product['product_info']['id_products'];
                            if (empty($product['options']['id_brands'])) {
                                $product['options']['id_brands'] = 0;
                            }
                            $line_item['id_brands'] = $product['options']['id_brands'];
                            $line_item['stock_status'] = $product['product_info']['stock_status'];
                            $line_item['service'] = $product['product_info']['set_as_service_product'];
                            $line_item['exclude_from_shipping_calculation'] = $product['product_info']['exclude_from_shipping_calculation'];
                            $cond_p['id_products'] = $line_item['id_products'];
                            $product_tshold = $this->fct->getonerow('products', $cond_p);

                            // Price
                            $new_price = $product['price'];
                            $list_price = $product['product_info']['list_price'];
                            $total_price = $list_price * $product['qty'];
                            $total_discount_price = $product['price'] * $product['qty'];
                            $discount = $product['product_info']['price'] - $product['price'];

                            $line_item['miles'] = $miles;
                            $line_item['redeem_miles'] = $redeem_miles;
                            $line_item['price_old'] = $list_price;
                            $line_item['price'] = $product['price'];
                            $line_item['discount'] = changeCurrency($discount, FALSE);
                            $line_item['quantity'] = $product['qty'];
                            $line_item['id_orders'] = $order_id;
                            $line_item['currency'] = $this->session->userdata('currency');
                            $line_item['default_currency'] = $this->config->item('default_currency');
                            $line_item['price_old_currency'] = changeCurrency($list_price, FALSE);
                            $line_item['price_currency'] = changeCurrency($new_price, FALSE);
                            $line_item['delivery_days'] = $product['product_info']['delivery_days'];

                            $sku = $product['product_info']['sku'];
                            $barcode = $product['product_info']['barcode'];
                            if ($product['product_info']['delivery_days'] != "") {
                                $line_item['delivery_days'] = $product['product_info']['delivery_days'];
                            } else {
                                $line_item['delivery_days'] = 0;
                            }
                            $line_item['total_price_old'] = $total_price;
                            $line_item['total_price_old_currency'] = changeCurrency($total_price, FALSE);

                            $line_item['total_price_currency'] = changeCurrency($total_discount_price, FALSE);
                            $line_item['total_price'] = $total_discount_price;
                            /*$net_price = $net_price + $total_price;*/

                            $line_item['weight'] = $product['product_info']['weight'];
                            $line_item['group_products'] = $product['product_info']['group_products'];
                            $line_item['options_en'] = "";
                            // options
                            
                            if (!empty($product['cart_options'])) {
                                //////////////////Threshold//////
                                $line_item['options_en'] = serializecartOptions($product['cart_options']);
                                $cond_p['combination'] = $line_item['options_en'];
                                $cond_p['id_products'] = $line_item['id_products'];
                                $product_tshold = $this->fct->getonerow('products_stock', $cond_p);
                                $sku = $product_tshold['sku'];
                                $barcode = $product_tshold['barcode'];
                                //$line_item['options'] = serializecartOptions($product['cart_options'],'');
                                // update the quantity of stock in product stock items
                                //$this->fct->UpdateQuantityInStock($line_item['id_products'],$line_item['options_en'],$line_item['quantity']);
                            }

                            $line_item['sku'] = $sku;
                            $line_item['barcode'] = $barcode;
                            //else {
                            // update the quantity of stock in products items
                            //$this->fct->UpdateQuantityInProducts($line_item['id_products'],$line_item['quantity']);
                            //	}
                            $this->db->insert('line_items', $line_item);
                            $new_id = $this->db->insert_id();

                        }

                        $orderData = $this->ecommerce_model->getOrder($order_id);

                        // update order amount and invoice template;
                        /*$order_amount = $net_price;
						$order_update['amount'] = $order_amount;
						$order_update['default_currency'] = $this->config->item('default_currency');
						$order_update['currency'] = $this->session->userdata('currency');
						$order_update['amount_currency'] = changeCurrency($order_amount,FALSE);
						$this->db->where('id_orders',$order_id);
						$this->db->update('orders',$order_update);*/
                        // destroy items in shopping cart
                        //$this->cart->destroy();
                        /*$this->cart->destroy();*/
                        $checkoutAttr = $this->ecommerce_model->getCheckoutAttr();
                        $sub_total = $checkoutAttr['sub_total'];
                        $net_discount_price = $checkoutAttr['net_discount_price'];
                        $net_price = $checkoutAttr['net_price'];
                        $discount = $checkoutAttr['discount'];
                        $weight = $checkoutAttr['weight'];
                        $miles = $checkoutAttr['miles'];
                        $total_miles = $checkoutAttr['total_miles'];
                        $total_redeem_miles = $checkoutAttr['total_redeem_miles'];
                        $net_redeem_miles_price = $checkoutAttr['net_redeem_miles_price'];
                        $net_discount_redeem_miles_price = $checkoutAttr['net_discount_redeem_miles_price'];
                        if (checkuserIfLogin()) {
                            $this->db->query('DELETE FROM shopping_cart WHERE id_user = ' . $this->session->userdata('login_id'));
                        }
                        $order_update_template['sub_total'] = $sub_total;
                        $order_update_template['weight'] = $weight;
                        $order_update_template['miles'] = $total_miles;
                        $order_update_template['redeem_miles'] = $total_redeem_miles;
                        $order_update_template['total_price'] = $net_price;
                        $order_update_template['redeem_discount_miles_amount'] = changeCurrency($net_discount_redeem_miles_price, false);
                        $order_update_template['redeem_miles_amount'] = changeCurrency($net_redeem_miles_price, false);
                        $order_update_template['payment_method'] = $this->input->post('payment_method');
                        $order_update_template['total_price_currency'] = changeCurrency($net_price, false);
                        $order_update_template['discount'] = changeCurrency($discount, false);
                        $order_update_template['amount'] = $net_discount_price + $order['shipping_charge'];
                        $order_update_template['amount_currency'] = changeCurrency($net_discount_price, false) + $order['shipping_charge_currency'];

                        /*	echo "<pre>";
						print_r($order_update_template);exit;*/

                        // update order invoice template
                        //$order_update_template['invoice_template'] = $invoiceTemplate;
                        $this->db->where('id_orders', $order_id);
                        $this->db->update('orders', $order_update_template);

                        //print '<pre>'; print_r($orderData); exit;

                        //generate invoice templat
                        $orderData = $this->ecommerce_model->getOrder($order_id);

                        $d['orderData'] = $orderData;
                        $paymentData = $orderData;
                        $paymentData['id_orders'] = 'o-' . $order_id;

                        $bool_payment = true;

                        if ($this->input->post('payment_method') == "credits" || $this->input->post('payment_method') == "checque" || $this->input->post('payment_method') == "cash") {
                            $cond_d['id_orders'] = $orderData['id_orders'];
                            $cond_d['rand'] = $orderData['rand'];
                            $this->ecommerce_model->updateOrderQuantities($cond_d);

                            ////////////////////////User Miles///////////////
                            $new_user_miles = $user['miles'] - $total_redeem_miles;
                            $update_miles['miles'] = $new_user_miles;
                            $this->db->where('id_user', $user['id_user']);
                            $this->db->update('user', $update_miles);
                        }

                        $paymentData['currency'] = $this->session->userdata('currency');
                        $paymentData['amount'] = $orderData['amount_currency'];
                        $paymentData['amount_default_currency'] = changeCurrency($orderData['amount_currency'], "", $this->config->item('default_currency'), $this->session->userdata('currency'));
                        
                        $paymentData['billing_id'] = $billing_select;
                        $paymentData['delivery_id'] = $delivery_select;

                        ////////////CREDITS CART//////////
                        if ($this->input->post('payment_method') == "credit_cards") {
                            $return['message'] = "<span class='vSuccess'>" . lang("payment_redirection_ni") . "</span>";
                            $return['payment'] = $this->fct->load_ni_payment_form($paymentData);
                            $return['result'] = 1;
                            $bool_payment = false;
                            die(json_encode($return));
                            exit;
                        }
                        
                        ////////////PAYPAL//////////
                        if ($this->input->post('payment_method') == "paypal") {
                            $return['message'] = "<span class='vSuccess'>" . lang("payment_redirection_ni") . "</span>";
                            $return['payment'] = $this->load->view('blocks/paypal', $paymentData, true);
                            $return['result'] = 1;
                            $bool_payment = false;
                            die(json_encode($return));
                            exit;
                        }

                        if ($bool_payment) {
                            $order_update_status['status'] = 'unread';

                            if ($this->input->post('payment_method') == "credits") {
                                $order_update_status['status'] = 'paid';

                                ////////////////////////User Credits ///////////////
                                $amount_credits_remove = $orderData['amount'];
                                $user_update['available_balance'] = $user['available_balance'] - $amount_credits_remove;
                                $this->db->where("id_user", $user['id_user']);
                                $this->db->update("user", $user_update);
                            }

                            $this->db->where('id_orders', $order_id);
                            $this->db->update('orders', $order_update_status);

                            $this->session->set_flashdata('success_message', 'Your order is complete! Your order number is ' . $order_id . '.A member of Spamiles team will review your order and may contact you for confirmation.');
                            $orderData = $this->ecommerce_model->getOrder($order_id);
                            $this->load->model('send_emails');
                            $this->send_emails->sendOrderToSupplier($orderData);
                            $this->send_emails->sendOrderToAdmin($orderData);
                            $this->send_emails->sendOrderToUser($orderData);

                            $return['result'] = 1;
                            $return['message'] = lang('system_is_redirecting_to_page');
                            $this->cart->destroy();

                            if (checkUserIfLogin()) {
                                $return['redirect_link'] = route_to('user/orders/' . $order_id . '/' . $orderData['rand']);
                            } else {
                                /*$return['redirect_link']=site_url();*/
                                $return['redirect_link'] = route_to('user/orders/' . $order_id . '/' . $orderData['rand']);
                            }
                        }

                        /*$invoiceTemplate = $this->ecommerce_model->generateInvoiceTemplate($orderData);*/
                        $return['result'] = 1;
                        die(json_encode($return));
                    }

                } else {
                    $this->checkout_template();
                }
            }
        } else {
            $this->session->set_flashdata('error_message', lang('login_to_continue'));
            $this->session->set_userdata('redirect_link', route_to('cart/checkout'));
            redirect(route_to('user/login'));
        }
    }


    public function quotation($id_supplier = 0)
    {
        $login = checkUserIfLogin();
        $guest = true;
        if ($id_supplier > 0) {

            $supplier = $this->ecommerce_model->getUserInfo($id_supplier);
        } else {
            $supplier = array('id_user' => "");
        }
        $allPostedVals = $this->input->post(NULL, TRUE);
        if (checkUserIfLogin() || $guest) {

            $cart_items = $this->cart->contents();
            if ($login) {
                $user = $this->ecommerce_model->getUserInfo();
                $order['id_user'] = $user['id_user'];
            }
            $order['sessionid'] = $this->session->userdata('session_id');
            $order['user_ip'] = $this->input->ip_address();
            $order['created_date'] = date('Y-m-d h:i:s');

            $order['rand'] = rand();
            if ($this->input->post('exported')) {
                $order['exported'] = $this->input->post('exported');
            }
            $order['comments'] = $this->input->post('comments');
            $order['status'] = 'quotation';
            $order['lang'] = $this->lang->lang();
            $order['shipping_charge'] = 0;
            $order['shipping_charge_currency'] = 0;
            $order['default_currency'] = $this->config->item('default_currency');
            $order['currency'] = $this->session->userdata('currency');

            $this->db->insert('quotation', $order);
            $order_id = $this->db->insert_id();


            // arrange and insert line items
            $cart_items = $this->cart->contents();
            $brands = $this->fct->getAll_cond('brands', 'sort_order asc', array('id_user' => $id_supplier));
            $ids_brands = $this->ecommerce_model->getIds('id_brands', $brands);
            $cond['ids_brands'] = $ids_brands;
            $products = $this->ecommerce_model->getCartProducts($cart_items, "", $cond);


            $net_price = 0;
            $net_discount_price = 0;
            $net_redeem_miles_price = 0;
            $net_discount_redeem_miles_price = 0;
            $miles = 0;
            $redeem_miles = 0;
            $total_miles = 0;
            $total_redeem_miles = 0;
            $selected_miles_redeem = $this->input->post('miles_redeem');
            $j = 0;
            foreach ($products as $product) {
                $j++;
                $cond_p = array();
                $line_item['redeem'] = 0;
                // Miles
                $qty = $product['qty'];
                $miles = $product['options']['miles'] * $qty;
                $redeem_miles = $product['options']['redeem_miles'] * $qty;
                $id_brands = $product['options']['id_brands'];
                if (empty($id_brands)) {
                    $id_brands = 0;
                }

                if (isset($selected_miles_redeem[$product['rowid']])) {
                    $line_item['redeem'] = 1;
                }

                $total_price = 0;
                $line_item['created_date'] = date('Y-m-d h:i:s');
                $line_item['id_products'] = $product['product_info']['id_products'];
                if (empty($product['product_info']['exclude_from_shipping_calculation'])) {
                    $product['product_info']['exclude_from_shipping_calculation'] = 0;
                }
                $line_item['exclude_from_shipping_calculation'] = $product['product_info']['exclude_from_shipping_calculation'];
                $cond_p['id_products'] = $line_item['id_products'];
                $product_tshold = $this->fct->getonerow('products', $cond_p);

                // Price
                $new_price = $product['price'];
                $list_price = $product['product_info']['list_price'];
                $total_price = $list_price * $product['qty'];
                $total_discount_price = $product['price'] * $product['qty'];
                $discount = $product['product_info']['price'] - $product['price'];


                $line_item['miles'] = $miles;
                $line_item['redeem_miles'] = $redeem_miles;

                $line_item['price_old'] = $list_price;
                $line_item['price'] = $product['price'];
                $line_item['discount'] = changeCurrency($discount, FALSE);

                $line_item['quantity'] = $product['qty'];
                $line_item['id_brands'] = $id_brands;

                $line_item['id_quotation'] = $order_id;

                $line_item['currency'] = $this->session->userdata('currency');
                $line_item['default_currency'] = $this->config->item('default_currency');
                $line_item['price_old_currency'] = changeCurrency($list_price, FALSE);
                $line_item['price_currency'] = changeCurrency($new_price, FALSE);
                $line_item['delivery_days'] = $product['product_info']['delivery_days'];

                $sku = $product['product_info']['sku'];
                $barcode = $product['product_info']['barcode'];
                if ($product['product_info']['delivery_days'] != "") {
                    $line_item['delivery_days'] = $product['product_info']['delivery_days'];
                } else {
                    $line_item['delivery_days'] = 0;
                }
                $line_item['total_price_old'] = $total_price;
                $line_item['total_price_old_currency'] = changeCurrency($total_price, FALSE);

                $line_item['total_price_currency'] = changeCurrency($total_discount_price, FALSE);
                $line_item['total_price'] = $total_discount_price;
                /*$net_price = $net_price + $total_price;*/
                $line_item['weight'] = $product['product_info']['weight'];
                // options
                if (!empty($product['cart_options'])) {
                    //////////////////Threshold//////
                    $line_item['options_en'] = serializecartOptions($product['cart_options']);
                    $cond_p['combination'] = $line_item['options_en'];
                    $cond_p['id_products'] = $line_item['id_products'];
                    $product_tshold = $this->fct->getonerow('products_stock', $cond_p);
                    $sku = $product_tshold['sku'];
                    $barcode = $product_tshold['barcode'];
                }

                $line_item['sku'] = $sku;
                if (empty($barcode)) {
                    $barcode = 0;
                }
                $line_item['barcode'] = $barcode;

                $this->db->insert('quotation_line_items', $line_item);
                $new_id = $this->db->insert_id();

            }


            $orderData = $this->ecommerce_model->getQuotation($order_id);

            $checkoutAttr = $this->ecommerce_model->getCheckoutAttr($supplier);
            $sub_total = $checkoutAttr['sub_total'];
            $net_discount_price = $checkoutAttr['net_discount_price'];
            $net_price = $checkoutAttr['net_price'];
            $discount = $checkoutAttr['discount'];
            $weight = $checkoutAttr['weight'];
            $miles = $checkoutAttr['miles'];
            $total_miles = $checkoutAttr['total_miles'];
            $total_redeem_miles = $checkoutAttr['total_redeem_miles'];
            $net_redeem_miles_price = $checkoutAttr['net_redeem_miles_price'];
            $net_discount_redeem_miles_price = $checkoutAttr['net_discount_redeem_miles_price'];

            $order_update_template['sub_total'] = $sub_total;
            $order_update_template['weight'] = $weight;
            $order_update_template['miles'] = $total_miles;
            $order_update_template['redeem_miles'] = $total_redeem_miles;
            $order_update_template['total_price'] = $net_price;
            $order_update_template['redeem_discount_miles_amount'] = changeCurrency($net_discount_redeem_miles_price, false);
            $order_update_template['redeem_miles_amount'] = changeCurrency($net_redeem_miles_price, false);
            $order_update_template['payment_method'] = $this->input->post('payment_method');
            $order_update_template['total_price_currency'] = changeCurrency($net_price, false);
            $order_update_template['discount'] = changeCurrency($discount, false);
            $order_update_template['amount'] = $net_discount_price + $order['shipping_charge'];
            $order_update_template['amount_currency'] = changeCurrency($net_discount_price, false) + $order['shipping_charge_currency'];
            $this->db->where('id_quotation', $order_id);
            $this->db->update('quotation', $order_update_template);
            //print '<pre>'; print_r($orderData); exit;

            //generate invoice templat
            $orderData = $this->ecommerce_model->getQuotation($order_id);
            $user = $this->ecommerce_model->getUserInfo();
            $this->session->set_flashdata('success_message', 'Quotation is created successfully! Your quotation number is ' . $order_id . '.');
            $return['result'] = 1;
            if ($login) {
                $redirect_link = route_to('user/quotation/' . $order_id . '/' . $orderData['rand']);
            } else {
                /*$return['redirect_link']=site_url();*/
                $redirect_link = route_to('user/quotation/' . $order_id . '/' . $orderData['rand']);
            }

            redirect($redirect_link);
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }


    }

    function export_to_shopping($id_order, $rand)
    {
        $this->ecommerce_model->exportQuotationToCart($id_order, $rand);
        $this->session->set_flashdata('success_message', ' Export to shopping cart is completed.');
        redirect(route_to('cart'));
    }

    /*function download_pdf($id_order,$rand)
{
require_once('./tcpdf/config/lang/eng.php');
//			require_once('./tcpdf/mytcpdf.php');
require_once('./tcpdf/tcpdf.php');
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 049');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 049', PDF_HEADER_STRING);

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->SetFont('helvetica', '', 10);

$pdf->AddPage();


$html = '<h1>Test TCPDF Methods in HTML</h1>
<h2 style="color:red;">IMPORTANT:</h2>
<span style="color:red;">If you are using user-generated content, the tcpdf tag can be unsafe.<br />
You can disable this tag by setting to ffalse the <b>K_TCPDF_CALLS_IN_HTML</b> constant on TCPDF configuration file.</span>
<h2>write1DBarcode method in HTML</h2>';


$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();
$pdf->Output('./uploads/quotation/dsdsds.pdf', 'D');
}*/
    function download_pdf($id_order, $rand)
    {
        ob_start();
        /*$this->download('quotation','quotation1471269377.pdf');*/
        $cond['rand'] = $rand;
        $orderData = $this->ecommerce_model->getQuotation($id_order, $cond);
        $data['orderData'] = $orderData;
        if (!empty($orderData)) {
            $html = $this->load->view('emails/quotation', $data, true);


            require_once('./tcpdf/config/lang/eng.php');
            //require_once('./tcpdf/mytcpdf.php');
            require_once('./tcpdf/tcpdf.php');
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // create new PDF document
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            //set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM - 15);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetHeaderMargin(false);
            $pdf->SetFooterMargin(false);
            //set auto page breaks
            //echo PDF_IMAGE_SCALE_RATIO;exit;
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_LEFT);
            //set image scale factor
            //$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


            $lg = Array();
            $lg['a_meta_charset'] = 'UTF-8';
//$lg['a_meta_dir'] = 'rtl';
            $lg['a_meta_language'] = 'fa';
            $lg['w_page'] = 'page';
            /*print_r($lg);*/
//exit;
            $fontname = 'dejavusans';
            //set some language-dependent strings
            $pdf->setLanguageArray($lg);
            /*$pdf->SetFont($fontname, '',8, '',false);*/
            $fontname = $pdf->addTTFfont(K_PATH_FONTS . 'fonts/Nobel-Regular.ttf', 'Nobel-Regular', '', 32);
            $pdf->SetFont($fontname, '', 8, '', false);
            $pdf->SetFont('dejavusans', '', 8);
            $pdf->setFontSubsetting(false);
            /*$pdf->SetHeaderData("lexus-logo.png", PDF_HEADER_LOGO_WIDTH, '', '');*/
            /*add a page*/
            $pdf->SetFont('aefurat', '', 18);
            $pdf->SetFont('times', '', 20, '', 'TrueTypeUnicode');
            $pdf->SetFont('dejavusans', '', 7);
            $pdf->setImageScale(0.8);
            $pdf->AddPage();

            // data to generate
            //print_r($acc_records);exit;
            ///////////////////////////////////////////////
            //echo $html;exit;
            $pdf->writeHTML($html);


            //Close and output PDF document
            $filename = 'quotation';
            $name = $filename . '.pdf';
            $file = './uploads/quotation/' . $name;

            $pdf->Output($name, 'D');


            /*redirect(route_to('user/downloadQuotation/'.$id_order.'/'.$rand.'/'.$name));*/
        } else {
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/quotation/' . $id_order . '/' . $rand));
        }
    }

    public function downloadQuotation($id_order, $rand, $name)
    {
        $this->download('quotation', $name);
        $this->session->set_flashdata('success_message', ' Download is completed.');
        redirect(route_to('user/quotation/' . $id_order . '/' . $rand . '/' . $name));
    }

    public function download($uploads_folder, $name)
    {
        $this->load->helper('download');
        $file = 'uploads/' . $uploads_folder . '/' . $name;

        $data = file_get_contents($file);
        force_download($name, $data);

    }

    public function submit_buy($order)
    {

        //
// $this->session->set_userdata("success_message","Information was inserted successfully");
        //$SECURE_SECRET = "2F93B702D722AE819AC549582F0D3890";
        $SECURE_SECRET = $this->config->item("SecureHashSecret");
        $appendAmp = 0;
        $vpcURL = "";
        $newHash = "";

        // if the form is submitted undergo the below procedures
        /*if (isset($_POST['accessCode']))
 {*/
        $arr = array();
        $arr['accessCode'] = $this->config->item("AccessCode");
        $arr['merchTxnRef'] = $order['id_orders']; // to be changed to order id
        $arr['merchant'] = $this->config->item("merchantID");
        $arr['orderInfo'] = $order['id_orders'];

        $amount = changeCurrency($order['amount'], 0, "USD");
        /*$arr['amount'] = 10000;*/
        $arr['amount'] = round($amount, 0) * 100;

        $arr['returnURL'] = str_replace("http://", "https://", route_to('cart/buy_verification?action=py'));
        ksort($arr);
        /*echo "<pre>";
print_r($arr);exit;*/
        $md5HashData = $SECURE_SECRET;

        foreach ($arr as $key => $value) {
            // create the md5 input and URL leaving out any fields that have no value
            //if (strlen($value) > 0 && ($key == 'accessCode' || $key == 'merchTxnRef' || $key == 'merchant' || $key == 'orderInfo' || $key == 'amount' || $key == 'returnURL')) {
            //print 'Key: '.$key.'  Value: '.$value."<br>";
            // this ensures the first paramter of the URL is preceded by the '?' char
            //if($key == 'orderInfo') $value = $id_buy_card_online;
            //if($key == 'amount') $value = 10900;
            if (strlen($value) > 0 && ($key == 'accessCode' || $key == 'merchTxnRef' || $key == 'merchant' || $key == 'orderInfo' || $key == 'amount' || $key == 'returnURL')) {
                if ($appendAmp == 0) {
                    $vpcURL .= urlencode($key) . '=' . urlencode($value);
                    $appendAmp = 1;
                } else {
                    $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                }
                $md5HashData .= $value;
                // }
            }
        }
        /*  $vpcURL.= "&amount=".urlencode(10900);
  $md5HashData .= 10900;
  $vpcURL.= "&orderInfo=".urlencode($id_card_holder);
  $md5HashData .= $id_card_holder;*/
        $newHash .= $vpcURL . "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
        /*echo $this->config->item("gatewayURL").$newHash;exit; */
        return "<script language=\"javascript\">top.location.href='" . $this->config->item("gatewayURL") . $newHash . "'</script>";
        //exit;
        /*} else {

 //$data["message"] = "";
 //$message = $this->load->view('email-template',$data,true);
 //echo $message;
 redirect(route_to("cart/checkout"));
 }*/


    }

    public function submit_order_by_paypal($order)
    {
        //
// $this->session->set_userdata("success_message","Information was inserted successfully");
        //$SECURE_SECRET = "2F93B702D722AE819AC549582F0D3890";
        $SECURE_SECRET = $this->config->item("SecureHashSecret");
        $appendAmp = 0;
        $vpcURL = "";
        $newHash = "";

        // if the form is submitted undergo the below procedures
        /*if (isset($_POST['accessCode']))
 {*/
        $arr = array();
        $arr['accessCode'] = $this->config->item("AccessCode");
        $arr['merchTxnRef'] = $order['id_orders']; // to be changed to order id
        $arr['merchant'] = $this->config->item("merchantID");
        $arr['orderInfo'] = "test";

        $arr['amount'] = changeCurrency($order['amount'], 0, "USD") * 100;
        $arr['returnURL'] = route_to('cart/buy_verification?action=py');
        ksort($arr);


        $md5HashData = $SECURE_SECRET;

        foreach ($arr as $key => $value) {
            // create the md5 input and URL leaving out any fields that have no value
            //if (strlen($value) > 0 && ($key == 'accessCode' || $key == 'merchTxnRef' || $key == 'merchant' || $key == 'orderInfo' || $key == 'amount' || $key == 'returnURL')) {
            //print 'Key: '.$key.'  Value: '.$value."<br>";
            // this ensures the first paramter of the URL is preceded by the '?' char
            //if($key == 'orderInfo') $value = $id_buy_card_online;
            //if($key == 'amount') $value = 10900;
            if ($appendAmp == 0) {
                $vpcURL .= urlencode($key) . '=' . urlencode($value);
                $appendAmp = 1;
            } else {
                $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
            }
            $md5HashData .= $value;
            // }
        }
        /*  $vpcURL.= "&amount=".urlencode(10900);
  $md5HashData .= 10900;
  $vpcURL.= "&orderInfo=".urlencode($id_card_holder);
  $md5HashData .= $id_card_holder;*/
        $newHash .= $vpcURL . "&vpc_SecureHash=" . strtoupper(md5($md5HashData));

        return "<script language=\"javascript\">top.location.href='" . $this->config->item("gatewayURL") . $newHash . "'</script>";
        //exit;
        /*} else {

 //$data["message"] = "";
 //$message = $this->load->view('email-template',$data,true);
 //echo $message;
 redirect(route_to("cart/checkout"));
 }*/


    }


    public function paypal_success()
    {

        if (isset($_REQUEST['invoice']) && isset($_REQUEST['invoice'])) {
            $arr[0] = $_REQUEST['invoice'];
            $arr[1] = $_REQUEST['txn_id'];
            $order_id = $arr[0];
            $ref_id = $arr[1];
            $data_array = array(
                'ref_id' => $ref_id,
                'status' => 'paid',
                'payment_date' => date('Y-m-d h:i:s'),
            );
            $this->db->where('id_orders', $order_id);
            $this->db->update('orders', $data_array);

            $this->cart->destroy();

            //$this->session->set_flashdata('success_message',lang('order_completed').$order_id.'. One of our sales persons will review your order and contact you soon.');
            $this->session->set_flashdata('success_message', 'Your order is complete! Your order number is ' . $order_id . '.A member of Spamiles team will review your order and may contact you for confirmation.');
            $orderData = $this->ecommerce_model->getOrder($order_id);

            //////////////UPDATE QUANTITIES//////////
            $cond_d['id_orders'] = $orderData['id_orders'];
            $cond_d['rand'] = $orderData['rand'];
            $this->ecommerce_model->updateOrderQuantities($cond_d);
            //////////////SEND EMAILS//////////
            $this->load->model('send_emails');
            $this->send_emails->sendOrderToAdmin($orderData);
            $this->send_emails->sendOrderToUser($orderData);

            /*if(checkUserIfLogin()){*/
            redirect(route_to('user/orders/' . $order_id . '/' . $orderData['rand']));
        }
    }

    public function paypal_cancel()
    { /*$this->session->set_userdata("error_message",$errorTxt);*/
        $this->session->set_flashdata('error_message', "Your payment process is canceled");
        $cond['id_orders'] = $merchTxnRef;
        $this->db->delete('orders', $cond);
        redirect(route_to('cart/checkout'));
    }


    public function paypal_error()
    { /*$this->session->set_userdata("error_message",$errorTxt);*/
        $this->session->set_flashdata('error_message', "Error");
        $cond['id_orders'] = $merchTxnRef;
        $this->db->delete('orders', $cond);
        redirect(route_to('cart/checkout'));
    }


    public function checkout_template($data = array())
    {
        $cart_items = $this->cart->contents();
        $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);
        /*			if(isset($_SERVER['HTTPS'] ) ) {
			}else{
			redirect(str_replace("http://","https://",route_to('cart/checkout')));}*/

        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 12));
        $data['page_title'] = $data['seo']['title' . getFieldLanguage()];
        // breadcrumbs
        $breadcrumbs = array();
        $breadcrumbs[0]['title'] = lang('home');
        $breadcrumbs[0]['link'] = site_url();
        $breadcrumbs[1]['title'] = $data['page_title'];
        $crumbs['breadcrumbs'] = $breadcrumbs;
        // end breadcrumbs

        //$data['settings'] = $this->fct->getonerow('settings',array('id_settings'=>1));
        $data['notice_intro'] = $this->fct->getonerow('dynamic_pages', array('id_dynamic_pages' => 18));

        $cart_items = $this->cart->contents();
        $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);

        $data['countries'] = $this->fct->getAll_cond('countries', 'title', array("status" => 0));
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();
            $data['payment_methods'] = $this->fct->getAll('payment_methods', 'sort_order');
            $data['userData'] = $user;

            $cond1['id_user'] = $user['id_user'];
            /*$cond1['billing']=1;*/
            $data['user_addresses_billing'] = $this->ecommerce_model->getUserAddresses($cond1);

            $cond2['id_user'] = $user['id_user'];
            /*$cond2['shipping']=1;*/
            $data['user_addresses_delivery'] = $this->ecommerce_model->getUserAddresses($cond2);


            /*$cond2['shipping']=1;*/
        }


        $cond3['pickup'] = 1;
        $data['pickup_addresses'] = $this->ecommerce_model->getUserAddresses($cond3);


        // js files
        $this->template->add_js('front/js/checkout.js');

        $this->template->add_css('front/craftpip/css/jquery-confirm.css');

        $this->template->add_js('front/craftpip/js/jquery-confirm.js');


        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'cart/checkout_address', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);

        $this->template->render();
    }

    public function notice()
    {
        $settings = $this->fct->getonerow('settings', array('id_settings' => 1));
        echo $settings['checkout_notice'];
    }

    /************************************************************************************************/
    public function validatecode()
    {
        $code = $this->input->post('code');
        $res = $this->ecommerce_model->getDiscountByCode($code);
        if (empty($res)) {
            $this->form_validation->set_message('validatecode', lang('code_error'));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**************************************STEPS VALIDATION**********************************************************/
    public function checkAramex($bool = false, $address = "")
    {

        $AramexAttr = $this->ecommerce_model->getCheckoutAttr();

        $weight = $AramexAttr['weight'];

        $number_of_pieces = $AramexAttr['number_of_pieces'];
        $net_discount_price = $AramexAttr['net_discount_price'];
        if (empty($address)) {
            $address = $this->getAddressInfo();
        }
        if (!isset($address['cc_iso']) && !empty($address['id_countries'])) {
            $country_info = $this->fct->getonerow('countries', array('id_countries' => $address['id_countries']));
            $address['cc_iso'] = $country_info['cc_iso'];
        }

        $arr['order_id'] = $AramexAttr['id_orders'];
        $arr['net_discount_price'] = $AramexAttr['net_discount_price'];
        if (isset($address['city']))
            $arr['OriginAddressCity'] = "Dubai";

        if (isset($address['cc_iso']))
            $arr['OriginAddressCountryCode'] = "AE";

        if (isset($address['state']))
            $arr['state'] = $address['state'];

        if (isset($address['postal_code']))
            $arr['postal_code'] = $address['postal_code'];

        if (isset($address['city']))
            $arr['DestinationAddressCity'] = $address['city'];

        if (isset($address['cc_iso']))
            $arr['DestinationAddressCountryCode'] = $address['cc_iso'];

        $arr['NumberOfPieces'] = $number_of_pieces;

        $arr['ActualWeight'] = array(
            'Value' => $weight,
            'Unit' => 'KG'
        );

        $arr['ChargeableWeight'] = array(
            'Value' => $weight,
            'Unit' => 'KG'
        );


        $arr['id_countries'] = $address['id_countries'];
        /*$address_zip = array(14,42,255);*/
        $cond_address = array(253);
        $address_zip = array();

        if (!empty($address['city']) && !empty($address['cc_iso']) && ((in_array($address['id_countries'], $address_zip) && $address['postal_code'] != "") || (!in_array($address['id_countries'], $address_zip)))
        ) {

            if ($weight > 0) {

                if (in_array($address['id_countries'], $cond_address)) {
                    $cond['id_countries'] = $address['id_countries'];
                    $cond['weight'] = $weight;
                    $cond['city'] = $address['city'];
                    $cond['net_discount_price'] = $net_discount_price;
                    $rate = $this->fct->getShippingByCountry($cond);
                } else {
                    $rate = $this->fct->getRate($arr, TRUE);
                }


                if ($bool) {
                    $grand_total = changeCurrency($net_discount_price, false, "AED") + changeCurrency($rate['value'], false, 'AED');
                    $rate['shippingCharge'] = $rate['value'];
                    $rate['shippingCharge_currency'] = changeCurrency($rate['value']);
                    $rate['grand_total'] = $grand_total;
                    $rate['grand_total_currency'] = changeCurrency($grand_total);

                    return $rate;
                } else {
                    if ($rate['HasErrors'] == 1) {
                        $this->form_validation->set_message('checkAramex', $rate['Notifications']);
                        return false;
                        /*	$return['message'] = $rate['Notifications'];
			$return['result'] = 4;
			die(json_encode($return));*/
                    } else {
                        return true;
                    }
                }
            } else {

                $grand_total = changeCurrency($net_discount_price, false, "AED");
                $rate['shippingCharge'] = 0;
                $rate['shippingCharge_currency'] = 0;
                $rate['grand_total'] = $grand_total;
                $rate['grand_total_currency'] = changeCurrency($grand_total);
                return $rate;

            }
        } else {

            return true;
        }
    }

    public function getAddressInfo()
    {
        if ($this->input->post('use_billing_address') == 1) {

            $billing_select = $this->input->post('billing_select');
            $billing_user_address = "";
            if ($billing_select != '') {
                $billing_user_address = $this->fct->getonerecord('users_addresses', array('id_users_addresses' => $billing_select));
            }
            if ($billing_user_address == '') {
                $address['first_name'] = $this->input->post("billing_first_name");
                $address['last_name'] = $this->input->post("billing_last_name");
                $address['name'] = $address['first_name'] . ' ' . $address['last_name'];
                $address['company'] = $this->input->post("billing_company");
                $address['billing'] = 1;
                $address['phone'] = $this->input->post("billing_phone");
                $address['fax'] = $this->input->post("billing_fax");
                $address['street_one'] = $this->input->post("billing_street_one");
                $address['street_two'] = $this->input->post("billing_street_two");
                $address['city'] = $this->input->post("billing_city");
                $address['state'] = $this->input->post("billing_state");
                $address['postal_code'] = $this->input->post("billing_postal_code");
                $address['id_countries'] = $this->input->post("billing_country");

            } else {

                $address['first_name'] = $billing_user_address['first_name'];
                $address['last_name'] = $billing_user_address['last_name'];
                $address['name'] = $billing_user_address['name'];
                $address['company'] = $billing_user_address['company'];
                $address['phone'] = $billing_user_address['phone'];
                $address['fax'] = $billing_user_address['fax'];
                $address['billing'] = 1;
                $address['street_one'] = $billing_user_address['street_one'];
                $address['street_two'] = $billing_user_address['street_two'];
                $address['city'] = $billing_user_address['city'];
                $address['state'] = $billing_user_address['state'];
                $address['postal_code'] = $billing_user_address['postal_code'];

                $address['id_countries'] = $billing_user_address['id_countries'];
            }
        } else {

            $delivery_select = $this->input->post('delivery_select');
            $deliver_user_address = "";

            if ($delivery_select != '') {
                $deliver_user_address = $this->fct->getonerecord('users_addresses', array('id_users_addresses' => $delivery_select));
            }

            if ($deliver_user_address == '') {
                $address['first_name'] = $this->input->post("delivery_first_name");
                $address['last_name'] = $this->input->post("delivery_last_name");
                $address['name'] = $address['first_name'] . ' ' . $address['last_name'];
                $address['company'] = $this->input->post("deliver_company");
                $address['shipping'] = 1;
                $address['phone'] = $this->input->post("delivery_phone");
                $address['fax'] = $this->input->post("delivery_fax");
                $address['street_one'] = $this->input->post("delivery_street_one");
                $address['street_two'] = $this->input->post("delivery_street_two");
                $address['city'] = $this->input->post("delivery_city");
                $address['state'] = $this->input->post("delivery_state");
                $address['postal_code'] = $this->input->post("delivery_postal_code");
                $address['id_countries'] = $this->input->post("delivery_country");
            } else {
                $address['first_name'] = $deliver_user_address['first_name'];
                $address['last_name'] = $deliver_user_address['last_name'];
                $delivery['name'] = $deliver_user_address['name'];
                $address['company'] = $deliver_user_address['company'];
                $address['phone'] = $deliver_user_address['phone'];
                $address['fax'] = $deliver_user_address['fax'];
                $address['shipping'] = 1;
                $address['street_one'] = $deliver_user_address['street_one'];
                $address['street_two'] = $deliver_user_address['street_two'];
                $address['city'] = $deliver_user_address['city'];
                $address['state'] = $deliver_user_address['state'];
                $address['postal_code'] = $deliver_user_address['postal_code'];
                $address['id_countries'] = $deliver_user_address['id_countries'];
            }

        }
        $country = $this->fct->getonerecord('countries', array('id_countries' => $address['id_countries']));
        $address['cc_iso'] = $country['cc_iso'];
        return $address;
    }

    public function stepOne()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('checkout_method', 'checkout method', 'trim|required|xss_clean');

        if ($this->form_validation->run() == FALSE) {

            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
            die(json_encode($return));
        } // if success
        else {
            $return['result'] = 1;
            die(json_encode($return));
        }
    }

    public function stepTwo()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);
        $allPostedVals['billing_select'] = "";
        $user = $this->ecommerce_model->getUserInfo();
        $this->form_validation->set_rules('checkout_method', 'checkout method', 'trim|xss_clean');
        if ($this->input->post('use_billing_address') == 1) {
            $this->form_validation->set_rules('aramex', lang('billing'), 'trim|xss_clean|callback_checkAramex[]');
        }

//$this->form_validation->set_rules('use_billing_address',"shipping",'trim|required|xss_clean');
        if (checkUserIfLogin()) {
            $this->form_validation->set_rules('billing_select', "Billing Address", 'trim|required|xss_clean');
        } else {
            if ($this->input->post('billing_select') == "") {

                $this->form_validation->set_rules('billing_email', lang('billing') . ': ' . lang('email'), 'trim|required|valid_email|xss_clean|callback_check_if_email_exists[]');
                //$this->form_validation->set_rules('billing_username',lang('billing').':Username','trim|required|xss_clean|callback_check_if_username_exists[]');

                $this->form_validation->set_rules('billing_select', lang('billing'), 'trim|xss_clean');
                $this->form_validation->set_rules('billing_first_name', lang('billing') . ': ' . lang('first_name'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('billing_last_name', lang('billing') . ': ' . lang('last_name'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('billing_phone', lang('delivery') . ': ' . lang('phone'), 'trim|required|xss_clean]');
                $this->form_validation->set_rules('billing_company', lang('billing') . ': ' . lang('company'), 'trim|xss_clean');
                $this->form_validation->set_rules('billing_postal_code', lang('billing') . ': ' . lang('postal_code'), 'trim|xss_clean|callback_checkPostalCode[]');

                $this->form_validation->set_rules('billing_country', lang('billing') . ': ' . lang('country'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('billing_state', lang('billing') . ': ' . lang('state'), 'trim|xss_clean');
                $this->form_validation->set_rules('billing_street_one', lang('billing') . ': street address', 'trim|required|xss_clean');
                $this->form_validation->set_rules('billing_street_two', lang('billing') . ': street address', 'trim|xss_clean');
                $this->form_validation->set_rules('billing_city', lang('billing') . ': ' . lang('city'), 'trim|required|xss_clean');
            }
        }


        if ($this->form_validation->run() == FALSE) {

            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
            die(json_encode($return));
        } // if success
        else {


            if ($this->input->post('billing_select') == "") {
                $id_country = $this->input->post('billing_country');
            } else {
                $cond['id_users_addresses'] = $this->input->post('billing_select');
                $address = $this->ecommerce_model->getUserAddresses($cond, 1, 0);
                $id_country = $address['id_countries'];
            }
            if ($this->input->post('use_billing_address') == 1) {
                /*if($id_country!="253"){*/
                $aramex = $this->checkAramex(true);
                $return['aramex'] = $aramex;
                $return['aramexStatus'] = 1;

                $cart_items = $this->cart->contents();
                if (empty($cart_items))
                    $cart_items = $this->ecommerce_model->loadUserCart();
                $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);
                $data['shippingCharge'] = $aramex['shippingCharge'];
                $data['section'] = "checkout";
                $return['cart'] = $this->load->view('cart/cart', $data, true);

                /*}else{
			$return['aramexStatus']=0;
				}*/
            }

            if ($this->input->post('use_billing_address') == 2) {
                $cond2['id_user'] = $user['id_user'];
                $cond2['branch'] = 1;
                $tabs_title = "Branch Information";
                $data2['select_title'] = "Select a branch from your address book or enter a new branch.";
                $data2['user_addresses_delivery'] = $this->ecommerce_model->getUserAddresses($cond2);
            } else {
                $cond2['id_user'] = $user['id_user'];
                $cond2['shipping'] = 1;
                $data2['select_title'] = "Select a shipping address from your address book or enter a new address.";
                $tabs_title = "Shipping Information";
                $data2['user_addresses_delivery'] = $this->ecommerce_model->getUserAddresses($cond2);
            }


            $shipping_address = $this->load->view('load/shipping_address', $data2, true);
            $return['shipping_address'] = $shipping_address;
            $return['tabs_title'] = $tabs_title;
            $return['id_country'] = $id_country;
            $return['result'] = 1;


            die(json_encode($return));
        }
    }

    public function stepThree()
    {

        $allPostedVals = $this->input->post(NULL, TRUE);

        $this->form_validation->set_rules('user_billing_address', lang('delivery') . ': ' . lang('user_billing_address'), 'trim|xss_clean');

        $this->form_validation->set_rules('aramex', lang('delivery'), 'trim|xss_clean|callback_checkAramex[]');
        if (checkUserIfLogin()) {
            $this->form_validation->set_rules('shipping_select', "Shipping Address", 'trim|required|xss_clean');
        } else {
            if ($this->input->post('delivery_select') == "" && ($this->input->post('use_billing_address') == 0 || $this->input->post('use_billing_address') == 2)) {

                if ($this->input->post('use_billing_address') == 2) {

                    $this->form_validation->set_rules('branch_name', ' branch name', 'trim|required|xss_clean');
                }
                $this->form_validation->set_rules('delivery_first_name', lang('delivery') . ': ' . lang('first_name'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('delivery_last_name', lang('delivery') . ': ' . lang('last_name'), 'trim|required|xss_clean');
                //$this->form_validation->set_rules('delivery_phone',lang('delivery').': '.lang('phone'),'trim|required|xss_clean');
                $this->form_validation->set_rules('delivery_phone', lang('delivery') . ': ' . lang('phone'), 'trim|required|xss_clean');
                $this->form_validation->set_rules('delivery_company', lang('delivery') . ': ' . lang('company'), 'trim|xss_clean');
                $this->form_validation->set_rules('delivery_postal_code', lang('delivery') . ': ' . lang('postal_code'), 'trim|xss_clean');
                $this->form_validation->set_rules('delivery_country', lang('delivery') . ': ' . lang('country'), 'trim|xss_clean');
                $this->form_validation->set_rules('delivery_state', lang('delivery') . ': ' . lang('state'), 'trim|xss_clean');
                $this->form_validation->set_rules('delivery_street_one', lang('delivery') . ': street address', 'trim|required|xss_clean');
                $this->form_validation->set_rules('delivery_street_two', lang('delivery') . ': street address', 'trim|xss_clean');
                $this->form_validation->set_rules('delivery_city', lang('delivery') . ': ' . lang('city'), 'trim|required|xss_clean');
            }
        }
        if ($this->form_validation->run() == FALSE) {

            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
            die(json_encode($return));
        } // if success
        else {
            $return['result'] = 1;
            if ($this->input->post('delivery_select') == "" && ($this->input->post('use_billing_address') == 0 || $this->input->post('use_billing_address') == 2)) {
                $id_country = $this->input->post('delivery_country');
            } else {
                $cond['id_users_addresses'] = $this->input->post('delivery_select');
                $address = $this->ecommerce_model->getUserAddresses($cond, 1, 0);
                $id_country = $address['id_countries'];
            }


            /*if($id_country!="253"){	*/
            $aramex = $this->checkAramex(true);
            $return['aramex'] = $aramex;
            $data['shippingCharge'] = $aramex['shippingCharge'];
            $return['aramexStatus'] = 1;/*}else{
			$return['aramexStatus']=0;
			$data['shippingCharge']=0;
				}*/
            $cart_items = $this->cart->contents();
            if (empty($cart_items))
                $cart_items = $this->ecommerce_model->loadUserCart();
            $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);
            $data['section'] = "checkout";
            $return['cart'] = $this->load->view('cart/checkbox-cart', $data, true);
            $return['id_country'] = $id_country;
            die(json_encode($return));
        }
    }

    public function stepFour()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);

        $this->form_validation->set_rules('payment_method', 'Payment Method', 'trim|required|xss_clean');
        if ($this->input->post('payment_method') == "credits") {

            $this->form_validation->set_rules('user_credit', "invalid", 'trim|xss_clean|callback_check_if_user_credits_valid[]');
        }


        if ($this->form_validation->run() == FALSE) {

            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            $return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
            die(json_encode($return));
        } // if success
        else {
            $payment_method = $this->input->post('payment_method');
            $this->session->set_userdata('payment_method', $payment_method);
            $return['result'] = 1;

            $return['aramex'] = 1;
            $data['shippingCharge'] = $this->input->post('shippingCharge');
            $return['aramexStatus'] = 1;
            $cart_items = $this->cart->contents();
            if (empty($cart_items))
                $cart_items = $this->ecommerce_model->loadUserCart();
            $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);
            $data['section'] = "checkout";
            $return['cart'] = $this->load->view('cart/checkbox-cart', $data, true);
            die(json_encode($return));
        }
    }

    /************************************************************************************************/

    public function add_promotion()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('code', lang('code'), 'trim|required|callback_validatecode[]');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $find = array('<p>', '</p>');
            $replace = array('', '');
            $return['message'] = str_replace($find, $replace, validation_errors());
        } else {
            $code = $this->input->post('code');
            $cart_items = $this->cart->contents();
            $products = $this->ecommerce_model->getCartProducts($cart_items);
            //print '<pre>';print_r($products);exit;
            $total_price = 0;
            $default_net_price = $this->cart->total();
            foreach ($products as $prod) {
                $id_stock = 0;
                $id = $prod['product_info']['id_products'];
                $qty = $prod['qty'];
                $p_list_price = $prod['product_info']['list_price'];
                $p_discount_expiration = $prod['product_info']['discount_expiration'];
                $p_price = $prod['product_info']['price'];
                if (!empty($prod['options'])) {
                    $options = $prod['options'];
                    $results = $this->ecommerce_model->getOptionsByIds($options);
                    $combination = serializecartOptions($results);
                    $stock = $this->fct->getonerow('products_stock', array('id_products' => $id, 'combination' => $combination));
                    if (!empty($stock)) {
                        $p_list_price = $stock['list_price'];
                        $p_discount_expiration = $stock['discount_expiration'];
                        $p_price = $stock['price'];
                        $id_stock = $stock['id_products_stock'];
                    }
                }
                // check for price segments
                $price_segment = $this->ecommerce_model->checkPriceSegment($id, $id_stock, $qty);
                // get discount by code
                $dis = $this->ecommerce_model->getDiscountByCode($code);
                //if(!empty($price_segment) && )
                if ($price_segment != '' && $price_segment < $p_price) {
                    $newprice = $price_segment;
                } else {
                    $newprice = $p_price;
                }
                //echo $newprice.' - '.addDiscount($p_list_price,$dis['discount']);exit;
                if ($newprice > addDiscount($p_list_price, $dis['discount'])) {
                    $newprice = addDiscount($p_list_price, $dis['discount']);
                }
                $total_price = round($total_price + ($newprice * $qty));
            }
            $total_discount = $default_net_price - $total_price;
            $return['result'] = 1;
            if ($total_discount == 0) {
                $return['message'] = '<span>' . lang('discount_not_added') . '</span>';
            } else {
                $return['message'] = '<span style="color:green">' . lang('discount_added') . '</span>';
            }
            $return['html'] = '';
            $return['html'] .= '<table border="1" cellpadding="10" cellspacing="0">';
            $return['html'] .= '<tr>';
            $return['html'] .= '<td>';
            $return['html'] .= '<span>' . lang('cart_total_price') . ':</span></td>';
            $return['html'] .= '<td>' . changeCurrency($default_net_price);
            $return['html'] .= '</td>';
            $return['html'] .= '</tr>';
            $return['html'] .= '<tr>';
            $return['html'] .= '<td>';
            $return['html'] .= '<span>' . lang('discount') . ':</span></td>';
            $return['html'] .= '<td>' . changeCurrency($total_discount);
            $return['html'] .= '</td>';
            $return['html'] .= '</tr>';
            $return['html'] .= '<tr>';
            $return['html'] .= '<td>';
            $return['html'] .= '<span>' . lang('cart_net_price') . ':</span></td>';
            $return['html'] .= '<td>' . changeCurrency($total_price);
            $return['html'] .= '<input type="hidden" name="order_total_price" value="' . $default_net_price . '" />';
            $return['html'] .= '<input type="hidden" name="order_discount" value="' . $total_discount . '" />';
            $return['html'] .= '<input type="hidden" name="order_net_price" value="' . $total_price . '" />';
            $return['html'] .= '</td>';
            $return['html'] .= '</tr>';
            $return['html'] .= '</table>';
        }
        echo json_encode($return);
    }

    public function remove_promotion()
    {
        $return['html'] = '';
        $return['html'] .= '<table border="1" cellpadding="10" cellspacing="0">';
        $return['html'] .= '<tr>';
        $return['html'] .= '<td>';
        $return['html'] .= '<span>' . lang('cart_net_price') . ':</span></td>';
        $return['html'] .= '<td>' . changeCurrency($this->cart->total());
        $return['html'] .= '<input type="hidden" name="order_total_price" value="' . $this->cart->total() . '" />';
        $return['html'] .= '<input type="hidden" name="order_discount" value="0" />';
        $return['html'] .= '<input type="hidden" name="order_net_price" value="' . $this->cart->total() . '" />';
        $return['html'] .= '</td>';
        $return['html'] .= '</tr>';
        $return['html'] .= '</table>';
        echo json_encode($return);
    }

    public function verified_address()
    {
        require_once("./easypost/lib/easypost.php");
        \EasyPost\EasyPost::setApiKey('cueqNZUb3ldeWTNX7MU3Mel8UXtaAMUi');

        try {
            // create address
            $address_params = array("name" => "Sawyer Bateman",
                "street1" => "388 Townasend St",
                //"street2" => "Apt 20",
                "city" => "San Francisco",
                "state" => "CA",
                "zip" => "94107",
                "country" => "US");

            $address = \EasyPost\Address::create($address_params);
            print_r($address);

            // retrieve
            $retrieved_address = \EasyPost\Address::retrieve($address->id);
            print_r($retrieved_address);

            // verify
            $verified_address = $address->verify();
            print_r($verified_address);

            // create and verify at the same time
            $verified_on_create = \EasyPost\Address::create_and_verify($address_params);
            print_r($verified_on_create);

            // all
            // $all = \EasyPost\Address::all();
            //print_r($all);

        } catch (Exception $e) {
            echo "Status: " . $e->getHttpStatus() . ":\n";
            echo $e->getMessage();
            if (!empty($e->param)) {
                echo "\nInvalid param: {$e->param}";
            }
            exit();
        }

        $return['price'] = "2000";
        die(json_encode($return));

    }

    public function getShippingPrice()
    {
        require_once("./easypost/lib/easypost.php");
        \EasyPost\EasyPost::setApiKey('cueqNZUb3ldeWTNX7MU3Mel8UXtaAMUi');

        try {
            // create address
            $address_params = array("name" => "Sawyer Bateman",
                "street1" => "388 Townasend St",
                //"street2" => "Apt 20",
                "city" => "San Francisco",
                "state" => "CA",
                "zip" => "94107",
                "country" => "US");

            $address = \EasyPost\Address::create($address_params);
            /*   print_r($address);*/

            // retrieve
            /*   $retrieved_address = \EasyPost\Address::retrieve($address->id);
    print_r($retrieved_address);*/

            // verify
            $verified_address = $address->verify();
            print_r($verified_address);

            // create and verify at the same time
            /*   $verified_on_create = \EasyPost\Address::create_and_verify($address_params);
    print_r($verified_on_create);*/

            // all
            // $all = \EasyPost\Address::all();
            //print_r($all);

        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            echo "Status: " . $e->getHttpStatus() . ":\n";
            echo $e->getMessage();
            if (!empty($e->param)) {
                echo "\nInvalid param: {$e->param}";
            }
            exit();
        }

        $return['price'] = "2000";
        die(json_encode($return));

    }

}
