<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class is loaded via application/core/MY_Controller.php file
 *
 * We can get requested controller either by get_class($this) or $this->router->fetch_class()
 * We can get request method by $this->router->fetch_method()
 */

abstract class BaseController extends CI_Controller
{

    /** @var CI_Template $template */
    public $template;

    /** @var  CI_Form_validation $form_validation */
    public $form_validation;

    /** @var  CI_Input $input */
    public $input;

    /** @var  CI_Config $config */
    public $config;

    /** @var  CI_Lang $lang */
    public $lang;

    /** @var Fct $fct */
    public $fct;

    /** @var Acl $acl */
    public $acl;

    /** @var Ecommerce_model $ecommerce_model */
    public $ecommerce_model;

    /** @var CI_Session $session */
    public $session;

    /** @var Send_emails $send_emails */
    public $send_emails;

    /** @var Export_fct $export_fct */
    public $export_fct;

    /** @var \core\ServiceLocator $serviceLocator Loaded in PostControllerHook*/
    public $serviceLocator;


    public function __construct()
    {
        parent::__construct();
    }

}