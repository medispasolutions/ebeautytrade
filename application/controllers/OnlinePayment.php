<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OnlinePayment extends CI_Controller {
	public function __construct()
{
	  parent::__construct();
}
    public function index()
    {

        $this->template->write_view('content', 'payment/OnlinePayment', $data);

        $this->template->render();
    }
}