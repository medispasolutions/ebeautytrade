<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class FailedLadingPayment extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->template->write_view('header', 'blocks/header', $data);
        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'payment/FailedLadingpagePayment', $data);
        $this->template->write_view('footer', 'blocks/footer', $data);
        $this->template->render();
    }
    
}