<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        //echo _getenv('DB_BASE');

        $data['login'] = checkUserIfLogin();


        if ($this->session->userdata('currency') == "") {
            $this->session->set_userdata('currency', $this->config->item("default_currency"));
        }
        $data['seo'] = $this->fct->getonerow('static_seo_pages', array('id_static_seo_pages' => 1));
        $cond['display_in_home'] = 1;
        $data['products'] = $this->ecommerce_model->getProducts($cond, 8, 0);
        $data['page_ads'] = "home";

        if ($this->session->userdata('login_id') == '') {
            $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s://" : "://") . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $this->session->set_userdata('redirect_link', $url);
        }


        $category_ads = "";
        $id_user_ads = "";
        $type = "";

        $page_ads = "home";
        $block_ads = "home_bottom_left";

        $data['home_bottom_ads_left'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads, $category_ads, $id_user_ads);

        $page_ads = "home";
        $block_ads = "home_bottom_right";

        $data['home_bottom_ads_right'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads, $category_ads, $id_user_ads);

        $page_ads = "home";
        $block_ads = "ads_steps";

        $data['ads_steps'] = $this->custom_fct->getBannerAds($page_ads, $type, $block_ads, $category_ads, $id_user_ads);

        $data['blogs'] = $this->custom_fct->getBlgs();
        $data['featuredblogs'] = $this->custom_fct->getfeaturedBlgs();

        /*$data['featured'] = $this->custom_fct->getBanner();*/
        /*		$cond_p=array();
                $cond_p['set_as_trends_updates']=1;
                $data['products'] = $this->ecommerce_model->getProducts($cond_p, 3, 0, 'created_date', 'desc');*/
        $cond_p = array();
        $cond_p['promote_to_front'] = 1;
        $cond_p['list'] = 1;
        $data['featured'] = $this->ecommerce_model->getProducts($cond_p, 16, 0, 'created_date', 'desc');

        $cond_p = array();
        $cond_p['set_as_oed'] = 1;
        $cond_p['list'] = 1;
        $data['special_products'] = $this->ecommerce_model->getProducts($cond_p, 16, 0, 'created_date', 'desc');

        $data['featured_brands'] = $this->custom_fct->getBrands(array('set_as_featured' => 1, 'status' => 1), 8, 0);

        //$data['trends_updates']=$this->fct->getRecords('trends_and_updates',array(),20,0);
        $this->template->write_view('header', 'blocks/header', $data);

        $this->template->write_view('quarter_left_sideBar', 'blocks/quarter_left_sideBar', $data);
        $this->template->write_view('content', 'blocks/banner', $data);
        $this->template->write_view('content', 'content/home', $data);

        $this->template->write_view('footer', 'blocks/footer', $data);

        $data['private_policy'] = $this->fct->getonerow('dynamic_pages', array('id_dynamic_pages' => 5));
        $data['disclaimer'] = $this->fct->getonerow('dynamic_pages', array('id_dynamic_pages' => 6));

        if ($this->input->get('open') == "meeting") {
            $data['popup_content'] = $this->load->view('popup/meeting', $data, true);
            $this->template->write_view('footer', 'popup/template', $data);
        }

        if ($this->input->get('open') == "afterRegistration") {
            $data['popup_content'] = $this->load->view('popup/afterRegistration', $data, true);
            $this->template->write_view('footer', 'popup/template', $data);
        }

        if ($this->input->get('open') == "afterOrder") {
            $data['popup_content'] = $this->load->view('popup/afterOrder', $data, true);
            $this->template->write_view('footer', 'popup/template', $data);
        }
        
        if ($this->input->get('open') == "afterPaymentSuccess") {
            $data['popup_content'] = $this->load->view('popup/afterPaymentSuccess', $data, true);
            $this->template->write_view('footer', 'popup/template', $data);
            
        }
        
        if ($this->input->get('open') == "afterApplicationSubmitted") {
            $data['popup_content'] = $this->load->view('popup/afterApplicationSubmittedNoPayement', $data, true);
            $this->template->write_view('footer', 'popup/template', $data);
        }

        $this->template->render();
    }

    public function session()
    {

        $this->session->set_userdata('redirect_link', $this->input->post('session'));
        die(1);
    }

    public function check()
    {
        $login = checkUserIfLogin();
        if ($login) {
            $result = 1;
            $redirect_link = "";
        } else {


            $result = 0;
        }


        $this->session->set_userdata('redirect_link', 'dsds');
        $return['result'] = 0;


        die(json_encode($return));
    }

    public function resizeImages()
    {
        $sourceimage = "./front/img/cart.png";
        $resize_settings['image_library'] = 'gd2';
        $resize_settings['source_image'] = $sourceimage;
        $resize_settings['maintain_ratio'] = false;
        $resize_settings['quality'] = '10%';
        $this->load->library('image_lib', $resize_settings);


        $resize_settings['new_image'] = 'cartfsf.png';
        $this->image_lib->initialize($resize_settings);
        $this->image_lib->resize();
    }

    public function change_currency($cur)
    {
        $this->session->set_userdata('currency', $cur);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function checkemailexistance()
    {
        $email = $this->input->post('newsletter_email');
        $check = $this->fct->getonerow('newsletter', array('email' => $email));
        if (empty($check)) {
            return true;
        } else {
            $this->form_validation->set_message('checkemailexistance', 'Your email exists, please try another email.');
            return FALSE;
        }
    }

    public function subscribe()
    {
        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('newsletter_email', 'Email', 'trim|required|valid_email|callback_checkemailexistance[]');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {
            $_data['email'] = $this->input->post('newsletter_email');
            $_data['created_date'] = date('Y-m-d h:i:s');
            $this->db->insert('newsletter', $_data);
            // send emails
            $this->load->model('send_emails');
            $this->send_emails->sendContactToAdmin($_data, 8);
            $this->send_emails->sendContactToUser($_data, 8);
            $return['result'] = 1;
            $return['message'] = 'Your email is added to our newsletter.';
        }
        echo json_encode($return);
    }


    public function send_meeting()
    {
        //print '<pre>';
        //print_r($_POST);exit;

        $allPostedVals = $this->input->post(NULL, TRUE);
        $this->form_validation->set_rules('name', lang('name'), 'trim|required');
        $this->form_validation->set_rules('phone', lang('phone'), 'trim|required');
        $this->form_validation->set_rules("phone", "phone", "callback_phone[]");
        $this->form_validation->set_rules('email', lang('email'), 'trim|required|valid_email');
        $this->form_validation->set_rules('message', lang('message'), 'trim|required');
        //$this->form_validation->set_rules('captcha',lang('captcha'),'trim|required|xss_clean|callback_validate_captcha[]');
        if ($this->form_validation->run() == FALSE) {
            $return['result'] = 0;
            $return['errors'] = array();
            $return['message'] = 'Error!';
            //$return['captcha'] = $this->fct->createNewCaptcha();
            $find = array('<p>', '</p>');
            $replace = array('', '');
            foreach ($allPostedVals as $key => $val) {
                if (form_error($key) != '') {
                    $return['errors'][$key] = str_replace($find, $replace, form_error($key));
                }
            }
        } else {


            $_data['name'] = $this->input->post('name');
            $_data['email'] = $this->input->post('email');

            $phone = $this->input->post("phone");
            $_data['phone'] = $phone;

            $_data['id_brands'] = $this->input->post('id_brands');
            $_data['type'] = 'meeting_request_spamiles';
            $_data['message'] = $this->input->post('message');
            $_data['created_date'] = date('Y-m-d h:i:s');
            $_data['lang'] = $this->lang->lang();
            $this->db->insert('contactform', $_data);
            $new_id = $this->db->insert_id();
            $_data['id_message'] = $new_id;
            // send emails
            $brand = $this->custom_fct->getOneBrand($_data['id_brands']);
            $_data['brand'] = $brand;

            $this->load->model('send_emails');
            //$this->send_emails->sendRequestToAdmin($_data);
            $return['result'] = 1;
            $return['message'] = lang('contact_success');
        }
        echo json_encode($return);
    }


///////////////////////////////////Main MEnu Drop Down Content////////////////////////////////////////
    public function loadBanner()
    {
        $data['banner'] = $this->custom_fct->getBanner();
        shuffle($data['banner']);

        $html = $this->load->view('load/banner', $data, true);
        die(json_encode($html));
    }

    public function getVideo($id = "")
    {

        $data['result'] = $this->fct->getonerecord('banner_ads', array('id_banner_ads' => $id));

        if (!empty($data['result'])) {
            $data['video'] = $data['result']['video'];
            $html = $this->load->view('load/video', $data, true);
            echo $html;
            exit;
        }
    }

    public function newsletter()
    {
        $data = array();
        $html = $this->load->view('load/newsletter', $data, true);
        echo $html;
        exit;
    }

///////////////////////////////////Main MEnu Drop Down Content////////////////////////////////////////
    public function getWidgets()
    {
        $widgets = $this->load->view('load/widgets', array(), true);
        $return['widgets'] = $widgets;

        die(json_encode($return));
    }

}