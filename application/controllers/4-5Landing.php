<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class LLanding extends BaseShopController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->template->write_view('content', 'content/Landing', $data);
        $this->template->render();
    }
    
    public function send()
    {

        $randomNumber = $this->input->post('random-number');
        
        if($this->input->post('uae-free-brand-number')){
            $planselected = $this->input->post('uae-free-brand-number');
        } else if($this->input->post('uae-gold-brand-number')){
            $planselected = $this->input->post('uae-gold-brand-number');
        } else if($this->input->post('uae-fbe-brand-number')){
            $planselected = $this->input->post('uae-fbe-brand-number');
        } else if($this->input->post('overseas-free-brand-number')){
            $planselected = $this->input->post('overseas-free-brand-number');
        } else if($this->input->post('overseas-gold-brand-number')){
            $planselected = $this->input->post('overseas-gold-brand-number');
        } else if($this->input->post('overseas-fbe-brand-number')){
            $planselected = $this->input->post('overseas-fbe-brand-number');
        }
        
        if($this->input->post('application-type') == 1){

            $_data['first_name'] = $this->input->post('first-name');
            $_data['last_name'] = $this->input->post('last-name');
            $_data['position'] = $this->input->post('position');
            $_data['phone'] = $this->input->post('phone');
            $_data['email'] = $this->input->post('email');
            $_data['company_name'] = $this->input->post('company-name');
            $_data['company_number'] = $this->input->post('company-number');
            $_data['company_address'] = $this->input->post('company-address');
            $_data['registration_number'] = $this->input->post('registration-number');
            $_data['business_based'] = $this->input->post('business-based');
            $_data['city'] = $this->input->post('city');
            $_data['website'] = $this->input->post('website');
            $_data['application_type'] = $this->input->post('application-type');
            $_data['brand_description'] = $this->input->post('brand-description');
            $_data['plan_selected'] = $planselected;
            $_data['random_number'] = $randomNumber;
            $_data['terms_condition'] = '1';
            
            $number = $this->input->post('amountofbrands');
        	for($i=1; $i<=$number; $i++)
        	{
    			$_branddata['brand_name'] = $this->input->post('brand-name'.$i);
    			$_branddata['country'] = $this->input->post('country'.$i);
    			$_branddata['brand_website'] = $this->input->post('brand-website'.$i);
    			$_branddata['random_number'] = $randomNumber;
    			$_branddata['created_date'] = date('Y-m-d h:i:s');
    			$this->db->insert('application_brand', $_branddata);
        	}

            /*$_data['captcha'] = $this->input->post('captcha');*/
            $_data['created_date'] = date('Y-m-d h:i:s');
    
            $this->db->insert('supplier_application', $_data);
            $new_id = $this->db->insert_id();
            $_data['id_message'] = $new_id;
    
            $return['result'] = 1;
            $return['message'] = lang('landing_success');

            $_userdata['company_name'] = $this->input->post('company-name');
            $_userdata['name'] = $this->input->post('first-name').' '.$this->input->post('last-name');
            $_userdata['first_name'] = $this->input->post('first-name');
            $_userdata['last_name'] = $this->input->post('last-name');
            $_userdata['email'] = $this->input->post('email');
            $_userdata['position'] = $this->input->post('position');
            $_userdata['password'] = md5($randomNumber.'2019');
            $_userdata['phone'] = $this->input->post('phone');
            $_userdata['address_1'] = $this->input->post('company-address');
            $_userdata['company_phone'] = $this->input->post('company-number');
            $_userdata['company_website'] = $this->input->post('website');
            $_userdata['deleted'] = '0';
            $_userdata['created_date'] = date('Y-m-d h:i:s');
            $_userdata['sort_order'] = '0';
            $_userdata['id_roles'] = '5';
            $_userdata['level'] = '0';
            $_userdata['id_countries'] = $this->input->post('first-name');
            $_userdata['city'] = $this->input->post('city');
            $_userdata['created_date'] = date('Y-m-d h:i:s');
            $_userdata['random_number'] = $this->input->post('random-number');
            $_userdata['plan_selected'] = $planselected;
            
            if($planselected == "free1" || $planselected == "free2" || $planselected == "free3" || $planselected == "free4"){
                
                $_userdata['id_user_type'] = '2';
                $_userdata['status'] = '1';
                $this->db->insert('user', $_userdata);
                
                $return['redirect'] = 'https://ebeautytrade.com/landing-page-payment?'.$randomNumber;
                echo json_encode($return);
                
            } else if($planselected == "gold1" || $planselected == "gold2" || $planselected == "gold3" || $planselected == "gold4"){
                
                $_userdata['id_user_type'] = '3';
                $_userdata['status'] = '3';
                $_userdata['expire_date'] = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
                $this->db->insert('user', $_userdata);
                
                $return['redirect'] = 'https://ebeautytrade.com/landing-page-payment?'.$randomNumber;
                echo json_encode($return);
                
            } else {
                
                $_userdata['id_user_type'] = '4';
                $_userdata['status'] = '3';
                $_userdata['expire_date'] = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
                $this->db->insert('user', $_userdata);
                
                $return['redirect'] = 'https://ebeautytrade.com/landing-page-payment?'.$randomNumber;
                echo json_encode($return);
                
            }
        
        } else {
            
            $_data['first_name'] = $this->input->post('first-name');
            $_data['last_name'] = $this->input->post('last-name');
            $_data['position'] = $this->input->post('position');
            $_data['phone'] = $this->input->post('phone');
            $_data['email'] = $this->input->post('email');
            $_data['company_name'] = $this->input->post('company-name');
            $_data['company_number'] = $this->input->post('company-number');
            $_data['company_address'] = $this->input->post('company-address');
            $_data['registration_number'] = $this->input->post('registration-number');
            $_data['business_based'] = $this->input->post('business-based');
            $_data['city'] = $this->input->post('city');
            $_data['website'] = $this->input->post('website');
            $_data['application_type'] = $this->input->post('application-type');
            $_data['brand_description'] = $this->input->post('brand-description');
            $_data['plan_selected'] = $this->input->post('planselected');
            $_data['random_number'] = $randomNumber;
            $_data['terms_condition'] = '1';
            
            $number = $this->input->post('Overseasamountofbrands');
        	for($i=1; $i<=$number; $i++)
        	{
    			$_branddata['brand_name'] = $this->input->post('brand-name'.$i);
    			$_branddata['country'] = $this->input->post('country'.$i);
    			$_branddata['brand_website'] = $this->input->post('brand-website'.$i);
    			$_branddata['random_number'] = $randomNumber;
    			$_branddata['created_date'] = date('Y-m-d h:i:s');
    			$this->db->insert('application_brand', $_branddata);
        	}
        	
            /*$_data['captcha'] = $this->input->post('captcha');*/
            $_data['created_date'] = date('Y-m-d h:i:s');
    
            $this->db->insert('supplier_application', $_data);
            $new_id = $this->db->insert_id();
            $_data['id_message'] = $new_id;
    
            /* send emails
            //$_data['lang']=$lang;
            $this->load->model('send_emails');
            $this->send_emails->sendContactUsToAdmin($_data);
            $this->send_emails->sendContactUsToUser($_data);
            //$this->session->set_flashdata('success_message',lang('contact_success'));
            */
            
            $_userdata['company_name'] = $this->input->post('company-name');
            $_userdata['name'] = $this->input->post('first-name').' '.$this->input->post('last-name');
            $_userdata['first_name'] = $this->input->post('first-name');
            $_userdata['last_name'] = $this->input->post('last-name');
            $_userdata['email'] = $this->input->post('email');
            $_userdata['position'] = $this->input->post('position');
            $_userdata['password'] = md5($randomNumber.'2019');
            $_userdata['phone'] = $this->input->post('phone');
            $_userdata['address_1'] = $this->input->post('company-address');
            $_userdata['company_phone'] = $this->input->post('company-number');
            $_userdata['company_website'] = $this->input->post('website');
            $_userdata['deleted'] = '0';
            $_userdata['created_date'] = date('Y-m-d h:i:s');
            $_userdata['sort_order'] = '0';
            $_userdata['id_roles'] = '5';
            $_userdata['level'] = '0';
            $_userdata['status'] = '2';
            $_userdata['id_user_type'] = '0';
            $_userdata['id_countries'] = $this->input->post('first-name');
            $_userdata['city'] = $this->input->post('city');
            $_userdata['random_number'] = $this->input->post('random-number');
            $this->db->insert('user', $_userdata);
                
             /*   
            $this->load->model('send_emails');
            $this->send_emails->sendReplyToOverseasAppicant($_userdata);
            $this->send_emails->sendEmailToAdminForOverseasApplicant($_userdata);
                */
                
            $return['result'] = 1;
            $return['message'] = lang('landing_success');
            $return['redirect'] = 'https://ebeautytrade.com/landing-page-payment?'.$randomNumber;
        ?>
            <script>window.location = 'https://ebeautytrade.com/landing-page-payment?'<?php echo $randomNumber; ?></script>
        <?php          
        }
    }
}