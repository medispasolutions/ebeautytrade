<?php
$lang['title_landing']="نرحب بك في صفحة تسجيل فلل حياة";
$lang['landing-lbl'] = "للإستفسار عن التملك  فضلاً إترك تفاصيل الاتصال بك لتكون أول من يعلم";

/////////////////////////////////FORM\\\\\\\\\\\\\\\\\\\\\

$lang['dowgroup_note'] = " <a href='http://www.dowgroup.com'>مجموعة ضوّ </a> لا تتحمل أي مسؤولية عن دقة وقانونية  المحتوى على هذا الموقع. لا يمكننا أن نقبل أي مسؤولية تأتى من المحتوى/المعلومات المتضمنة في هذا الموقع .";
$lang['surname'] = "اللقب";
$lang['full_name'] = "الإسم";
$lang['city'] = "المدينة";
$lang['partners'] = "الشركاء";
$lang['country'] = "البلد";
$lang['address'] = "العنوان";
$lang['gender'] = "الجنس";
$lang['male'] = "ذكر";
$lang['female'] = "أنثى";
$lang['phone_number'] = "رقم الهاتف";
$lang['phone'] = "رقم الهاتف";
$lang['mobile'] = "رقم الهاتف الجوال";
$lang['email'] = "البريد الإلكتروني";
$lang['email_address'] =  "البريد الإلكتروني";
$lang['message'] = "تعليق";
$lang['comments'] = "تعليق";
$lang['location_map'] = "خريطة الموقع";
$lang['locations_and_contact_information'] = "مواقع ومعلومات الاتصال";
$lang['send_us_a_message'] = "أرسل رسالة";
$lang['send_message'] = "أرسل";
$lang['complete_registration'] = "نشكر لك التسجيل معنا ونتطلع لخدمتك دائماً";
$lang['required_field_note'] = 'جميع الحقول التى بجوارها (<span class="required">*</span>) هي مطلوبة';
$lang['submit'] = "إرسال";
$lang['contact_form'] = "نموذج الاتصال";
$lang['contact_us'] = "تواصل معنا";
$lang['contact_us_txt'] = "نحن دائما على استعداد لمساعدتك";
$lang['contact_success'] = "شكراً على إتصالك! طلبك وصلنا بنجاح وسوف نكون على تواصل معك قريباً.";
$lang['our_clinic'] = "العيادة";


/////////////////////////////////Main Menu\\\\\\\\\\\\\\\\\\\\\
$lang['home'] = "الصفحة الرئيسية";
$lang['about_us'] = "من نحن";
$lang['our_services'] = "خدماتنا";
$lang['concierge_services'] = "الخدمة المتميزة للعملاء";
$lang['retailer'] = "متاجر التجزئة";
$lang['faqs'] = "أسئلة شائعة";
$lang['media_center'] = "المركز الاعلامي";
$lang['blogs'] = "مدوتنا الإلكترونية";
$lang['gallery'] = "معرض صور";
$lang['login'] = "تسجيل الدخول";
$lang['register'] = "إنشاء حساب";

$lang['select_language'] = "اختار اللغة";
$lang['coming_soon'] = "قريبا...";
$lang['menu'] = "القائمة";
$lang['you_have_been_successfully_registered_as_an']="لقد تم انشاء حساب خاص بك في";
/////////////////////////////////Footer\\\\\\\\\\\\\\\\\\\\\
$lang['footer_phone_txt'] = "الوصول إلى أحد مستشاري المبيعات لدينا اليوم";
$lang['location'] = "موقعنا";
$lang['toll_free'] = "الرقم المجاني";
$lang['rights_reserved'] = "الحقوق محفوظة";
$lang['alessia_f'] = "© 2015 أليسيا";
$lang['by_dow'] = "بواسطة مجموعة ضوّ";
$lang['alessia'] = "أليسيا";
$lang['share_this_video'] = "شارك هذا الفيديو";
$lang['share_this_blog'] = "شارك";

$lang['keep_in_touch'] = "ابقى على اتصال";
$lang['keep_in_touch_txt'] = "نود أن نسمع <br /> منك";
$lang['copy_right'] = "حقوق التأليف والنشر الحياة فلل 2015. جميع الحقوق محفوظة.";
$lang['back'] = "رجوع";

$lang['dear'] = "عزيزي";
$lang['thank_you'] = "شكرا لك،";
$lang['click_to_enlarge'] = "اضغط للتكبير";
$lang['read_more'] = "قراءة المزيد";


/////////////////////////////////USERs\\\\\\\\\\\\\\\\\\\\\
$lang['error'] = "هناك خطا!";
$lang['success'] = "تم بنجاح!";

$lang['password_request_sent'] = "تم ارسال طلبك ، ارسل الرابط إعادة تعيين إلى:";
$lang['request_sent'] = "تم تقديم طلبك بنجاح";
$lang['password'] = "كلمة السر";
$lang['password_request'] = "إعادة تعيين كلمة السر";
$lang['update_password'] = "تعديل";
$lang['invalid_token'] = "الرمز غير صحيح";
$lang['press'] = "صحافة";
$lang['token_was_used'] = "تم استخدام الرمز من قبل";
$lang['using_fake_link'] = "تحاول الوصول إلى صفحة غير موجودة.";
$lang['email_exists'] = "البريد الإلكتروني موجود، يرجى محاولة ادخال بريد إلكتروني آخر.";
$lang['username_exists'] = "اسم المستخدم موجودا، الرجاء محاولة واحدة أخرى.";
$lang['password_updated'] = "تم تعديل كلمة السر .";
$lang['account_does_not_exists'] = "لا يوجد حساب، الرجاء التسجيل.";
$lang['system_is_redirecting_to_page'] = "النظام هو إعادة توجيه إلى الصفحة.";
$lang['your_are_registered_successfully'] = "تم التسجيل! إعادة توجيه إلى الصفحة.";
$lang['welcome'] = "أهلا بك";
$lang[',']='،';
$lang['you_have_successfully_login_with'] = " لقد تم تسجيل الدخول بنجاح مع";
$lang['cartel']='cartel.com';
$lang['account_is_blocked'] = "<a href='".route_to('contactus')."'>تم حظر الحساب الخاص بك، يرجى الاتصال </a>بالمسؤول.";
$lang['logout'] = "تسجيل خروج";
$lang['ar']='عربي';
$lang['en']='إنجليزي';
$lang['my_account'] = "حسابك";
$lang['account'] = "حسابك";
$lang['logout_success'] = "تم تسجيل الخروج بنجاح، سنكون في انتظاركم لزيارة موقعنا مرة أخرى.";
$lang['last_login'] = "آخر تسجيل دخول";
$lang['number_of_orders'] = "Number of <a href='".route_to('user/orders')."'>orders</a>";
$lang['edit_profile'] = "تعديل  حسابك";
$lang['orders_history'] = "الطلبات";
$lang['schedule_a_new_pickup'] = "حدد موعد الإستلام";
$lang['urgent_required'] = "عاجل حقل مطلوب";
$lang['access_denied'] = "تحاول الوصول الى صفحة خاطئة";
$lang['login_expired'] = "انتهت مدةالصلاحية، يرجى المحاولة للدخول مرة أخرى.";
$lang['profile_updated'] = "تم تعديل المعلومات الخاصة بك بنجاح.";
$lang['update_profile'] = "تعديل";
$lang['forget_password'] = "نسيت كلمة المرور";
$lang['forgot_your_password'] = "نسيت كلمة المرور";
$lang['forgot_password'] = "نسيت كلمة المرور";
$lang['if_you_are_aleady_registered'] = "تسجيل الدخول (اذا كان لديك حساب)";
$lang['send_request'] = "ارسل الطلب";
$lang['enter_email'] = "يرجى إدخال البريد الإلكتروني في الحقل أدناه";

$lang['change_your_password']='تغيير كلمة السر';
$lang['new_password']='كلمة  السر الجديدة';
$lang['old_password']='كلمة السر السابقة';
$lang['retype_new_password']='أعد كتابة كلمة السر الجديد';
$lang['edit_your_profile']='تعديل حسابك';
$lang['edit_your_account_information']='تعديل معلومات حسابك';
$lang['schedule_a_new_pickup']='تسجيل موعد تنظيف';
$lang['new_order']='طلب جديد';
$lang['view_your_order_history']='عرض جميع طلباتك';
$lang['check_your_order_history']='تاكد من طلباتك';
$lang['change']='تعديل';



$lang['pick_up_and_delivery_address']='عنوان التسليم';
$lang['delivery_address']='عنوان التسليم';
$lang['order_info']='معلومات الطلب';
$lang['type_of_garment']='نوع الملابس';
$lang['type_of_service']='نوع الخدمات';
$lang['number_of_pieces']='عدد القطع';
$lang['special_cleaning_instructions']='تعليمات التنظيف الخاصة';
$lang['urgent']='مستعجل';
$lang['no']='كلا';
$lang['yes']='نعم';
$lang['user_account_update_profile']='تعديل حسابك';
$lang['create_an_account']='إنشاء حساب';
$lang['password_letters']='كلمة السر (8-20 حروف أو أرقام)';
$lang['confirm_Password']='تأكيد كلمة السر';
/////////////////////////////////ORDERS\\\\\\\\\\\\\\\\\\\\\

$lang['please_login'] = "يرجى الدخول أو التسجيل للمتابعة.";


$lang['continue_shopping'] = "مواصلة التسوق؟";
$lang['login_to_continue'] = "يرجى الدخول أو التسجيل للمتابعة.";
$lang['order_completed'] =  " تم تسجيل طلبك! رقم الطلب";
$lang['one_of_our_sales_will_review_your_order'] = " سيقوم أحد مندوبي المبيعات لدينا بمراجعة طلبك وسنتصل بك قريبا.";
$lang['cart_is_empty'] = "Your shopping cart is empty, <a href='".route_to('products')."'>add products?</a>";
$lang['completed'] = "منجز";
$lang['pending'] = "قيد الانتظار";
$lang['paid'] = "مدفوع";
$lang['canceled'] = "ملغى";
$lang['delivery'] = "Delivery";
$lang['order_completed'] = "تم تسجيل طلبك! رقم الطلب";
$lang['order_id'] = "رقم الطلب";
$lang['created_date'] = "تاريخ الإنشاء";
$lang['amount'] = "الكمية";
$lang['status'] = "الحالة";
$lang['pending'] = "قيد الانتظار";
$lang['paid'] = "مدفوع";
$lang['canceled'] = "ملغى";

$lang['load_more'] = "عرض المزيد";
$lang['view'] = "عرض";
$lang['view_all'] = "عرض الجميع";
$lang['view_all_services'] = "عرض جميع الخدمات";
$lang['viewdetails'] = "عرض التفاصيل";
$lang['wrong_order'] = "تحاول الوصول إلى  طلب خاطئ.";
$lang['incorrect_email_pass'] = "البريد الإلكتروني أو كلمة السر غير صحيحة";
$lang['print'] = "طباعة";
$lang['order_details'] = "تفاصيل الطلب";
$lang['thank_you_for_your_order'] = "شكرا لطلبك،";
$lang['want_to_manage_order_online'] = "تريد عرض طلبك على الإنترنت؟";

$lang['order_email_note'] = "يرجى ملاحظة: هذيرجى عدم الرد على هذه الرسالة. <br /> <br /> وشكرا مرة أخرى للتسوق معنا.";

$lang['select'] = "اختار";
$lang['new_address'] = "عنوان جديد";
$lang['company'] = "الشركة";
$lang['company_name'] = "اسم الشركة";
$lang['postal_code'] = "Postal Code";


$lang['street_address'] = "عنوان الشارع";
$lang['first_name'] = "الاسم الأول";
$lang['last_name'] = "العائلة";
$lang['surname'] = "العائلة";
$lang['state'] = "الحالة";
$lang['delivery_information'] = "معلومات التوصيل";

$lang['payment_methods'] = "طرق الدفع";
$lang['payment_method'] = "طريقة الدفع";
$lang['payment_date'] = "تاريخ الدفع";
$lang['order_comments'] = "تعليق";

$lang['thank_you'] = 'شكرا،';
$lang['invoice'] = 'الفاتورة';
/*$lang['AED'] = 'AED';
$lang['USD'] = 'USD';
$lang['EUR'] = 'EURO';*/
$lang['orders'] = "الطلبات";
$lang['no_orders'] = "لا يوجد طلبات";
$lang['contact_success'] = "شكراً على إتصالك! طلبك وصلنا بنجاح وسوف نكون على تواصل معك قريباً.";
$lang['enquiry_success'] =  ".شكراً على إتصالك! طلبك وصلنا بنجاح وسوف نكون على تواصل معك قريباً";
$lang['credit_application_form_success'] = "Thank you for giving your time filling up the form, we will review your information and get back to you shortly.";
$lang['rma_form_success'] = "Thank you for giving your time filling up the form, we will review your information and get back to you shortly.";

$lang['name'] = "Name";
$lang['password_reminder_button'] = "Reset password";