<?php
require_once( BASEPATH .'database/DB'. EXT );

$lang['title_landing']="Welcome to Hayat Villas website";
$lang['landing-lbl'] = "for more information about purchase, please fill in your personal details<br />
to be the first to know";
$lang['captcha_field_required']="The Captcha field is required.";
$lang['security_code_incorrect'] ='The Security Code you have entered is incorrect, please try again and enter the correct code.';
$lang['please_type_the_letters_below'] = 'Please type the letters below';
$lang['approved_by_client'] = 'Approved by client';
/*$lang['add_my_list'] = 'Add to My List for easy re-ordering';*/
$lang['add_my_list'] = 'Add to "Re-order list"';
$lang['pickup'] = 'Pick Up';
$lang['suppliers_title'] = 'Suppliers';
$lang['consumers']='consumers';
$lang['multibuy_prices'] = 'Multi Prices';
$lang['group_products'] = 'Products Kits';
$lang['quantity'] = "QTY";
$lang['license_number'] = "Trade License Number";
$lang['no_quotation'] = "No quotation found.";
$lang['days'] = "Days";
$lang['sales'] = "Sales";
$lang['account_manager'] = "Account Manager";
$lang['position'] = "Position";
$lang['return_quantity'] = "Return";
$lang['areas'] = "Areas";
$lang['area'] = "Area";
$lang['reqturn_quantity'] = "Return Qty";
$lang['LPO'] = "LPO";
$lang['credit_cart'] = "credit cart";
$lang['stock_request'] = "Request";
$lang['total_price'] = "Total Price";
$lang['order_id'] = "Order";
$lang['shelf_location'] = "Shelf Location";
$lang['barcode'] = "Barcode";
$lang['log_to_buy'] = "Kindly Login to Buy.";
$lang['order_id'] = "Order";
$lang['total_amount'] = "Amount";
$lang['total_amount_credits'] = "Amount Credits";
$lang['shipping_charge'] = "Shipping Charge";
$lang['net_price'] = "Net Price";
$lang['customer_name'] = "Customer Name";
$lang['invoice_id'] = "Invoice";
$lang['created_date'] = "Created Date";
$lang['you_cannot_redeem'] = "You cannot redeem it";
$lang['submit_information'] = "Proceed";
/////////////////////////////////FORM\\\\\\\\\\\\\\\\\\\\\
$lang['dowgroup_note'] = "<a href='http://www.dowgroup.com'>Dow Group</a> bears no responsibility for the accuracy or legality of the content on this website. We cannot accept any liability arising from the content/ information contained within this site.";
$lang['admin'] = "admin";
$lang['supplier'] = "supplier";
$lang['store_owner'] = "Spa/Salon";
$lang['master'] = "master";
$lang['users'] = "users";
$lang['surname'] = "Surname";
$lang['partners'] = "partners";
$lang['full_name'] = "Full Name";
$lang['type'] = "type";
$lang['city'] = "City";
$lang['country'] = "Country";
$lang['supplier'] = "Supplier";
$lang['address'] = "Address";
$lang['phone_number'] = "Phone no";
$lang['phone'] = "phone";
$lang['mobile'] = "mobile";
$lang['paid_m'] = "Purchase Invoice";
$lang['approved'] = "Approved";
$lang['canceled'] = "Canceled";
$lang['in_progress'] = "In progress";
$lang['pending'] = "pending";
$lang['consignment_m'] = "Delivery Note";
$lang['email'] = "E-mail";
$lang['email_address'] = "E-mail Address";
$lang['message'] = "Message";
$lang['comments'] = "Comments";
$lang['business_type'] = "Business Type";
$lang['hear_about_us'] = "Hear About Us";
$lang['trading_name'] = "Trade Name";
$lang['trading_licence'] = "Trading Licence";
$lang['location_map'] = "Location Map";
$lang['locations_and_contact_information'] = "Locations & Contact information";
$lang['send_us_a_message'] = "Send us a message";
$lang['send_message'] = "Send Message";
$lang['complete_registration'] = "Thank you for registering with us, we look forward to serve yoy";
$lang['required_field_note'] = 'All fields with (<span class="required">*</span>) must be filled';
$lang['submit'] = "SUBMIT";
$lang['contact_form'] = "Contact Form";
$lang['contact_us'] = "Contact Us";
$lang['contact_us_txt'] = "We are always ready to assist you";
$lang['contact_success'] = "Thank you, your enquiry has been successfully submitted, the Supplier will be in touch with you shortly.";
$lang['landing_success'] = "Thank you, your application was successfully submitted, the administrator will be in touch with you shortly.";

$lang["booking_success"]="Thank you, your message has been successfully submitted and received and we will be in touch with you shortly.";
$lang['our_clinic'] = "Our Clinic";
$lang['suggester_retail_price'] = "Retail Price";
/////////////////////////////////Main Menu\\\\\\\\\\\\\\\\\\\\\
$lang['home'] = "Home ";
$lang['about_us'] = "about us";
$lang['our_services'] = "our services";
$lang['concierge_services'] = "Concierge Services";
$lang['retailer'] = "Retailer";
$lang['faqs'] = "FAQS";
$lang['media_center'] = "Media Center";
$lang['blogs'] = "Blogs";
$lang['login'] = "Login";
$lang['register'] = "Register";

$lang['select_language'] = "Select Language";
$lang['coming_soon'] = "coming soon...";
$lang['menu'] = "MENU";

$lang['not_available_in_stock'] = "Out of Stock";
$lang['available_in_stock'] = "In Stock";
/////////////////////////////////Footer\\\\\\\\\\\\\\\\\\\\\
$lang['footer_phone_txt'] = "Reach one of our sales consultants today";
$lang['location'] = "Location";
$lang['toll_free'] = "Toll Free";
$lang['rights_reserved'] = "Rights Reserved";
$lang['alessia_f'] = "© 2015 Alessia";
$lang['alessia'] = "Alessia";
$lang['by_dow'] = "By Dow Group";
$lang['share_this_video'] = "Share this Video";
$lang['share_this_blog'] = "Share this Blog";
$lang['back'] = "Back";

$lang['keep_in_touch_txt'] = "we would like to hear <br /> from you";
$lang['copy_right'] = "Copyright Hayat Villas 2015. All Rights Reserved.";

$lang['click_to_enlarge'] = "click to enlarge";
$lang['read_more'] = "read more";
$lang['problem_menu'] = "Problem Menu";
$lang[''] = "";

$lang['dear'] = "Dear";
$lang['thank_you'] = "Thank you";


$lang['notcorrectcharacters'] = 'Characters do not match.';
/////////////////////////////////USERs\\\\\\\\\\\\\\\\\\\\\
$lang['error'] = "Error!";
$lang['success'] = "Success!";
$lang['dear'] = "Dear";
$lang['password_request_sent'] = "Your request is submitted succesfully, a reset link is sent to: ";
$lang['request_sent'] = "Your request is submitted succesfully ";
$lang['password'] = "password";
$lang['confirm_password'] = "confirm password";
$lang['password_request'] = "Reset Password";
$lang['update_password'] = "Update Password";
$lang['invalid_token'] = "invalid token";
$lang['token_is_expired'] = "Request token is expired.";
$lang['account_is_expired'] = "Account is expired. Click on the below button to reactivate your account .";
$lang['account_is_reactivate'] = "Account is reactivate.";
$lang['token_was_used'] = "Token has been already used.";
$lang['using_fake_link'] = "You are trying to access a non existing page.";
$lang['email_exists'] = "Email exists, please try another email address.";
$lang['username_exists'] = "Username exists, please try another one.";
$lang['password_updated'] = "Password is updated successfully.";
$lang['account_does_not_exists'] = "Your account does not exits, please register.";

$lang['you_have_been_successfully_registered_as_an']="You have been successfully registered as an";
//$lang['account_is_blocked'] = "Your account is blocked, please <a href='".route_to('contactus')."/contact' target='_blank'>contact the administrator</a>.";
$lang['account_is_blocked'] = "Your account is under review, please <a href='".route_to('contactus')."/contact' target='_blank'>contact the administrator</a>.";
$lang['logout'] = "Logout";

$lang['my_account'] = "My Account";
$lang['schedule_a_new_pickup'] = "schedule a new pickup";
$lang['urgent_required'] = "Urgent field is required";
$lang['account'] = "Account";
$lang['logout_success'] = "You are logged out successfully, we will be waiting for you to visit our store again.";
$lang['last_login'] = "Last Login";
$lang['number_of_orders'] = "Number of <a href='".route_to('user/orders')."'>orders</a>";
$lang['edit_profile'] = "edit profile";
$lang['orders_history'] = "orders history";
$lang['one_of_our_sales_will_review_your_order'] = " One of our sales persons will review your order and contact you soon.";
$lang['compare_products'] = "Compare Products";
$lang['based_on_selection'] ='Price based on selection';
$lang['miles_based_on_selection'] ='Miles based on selection';
$lang['access_denied'] = "You are trying to access non existing page.";
$lang['login_expired'] = "Token is expired, please try to login another time.";
$lang['profile_updated'] = "Your information is updated successfully.";
$lang['files_updated'] = "Your files is updated successfully.";
$lang['update_profile'] = "Update";
$lang['forgot_your_password'] = "Forget Your Password";
$lang['forget_password'] = "Forget Password";
$lang['forgot_password'] = "Forgot Password";
$lang['if_you_are_aleady_registered'] = "Login (if you are aleady registered)";
$lang['system_is_redirecting_to_page'] = "System is redirecting to page.";
$lang['payment_redirection_ni'] ="System is redirecting to merchant....";

$lang['send_request'] = "Send Request";
$lang['welcome'] = "Welcome ";
$lang['press'] = "press";
$lang[',']=',';
$lang['ar']='AR';
$lang['en']='EN';
$lang['spamiles']='ebeautytrade.com';
$lang['you_have_successfully_login_with'] = "you are successfully logged in ";

$lang['enter_email'] = "Please enter your email address in the below field";
$lang['your_are_registered_successfully'] = "Your are registered successfully ! System is redirecting to page.";

$lang['user_account_update_profile']='User Account: Update Profile';
$lang['create_an_account']='Create An Account';
$lang['password_letters']='Password (8-20 letters or numbers)';
$lang['confirm_Password']='confirm Password';

$lang['change_your_password']='Change Your Password';
$lang['new_password']='New Password';
$lang['old_password']='Old Password';
$lang['retype_new_password']='Retype New Password';
$lang['edit_your_profile']='Edit Your Profile';
$lang['edit_your_account_information']='Edit your account information';
$lang['schedule_a_new_pickup']='Schedule a new pickup';
$lang['new_order']='New order';
$lang['view_your_order_history']='View Your Order History';
$lang['check_your_order_history']='Check your order history';
$lang['change']='Change';


$lang['pick_up_and_delivery_address']='pick up and delivery address';
$lang['delivery_address']='Delivery Address';
$lang['order_info']='Order Info';
$lang['type_of_garment']='Type of garment';
$lang['type_of_service']='Type of service';
$lang['number_of_pieces']='Number of pieces';
$lang['special_cleaning_instructions']='Special cleaning instructions';
$lang['urgent']='Urgent';
$lang['no']='No';
$lang['yes']='Yes';
/////////////////////////////////ORDERS\\\\\\\\\\\\\\\\\\\\\
$lang['cart_added'] = "is added to your <a href='".route_to('cart')."'>shopping cart</a>.";
$lang['added_to_favorites'] = "is added to <a href='".route_to('user/checklist')."'>your list</a>.";
$lang['was_alread_added'] = " was already added to <a href='".route_to('user/checklist')."'>your list</a>.";
$lang['gallery'] = "gallery";
$lang['cart_product_id'] = "ID";
$lang['cart_product_name'] = "Product Name";
$lang['cart_product_quantity'] = "Quantity";
$lang['cart_total_price'] = "Total Price";
$lang['cart_product_price'] = "Unit Price";
$lang['cart_net_price'] = "Net Price";
$lang['cart_remove'] = "Remove";
$lang['update_quantity'] = "Update Quantity";
$lang['checkout'] = "Checkout";
$lang['continue_shopping'] = "continue shopping?";
$lang['login_to_continue'] = "please login or register to continue.";
$lang['login_to_continue_meeting_request'] = "Kindly login to use this feature.";

$lang['order_completed'] = "Your order is complete! Your order number is ";
$lang['cart_is_empty'] = "Your shopping cart is empty, <a href='".route_to('products')."'>add products?</a>";
$lang['billing'] = "Billing";
$lang['delivery'] = "Delivery";
$lang['completed'] = "Completed";
$lang['pending'] = "Pending";
$lang['paid'] = "Paid";
$lang['canceled'] = "Canceled";
$lang['order_completed'] = "Your order is complete! Your order number is ";
$lang['order_id'] = "Order ID";
$lang['created_date'] = "Created Date";
$lang['amount'] = "Amount";
$lang['status'] = "Status";
$lang['pending'] = "Pending";
$lang['paid'] = "Paid";
$lang['canceled'] = "Canceled";
$lang['order_operations'] = "Operations";
$lang['load_more'] = "Load More";
$lang['view'] = "view";
$lang['view_all'] = "view all";
$lang['view_all_services'] = "view all services";
$lang['viewdetails'] = "View Details";
$lang['wrong_order'] = "You are trying to access a wrong order.";
$lang['incorrect_email_pass'] = "Incorrect email or password";
$lang['print'] = "Print";
$lang['order_details'] = "Order Details";
$lang['thank_you_for_your_order'] = "Thanks for your order,";
$lang['want_to_manage_order_online'] = "Want to view your order online?";
$lang['to_check_status'] = "Please visit our <a href='".site_url()."'>home page</a>, login and click on 'My account' in the top right menu.";
$lang['products_on_order'] = "Products On Order";
$lang['order_email_note'] = "Please note: This e-mail message is an automated notification. Please do not reply to this message.<br /><br />Thanks again for shopping with us.";



$lang['select'] = "Select";
$lang['new_address'] = "New Address";
$lang['company'] = "Company";
$lang['company_name'] = "Company Name";
$lang['postal_code'] = "Postal Code";
$lang['city'] = "City";
$lang['country'] = "Country";
$lang['gender'] = "Gender";
$lang['male'] = "Male";
$lang['female'] = "Female";
$lang['street_address'] = "Street Address";
$lang['street'] = "Street Address";
$lang['first_name'] = "First Name";
$lang['last_name'] = "Last Name";
$lang['surname'] = "Last Name";
$lang['state'] = "State";
$lang['delivery_information'] = "Delivery Information";
$lang['make_delivery_as_billing'] = "My delivery information is the same as my billing information.";
$lang['payment_methods'] = "Payment Methods";
$lang['payment_method'] = "payment method";
$lang['payment_date'] = "payment date";
$lang['order_comments'] = "Order Comments";
$lang['dear'] = 'Dear';
$lang['thank_you'] = 'Thank you,';
$lang['invoice'] = 'Invoice';
/*$lang['AED'] = 'AED';
$lang['USD'] = 'USD';
$lang['EUR'] = 'EURO';*/
$lang['orders'] = "orders";
$lang['no_orders'] = "You have no orders.";

$lang['contact_success'] = "Thank you, your enquiry has been successfully submitted, the Supplier will be in touch with you shortly.";
$lang['enquiry_success'] = "Thank you, your enquiry has been successfully submitted, the Supplier will be in touch with you shortly.";
$lang['credit_application_form_success'] = "Thank you for giving your time filling up the form, we will review your information and get back to you shortly.";
$lang['rma_form_success'] = "Thank you for giving your time filling up the form, we will review your information and get back to you shortly.";

/////////////////////////////////Newsletter\\\\\\\\\\\\\\\\\\\\\
$lang['the_subscription_has_been_removed'] = "The subscription has been removed.";
$lang['you_are_currently_not_subscribed_to_any_newsletter'] = "You are currently not subscribed to any newsletter.";
$lang['the_subscription_has_been_saved'] = "The subscription has been saved.";
$lang['you_are_currently_subscribed'] = "You are currently subscribed to 'General Subscription'.";


/////////////////////////////////CART\\\\\\\\\\\\\\\\\\\\\
$lang['cart_item_removed'] = " are removed from your shopping cart.";
$lang['favorite_removed'] = "is removed from your list.";
$lang['cart_item_removed_error'] = "Error in removing the product, please try again. If the problem persists please contact the administrator.";
$lang['cart_quantity_updated_note'] = "Your shopping cart is updated successfully, some of the products having no stock are not updated.";
$lang['cart_quantity_updated_note2'] = "The Quantity  is not available.";
$lang['cart_quantity_updated'] = "Your shopping cart is updated successfully.";
$lang['please_login'] = "Please login to continue.";
//$lang['empty_cart'] = "You have no items in your shopping cart. Go to Shop By Brand & choose your items.";
$lang['empty_cart'] = "You have no items in your shopping cart.";

$lang['availability_in_stock'] = "Availability: In Stock";
$lang['availability_out_stock'] = "Availability: Out of stock";
$lang['out_of_stock'] = "Out of stock";
$lang['cart_updated'] = ", quantity is updated in your <a href='".route_to('cart')."'>shopping cart</a>.";
/*$lang['stock_not_available'] = "Stock is not available for";*/
$lang['stock_not_available'] = "The Quantity you are asking for is not available for now, <a href='".route_to('cart/sendInquiry')."'>Click Here</a> to inquiry.";
$lang['compare_login_error'] = "Please <a href='".route_to('user/login')."'>login</a> to continue.";
$lang['compare_count_error'] = "Ops! you have exceeded the maximum number of products in your <a href='".route_to('user/compareProducts')."'>compare basket</a>.";
$lang['added_to_compare'] = " has been added to <a href='".route_to('user/compareProducts')."'>comparison list</a>.";
$lang['deleted_from_compare'] = "Product is removed from your <a href='".route_to('user/compareProducts')."'>compare list</a>.";
$lang['no_compare'] = "No products in your compare list, <a href='".route_to('products')."'>add product?</a>";
$lang['no_favorites'] = "You have no items in your list.";
$lang['no_catalogues_found'] = "Coming soon...";
$lang['no_data_found'] = "Coming soon...";
$lang['no_product_selected'] = "You did not choose any product.";
$lang['compare_removed'] = "is removed from your compare list.";
$lang['compare_list_cleared'] = "The comparison list was cleared.";
$lang['was_already_added_compare'] = "was already added to <a href='".route_to('user/compareProducts')."'>compare list</a>.";
$lang['was_already_added_wishlist'] = "was already added to <a href='".route_to('user/favorites')."'>your list</a>.";/////////////////////////////////Currencies\\\\\\\\\\\\\\\\\\\\
$lang['AED'] = "AED";
$lang['USD'] = "USD";
$lang['GBP'] = "GBP";
$lang['SAP'] = "SAP";
$lang['EUR'] = "EUR";

$lang['name'] = "Name";
$lang['password_reminder_button'] = "Reset password";