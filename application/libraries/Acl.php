<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Simple config file based ACL
 *
 * @author Kevin Phillips <kevin@kevinphillips.co.nz>
 */
class Acl {

	private $_CI;
	private $acl;

    /** @var \core\ServiceLocator */
    protected $serviceLocator;

	public function __construct()
	{
		$this->_CI = & get_instance();
		$this->_CI->load->library('session');
		$this->_CI->load->config('acl', TRUE);
		$this->acl = $this->_CI->config->item('permission', 'acl');

        $this->serviceLocator = get_service_locator();
	}

	/**
	 * function that checks that the user has the required permissions
	 *
	 * @param string $controller
	 * @param array $required_permissions
	 * @param integer $author_uid
	 * @return boolean
	 */
	public function has_permission($controller, $required_permissions = array('delete all'), $author_uid = NULL)
	{
		/* make sure that the required permissions is an array */
		if ( ! is_array($required_permissions))
		{
			$required_permissions = explode( ',', $required_permissions );	
		}
			
		/* Get the vars from ci_session */
		$uid = $this->_CI->session->userdata('uid');
		$user_roles = $this->_CI->session->userdata('roles');

		/* Shouldn't happen but if we stick to belt and braces we should be OK */
		if ( ! $uid OR ! $user_roles)
		{
			return FALSE;		
		}	

		/* set empty array */
		$permissions = array();

		/* Load the permissions config */
		foreach ($this->acl[$controller] as $actions => $roles)
		{
			foreach ($user_roles as $user_role)
			{
				
				if (in_array( $user_role, $roles ))
				{
					$permissions[$actions] = $roles;	
				}					
			}
		}

		foreach ($permissions as $action => $role)
		{
			if (in_array($action, $required_permissions))
			{
				if (($action == 'edit own' OR $action == 'delete own') && ( ! isset($author_uid) OR $author_uid != $uid))
				{
					return FALSE;
				}
				return TRUE;
			}
		}
	}
	
	/**
	 * Function to see if a user is logged in
	 */
	public function is_logged_in()
	{
		$uid = $this->_CI->session->userdata('uid');
		if ($uid)
		{
			return TRUE;
		}
	}
    
    /**
     * @return boolean Returns whether backoffice user is properly logged in
     */
    public function isCurrentUserLoggedIn()
    {
        return $this->is_logged_in();
    }
    
    /**
     * @return boolean Returns whether backoffice user is logged in and is a spamiles admin
     */
    public function isCurrentUserAAdmin()
    {
        return $this->isCurrentUserLoggedIn() && !checkIfSupplier_admin();
    }
    
    /**
     * @return boolean Returns whether backoffice user is logged in and is a supplier and not spamiles admin
     */
    public function isCurrentUserASupplier()
    {
        return $this->isCurrentUserLoggedIn() && checkIfSupplier_admin();
    }

    /**
     * @return null|int Gets the current user id or null if not logged in
     */
    public function getCurrentUserId()
    {
        if (!$this->is_logged_in()) {
            return null;
        } else {
            return (int)$this->_CI->session->userdata('uid');
        }
    }
    
    /**
     * @return null|int Gets the current user type or null if not logged in
     */
     
    public function getCurrentUserType()
    {
        if (!$this->is_logged_in()) {
            return null;
        } else {
            $this->_CI->load->model("user_m");
            $users = $this->_CI->user_m->get_user($this->getCurrentUserId());
            return $users['id_user_type'];
        }
    }

    /**
     * @return [] Gets the current user data from db
     */
    public function getCurrentUserData()
    {
        return $this->serviceLocator->user($this->getCurrentUserId());
    }

    /**
     * TODO: move it somewhere else
     * @return int Returns int from 0-100 range, which is the % of required personal data given by the user
     */
    public function personalDataCompletionPercent()
    {
        $currentUser = $this->serviceLocator->user($this->getCurrentUserId());
        $currentUserData = $currentUser->toArray();

        $requiredFields = [
            //company data
            'trading_name',
            'logo',
            'address_1',
            'id_countries',
            'company_email',
            'company_phone',
            //'company_location_map',

            //delivery
            'delivery_shipping_from',

            //personal data
            'first_name',
            'last_name',
            'phone',
            'email',
        ];

        $sum = count($requiredFields);

        $points = 0;
        foreach ($requiredFields as $field) {
            if (
                isset($currentUserData[$field])
                && notEmpty($currentUserData[$field])
            ) {
                $points++;
            }
        }

        //more complicated
        $sum++;
        if ($currentUser->getDeliveryTime()) {
            $points++;
        }

        $sum++;
        if ($currentUser->getDeliveryFee()) {
            $points++;
        }

        $sum++;
        if ($currentUser->getCreditTerms()) {
            $points++;
        }
        /*
        $sum++;
        if ($currentUser->getPaymentMethods()) {
            $points++;
        }
        */

        return round(($points/$sum)*100);
    }

    /**
     * TODO: move it somewhere else
     * @return int Returns int from 0-100 range, which is the % of required brand data given by the user
     */
    public function brandDataCompletionPercent()
    {
        $this->_CI->load->model("user_m");
        $condition['id_user'] = $this->getCurrentUserId();
        $users = $this->_CI->user_m->getparentuser($condition, 100, 0);
        
        foreach ($users as $user) {
            if ($user['id_parent'] != 0 || $user['id_parent'] != "") {
                $parentid = $user['id_parent'];
            }
        }
        if($parentid){
            $cond['id_user'] = $parentid;
        } else {
            $cond['id_user'] = $this->getCurrentUserId();
        }

        $this->_CI->load->model("brands_m");
        $brands = $this->_CI->brands_m->getRecords($cond, 100, 0);

        $requiredFields = [
            'logo',
            'title',
            'photo',
            'name',
            'last_name',
            'position',
            'phone',
            'email'
        ];

        $maxPoints = count($requiredFields);
        $currentMaxPoints = 0;
        foreach ($brands as $brand) {
            //status published
            if ($brand['status'] == 1) {
                return 100;
            }

            $points = 0;
            foreach ($requiredFields as $field) {
                if (
                    isset($brand[$field])
                    && notEmpty($brand[$field])
                ) {
                    $points++;
                }
            }
            if ($points == $maxPoints) {
                return 100;
            } else {
                $currentMaxPoints = max($currentMaxPoints, $points);
            }
        }

        return round(($currentMaxPoints/$maxPoints)*100);
    }

    /**
     * TODO: move it somewhere else
     * @return int Returns int from 0-100 range, which is the % of profile completion
     */
    public function profileCompletionPercent()
    {
        //each part has 50%
        return ($this->personalDataCompletionPercent() + $this->brandDataCompletionPercent())*0.5;
    }
}

/* End of application/libraries/acl.php */
