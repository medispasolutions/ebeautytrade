<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('changeDate')){
function changeDate($date){
	$time = strtotime($date);
	$newdate = date('d/m/Y',$time);
	return $newdate;
}
}

if ( ! function_exists('categories_section')){
function categories_section(){

$categories_section="b2b";
	return $categories_section;
}
}

if ( ! function_exists('changeToCountDownDate')){
function changeToCountDownDate($date){
 $time = strtotime($date);
 $newdate = date('Y/m/d',$time);
 return $newdate;
}
}
if ( ! function_exists('changeToCountDownDate')){
function changeToCountDownDate($date){
	$time = strtotime($date);
	$newdate = date('Y/m/d',$time);
	return $newdate;
}
}

if ( ! function_exists('cleanPhone')){
function cleanPhone($phone){
	$new_phone=str_replace('-',' ',$phone);
	return $new_phone;
}
}

if ( ! function_exists('time_slots')){
function time_slots(){
$data['9:00 AM - 11:00 AM']='9:00 AM - 11:00 AM';
$data['11:00 AM - 1:00 PM']='11:00 AM - 1:00 PM';
$data['2:00 PM - 4:00 PM']='2:00 PM - 4:00 PM';
$data['4:00 PM - 6:00 PM']='4:00 PM - 6:00 PM';
	return $data;
}
}


if ( ! function_exists('arrangeParentLevels')){
function arrangeParentLevels($arr)
{
	
	$newarr = array();
	$finalarr = array();
	if(!empty($arr)) {
		$levelArr = $arr;
		$i=0;
		$newarr[$i] = $arr;
		while(isset($levelArr['id_parent']) && $levelArr['id_parent'] != 0) {
			$i++;
			$newarr[$i] = $levelArr['parent_level'];
			$levelArr = $levelArr['parent_level'];
		}
		//print '<pre>'; print_r($newarr); exit;
		$w=0;
		$cc = count($newarr);
		//echo 'w: '.$cc;exit;
		for($k = $cc; $k > 0; $k--) {
			//echo 333;exit;
			$finalarr[$w] = $newarr[$k - 1];
			$w++;
		}
		//echo 2;exit;
	}
	
	//print '<pre>'; print_r($finalarr); exit;
    return $finalarr;
}
}

if ( ! function_exists('displayLevels')){
function displayLevels($arr,$index,$id = "",$select_categories="")
{
	$string = '';
	//print '<pre>'; print_r($arr); exit;
	foreach($arr as $k => $ar) {
$cl = "";
if(($id != "" && $ar['id_categories'] == $id) || (!empty($select_categories) && in_array($ar['id_categories'],$select_categories)))
$cl = "selected='selected'";
		$string .= '<option value="'.$ar['id_categories'].'" '.$cl.'>'.str_repeat('-',$index).' '.$ar['title'].'</option>';
		if(!empty($ar['sub_levels'])) {
			$string .= displayLevels($ar['sub_levels'],$index + 1,$id,$select_categories);
		}
	}
    return $string;
}
}




if ( ! function_exists('getNewUrl')){
function getNewUrl($url,$keyword){
	$new_url=str_replace('&'.$keyword,'',$url);
	return $new_url;
}
}

if ( ! function_exists('getDir')){
function getDir($lang11 = ''){
	$dir = '';
	if($lang11 == '') {
		$CI =& get_instance();
		$lng = $CI->lang->lang();
	}
	else {
		$lng = $lang11;
	}
	
	if($lng == 'ar')
	$dir = 'dir="rtl"';
	return $dir;
}
}

if ( ! function_exists('GenerateBreadCrumbs')){
function GenerateBreadCrumbs($array){
	$count = count($array);
	$breadcrumbs = '<ul class="breadcrumbs">';
		$i=0;
		foreach($array as $arr) {
			$i++;
			if($i != $count) {
				$breadcrumbs .= '<li><a href="'.$arr['link'].'">'.$arr['title'].'</a></li>';
			}
			else {
				$breadcrumbs .= '<li><a class="default">'.$arr['title'].'</a></li>';
			}
		}
	$breadcrumbs .= '</ul>';
	return $breadcrumbs;
}
}

if ( ! function_exists('route_to')){
function route_to($path,$sub = 0) {
	//echo $path.'<br />';
    $CI =& get_instance();
	//if($sub == 0)
    //$route = array_search($path, $CI->router->routes, TRUE);
	//else
	$route = array_search($path, $CI->router->routes, TRUE);
	if($route == '') {$route = $path;}
    return site_url($route);
}
}

if ( ! function_exists('getYoutubeCode')){
function getYoutubeCode($link) {
	$data = explode('?v=',$link);
    return $data[1];
}
}

if ( ! function_exists('youtube_image')){
function youtube_image($id) {
    $resolution = array (
        'maxresdefault',
        'sddefault',
        'mqdefault',
        'hqdefault',
        'default'
    );

    for ($x = 0; $x < sizeof($resolution); $x++) {
        $url = 'http://img.youtube.com/vi/' . $id . '/' . $resolution[$x] . '.jpg';
		$gh = get_headers($url);
        if ($gh[0] == 'HTTP/1.0 200 OK') {
            break;
        }
    }
    return $url;
}
}

if ( ! function_exists('getVimeoCode')){
function getVimeoCode($link) {
	$data = explode('vimeo.com/',$link);
    return $data[1];
}
}

if ( ! function_exists('vimeo_image')){
function vimeo_image($id,$request = 'thumb') {
	$link='http://vimeo.com/api/v2/video/'.$id.'.php';
	//echo $link;exit;
	//$link = str_replace('http://vimeo.com/', 'http://vimeo.com/api/v2/video/', $link) . '.php';

	$html_returned = unserialize(file_get_contents($link));
	// $feedURL = 'http://gdata.youtube.com/feeds/api/videos/' . $youtubeVideoID;

     // read feed into SimpleXML object
     //$entry = simplexml_load_file($link);
	//print '<pre>';print_r($html_returned);exit;
	if ( $html_returned === false )
	{
	 return '';
	}
	else {
		if($request == 'thumb') {
	  	$thumb_url = $html_returned[0]['thumbnail_large']; 
	  	return $thumb_url;
		}
		elseif($request == 'duration') {
			$duration = $html_returned[0]['duration']; 
	  		return $duration;
		}
	}
	//print '<pre>';print_r($html_returned);exit;
	
}
}

if ( ! function_exists('dateArray')){
function dateArray($date) {
	//$link = str_replace('http://vimeo.com/', 'http://vimeo.com/api/v2/video/', $link) . '.php';
    list($y,$m,$d) = explode('-',$date);
	$arr['year'] = $y;
	$arr['month'] = $m;
	$arr['day'] = $d;
	return $arr;
}
}

if ( ! function_exists('getCategoriesPositions')){
function getCategoriesPositions() {
	$arr[''] = '- Select Position-';
  	$arr['top'] = 'Primary Menu';
	$arr['bottom'] = 'Secondary Menu';
	return $arr;
}
}

if ( ! function_exists('careersDate')){
function careersDate($date) {
	$datetime = strtotime($date);
	$newdate = date('Y-m-d',$datetime);
    list($y,$m,$d) = explode('-',$newdate);
	$newdate = $d.' - '.$m.' - '.$y;
	return $newdate;
}
}

if ( ! function_exists('getMP4Info')){
function getMP4Info($videofile) {
	
	/*$ffmpeg_path = 'ffmpeg'; //or: /usr/bin/ffmpeg - depends on your installation
	$vid = $videofile; //Replace here!
	
	if (file_exists($vid)) {
	
		$video_attributes = _get_video_attributes($vid, $ffmpeg_path);
	
		print_r('Video codec: ' . $video_attributes['codec'] . ' - width: '  . $video_attributes['width'] 
				. ' - height: ' .  $video_attributes['height'] . ' <br/>');
	
		print_r('Video duration: ' . $video_attributes['hours'] . ':' . $video_attributes['mins'] . ':'
			   . $video_attributes['secs'] . '.'. $video_attributes['ms']);exit;
	} else { echo 'File does not exist.'; }*/
	return '1s';

}
}

function _get_video_attributes($video, $ffmpeg) {

    $command = $ffmpeg . ' -i ' . $video . ' -vstats 2>&1';  
    $output = shell_exec($command);  

    $regex_sizes = "/Video: ([^,]*), ([^,]*), ([0-9]{1,4})x([0-9]{1,4})/";
    if (preg_match($regex_sizes, $output, $regs)) {
        $codec = $regs [1] ? $regs [1] : null;
        $width = $regs [3] ? $regs [3] : null;
        $height = $regs [4] ? $regs [4] : null;
     }

    $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
    if (preg_match($regex_duration, $output, $regs)) {
        $hours = $regs [1] ? $regs [1] : null;
        $mins = $regs [2] ? $regs [2] : null;
        $secs = $regs [3] ? $regs [3] : null;
        $ms = $regs [4] ? $regs [4] : null;
    }

    return array ('codec' => $codec,
            'width' => $width,
            'height' => $height,
            'hours' => $hours,
            'mins' => $mins,
            'secs' => $secs,
            'ms' => $ms
    );

}


if ( ! function_exists('getYoutubeInfo')){
function getYoutubeInfo($youtubeVideoID) {
 $obj= new stdClass;
      
 // set video data feed URL
     $feedURL = 'http://gdata.youtube.com/feeds/api/videos/' . $youtubeVideoID;

     // read feed into SimpleXML object
     $entry = simplexml_load_file($feedURL);
      
       // get nodes in media: namespace for media information
       $media = $entry->children('http://search.yahoo.com/mrss/');
       $obj->title = $media->group->title;
       $obj->description = $media->group->description;
      
       // get video player URL
       $attrs = $media->group->player->attributes();
       $obj->watchURL = $attrs['url']; 
      
       // get video thumbnail
       $attrs = $media->group->thumbnail[0]->attributes();
       $obj->thumbnailURL = $attrs['url']; 
            
       // get <yt:duration> node for video length
       $yt = $media->children('http://gdata.youtube.com/schemas/2007');
       $attrs = $yt->duration->attributes();
       $obj->length = $attrs['seconds']; 
      
       // get <yt:stats> node for viewer statistics
       $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
       $attrs = $yt->statistics->attributes();
       $obj->viewCount = $attrs['viewCount']; 
      
       // get <gd:rating> node for video ratings
       $gd = $entry->children('http://schemas.google.com/g/2005'); 
       if ($gd->rating) 
 { 
         $attrs = $gd->rating->attributes();
         $obj->rating = $attrs['average']; 
       } 
 else 
 {
        $obj->rating = 0;         
       }
        
 // get <gd:comments> node for video comments
       $gd = $entry->children('http://schemas.google.com/g/2005');
       if ($gd->comments->feedLink) 
 { 
         $attrs = $gd->comments->feedLink->attributes();
         $obj->commentsURL = $attrs['href']; 
         $obj->commentsCount = $attrs['countHint']; 
       }
//$videoInfo = parseVideoEntry($videoId);
//print '<pre>';print_r($obj);exit; 
       return $obj;      


}
}

if ( ! function_exists('getTaskStatus')){
function getTaskStatus($id=""){

	$arr[0]='-Select Status-';
	$arr[1]='Canceled';
	$arr[2]='In Progress';
	$arr[3]='Done';
	$arr[4]='Pending';
	$arr[5]='Approved';
	if($id!=""){
		return $arr[$id];
		}else{
	return $arr;}
	}}
	
if ( ! function_exists('monthByID')){
function monthByID($mon) {
	$month = '';
	switch($mon) {
		case 1:
			$month = 'Jan';
			break;
		case 2:
			$month = 'Feb';
			break;
		case 3:
			$month = 'Mar';
			break;
		case 4:
			$month = 'Apr';
			break;
		case 5:
			$month = 'May';
			break;
		case 6:
			$month = 'Jun';
			break;
		case 7:
			$month = 'Jul';
			break;
		case 8:
			$month = 'Aug';
			break;
		case 9:
			$month = 'Sep';
			break;
		case 10:
			$month = 'Oct';
			break;
		case 11:
			$month = 'Nov';
			break;
		case 12:
			$month = 'Dec';
			break;
	}
	return $month;
}
}	
	
if ( ! function_exists('getPriorities')){
function getPriorities($id=""){

	$arr[0]='-Select Priority-';
	$arr[1]='High';
	$arr[2]='Low';
	$arr[3]='Medium';
	if($id!=""){
		return $arr[$id];
		}else{
	return $arr;}
	}}	

if ( ! function_exists('getlengthInMinutes')){
function getlengthInMinutes($seconds) {
	if($seconds < 60) {
		$length = $seconds.'s';
	}
	else {
		$minutes = round($seconds / 60);
		$seconds = $seconds - ($minutes * 60);
		if($seconds == 0)
		$length = $minutes.'m';
		else
		$length = $minutes.'m'.$seconds.'s';
	}
	return $length;
}
}

if ( ! function_exists('monthByID')){
function monthByID($mon) {
	$month = '';
	switch($mon) {
		case 1:
			$month = 'Jan';
			break;
		case 2:
			$month = 'Feb';
			break;
		case 3:
			$month = 'Mar';
			break;
		case 4:
			$month = 'Apr';
			break;
		case 5:
			$month = 'May';
			break;
		case 6:
			$month = 'Jun';
			break;
		case 7:
			$month = 'Jul';
			break;
		case 8:
			$month = 'Aug';
			break;
		case 9:
			$month = 'Sep';
			break;
		case 10:
			$month = 'Oct';
			break;
		case 11:
			$month = 'Nov';
			break;
		case 12:
			$month = 'Dec';
			break;
	}
	return $month;
}
}

if ( ! function_exists('formatBytes')){
function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'k', 'M', 'G', 'T');   
    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}
}