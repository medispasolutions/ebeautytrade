<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('displayCustomerPrice')){
function displayCustomerPrice($list_price,$discount_expiration,$price){
	$CI =& get_instance();
	if($discount_expiration != '' && $discount_expiration != '0000-00-00' && $discount_expiration < date("Y-m-d")) {
		$price = $list_price;
	}
	if($CI->session->userdata('login_id') != '') {
		$id_user = $CI->session->userdata('login_id');
		$discounts = $CI->config->item('discounts');
		
		if(isset($discounts[$id_user]) || $CI->session->userdata('discount_auto_login')==1) {
		
			$dsc=0;
			if(isset($discounts[$id_user])){
			$dsc = $discounts[$id_user];}
			
			if($CI->session->userdata('discount_auto_login')==1 && $dsc<10){
				$dsc=0;
				}
				
			$newprice = round($list_price - ($list_price * ($dsc / 100)),3);
			
			//echo $newprice;exit;
			if($newprice > $price)
			$newprice = $price;
		}
		else {
			$newprice = $price;
		}
	}
	else {
		$newprice = $price;
	}

	//echo $newprice;exit;
	return $newprice;
}
}
if ( ! function_exists('removeCurrency')){
function removeCurrency($price){
	$currencies = $this->fct->getAll('currency_rates','sort_order');
$currency= $this->session->userdata('currency');
$find=array($currency,' ');
$replace=array('','');
$new_price = str_replace($find,$replace,$price);
return $price;
}
}

if ( ! function_exists('getMiles')){
function getMiles($general_miles,$price){
$miles=0;
	if($general_miles>=0 && $general_miles<=100){
	$price_miles=($general_miles*$price)/100;
	$miles=round($price_miles*10,2);}
	return $miles;
	
	
}
}
if ( ! function_exists('convert_number_to_words')){
function convert_number_to_words($number) {

$explode_number=explode(".", (string) $number);
if(isset($explode_number[1]) && $explode_number[1]==0){
	$number=$explode_number[0];
	}

    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}}

if ( ! function_exists('displayProductDiscountPrice')){
function displayProductDiscountPrice($discount,$discount_expiration,$price){
	$CI =& get_instance();
	if($discount_expiration != '' && $discount_expiration != '0000-00-00' && $discount_expiration < date("Y-m-d")) {
		$newprice=$price;
	}else{
		$newprice = round($price - ($price * ($discount / 100)), 2);
		}	
	//echo $newprice;exit;
	return $newprice;
}
}


if ( ! function_exists('displayWasCustomerPrice')){
function displayWasCustomerPrice($list_price){
	$CI =& get_instance();
	if($CI->session->userdata('login_id') != '') {
		$id_user = $CI->session->userdata('login_id');
		$discounts = $CI->config->item('discounts');
		if(isset($discounts[$id_user])) {
			$dsc = $discounts[$id_user];
			$newprice = round($list_price - ($list_price * ($dsc / 100)), 2);
		}
		else {
			$newprice = $list_price;
		}
	}
	else {
		$newprice = $list_price;
	}
	//echo $newprice;exit;
	return $newprice;
}
}

if ( ! function_exists('addDiscount')){
function addDiscount($price,$dis){
	return round($price - ($price * ($dis / 100)), 2);
}
}