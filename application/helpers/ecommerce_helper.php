<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('serializeStock')) {
    function serializeStock($options, $id_product)
    {
        $CI =& get_instance();
        $arr = array();
        $str = '';
        $text = '';
        $title = '';
        $id = $id_product;
        if (!empty($options)) {
            $str .= '{';
            $i = 0;
            $c = count($options);
            foreach ($options as $k => $v) {

                $i++;
                foreach ($v as $y => $s) {
                    $str .= $y . ':' . $s;
                    $text .= $s;
                    $option = $CI->fct->getonecell('attribute_options', 'title', array('id_attribute_options' => $y));
                    $title .= $option;
                    $id .= $y;
                }
                if ($i != $c) {
                    $str .= ',';
                    $text .= ' - ';
                    $title .= ' - ';
                }
            }
            $str .= '}';
        }
        $arr['str'] = $str;
        $arr['text'] = $text;
        $arr['title'] = $title;
        $arr['id'] = intval($id);
        //print'<pre>';print_r($arr);exit;
        return $arr;
    }
}

if (!function_exists('getTerms')) {
    function getTerms($user)
    {
        /** @var \core\ServiceLocator $serviceLocator */
        $serviceLocator = get_service_locator();
        $userModel = $serviceLocator->user($user['id_user']);

        $arr = array();
        $i = 0;

        if ($user['company_name']) {
            $arr[$i] = "<td>&nbsp;&nbsp;Supplier</td><td><b>{$user['company_name']}</b></td>";
            $i++;
        }
        
        if ($user['fulfilled_by']) {
            $arr[$i] = "<td>&nbsp;&nbsp;Fulfilled By</td><td>{$user['fulfilled_by']}</td>";
            $i++;
        }
        
        if ($user['delivery_shipping_from']) {
            $arr[$i] = "<td>&nbsp;&nbsp;Shipping from</td><td>{$user['delivery_shipping_from']}</td>";
            $i++;
        }

        if (!empty($user['delivery_time'])) {
            $text = $userModel->getDeliveryTime(true);
            if ($text) {
                $arr[$i] = "<td>&nbsp;&nbsp;Delivery time</td><td>{$text}</td>";
            } else {
                $arr[$i] = $user['delivery_time'];
            }
            $i++;
        }

        if (!empty($user['delivery_fee'])) {
            $text = $userModel->getDeliveryFee(true);
            if ($text) {
                $arr[$i] = "<td>&nbsp;&nbsp;Delivery fee</td><td>{$text}</td>";
            } else {
                $arr[$i] = $user['delivery_fee'];
            }
            $i++;
        }

        if (!empty($user['min_order'])) {
            $arr[$i] =  "<td>&nbsp;&nbsp;Minimum Order</td><td>". $user['min_order']."</td>";
            $i++;
        }

        if (!empty($user['credit_terms'])) {
            if ($user['credit_terms'] == 'consult-with-supplier') {
                $arr[$i] = "<td>&nbsp;&nbsp;Credit</td><td>".$userModel->getCreditTerms(true)."</td>";
            } elseif ($user['credit_terms'] == 'do-not-publish') {

            } else {
                $arr[$i] = $user['credit_terms'];
            }
            $i++;
        }

        $text = $userModel->getPaymentMethods(true);
        if ($text) {
            $arr[$i] = "<td>&nbsp;&nbsp;Payment methods</td><td>{$text}, Bank transfer</td>";
            $i++;
        }

        if (!empty($user['note'])) {
             $arr[$i] =  "<td>&nbsp;&nbsp;Note:</td><td>{$user['note']}</td>";
              $i++;
        }
        
        return $arr;

    }
}

if (!function_exists('add_quotes')) {
    function add_quotes($str)
    {
        return sprintf("'%s'", $str);
    }
}

if (!function_exists('getDeliverText')) {
    function getDeliverText($supllier_name, $text)
    {
        $find = array('{supplier name}');
        $replace = array($supllier_name);
        if (!empty($text)) {
            $deliver_txt = str_replace($find, $replace, $text);
        } else {
            $deliver_txt = 'Delivered by ' . $supllier_name;
        }

        return $deliver_txt;
    }
}

if (!function_exists('roundPrice')) {
    function roundPrice($price)
    {
        $p = round(floatval($price), 2);
        $n = number_format($p, 2, '.', ',');
        return $n;
    }
}

if (!function_exists('is_multi')) {
    function is_multi($a)
    {
        $rv = array_filter($a, 'is_array');
        if (count($rv) > 0) return true;
        return false;
    }
}

if (!function_exists('getLimitText')) {
    function getLimitText($field)
    {
        switch ($field) {
            case 'title':
                $result = 100;
                break;

            case 'description':
                $result = 300;
                break;
            case 'overview':
                $result = 1000;
                break;
            case 'meta_description':
                $result = 160;
                break;
            case 'page_title':
                $result = 450;
                break;

            default:
                $result = 10;
        }

        return $result;
    }
}

if (!function_exists('getMaxSize')) {
    function getMaxSize($field)
    {
        switch ($field) {
            case 'file':
                $result['size_mega'] = 1;
                $result['size'] = 1000000;
                break;
            case 'image':
                $result['size_mega'] = 1;
                $result['size'] = 1000000;
                break;

            default:
                $result['size_mega'] = 2;
                $result['size'] = 1000000;
        }

        return $result;
    }
}

if (!function_exists('checkIfInArray')) {
    function checkIfInArray($array, $id)
    {

        if (!empty($array)) {

            foreach ($array as $arr) {
                if ($arr['id_products'] == $id) return true;
            }
            return false;
        }
    }
}

if (!function_exists('getExpireDate')) {
    function getExpireDate()
    {
        $days = 30;
        return $days;
    }
}

if (!function_exists('unSerializeStock')) {
    function unSerializeStock($options)
    {
        if (!empty($options)) {
            $arr = array();
            $find = array('{', '}');
            $replace = array('', '');
            $options = str_replace($find, $replace, $options);
            $options = explode(',', $options);
            //print '<pre>';print_r($options);exit;
            if (!empty($options)) {
                foreach ($options as $opt) {
                    $d = explode(':', $opt);
                    $arr[$d[0]] = $d[1];
                }
            }
            return $arr;
        }
    }
}

if (!function_exists('arrangeStockIds')) {
    function arrangeStockIds($options)
    {
        $arr = array();
        if (!empty($options)) {
            foreach ($options as $key => $opt) {
                array_push($arr, $key);
            }
        }
        return $arr;
    }
}

if (!function_exists('serializecartOptions')) {
    function serializecartOptions($options, $field = '')
    {
        $arr = array();
        $str = '';
        if (!empty($options)) {
            $str .= '{';
            $i = 0;
            $c = count($options);
            foreach ($options as $k => $v) {
                $i++;
                $str .= $v['id_attribute_options'] . ':' . $v['title' . $field];
                if ($i != $c) {
                    $str .= ',';
                }
            }
            $str .= '}';
        }
        return $str;
    }
}


if (!function_exists('getAllCount')) {
    function getAllCount($attributes)
    {
        $count_all = 1;
        foreach ($attributes as $attr) {
            $count_all = $count_all * count($attr['options']);
        }
        return $count_all;
    }
}

if (!function_exists('PrepareAttributes')) {
    function PrepareAttributes($attributes, $arr, $c, $i = 0, $new1 = array(), $new2 = array())
    {
        $i++;
        if ($i <= $c) {
            $old = $new1;
            foreach ($arr as $k => $v) {
                $new1[] = array($v['id_attribute_options'] => $v['title']);
                if ($i == $c) {
                    $new2[] = $new1;
                    $new1 = $old;
                } else {
                    $new2 = PrepareAttributes($attributes, $attributes[$i]['options'], $c, $i, $new1, $new2);
                    array_splice($new1, ($i - 1));
                }
            }
            return $new2;
        } else {
            return false;
        }
    }
}

if (!function_exists('getCartIds')) {
    function getCartIds($array)
    {
        $ids = array();
        foreach ($array as $arr) {
            array_push($ids, $arr['id']);
        }
        return $ids;
    }
}

if (!function_exists('arrangeCartProducts')) {
    function arrangeCartProducts($cart_items, $productsData)
    {
        $products = array();
        $i = 0;
        //print '<pre>';print_r($productsData);exit;
        if (!empty($cart_items)) {
            foreach ($cart_items as $item) {
                $products[$i] = $item;
                foreach ($productsData as $prod) {
                    if ($prod['id_products'] == $item['id']) {
                        $products[$i]['product_info'] = $prod;
                        $i++;
                        break;
                    }
                }
            }
        }
        return $products;
    }
}

if (!function_exists('changeCurrency')) {
    function changeCurrency($price, $with_currency = 1, $currency_to = '', $currency_from = '')
    {
        $CI =& get_instance();

        $default_currency = $CI->config->item("default_currency");
        if ($currency_from == "") {
            $currency_from = $default_currency;
        }
        if ($currency_to == '') {
            if ($CI->session->userdata('currency')) {
                $currency_to = $CI->session->userdata('currency');
            } else {
                $currency_to = $default_currency;
                $CI->session->set_userdata('currency', $default_currency);
            }
        }

        $rate = $CI->config->item($currency_from . '_to_' . $currency_to);

        if ($rate != '')
            $price = $rate * $price;

        $n_price = round($price, 2);

        /*echo "n_price:".$n_price.'price:'.$price;exit;*/
        if ($n_price != $price) {
            /*if(($n_price < $price) && $n_price>1)
            $n_price = $n_price + 1;*/
        }
        //echo $n_price;exit;
        /*	if($with_currency == 1){
            $p=round(floatval($n_price),2);
            return $p.' <span class="currency_txt">'.displayCurrencyByLang($currency_to).'</span>';}
            else{
            $p=round(floatval($n_price),2);
            return $p;}*/

        if ($with_currency == 1) {
            $p = round(floatval($n_price), 2);
            $n = number_format($p, 2, '.', ',');
            return $n . ' <span class="currency_txt">' . displayCurrencyByLang($currency_to) . '</span>';
        } else {
            $p = round(floatval($n_price), 2);
            $n = number_format($p, 2, '.', ',');
            return $p;
        }

    }
}

if (!function_exists('displayCurrencyByLang')) {
    function displayCurrencyByLang($cur)
    {
        if (lang($cur) == '')
            return $cur;
        else
            return lang($cur);
    }
}

if (!function_exists('displayDiscount')) {
    function displayDiscount($discount)
    {
        return $discount . ' %<br />' . lang('off');
    }
}

if (!function_exists('cartTotalItems')) {
    function cartTotalItems()
    {
        $CI =& get_instance();
        $total = 0;
        $cart_items = $CI->cart->contents();
        foreach ($cart_items as $item) {
            $total = $total + $item['qty'];
        }
        return $total;
    }
}