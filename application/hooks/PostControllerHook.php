<?php

class PostControllerHook
{

    public function run()
    {
        $CI =& get_instance();

        if (!isset($CI->serviceLocator)) {
            $CI->serviceLocator = get_service_locator();
        }
    }

}