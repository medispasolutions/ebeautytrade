<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * PW: This is a hard hack, but I don't know how to autoload specific controller classes, so I require them here
 * cause I want them to be in the controllers directory
 */

require_once(APPPATH.'controllers/BaseController.php');
require_once(APPPATH.'controllers/BaseShopController.php');
require_once(APPPATH.'controllers/back_office/BaseBackofficeController.php');

//some random classes
if (!function_exists('get_service_locator')) {
    $_SERVICE_LOCATOR = new \core\ServiceLocator();
    /**
     * @return \core\ServiceLocator
     */
    function get_service_locator()
    {
        global $_SERVICE_LOCATOR;
        return $_SERVICE_LOCATOR;
    }
}

if (!function_exists('trimEmptyLines')) {
    function trimEmptyLines($content)
    {
        $content = explode("\r\n", $content);
        foreach ($content as $i=>$w) {
            if (!trim($w)) {
                unset($content[$i]);
            } else {
                $content[$i] = trim($w);
            }
        }
        return implode("\r\n", $content);
    }
}

if (!function_exists('reallyEmpty')) {
    function reallyEmpty($content)
    {
        if (!is_array($content)) {
            return empty($content);
        } elseif (empty($content)) {
            return true;
        }

        $empty = true;
        foreach ($content as $i=>$w) {
            if (!is_array($w)) {
                if (!empty($w)) {
                    $empty = false;
                    break;
                }
            } else {
                if (!reallyEmpty($w)) {
                    $empty = false;
                    break;
                }
            }
        }
        return $empty;
    }
}

if (!function_exists('notEmpty')) {
    function notEmpty($content)
    {
        if (!is_array($content)) {
            return !empty($content);
        } elseif (empty($content)) {
            return false;
        }

        $notEmpty = true;
        foreach ($content as $i=>$w) {
            if (!is_array($w)) {
                if (empty($w)) {
                    $notEmpty = false;
                    break;
                }
            } else {
                if (!notEmpty($w)) {
                    $notEmpty = false;
                    break;
                }
            }
        }
        return $notEmpty;
    }
}

if (!function_exists('trimAll')) {
    function trimAll($data)
    {
        if (!is_array($data)) {
            return trim($data);
        } else {
            $tmp = [];
            foreach ($data as $id => $v) {
                $tmp[$id] = trimAll($v);
            }
            return $tmp;
        }
    }
}