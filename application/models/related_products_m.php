<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Related_products_m extends CI_Model{

public function get_related_products($id){
$this->db->where('id_related_products',$id);
$this->db->where('deleted',0);
$query = $this->db->get('related_products');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  related_products.* ";
        $q .= " from  related_products ";
		$q .= " where related_products.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(related_products.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}