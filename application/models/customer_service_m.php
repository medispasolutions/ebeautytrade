<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_service_m extends CI_Model{

public function get_customer_service($id){
$this->db->where('id_customer_service',$id);
$this->db->where('deleted',0);
$query = $this->db->get('customer_service');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'customer_service')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('customer_service');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}