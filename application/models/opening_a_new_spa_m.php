<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Opening_a_new_spa_m extends CI_Model{

public function get_opening_a_new_spa($id){
$this->db->where('id_opening_a_new_spa',$id);
$this->db->where('deleted',0);
$query = $this->db->get('opening_a_new_spa');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'opening_a_new_spa')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('opening_a_new_spa');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}