<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact_branches_m extends CI_Model{

public function get_contact_branches($id){
$this->db->where('id_contact_branches',$id);
$this->db->where('deleted',0);
$query = $this->db->get('contact_branches');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  contact_branches.* ";
        $q .= " from  contact_branches ";
		$q .= " where contact_branches.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(contact_branches.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}