<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promocode_m extends CI_Model
{
    public function get_promocode($id)
    {
        $this->db->where('id_promo_code', $id);
        $this->db->where('deleted', 0);
        $query = $this->db->get('promo_code');
        return $query->row_array();
    }

    public function list_paginate($order, $limit, $offset)
    {
        $this->db->where('deleted', 0);
        if ($this->db->field_exists($order, 'promo_code')) {
            $this->db->order_by($order);
        }
        $this->db->limit($limit, $offset);
        $query = $this->db->get('promo_code');
        return $query->result_array();
    }

    public function getAll($table, $order)
    {
        $this->db->where('deleted', 0);
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    public function getAll_cond($cond = array(), $limit = '', $offset = '')
    {
        $this->db->select('*');
        $this->db->from('promo_code');
        $this->db->where('deleted', 0);
        if (!empty($cond))
            $this->db->where($cond);
        $this->db->order_by('sort_order');
        if ($limit != '')
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($limit != '') {
            $results = $query->result_array();
            if (!empty($results)) {
                foreach ($results as $key => $res) {
                    $this->db->select('*');
                    $this->db->from('promo_code');
                    $this->db->where('deleted', 0);
                    $this->db->order_by('sort_order');
                    $query = $this->db->get();
                    $resss = $query->result_array();
                    $results[$key]['sub_levels'] = $resss;
                }
            }
        } else {
            $results = $query->num_rows();
        }
        return $results;
    }
    
    
///////////////////////////////////////////CHECKLIST PROMO CODE/////////////////////////////////////////////////////////	
    public function getRecords($cond, $limit = "", $offset = "")
    {
        $q = "";
        $q .= " select promo_code.* ";
        $q .= " from promo_code ";
        $q .= " where promo_code.deleted=0 ";

        if (isset($cond["id_user"]) && !empty($cond["id_user"])) {

            $q .= ' and promo_code.id_user=' . $cond["id_user"];
        }

        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }

        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }

        return $data;
    }
}