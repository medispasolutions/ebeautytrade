<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Brands_m extends CI_Model
{

    public function get_brands($id)
    {
        $this->db->where('id_brands', $id);
        $this->db->where('deleted', 0);
        $query = $this->db->get('brands');
        return $query->row_array();
    }


    public function list_paginate($order, $limit, $offset, $cond)
    {
        $cond['deleted'] = 0;
        if (isset($cond['brand_name'])) {
            $this->db->like('UPPER(title)', strtoupper($cond['brand_name']));
            unset($cond['brand_name']);
        }
        $this->db->where($cond);
        if ($this->db->field_exists($order, 'brands')) {
            $this->db->order_by($order);
        }
        $this->db->limit($limit, $offset);
        $query = $this->db->get('brands');

        return $query->result_array();
    }

    public function getAll($table, $order, $cond)
    {
        $cond['deleted'] = 0;
        if (isset($cond['brand_name'])) {
            $this->db->like('UPPER(title)', strtoupper($cond['brand_name']));
            unset($cond['brand_name']);
        }
        $this->db->where($cond);
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($table);

        return $query->num_rows();
    }

///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
    public function getRecords($cond, $limit = "", $offset = "")
    {
        $q = "";
        $q .= " select  brands.* ";
        $q .= " from  brands ";
        $q .= " where brands.deleted=0 ";

        if (isset($cond["brand_name"]) && !empty($cond["brand_name"])) {

            $q .= " and UPPER(brands.title) like '%" . strtoupper($cond["brand_name"]) . "%' ";
        }

        if (isset($cond["id_user"]) && !empty($cond["id_user"])) {

            $q .= ' and brands.id_user=' . $cond["id_user"];
        }

        if (isset($cond["brand"]) && !empty($cond["brand"])) {

            $q .= ' and brands.id_brands=' . $cond["brand"];
        }
        
        if (isset($cond["status"]) && !empty($cond["status"])) {

            $q .= ' and brands.status=' . $cond["status"];
        }
        
        if (isset($cond["set_as_featured"]) && !empty($cond["set_as_featured"])) {

            $q .= ' and brands.set_as_featured=' . $cond["set_as_featured"];
        }

        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }

        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }

        return $data;
    }

}