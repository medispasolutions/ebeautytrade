<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Trading_license_m extends CI_Model{

public function get_trading_license($id){
$this->db->where('id_trading_license',$id);
$this->db->where('deleted',0);
$query = $this->db->get('trading_license');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  trading_license.* ";
        $q .= " from  trading_license ";
		$q .= " where trading_license.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(trading_license.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}