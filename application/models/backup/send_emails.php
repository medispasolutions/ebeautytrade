<?php

class Send_emails extends CI_Model
{

    //=================================================
    // functions validated by BI

    /**
     * Sent to the admins after shop user initiates registration with a valid email (even if not all of the data is correct)
     * @param $data
     */
    public function sendFirstStep($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $condition2 = array('id_auto_reply_messages' => 13);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $sysdata = date("Y-m-d h:i:s");
        $subject = 'New first step registration on ' . $data['created_date'];

        $data['user'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/firstStep', $data, true);

        //  $body.='<p><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    /**
     * Sent to the admins after shop user finishes registration
     * @param $data
     */
    public function sendUserToAdmin($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $condition2 = array('id_auto_reply_messages' => 13);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $sysdata = date("Y-m-d h:i:s");
        $subject = 'New user registered on ' . $data['created_date'];

        $data['user'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/registerAdmin', $data, true);

        //  $body.='<p><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    /**
     * Sent to the shop user after he submits his registration
     * @param $data
     * @return bool
     */
    public function sendUserReply($data)
    {
        $config = $this->getSenderConfig();

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];
        // echo $admindata['website_title'.getFieldLanguage($this->lang->lang())];exit;


        $condition2 = array('id_auto_reply_messages' => 13);

        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];
        $data['user'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;

        // get reply message

        $body = $this->wrapUserEmail($this->load->view('emails/new/to_user_after_registration', $data, true));

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(_getenv('MAILER_DEFAULT_FROM'), $adminname);
        $this->email->to($data['email']);
        $this->email->subject('Welcome, ' . $data['user']['name'] . '!');
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => _getenv('MAILER_DEFAULT_FROM'),
                'from_name' => $adminname,
                'to' => $data['email'],
                'subject' => 'Welcome, ' . $data['user']['name'] . '!',
                'message' => $body,
            ]);
            return true;
        }

        return $this->email->send();
    }

    /**
     * This is the email with the link to password reset
     * @param $data
     * @param $request
     * @param bool $admin
     * @return bool
     */
    public function sendPasswordRequest($data, $request, $admin = false)
    {
        $config = $this->getSenderConfig();

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $condition2 = array('id_auto_reply_messages' => 13);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) {
            $adminemail = $replymessage['email'];
        }

        // get reply message
        if ($admin) {
            $reset_link = route_to('back_office/home/loginbypassword') . '?token=' . $request['token'];
        } else {
            $reset_link = route_to('user/loginbypassword') . '?token=' . $request['token'];
        }
        $data['user'] = $data;
        $data['adminname'] = $adminname;
        $data['reset_link'] = $reset_link;

        $body = $this->wrapUserEmail($this->load->view('emails/new/to_user_and_supplier_password_reset', $data, true));

        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(_getenv('MAILER_DEFAULT_FROM'), $adminname);
        $this->email->to($data['email']);
        $this->email->subject(lang('password_request') . ' - ' . $adminname);
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => _getenv('MAILER_DEFAULT_FROM'),
                'from_name' => $adminname,
                'to' => $data['email'],
                'subject' => lang('password_request') . ' - ' . $adminname,
                'message' => $body,
            ]);
            return true;
        }

        return $this->email->send();
    }


    public function inform_user_about_account_activation($data)
    {
        $data['user'] = $data;
        $data['admindata'] = $admindata;
        $body = $this->wrapUserEmail($this->load->view('emails/new/to_user_account_activated', $data, true));

        $send['message'] = $body;
        $send['subject'] = "Your account at spamiles.com is activated";
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminname = $admindata['website_title' . getFieldLanguage()];
        $send['from_name'] = $adminname;

        $send['to_email'] = $data['email'];

        return $this->fct->sendEmail($send);
    }

    /**
     * Inform user about changes to his account done by the admin (possibly accepting his account)
     * @param $data
     */
    public function inform_client($data)
    {
        if ($data['status'] == 1 && $data['id_roles'] == 4) {
            return $this->inform_user_about_account_activation($data);
        }

        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        if ($data['status'] == 1) {
            if ($data['id_roles'] == 5) {
                $id_auto_reply_messages = 23;
            } else {
                $id_auto_reply_messages = 14;
            }
        } else {
            if ($data['status'] == 2) {
                $id_auto_reply_messages = 17;
            } else {
                $id_auto_reply_messages = 7;
            }
        }
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $data['user'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/inform_client', $data, true);
        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email

        $send['message'] = $body;
        $send['subject'] = $replymessage['title' . getFieldLanguage()];
        $send['from_email'] = $adminemail;
        $send['from_name'] = $adminname;
        $send['to_email'] = $data['email'];

        $this->fct->sendEmail($send);
    }

    /**
     * Inform admins about new order
     * @param $orderData
     */
    public function sendOrderToAdmin($orderData)
    {
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $orderData = $this->ecommerce_model->getOrder($orderData['id_orders']);
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];
        $data['orderData'] = $orderData;
        $data['supplier_e'] = 2;

        $subject = 'New order has been submitted at ' . $adminname . ' on ' . $orderData['created_date'];
        // get reply message
        $body = '<p>' . $subject . '</p>';
        $body .= $this->load->view('emails/order', $data, true);
        $body .= '<p>To view order in the admin  click on the below link:<br />';
        $adminOrderLink = base_url() . 'back_office/orders/view/' . $orderData['id_orders'] . '/' . $orderData['rand'];
        $body .= '<a href="' . $adminOrderLink . '">' . $adminOrderLink . '</a>';
        $body .= '</p>';

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    /**
     * Send the order details to the user
     * @param $orderData
     * @param int $id_auto_reply
     * @return bool
     */
    public function sendOrderToUser($orderData, $id_auto_reply = 0)
    {
        $config = $this->getSenderConfig();

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        if ($id_auto_reply == 1) {
            $subject = 'Spamiles: Order #' . $orderData['id_orders'];
        } else {
            $subject = 'Spamiles: New Order #' . $orderData['id_orders'];
        }
        // get reply message
        $data['orderData'] = $orderData;
        $data['supplier_e'] = 2;
        $data['id_auto_reply'] = $id_auto_reply;
        $body = $this->wrapUserEmail($this->load->view('emails/new/to_user_order_confirmation', $data, true));

        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(_getenv('MAILER_DEFAULT_FROM'), $adminname);
        $this->email->to($orderData['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => _getenv('MAILER_DEFAULT_FROM'),
                'from_name' => $adminname,
                'to' => $orderData['user']['email'],
                'subject' => $subject,
                'message' => $body,
            ]);
            return true;
        }

        return $this->email->send();
    }

    /**
     * Send order details to supplier
     * @param $orderData
     * @return bool
     */
    public function sendOrderToSupplier($orderData)
    {
        //make sure we have all data
        $orderData = $this->ecommerce_model->getOrder($orderData['id_orders']);
        $supplierData = $this->fct->getonerow('user', array('id_user' => $orderData['id_supplier']));

        //determine recipients
        $recipients = [];

        //first company email of supplier (or his email if not present)
        if ($supplierData['company_email']) {
            $recipients[] = $supplierData['company_email'];
        } elseif($supplierData['email']) {
            $recipients[] = $supplierData['email'];
        }

        //further recipients - people responsible for brands
        $this->db->select('line_items.*');
        $this->db->from('line_items');
        $this->db->where('id_orders', $orderData['id_orders']);
        $orderLines = $this->db->get()->result_array();
        foreach ($orderLines as $line) {
            $productData = $this->fct->getonerow('products', array('id_products' => $line['id_products']));
            $brandData = $this->fct->getonerow('brands', array('id_brands' => $productData['id_brands']));

            if ($brandData['email'] && !in_array($brandData['email'], $recipients)) {
                $recipients[] = $brandData['email'];
            }
        }

        //if no recipients (shouldn't be possible) then no mail
        if (empty($recipients)) {
            return;
        }

        //determine from name
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        //prepare email data
        $data['orderData'] = $orderData;
        $data['supplier_e'] = 1;
        $supplierData = $this->fct->getonerow('user', array('id_user' => $data['orderData']['id_supplier']));

        //prepare email config, subject and body
        $config = $this->getSenderConfig();
        $subject = 'Spamiles: New Order #' . $orderData['id_orders'];
        $body = $this->load->view('emails/order', $data, true);

        //send email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(_getenv('MAILER_DEFAULT_FROM'), $adminname);
        $this->email->to(implode(',', $recipients));
        $this->email->subject($subject);
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => _getenv('MAILER_DEFAULT_FROM'),
                'from_name' => $adminname,
                'to' => implode(',', $recipients),
                'subject' => $subject,
                'message' => $body,
            ]);
            return true;
        }

        return $this->email->send();
    }

    /**
     * Mail to admin about a filled contact form by user on a product page
     * @param $data
     */
    public function sendRequestToAdmin($data)
    {
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $id_auto_reply_messages = 18;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);

        $subject = "New enquiry from user";
        $data['request'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/meetingRequestToAdmin', $data, true);

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    /**
     * Mail to supplier about a filled contact form
     * @param $data
     */
    public function sendRequestToBrand($data)
    {
        $config = $this->getSenderConfig();

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 18;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);

        $subject = "Enquiry from Spamiles Added by " . $data['name'];
        $data['request'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/meetingRequestToBrand', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(_getenv('MAILER_DEFAULT_FROM'), $adminname);
        $this->email->to($data['brand']['email']);
        $this->email->subject($subject);
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => _getenv('MAILER_DEFAULT_FROM'),
                'from_name' => $adminname,
                'to' => $data['brand']['email'],
                'subject' => $subject,
                'message' => $body,
            ]);
            return true;
        }

        return $this->email->send();
    }

    public function sendRequestToClient($data)
    {
        $config = $this->getSenderConfig();

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 18;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = "Enquiry sent";
        $data['request'] = $data;
        $data['replyMessage'] = $replymessage;
        $data['admindata'] = $admindata;

        $body = $this->wrapUserEmail($this->load->view('emails/new/to_user_enquiry_confirmation', $data, true));

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from(_getenv('MAILER_DEFAULT_FROM'), $adminname);
        $this->email->to($data['email']);
        $this->email->subject($subject);
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => _getenv('MAILER_DEFAULT_FROM'),
                'from_name' => $adminname,
                'to' => $data['email'],
                'subject' => $subject,
                'message' => $body,
            ]);
            return true;
        }

        return $this->email->send();
    }

    public function sendProductStatusChangeToAdmin($user, $product)
    {
        $brand = $this->fct->getonerecord('brands', array('id_brands' => $product['id_brands']));

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $id_auto_reply_messages = 15;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);

        $status = "";
        switch ($product['status']) {
            case 0:
                $status = "Published";break;
            case 1:
                $status = "Unpublished";break;
            case 2:
                $status = "Under review";break;
            case 3:
                $status = "Draft";break;
        }
        
        $subject = 'New status (' . $status . ') of product "' . $product['title'] . '" By ' . $user['name'] . ' Brand ' . $brand['title'] . ' on ' . date("F d,Y g:i a", strtotime($brand['created_date']));

        $data['user'] = $user;
        $data['subject'] = $subject;
        $data['product'] = $product;
        $data['brand'] = $brand;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;

        $body = $this->load->view('emails/supplier_modifications', $data, true);

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    public function sendBrandToAdmin($brand, $user)
    {
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $id_auto_reply_messages = 24;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        /*$subject="New brands have been added to ".$user['name']." on ".date("F d,Y g:i a", strtotime($brand['created_date']));*/

        $subject = "New brand: " . $brand['title'] . " has been added by " . $user['name'] . " on " . date("F d,Y g:i a", strtotime($brand['created_date']));

        $data['user'] = $user;
        $data['brand'] = $brand;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/sendBrandToAdmin', $data, true);
        $send['message'] = $body;
        $send['subject'] = $subject;
        return $this->fct->sendEmail($send);
    }

    /**
     * Wrap email to user into a common wrapper
     * @param $content
     * @return mixed
     */
    protected function wrapUserEmail($content)
    {
        $data = array();
        $data['content'] = $content;
        $data['site_url'] = site_url();
        return $this->load->view('emails/new/_user_template', $data, true);
    }

    /**
     * Get standard email config
     * @return array
     */
    protected function getSenderConfig()
    {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => _getenv('MAILER_HOST'),
            'smtp_port' => (int)_getenv('MAILER_PORT'),
            'smtp_user' => _getenv('MAILER_USER'), // change it to yours
            'smtp_pass' => _getenv('MAILER_PASS'), //change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
            //'wordwrap' => TRUE
        );

        return $config;
    }



    //=================================================
    // rest of the functions

/////////////////////////////////////////////////////////////////////////////






    public function sendBookingMeeting($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];
        // echo $admindata['website_title'.getFieldLanguage($this->lang->lang())];exit;


        $condition2 = array('id_auto_reply_messages' => 13);

        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];
        $data['user'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;

        // get reply message

        $body = $this->load->view('emails/campaign_email', $data, true);

        //  $body.='<p><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject('Thank you for your interest!');
        $this->email->message($body);
        $this->email->send();
    }

    public function sendGuestReply($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];
        // echo $admindata['website_title'.getFieldLanguage($this->lang->lang())];exit;


        $condition2 = array('id_auto_reply_messages' => 13);

        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];
        $data['user'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;

        // get reply message

        $body = $this->load->view('emails/registerGuest', $data, true);
        //  $body.='<p><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject('Welcome to Spamiles!');
        $this->email->message($body);
        $this->email->send();
    }

/////////////////////////////////NewsLetter////////////////////////////////////////////
    public function sendNewsLetter($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );
        $user = $this->ecommerce_model->getUserInfo();
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];
        // echo $admindata['website_title'.getFieldLanguage($this->lang->lang())];exit;

        $condition2 = array('id_auto_reply_messages' => 33);

        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;

        // get reply message

        $body = $this->load->view('emails/newsletter', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject($replymessage['title'] . ' - ' . $adminname . '!');
        $this->email->message($body);
        $this->email->send();
    }

/////////////////////////////////////////////////////////////////////////////


    public function sendQuotation($orderData)
    {

        $emails = $this->input->post('emails');
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $subject = 'Spamiles: Quotation No. ' . $orderData['id_quotation'] . ' from ' . $orderData['user']['name'] . '';
        // get reply message
        $data['orderData'] = $orderData;
        $body = $this->load->view('emails/quotation', $data, true);
        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from($orderData['user']['email'], $orderData['user']['name']);
        $this->email->to(implode(',', $emails));
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }


    public function sendUpdateOrderToUser($orderData)
    {


        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $subject = 'Spamiles:  Order #' . $orderData['id_orders'] . ' is updated.';
        // get reply message
        $data['orderData'] = $orderData;
        $body = $this->load->view('emails/order', $data, true);
        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from($adminemail, $adminname);
        $this->email->to($orderData['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

/////////////////////////////////////////////////////////////////////////////
    public function sendDeliveryOrderToAdmin($deliveryOrder)
    {


        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $orderData = $this->admin_fct->getDeliveryOrder($deliveryOrder['id_delivery_order']);
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];
        $data['orderData'] = $orderData;

        $id_auto_reply_messages = 31;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = 'New delivery order has been submitted on ' . $orderData['created_date'];
        // get reply message
        $body = '<p>' . $subject . '</p>';
        $body .= $this->load->view('emails/delivery_order', $data, true);
        $body .= '<p>To view order in the admin  click on the below link:<br />';
        $adminOrderLink = base_url() . 'back_office/inventory/view_delivery_order/' . $orderData['id_delivery_order'] . '/' . $orderData['rand'];
        $body .= '<a href="' . $adminOrderLink . '">' . $adminOrderLink . '</a>';
        $body .= '</p>';


        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");

	  $this->email->from($orderData['user']['email'],$orderData['user']['name']);
	  $this->email->to($adminemail);

	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/
        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    public function sendDeliveryOrderToUser($deliveryOrder)
    {
        $orderData = $this->admin_fct->getDeliveryOrder($deliveryOrder['id_delivery_order']);
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $id_auto_reply_messages = 31;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = 'Spamiles: New Request Order #' . $orderData['id_delivery_order'];
        // get reply message
        $data['orderData'] = $orderData;
        $body = $this->load->view('emails/delivery_order', $data, true);
        /*	  $body .= '<p>To view order in the admin  click on the below link:<br />';
	  $adminOrderLink = base_url().'back_office/inventory/view_delivery_order/'.$orderData['id_delivery_order'].'/'.$orderData['rand'];
	  $body .= '<a href="'.$adminOrderLink.'">'.$adminOrderLink.'</a>';
	  $body .= '</p>';*/
        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");


        $this->email->from($adminemail, $adminname);
        $this->email->to($orderData['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

    public function inform_supplier_deliveryOrder($orderData)
    {
        /*$orderData = $this->admin_fct->getDeliveryOrder($orderData['id_delivery_order']);*/
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $id_auto_reply_messages = 31;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        if (!empty($orderData['id_invoice'])) {
            $id_order = $orderData['id_invoice'];
        } else {
            $id_order = $orderData['id_delivery_order'];
        }
        $subject = 'Spamiles: ' . $orderData['order_name'] . '#' . $id_order;
        // get reply message

        $data['orderData'] = $orderData;
        $data['type'] = $orderData['type'];
        $data['form'] = $orderData['form'];
        $data['export'] = $orderData['export'];
        $data['page_title'] = $orderData['page_title'];
        $data['order_name'] = $orderData['order_name'];
        $data['section_name'] = $orderData['section_name'];

        $body = $this->load->view('emails/delivery_order', $data, true);
        /*	  $body .= '<p>To view order in the admin  click on the below link:<br />';
	  $adminOrderLink = base_url().'back_office/inventory/view_delivery_order/'.$orderData['id_delivery_order'].'/'.$orderData['rand'];
	  $body .= '<a href="'.$adminOrderLink.'">'.$adminOrderLink.'</a>';
	  $body .= '</p>';*/
        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");


        $this->email->from($adminemail, $adminname);
        $this->email->to($orderData['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

    public function inform_admin_deliveryOrder($orderData)
    {
        /*$orderData = $this->admin_fct->getDeliveryOrder($orderData['id_delivery_order']);*/
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $id_auto_reply_messages = 31;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        if (!empty($orderData['id_invoice'])) {
            $id_order = $orderData['id_invoice'];
        } else {
            $id_order = $orderData['id_delivery_order'];
        }
        $subject = 'Spamiles: ' . $orderData['order_name'] . '#' . $id_order;
        // get reply message

        $data['orderData'] = $orderData;
        $data['type'] = $orderData['type'];
        $data['form'] = $orderData['form'];
        $data['export'] = $orderData['export'];
        $data['page_title'] = $orderData['page_title'];
        $data['order_name'] = $orderData['order_name'];
        $data['section_name'] = $orderData['section_name'];

        $body = $this->load->view('emails/delivery_order', $data, true);
        $body .= '<p>To view order in the admin  click on the below link:<br />';
        $adminOrderLink = base_url() . 'back_office/inventory/view_delivery_order/' . $orderData['id_delivery_order'] . '/' . $orderData['rand'];
        $body .= '<a href="' . $adminOrderLink . '">' . $adminOrderLink . '</a>';
        $body .= '</p>';
        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");


        $this->email->from($orderData['user']['email'], $orderData['user']['trading_name']);
        $this->email->to($adminemail);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

    public function sendRequestOrderToUser($deliveryOrder)
    {
        $orderData = $this->admin_fct->getDeliveryOrder($deliveryOrder['id_delivery_order']);
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $id_auto_reply_messages = 31;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = 'Spamiles: New Request Order #' . $orderData['id_delivery_order'];
        // get reply message
        $data['orderData'] = $orderData;
        /*$body = $this->load->view('emails/delivery_order',$data,true);*/
        $body = $this->load->view('emails/delivery_order', $data, true);
        /*	  $body .= '<p>To view order in the admin  click on the below link:<br />';
	  $adminOrderLink = base_url().'back_office/inventory/view_delivery_order/'.$orderData['id_delivery_order'].'/'.$orderData['rand'];
	  $body .= '<a href="'.$adminOrderLink.'">'.$adminOrderLink.'</a>';
	  $body .= '</p>';*/
        //  print '<pre>';print_r($orderData);exit;
        // get admin email


        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");


        $this->email->from($adminemail, $adminname);
        $this->email->to($orderData['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

    public function sendRequestOrderToAdmin($deliveryOrder)
    {
        $orderData = $this->admin_fct->getDeliveryOrder($deliveryOrder['id_delivery_order']);

        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $id_auto_reply_messages = 31;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);

        $user = $this->fct->getonerecord('user', array('id_user' => $orderData['id_user']));

        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];
        $subject = 'Spamiles: New Request Order #' . $orderData['id_delivery_order'] . ' sent to ' . $user['name'];
        // get reply message
        $data['orderData'] = $orderData;
        /*$body = $this->load->view('emails/delivery_order',$data,true);*/
        $body = $this->load->view('emails/delivery_order', $data, true);
        /*	  $body .= '<p>To view order in the admin  click on the below link:<br />';
	  $adminOrderLink = base_url().'back_office/inventory/view_delivery_order/'.$orderData['id_delivery_order'].'/'.$orderData['rand'];
	  $body .= '<a href="'.$adminOrderLink.'">'.$adminOrderLink.'</a>';
	  $body .= '</p>';*/
        //  print '<pre>';print_r($orderData);exit;
        // get admin email


        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");


	  $this->email->from($orderData['user']['email'],$orderData['user']['name']);
	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/
        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }


    public function stock_return($orderData)
    {
        $id_stock_return = $orderData['id_stock_return'];
        $rand = $orderData['rand'];
        $orderData = $this->admin_fct->getReturnStockOrder($id_stock_return, $cond);
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $id_auto_reply_messages = 31;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = 'Spamiles: D-Order#' . $orderData['id_delivery_order'] . " is updated";
        // get reply message
        $data['orderData'] = $orderData;
        $datap['return'] = 1;
        $body = $this->load->view('emails/stock_return', $data, true);
        /*	  $body .= '<p>To view order in the admin  click on the below link:<br />';
	  $adminOrderLink = base_url().'back_office/inventory/view_delivery_order/'.$orderData['id_delivery_order'].'/'.$orderData['rand'];
	  $body .= '<a href="'.$adminOrderLink.'">'.$adminOrderLink.'</a>';
	  $body .= '</p>';*/
        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");


        $this->email->from($adminemail, $adminname);
        $this->email->to($orderData['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }


//////////////////////////////////////////////////////////////////
    public function sendUpdateInvoiceToUser($orderData)
    {


        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage($this->lang->lang())];

        $id_auto_reply_messages = 32;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = 'Spamiles:  Invoice#' . $orderData['invoice']['id_invoices'] . '-' . $orderData['invoice']['status'];
        // get reply message
        $data["info"] = $orderData;
        $data["userData"] = $data["info"]['user'];
        $data['orderData'] = $data["info"]['invoice'];
        $body = $this->load->view('emails/supplier_invoice', $data, true);
        //  print '<pre>';print_r($orderData);exit;
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");


        /*$data["info"]['user']['email']="alaa.zaklit@dowgroup.com";*/
        $this->email->from($adminemail, $adminname);
        $this->email->to($data["info"]['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

//////////////////////////////////////////////////////////////////
    public function sendNewsletterToAdmin($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title'];

        $sysdata = date("Y-m-d h:i:s");
        $subject = 'New email added to our newsletter system: ' . $data['created_date'];
        $body = '<p>' . $subject . '.</p>
	  <p>
	  <u><strong>Details :</strong></u></p>';
        /*
	  if(!empty($data['name'])) {
	  $body.='<p>
	  	<strong>Name : </strong>'.$data['name'].'</p>';
	  }*/
        if (!empty($data['email'])) {
            $body .= '<p>
	  	<strong>Email : </strong><a href="mailto:' . $data['email'] . '">' . $data['email'] . '</a></p>';
        }
        //  $body.='<p><strong>Thank you, <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email

        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($data['email']);
	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }


    public function sendNewsletterReplyToUser($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title'];

        $condition2 = array('id_auto_reply_messages' => 4);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        $message = '';
        $message = $replymessage['message'];
        // get reply message
        $body = '
	  <p>Dear ' . ucfirst($data['name']) . '</span>,</p>
	  <p>' . $message . '</p>';
        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject($replymessage['title']);
        $this->email->message($body);
        $this->email->send();
    }


//////////////////////////////////////////////////////////////////
    public function sendProductsInquiry($id)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];


        $condition2 = array('id_auto_reply_messages' => 16);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $data['product_inquiry'] = $this->fct->getonerecord('product_inquiry', array('id_product_inquiry' => $id));
        $user = $this->fct->getonerecord('user', array('id_user' => $data['product_inquiry']['id_user']));
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/products_inquiry', $data, true);
        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email
        /*	$adminemail="alaa.zaklit@dowgroup.com";
	$user['email']="alaa.zaklit@dowgroup.com";*/
        $subject = "New inquiry quantity from " . $user['name'];
        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($user['email'],$user['name']);

	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }


////////////////////////////PRE REGRISTRATION//////////////////////////////////////
    public function inform_pre_registration($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 34;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $data['user'] = $data;
        $data['link'] = route_to('user/pre_register/' . $data['id_user'] . '/' . $data['token']);
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/inform_pre_registration', $data, true);
        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';


        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject('Welcome, ' . $data['name'] . '!');
        $this->email->message($body);
        $this->email->send();
    }

    public function inform_pre_registration_admin($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 34;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $data['user'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/inform_pre_registration_admin', $data, true);
        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';


        // get admin email
        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($data['user']['email'],$data['user']['name']);
	  $this->email->to($adminemail);
	  $this->email->subject($data['user']['name'].' is completed his profile');
	  $this->email->message($body);
	  $this->email->send();*/

        $send['message'] = $body;
        $send['subject'] = $data['user']['name'] . ' is completed his profile';
        $this->fct->sendEmail($send);
    }


///////////////////////////////CONTACTUS///////////////////////////////////
    public function sendContactUsToAdmin($data, $id_auto_reply_messages = 35)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = $replymessage['title'];
        $data['request'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/sendContactUsToAdmin', $data, true);

        $send['message'] = $body;
        if ($data['type'] == "booking") {
            $send['subject'] = 'New booking message from  ' . $data['name'] . ' on ' . $data['created_date'];
        } else {
            $send['subject'] = 'New contact us message from  ' . $data['name'] . ' on ' . $data['created_date'];
        }
        $this->fct->sendEmail($send);
    }

    public function sendContactUsToUser($data, $id_auto_reply_messages = 35)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];


        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = $replymessage['title'];
        $data['request'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/sendContactUsToUser', $data, true);


        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject($subject);
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => $adminemail,
                'from_name' => $adminname,
                'to' => $data['email'],
                'subject' => $subject,
                'message' => $body,
            ]);
            return true;
        }

        $this->email->send();

    }


///////////////////////////////BRAND///////////////////////////////////




///////////////////////////////QUESTION///////////////////////////////////
    public function sendRequestPhoneToBrand($data, $id_auto_reply_messages = 37)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];


        $subject = $replymessage['title'];
        $data['request'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/requestForPhone', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($data['email'], $data['name']);
        $this->email->to($data['brand']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

///////////////////////////////QUESTION///////////////////////////////////
    public function sendQuestionToAdmin($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 29;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = $replymessage['title'];
        $data['request'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/questionToAdmin', $data, true);


        // get admin email
        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($data['email'],$data['name']);
	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    public function sendQuestionToClient($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 29;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = $replymessage['title'];
        $data['request'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/questionToClient', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

    public function question_inform_client($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 29;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $data['data'] = $data;

        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/question_inform_client', $data, true);
        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email

        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject('Q&A | Spamiles');
        $this->email->message($body);
        $this->email->send();
    }

///////////////////////////////QUESTION///////////////////////////////////
    public function sendSpaToAdmin($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 30;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = $replymessage['title'];
        $data['data'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/spaToAdmin', $data, true);


        // get admin email
        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($data['email'],$data['name']);
	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    public function sendSpaToClient($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 30;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = $replymessage['title'];
        $data['data'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/spaToClient', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }


///////////////////////////////PRODUCTS///////////////////////////////////
    public function sendDeleteProductsToAdmin($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 19;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = 'New deleted product(s) from ' . $data['user']['name'] . ' on ' . date('Y-m-d h:i:s') . '.';
        $data['info'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/deleteProductsToAdmin', $data, true);

        // get admin email

        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($data['user']['email'],$data['user']['name']);
	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    public function sendDeleteProductsToClient($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 19;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = "Spamiles:" . $replymessage['title'];
        $data['info'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/deleteProductsToClient', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

    public function sendProductsStatusToClient($data)
    {

        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 20;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = "Spamiles:" . $replymessage['title'];
        $data['info'] = $data;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/productsStatusToClient', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['user']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }

///////////////////////////////Credits///////////////////////////////////
    public function sendCreditsToAdmin($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 21;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];
        $name = $data['user_credits']['first_name'] . ' ' . $data['user_credits']['last_name'];
        $subject = "New credits have been added to " . $name . " on " . date("F d,Y g:i a", strtotime($data['user_credits']['created_date']));

        /*	  	$data['user']=$data['user'];
		$data['user_credits']=$data['user_credits'];*/
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/sendCreditsToAdmin', $data, true);

        // get admin email

        /*	  $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($data['user_credits']['email'],$name);
	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();*/

        $send['message'] = $body;
        $send['subject'] = $subject;
        $this->fct->sendEmail($send);
    }

    public function sendCreditsToClient($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 21;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];
        $name = $data['user_credits']['first_name'] . ' ' . $data['user_credits']['last_name'];
        $subject = $replymessage['title'];
        /*	  	$data['user']=$data['user'];
		$data['user_credits']=$data['user_credits'];*/

        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/sendCreditsToClient', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($data['user_credits']['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
    }


///////////////////////////////Brand///////////////////////////////////


    public function sendBrandToClient($brand, $user)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 24;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        $subject = $replymessage['title'];

        $data['user'] = $user;
        $data['brand'] = $brand;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/sendBrandToClient', $data, true);

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($user['email']);
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();

    }


    public function inform_client_brand($brand, $user)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        if ($brand['status'] == 1) {
            $id_auto_reply_messages = 25;
        } else {
            if ($brand['status'] == 2) {
                $id_auto_reply_messages = 27;
            } else {
                $id_auto_reply_messages = 26;
            }
        }
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];


        $data['user'] = $user;
        $data['brand'] = $brand;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;
        $body = $this->load->view('emails/sendBrandToClient', $data, true);


        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($user['email']);
        $this->email->subject($replymessage['title' . getFieldLanguage()]);
        $this->email->message($body);
        $this->email->send();
    }


//////////////////////////////////////////////////////////////////
    public function informProductStatusToClient($user, $product)
    {
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];

        $id_auto_reply_messages = 16;
        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        if ($product['status'] == 0) {
            $replymessage['message'] = '<a href="' . route_to('products/details/' . $product['id_products']) . '">' . $product['title'] . '</a> product is published.';

            $subject = $product['title'] . " product is published.";
        } else {
            if ($product['status'] == 2) {
                $replymessage['message'] = $product['title'] . " product is under review.";
                $subject = $replymessage['message'];
            } else {
                if ($product['status'] == 1) {
                    $replymessage['message'] = $product['title'] . " product is unpublished.";
                    $subject = $replymessage['message'];
                }
            }
        }


        $data['product'] = $product;
        $data['user'] = $user;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;

        $body = $this->load->view('emails/inform_client_status_product', $data, true);


        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email

        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($adminemail, $adminname);
        $this->email->to($user['email']);
        $this->email->subject($subject);
        $this->email->message($body);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => $adminemail,
                'from_name' => $adminname,
                'to' => $user['email'],
                'subject' => $subject,
                'message' => $body,
            ]);
            return true;
        }

        $this->email->send();
    }


//////////////////////////////////////////////////////////////////
    public function informProductModificationsToAdmin($user, $product)
    {
        $brand = $this->fct->getonerecord('brands', array('id_brands' => $product['id_brands']));
        $config = Array(
            'mailtype' => 'html'
        );

        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);

        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];


        $id_auto_reply_messages = 15;

        $condition2 = array('id_auto_reply_messages' => $id_auto_reply_messages);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);
        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];

        /*$subject='New modifications at product "'.$product['title'].'"';*/
        if (isset($product['new']) && $product['new'] == 1) {
            $subject = 'New product "' . $product['title'] . '" is added By ' . $user['name'] . ' Brand ' . $brand['title'] . ' on ' . date("F d,Y g:i a", strtotime($brand['created_date']));
        } else {
            $subject = 'Modification applied to "' . $product['title'] . '" By ' . $user['name'] . ' Brand ' . $brand['title'] . ' on ' . date("F d,Y g:i a", strtotime($brand['created_date']));
        }
        $data['user'] = $user;
        $data['subject'] = $subject;
        $data['product'] = $product;
        $data['brand'] = $brand;
        $data['replymessage'] = $replymessage;
        $data['admindata'] = $admindata;

        $body = $this->load->view('emails/supplier_modifications', $data, true);
        // $body.='<p '.getDir($this->lang->lang()).'><strong>'.lang('thank_you').' <a href="'.base_url().'" target="_blank">'.$adminname.'</a></strong></p>';
        // get admin email


      $this->email->clear(TRUE);
	  $this->email->initialize($config);
	  $this->email->set_newline("\r\n");
	  $this->email->from($user['email'],$user['name']);
	  $this->email->to($adminemail);
	  $this->email->subject($subject);
	  $this->email->message($body);
	  $this->email->send();

       // $send['message'] = $body;
        //$send['subject'] = $subject;
        //$this->fct->sendEmail($send);
    }

////////////////////////////////////////////////////////////////////////////////////////////
    public function sendRequestQuoteToUser($data)
    {
        $config = Array(
            'mailtype' => 'html'
        );
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title' . getFieldLanguage()];


        $condition2 = array('id_auto_reply_messages' => 9);
        $replymessage = $this->fct->getonerecord('auto_reply_messages', $condition2);

        if (isset($replymessage['email']) && !empty($replymessage['email'])) $adminemail = $replymessage['email'];
        $message = '';

        // get reply message
        /*$body = '<p '.getDir().'>'.lang('dear').' '.ucfirst($data['name']).'</span>,</p>';*/
        $body = '';
        if ($data['msg_fr'] == "cart") {
            $body .= $this->getShoppingCart();
        }

        if ($data['msg_fr'] == "compare_products") {
            $body .= $this->getCompareProducts();
        }

        if (!empty($data['message'])) {
            $body .= '<p><strong>Message : </strong>' . $data['message'] . '</p>';
        }


        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");


        $this->email->from($adminemail, $adminname);

        $this->email->to($data['email']);
        $this->email->subject($replymessage['title' . getFieldLanguage()]);
        $this->email->message($body);
        $this->email->send();
    }

    /*public function getShoppingCart()
{
		$cart_items = $this->cart->contents();
		if(empty($cart_items))
		$cart_items = $this->ecommerce_model->loadUserCart();
		$products = $this->ecommerce_model->getCartProducts($cart_items);
$html="";
$attributes = array('method'=>'post','id'=>'update_cart_form');
$action = route_to('cart/update_quantity');


$html .='<h1 style="color:#b5121b;text-align:center;">Shopping Cart</h1>';


$html .='<table align="center;" style=" border-collapse: collapse;
   	  margin:0 auto ;
      width: 800px;">';
$html .='<tbody>';
$html .='<tr>';
$html .='<th style="background-color: #ab720c;
    color: #fff;
    font-size: 14px;
    font-weight: 400;
    padding: 15px 21px;
	border: 1px solid #dcdcdc;
    text-align: left;" >'.lang('cart_product_name').'</th>';
$html .='<th style="background-color: #ab720c;
    color: #fff;
    font-size: 14px;
    font-weight: 400;
    padding: 15px 21px;
	border: 1px solid #dcdcdc;
    text-align: left;">'.lang('cart_product_price').'</th>';
$html .='<th style="background-color: #ab720c;
    color: #fff;
    font-size: 14px;
    font-weight: 400;
    padding: 15px 21px;
    text-align: left;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">'.lang('cart_product_quantity').'</th>';
$html .='<th style="background-color: #ab720c;
    color: #fff;
    font-size: 14px;
    font-weight: 400;
    padding: 15px 21px;
	border: 1px solid #dcdcdc;
    text-align: left;">'.lang('cart_total_price').'</th>';
$html .='<th></th>';
$html .=' </tr>';

if(!empty($products)) {

$net_price = 0;
foreach($products as $pro) {

$html .='<input type="hidden" name="product_id[]" value="'.$pro['product_info']['id_products'].'" />';
$html .='<input type="hidden" name="product_rowid[]" value="'.$pro['rowid'].'" />';
$html .='<tr>';
$html .='<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
	if(!empty($pro['product_info']['gallery'])) {
$html .='<a href="'.route_to('products/details/'.$pro['product_info']['title_url'.getUrlFieldLanguage()]).'">';
$html .='<img align="top" width="50" src="'.base_url().'uploads/products/gallery/120x120/'.$pro['product_info']['gallery'][0]['image'].'" title="'.$pro['product_info']['title'.getFieldLanguage()].'" alt="'.$pro['product_info']['title'.getFieldLanguage()].'" />';
$html .='</a>';
$html .='<br />';
      }else{
$html .='<a href="'.route_to('products/details/'.$pro['product_info']['title_url'.getUrlFieldLanguage()]).'">';
$html .='<img align="top" width="50" src="'.base_url().'front/img/product/products_1.jpg" title="'.$pro['product_info']['title'.getFieldLanguage()].'" alt="'.$pro['product_info']['title'.getFieldLanguage()].'" />';
$html .='</a>';
$html .='<br />';

		  }

$html .='<a style="color: #ab710a;
    text-decoration: none;" href="'.route_to('products/details/'.$pro['product_info']['title_url'.getUrlFieldLanguage()]).'">'.$pro['product_info']['title'.getFieldLanguage()].'</a>';
 if(!empty($pro['cart_options'])) {
		$cart_options = $pro['cart_options'];

		$i=0;
		$c = count($cart_options);
		foreach($cart_options as $opt) {
			$i++;
			//echo $opt['attr_label'.getFieldLanguage()].': <span>'.$opt['title'.getFieldLanguage()].'</span>';
			echo '<span>'.$opt['title'].'</span>';
			if($i != $c) echo '-';
	 }}
$html .=' </td>';

$html .='<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">'.changeCurrency($pro['price']).'</td>';
$html .='<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
$html .=$pro["qty"];
$html .='</td>';
$html .=' <td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
	border: 1px solid #dcdcdc;
    padding: 15px 29px;
	font-family: Oxygen,sans-serif;">';

		    $total_price = $pro['price'] * $pro['qty'];
			$net_price = $net_price + $total_price;
$html .=changeCurrency($total_price);

$html .=' </td>';

$html .='</tr>';
 }
$html .='<tr>';
$html .='<td colspan="4">';
$html .=' <table border="0" cellpadding="0" cellspacing="0" style="padding-top:10px;">';
$html .=' <tr>';
$html .=' <td style="color: #ab710a;">';
$html .=' <span>'.lang('cart_net_price').':</span></td>';
$html .=' <td style="padding-left:10px;">'.changeCurrency($net_price).'</td>';
$html .=' </tr>';
$html .=' </table>';
$html .='</td>';
$html .='</tr>';
 } else {
$html .='<tr>';
$html .='<td colspan="5" align="center">';
 echo lang('empty_cart');
$html .='</td>';
$html .='</tr>';
 }
$html .=' </tbody>';
$html .='</table>';

return $html;
}
*/
    public function getCompareProducts()
    {

        $id_user = $this->session->userdata('login_id');
        $session_id = $this->session->userdata('session_id');

        $user_compare_products = $this->ecommerce_model->getUserCompareProducts($id_user, $session_id);
        $html = "";
        $html = '<h1 style="text-align:center; color:#b5121b">Compare Products</h1>';
        $html .= '<table border="0" cellpadding="0" cellspacing="0" style="background: #efefef none repeat scroll 0 0;
    float: left;
    padding: 1%;
    width: 98%;">';
        if (!empty($user_compare_products)) {
            $html .= '<tr>';
            $th_width = 100 / count($user_compare_products);
            foreach ($user_compare_products as $pro) {
                $html .= '<th  style="   background: #ab720c none repeat scroll 0 0;
    color: #fff;
    width:' . $th_width . '% ;
    border:none;
    font-size: 14px;
    font-weight: 600;
    padding: 16px 19px;
	border-right:1px solid #efefef;
    text-transform: uppercase;
    font-family: Oxygen,sans-serif;" >
	<a style="color:white;text-decoration:none;" href="' . route_to('products/details/' . $pro['title_url' . getUrlFieldLanguage()]) . '" target="_blank">' . $pro['title' . getFieldLanguage()] . '</a></th>';
            }
            $html .= '</tr>';
            $html .= '<tr>';

            foreach ($user_compare_products as $pro) {

                $html .= '<td style="background: #fff none repeat scroll 0 0;
    text-align:center;
    border:none;
    border-right:1px solid #efefef;
    " ><div style="
    text-align: center;
    vertical-align: middle;">';

                if (!empty($pro['gallery'])) {
                    $html .= '<a  href="' . route_to('products/details/' . $pro['title_url'] . getUrlFieldLanguage()) . '">';
                    $html .= '<img align="top" src="' . base_url() . 'uploads/products/gallery/230x186/' . $pro['gallery'][0]['image'] . '" title="' . $pro['title' . getFieldLanguage()] . '" alt="' . $pro['title' . getFieldLanguage()] . '"/>';
                    $html .= '</a>';
                } else {
                    $html .= '<a  href="' . route_to('products/details/' . $pro['title_url'] . getUrlFieldLanguage()) . '">';
                    $html .= '<img align="top" style="max-width:220px;max-height:220px;" src="' . base_url() . 'front/img/product/products_1.jpg" alt="' . $pro['title' . getFieldLanguage()] . '"/>';
                    $html .= '</a>';
                }
                $html .= '</div></td>';
            }
            $html .= '</tr>';
            if ($this->session->userdata('login_id') != '') {
                $html .= '<tr>';

                foreach ($user_compare_products as $pro) {
                    $html .= '<td style=" color: #707070;
    font-size: 13px;
    font-weight: 400;
    border:none;
    border-right:1px solid #efefef;
    padding: 16px 19px; " >';
                    if (isset($pro['stock']) && !empty($pro['stock'])) {
                        $stock = $pro['stock'];
                        $html .= 'span style="color:#ff8500">Prices:</span> <br /><br />';

                        foreach ($stock as $stck) {
                            $list_price = $stck['list_price'];
                            $price = $stck['price'];
                            $discount = $stck['discount'];
                            $discount_expiration = $stck['discount_expiration'];
                            $hide_price = $stck['hide_price'];

                            if ($hide_price == 0) {
                                $stock_name = unSerializeStock($stck['combination']);
                                if (!empty($stock_name)) {
                                    foreach ($stock_name as $val) echo '<strong>' . $val . ':</strong> ';
                                }
                                if (displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price, $discount_expiration, $price)) {
                                    $html .= ' Was ';
                                    changeCurrency(displayWasCustomerPrice($list_price));
                                    $html .= ' New';
                                    echo changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price));
                                } else {
                                    echo changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price));
                                }
                            }


                        }
                    } else {
                        $list_price = $pro['list_price'];
                        $price = $pro['price'];
                        $discount = $pro['discount'];
                        $discount_expiration = $pro['discount_expiration'];
                        $hide_price = $pro['hide_price'];
                        if ($hide_price == 0) {
                            $html .= 'Price: ';

                            if (displayWasCustomerPrice($list_price) != displayCustomerPrice($list_price, $discount_expiration, $price)) {
                                $html .= 'was';

                                $html .= changeCurrency(displayWasCustomerPrice($list_price));
                                $html .= changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price));
                            } else {
                                $html .= changeCurrency(displayCustomerPrice($list_price, $discount_expiration, $price));
                            }
                        } else {

                        }
                        $html .= '<br />';
                    }
                    $html .= '</td>';
                }
                $html .= '</tr>';
            } else {
                $html .= '<tr><td colspan="4"> <a color: #616161; href="' . route_to('user/login') . '">To view price:' . lang('login_register') . '</a></td></tr>';
            }
            $html .= '<tr>';
            foreach ($user_compare_products as $pro) {
                $html .= ' <td style="   background: #fff none repeat scroll 0 0;
    color: #707070;
    font-size: 13px;
    font-weight: 400;
    border:none;
     border-right:1px solid #efefef;
    padding: 16px 19px;
     font-family: Open Sans;" >';
                $html .= '<span style="display: block; padding: 8px 0;">Description:</span>';
                $html .= '<span 
    style="max-height: 110px;
    overflow: hidden;
    padding: 8px 0;
    display: block;"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a dui id justo tempus tristique sed at mauris. Morbi diam leo, fringilla vitae eleifend ut, pretium a enim. </span>';

                $html .= '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';
        /*echo $html;exit;*/
        return $html;
    }

    public function getShoppingCart()
    {
        $condition1 = array('id_settings' => 1);
        $admindata = $this->fct->getonerow('settings', $condition1);
        $user = $this->ecommerce_model->getUserInfo($this->session->userdata('login_id'));
        $adminProfile = $this->ecommerce_model->getUserInfo(1);

        $cart_items = $this->cart->contents();
        if (empty($cart_items))
            $cart_items = $this->ecommerce_model->loadUserCart();
        $products = $this->ecommerce_model->getCartProducts($cart_items);
        $html = "";
        $attributes = array('method' => 'post', 'id' => 'update_cart_form');
        $action = route_to('cart/update_quantity');


        $html .= '<table style="width:800px;margin:0 auto;">';
        $html .= '<tr style=" float: left;
    padding: 20px 0;
    width: 100%;">';
        $html .= '<td>';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<img src="' . base_url() . 'front/img/logo.png">';
        $html .= '</td>';
        $html .= '<td style="padding-left: 70px">';
        $html .= '<span style="  color: #616161;
    display: block;
     font-family: Oxygen,sans-serif;
    font-size: 24px;
    font-weight: 400;
    padding-bottom: 15px;">AL MAKAAN TRADING JLT</span>';
        $html .= '<span style="color: #616161;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;">Tel: ' . $admindata['phone'] . '</span>';
        if (!empty($admindata['fax'])) {
            $html .= '<span style="    color: #616161;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;
	 display: inline-block;
    padding-left: 40px;">Fax: ' . $admindata['fax'] . '</span><br>';
        }
        $html .= '<span style="color: #616161;
	margin-top:0;
	float:left;
	width:100%;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;">' . $admindata['address'] . '</span><br>';
        $html .= '<span style="color: #616161;
margin-top:0;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;">E-mail: <a href="mailto:' . $admindata['email'] . '" >' . $admindata['email'] . '</a></span>';
        $html .= '<span style="    color: #616161;
margin-top:0;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;
	 display: inline-block;
    padding-left: 40px;">Web: <a target="_blank" href="' . $admindata['website'] . '" >' . $admindata['website'] . '</a></span><br>';

        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table width="100%">';
        $html .= '<h2  style="border-bottom: 1px solid #dcdcdc;
    border-top: 1px solid #dcdcdc;
    color: #b5121b;
    font-family: Lato,sans-serif;
    font-size: 20px;
    font-weight: 400;
    padding: 10px 0;
	
    text-align: center;">Sales Quotation</h2>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td style="color: #353535;
    font-family: "Oxygen",sans-serif;
    font-size: 14px;
    font-weight: 400;
    padding-top: 27px;
	padding-bottom:15px;">';
        $html .= 'Date';
        $html .= '<span style="color: #616161;
    display: inline-block;
    font-weight: 300;
    padding-left: 26px;">' . date("Y-m-d") . '</span>';
        $html .= '</td>';
        $html .= '<tr style="  color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;">';
        $html .= '<td style="padding-bottom:20px;">';
        $html .= '<div  style="float: left;"> Customer </div>';
        $html .= '<div  style="float: left;"> 
<span style="
display: block;
color: #616161;
display: inline-block;
font-weight: 300;
padding-left: 26px;">' . $user['company_name'] . '</span>
<br><span style="
display: block;
color: #616161;
display: inline-block;
font-weight: 300;
padding-left: 26px;">Tel: ' . $user['phone'] . '</span>
<br>';
        if (!empty($user['fax'])) {
            $html .= '<span style="
display: block;
color: #616161;
display: inline-block;
font-weight: 300;
padding-left: 26px;">Fax: ' . $user['fax'] . '</span>';
        }
        $html .= '</div>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table align="center;" style=" border-collapse: collapse;
   	  margin:0 auto ;
      width: 800px;">';
        $html .= '<tbody>';
        $html .= '<tr>';
        $html .= '<th style=" color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;
    padding: 10px 30px;
	 border: 1px solid #dcdcdc;
    text-align: left;" >Code No</th>';
        $html .= '<th style=" color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;
	width: 50%;
    padding: 10px 30px;
	 border: 1px solid #dcdcdc;
    text-align: left;">Item Name</th>';
        $html .= '<th style="color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;
    padding: 10px 30px;
	 border: 1px solid #dcdcdc;
    text-align: left;">Unit</th>';
        $html .= '<th style=" color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;
    padding: 10px 30px;
	 border: 1px solid #dcdcdc;
    text-align: left;
    ;">UPC</th>';


        $html .= '<th style=" color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;
    padding: 10px 30px;
	 border: 1px solid #dcdcdc;
    text-align: left;
    ;">Order Qty</th>';


        $html .= '<th style=" color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;
    padding: 10px 30px;
    text-align: left;
	 border: 1px solid #dcdcdc;
    ;">Order Ctn</th>';


        $html .= '<th style=" color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 400;
    padding: 10px 30px;
	 border: 1px solid #dcdcdc;
    text-align: left;
    ;">Unit Price</th>';


        $html .= '<th style=" color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
	 border: 1px solid #dcdcdc;
    font-weight: 400;
    padding: 10px 30px;
    text-align: left;
    ;">Value</th>';


        $html .= '<th></th>';
        $html .= ' </tr>';

        if (!empty($products)) {

            $net_price = 0;
            $net_discount_price = 0;
            foreach ($products as $pro) {
                /*	echo "<pre>";
	print_r($pro);exit;*/

                $html .= '<input type="hidden" name="product_id[]" value="' . $pro['product_info']['id_products'] . '" />';
                $html .= '<input type="hidden" name="product_rowid[]" value="' . $pro['rowid'] . '" />';
                $html .= '<tr>';
                $html .= '<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
                $html .= $pro['product_info']['id_products'];
                $html .= ' </td>';


                $html .= '<td  style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	width: 50%;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;" >';
                $html .= '<a  style="text-decoration: none; color: #616161;" href="' . route_to('products/details/' . $pro['product_info']['title_url' . getUrlFieldLanguage()]) . '">' . $pro['product_info']['title' . getFieldLanguage()] . '</a></td>';

                $html .= '<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	width: 50%;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
                $html .= $this->session->userdata('currency');
                $html .= '</td>';

                $html .= '<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	width: 50%;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
                $html .= $pro['product_info']["quantity"];
                $html .= '</td>';


                $html .= '<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	width: 50%;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
                $html .= $pro['qty'];
                $html .= '</td>';

                $html .= '<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	width: 50%;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
                $html .= '0.04';
                $html .= '</td>';

                $html .= '<td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	width: 50%;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';
                /*$html .=$pro['price'];*/


                $segmentPrice = $this->ecommerce_model->checkPriceSegment($pro['id'], '', $pro['qty']);

                $customerPrice = displayCustomerPrice($pro['product_info']['list_price'], $pro['product_info']['discount_expiration'], $pro['product_info']['price']);

                $productDiscountPrice = displayProductDiscountPrice($pro['product_info']['discount'], $pro['product_info']['discount_expiration'], $pro['product_info']['price']);

                $new_price = min($segmentPrice, $customerPrice, $productDiscountPrice);

                if ($new_price < $pro['product_info']['list_price']) {
                    $html .= '<div style="text-decoration:line-through;">';
                    $html .= changeCurrency($pro["product_info"]["list_price"]);
                    $html .= '</div>';
                }


                $html .= changeCurrency($new_price);

                $html .= '</td>';

                $html .= ' <td style="background-color: #ffffff;
    color: #616161;
    font-size: 14px;
    font-weight: 300;
    padding: 15px 29px;
	width: 50%;
	border: 1px solid #dcdcdc;
	font-family: Oxygen,sans-serif;">';

                $total_price = $pro['product_info']['list_price'] * $pro['qty'];
                $total_discount_price = $new_price * $pro['qty'];
                $net_price = $net_price + $total_price;
                $net_discount_price = $net_discount_price + $total_discount_price;

                if ($total_discount_price < $total_price) {
                    $html .= '<div style="text-decoration:line-through;">';
                    $html .= changeCurrency($total_price);
                    $html .= '</div>';
                }
                $html .= changeCurrency($total_discount_price);

                $html .= ' </td>';

                $html .= '</tr>';
                $html .= '<tr>';
                $html .= '<td colspan="8" style="padding:20px 0;text-align:center;">';
                if (!empty($pro['product_info']['gallery'])) {
                    $html .= '<a href="' . route_to('products/details/' . $pro['product_info']['title_url' . getUrlFieldLanguage()]) . '">';
                    $html .= '<img align="top"  src="' . base_url() . 'uploads/products/gallery/230x186/' . $pro['product_info']['gallery'][0]['image'] . '" title="' . $pro['product_info']['title' . getFieldLanguage()] . '" alt="' . $pro['product_info']['title' . getFieldLanguage()] . '" />';
                    $html .= '</a>';
                    $html .= '<br />';
                } else {
                    $html .= '<a href="' . route_to('products/details/' . $pro['product_info']['title_url' . getUrlFieldLanguage()]) . '">';
                    $html .= '<img align="top" width="230px;"  src="' . base_url() . 'front/img/product/products_1.jpg" title="' . $pro['product_info']['title' . getFieldLanguage()] . '" alt="' . $pro['product_info']['title' . getFieldLanguage()] . '" />';
                    $html .= '</a>';
                    $html .= '<br />';

                }
                $html .= '</td>';
                $html .= '</tr>';
            }
            $html .= '<tr>';
            $html .= '<td colspan="8">';
            $html .= '<table style=" border-collapse: collapse;
    display: block;
    float: right;
    margin-top: 20px;
    overflow: auto;
    width: auto;">';
            $html .= '<tbody>';
            $html .= '<tr>';
            $html .= '<td style=" color: #616161;
    font-family: Oxygen,sans-serif;
	border: 1px solid #dcdcdc;
    font-size: 14px;
    font-weight: 300;
    padding: 10px 30px;
">Total</td>';
            $html .= '<td style=" color: #616161;
    font-family: Oxygen,sans-serif;
	border: 1px solid #dcdcdc;
    font-size: 14px;
    font-weight: 300;
    padding: 10px 30px;
">';
            $html .= changeCurrency($net_price);
            $html .= '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td style=" color: #616161;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
	border: 1px solid #dcdcdc;
    font-weight: 300;
    padding: 10px 30px;
">Discount</td>';
            $html .= '<td style=" color: #616161;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
	border: 1px solid #dcdcdc;
    font-weight: 300;
    padding: 10px 30px;
" >';
            $discount = changeCurrency($net_price) - changeCurrency($net_discount_price);
            $html .= changeCurrency($discount);
            $html .= '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td style=" color: #616161;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;
    padding: 10px 30px;
	border: 1px solid #dcdcdc;
" >Net</td>';
            $html .= '<td style=" color: #616161;
    font-family: Oxygen,sans-serif;
	border: 1px solid #dcdcdc;
    font-size: 14px;
    font-weight: 300;
    padding: 10px 30px;
" >';
            $html .= changeCurrency($net_discount_price);
            $html .= '</td>';
            $html .= '</tr>';
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td colspan="8">';
            $html .= ' <table border="0" cellpadding="0" cellspacing="0" style=" color: #616161;
    float: left;
    font-family: "Oxygen",sans-serif;
    font-size: 14px;
    font-weight: 300;
    padding-top: 20px;
    width: 100%;">';
            $html .= ' <tr>';
            $html .= ' <td style="padding-bottom: 5px;">Delivery: ( 0 ) days from confirmation</td>';
            $html .= ' </tr>';
            $html .= ' <tr>';
            $html .= ' <td style="padding-bottom: 5px;">Validity: ( 0 ) days</td>';
            $html .= ' </tr>';
            $html .= ' <tr>';
            $html .= ' <td style="padding-bottom: 5px;">Payment: According to Agreement</td>';
            $html .= ' </tr>';
            $html .= ' </table>';
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td colspan="8">';
            $html .= ' <table border="0" cellpadding="0" cellspacing="0" style=" padding-top:25px; color: #353535;
    float: left;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;
    padding-top: 44px;
    width: 100%;">';
            $html .= ' <tr>';

            $html .= ' <td style="color: #353535;
    font-family: Oxygen,sans-serif;
    font-size: 14px;
    font-weight: 300;">With Sincere Thanks & Best Regards,
<br>';
            $html .= $adminProfile['name'];
            $html .= '</td>';
            $html .= ' </tr>';
            $html .= ' </table>';
            $html .= '</td>';
            $html .= '</tr>';
        } else {
            $html .= '<tr>';
            $html .= '<td colspan="8" align="center">';
            echo lang('empty_cart');
            $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= ' </tbody>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= "</table>";
        /*echo $html;exit;*/
        return $html;
    }


}