<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Notifications_m extends CI_Model{

public function get_notifications($id){
$this->db->where('id_notifications',$id);
$this->db->where('deleted',0);
$query = $this->db->get('notifications');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  notifications.* ";
        $q .= " from  notifications ";
		$q .= " where notifications.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(notifications.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}