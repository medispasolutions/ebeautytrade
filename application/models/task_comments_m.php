<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Task_comments_m extends CI_Model{

public function get_task_comments($id){
$this->db->where('id_task_comments',$id);
$this->db->where('deleted',0);
$query = $this->db->get('task_comments');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  task_comments.* ";
        $q .= " from  task_comments ";
		$q .= " where task_comments.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(task_comments.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}