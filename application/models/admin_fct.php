<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Admin_fct extends CI_Model
{
	    ///////////////////////////////////////////////////////////////////////////////////////
    public function getDeliveryOrders($cond = array(), $limit = '', $offset = '')
    {
 $cond['delivery_order.deleted']=0;
 if(isset($cond['join'])){
	 $left='';
	 }else{
		 $left="left";}
 $sqla =" select delivery_order.*,user.name, user.trading_name, user.email from delivery_order ";
  $sqla .= $left." join delivery_order_line_items on delivery_order.id_delivery_order = delivery_order_line_items.id_orders ";
 $sqla .= $left." join user on delivery_order.id_user = user.id_user where delivery_order.deleted=0   ";
        

if (isset($cond['name'])&& !empty($cond['name'])) {
$sqla .= " and UPPER(user.name) like '%".strtoupper($cond['name'])."%' ";
}

if (isset($cond['trading_name'])&& !empty($cond['trading_name'])) {
$sqla .= " and UPPER(user.trading_name) like '%".strtoupper($cond['trading_name'])."%' ";
}

if (isset($cond['order_id_keyword'])&& !empty($cond['order_id_keyword'])) {
$sqla .= " and UPPER(delivery_order.id_delivery_order) like '%".strtoupper($cond['order_id_keyword'])."%' ";
}
if(isset($cond['from_date']) && !empty($cond['from_date'])) {
 $sqla .= " and delivery_order.created_date>='".$cond['from_date']."'";
}

if(isset($cond['id_role']) && !empty($cond['id_role'])) {
 $sqla .= " and delivery_order.id_role ='".$cond['id_role']."'";
}

if(isset($cond['id_user']) && !empty($cond['id_user'])) {
 $sqla .= " and delivery_order.id_user ='".$cond['id_user']."'";
 
}

if(isset($cond['to_date']) && !empty($cond['to_date'])) {
 $sqla .= " and delivery_order.created_date<='".$cond['to_date']."'";
}

if(isset($cond['status']) && !empty($cond['status'])) {
		
 $sqla .= " and delivery_order.status='".$cond['status']."'";
}


if(isset($cond['flag']) && $cond['flag']==3) {
	 $sqla .= " and delivery_order_line_items.flag=0 ";
	}

if(isset($cond['type']) && !empty($cond['type'])) {
 $sqla .= " and delivery_order.type='".$cond['type']."'";

}

if(isset($cond['id_brands']) && !empty($cond['id_brands'])) {
 $sqla .= " and delivery_order_line_items.id_brands='".$cond['id_brands']."'";

}

if(isset($cond['id_role_1']) && $cond['id_role_1']) {
 $sqla .= " and delivery_order.id_role !=5";
}
 if(isset($cond['section']) && $cond['section']=="invoices" ) {
$sqla .= " and invoice=1";}

if(isset($cond['section']) && $cond['section']!="invoices") {
if(isset($cond['export']) && $cond['export']==1) {
$sqla .= " and delivery_order.export =".$cond['export'];

$sqla .= " and delivery_order.invoice !=1";
}else{
	if(isset($cond['section'])){
$sqla .= " and delivery_order.id_old_delivery_order ='' ";	}
	}}
	
	if(isset($cond['exported']) && $cond['exported']==1) {
	$sqla .= " and delivery_order.id_delivery_order IN (select id_old_delivery_order from delivery_order where deleted=0 and id_old_delivery_order!='')";	
		}
		
	if(isset($cond['exported']) && $cond['exported']==2) {
			$sqla .= " and delivery_order.id_delivery_order NOT IN (select id_old_delivery_order from delivery_order where deleted=0 and id_old_delivery_order!='')";	}	

if(isset($cond['available_orders']) && $cond['available_orders']==1) {
 $sqla .=" and delivery_order.status=1  and delivery_order.id_delivery_order NOT IN (select id_old_delivery_order from delivery_order where deleted=0)";	
	}
	
/*if(isset($cond['available_orders']) && $cond['available_orders']==1) {
 $sqla .=" and delivery_order.status=1 and delivery_order.id_old_delivery_order !='' and delivery_order.id_delivery_order NOT IN (select id_old_delivery_order from delivery_order where deleted=0)";	
	}	*/
	
if(isset($cond['order_type']) && !empty($cond['order_type'])) {
if($cond['order_type']==2) {
$sqla .= " and delivery_order.invoice =1";
}

if($cond['order_type']==1) {
$sqla .= " and delivery_order.type =1";
$sqla .= " and delivery_order.invoice =0";
}

}

if(isset($cond['return']) && $cond['return']) {
 $sqla .= " and delivery_order.return =".$cond['return'];
}else{
 $sqla .= " and delivery_order.return =0";	}

if(isset($cond['delivery_order']) && !empty($cond['delivery_order'])) {
 $sqla .= " and( delivery_order.id_delivery_order='".$cond['delivery_order']."' || delivery_order.id_invoice='".$cond['delivery_order']."')";
}

 $sqla.=" group by delivery_order.id_delivery_order   ";
 $sqla .= " ORDER BY delivery_order.id_delivery_order DESC";
 if($limit != "") {
$sqla .= " LIMIT ".$limit." OFFSET ".$offset;

}


$query = $this->db->query($sqla);
if($limit != "") {

$data = $query->result_array();

$results=array();
$i=0;
foreach($data as $res){
	$exported_order=$this->fct->getonerecord('delivery_order',array('id_old_delivery_order'=>$res['id_delivery_order']));
		
$results[$i]=$res;
$results[$i]['exported_order']=$exported_order;
$i++;	}

}else {
$results= $query->num_rows();
}
return $results;
    }
	
	public function getProductsTotal($products){
		$total_qty=0;
		$total_virtual_qty=0;
		$total_price=0;
		if(!empty($products)){
	foreach($products as $product){
		
		
		if(empty($product['products_stock'])){
		$total_qty=$total_qty+$product['quantity'];
		$total_price=$total_price+$product['price'];
		$total_virtual_qty=$total_virtual_qty+$product['stock_status_quantity_threshold'];
		}else{	
		///////////IF WE HAVE OPTIONS//////////////
		$products_stock=$product['products_stock'];
		foreach($products_stock as $product_stock){
		$total_qty=$total_qty+$product_stock['quantity'];
		$total_price=$total_price+$product_stock['price'];
		$total_virtual_qty=$total_virtual_qty+$product['stock_status_quantity_threshold'];
		}
		/////////////////////////////////////////	
		}}}
		
		$return['total_qty']=$total_qty;
		$return['total_virtual_qty']=$total_virtual_qty;
		$return['total_price']=$total_price;
		
		return $return;
		}
	
/*public function getProductsWithOptions($cond){
	 
/////////////PRODUCTS///////////
$q="SELECT 'product' as type, products.id_products,products.stock_status_quantity_threshold as stock_status_quantity_threshold, products.created_date as created_date FROM products where products.deleted=0 ";
$q.=" group by products.id_products ";
/////////////PRODUCTS STOCK///////////
$q.=" UNION (SELECT 'stock' as type, products_stock.id_products_stock as id_products,products.stock_status_quantity_threshold as stock_status_quantity_threshold , products_stock.created_date as created_date FROM  products_stock join products on products.id_products=products_stock.id_products  ";
$q.="where  products.deleted=0 and products_stock.deleted=0 and products_stock.status=1  ";

if(isset($cond['id_user']) && !empty($cond['id_user'])) {
 $q .= " and products.id_user=".$cond['id_user'];
}

 $q.=" group by products_stock.id_products_stock )  ";
 $q .= " ORDER BY created_date DESC";

//echo $sqla;exit;
$query = $this->db->query($q);
if($limit != "") {
$results = $query->result_array();
}else {
$results =$query->num_rows();
}

return $results;


 }*/

	
	
		    ///////////////////////////////////////////////////////////////////////////////////////
   public function getGroupProductsOrders($cond = array(), $limit = '', $offset = '')
    {
 $cond['group_products.deleted']=0;
 if(isset($cond['join'])){
	 $left='';
	 }else{
		 $left="left";}
 $sqla =" select products.*, products.id_products as id_orders from products ";
  $sqla .= $left." join group_products_line_items on products.id_products = group_products_line_items.id_orders ";

 $sqla .=' where products.deleted=0';       

if (isset($cond['name'])&& !empty($cond['name'])) {
$sqla .= " and UPPER(user.name) like '%".strtoupper($cond['name'])."%' ";
}

if (isset($cond['trading_name'])&& !empty($cond['trading_name'])) {
$sqla .= " and UPPER(user.trading_name) like '%".strtoupper($cond['trading_name'])."%' ";
}

if (isset($cond['order_id_keyword'])&& !empty($cond['order_id_keyword'])) {
$sqla .= " and UPPER(group_products.id_group_products) like '%".strtoupper($cond['order_id_keyword'])."%' ";
}
if(isset($cond['from_date']) && !empty($cond['from_date'])) {
 $sqla .= " and group_products.created_date>='".$cond['from_date']."'";
}

if(isset($cond['id_role']) && !empty($cond['id_role'])) {
 $sqla .= " and group_products.id_role ='".$cond['id_role']."'";
}

if(isset($cond['id_user']) && !empty($cond['id_user'])) {
 $sqla .= " and group_products.id_user ='".$cond['id_user']."'";
 
}

if(isset($cond['to_date']) && !empty($cond['to_date'])) {
 $sqla .= " and group_products.created_date<='".$cond['to_date']."'";
}

if(isset($cond['status']) && !empty($cond['status'])) {
$sqla .= " and group_products.status='".$cond['status']."'";
}

if(isset($cond['type']) && !empty($cond['type'])) {
 $sqla .= " and group_products.type='".$cond['type']."'";}

if(isset($cond['id_brands']) && !empty($cond['id_brands'])) {
 $sqla .= " and group_products_line_items.id_brands='".$cond['id_brands']."'";

}

if(isset($cond['id_role_1']) && $cond['id_role_1']) {
 $sqla .= " and group_products.id_role !=5";
}

if(isset($cond['exported']) && $cond['exported']==2) {
$sqla .= " and group_products.id_group_products NOT IN (select id_old_group_products from group_products where deleted=0 and id_old_group_products!='')";	}	

if(isset($cond['available_group_products']) && $cond['available_group_products']==1) {
 $sqla .=" and group_products.status=1 and group_products.id_old_group_products !='' and group_products.id_group_products NOT IN (select id_old_group_products from group_products where deleted=0)";	
	}
	
if(isset($cond['order_type']) && !empty($cond['order_type'])) {
if($cond['order_type']==2) {
$sqla .= " and group_products.invoice =1";
}

if($cond['order_type']==1) {
$sqla .= " and group_products.type =1";
$sqla .= " and group_products.invoice =0";
}}


if(isset($cond['group_products']) && !empty($cond['group_products'])) {
 $sqla .= " and group_products.id_group_products='".$cond['group_products']."'";
}

$sqla.=" group by products.id_products  ";
$sqla .= " ORDER BY products.barcode DESC";
if($limit != "") {
$sqla .= " LIMIT ".$limit." OFFSET ".$offset;}


$query = $this->db->query($sqla);
if($limit != "") {
$data = $query->result_array();
$results=array();
$i=0;
foreach($data as $res){
$exported_order=$this->fct->getonerecord('group_products',array('id_old_group_products'=>$res['id_group_products']));	
$results[$i]=$res;
$results[$i]['exported_order']=$exported_order;
$i++;	}

}else {
$results= $query->num_rows();
}
return $results;
    }
	///////////////////////////////////////////////////////////////////////////////////////
  public function createNewRecord($table)
{
	$insert = array();
	$insert['created_date'] = date("Y-m-d H:i:s");
	$insert['status']=5;
	$this->db->insert($table,$insert);
    $last_id = $this->db->insert_id();

    return $last_id;
}
	
		    ///////////////////////////////////////////////////////////////////////////////////////
    public function generateInvocie($data)
    {			   ////////CREATE INVOICE/////////
				    $order['created_date'] = date('Y-m-d h:i:s');
					$order['rand'] = rand();
					$order['currency'] = $data['currency'];
					$order['default_currency'] = $data['currency'];
					$order['total_amount'] = $data['total_amount'];
					$order['total_amount_credits'] = $data['total_amount_credits'];
					$order['total_customer_charge'] = $data['total_customer_charge'];
					$order['net_price'] = $data['net_price'];
					
					$order['from_date'] = $data['from_date'];
					$order['to_date'] = $data['to_date'];
					$order['id_user'] = $data['id_user'];
					$order['status'] = 'pending';
					
					$this->db->insert('invoices',$order);
					$order_id = $this->db->insert_id();
					
					 ////////LINE ITEMS/////////
					 $i=0;
					 $brands=$data['brands'];
					 foreach($brands as $brand){
					$i++; 
					$items=$brand['line_items'];
					
					foreach($items as $val){
	
						$line_item['id_orders']=$val['id_orders'];
						
						$line_item['id_brands']=$brand['id_brands'];
						$line_item['orders_created_date']=$val['created_date'];
						$line_item['id_products']=$val["product"]['id_products'];
						$line_item['sku']=$val["product"]['sku'];
						if(isset($val["product"]['combination'])){
						$line_item['options']=$val["product"]['combination'];}
						$line_item['quantity']=$val['quantity'];
						$line_item['return_quantity']=$val['return_quantity'];
						$line_item['qty_sold_by_admin']=$val['qty_sold_by_admin'];
						$line_item['qty_payable']=$val['qty_payable'];
						$line_item['credit_cart']=$val['credits_val'];
						$line_item['total_price']=$val['total_price'];
						$line_item['id_line_items']=$val['id_line_items'];
						$line_item['id_invoices']=$order_id;
					   $this->db->insert('invoices_line_items',$line_item);
				
						$update_line_item['invoice']=1;
						$update_line_item['updated_date']=date('Y-m-d h:i:s');
						
						$this->db->where(array('id_line_items'=>$val['id_line_items']));
						$this->db->update('line_items',$update_line_item);
						}	 
					 }
					 
					return $order_id; 
		
		}
	
		    ///////////////////////////////////////////////////////////////////////////////////////
    public function getStockReturnOrders($cond = array(), $limit = '', $offset = '')
    {
 $cond['delivery_order.deleted']=0;
 $sqla =" select stock_return.*,user.name,user.trading_name , user.email from stock_return";
 $sqla .=" left join user on stock_return.id_user = user.id_user where stock_return.deleted=0    ";
        

		
if (isset($cond['name'])&& !empty($cond['name'])) {
$sqla .= " and UPPER(user.name) like '%".strtoupper($cond['name'])."%' ";
}

if (isset($cond['trading_name'])&& !empty($cond['trading_name'])) {

$sqla .= " and UPPER(user.trading_name) like '%".strtoupper($cond['trading_name'])."%' ";
}


if (isset($cond['order_id_keyword'])&& !empty($cond['order_id_keyword'])) {
$sqla .= " and UPPER(stock_return.id_stock_return) like '%".strtoupper($cond['order_id_keyword'])."%' ";
}
if(isset($cond['from_date']) && !empty($cond['from_date'])) {
 $sqla .= " and stock_return.created_date>='".$cond['from_date']."'";
}

if(isset($cond['id_role']) && !empty($cond['id_role'])) {
 $sqla .= " and stock_return.id_role ='".$cond['id_role']."'";
}

if(isset($cond['id_user']) && !empty($cond['id_user'])) {
 $sqla .= " and stock_return.id_user ='".$cond['id_user']."'";
 
}

if(isset($cond['to_date']) && !empty($cond['to_date'])) {
 $sqla .= " and stock_return.created_date<='".$cond['to_date']."'";
}

if(isset($cond['status']) && !empty($cond['status'])) {
		
 $sqla .= " and stock_return.status='".$cond['status']."'";
}

if(isset($cond['type']) && !empty($cond['type'])) {
 $sqla .= " and stock_return.type='".$cond['type']."'";
}

if(isset($cond['id_role_1']) && $cond['id_role_1'] && !isset($cond['id_role'])) {
 $sqla .= " and stock_return.id_role !=5";
}


if(isset($cond['stock_return']) && !empty($cond['stock_return'])) {
 $sqla .= " and stock_return.id_stock_return='".$cond['stock_return']."'";
}

 $sqla.=" group by stock_return.id_stock_return   ";
 $sqla .= " ORDER BY stock_return.id_stock_return DESC";
 if($limit != "") {
	
  $sqla .= " LIMIT ".$limit." OFFSET ".$offset;
  
 }

//echo $sqla;exit;
$query = $this->db->query($sqla);
if($limit != "") {
$results = $query->result_array();

}else {
$results= $query->num_rows();
}
return $results;
    }
///////////////////////////////////////////////////////////////////////////////////////
    public function checkThreshold($product)
    {

		
		$combination="";
		$qty=$product['quantity'];
		$threshold=$product['threshold'];
		
if($qty>$threshold){
	$update['threshold_flag']=0;
	}else{
	$update['threshold_flag']=1;
	}
	
	$update['updated_date']=date("Y-m-d h:i:s");
	
		if(isset($product['combination'])){
		$this->db->where('id_products_stock',$product['id_products_stock']);
		$this->db->update('products_stock',$update);
			}else{
		$this->db->where('id_products',$product['id_products']);
		$this->db->update('products',$update);	
		
				}
				return true;
				
		}	
		    ///////////////////////////////////////////////////////////////////////////////////////
    public function getStoreKeeper($cond = array())
    {

 $sqla =" select delivery_order_line_items.* from delivery_order_line_items";
 $sqla .=" join delivery_order on delivery_order.id_delivery_order = delivery_order_line_items.id_orders ";
 /*$sqla .=" join store_keeper on delivery_order_line_items.id_delivery_order_line_items = store_keeper.id_line_items_return ";*/
$sqla .=" where delivery_order.deleted=0 and delivery_order_line_items.deleted=0   ";  

if(isset($cond['id_role']) && !empty($cond['id_role'])) {
 $sqla .= " and delivery_order.id_role ='".$cond['id_role']."'";
}

if(isset($cond['id_user']) && !empty($cond['id_user'])) {
 $sqla .= " and delivery_order.id_user ='".$cond['id_user']."'";
 }

if(isset($cond['status']) && !empty($cond['status'])) {	
 $sqla .= " and delivery_order.status='".$cond['status']."'";
}

if(isset($cond['type']) && !empty($cond['type'])) {
 $sqla .= " and delivery_order.type='".$cond['type']."'";
}

if(isset($cond['product_type']) && !empty($cond['product_type'])) {
 $sqla .= " and delivery_order_line_items.type='".$cond['product_type']."'";
}

if(isset($cond['id_products']) && !empty($cond['id_products'])) {
 $sqla .= " and delivery_order_line_items.id_products='".$cond['id_products']."'";
}

if(isset($cond['id_delivery_order_line_items']) && !empty($cond['id_delivery_order_line_items'])) {
 $sqla .= " and delivery_order_line_items.id_delivery_order_line_items='".$cond['id_delivery_order_line_items']."'";
}


if(isset($cond['delivery_order']) && !empty($cond['delivery_order'])) {
 $sqla .= " and delivery_order.id_delivery_order='".$cond['delivery_order']."'";
}
$sqla .= " and delivery_order_line_items.quantity>0";

 $sqla.=" group by delivery_order_line_items.id_delivery_order_line_items   ";
 $sqla .= " ORDER BY delivery_order_line_items.id_delivery_order_line_items DESC";


//echo $sqla;exit;
$query = $this->db->query($sqla);

$results = $query->result_array();

foreach($results as $results){
	}


return $data;
    }
	

	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getDeliveryOrder_old($order_id,$cond=array())
    {
        $data = array();
		$flag="";
		if(isset($cond['flag']) && $cond['flag']==4 ){
			$flag=$cond['flag'];
			}
        $this->db->select('delivery_order.*');
        $this->db->from('delivery_order');
		$this->db->join('delivery_order_line_items','delivery_order_line_items.id_orders = delivery_order.id_delivery_order');
        $this->db->where('id_delivery_order', $order_id);
		if(isset($cond)){
		$this->db->where($cond);}
        $query = $this->db->get();
        $order = $query->row_array();
		if(!empty($order)){

        //get user
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->where('id_user', $order['id_user']);
        $query = $this->db->get();
        $user  = $query->row_array();

        // line items
        $line_items          = array();
        $this->db->select('delivery_order_line_items.*');
        $this->db->from('delivery_order_line_items');
		$cond2['id_orders']=$order['id_delivery_order'];
		
		if(isset($cond['flag']) && $cond['flag']==4 ){
		$cond2['flag']=0;}
        $this->db->where('id_orders', $order['id_delivery_order']);
        $query = $this->db->get();
        $items = $query->result_array();
        $i     = 0;
        foreach ($items as $item) {
			$stock="";
			if($item['type']=="option"){
			$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$item['id_products']));
			$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
		}else{
			$product=$this->fct->getonerecord('products',array('id_products'=>$item['id_products']));	
				}
            $this->db->select('products.*');
            $this->db->from('products');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $res   = $query->row_array();
            // product gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $this->db->where('id_products', $product['id_products']);
            $query                                = $this->db->get();
            $gallery                              = $query->result_array();
            $line_items[$i]                       = $item;
            $line_items[$i]['product']            = $product;
			$line_items[$i]['stock']            = $stock;
            $line_items[$i]['product']['gallery'] = $gallery;
			
			    $this->db->select('brands.*');
            $this->db->from('brands');
            $this->db->where('id_brands', $item['id_brands']);
            $query = $this->db->get();
            $brand   = $query->row_array();
			$line_items[$i]['brand']            = $brand;
            $i++;
        }
        // set data
        $data                  = $order;
        $data['user']          = $user;
   
        $data['line_items']    = $line_items;
       
        /*print '<pre>';
        print_r($data);
        exit;*/
       }
	    return $data;
    }
	
	 public function getProductsStock($product,$cond2=array(),$cond3=array()){	
	
	 /////////////////////////////////////// delivery order line items //////////////////////////////////////////////
	$cond2['delivery_order_line_items.deleted']=0;
		$data=array();
		if($product['product_type']=="stock"){
			
				$type="option";}else{
				
				$type="product"; }	
				
		$cond2['delivery_order_line_items.type']=$type;		
		$cond2['delivery_order_line_items.id_products']=$product['id_products'];
		$this->db->select('delivery_order_line_items.*, delivery_order.type as type_order');
        $this->db->from('delivery_order');
		$this->db->join('delivery_order_line_items','delivery_order_line_items.id_orders = delivery_order.id_delivery_order');
	/////////////////////////////////////	
if(isset($cond3['order_type']) && !empty($cond3['order_type'])) {

/*$ignore="select id_old_delivery_order from delivery_order where deleted=0 ";
$this->db->where_not_in('delivery_order.id_delivery_order', 'select id_old_delivery_order from delivery_order where deleted=0');*/

if($cond3['order_type']==2) {
$cond2['delivery_order.invoice']=1;
}

if($cond3['order_type']==1) {
$cond2['delivery_order.type']=$cond3['order_type'];
$cond2['delivery_order.invoice']=0;
$cond2['delivery_order.invoice']=0;}

$cond2['delivery_order.status']=1;
/*$cond2['delivery_order.id_old_delivery_order !=']="";*/
$this->db->where('delivery_order.id_delivery_order NOT IN (select id_old_delivery_order from delivery_order where deleted=0)');
}


////////////////////////////////////////////////////////////
		if(isset($cond2)){
		$this->db->where($cond2);}
		
		$this->db->group_by('delivery_order_line_items.id_delivery_order_line_items');
        $this->db->order_by('delivery_order_line_items.id_delivery_order_line_items asc');
        $query = $this->db->get();
		
        $delivery_order_line_items = $query->result_array();
		

	

		$qty_sold_by_admin=0;
		$qty_payable=0;
		
		$qty_sold_by_admin_paid=0;
		$qty_payable_paid=0;
	    $total_quantity=0;
		$return_quantity=0;
		

		
		foreach($delivery_order_line_items as $val){
		$return_quantity=$return_quantity+$val['return_quantity'];
		$total_quantity=$total_quantity+$val['quantity'];

		if($val['type_order']==2){
		$qty_payable=$qty_payable+$val['quantity'];
		$qty_payable_paid=$qty_payable_paid+$val['sold_quantity'];}else{
		$qty_sold_by_admin=$qty_sold_by_admin+$val['quantity'];
		$qty_sold_by_admin_paid=$qty_sold_by_admin_paid+$val['sold_quantity'];}
		}
	
		$data['qty_sold_by_admin']=$qty_sold_by_admin;
		$data['qty_payable']=$qty_payable;
		$data['return_quantity']=$return_quantity;
		$data['total_quantity']=$total_quantity;
		$data['qty_sold_by_admin_paid']=$qty_sold_by_admin_paid;
		$data['qty_payable_paid']=$qty_payable_paid;
		
		$data['remain_consignment']=$qty_sold_by_admin-$qty_sold_by_admin_paid;
		$data['remain_paid']=$qty_payable-$qty_payable_paid;
		
		$data['quantity']=$total_quantity-$return_quantity-$qty_payable_paid-$qty_sold_by_admin_paid;
			
	return $data;}
	
	public function getProductsQuantityPaidStatus($product,$cond2=array()){
	 /////////////////////////////////////// orders recording //////////////////////////////////////////////
	$cond2['delivery_order_line_items.deleted']=0;
		$data=array();
		if($product['product_type']=="stock"){
			
				$type="option";}else{
				
				$type="product"; }	
				
		$cond2['delivery_order_line_items.type']=$type;		
		
		$this->db->select('orders_recording.*, delivery_order.type as type_order');
        $this->db->from('delivery_order_line_items');
		$this->db->join('delivery_order','delivery_order_line_items.id_orders = delivery_order.id_delivery_order');
		$this->db->join('orders_recording','orders_recording.id_delivery_order_line_items = delivery_order_line_items.id_delivery_order_line_items');
		
		if(isset($cond2)){
		$this->db->where($cond2);}
		$this->db->group_by('orders_recording.id_orders_recording');
        $this->db->order_by('orders_recording.id_orders_recording asc');
        $query = $this->db->get();
        $delivery_order_line_items = $query->result_array();
		
		
	
		$qty_sold_by_admin=0;
		$qty_payable=0;
		
		$qty_sold_by_admin_paid=0;
		$qty_payable_paid=0;
	    $total_quantity=0;
		$return_quantity=0;
		foreach($delivery_order_line_items as $val){
		$total_quantity=$total_quantity+$val['quantity'];

		if($val['type_order']==2){
		$qty_payable=$qty_payable+$val['quantity'];
		}else{
		$qty_sold_by_admin=$qty_sold_by_admin+$val['quantity'];
		}
		}
	
		$data['qty_sold_by_admin']=$qty_sold_by_admin;
		$data['qty_payable']=$qty_payable;
		$data['total_quantity']=$total_quantity;
	
	return $data;}
	
	public function getDeliveryOrder($order_id,$cond=array())
    {
        $data = array();
		$flag="";
		if(isset($cond['flag']) && $cond['flag']==4 ){
			$flag=$cond['flag'];
		$cond['flag']=0;}
        $this->db->select('delivery_order.*');
        $this->db->from('delivery_order');
		$this->db->join('delivery_order_line_items','delivery_order_line_items.id_orders = delivery_order.id_delivery_order','left');
        $this->db->where('id_delivery_order', $order_id);
		if(isset($cond)){
		$this->db->where($cond);}
        $query = $this->db->get();
        $order = $query->row_array();
		if(!empty($order)){

        //get user
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->where('id_user', $order['id_user']);
        $query = $this->db->get();
        $user  = $query->row_array();
		
		$this->db->select('delivery_order.*');
        $this->db->from('delivery_order');
        $this->db->where('id_old_delivery_order', $order['id_delivery_order']);
        $query = $this->db->get();
        $exported_order  = $query->row_array();
		
		$this->db->select('delivery_order.*');
        $this->db->from('delivery_order');
        $this->db->where('id_delivery_order', $order['id_old_delivery_order']);
        $query = $this->db->get();
        $parent_order  = $query->row_array();
		
		$this->db->select('brands.*');
        $this->db->from('brands');
        $this->db->where('id_brands', $order['id_brands']);
        $query = $this->db->get();
        $brand  = $query->row_array();

        // line items
        $line_items          = array();
        $this->db->select('delivery_order_line_items.*');
        $this->db->from('delivery_order_line_items');
			if($flag==4 ){
		$cond2['flag']=0;}
		$cond2['id_orders']=$order['id_delivery_order'];
	
        $this->db->where($cond2);
		$this->db->group_by('delivery_order_line_items.id_brands');
        $query = $this->db->get();
        $brands_line_items = $query->result_array();


		$b=0;	
		foreach($brands_line_items as $val){
			$cond_b['id_brands']=$val['id_brands'];
			$cond_b['id_orders']=$order['id_delivery_order'];
		if($flag==4 ){
		$cond_b['flag']=0;}
			$line_items_b=$this->fct->getAll_cond('delivery_order_line_items','id_delivery_order_line_items desc',$cond_b);
		  
			$brand=$this->fct->getonerecord('brands',array('id_brands'=>$val['id_brands']));

			$i     = 0;
			$line_items[$b]                    = $brand;
			  foreach ($line_items_b as $item) {
				 
				   
			$stock="";
			if($item['type']=="option"){
				
			$stock=$this->fct->getonerow('products_stock',array('id_products_stock'=>$item['id_products']));
			$product=$this->fct->getonerow('products',array('id_products'=>$stock['id_products']));
		}else{
			$product=$this->fct->getonerow('products',array('id_products'=>$item['id_products']));	
				}
            $this->db->select('products.*');
            $this->db->from('products');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $res   = $query->row_array();
            // product gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $this->db->where('id_products', $product['id_products']);
            $query                                = $this->db->get();
            $gallery                              = $query->result_array();
			
            $line_items[$b]['line_items'][$i]                    = $item;
            $line_items[$b]['line_items'][$i]['product']            = $product;
			$line_items[$b]['line_items'][$i]['stock']            = $stock;
            $line_items[$b]['line_items'][$i]['product']['gallery'] = $gallery;

            $i++;
        }
			
			$b++;
			}
        
      
        // set data
        $data                  = $order;
        $data['user']          = $user;
		$data['brand']          = $brand;
   		$data['line_items']    = $line_items;
		$data['exported_order']    = $exported_order;
		$data['parent']    = $parent_order;
     }
	    return $data;
    }
	
		public function getGroupProductsOrder($order_id,$cond=array())
    {
        $data = array();
		$cond['group_products']=1;
		$flag="";
		if(isset($cond['flag']) && $cond['flag']==4 ){
			$flag=$cond['flag'];
		$cond['flag']=0;}
        $this->db->select('products.*, products.id_products as id_orders');
        $this->db->from('products');
		$this->db->join('group_products_line_items','group_products_line_items.id_orders = products.id_products','left');
        $this->db->where('products.id_products', $order_id);
		if(isset($cond)){
		$this->db->where($cond);}
        $query = $this->db->get();
        $order = $query->row_array();
		if(!empty($order)){
        //get user
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->where('id_user', $order['id_user']);
        $query = $this->db->get();
        $user  = $query->row_array();

		


        // line items
        $group_products_line_items          = array();
        $this->db->select('group_products_line_items.*');
        $this->db->from('group_products_line_items');
			if($flag==4 ){
		$cond2['flag']=0;}
		$cond2['id_orders']=$order['id_products'];
	
        $this->db->where($cond2);
		$this->db->group_by('group_products_line_items.id_brands');
        $query = $this->db->get();
        $brands_line_items = $query->result_array();


		$b=0;	
		foreach($brands_line_items as $val){
			$cond_b['id_brands']=$val['id_brands'];
			$cond_b['id_orders']=$order['id_orders'];
		if($flag==4 ){
		$cond_b['flag']=0;}
			$group_products_line_items_b=$this->fct->getAll_cond('group_products_line_items','id_group_products_line_items desc',$cond_b);
		  
			$brand=$this->fct->getonerecord('brands',array('id_brands'=>$val['id_brands']));

			$i     = 0;
			$group_products_line_items[$b]                    = $brand;
			  foreach ($group_products_line_items_b as $item) {
 
			$stock="";
			if($item['type']=="option"){
				
			$stock=$this->fct->getonerow('products_stock',array('id_products_stock'=>$item['id_products']));
			$product=$this->fct->getonerow('products',array('id_products'=>$stock['id_products']));
		}else{
			$product=$this->fct->getonerow('products',array('id_products'=>$item['id_products']));	
				}
            $this->db->select('products.*');
            $this->db->from('products');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $res   = $query->row_array();
            // product gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $this->db->where('id_products', $product['id_products']);
            $query                                = $this->db->get();
            $gallery                              = $query->result_array();
			
            $group_products_line_items[$b]['line_items'][$i]                    = $item;
            $group_products_line_items[$b]['line_items'][$i]['product']            = $product;
			$group_products_line_items[$b]['line_items'][$i]['stock']            = $stock;
    		$i++;}
			
	
			
			$b++;
			}
        // set data
        $data                  = $order;
        $data['user']          = $user;
	
   		$data['group_products_line_items']    = $group_products_line_items;
	
     }
	 

	    return $data;
    }	
	

    public function getGroupProductsLineItem($id_line_item,$cond=array())
    {   
	if($id_line_item!=""){
		$cond['id_group_products_line_items']=$id_line_item;}
	$line_item=$this->fct->getonerecord('group_products_line_items',$cond);
        $data = array();
		
		if(!empty($line_item)){
		$data=$line_item;
		if($line_item['type']=="option"){
		$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));	
		$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
		$type="stock";
		
			}else{
		$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
		$stock=array();
		$type="product";
		}
		if(empty($line_item['id_purchases'])){
		$line_item['id_purchases']=0;	}
		
            $this->db->select('*');
            $this->db->from('purchases');
            $this->db->where('id_purchases', $line_item['id_purchases']);
            $query                                = $this->db->get();
            $purchase                              = $query->row_array();	

		$data['product']=$product;
		$data['stock']=$stock;
		$data['purchase']=$purchase;
		}
	    return $data;
    }
	

	
	
	
public function updateDeliverOrders($data){
	$combination=$data['options_en'];
	$quantity=$data['quantity'];
	$group_products_quantity=0;
	$id_products=$data['id_products'];
	$id_line_items=$data['id_line_items'];
$k=0;
if($data['group_products']==1){
	$orderData=$this->admin_fct->getGroupProductsOrder($id_products);
	$brands=$orderData['group_products_line_items'];
	
	foreach($brands as $brand){
	$group_products_line_items=$brand['line_items'];	
	foreach($group_products_line_items as $group_products_line_item){
$group_products_quantity=$quantity*$group_products_line_item['quantity'];

	if($group_products_line_item['type']=="option"){
		$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$group_products_line_item['id_products']));
		$combination=$stock['combination'];
		$id_products=$stock['id_products'];
		}else{
		$product=$this->fct->getonerecord('products',array('id_products'=>$group_products_line_item['id_products']));
		$combination="";
		$id_products=$product['id_products'];}
		$line_items_arr[$k]['options_en']=$combination;
		$line_items_arr[$k]['quantity']=$group_products_quantity;
		$line_items_arr[$k]['id_products']=$id_products; 
		$line_items_arr[$k]['group_products']=1;
		$k++;}}
		
	}else{
	$line_items_arr[0]=$data;
	$line_items_arr[0]['group_products']=0;
		}
		
foreach($line_items_arr as $line_item_a){	
$id_products=$line_item_a['id_products'];
$combination=$line_item_a['options_en'];	
$quantity=$line_item_a['quantity'];

	if(!empty($combination)){
		$stock=$this->fct->getonerecord('products_stock',array('id_products'=>$id_products,'combination'=>$combination));
		$product=$this->fct->getonerecord('products',array('id_products'=>$id_products));
		$type="option";
		$id_product=$stock['id_products_stock'];
		}else{	
	$stock=array();		
	$product=$this->fct->getonerecord('products',array('id_products'=>$id_products));
	$type="product";
	$id_product=$product['id_products'];}

	/*$cond2['id_orders']=$order['id_delivery_order'];*/
	$cond2['delivery_order_line_items.flag']=0;
	$cond2['delivery_order_line_items.id_products']=$id_product;
	$cond2['delivery_order_line_items.type']=$type;
	$cond2['delivery_order.status']=1;



       	 $sqla =" select delivery_order_line_items.* from delivery_order_line_items ";
 		 $sqla .=" join delivery_order on delivery_order.id_delivery_order = delivery_order_line_items.id_orders where delivery_order.deleted=0 and delivery_order.status=1 and delivery_order_line_items.deleted=0 ";
		 $sqla .=" and delivery_order_line_items.flag=0 and delivery_order_line_items.id_products=".$id_product.' and delivery_order_line_items.type="'.$type.'"';
		 $sqla .=" and delivery_order.status=1 and delivery_order.id_delivery_order NOT IN (select id_old_delivery_order from delivery_order where deleted=0)";
		//echo $sqla;exit;
		 $sqla .=" and(delivery_order.invoice=1 or delivery_order.id_old_delivery_order !='')";
		 $sqla .=" group by delivery_order_line_items.id_delivery_order_line_items ";	

		$query = $this->db->query($sqla);
        $line_items = $query->result_array();
	
		foreach($line_items as $line_item){

		$remaining_quantity=$line_item['quantity']-$line_item['return_quantity']-$line_item['sold_quantity'];
		
		
		if($remaining_quantity>=$quantity){	
		$quantity_sold=$quantity;
		$quantity=0;
			}else{
		$quantity_sold=$remaining_quantity;
		$quantity=$quantity-$remaining_quantity;	
				}
				
		$new_quantity_sold=$quantity_sold+$line_item['sold_quantity'];
		
		
		/////////////////////////////////////////UPDATE LINE ITEMS//////////////////////
		$remaining_quantity=$line_item['quantity']-$line_item['return_quantity']-$new_quantity_sold;
		$update_delivery_order_line_item['sold_quantity']=$new_quantity_sold;
		if($remaining_quantity<=0){
		$update_delivery_order_line_item['flag']=1;}else{
		$update_delivery_order_line_item['flag']=0;}	
		$this->db->where(array('id_delivery_order_line_items'=>$line_item['id_delivery_order_line_items']));	
		$this->db->update('delivery_order_line_items',$update_delivery_order_line_item);
	
	    /////////////////////////////////////////Recording line item//////////////////////
 		$insert_record['id_line_items']=$id_line_items;
		$insert_record['quantity']=$quantity_sold;
		$insert_record['id_orders']=$data['id_orders'];
	
		
		if(isset($data['admin']) && $data['admin']!=0){
		$line_item['admin']=1;}
		
		$insert_record['created_date']=date('Y-m-d h:i:s');;
		$insert_record['id_delivery_order_line_items']=$line_item['id_delivery_order_line_items'];
		$this->db->insert('orders_recording',$insert_record);
		
		if($quantity==0) break;
		}
		
		}	
		return true;
		
	
	}	
	public function getReturnStockOrder($order_id,$cond=array())
    {
        $data = array();
        $this->db->select('stock_return.*');
        $this->db->from('stock_return');
        $this->db->where('id_stock_return', $order_id);
		if(isset($cond)){
		$this->db->where($cond);}
        $query = $this->db->get();
        $order = $query->row_array();
		if(!empty($order)){

        //get user
        $this->db->select('user.*');
        $this->db->from('user');
        $this->db->where('id_user', $order['id_user']);
        $query = $this->db->get();
        $user  = $query->row_array();
$b=0;	
 // line items
 $sqla =" select delivery_order_line_items.*,delivery_order.type as type_order,delivery_order.invoice as invoice ,delivery_order.rand as order_rand , delivery_order.id_invoice as id_invoice  , stock_return_line_items.id_stock_return_line_items, stock_return_line_items.quantity as return_quantity_stock from delivery_order_line_items";
 $sqla .=" join stock_return_line_items on stock_return_line_items.id_line_items = delivery_order_line_items.id_delivery_order_line_items ";
  $sqla .=" join delivery_order on delivery_order.id_delivery_order = delivery_order_line_items.id_orders ";
 /*$sqla .=" join store_keeper on delivery_order_line_items.id_delivery_order_line_items = store_keeper.id_line_items_return ";*/
$sqla .=" where delivery_order_line_items.deleted=0 and stock_return_line_items.deleted=0 and  stock_return_line_items.id_orders=".$order_id;  


   //echo $sqla;exit;
$query = $this->db->query($sqla);
$line_items = $query->result_array();

			$i     = 0;
			
			  foreach ($line_items as $item) {
		   
			$stock="";
			if($item['type']=="option"){
			$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$item['id_products']));
			$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
		}else{
			$product=$this->fct->getonerecord('products',array('id_products'=>$item['id_products']));	
				}
            $this->db->select('products.*');
            $this->db->from('products');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $res   = $query->row_array();
			
			$delivery_order=$this->fct->getonerecord('delivery_order',array('id_delivery_order'=>$item['id_orders']));
            // product gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $this->db->where('id_products', $product['id_products']);
            $query                                = $this->db->get();
            $gallery                              = $query->result_array();
			
            $line_items[$i]                    = $item;
            $line_items[$i]['product']            = $product;
			$line_items[$i]['delivery_order']    = $delivery_order;
			$line_items[$i]['stock']            = $stock;
            $line_items[$i]['product']['gallery'] = $gallery;

            $i++;
        }
			
	
	
        
      
        // set data
        $data                  = $order;
        $data['user']          = $user;
   		$data['line_items']    = $line_items;
     }
	    return $data;
    }	
	
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getLineItem($id_line_item,$cond=array())
    {   $line_item=$this->fct->getonerecord('line_items',array('id_line_items'=>$id_line_item));
        $data = array();
		
		if(!empty($line_item)){
		$data=$line_item;
		if(!empty($line_item['option'])){
		$stock=$this->fct->getonerecord('products_stock',array('id_products'=>$line_item['id_products'],'combination'=>$line_item['option']));	
		$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
		$type="stock";
		
			}else{
		$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
		$stock=array();
		$type="product";
		}
		


		$data['product']=$product;
		$data['stock']=$stock;
	
		}
	    return $data;
    }
	
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getDeliveryLineItem($id_line_item,$cond=array())
    {   
	if($id_line_item!=""){
		$cond['id_delivery_order_line_items']=$id_line_item;}
	$line_item=$this->fct->getonerecord('delivery_order_line_items',$cond);
        $data = array();
		
		if(!empty($line_item)){
		$data=$line_item;
		if($line_item['type']=="option"){
		$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));	
		$product=$this->fct->getonerecord('products',array('id_products'=>$stock['id_products']));
		$type="stock";
		
			}else{
		$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
		$stock=array();
		$type="product";
		}
		
            $this->db->select('*');
            $this->db->from('purchases');
            $this->db->where('id_purchases', $line_item['id_purchases']);
            $query                                = $this->db->get();
            $purchase                              = $query->row_array();	

		$data['product']=$product;
		$data['stock']=$stock;
		$data['purchase']=$purchase;
		}
	    return $data;
    }
	
	public function delivery_order_line_items($cond=array())
    {   

       	 $sqla =" select delivery_order_line_items.*, delivery_order.created_date, delivery_order.id_invoice, delivery_order.rand, delivery_order.id_delivery_order, delivery_order.id_role, delivery_order.invoice , delivery_order.type as type  , delivery_order.return, delivery_order.status as order_status from delivery_order_line_items ";
 		 $sqla .=" join delivery_order on delivery_order.id_delivery_order = delivery_order_line_items.id_orders where delivery_order.deleted=0 and delivery_order_line_items.deleted=0 ";
		 
		 $sqla .=" and delivery_order.status=1 and delivery_order.id_delivery_order NOT IN (select id_old_delivery_order from delivery_order where deleted=0)";
		//echo $sqla;exit;
		$sqla .="and(delivery_order.invoice=1 or delivery_order.id_old_delivery_order !='')";
		if(isset($cond['type'])){
		$sqla .=" and delivery_order_line_items.type='".$cond['type']."'";	}
		

		if(isset($cond['id_products'])){
		$sqla .=" and delivery_order_line_items.id_products=".$cond['id_products'];	}
		
	$sqla .=" group by delivery_order_line_items.id_delivery_order_line_items ";	
$query = $this->db->query($sqla);
$results = $query->result_array();


return $results;
	
	    return $data;
    }
	
public function updateProductsQuantities(){

	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$order_status=$this->input->post('type');
	
						$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];
						if(isset($price_arr[$key])){
						$price=$price_arr[$key];}else{$price=0;}
						if(isset($cost_arr[$key])){
						$cost=$cost_arr[$key];}else{$cost=0;}
						
						$line_item=$this->admin_fct->getDeliveryLineItem($val);
			
						$type=$line_item['type'];
						$stock=$line_item['stock'];
						$product=$line_item['product'];
						$purchase=$line_item['purchase'];
							
$d_purchase['deleted']=0;	
$d_purchase['quantity']=$qty;
$d_purchase['status']=$order_status;
$d_purchase['price']=$price;
$d_purchase['cost']=$cost;
$d_purchase['id_products']=$line_item['id_products'];	

if($type=="option"){
$d_purchase['type']="stock";}else{
$d_purchase['type']="product";
}
$delete_qty=0;
////////// //////////////////////////	
if(!empty($purchase)){
	$delete_qty=$purchases['quantity'];
	$d_purchase['updated_date']=date('Y-m-d h:i:s');	
	$this->db->where("id_purchases",$purchase["id_purchases"]);
	$this->db->update("purchases",$d_purchase);
	$new_id=$purchase['id_purchases'];
}else{
$d_purchase['created_date']=date('Y-m-d h:i:s');
$this->db->insert("purchases",$d_purchase);	
	
$new_id = $this->db->insert_id();}
	

//////////UPDATE PRODUCT//////////////////////////	
if($type=="option"){
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));
	$update_product['quantity']=$stock['quantity']+$qty-$delete_qty;
	$this->db->where("id_products_stock",$stock["id_products_stock"]);
	$this->db->update('products_stock',$update_product);
	
	//////////////////Threshold//////
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$stock['id_products_stock']));
	
	$this->admin_fct->checkThreshold($stock);
	}else{
		
	$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
	
	$update_product['quantity']=$product['quantity']+$qty-$delete_qty;

	$this->db->where("id_products",$product["id_products"]);
	$this->db->update('products',$update_product);
	//////////////////Threshold//////
	$product=$this->fct->getonerecord('products',array('id_products'=>$product['id_products']));
	$this->admin_fct->checkThreshold($product);
				}	

					    $update_line_item['id_purchases'] = $new_id;
						
						// options
						$this->db->where("id_delivery_order_line_items",$val);
						$this->db->update('delivery_order_line_items',$update_line_item);
		 }
		}



		
public function updateReturnStocks(){

	$qty_arr=$this->input->post('qty');
	$ids_arr=$this->input->post('ids');
	$price_arr=$this->input->post('price');
	$cost_arr=$this->input->post('cost');
	$order_status=$this->input->post('type');
	
						$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
						$qty=$qty_arr[$key];
						if(isset($price_arr[$key])){
						$price=$price_arr[$key];}else{$price=0;}
						if(isset($cost_arr[$key])){
						$cost=$cost_arr[$key];}else{$cost=0;}
						
						$line_item=$this->admin_fct->getDeliveryLineItem($val);
			
						$type=$line_item['type'];
						$stock=$line_item['stock'];
						$product=$line_item['product'];
						$purchase=$line_item['purchase'];
				
						
$d_purchase['deleted']=0;	
$d_purchase['quantity']=$qty;
$d_purchase['status']=$order_status;
$d_purchase['price']=$price;
$d_purchase['cost']=$cost;
$d_purchase['id_products']=$line_item['id_products'];	

if($type=="option"){
$d_purchase['type']="stock";}else{
$d_purchase['type']="product";
}

$delete_qty=0;


//////////UPDATE PRODUCT//////////////////////////	
if($type=="option"){
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));
	$update_product['quantity']=$stock['quantity']+$qty-$delete_qty;
	$this->db->where("id_products_stock",$stock["id_products_stock"]);
	$this->db->update('products_stock',$update_product);}else{
	$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
	$update_product['quantity']=$product['quantity']+$qty-$delete_qty;
	$this->db->where("id_products",$product["id_products"]);
	$this->db->update('products',$update_product);
				}	

$update_line_item['id_purchases'] =0;					
// options
$this->db->where("id_delivery_order_line_items",$val);
$this->db->update('delivery_order_line_items',$update_line_item);
		 }
		}	
		
public function updateReturnStocks2(){

 $id_stock_return=$this->input->post('id_stock_return');
  $rand=$this->input->post('rand');
	
$cond=array('rand'=>$rand);
$order_details=$this->admin_fct->getReturnStockOrder($id_stock_return,$cond);
$line_items=$order_details['line_items'];

						$j=0;
						foreach($line_items as $line_item) {$j++;
						
$type=$line_item['type'];						
					

$delete_qty=$line_item['return_quantity_stock'];
$qty=$line_item['quantity'];

//////////UPDATE PRODUCT//////////////////////////	
if($type=="option"){
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));
	$update_product['quantity']=$stock['quantity']-$delete_qty;
	$this->db->where("id_products_stock",$stock["id_products_stock"]);
	$this->db->update('products_stock',$update_product);
		//////////////////Threshold//////
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$stock['id_products_stock']));
	$this->admin_fct->checkThreshold($stock);}else{
	$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
	$update_product['quantity']=$product['quantity']-$delete_qty;
	$this->db->where("id_products",$product["id_products"]);
	$this->db->update('products',$update_product);
	
	//////////////////Threshold//////
	$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));;
	$this->admin_fct->checkThreshold($product);
				}	

$update_line_item['id_purchases'] =0;	
$update_line_item['return_quantity'] =$line_item['return_quantity']+$delete_qty;
$remaining_quantity=$line_item['quantity']-$line_item['sold_quantity']-$line_item['return_quantity']-$delete_qty;	

if($remaining_quantity<=0){
$update_line_item['flag'] =1;	}

		
// options
$this->db->where("id_delivery_order_line_items",$line_item["id_delivery_order_line_items"]);
$this->db->update('delivery_order_line_items',$update_line_item);
}}			
		
public function removeProductsQuantities(){

	$ids_arr=$this->input->post('ids');


	
	
						$j=0;
						foreach($ids_arr as $key=>$val) {$j++;
					
						
						$line_item=$this->admin_fct->getDeliveryLineItem($val);
	
						$delete_qty=$line_item['quantity'];
						$type=$line_item['type'];
					
			
				
						
$d_purchase['deleted']=1;	

//////////UPDATE PRODUCT//////////////////////////	
if($type=="option"){

	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));
	$update_product['quantity']=$stock['quantity']-$delete_qty;
	$this->db->where("id_products_stock",$stock["id_products_stock"]);
	$this->db->update('products_stock',$update_product);
	//////////////////Threshold//////	
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$stock['id_products_stock']));
	$this->admin_fct->checkThreshold($stock);
}else{
	$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
	$update_product['quantity']=$product['quantity']-$delete_qty;
	$this->db->where("id_products",$product["id_products"]);
	$this->db->update('products',$update_product);
	
//////////////////Threshold//////	
$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
$this->admin_fct->checkThreshold($product);
				}	
						
	$d_purchase['deleted']=1;
	$this->db->where("id_purchases",$line_item["id_purchases"]);
	$this->db->update('purchases',$d_purchase);				
			}
		}		
		
public function returnOrderProductsQuantities(){

	$ids_arr=$this->input->post('checklist');
	$return_to_stock=$this->input->post('return_to_stock');
	$id_order=$this->input->post('id_order');
	$rand=$this->input->post('rand');
	$return_qty_arr=$this->input->post('qty');
	$order=$this->fct->getOrder($id_order,$rand);

	$j=0;
	
$net_price = 0;
$net_discount_price = 0;
$weight=0;
$number_of_pieces=0;
$i=0;
$miles=0;
$total_redeem_miles=0;
$total_miles=0;
$net_redeem_miles_price=0;
$net_discount_redeem_miles_price=0;



	foreach($ids_arr as $key=>$val) {$j++;
					
	$return_qty=$return_qty_arr[$val];
						
	$line_item=$this->admin_fct->getLineItem($val);
	$old_qty=$line_item['quantity']+$line_item['return_quantity'];
	
	if(!empty($line_item['options_en'])){
		$type='option';
		}else{
		$type='product';
		}


						
/*$d_purchase['deleted']=1;	*/

//////////UPDATE PRODUCT//////////////////////////	
if(isset($return_to_stock[$key])){
if($type=="option"){

	$stock=$this->fct->getonerecord('products_stock',array('combination'=>$line_item['options_en'],'id_products'=>$line_item['id_products']));
	/*$update_product['quantity']=$stock['quantity']-$line_item['return_quantity']+$return_qty;*/
	$update_product['quantity']=$stock['quantity']+$return_qty;
	$update_product['return_quantity']=$stock['return_quantity']-$line_item['return_quantity']+$return_qty;	
	$this->db->where("id_products_stock",$stock["id_products_stock"]);
	$this->db->update('products_stock',$update_product);
		//////////////////Threshold//////	
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$stock['id_products_stock']));
	$this->admin_fct->checkThreshold($stock);
	}else{
	$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
	/*$update_product['quantity']=$product['quantity']-$line_item['return_quantity']+$return_qty;*/
	$update_product['quantity']=$product['quantity']+$return_qty;

	$update_product['return_quantity']=$product['return_quantity']-$line_item['return_quantity']+$return_qty;	
	$this->db->where("id_products",$product["id_products"]);
	$this->db->update('products',$update_product);
		//////////////////Threshold//////	
	$product_t=$this->fct->getonerecord('products',array('id_products'=>$product['id_products']));
	$this->admin_fct->checkThreshold($product_t);
}}else{}


	
//////////////////UPDATE DELIVERY ORDER//////////////////////////		

	$cond3['orders_recording.id_orders']=$id_order;
	$cond3['orders_recording.id_line_items']=$val;


	    $this->db->select('delivery_order_line_items.*, orders_recording.quantity as quantity_recordring, orders_recording.id_orders_recording');
        $this->db->from('orders_recording');
		$this->db->join('delivery_order_line_items','delivery_order_line_items.id_delivery_order_line_items = orders_recording.id_delivery_order_line_items');
        $this->db->where($cond3);
	
        $query = $this->db->get();
        $orders_recording = $query->result_array();
		$new_return_qty=$return_qty;

		foreach($orders_recording as $val){
			
		$record_qty=$val['quantity_recordring'];
		$sold_quantity=$val['sold_quantity'];

		if($record_qty>=$new_return_qty){	
		$return_qty_delivery=$new_return_qty;
		$new_return_qty=0;
			}else{
		$return_qty_delivery=$record_qty;
		$new_return_qty=$new_return_qty-$record_qty;	
				}
						
		$new_sold_quantity=$val['sold_quantity']-$return_qty_delivery;

		$return_quantity_delivery_order=$val['return_quantity'];
		
		if(isset($return_to_stock[$key])){}else{
		$return_quantity_delivery_order=$val['return_quantity']+$return_qty_delivery;}
		
		$remaining_qty_delivery_order=$val['quantity']-$return_quantity_delivery_order-$new_sold_quantity;
	    
		if($remaining_qty_delivery_order<=0){
		$update_delivery_order_line_item['flag']=1;}else{
		$update_delivery_order_line_item['flag']=0;}
		$update_delivery_order_line_item['return_quantity']=$return_quantity_delivery_order;
		$update_delivery_order_line_item['sold_quantity']=$new_sold_quantity;
		
		$this->db->where(array('id_delivery_order_line_items'=>$val['id_delivery_order_line_items']));	
		$this->db->update('delivery_order_line_items',$update_delivery_order_line_item);
		
		$update_orders_recording['quantity']=$record_qty-$return_qty_delivery;
		
		
	/*	$this->db->where(array('id_orders_recording'=>$val['id_orders_recording']));	
		$this->db->update('orders_recording',$update_orders_recording);	*/			
			}

					
///////////////////////////////////////////////////////////////////////////////////////////
$new_qty=$line_item['quantity']-$return_qty;	
	
///////////////////////////////////////////////////////////LINE ITEMS/////////////////////
// Miles
								
								
/*	 $miles=$line_item['miles']*$new_qty;
	 $redeem_miles=$line_item['redeem_miles']*$new_qty;*/
	 $total_price = 0;
	 $update_line_item['updated_date'] = date('Y-m-d h:i:s');
	
// Price	
$new_price=$line_item['price'];
$list_price=$line_item['price_old']; 
$total_price = $list_price * $new_qty;
$total_discount_price = $line_item['price'] * $new_qty;		
$total_miles=$total_miles+$miles;


						/*$update_line_item['miles'] = $miles;
						$update_line_item['redeem_miles'] = $redeem_miles;*/

						$update_line_item['discount'] = $total_discount_price;
						
						$update_line_item['quantity'] = $new_qty;
				
						$update_line_item['return_quantity'] = $line_item['return_quantity']+$return_qty;

						$update_line_item['total_price_old'] =  changeCurrency($total_price,FALSE,$line_item['default_currency'],$line_item['currency']);
						$update_line_item['total_price_old_currency'] = $total_price;	
						
						$update_line_item['total_price'] = changeCurrency($total_discount_price,FALSE,$line_item['default_currency'],$line_item['currency']);
						$update_line_item['total_price_currency'] = $total_discount_price;
				
				
						$this->db->where(array('id_line_items'=>$line_item['id_line_items']));
						$this->db->update('line_items',$update_line_item);
						
						
						$line_item_n=$this->fct->getonerecord('line_items',array('id_line_items'=>$line_item['id_line_items']));
	
					
						
			}
			
		$this->UpdateOrder($id_order,$rand);
						
		}
		
public function UpdateOrder($id_order,$rand){

	$order=$this->fct->getOrder($id_order,$rand);

	$line_items=$order['line_items'];


	$j=0;
	
$net_price = 0;
$net_discount_price = 0;
$weight=0;
$number_of_pieces=0;
$i=0;
$miles=0;
$total_redeem_miles=0;
$total_miles=0;
$net_redeem_miles_price=0;
$net_discount_redeem_miles_price=0;

	foreach($line_items as $line_item) {$j++;
					
	 $qty=$line_item['quantity'];
	 $miles=$line_item['miles']*$qty;
	 $redeem_miles=$line_item['redeem_miles']*$qty;


// Price	
$new_price=$line_item['price'];
$list_price=$line_item['price_old']; 
$total_price = $list_price * $qty;
$total_discount_price = $line_item['price'] * $qty;	
	
$total_miles=$total_miles+$miles;
if($line_item['redeem']==1){
$total_redeem_miles=$total_redeem_miles+$redeem_miles;}

if($line_item['redeem']==1){
	$net_redeem_miles_price=$net_redeem_miles_price + $total_price;
	$net_discount_redeem_miles_price=$net_discount_redeem_miles_price + $total_discount_price;
	}else{
$net_price = $net_price + $total_price;
$net_discount_price = $net_discount_price + $total_discount_price;}

}
			
			//$discount=($net_price+$net_redeem_miles_price)-($net_discount_price+$net_discount_redeem_miles_price);
			//$sub_total=$net_price+$net_redeem_miles_price;
			$discount=$net_price-$net_discount_price;
			$sub_total=$net_price;
			            $order_update_template['sub_total'] = $sub_total ;
						$order_update_template['miles'] = $total_miles ;
						$order_update_template['redeem_miles'] = $total_redeem_miles ;
						$order_update_template['total_price'] = changeCurrency($net_price,false,$line_item['default_currency'],$line_item['currency']) ; ;
						$order_update_template['redeem_discount_miles_amount'] = $net_discount_redeem_miles_price ;
						$order_update_template['redeem_miles_amount'] = $net_redeem_miles_price ;
				
						$order_update_template['total_price_currency'] = $net_price;
						$order_update_template['discount'] =$discount;
						$order_update_template['amount'] = changeCurrency($net_discount_price+$order['shipping_charge'],false,$line_item['default_currency'],$line_item['currency']);
						$order_update_template['amount_currency'] = $net_discount_price+$order['shipping_charge_currency'];
							$this->db->where(array('id_orders'=>$id_order,'rand'=>$rand));
						$this->db->update('orders',$order_update_template);
						
						
		}
		
	
		
public function updateUserCredits($cond,$old_order){
	$id_order=$cond['id_order'];
	$rand=$cond['rand'];
	$order=$this->fct->getOrder($id_order,$rand);
	$return_order=$this->fct->getonerecord('return_orders',array('id_orders'=>$order['id_orders']));


   $user=$this->ecommerce_model->getUserInfo($order['id_user']);

   ////////////////////update User Miles//////////////////
   if($order['payment_method']=="credits"){
   $amount_credits_update=$old_order['amount']-$order['amount'];
   $user_update['available_balance'] = $user['available_balance']+$amount_credits_update;
   $this->db->where("id_user",$order['id_user']);
   $this->db->update("user",$user_update);}
   
	////////////////////update User Miles//////////////////
	
	$miles=$old_order['miles']-$order['miles'];
	$redeem_miles=$old_order['redeem_miles']-$order['redeem_miles'];

	$update_miles['miles']=$user['miles']-$miles+$redeem_miles;

	$this->db->where('id_user',$order['id_user']);
	$this->db->update('user',$update_miles);
	
		}
		
public function return_details($order_id,$rand)
    {
	$order=$this->fct->getOrder($order_id,$rand);
	$old_order=$this->fct->getonerecord('return_orders',array('id_orders'=>$order['id_orders']));


	if(!empty($old_order)){
	
	$amount_return=$old_order['amount_currency']-$order['amount_currency'];
	}else{
	$amount_return=0;}
	
	$data['amount_return']=$amount_return;
	
	
	return $data;
		}				
		
public function returnStock(){

	$ids_arr=$this->input->post('ids');
	$return_qty_arr=$this->input->post('return_arr');

	$j=0;
	foreach($ids_arr as $key=>$val) {$j++;
					
	$delete_qty=$return_qty_arr[$key];					
	$line_item=$this->admin_fct->getLineItem($val);
	if(!empty($line_item['option'])){
		$type='option';
		}else{
		$type='product';
		}


						
/*$d_purchase['deleted']=1;	*/

//////////UPDATE PRODUCT//////////////////////////	
if($type=="option"){
	$stock=$this->fct->getonerecord('products_stock',array('id_products_stock'=>$line_item['id_products']));
	$update_product['quantity']=$stock['quantity']-$delete_qty;
	$update_product['return_quantity']=$product['quantity']+$delete_qty;
	$this->db->where("id_products_stock",$stock["id_products_stock"]);
	$this->db->update('products_stock',$update_product);
	}else{
	$product=$this->fct->getonerecord('products',array('id_products'=>$line_item['id_products']));
	$update_product['quantity']=$product['quantity']-$delete_qty;
	$update_product['return_quantity']=$product['quantity']+$delete_qty;
	$this->db->where("id_products",$product["id_products"]);
	$this->db->update('products',$update_product);
				}	
						

/*	$this->db->where("id_purchases",$line_item["id_purchases"]);
	$this->db->update('purchases',$d_purchase);	*/			
			}
		}
		
	public function getTaskLocations(){ 
	$results['']='Select Location';
$sqla="SELECT  location from task ";
$sqla .= " where task.deleted=0  and  location!='' ";
$sqla .= " group by  task.location  ";
//echo $sqla;exit;
$query = $this->db->query($sqla);
$data = $query->result_array();
foreach($data as $res){
	$results[$res['location']]=$res['location'];
	}
return $results;}				
		
	}