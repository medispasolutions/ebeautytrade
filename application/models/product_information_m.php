<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_information_m extends CI_Model{

public function get_product_information($id){
$this->db->where('id_product_information',$id);
$this->db->where('deleted',0);
$query = $this->db->get('product_information');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset,$cond){
$cond['deleted']=0;
$this->db->where($cond);
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'product_information')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('product_information');
return $query->result_array();	
}

public function getAll($table,$order,$cond){
$cond['deleted']=0;
$this->db->where($cond);
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}