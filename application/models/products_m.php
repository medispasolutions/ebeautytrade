<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products_m extends CI_Model{

public function get_products($id){
$this->db->where('id_products',$id);
$this->db->where('deleted',0);
$query = $this->db->get('products');
return $query->row_array();
}



////////////////////////////////////////CATEGORIES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public function select_product_categories($id_products)
{
 $this->db->select('categories.*');
 $this->db->from('categories');
 $this->db->join('products_categories','products_categories.id_categories = categories.id_categories');
 $cond = array(
  'products_categories.id_products'=>$id_products,
 );
 $this->db->where($cond);
 $this->db->group_by('categories.id_categories');
 $this->db->order_by('categories.title');
 $query = $this->db->get();
 $categories = $query->result_array();
 $results = array();
 foreach($categories as $category) {
  array_push($results,$category['id_categories']);
 }
 return $results;
}
	
	public function insert_product_categories($id_products,$categories)
{

 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('products_categories');
 $cond = array('id_products'=>$id_products);
 $this->db->where($cond);
 $this->db->order_by('products_categories.id_products_categories');
 $query = $this->db->get();
 $results = $query->result_array();
 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_categories']);
  }
 }
 foreach($categories as $category) {
  if(!in_array($category,$old_ids)) {
   array_push($new_ids,$category);
  }
 }
 if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$categories)) {
    array_push($deleted_ids,$id);
   }
  }
 }
 if(!empty($deleted_ids)) {
  $q = 'DELETE FROM products_categories WHERE id_categories IN ('.implode(',',$deleted_ids).') AND id_products='.$id_products;
  $this->db->query($q);
 }
 if(!empty($new_ids)) {
  foreach($new_ids as $new_id) {
   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_categories'] = $new_id;
   $data['id_products'] = $id_products;
   $this->db->insert('products_categories',$data);
  }
 }
}



public function insert_product_informations($id_products,$product_titles,$product_descriptions,$videos)
{

 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('product_information');
 $cond = array('id_products'=>$id_products);
 $this->db->where($cond);
 $this->db->order_by('product_information.id_product_information');
 $query = $this->db->get();
 $results = $query->result_array();
 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_product_information']);
  }
 }

 foreach($product_descriptions as $key=>$info) {
$_data["title"]=$product_titles[$key];
$_data["description"]=$product_descriptions[$key];
$_data["video"]=$videos[$key];
$_data["file"]="";
$_data["file2"]="";
if($key!=""){
$cond_file=array("id_product_information"=>$key);
$_data["file"]=$this->fct->getonecell("product_information","file",$cond_file);
$_data["file2"]=$this->fct->getonecell("product_information","file2",$cond_file);}
if(!empty($_FILES["file_product_info_".$key]["name"])) {

if($key!=""){
$cond_file=array("id_product_information"=>$key);
$old_file=$this->fct->getonecell("product_information","file",$cond_file);
if(!empty($old_file))
unlink("./uploads/product_information/".$old_file);	
}
$file1= $this->fct->uploadImage("file_product_info_".$key,"product_information");
$_data["file"]=$file1;	
}

if(!empty($_FILES["file2_product_info_".$key]["name"])) {

if($key!=""){
$cond_file=array("id_product_information"=>$key);
$old_file=$this->fct->getonecell("product_information","file2",$cond_file);
if(!empty($old_file))
unlink("./uploads/product_information/".$old_file);	
}
$file1= $this->fct->uploadImage("file2_product_info_".$key,"product_information");
$_data["file2"]=$file1;	
}

$_data["id_products"]=$id_products;
  if(!in_array($key,$old_ids)) {
if($product_titles[$key]!="" && $product_titles[$key]){ $this->db->insert('product_information',$_data); }
 }else{	
if($product_titles[$key]!="" && $product_descriptions[$key]!=""){ 

$this->db->where(array('id_product_information'=>$key));
$this->db->update('product_information',$_data); 
  }else{
$this->db->delete('product_information',array('id_product_information'=>$key));	  }
  
	 }


}}

///////////////////////////////////////classifications\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public function select_product_classifications($id_products)
{
 $this->db->select('classifications.*');
 $this->db->from('classifications');
 $this->db->join('products_classifications','products_classifications.id_classifications = classifications.id_classifications');
 $cond = array(
  'products_classifications.id_products'=>$id_products,
 );
 $this->db->where($cond);
 $this->db->group_by('classifications.id_classifications');
 $this->db->order_by('classifications.title');
 $query = $this->db->get();
 $classifications = $query->result_array();
 $results = array();
 foreach($classifications as $category) {
  array_push($results,$category['id_classifications']);
 }
 return $results;
}
	
public function insert_product_classifications($id_products,$classifications)
{
 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('products_classifications');
 $cond = array('id_products'=>$id_products);
 $this->db->where($cond);
 $this->db->order_by('products_classifications.id_products_classifications');
 $query = $this->db->get();
 $results = $query->result_array();
 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_classifications']);
  }
 }
 foreach($classifications as $category) {
  if(!in_array($category,$old_ids)) {
   array_push($new_ids,$category);
  }
 }
 if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$classifications)) {
    array_push($deleted_ids,$id);
   }
  }
 }
 if(!empty($deleted_ids)) {
  $q = 'DELETE FROM products_classifications WHERE id_classifications IN ('.implode(',',$deleted_ids).') AND id_products='.$id_products;
  $this->db->query($q);
 }
 if(!empty($new_ids)) {
  foreach($new_ids as $new_id) {
   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_classifications'] = $new_id;
   $data['id_products'] = $id_products;
   $this->db->insert('products_classifications',$data);
  }
 }
}



public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'products')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('products');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}