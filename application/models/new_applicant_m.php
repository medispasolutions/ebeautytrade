<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class New_applicant_m extends CI_Model
{
    public function get_supplier_application($id)
    {
        $this->db->where('id_supplier_application', $id);
        $this->db->where('deleted', 0);
        $query = $this->db->get('supplier_application');
        return $query->row_array();
    }
    
    /////////////////CHECKLIST CATEGORIES//////////////////////
    public function getRecords($cond, $limit = "", $offset = "")
    {
        $q = "";
        $q .= " select supplier_application.* ";
        $q .= " from supplier_application ";
        $q .= " where supplier_application.deleted=0 ";

        if (isset($cond["keyword"]) && !empty($cond["keyword"])) {
            $q .= " and UPPER(supplier_application.first_name) like '%" . strtoupper($cond["keyword"]) . "%' ";
        }
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }

        return $data;
    }
}