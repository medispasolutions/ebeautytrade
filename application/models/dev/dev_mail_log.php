<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property \CI_Session $session

 */
class dev_mail_log extends CI_Model
{

    protected $log;

    protected $sessionBag = "devmaillog";

    public function __construct()
    {
        parent::__construct();
        $this->log = $this->session->userdata($this->sessionBag);
        if (!is_array($this->log)) {
            $this->log = [];
        }
    }

    public function add($mailDetails)
    {
        /** @var CI_Email $mailer */
        $data = [
            'config' => $mailDetails['config'],
            'from' => $mailDetails['from'],
            'from_name' => $mailDetails['from_name'],
            'to' => $mailDetails['to'],
            'subject' => $mailDetails['subject'],
            'message' => $mailDetails['message'],
            'time' => time(),
        ];

        array_unshift($this->log, $data);
        $this->session->set_userdata($this->sessionBag, $this->log);
    }

    public function get()
    {
        foreach ($this->log as & $entry) {
            $entry['timeago'] = $this->timeAgo($entry['time']);
        }
        return $this->log;
    }

    public function clear()
    {
        $this->log = [];
        $this->session->set_userdata($this->sessionBag, $this->log);
    }

    protected function timeAgo($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'year',
            30 * 24 * 60 * 60  =>  'month',
            24 * 60 * 60  =>  'day',
            60 * 60  =>  'hour',
            60  =>  'minute',
            1  =>  'second'
        );
        $a_plural = array( 'year'   => 'years',
            'month'  => 'months',
            'day'    => 'days',
            'hour'   => 'hours',
            'minute' => 'minutes',
            'second' => 'seconds'
        );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
            }
        }
    }

}