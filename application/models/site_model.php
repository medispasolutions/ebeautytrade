<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Site_model extends CI_Model{
	
public function getBanners($section = '')
{
	$banners = array();
	$this->db->select('*');
	$this->db->from('banner');
	$cond = array(
		'deleted'=>0,
		'display'=>0
		//'version'=>$this->lang->lang()
	);
	$this->db->where($cond);
	if($section != '') {
		$this->db->like('pages',$section);
	}
	$this->db->order_by('sort_order');
	$query = $this->db->get();
	$results = $query->result_array();
	if(!empty($results)) {
		$i=0;
		foreach($results as $res) {
			$this->db->select('*');
			$this->db->from('banner_items');
			$cond1 = array(
				'deleted'=>0,
				'id_banner'=>$res['id_banner']
			);
			$this->db->where($cond1);
			$this->db->order_by('sort_order');
			$query = $this->db->get();
			$items = $query->result_array();
			$banners[$i] = $res;
			$banners[$i]['items'] = $items;
			$i++;
		}
	}
	return $banners;
}

/*************************************************************************/
function getFileGallery($table,$id,$types = array())
{
	if(!empty($types))
	$csv =  implode(',', array_map('add_quotes', $types));
	$q = "SELECT * FROM ".$table."_gallery WHERE id_".$table." = ".$id;
	if(!empty($types))
	$q .= " AND type IN (".$csv.")";
	$q .= " ORDER BY sort_order";
	//echo $q;exit;
	$query = $this->db->query($q);
	$results = $query->result_array();
	return $results;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////
public function getBrands($limit = '',$offset = '')
{
	$this->db->select("*");
	$this->db->from("brands");
	$this->db->where("deleted",0);
	$this->db->where("logo !=","");
	$this->db->order_by("sort_order");
	if($limit != "")
	$this->db->limit($limit,$offset);
	$query = $this->db->get();
	if($limit != "")
	$results = $query->result_array();
	else
	$results = $query->num_rows();
	return $results;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
public function getServices($cond =  array(),$limit = '',$offset = '')
{
	$this->db->select("*");
	$this->db->from("services");
	$this->db->where("deleted",0);
	if(!empty($cond))
	$htis->db->where($cond);
	$this->db->order_by("sort_order");
	if($limit != "")
	$this->db->limit($limit,$offset);
	$query = $this->db->get();
	if($limit != "")
	$results = $query->result_array();
	else
	$results = $query->num_rows();
	return $results;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
public function getHomeProducts()
{
	$this->db->select("*");
	$this->db->from("products");
	$this->db->where("deleted",0);
	$this->db->where("display_in_home",1);
	$this->db->order_by("sort_order");
	$this->db->limit(2,0);
	$query = $this->db->get();
	$results['row_one'] = $query->result_array();
		$this->db->select("*");
	$this->db->from("products");
	$this->db->where("deleted",0);
	$this->db->where("display_in_home",1);
	$this->db->order_by("sort_order");
	$this->db->limit(3,2);
	$query = $this->db->get();
	$results['row_two'] = $query->result_array();

	return $results;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
public function searchWebsite($keywords,$limit = "",$offset = "")
{
	$sqla="SELECT concat(id_branches,'s:branches','s:Branches') as id, title_url ,title, created_date , description FROM branches where deleted = 0 ";
	$sqla.="AND (UPPER(branches.title) like '%".strtoupper($keywords)."%' OR UPPER(branches.description) like '%".strtoupper($keywords)."%') ";	
	
	$sqla.=" UNION (SELECT concat(id_brands,'s:brands','s:Brands') as id, title_url , title, created_date , meta_description AS description FROM brands where deleted = 0 ";
	$sqla.="AND (UPPER(brands.title) like '%".strtoupper($keywords)."%' OR UPPER(brands.meta_description) like '%".strtoupper($keywords)."%')) ";
	
	$sqla.=" UNION (SELECT concat(id_categories,'s:categories','s:Categories') as id, title_url , title, created_date , meta_description AS description FROM categories where deleted = 0 ";
	$sqla.="AND (UPPER(categories.title) like '%".strtoupper($keywords)."%' OR UPPER(categories.meta_description) like '%".strtoupper($keywords)."%')) ";
	
	$sqla.=" UNION (SELECT concat(id_categories_sub,'s:categories_sub','s:Categories Sub') as id, title_url , title, created_date , meta_description AS description FROM categories_sub where deleted = 0 ";
	$sqla.="AND (UPPER(categories_sub.title) like '%".strtoupper($keywords)."%' OR UPPER(categories_sub.meta_description) like '%".strtoupper($keywords)."%')) ";

	$sqla.=" UNION (SELECT concat(id_dynamic_pages,'s:dynamic_pages','s:Dynamic Pages') as id, title_url , title, created_date , description  FROM dynamic_pages where deleted = 0 ";
	$sqla.="AND (UPPER(dynamic_pages.title) like '%".strtoupper($keywords)."%' OR UPPER(dynamic_pages.description) like '%".strtoupper($keywords)."%')) ";
	
	$sqla.="UNION (SELECT concat(id_products,'s:products','s:Products') as id, title_url , title, created_date , brief AS description  FROM products where deleted = 0 AND status = 0 ";
	$sqla.="AND (UPPER(products.title) like '%".strtoupper($keywords)."%' OR UPPER(products.brief) like '%".strtoupper($keywords)."%') )";	
	
	$sqla .= " ORDER BY created_date DESC";
	if($limit != "") {
		$sqla .= " LIMIT ".$limit." OFFSET ".$offset;
	}

	$query = $this->db->query($sqla);
	if($limit != "") {
		$results = $query->result_array();
	}
	else {
		$results = $query->num_rows();
	}
	return $results;
}


}