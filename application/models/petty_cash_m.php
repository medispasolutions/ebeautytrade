<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Petty_cash_m extends CI_Model{

public function get_petty_cash($id){
$this->db->where('id_petty_cash',$id);
$this->db->where('deleted',0);
$query = $this->db->get('petty_cash');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  petty_cash.* ";
        $q .= " from  petty_cash ";
		$q .= " where petty_cash.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(petty_cash.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}