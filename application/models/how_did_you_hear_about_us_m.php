<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class How_did_you_hear_about_us_m extends CI_Model{

public function get_how_did_you_hear_about_us($id){
$this->db->where('id_how_did_you_hear_about_us',$id);
$this->db->where('deleted',0);
$query = $this->db->get('how_did_you_hear_about_us');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'how_did_you_hear_about_us')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('how_did_you_hear_about_us');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}