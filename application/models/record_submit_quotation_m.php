<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Record_submit_quotation_m extends CI_Model{

public function get_record_submit_quotation($id){
$this->db->where('id_record_submit_quotation',$id);
$this->db->where('deleted',0);
$query = $this->db->get('record_submit_quotation');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  record_submit_quotation.* ";
        $q .= " from  record_submit_quotation ";
		$q .= " where record_submit_quotation.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(record_submit_quotation.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}