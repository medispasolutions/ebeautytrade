<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Import_model extends CI_Model{
	
public function import($section,$arr)
{
	$data = array();
	if($section == "products")
	$data = $this->importImages($arr);
	
	if($section == "uploadProducts")
	$data = $this->importProducts_admin($arr);
	
		if($section == "importImages")
	$data = $this->importImages($arr);
	
	if($section == "importWhatsNews")
	$data = $this->importWhatsNews($arr);
		
	if($section == "updataStockStatus")
	$data = $this->updataStockStatus($arr);
	
	if($section == "categories2")
	$data = $this->importProductsCategories($arr);
	
	if($section == "classifications2")
	$data = $this->importProductsClassifications($arr);
	return $data;
}


/////////////////////////////////////////////////////////////////////////
private function updataStockStatus($arr)
{


	$arrayCount = count($arr);
	
	for($i=0;$i<=$arrayCount;$i++){
		
		$valid = 0;
		
		$sku = trim($arr[$i]["F"]);
		$status = trim($arr[$i]["BT"]);
		$product=$this->fct->getonerecord('products',array('sku'=>$sku));
	
		// check unit
		$_data['availability']=$status;
		$cond["id_products"]=$product['id_products'];
		$this->db->where($cond);
	    $this->db->update('products',$_data); 
}
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
private function importImages($arr)
{


	$arrayCount = count($arr);
	
	for($i=6000;$i<=$arrayCount;$i++){
				if($i<6500){
		$valid = 0;
		$sku = trim($arr[$i]["A"]);
		$image = trim($arr[$i]["B"]);
		$product=$this->fct->getonerecord('products',array('sku'=>$sku));
	
		// check unit

echo 'uploading' ;
$image_path=$image;
$image_arr=explode("/",$image);

$image_cc=count($image_arr);
$col=$image_cc-1;

$image_name=$image_arr[$col];


	if(!empty($image_name) && file_exists('./product/product'.$image_path) && !file_exists('./uploads/products/gallery/2100x2100/'.$image_name) && isset($product['id_products'])){
	
		copy('./product/product'.$image_path, './uploads/products/gallery/'.$image_name);
		echo $image_name;
		$this->fct->createthumb($image_name,"products/gallery","854x854,500x500,295x295,196x196,120x120,65x65,2100x2100");
		$_data["image"]=$image_name;
		$_data["created_date"]=date("Y-m-d h:i:s");
		$_data["id_products"]=$product['id_products'];
	    $this->db->insert('products_gallery',$_data); 
	
		}}else{
			break;}
	
	
		echo 'done' ;
	
		//print_r($p);exit;
	}
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
private function importProductsClassifications($arr)
{
		$q  = "";
        $q .= " select  products_categories.* ";
        $q .= " from categories ";
        $q .= " join products_categories on products_categories.id_categories = categories.id_categories ";
        $q .= 'where categories.title="Woman" ';
		$q .=' OR categories.title="Unisex" ';
		$q .=' OR categories.title="Man" ';
		$q .=' OR categories.title="Lifestyle" ';
	
		$query = $this->db->query($q);
		$results = $query->result_array();
	   foreach($results as $res){
		  $cond['id_categories']=$res['id_categories'];
		  $this->db->delete('categories',$cond);
		}
}

/////////////////////////////////////////////////////////////////////////
private function importProductsCategories($arr)
{


	$arrayCount = count($arr);
	
	for($i=923;$i<=$arrayCount;$i++){
		$valid = 0;
		$id_categories = trim($arr[$i]["E"]);
		$sku = trim($arr[$i]["F"]);
		$product=$this->fct->getonerecord('products',array('sku'=>$sku));
		$cats_arr=explode(",",$id_categories);
		// check unit
	   foreach($cats_arr as $category){
		   $check=$this->fct->getonerecord('products_categories',array('id_categories'=>$category,'id_products'=>$product['id_products']));
		   if(empty($check)){
		   $p['created_date'] = date("Y-m-d H:i:s");
		   $p['id_categories'] = $category;
		   $p['id_products'] = $product['id_products'];
		   $this->db->insert("products_categories",$p);}
		   }
		//print_r($p);exit;
	}
	return TRUE;
}
/////////////////////////////////////////////////////////////////////////
private function importProducts($arr)
{
	$arrayCount = count($arr);
	//print '<pre>';print_r($arr);exit;
	for($i=3;$i<=$arrayCount;$i++){
		$valid = 0;
		$sku = trim($arr[$i]["F"]);//done
		$has_options = trim($arr[$i]["G"]);//done
		$title = trim($arr[$i]["H"]);//done
		$status = trim($arr[$i]["Z"]);//done
		$visibility = trim($arr[$i]["AB"]);//done
		$color_options = trim($arr[$i]["AD"]);//done
		$size_options = trim($arr[$i]["AE"]);//done
		$designer = trim($arr[$i]["AF"]);
		$list_price = trim($arr[$i]["AH"]);//done
		$price = trim($arr[$i]["AI"]);//done
		$weight = trim($arr[$i]["AJ"]);//done
		$description = trim($arr[$i]["AL"]);//done!
		$short_description= trim($arr[$i]["AM"]);//done!
		$meta_keyword= trim($arr[$i]["AN"]);//done
		$meta_title= trim($arr[$i]["I"]);//done
		$meta_description= trim($arr[$i]["J"]);//done
		$custom_layout_update= trim($arr[$i]["AO"]);//done
		$size_and_fit= trim($arr[$i]["AP"]);//done
		$washing_information= trim($arr[$i]["AQ"]);//done
		$expire_date= trim($arr[$i]["AR"]);//done
		$qty = trim($arr[$i]["AX"]);//done
		


		// check Designers
		$check_designer = $this->fct->getonerow("designers",array("UPPER(title)"=>strtoupper($designer)));
		if(empty($check_designer)) {
			$i_dsgn['created_date'] = date("Y-m-d H:i:s");
			$i_dsgn['meta_title'] = $designer;
			$i_dsgn['meta_keywords'] = $designer;
			$i_dsgn['meta_description'] = $designer;
			$i_dsgn['title'] = $designer;
			$i_dsgn['title_url'] = $this->fct->cleanURL("designers",$designer);
			$this->db->insert("designers",$i_dsgn);
			$id_designers = $this->db->insert_id();
		}
		else {
			$id_designers = $check_designer['id_designers'];
		}
		
		
		// check COLOR
		$sumb_color_options=explode("/",$color_options);
		$k=0;
		$color_arr=array();
foreach($sumb_color_options as $key => $color_optionn){
		$check_color_option = $this->fct->getonerow("attribute_options",array("UPPER(title)"=>strtoupper($color_optionn)));
		if(empty($check_color_option)) {
			$i_color['created_date'] = date("Y-m-d H:i:s");
			$i_color['meta_title'] = $color_optionn;
			$i_color['meta_keywords'] = $color_optionn;
			$i_color['meta_description'] = $color_optionn;
			$i_color['title'] = $color_optionn;
			$i_color['id_attributes'] = 2;
			$i_color['title'] = $color_optionn;
			$i_color['option'] = $color_optionn;
			$i_color['title_url'] = $this->fct->cleanURL("attribute_options",$color_optionn);
			$this->db->insert("attribute_options",$i_color);
			$id_color_option = $this->db->insert_id();
		}
		else {
			$id_color_option = $check_color_option['id_attribute_options'];
		}
		$color_arr[$k]=$id_color_option;
		$k++;
		}
		
		// check SIZE
		$sumb_size_options=explode("/",$size_options);
		$k=0;
		$size_arr=array();
foreach($sumb_size_options as $key => $size_optionn){
		$check_size_option = $this->fct->getonerow("attribute_options",array("UPPER(title)"=>strtoupper($size_optionn)));
		if(empty($check_size_option)) {
			$i_size['created_date'] = date("Y-m-d H:i:s");
			$i_size['meta_title'] = $size_optionn;
			$i_size['meta_keywords'] = $size_optionn;
			$i_size['meta_description'] = $size_optionn;
			$i_size['title'] = $size_optionn;
			$i_size['id_attributes'] = 1;
			$i_size['title'] = $size_optionn;
			$i_size['option'] = $size_optionn;
			$i_size['title_url'] = $this->fct->cleanURL("attribute_options",$size_optionn);
			$this->db->insert("attribute_options",$i_size);
			$id_size_option = $this->db->insert_id();
		}
		else {
			$id_size_option = $check_size_option['id_attribute_options'];
		}
		$size_arr[$k]=$id_size_option;
		$k++;
		}
		
		// INSERT PRODUCT
	
		$p['title'] = $title;
		$p['weight'] = $weight;
		$p['id_designers'] = $id_designers;
		$p['meta_title'] = $meta_title;
		$p['meta_keywords'] = $meta_keyword;
		$p['meta_description'] = $meta_description;
		$p['title_url'] = $this->fct->cleanURL("products",$title);
		$p['sku'] = $sku;
		$p['list_price'] = $list_price;
		$p['discount_expiration'] = $expire_date;
		$p['discount'] = ($price*100)/$list_price;
		$p['price'] = $price;
		$p['has_options'] = $has_options;
		$p['quantity'] = $qty;
		$p['washing_instructions'] = $description;
		$p['size_and_fit'] = $size_and_fit;
		$p['editors_notes_and_details'] = $qty;
	    $p['washing_information'] = $short_description;
	    $p['custom_layout_update'] = $custom_layout_update;
		if($status=="Enabled"){
		$p['availability'] = 1;}else{
		$p['availability'] = 0;	}
		
		if($visibility=="Not Visible Individually"){
		$p['status'] = 0;}else{
		$p['availability'] = 1;	}
		//print_r($p);exit;
		$this->db->insert("products",$p);
		$id_product = $this->db->insert_id();
		
		// INSERT COLORS
		foreach($color_arr as  $color){
		$color_product['created_date'] = date("Y-m-d H:i:s");
		$color_product['id_products']=$id_product;
		$color_product['id_attribute_options']=$color;
		$color_product['id_attributes']=2;
		$this->db->insert("product_options",$color_product);
	}
	
			// INSERT Sizes
	foreach($size_arr as  $size){
		$size_product['created_date'] = date("Y-m-d H:i:s");
		$size_product['id_products']=$id_product;
		$size_product['id_attribute_options']=$size;
		$size_product['id_attributes']=1;
		$this->db->insert("product_options",$size_product);
	}
		
	}
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
private function importProducts_admin($arr)
{
	

	$arrayCount = count($arr);
	//print '<pre>';print_r($arr);exit;
	for($i=2;$i<=$arrayCount;$i++){
		$valid = 0;


		$title = $arr[$i]["A"];	
	
		$sku = $arr[$i]["B"];
		
		$list_price = $arr[$i]["C"];
		$discount = $arr[$i]["D"];
		$discount_expiration = $arr[$i]["E"];
		$price = $arr[$i]["F"];
		$retail_price = $arr[$i]["G"];
		$general_miles = $arr[$i]["H"];
		$redeem_miles = $arr[$i]["I"];
		$new_arrival =$arr[$i]["J"];
		$exportable = $arr[$i]["K"];
		$height = $arr[$i]["L"];
		$length = $arr[$i]["M"];
		$width = $arr[$i]["N"];
		$weight = $arr[$i]["O"];
		$brief = trim($arr[$i]["P"]);
		$created_date = date("Y-m-d h:i:s");;

		//INSERT PRODUCT
		$P['title']=$title;	
		$P['sku']=$sku;
		$P['list_price']=$list_price;
		$P['discount']=$discount;
	    $P['discount_expiration']=$discount_expiration;
		$P['price']=$price;
		$P['retail_price']=$retail_price;
		$P['general_miles']=$general_miles;
		$P['redeem_miles']=$redeem_miles;
		$P['set_as_new']=$new_arrival;
		$P['set_as_clearance']=$exportable;
		$P['height']=$height;
	    $P['length']=$length;
		$P['width']=$width;
		$P['weight']=$weight;
		$P['brief']=$brief;
		$P['created_date']=$created_date;

		$this->db->insert("products",$P);
		$id_product = $this->db->insert_id();

}
return TRUE;
}
/////////////////////////////////////////////////////////////////////////
private function importProducts_old($arr)
{
	$arrayCount = count($arr);
	//print '<pre>';print_r($arr);exit;
	for($i=3;$i<=$arrayCount;$i++){
		$valid = 0;
		$sku = trim($arr[$i]["A"]);
		$title = trim($arr[$i]["B"]);
		$unit = trim($arr[$i]["C"]);
		$qty = trim($arr[$i]["D"]);
		$price = trim($arr[$i]["F"]);
		$brand = trim($arr[$i]["G"]);
		$material = trim($arr[$i]["H"]);
		$category = trim($arr[$i]["I"]);
		$packing = trim($arr[$i]["J"]);
		// check material
		$check_material = $this->fct->getonerow("categories",array("UPPER(title)"=>strtoupper($material)));
		if(empty($check_material)) {
			$i_mat['created_date'] = date("Y-m-d H:i:s");
			$i_mat['meta_title'] = $material;
			$i_mat['meta_keywords'] = $material;
			$i_mat['meta_description'] = $material;
			$i_mat['title'] = $material;
			$i_mat['title_url'] = $this->fct->cleanURL("categories",url_title($material));
			$this->db->insert("categories",$i_mat);
			$id_mat = $this->db->insert_id();
		}
		else {
			//print '<pre>';print_r($check_material);exit;
			$id_mat = $check_material['id_categories'];
		}
		// check category
		$check_category = $this->fct->getonerow("categories_sub",array("UPPER(title)"=>strtoupper($category)));
		if(empty($check_category)) {
			$i_cat['created_date'] = date("Y-m-d H:i:s");
			$i_cat['meta_title'] = $category;
			$i_cat['meta_keywords'] = $category;
			$i_cat['meta_description'] = $category;
			$i_cat['title'] = $category;
			$i_cat['title_url'] = $this->fct->cleanURL("categories_sub",url_title($category));
			$this->db->insert("categories_sub",$i_cat);
			$id_cat = $this->db->insert_id();
		}
		else {
			$id_cat = $check_category['id_categories_sub'];
		}
		// check cat mat rel
		$check_category = $this->fct->getonerow("categories_sub_categories",array("id_categories"=>$id_mat,"id_categories_sub"=>$id_cat));
		if(empty($check_category)) {
			$i_rel['id_categories'] = $id_mat;
			$i_rel['id_categories_sub'] = $id_cat;
			$this->db->insert("categories_sub_categories",$i_rel);
		}
		// check brand
		$check_brand = $this->fct->getonerow("brands",array("UPPER(title)"=>strtoupper($brand)));
		if(empty($check_brand)) {
			$i_brd['created_date'] = date("Y-m-d H:i:s");
			$i_brd['meta_title'] = $brand;
			$i_brd['meta_keywords'] = $brand;
			$i_brd['meta_description'] = $brand;
			$i_brd['title'] = $brand;
			$i_brd['title_url'] = $this->fct->cleanURL("brands",url_title($brand));
			$this->db->insert("brands",$i_brd);
			$id_brd = $this->db->insert_id();
		}
		else {
			$id_brd = $check_brand['id_brands'];
		}
		// check unit
		if(strtoupper($unit) == "PC" || strtoupper($unit) == "PCS") {
			$un = 1;
		}
		else {
			$un = 2;
		}
		$p['title'] = $title;
		$p['meta_title'] = $title;
		$p['meta_keywords'] = $title;
		$p['meta_description'] = $title;
		$p['title_url'] = $this->fct->cleanURL("products",url_title($brand));
		$p['sku'] = $sku;
		$p['id_units'] = $un;
		$p['id_brands'] = $id_brd;
		$p['id_categories'] = $id_mat;
		$p['id_categories_sub'] = $id_cat;
		$p['list_price'] = $price;
		$p['price'] = $price;
		$p['packing'] = $packing;
		//print_r($p);exit;
		$this->db->insert("products",$p);
	}
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
private function importCategories($arr)
{


	$arrayCount = count($arr);
	
	for($i=3;$i<=$arrayCount;$i++){
		$valid = 0;
		$id_category = trim($arr[$i]["A"]);
		$title = trim($arr[$i]["B"]);

		// check unit
	    $p['id_categories'] = $id_category;
		$p['title'] = $title;
		$p['meta_title'] = $title;
		$p['meta_keywords'] = $title;
		$p['meta_description'] = $title;
		$p['title_url'] = $this->fct->cleanURL("categories",$title);
		$p['created_date'] = date("Y-m-d H:i:s");

		//print_r($p);exit;
		$this->db->insert("categories",$p);
	}
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
private function importWhatsNews($arr)
{
		$q  = "";
        $q .= " select  products_categories.* ";
        $q .= " from categories ";
        $q .= " join products_categories on products_categories.id_categories = categories.id_categories ";
        $q .= 'where categories.deleted=0 ';
		$q .=' and products_categories.id_categories=403 ';
	
		$query = $this->db->query($q);
		$results = $query->result_array();
	   foreach($results as $res){
		  $data['set_as_new']=1;
		  $this->db->where("id_products",$res['id_products']);
	      $this->db->update('products',$data);
		}
}
/////////////////////////////////////////////////////////////////////////
}