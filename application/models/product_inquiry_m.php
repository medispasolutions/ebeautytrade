<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_inquiry_m extends CI_Model{

public function get_product_inquiry($id){
$this->db->where('id_product_inquiry',$id);
$this->db->where('deleted',0);
$query = $this->db->get('product_inquiry');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  product_inquiry.*, products.title as product_name, user.name ";
        $q .= " from  product_inquiry ";
		$q .= " join  user  on product_inquiry.id_user=user.id_user ";
		$q .= " join  products  on product_inquiry.id_products=products.id_products ";
		$q .= " where product_inquiry.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(products.title) like '%".strtoupper($cond["keyword"])."%' ";}
		 
		 	if(isset($cond["user"]) && !empty($cond["user"])){
		
		 $q .= " and UPPER(user.name) like '%".strtoupper($cond["user"])."%' ";}
		 
		
            $q .= "  order by id_product_inquiry desc";
     
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}