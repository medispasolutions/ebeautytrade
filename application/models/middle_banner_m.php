<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Middle_banner_m extends CI_Model{

public function get_middle_banner($id){
$this->db->where('id_middle_banner',$id);
$this->db->where('deleted',0);
$query = $this->db->get('middle_banner');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  middle_banner.* ";
        $q .= " from  middle_banner ";
		$q .= " where middle_banner.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(middle_banner.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}