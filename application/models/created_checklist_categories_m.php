<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Created_checklist_categories_m extends CI_Model{

public function get_created_checklist_categories($id){
$this->db->where('id_created_checklist_categories',$id);
$this->db->where('deleted',0);
$query = $this->db->get('created_checklist_categories');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  created_checklist_categories.* ";
        $q .= " from  created_checklist_categories ";
		$q .= " where created_checklist_categories.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(created_checklist_categories.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}