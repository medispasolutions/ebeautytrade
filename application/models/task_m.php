<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Task_m extends CI_Model{

public function get_task($id){
$this->db->where('id_task',$id);
$this->db->where('deleted',0);
$query = $this->db->get('task');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	$current_date=date("Y-m-d h:i:s");
	$c="";
	
	$c .= " and task.deleted=0 ";
	
			if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $c .= " and (UPPER(task.description) like '%".strtoupper($cond["keyword"])."%' ||  UPPER(task.location) like '%".strtoupper($cond["keyword"])."%')";}
		 	if(isset($cond["location"]) && !empty($cond["location"])){
		
		 $c .= " and UPPER(task.location) like '%".strtoupper($cond["location"])."%'";}
		 if(isset($cond["status"]) && !empty($cond["status"])){
		 $c .= " and task.status=".$cond['status'];}
		 
		if(isset($cond["priority"]) && !empty($cond["priority"])){
		 $c .= " and task.priority=".$cond['priority'];} 
		 
			if(isset($cond["category"]) && !empty($cond["category"])){
		 $c .= " and task.id_categories_task=".$cond['category'];} 
		 
		if(isset($cond["from_date"]) && !empty($cond["from_date"])){
	
		 $c .= " and task.date>='".$cond['from_date']."'";}  
		 
		if(isset($cond["to_date"]) && !empty($cond["to_date"])){
		 $c .= " and task.date<='".$cond['to_date']."'";} 
	
		$q = "";
        $q .= " select *, 1 AS p from task where date >= '".$current_date."'"; 
		$q .= " ".$c." ";
		$q .="union(select *, 2 AS p from task WHERE date < '".$current_date."'";
		$q .= " ".$c." ";
		$q .= " ) ";


		   
		$q .= "ORDER BY p, date ASC";
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}