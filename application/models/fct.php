<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Fct extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('phpexcel');
    }

    function write_file($path, $data)
    {
        if (!write_file($path, $data)) {
            echo 'Unable to write the file';
        } else {
//echo 'File written!';
        }
    }

    function sendEmail($data)
    {

        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => _getenv('MAILER_HOST'),
            'smtp_port' => (int)_getenv('MAILER_PORT'),
            'smtp_user' => _getenv('MAILER_USER'), // change it to yours
            'smtp_pass' => _getenv('MAILER_PASS'), //change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
        );

        if (isset($data['from_email'])) {
            $from = $data['from_email'];
        } else {
            $from = _getenv('MAILER_DEFAULT_FROM');
        }

        if (isset($data['to_email'])) {
            $to = $data['to_email'];
        } else {
            $to = _getenv('MAILER_DEFAULT_TO');
        }

        $subject = $data['subject'];
        $msg = $data['message'];

        // get admin email
        $this->email->clear(TRUE);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        if (isset($data['from_name'])) {
            $this->email->from($from, $data['from_name']);
        } else {
            $this->email->from($from);
        }
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($msg);

        if (_getenv('MAILER_MOCKING') == 1) {
            $this->load->model('dev/dev_mail_log');
            $this->dev_mail_log->add([
                'config' => $config,
                'from' => $from,
                'from_name' => $data['from_name'],
                'to' => $to,
                'subject' => $subject,
                'message' => $msg,
            ]);
            return true;
        }

        return $this->email->send();
    }

    public function getMessagesType()
    {

        $q = " select  contactform.* ";
        $q .= " from  contactform ";
        $q .= " where contactform.deleted=0 and contactform.type!='0' ";
        $q .= " group by contactform.type    ";

        $query = $this->db->query($q);

        $results = $query->result_array();

        return $results;
    }

    public function getRecords($table, $cond, $limit = "", $offset = "")
    {
        $q = "";
        $q .= " select  " . $table . ".* ";
        $q .= " from  " . $table . " ";
        $q .= " where " . $table . ".deleted=0 ";

        if (isset($cond["keyword"]) && !empty($cond["keyword"])) {

            $q .= " and UPPER(" . $table . ".title_ar) like '%" . strtoupper($cond["keyword"]) . "%' ";
            unset($cond["keyword"]);
        }

        if (isset($cond['type'])) {
            $q .= " and type=" . $cond['type'];
        }

        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }

        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }

        return $data;
    }

    public function getNotifications($cond, $limit = '', $offset = '', $order = 'products.sort_order', $desc = "desc")
    {

        $sqla = "SELECT 'product' as type, products.title as title, products.id_products as id, products.updated_date as updated_date FROM products ";
        if (isset($cond['category']) || isset($cond['categories'])) {
            $sqla .= "  join products_categories on products_categories.id_products = products.id_products ";
        }
        $sqla .= " where products.deleted=0  ";

        if (isset($cond['id_brands']) && !empty($cond['id_brands'])) {
            $sqla .= " and products.id_brands=" . $cond['id_brands'];
        }

        if (isset($cond['id_supplier']) && !empty($cond['id_supplier'])) {
            $sqla .= " and products.id_user=" . $cond['id_supplier'];
        }

        if (isset($cond['threshold']) && !empty($cond['threshold'])) {
            $sqla .= " and products.threshold_flag=" . $cond['threshold'];
        }

        if (isset($cond['from_date']) && !empty($cond['from_date'])) {
            $sqla .= " and products.created_date>='" . $cond['from_date'] . "'";
        }

        if (isset($cond['to_date']) && !empty($cond['to_date'])) {
            $sqla .= " and products.created_date<='" . $cond['to_date'] . "'";
        }

        if (isset($cond['category']) && !empty($cond['category'])) {
            $sqla .= " and products_categories.id_categories=" . $cond['category'];
        }
        if (isset($cond['sku']) && !empty($cond['sku'])) {
            $sqla .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['product_name']) && !empty($cond['product_name'])) {
            $sqla .= " and UPPER(products.title) like '%" . strtoupper($cond['product_name']) . "%' ";
        }

        if (isset($cond['sales']) && $cond['sales'] == 1) {
            $sqla .= ' AND products.discount_status=1 ';
        }

        $sqla .= " group by products.id_products ";

        $sqla .= " UNION (SELECT 'stock' as type, products.title as title, products_stock.id_products_stock as id, products_stock.updated_date as created_date FROM  products_stock join products on products.id_products=products_stock.id_products  ";
        if (isset($cond['category']) || isset($cond['categories'])) {
            $sqla .= "  join products_categories on products_categories.id_products = products.id_products ";
        }
        $sqla .= "where  products.deleted=0 and products_stock.deleted=0 and products_stock.status=1 ";

        if (isset($cond['id_brands']) && !empty($cond['id_brands'])) {
            $sqla .= " and products.id_brands=" . $cond['id_brands'];
        }

        if (isset($cond['threshold']) && !empty($cond['threshold'])) {
            $sqla .= " and products_stock.threshold_flag=" . $cond['threshold'];
        }


        if (isset($cond['from_date']) && !empty($cond['from_date'])) {
            $sqla .= " and products_stock.created_date>='" . $cond['from_date'] . "'";
        }

        if (isset($cond['to_date']) && !empty($cond['to_date'])) {
            $sqla .= " and products_stock.created_date<='" . $cond['to_date'] . "'";
        }

        if (isset($cond['category']) && !empty($cond['category'])) {
            $sqla .= " and products_categories.id_categories=" . $cond['category'];
        }
        if (isset($cond['sku'])) {
            $sqla .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['product_name'])) {

            $sqla .= " and UPPER(products.title) like '%" . strtoupper($cond['product_name']) . "%' ";
        }

        if (isset($cond['sales'])) {

            $sqla .= ' AND products_stock.discount_status=' . $cond['sales'];
        }
        $sqla .= " group by products_stock.id_products_stock )  ";


        $sqla .= " UNION (SELECT 'delivery_order' as type, delivery_order.id_role as title, delivery_order.id_delivery_order as id, delivery_order.created_date as created_date FROM  delivery_order ";

        $sqla .= "where  delivery_order.status =2 and delivery_order.deleted=0  ";


        $sqla .= " group by delivery_order.id_delivery_order )  ";
        $sqla .= " ORDER BY updated_date DESC";
        if ($limit != "") {

            $sqla .= " LIMIT " . $limit . " OFFSET " . $offset;

        }


        //echo $sqla;exit;
        $query = $this->db->query($sqla);
        if ($limit != "") {
            $results = $query->result_array();

        } else {
            return $query->num_rows();
        }


        return $results;
    }


    public function insert_user_license($id_user)
    {
        $trading_license = $this->input->post('trading_license');
        $trading_license_name = $this->input->post('trading_license_name');
        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from('trading_license');
        $cond = array('id_user' => $id_user);
        $this->db->where($cond);
        $this->db->order_by('trading_license.id_trading_license');
        $query = $this->db->get();
        $results = $query->result_array();
        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_trading_license']);
            }
        }
        if (isset($trading_license) && !empty($trading_license)) {
            foreach ($trading_license as $key => $val) {
                if (!in_array($val, $old_ids)) {
                    array_push($new_ids, $val);
                }
            }
        }

        $deleted_ids = array();
        if (!empty($old_ids)) {
            foreach ($old_ids as $id) {
                if (!in_array($id, $trading_license)) {
                    array_push($deleted_ids, $id);
                }
            }
        }

        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM trading_license WHERE id_user = ' . $id_user . ' AND id_trading_license IN (' . implode(',', $deleted_ids) . ')';
            $this->db->query($q);
        }
        if (isset($trading_license) && !empty($trading_license)) {
            foreach ($trading_license as $key => $info) {
                $_data["file"] = "";

                if ($info != "") {
                    $cond_file = array("id_trading_license" => $info);
                    $_data["file"] = $this->fct->getonecell("trading_license", "file", $cond_file);
                }
                if (!empty($_FILES["trading_license_" . $info]["name"])) {


                    if ($info != "") {
                        $cond_file = array("id_trading_license" => $info);
                        $old_file = $this->fct->getonecell("trading_license", "file", $cond_file);
                        if (!empty($old_file))
                            unlink("./uploads/trading_license/" . $old_file);
                    }
                    $file1 = $this->fct->uploadImage("trading_license_" . $info, "trading_license");
                    $_data["file"] = $file1;
                }
            }


            $_data["id_user"] = $id_user;
            if (!in_array($info, $old_ids)) {
                if ($_data["file"] != "") {
                    $this->db->insert('trading_license', $_data);
                }
            } else {
                if ($_data["file"] != "") {
                    $this->db->where(array('id_trading_license' => $info));
                    $this->db->update('trading_license', $_data);
                } else {
                    if (!isset($trading_license_name[$key])) {
                        $this->db->delete('trading_license', array('id_trading_license' => $info));
                    }
                }
            }
        }
    }

    function changeMonethToArabic($mon)
    {
        $month = '';
        switch ($mon) {
            case 'January';
                $month = 'كانون الثاني';
                break;
            case 'Feb';
                $month = 'شباط';
                break;
            case 'Mar';
                $month = 'آذار';
                break;
            case 'Apr';
                $month = 'نيسان';
                break;
            case 'May';
                $month = 'أيار';
                break;
            case 'Jun';
                $month = 'حزيران';
                break;
            case 'Jul';
                $month = 'تموز';
                break;
            case 'Aug';
                $month = 'آب';
                break;
            case 'Sep';
                $month = 'أيلول';
                break;
            case 'Oct';
                $month = 'تشرين الأول';
                break;
            case 'Nov';
                $month = 'تشرين الثاني';
                break;
            case 'Dec';
                $month = 'كانون الأول';
                break;
        }
        return $month;
    }


///////////////////////////////////////////////////////////////////////////////////////
    public function insertNewLog($insert)
    {

        $userData = $this->ecommerce_model->getUserInfo();

        if (!empty($userData)) {
            $insert['created_date'] = date('Y-m-d H:i:s');
            $insert['id_user'] = $userData['id_user'];
            $insert['name'] = $userData['name'];
            $this->db->insert('tracing', $insert);
        }
    }


///////////////////////////////////////////////////////////////////////////////////////
    public function getTracing($cond, $limit = '', $offset = '')
    {


        $q = "";
        $q .= " select  tracing.*, user.name ";
        $q .= " from tracing ";
        $q .= " join user on tracing.id_user = user.id_user ";
        $q .= 'where tracing.deleted=0 ';

        if (isset($cond['keywords'])) {
            $q .= ' and (user.name like "%' . $cond['keywords'] . '%" or tracing.section like "%' . $cond['keywords'] . '%")';
        }

        if (isset($cond['id_user'])) {
            $q .= ' and user.id_user=' . $cond['id_user'];
        }

        if (isset($cond['section'])) {
            $q .= ' and tracing.section=' . $cond['section'];
        }


        $q .= "  group by tracing.id_tracing ";
        $q .= "  order by tracing.created_date desc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
        $query = $this->db->query($q);
        if ($limit == "") {
            $data = $query->num_rows();
        } else {
            $data = $query->result_array();
        }

        return $data;
    }

    function getRate($arr, $with_cur = FALSE)
    {
        if ($arr['id_countries'] != 253) {
            $params = array(
                'ClientInfo' => array(
                    'AccountCountryCode' => 'AE',
                    'AccountEntity' => 'DXB',
                    'AccountNumber' => '54230',
                    'AccountPin' => '654154',
                    'UserName' => 'admin@medispasolutions.com',
                    'Password' => 'medispa966',
                    'Version' => 'v1.0'
                ),

                'Transaction' => array(
                    'Reference1' => $arr['order_id']
                ),

                'OriginAddress' => array(
                    'City' => $arr['OriginAddressCity'],
                    'CountryCode' => $arr['OriginAddressCountryCode']
                ),

                'DestinationAddress' => array(
                    'City' => $arr['DestinationAddressCity'],
                    'CountryCode' => $arr['DestinationAddressCountryCode']
                ),
                'ShipmentDetails' => array(
                    'PaymentType' => 'P',
                    'ProductGroup' => 'EXP',
                    'ProductType' => 'PPX',
                    'ActualWeight' => $arr['ActualWeight'],
                    'ChargeableWeight' => $arr['ChargeableWeight'],
                    'NumberOfPieces' => $arr['NumberOfPieces']
                )
            );
            /* print '<pre>'; print_r($params);exit;*/ //exit;

            if (isset($arr['postal_code']) && !empty($arr['postal_code'])) {
                if (isset($arr['state']) && !empty($arr['state'])) {
                    $params['DestinationAddress']['StateOrProvinceCode'] = $arr['state'];
                }
                $params['DestinationAddress']['PostCode'] = $arr['postal_code'];
            }

            $soapClient = new SoapClient('./aramex/aramex-rates-calculator-wsdl-live.wsdl', array('trace' => 1));

            $results = $soapClient->CalculateRate($params);

            $res = array();
            $res = 'error';
            $data['Notifications'] = "";
            if (isset($results->HasErrors)) {
                $r = $results->HasErrors;
                $HasErrors = $results->HasErrors;
                $data['HasErrors'] = $HasErrors;
            }
            if (isset($results->Notifications->Notification->Message)) {
                $data['Notifications'] = $results->Notifications->Notification->Message;
            }
            if (isset($results->TotalAmount->Value)) {
                if ($with_cur) {
                    $data['aramexCurrency'] = $results->TotalAmount->CurrencyCode;
                    $data['value'] = $results->TotalAmount->Value;

                } else {
                    $data['value'] = $results->TotalAmount->Value;
                }
            }
        } else {
            $data_shipping['net_discount_price'] = $arr['net_discount_price'];
            $data_shipping['weight'] = $arr['ActualWeight']['Value'];
            $data = $this->fct->getShippingByCountry($data_shipping);
        }

        return $data;
        //die();
    }

    public function getShippingByCountry($data)
    {

        $city = $data['city'];
        $city_u = strtoupper($city);
        $id_country = $data['id_countries'];
        $amount = $data['net_discount_price'];
        $weight = $data['weight'];
        $shipping = 0;


        //////////////////UAE///////////////
        if ($id_country == 253) {
            $thresholdAmount = 500;

            if ($amount < $thresholdAmount) {
                $shipping = 25;
            }
        }

        $result['aramexCurrency'] = $this->config->item('default_currency');
        $result['value'] = $shipping;
        $result['Notifications'] = '';
        $result['HasErrors'] = '';
        return $result;
    }

    /////////////////////////////////////GET PRODUCTS VISITS//////////////////////////////////////////////////
    public function getProductsVisits($cond, $limit = "", $offset = "")
    {

        $q = "";
        $q .= " select  products.id_products, products.title, products.visits ";
        $q .= " from products ";

        $q .= ' where products.deleted=0 and products.visits>0 ';

        if (isset($cond['id_user']) && !empty($cond['id_user'])) {
            $q .= " and products.id_user=" . $cond['id_user'];
        }

        $q .= "  order by products.visits desc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }

        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }


        return $data;
    }

/////////////////////////////////////////////////////////////////Credits Payment\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    function c_ni_payment_success()
    {
        if ($this->session->userdata('login_id') != "") {
            if (!isset($_REQUEST['responseParameter'])) redirect(route_to("user"));

            $data['lang'] = $this->lang->lang();

            $responseParameter = $_REQUEST['responseParameter'];
            $merchantId = $_REQUEST['merchantId'];

            $enc = MCRYPT_RIJNDAEL_128;
            $key = $this->config->item("payment_encryption_key");
            $mode = MCRYPT_MODE_CBC;
            $iv = $this->config->item("payment_iv");
            $rand = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
            $merchantId = $this->config->item("payment_merchant_id");
            $crypt = base64_decode($responseParameter);
            $padtext = mcrypt_decrypt($enc, base64_decode($key), $crypt, $mode, $iv);
            $pad = ord($padtext{strlen($padtext) - 1});
            if ($pad > strlen($padtext)) return false;
            if (strspn($padtext, $padtext{strlen($padtext) - 1}, strlen($padtext) - $pad) != $pad) {
                $text = "Error";
            }
            $text = substr($padtext, 0, -1 * $pad);
            $arr = explode("|", $text);
            //print '<pre>';print_r($arr);exit;
            if (!isset($arr[0]) || !isset($arr[1])) die("Error in response data, please contact the administrator.");
            $order_id = $arr[0];
            $ref_id = $arr[1];


            // set order as paidorderData
            $order_update['updated_date'] = date("Y-m-d H:i:s");
            $order_update['payment_date'] = date("Y-m-d H:i:s");
            $order_update['status'] = "paid";
            $order_update['gateway_response_id'] = $ref_id;
            $this->db->where("id_orders", $order_id);
            $this->db->update("orders", $order_update);
            $this->cart->destroy();
            $this->paymentSuccess($arr);

        } else {
            //$this->session->set_userdata("redirect_link",route_to("user/ni_payment_success"));
            $this->session->set_flashdata('error_message', lang('access_denied'));
            redirect(route_to('user/login'));
        }
    }

    public function load_ni_payment_form1($vars)
    {
        $order_id = $vars['id_orders'];
        $amount = $vars['amount_default_currency'];
        $currency = $vars['default_currency'];
        $url_success = str_replace("http:", "https:", route_to("user/ni_payment_success1"));
        $url_error = str_replace("http:", "https:", route_to("user/payment_error1/" . $order_id));
        $TransactionType = "01";
        $TransactionMode = "INTERNET";
        $PayModeType = "CC"; // (CC)-Credit Card, (DC)-Debit Card, (DD)-Direct Debit
        $CreditCardNumber = "";
        $CVV = "";
        $ExpiryMonth = "";
        $ExpiryYear = "";
        $CardType = "VISA";

        $BillToFirstName = $vars['billing']['first_name'];
        $BillToLastName = $vars['billing']['last_name'];
        $BillToStreet1 = $vars['billing']['street_one'];
        $BillToStreet2 = $vars['billing']['street_two'];
        $BillToCity = $vars['billing']['city'];
        $BillToState = $vars['billing']['state'];
        $BillToPostalCode = $vars['billing']['postal_code'];
        $BillToCountry = $vars['billing']['country']['cc_iso'];
        $BillToEmail = $vars['user']['email'];
        $GatewayID = "";
        $CustomerID = $vars['id_user'];

        $ShipToFirstName = "";
        $ShipToLastName = "";
        $ShipToStreet1 = "";
        $ShipToStreet2 = "";
        $ShipToState = "";
        $ShipToPostalCode = "";
        $ShipToCountry = "";
        $ShipToPhoneNumber1 = "";
        $ShipToPhoneNumber2 = "";
        $ShipToPhoneNumber3 = "";
        $ShipToMobileNumber = "";
        $TransactionSource = "";
        $ProductInfo = "";
        $IsUserLoggedIn = ""; // TRUE or FALSE
        $ItemTotal = "";
        $ItemCategory = "";
        $IgnoreValidationResult = "";
        $udf1 = "";
        $Udf2 = "";
        $Udf3 = "";
        $Udf4 = "";
        $Udf5 = "";
        // start encryption
        $enc = MCRYPT_RIJNDAEL_128;
        $key = $this->config->item("payment_encryption_key");
        $mode = MCRYPT_MODE_CBC;
        $iv = $this->config->item("payment_iv");
        $rand = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
        $merchantId = $this->config->item("payment_merchant_id");

        if (empty($BillToPostalCode)) $BillToPostalCode = 971;
        $text = $order_id . "|" . $currency . "|" . $amount . "|" . $url_success . "|" . $url_error . "|" . $TransactionType . "|" . $TransactionMode . "|||||||" . $BillToFirstName . "|" . $BillToLastName . "|" . $BillToStreet1 . "|" . $BillToStreet2 . "|" . $BillToCity . "|" . $BillToState . "|" . $BillToPostalCode . "|" . $BillToCountry . "|" . $BillToEmail . "|" . $GatewayID . "|" . $CustomerID . "||||||" . $ShipToFirstName . "|" . $ShipToLastName . "|" . $ShipToStreet1 . "|" . $ShipToStreet2 . "|" . $ShipToState . "|" . $ShipToPostalCode . "|" . $ShipToCountry . "|" . $ShipToPhoneNumber1 . "|" . $ShipToPhoneNumber2 . "|" . $ShipToPhoneNumber3 . "|" . $ShipToMobileNumber . "|" . $ProductInfo . "|" . $IsUserLoggedIn . "|" . $ItemCategory . "|" . $ItemTotal . "|" . $IgnoreValidationResult . "|" . $udf1 . "|" . $Udf2 . "|" . $Udf3 . "|" . $Udf4 . "|" . $Udf5 . "| ";
        $text1 = $text;


        //echo $text1;exit;

        $size = mcrypt_get_block_size($enc, $mode);
        $pad = $size - (strlen($text1) % $size);
        $padtext = $text1 . str_repeat(chr($pad), $pad);
        $crypt = mcrypt_encrypt($enc, base64_decode($key), $padtext, $mode, $iv);
        $requestParameter = base64_encode($crypt);

        //echo $text1.'<br />'.$requestParameter;exit;
        $urlll = $this->config->item("payment_post_url");

        //$urlll = route_to("user/ni_payment_success_debug/".$order_id);
        /*return '<form id="payNI" action="'.$urlll.'" method="post"><input type="hidden" name="requestParameter" value="'.$merchantId.'|'.$requestParameter.'"></form><script>$(document).ready(function(){$("#payNI").submit();});</script>';*/

        return '<form id="payNI" action="' . $urlll . '" method="post"><input type="hidden" name="requestParameter" value="' . $merchantId . '|' . $requestParameter . '"></form><script>$(document).ready(function(){$("#payNI").submit();});</script>';

    }


///////////////////////////////PAYMENT CHECKOUT///////////////////////////////

    public function load_ni_payment_form($vars)
    {
        $order_id = $vars['id_orders'];
        $amount = $vars['amount_default_currency'];
        $currency = $vars['default_currency'];
        $url_success = str_replace("http:", "https:", route_to("user/ni_payment_success"));
        $url_error = str_replace("http:", "https:", route_to("user/payment_error/" . $order_id));
        $TransactionType = "01";
        $TransactionMode = "INTERNET";
        $PayModeType = "CC"; // (CC)-Credit Card, (DC)-Debit Card, (DD)-Direct Debit
        $CreditCardNumber = "";
        $CVV = "";
        $ExpiryMonth = "";
        $ExpiryYear = "";
        $CardType = "VISA";

        $BillToFirstName = $vars['billing']['first_name'];
        $BillToLastName = $vars['billing']['last_name'];
        $BillToStreet1 = $vars['billing']['street_one'];
        $BillToStreet2 = $vars['billing']['street_two'];
        $BillToCity = $vars['billing']['city'];
        $BillToState = $vars['billing']['state'];
        $BillToPostalCode = $vars['billing']['postal_code'];
        $BillToCountry = $vars['billing']['country']['cc_iso'];
        $BillToEmail = $vars['user']['email'];
        $GatewayID = "";
        $CustomerID = $vars['id_user'];

        $ShipToFirstName = "";
        $ShipToLastName = "";
        $ShipToStreet1 = "";
        $ShipToStreet2 = "";
        $ShipToState = "";
        $ShipToPostalCode = "";
        $ShipToCountry = "";
        $ShipToPhoneNumber1 = "";
        $ShipToPhoneNumber2 = "";
        $ShipToPhoneNumber3 = "";
        $ShipToMobileNumber = "";
        $TransactionSource = "";
        $ProductInfo = "";
        $IsUserLoggedIn = ""; // TRUE or FALSE
        $ItemTotal = "";
        $ItemCategory = "";
        $IgnoreValidationResult = "";
        $udf1 = "";
        $Udf2 = "";
        $Udf3 = "";
        $Udf4 = "";
        $Udf5 = "";
        // start encryption
        $enc = MCRYPT_RIJNDAEL_128;
        $key = $this->config->item("payment_encryption_key");
        $mode = MCRYPT_MODE_CBC;
        $iv = $this->config->item("payment_iv");
        $rand = substr(hash('sha512', uniqid(rand(), true)), 0, 5);
        $merchantId = $this->config->item("payment_merchant_id");

        if (empty($BillToPostalCode)) $BillToPostalCode = 971;
        $text = $order_id . "|" . $currency . "|" . $amount . "|" . $url_success . "|" . $url_error . "|" . $TransactionType . "|" . $TransactionMode . "|||||||" . $BillToFirstName . "|" . $BillToLastName . "|" . $BillToStreet1 . "|" . $BillToStreet2 . "|" . $BillToCity . "|" . $BillToState . "|" . $BillToPostalCode . "|" . $BillToCountry . "|" . $BillToEmail . "|" . $GatewayID . "|" . $CustomerID . "||||||" . $ShipToFirstName . "|" . $ShipToLastName . "|" . $ShipToStreet1 . "|" . $ShipToStreet2 . "|" . $ShipToState . "|" . $ShipToPostalCode . "|" . $ShipToCountry . "|" . $ShipToPhoneNumber1 . "|" . $ShipToPhoneNumber2 . "|" . $ShipToPhoneNumber3 . "|" . $ShipToMobileNumber . "|" . $ProductInfo . "|" . $IsUserLoggedIn . "|" . $ItemCategory . "|" . $ItemTotal . "|" . $IgnoreValidationResult . "|" . $udf1 . "|" . $Udf2 . "|" . $Udf3 . "|" . $Udf4 . "|" . $Udf5 . "| ";
        $text1 = $text;


        //echo $text1;exit;

        $size = mcrypt_get_block_size($enc, $mode);
        $pad = $size - (strlen($text1) % $size);
        $padtext = $text1 . str_repeat(chr($pad), $pad);
        $crypt = mcrypt_encrypt($enc, base64_decode($key), $padtext, $mode, $iv);
        $requestParameter = base64_encode($crypt);

        //echo $text1.'<br />'.$requestParameter;exit;
        $urlll = $this->config->item("payment_post_url");

        //$urlll = route_to("user/ni_payment_success_debug/".$order_id);
        /*return '<form id="payNI" action="'.$urlll.'" method="post"><input type="hidden" name="requestParameter" value="'.$merchantId.'|'.$requestParameter.'"></form><script>$(document).ready(function(){$("#payNI").submit();});</script>';*/

        //return '<form id="payNI" action="' . $urlll . '" method="post"><input type="hidden" name="requestParameter" value="' . $merchantId . '|' . $requestParameter . '"></form><script>$(document).ready(function(){$("#payNI").submit();});</script>';
        
        $billingId = $vars['billing_id'];
        $shippingId = $vars['delivery_id'];
        
        return '<form id="payNI" action="https://www.ebeautytrade.com/OnlinePayment" method="post"><input type="hidden" name="requestParameter" value="' . $merchantId . '|' . $requestParameter . '"><input type="hidden" name="order" value="' . $order_id . '"><input type="hidden" name="billing" value="' . $billingId . '"><input type="hidden" name="shipping" value="' . $shippingId . '"><input type="hidden" name="amount" value="' . $amount . '"><input type="hidden" name="user" value="' . $CustomerID . '"></form><script>$(document).ready(function(){$("#payNI").submit();});</script>';

    }


/////////////////////////////////////////////////////////////////End\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function registerEmail($data)
    {
        $html = $this->load->view('emails/order', $data, true);
        echo $html;
        exit;
    }


    public function dateIfValid($date = "")
    {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date)) {
            return true;
        } else {
            return false;
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getAttributeOption($attribute, $options)
    {
        $this->db->select('orders.*,user.name,user.first_name,user.surname');
        $this->db->from('orders');
        $this->db->join('user', 'orders.id_user = user.id_user');
        /*if(isset($cond['user.name'])) {
		$this->db->like('UPPER(name)',strtoupper($cond['user.name']));
		unset($cond['user.name']);
	}*/
        if (isset($cond['user.first_name'])) {
            $this->db->like('UPPER(first_name)', strtoupper($cond['user.first_name']));
            unset($cond['user.first_name']);
        }
        if (isset($cond['user.surname'])) {
            $this->db->like('UPPER(surname)', strtoupper($cond['user.surname']));
            unset($cond['user.surname']);
        }
        if (!empty($cond)) {
            $this->db->where($cond);
        }
        $this->db->group_by('orders.id_orders');
        $this->db->order_by('id_orders DESC');
        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        if ($limit != '') {
            $result = $query->result_array();
        } else {
            $result = $query->num_rows();
        }
        return $result;
    }


///////////////////////////////////////////////////////////////////////////////////////
    public function getCourses($cond = array(), $limit = '', $offset = '')
    {
        $data = array();
        $this->db->select('courses_location.*, co');
        $this->db->from('products');
        //print_r($cond);exit;
        if (isset($cond['products_categories.id_categories'])) {
            //$this->db->join('products_categories','products_categories.id_products = products.id_products');
            $this->db->join('products_categories', 'products_categories.id_products = products.id_products');
        }


        if (isset($cond['product_options.id_attribute_options'])) {
            //$this->db->join('products_categories','products_categories.id_products = products.id_products');
            $this->db->join('product_options', 'products.id_products = product_options.id_products');

            $cond_arr = $cond['product_options.id_attribute_options'];
            unset($cond['product_options.id_attribute_options']);;
        }


        /*	if(isset($cond['brands.title_url'.getUrlFieldLanguage($this->lang->lang())])) {
		$this->db->join('brands','brands.id_brands = products.id_brands');
	}*/
        /*	if(isset($cond['categories_sub.title_url'.getUrlFieldLanguage($this->lang->lang())])) {
		$this->db->join('categories_sub','categories_sub.id_categories_sub = products.id_categories_sub');
	}*/
        if (isset($cond['products.title'])) {
            $this->db->like('UPPER(products.title' . getFieldLanguage() . ')', strtoupper($cond['products.title' . getFieldLanguage()]));
            unset($cond['products.title' . getFieldLanguage()]);
        }
        $cond['products.deleted'] = 0;
        //$cond['products.lang'] = $this->lang->lang();
        //print_r($cond);exit;

        $this->db->where($cond);

        if (isset($cond_arr)) {
            $this->db->where_in('product_options.id_attribute_options', $cond_arr);
        }

        $this->db->group_by('products.id_products');
        $this->db->order_by($order);
        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();


        if ($limit == '') {
            $data = $query->num_rows();
        } else {
            //echo $this->db->last_query(); exit;
            $results = $query->result_array();
            if (!empty($results)) {
                $j = 0;
                foreach ($results as $res) {
                    // get gallery
                    $this->db->select('*');
                    $this->db->from('products_gallery');
                    $cnd = array('id_products' => $res['id_products']);
                    $this->db->where($cnd);
                    $this->db->order_by('sort_order');
                    $query = $this->db->get();
                    $gallery = $query->result_array();
                    $data[$j] = $res;
                    $data[$j]['gallery'] = $gallery;
                    // get attributes
                    $attributes = $this->getProductAttributes($res['id_products']);
                    //print_r($attributes);
                    $data[$j]['attributes'] = $attributes;
                    // get stock
                    if (!empty($attributes)) {
                        $data[$j]['first_available_stock'] = $this->fct->getonerow('products_stock', array('status' => 1, 'id_products' => $data[$j]['id_products'], 'quantity !=' => 0));
                        if (empty($data[$j]['first_available_stock']))
                            $data[$j]['first_available_stock'] = $this->fct->getonerow('products_stock', array('status' => 1, 'id_products' => $data[$j]['id_products']));
                    }
                    $j++;
                }
            }
        }
        return $data;
    }

///////////////////////////////////////PRODUCTS/////////////////////////////////////////////////////////////////////
    public function getNotSelectedAttributes_byProduct($id_product)
    {
        $q = 'SELECT * FROM attributes WHERE deleted = 0 AND id_attributes NOT IN(SELECT id_attributes FROM product_attributes WHERE id_products = ' . $id_product . ')';
        $query = $this->db->query($q);
        $result = $query->result_array();
        return $result;
    }

    public function getSelectedAttributes_byProduct($id_product)
    {
        $q = 'SELECT attributes.*, product_attributes.id_product_attributes FROM product_attributes';
        $q .= ' JOIN attributes ON attributes.id_attributes = product_attributes.id_attributes';
        $q .= ' WHERE product_attributes.id_products = ' . $id_product;
        $q .= ' GROUP BY product_attributes.id_attributes';
        $q .= ' ORDER BY attributes.sort_order';
        $query = $this->db->query($q);
        $result = $query->result_array();
        return $result;
    }

    public function getOneProduct($product_url)
    {
        $data = array();
        $this->db->select('products.*');
        $this->db->from('products');
        if (is_numeric($product_url))
            $cond['products.id_products'] = $product_url;
        else
            $cond['products.title_url' . getUrlFieldLanguage($this->lang->lang())] = $product_url;
        $this->db->where($cond);
        //$this->db->group_by('products.id_products');
        $this->db->order_by('products.sort_order');
        $query = $this->db->get();
        /*echo $this->db->last_query();
	exit;*/
        $results = $query->row_array();
        $data = $results;
        if (!empty($results)) {
            $this->db->select('categories.*');
            $this->db->from('categories');
            //$this->db->join('products_categories','products_categories.id_categories = categories.id_categories');
            $cnd = array('id_categories' => $results['id_categories']);
            $this->db->where($cnd);
            //$this->db->order_by('categories.sort_order');
            $query = $this->db->get();
            $category = $query->row_array();
            $data['category'] = $category;
            // product sub category
            $this->db->select('categories_sub.*');
            $this->db->from('categories_sub');
            //$this->db->join('products_categories','products_categories.id_categories = categories.id_categories');
            $cnd = array('id_categories_sub' => $results['id_categories_sub']);
            $this->db->where($cnd);
            //$this->db->order_by('categories.sort_order');
            $query = $this->db->get();
            $sub_category = $query->row_array();
            $data['sub_category'] = $sub_category;


            // get gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $cnd = array('id_products' => $results['id_products']);
            $this->db->where($cnd);
            $this->db->order_by('sort_order');
            $query = $this->db->get();
            $gallery = $query->result_array();
            $data['gallery'] = $gallery;
            // get attributes
            $this->db->select('attributes.*, product_attributes.id_product_attributes');
            $this->db->from('attributes');
            $this->db->join('product_attributes', 'product_attributes.id_attributes = attributes.id_attributes');
            $cnd = array(
                'product_attributes.id_products' => $results['id_products']
            );
            $this->db->where($cnd);
            $this->db->group_by('attributes.id_attributes');
            $this->db->order_by('attributes.sort_order');
            $query = $this->db->get();
            $attributes = $query->result_array();
            $data['attributes'] = $attributes;
            if (!empty($attributes)) {
                $k = 0;
                foreach ($attributes as $attr) {
                    // get product attribute options
                    $this->db->select('attribute_options.*');
                    $this->db->from('attribute_options');
                    $this->db->join('product_options', 'product_options.id_attribute_options = attribute_options.id_attribute_options');

                    $cnd = array(
                        'product_options.id_products' => $results['id_products'],
                        'attribute_options.id_attributes' => $attr['id_attributes'],
                        'product_options.status' => 1
                    );
                    $this->db->where($cnd);
                    $this->db->group_by('attribute_options.id_attribute_options');
                    $this->db->order_by('attribute_options.sort_order');
                    $query = $this->db->get();
                    $options = $query->result_array();
                    $data['attributes'][$k]['options'] = $options;
                    $k++;
                }
                // get first available stock
                $data['first_available_stock'] = $this->fct->getonerow('products_stock', array('status' => 1, 'id_products' => $data['id_products'], 'quantity !=' => 0));
                if (empty($data['first_available_stock']))
                    $data['first_available_stock'] = $this->fct->getonerow('products_stock', array('status' => 1, 'id_products' => $data['id_products']));

            }
        }
        /*print '<pre>';
	print_r($data);
	exit;*/
        return $data;
    }

    public function getOneProduct_admin($cond)
    {

        $q = 'SELECT products.* FROM products';
        if (isset($cond['id_user'])) {
            $q .= ' Left JOIN brands ON brands.id_brands = products.id_brands';
        }
        $q .= ' WHERE products.id_products = ' . $cond['id_products'];
        if (isset($cond['id_user'])) {
            $q .= " and (brands.id_user=" . $cond['id_user'] . " || products.id_user=" . $cond['id_user'] . ")";
        }
        $q .= ' GROUP BY products.id_products';

        $query = $this->db->query($q);
        $result = $query->row_array();
        return $result;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getSubCategories($parent_categories = array())
    {
        $this->db->select('*');
        $this->db->from('categories');
        $this->db->where('deleted', 0);
        if (!empty($parent_categories)) {
            $this->db->where_in('id_parent', $parent_categories);
        } else {
            $this->db->where('id_parent !=', 0);
        }
        $this->db->order_by('sort_order');
        $query = $this->db->get();
        $results = $query->result_array();
        /*if($limit != '') {
		$results = $query->result_array();
		if(!empty($results)) {
			foreach($results as $key=>$res) {
				$this->db->select('*');
				$this->db->from('categories');
				$this->db->where('deleted',0);
				$this->db->where('id_parent',$res['id_categories']);
				$this->db->order_by('sort_order');
				$query = $this->db->get();
				$resss = $query->result_array();
				$results[$key]['sub_levels'] = $resss;
			}
		}
	}
	else {
		$results = $query->num_rows();
	}*/
        return $results;
    }

    public function select_product_sub_categories($id_products)
    {
        //echo $id_products;exit;
        $this->db->select('categories.*');
        $this->db->from('categories');
        $this->db->join('products_sub_categories', 'categories.id_categories = products_sub_categories.id_categories');
        $cond = array(
            'products_sub_categories.id_products' => $id_products,
        );
        $this->db->where($cond);
        $this->db->group_by('categories.id_categories');
        $this->db->order_by('categories.title');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $categories = $query->result_array();
        $results = array();
        foreach ($categories as $category) {
            array_push($results, $category['id_categories']);
        }
        return $results;
    }

    public function insert_product_sub_categories($id_products, $categories)
    {
        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from('products_sub_categories');
        $cond = array('id_products' => $id_products);
        $this->db->where($cond);
        $this->db->order_by('products_sub_categories.id_products_sub_categories');
        $query = $this->db->get();
        $results = $query->result_array();
        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_categories']);
            }
        }
        foreach ($categories as $category) {
            if (!in_array($category, $old_ids)) {
                array_push($new_ids, $category);
            }
        }
        if (!empty($old_ids)) {
            foreach ($old_ids as $id) {
                if (!in_array($id, $categories)) {
                    array_push($deleted_ids, $id);
                }
            }
        }
        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM products_sub_categories WHERE id_products = ' . $id_products . ' AND id_categories IN (' . implode(',', $deleted_ids) . ')';
            $this->db->query($q);
        }
        if (!empty($new_ids)) {
            foreach ($new_ids as $new_id) {
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['id_categories'] = $new_id;
                $data['id_products'] = $id_products;
                $this->db->insert('products_sub_categories', $data);
            }
        }
    }

    public function getOrder($order_id, $rand)
    {

        $data = array();
        $this->db->select('orders.*');
        $this->db->from('orders');
        $this->db->where('id_orders', $order_id);
        $this->db->where('rand', $rand);
        $query = $this->db->get();
        $order = $query->row_array();

        if (!empty($order)) {
            //get payment method
            $this->db->select('payment_methods.*');
            $this->db->from('payment_methods');
            $this->db->where('index', $order['payment_method']);
            $query = $this->db->get();
            $paymentmethod = $query->row_array();
            //get user
            $this->db->select('user.*');
            $this->db->from('user');
            $this->db->where('id_user', $order['id_user']);
            $query = $this->db->get();
            $user = $query->row_array();
            // get billing information
            $this->db->select('address_book.*');
            $this->db->from('address_book');
            $this->db->where('id_address_book', $order['billing_id']);
            $query = $this->db->get();
            $billing = $query->row_array();
            // get billing country
            if (!empty($billing['id_countries'])) {
                $this->db->select('countries.*');
                $this->db->from('countries');
                $this->db->where('id_countries', $billing['id_countries']);
                $query = $this->db->get();
                $country = $query->row_array();
                $billing['country'] = $country;
            }
            // get delivery information
            $this->db->select('address_book.*');
            $this->db->from('address_book');
            $this->db->where('id_address_book', $order['delivery_id']);
            $query = $this->db->get();
            $delivery = $query->row_array();
            if (empty($delivery['id_countries'])) {
                $delivery['id_countries'] = 0;
            }
            // get delivery country
            $this->db->select('countries.*');
            $this->db->from('countries');
            $this->db->where('id_countries', $delivery['id_countries']);
            $query = $this->db->get();
            $country = $query->row_array();
            $delivery['country'] = $country;
            // line items
            $line_items = array();
            $this->db->select('line_items.*');
            $this->db->from('line_items');
            $this->db->where('id_orders', $order['id_orders']);
            $query = $this->db->get();
            $items = $query->result_array();
            $i = 0;
            foreach ($items as $item) {
                $this->db->select('products.*');
                $this->db->from('products');
                $this->db->where('id_products', $item['id_products']);
                $query = $this->db->get();
                $res = $query->row_array();
                // product gallery
                $this->db->select('*');
                $this->db->from('products_gallery');
                $this->db->where('id_products', $item['id_products']);
                $query = $this->db->get();
                $gallery = $query->result_array();
                $line_items[$i] = $item;
                $line_items[$i]['product'] = $res;
                $line_items[$i]['product']['gallery'] = $gallery;
                $i++;
            }
            // set data
            $data = $order;
            $data['user'] = $user;
            $data['billing'] = $billing;
            $data['delivery'] = $delivery;
            $data['line_items'] = $line_items;
            $data['paymentmethod'] = $paymentmethod;
            return $data;
        } else {
            $data = "";
            return $data;
        }
        /*print '<pre>';
	print_r($data);
	exit;*/

    }

    public function uploadImage_texteditor($image, $path)
    {
        $config = array();
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['upload_path'] = './uploads/' . $path;
        $config['max_size'] = '8000';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($image, true)) {
            echo $this->upload_err = $this->upload->display_errors();
            return "";
        } else {
            $path = $this->upload->data();
            return $path['file_name'];
        }
    }

    public function select_many_to_many($field, $id, $table_1, $table_2, $order = 'title')
    {
        $this->db->select($table_1 . '.*');
        $this->db->from($table_1);
        $this->db->join($table_2, $table_1 . '.id_' . $table_1 . ' = ' . $table_2 . '.id_' . $table_1);
        $cond = array(
            $table_2 . '.' . $field => $id,
        );
        $this->db->where($cond);
        $this->db->group_by($table_1 . '.id_' . $table_1);
        $this->db->order_by($table_1 . '.' . $order);
        $query = $this->db->get();
        $arr = $query->result_array();
        $results = array();
        foreach ($arr as $ar) {
            array_push($results, $ar['id_' . $table_1]);
        }
        return $results;
    }

    public function insert_many_to_many($field, $id, $arr, $table_1, $table_2)
    {
        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from($table_2);
        $cond = array($field => $id);
        $this->db->where($cond);
        $this->db->order_by($table_2 . '.id_' . $table_2);
        $query = $this->db->get();
        $results = $query->result_array();
        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_' . $table_1]);
            }
        }
        foreach ($arr as $ar) {
            if (!in_array($ar, $old_ids)) {
                array_push($new_ids, $ar);
            }
        }
        if (!empty($old_ids)) {
            foreach ($old_ids as $id11) {
                if (!in_array($id11, $arr)) {
                    array_push($deleted_ids, $id11);
                }
            }
        }
        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM ' . $table_2 . ' WHERE ' . $field . ' = ' . $id . ' AND id_' . $table_1 . ' IN (' . implode(',', $deleted_ids) . ')';
            $this->db->query($q);
        }
        if (!empty($new_ids)) {
            foreach ($new_ids as $new_id) {
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['id_' . $table_1] = $new_id;
                $data[$field] = $id;
                $this->db->insert($table_2, $data);
                //echo $this->db->last_query();exit;
            }
        }
    }

    function getonecell($table, $select, $cond)
    {
        $this->db->where($cond);
        $query = $this->db->get($table);
        $res = $query->row_array();
        if (count($res) > 0)
            return $res[$select];
    }

    function getCartRow($id_row)
    {
        $dataTmp = $this->cart->contents();
        foreach ($dataTmp as $item) {
            if ($item['rowid'] == $id_row) {
                $result = $item;
                break;
            }
        }
        return $result;
    }

    function getonerow($table, $condition)
    {
        $this->db->where($condition);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    function getoneuser($condition)
    {
        $this->db->where($condition);
        $query = $this->db->get('user');
        return $query->row_array();
    }

    function getonerecord($table, $condition)
    {
        $this->db->where('deleted', 0);
        $this->db->where($condition);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }
    //do not allow user with id_roles=5 (supplier) to login from frontend
    function getonerecord_supplier($table, $condition)
    {
        $this->db->where('deleted', 0);
        $this->db->where('id_roles!=',5,FALSE);
        $this->db->where($condition);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }


    }

    function getAll_1($table, $order)
    {
//$this->db->where('deleted',0);
        $this->db->order_by($order);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    function getAll($table, $order)
    {
        $this->db->where('deleted', 0);
//if ($this->db->field_exists($order, $table)){
        $this->db->order_by($order);
//}
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAllHere($table, $order)
    {
        $this->db->where('deleted', 0);
        $this->db->where('id_locations', $this->session->userdata('country'));
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAllHere1($table, $order)
    {
        $this->db->where('deleted', 0);
        $this->db->where('active', 1);
        $this->db->where('id_locations', $this->session->userdata('country'));
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAll_orderdate($table)
    {
        $this->db->where('deleted', 0);
        $this->db->order_by('created_date', 'DESC');
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAll_orderdate1($table)
    {
        $this->db->where('deleted', 0);
        $this->db->where('active', 1);
        $this->db->where('id_locations', $this->session->userdata('country'));
        $this->db->order_by('created_date', 'DESC');
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAll_orderdate2($table)
    {
        $this->db->where('deleted', 0);
        $this->db->where('id_locations', $this->session->userdata('country'));
        $this->db->order_by('created_date', 'DESC');
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAll_cond($table, $order, $cond)
    {
        $this->db->where('deleted', 0);
        $this->db->where($cond);
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();

        } else {
            return array();
        }
    }


    function getAll_cond_1($table, $order, $cond, $desc = "asc")
    {
        $this->db->where($cond);
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order, $desc);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }


    function getAll_limit($table, $order, $des, $limit)
    {
        $this->db->where('deleted', 0);
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order, $des);
        }
        $query = $this->db->get($table, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAll_limit_cond($table, $order, $des, $limit, $cond)
    {
        $this->db->where('deleted', 0);
        $this->db->where($cond);
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order, $des);
        }
        $query = $this->db->get($table, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }


    function getAll_001($table, $order)
    {
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAll_desc($table, $order, $cond)
    {
        $this->db->where('deleted', 0);
        $this->db->where($cond);
        if ($this->db->field_exists($order, $table)) {
            $this->db->order_by($order, 'desc');
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getto_do_list()
    {
        $yesterday = date("Y-m-d", time() - 86400);
        $q = "SELECT * FROM `do_it` WHERE Deadline > '" . $yesterday . "' AND deleted = 0 ORDER BY  Deadline LIMIT 4";
        $query = $this->db->query($q);
        return $query->result_array();
    }

    function getto_do_list_completed()
    {
        $yesterday = date("Y-m-d", time() - 86400);
        $q = "SELECT * FROM `do_it` WHERE  deleted = 0  AND completed = 1 ORDER BY  Deadline LIMIT 4";
        $query = $this->db->query($q);
        return $query->result_array();
    }

    function module($id)
    {
        $this->db->where('id_module', $id);
        $query = $this->db->get('module');
        $res = $query->row_array();
        return $res["activate"];
    }


    function get_unreaded_emails($cond = array())
    {
        if (isset($cond) && !empty($cond)) {
            $this->db->where($cond);
        }
        $this->db->where('deleted', 0);
        $this->db->where('readed', 0);
        $this->db->order_by('created_date', 'DESC');
        $query = $this->db->get('contactform');
        return $query->result_array();
    }

    public function getMessages($cond = array(), $limit = "", $offset = "")
    {

        $q = "";
        $q .= " select  contactform.*";
        $q .= " from contactform ";
        if (checkIfSupplier() || isset($cond['user']) || isset($cond['brand'])) {
            $q .= "  join brands on contactform.id_brands = brands.id_brands ";
        }

        if ($cond['trading_name']) {
            $q .= "  join user on contactform.id_user = user.id_user ";
        }


        $q .= ' where contactform.deleted=0 ';
        if (checkIfSupplier()) {
            $user = $this->ecommerce_model->getUserInfo();
            $q .= ' and brands.deleted=0 ';
            $q .= ' and brands.id_user=' . $user['id_user'];
        }
        if (isset($cond['user'])) {
            $q .= ' and brands.id_user=' . $cond['user'];
        }

        if (isset($cond['brand'])) {

            $q .= ' and brands.id_brands=' . $cond['brand'];
        }

        if (isset($cond['readed'])) {
            $q .= ' and contactform.readed=' . $cond['readed'];
        }

        if (isset($cond['type'])) {
            $q .= ' and contactform.type="' . $cond['type'] . '"';
        }

        if (isset($cond['name'])) {
            $q .= ' and contactform.name like "%' . $cond['name'] . '%"';
        }

        if (isset($cond['email'])) {
            $q .= ' and contactform.email like "%' . $cond['email'] . '%"';
        }

        if ($cond['trading_name']) {
            $q .= ' and user.trading_name like "%' . $cond['trading_name'] . '%"';
        }


        if ($this->input->get("type") != "")
            $q .= ' AND type = "' . $this->input->get("type") . '"';
        if ($this->input->get("status") != "") {
            if ($this->input->get("status") == 'read')
                $q .= ' AND readed = 1';
            else
                $q .= ' AND readed = 0';
        }


        $q .= "  group by contactform.id ";
        $q .= "  order by contactform.created_date desc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }

        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }


        return $data;
    }

// Delete Directory and all related
    public static function deleteDirectory($dir)
    {
        if (!file_exists($dir)) return true;
        if (!is_dir($dir)) return unlink($dir);
        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') continue;
            if (!self::deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) return false;
        }
        return rmdir($dir);
    }

// upload file
    public function uploadCV($image, $path)
    {
        $config = array();
        $config['allowed_types'] = 'txt|pdf|docx|doc';
        $config['upload_path'] = './uploads/' . $path;
        $config['max_size'] = '555524048';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($image, true)) {
            echo $this->upload_err = $this->upload->display_errors();
            exit;
//return "";
        } else {
            $path = $this->upload->data();
            return $path['file_name'];
        }
    }

    /*public function uploadVideo($name,$path){

	$videoData = $_FILES[$name];
	$videoName = $_FILES[$name]['name'];
	if(file_exists('./uploads/'.$path.'/'.$videoName)) {
		return '';
	}
	else {
		move_uploaded_file($_FILES["file"]["tmp_name"],'./uploads/'.$path.'/'.$videoName);
		return $videoName;
	}
}*/

    public function uploadVideo($image, $path)
    {
        $config = array();
        $config['allowed_types'] = 'mp4';
        $config['upload_path'] = './uploads/' . $path;
        $config['max_size'] = '24048';
        $this->load->library('upload');
        $this->upload->initialize($config);
//print_r($config);exit;
        if (!$this->upload->do_upload($image, true)) {
            echo $this->upload_err = $this->upload->display_errors();
            exit;
//return "";
        } else {
            $path = $this->upload->data();
            return $path['file_name'];
        }
    }

    public function uploadImage($image, $path)
    {

        $config = array();
        $config['allowed_types'] = 'jpg|png|gif|txt|pdf|docx|doc|csv|jpeg|ppt|pptx';
        $config['upload_path'] = './uploads/' . $path;
        $config['max_size'] = '24048';
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($image, true)) {
            echo $this->upload_err = $this->upload->display_errors();
            exit;
//return "";
        } else {
            $path = $this->upload->data();
            return $path['file_name'];
        }
    }

// create thumbs crop
    public function createthumb($name, $table, $thumb_value)
    {
        $path = 'uploads/' . $table . '/';
// Create an array that holds the various image sizes
        $configs = array();
        $sumb_val = explode(",", $thumb_value);
        foreach ($sumb_val as $key => $value) {
            list($width, $height) = explode("x", $value);
            $configs[] = array(
                'path' => $path. $value . "/",
                'source_image' => $name, 'new_image' => $value . "/" . $name, 'width' => $width, 'height' => $height, 'maintain_ratio' => FALSE);
        }
        $this->load->library('image_lib');
        foreach ($configs as $config) {
            $this->fullmkdir($config['path']);
            $this->image_lib->thumb($config, FCPATH . '' . $path);
        }
    }

    protected function fullmkdir($path)
    {
        $path = explode("/", $path);
        $currentPath = "";

        foreach ($path as $part) {
            if (!trim($part)) {
                continue;
            }
            $currentPath .= $part.'/';
            if (!is_dir($currentPath)) {
                mkdir($currentPath);
            }
        }
    }

// create thumbs resize
    public function createthumb1($name, $table, $thumb_value)
    {
        $path = 'uploads/' . $table . '/';
// Create an array that holds the various image sizes
        $configs = array();
        $sumb_val = explode(",", $thumb_value);
        foreach ($sumb_val as $key => $value) {
            list($width, $height) = explode("x", $value);
            $configs[] = array(
                'path' => $path. $value . "/",
                'source_image' => $name, 'new_image' => $value . "/" . $name, 'width' => $width, 'height' => $height, 'maintain_ratio' => TRUE);
        }
        $this->load->library('image_lib');
        foreach ($configs as $config) {
            $this->fullmkdir($config['path']);
            $this->image_lib->thumb($config, FCPATH . '' . $path);
        }
    }

    public function createImageCopy($path, $filename)
    {
        $this->load->library('image_lib');
        $path = './' . $path . '/';
        $config['create_thumb'] = FALSE;
        $config['source_image'] = $path . $filename;
        // explode file name from extension
        $parts = explode('.', $filename);
        $ext = array_pop($parts);
        $file_name = array_shift($parts);
        // explode extension from file name
        $x = explode('.', $filename);
        $extension = end($x);
        $new_file_name = '';
        for ($i = 1; $i <= 1000; $i++) {
            //	echo $path.$file_name.$i.$extension;exit;
            if (!file_exists($path . $file_name . $i . '.' . $extension)) {
                $new_file_name = $file_name . $i . '.' . $extension;
                break;
            }
        }
        $config['new_image'] = $path . $new_file_name;
        $this->image_lib->clear();
        // Set your config up
        $this->image_lib->initialize($config);
        // Do your manipulation
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
            exit;
        }
        return $new_file_name;
    }


    public function date_in_formate($date)
    {
        if ($date != "") {
            list($d, $m, $y) = explode('/', $date);
            $date = $y . "-" . $m . "-" . $d;
        } else {
            $date = "0000-00-00";
        }
        return $date;
    }

    public function date_out_formate($date)
    {
        if (!empty($date) && $date != "0000-00-00") {
            list($y, $m, $d) = explode('-', $date);
            $date = $d . "/" . $m . "/" . $y;
        } else {
            $date = "";
        }
        return $date;
    }

    public function date_in_formate1($date)
    {
        if ($date != "") {
            list($d, $m, $y) = explode('/', $date);
            $date = $y . "-" . $m . "-" . $d;
        } else {
            $date = "0000-00-00";
        }
        return $date;
    }

    public function date_out_formate1($date)
    {
        if (!empty($date) && $date != "0000-00-00") {
            list($y, $m, $d) = explode('-', $date);
            $date = $d . "/" . $m . "/" . $y;
        } else {
            $date = "";
        }
        return $date;
    }

// Delete Related images when delete any items from the recycle Bin .
    public function deleter_related_images($content_type, $id)
    {
        $tables = str_replace(" ", "_", $content_type);
        $q1 = "SELECT * FROM `" . $tables . "` WHERE id_" . $tables . " = " . $id;
        $query1 = $this->db->query($q1);
        $row = $query1->row_array();
        $q = "SELECT name,thumb_val,thumb FROM `content_type_attr` WHERE id_content = (SELECT id_content FROM `content_type` WHERE name ='" . $content_type . "') AND (type = 4 OR type =5)";
        $query = $this->db->query($q);
        $related = $query->result_array();

        foreach ($related as $vv) {
            if ($row[$vv["name"]] != '') {
                if (file_exists('./uploads/' . $tables . '/' . $row[$vv["name"]])) {
                    unlink('./uploads/' . $tables . '/' . $row[$vv["name"]]);
                }
                if ($vv["thumb"] == 1) {
                    $sumb_val0 = explode(",", $vv["thumb_val"]);
                    foreach ($sumb_val0 as $key => $value) {
                        if (file_exists('./uploads/' . $tables . '/' . $value . '/' . $row[$vv["name"]])) {
                            unlink("./uploads/" . $tables . "/" . $value . "/" . $row[$vv["name"]]);
                        }
                    }
                }
            }

        }
    }


    public function outputKey()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $uniqid = uniqid(mt_rand(), true);
        $formKey = md5($ip . $uniqid);
        $this->session->set_userdata('formKey', $formKey);
        echo "<input type='hidden' name='form_key' id='form_key' value='" . $formKey . "' />";
    }

    public function check_token()
    {
        $token = $this->session->userdata('formKey');
        if ($this->input->post('form_key') == $token) {
            $res = true;
        } else {
            $res = false;
        }
        $this->session->unset_userdata('formKey');
        return $res;
    }

    function getcountry($id_locations)
    {
        $this->db->select('locations.*, countries.*');
        $this->db->from('locations');
        $this->db->join('countries', 'countries.id_countries = locations.id_countries');
        $this->db->where('locations.id_locations', $id_locations);
        $this->db->where('locations.deleted', 0);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function getCount($table)
    {
        $this->db->select('id_products');
        $this->db->from($table);

        $query = $this->db->get();
        $res = $query->num_rows();
        return $res;
    }


    function getmaxsort($table, $array)
    {
        $this->db->select_max('sort_order');
        $this->db->where_in('id_' . $table, $array);
        $query = $this->db->get($table);
        $res = $query->row_array();
        return $res["sort_order"];
    }

    function get_first_row($table, $where = array())
    {
        if (count($where) > 0)
            $query = $this->db->get_where($table, $where);
        else
            $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->first_row();
        } else {
            return array();
        }
    }

    function get_last_row($table, $where = array())
    {
        if (count($where) > 0)
            $query = $this->db->get_where($table, $where);
        else
            $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->last_row();
        } else {
            return array();
        }
    }

    function union_tables($tabel1, $table2)
    {
        $this->db->select('title, description, created_date');
        $this->db->from($tabel1);
        $subQuery1 = $this->db->_compile_select();
        $this->db->_reset_select();
        $this->db->select('title, destination AS description, created_date');
        $this->db->from($table2);
        $subQuery2 = $this->db->_compile_select();
        $this->db->_reset_select();
        $query = $this->db->query($subQuery1 . " UNION " . $subQuery2);
        return $query->result_array();
    }

    function another_way_for_union()
    {
        $this->db->select('title, content, date');
        $this->db->from('mytable1');
        $query1 = $this->db->get()->result();

        $this->db->select('title, content, date');
        $this->db->from('mytable2');
        $query2 = $this->db->get()->result();
// Merge both query results
        $query = array_merge($query1, $query2);
    }


    function soso()
    {
//$fields = $this->db->list_fields('maintenance');
        $query = $this->db->query('SELECT deleted FROM maintenance');
        $fields = $query->list_fields();
        foreach ($fields as $field) {
            echo $field . "<br />";
        }
        $fields = $this->db->field_data('maintenance');
        foreach ($fields as $field) {
            echo $field->name;
            echo $field->type;
            echo $field->max_length;
            echo $field->primary_key;
            echo "<br />";
        }
    }

    function to_excel($query, $filename = 'exceloutput')
    {
        $headers = ''; // just creating the var for field headers to append to below
        $data = ''; // just creating the var for field data to append to below

        $obj = &get_instance();
        $query = $this->db->query($query);
        $fields = $query->field_data();
        if ($query->num_rows() == 0) {
            #echo '<p>The table appears to have no data.</p>';
            return 'empty';
        } else {
            # Headers array
            $headers_array = array();

            foreach ($fields as $field) {
                # Add to headers
                if (!in_array($field->name, $headers_array)) {
                    $headers .= $field->name . "\t";
                    $headers_array[] = $field->name;
                }

            }

            foreach ($query->result() as $row) {
                $line = '';
                foreach ($row as $value) {
                    if ((!isset($value)) or ($value == "")) {
                        $value = "\t";
                    } else {
                        $value = str_replace('"', '""', $value);
                        $value = '"' . $value . '"' . "\t";
                    }
                    $line .= $value;
                }
                $data .= trim($line) . "\n";
            }

            $data = str_replace("\r", "", $data);

            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=$filename.xls");
            echo "$headers\n$data";
        }
    }


    public function GetRestRequestOrder()
    {
        $q = "SELECT * FROM request_order WHERE  deleted = 0 AND id_request_order NOT IN ( SELECT id_request_order FROM request_quotaion ) AND id_request_order NOT IN ( SELECT id_request_order FROM purchase_order ) AND id_locations = " . $this->session->userdata('country') . " ORDER BY sort_order";
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function check_subqoutation($id)
    {
        $this->db->where('id_sb', $id);
        $query = $this->db->get('request_quot_sb');
        if ($query->num_rows() == 0)
            return false;
        else
            return true;
    }

    public function GetRestQoutation()
    {
        $q = "SELECT * FROM request_quotaion WHERE  deleted = 0 AND id_request_quotaion NOT IN ( SELECT id_request_quotaion FROM purchase_order ) AND id_locations = " . $this->session->userdata('country') . " ORDER BY sort_order";
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }


    public function select_currency()
    {
        $this->db->select('countries.currency_code');
        $this->db->from('countries');
        $this->db->join('locations', 'locations.id_countries = countries.id_countries');
        $this->db->group_by('locations.id_countries');
        $this->db->order_by('locations.sort_order');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function GetPurchaseOrder()
    {
        $q = "SELECT * FROM purchase_order WHERE  deleted = 0 AND id_purchase_order NOT IN ( SELECT id_purchase_order FROM receving_po ) AND id_locations = " . $this->session->userdata('country') . " ORDER BY sort_order";
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function check_subreceiving($id)
    {
        $this->db->where('id_purchase_order_sb', $id);
        $query = $this->db->get('receving_po_sb');
        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return array();
    }


    function GetBirthdaysThisMonth()
    {
        $month = date('m');
        $q = "SELECT *  FROM clients WHERE dob LIKE '%-" . $month . "-%' AND active = 1 AND deleted = 0";
        $query = $this->db->query($q);
        $result = $query->result_array();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else {
            return '';
        }
    }


    function getAll_calls($handled)
    {
        $this->db->where('deleted', 0);
        $this->db->where('handled', $handled);
        $this->db->order_by('created_date', 'DESC');
        $query = $this->db->get('call_center');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function getAll_complains($solved)
    {
        $this->db->where('deleted', 0);
        $this->db->where('solved', $solved);
        $this->db->order_by('created_date', 'DESC');
        $query = $this->db->get('complains');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    function get_temp_list($id)
    {
        $this->db->select('newsletter_recipients.*,newsletter_temp_list.id_temp_list');
        $this->db->from('newsletter_temp_list');
        $this->db->join('newsletter_recipients', 'newsletter_recipients.id_recipients = newsletter_temp_list.id_recipients');
        $this->db->where('newsletter_temp_list.id_template', $id);
        $this->db->where('newsletter_recipients.deleted', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function upload_ajax($file, $path)
    {
        $actual_image_name = '';
        $valid_formats = array("jpg", "png", "gif", "bmp", "txt", "pdf", "doc", "docx", "xls", "xlsx");
        $name = $_FILES[$file]['name'];
        $size = $_FILES[$file]['size'];
        if (strlen($name)) {
            list($txt, $ext) = explode(".", $name);
            if (in_array($ext, $valid_formats)) {
                if ($size < (1024 * 1024)) {
//$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
                    $actual_image_name = str_replace(" ", "_", $txt) . time() . "." . $ext;
                    $tmp = $_FILES[$file]['tmp_name'];
                    if (move_uploaded_file($tmp, $path . $actual_image_name)) {
//mysql_query("UPDATE users SET profile_image='$actual_image_name' WHERE uid='$session_id'");

//echo "<img src='".$path.$actual_image_name."'  class='preview' width='50' height='50'>";
//echo "<img src='".base_url()."images/activate.png' />&nbsp;&nbsp;<span style='color:#5ac43f'>file was uploaded successfully</span>";
                        return $actual_image_name;
                        exit;
                    } else
                        echo "<img src='" . base_url() . "images/icon-error.gif' />&nbsp;&nbsp;<span style='color:#A5000B'>failed</span>";
                    exit;
                } else
                    echo "<img src='" . base_url() . "images/icon-error.gif' />&nbsp;&nbsp;<span style='color:#A5000B'>Image file size max 1 MB</span>";
                exit;
            } else
                echo "<img src='" . base_url() . "images/icon-error.gif' />&nbsp;&nbsp;<span style='color:#A5000B'>Invalid file format..</span>";
            exit;
        } else {
            echo "<img src='" . base_url() . "images/icon-error.gif' />&nbsp;&nbsp;<span style='color:#A5000B'>Please select image..!</span>";
            exit;
        }
    }

    public function parent_rows($id = 0, $arr)
    {
        $q = "SELECT * FROM testing WHERE id_parent = " . $id;
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            foreach ($res as $val) {
                $arr[] = $val;
                $arr = $this->parent_rows($val["id"], $arr);
            }
        }
        return $arr;
    }

// url generate
/////////////////////////////////////////////////////////////////////////////////////////////
    public function cleanURL($table, $url_name, $id = '')
    {
        $url_name = strtolower($url_name);
        $cond = array('title_url' => $url_name);
        if ($id != '')
            $cond["id_" . $table . " != "] = $id;
        $check_exist = $this->if_existings($table, $cond);
        if ($check_exist == false) {
            return $url_name;
        }

        $new_url_name = '';
        for ($i = 1; $i < 1000; $i++) {
            $new_url_name = $url_name . $i;
            $cond = array('title_url' => $new_url_name);
            if ($id != '')
                $cond["id_" . $table . " != "] = $id;
            $check_exist = $this->if_existings($table, $cond);
            if ($check_exist == false) {
                $return_url = $new_url_name;
                break;
            }
        }
        return $return_url;
    }

    function if_existings($table, $cond)
    {
        $this->db->where($cond);
        $query = $this->db->get($table);
        return ($query->num_rows() > 0) ? true : false;
    }
// custom //////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
    public function getTablePagination($table, $cond = array(), $limit = '', $offset = '', $sort_order = "sort_order")
    {
        $data = array();
        $this->db->select('*');
        $this->db->from($table);
        $cond['deleted'] = 0;
        $this->db->where($cond);
        $this->db->order_by($sort_order);
        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get();
        if ($limit == '') {
            $data = $query->num_rows();
        } else {
            $data = $query->result_array();
        }
        return $data;
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function select_product_categories($id_products, $cond = array())
    {
        //echo $id_products;exit;
        $this->db->select('categories.*');
        $this->db->from('categories');
        $this->db->join('products_categories', 'categories.id_categories = products_categories.id_categories');
        $cond['products_categories.id_products'] = $id_products;
        $this->db->where($cond);
        $this->db->group_by('categories.id_categories');
        $this->db->order_by('categories.title');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $categories = $query->result_array();
        $results = array();
        foreach ($categories as $category) {
            array_push($results, $category['id_categories']);
        }
        return $results;
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function insert_product_categories($id_products, $categories)
    {
        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from('products_categories');
        $cond = array('id_products' => $id_products);
        $this->db->where($cond);
        $this->db->order_by('products_categories.id_products_categories');
        $query = $this->db->get();
        $results = $query->result_array();
        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_categories']);
            }
        }
        foreach ($categories as $category) {
            if (!in_array($category, $old_ids)) {
                array_push($new_ids, $category);
            }
        }
        if (!empty($old_ids)) {
            foreach ($old_ids as $id) {
                if (!in_array($id, $categories)) {
                    array_push($deleted_ids, $id);
                }
            }
        }
        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM products_categories WHERE id_products = ' . $id_products . ' AND id_categories IN (' . implode(',', $deleted_ids) . ')';
            $this->db->query($q);
        }
        if (!empty($new_ids)) {
            foreach ($new_ids as $new_id) {
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['id_categories'] = $new_id;
                $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($new_id);
                $parent_levels = arrangeParentLevels($parent_levels);
                $ids_parent = $this->ecommerce_model->getIds('id_categories', $parent_levels);
                if (!empty($ids_parent)) {
                    $ids_parent = implode(',', $ids_parent);
                }
                $data['ids_parent'] = $ids_parent;
                $data['id_products'] = $id_products;
                $this->db->insert('products_categories', $data);
            }
        }
    }


/////////////////////////////////////////////////////////////////////////////////////////////
    public function select_product_related($id_products)
    {
        //echo $id_products;exit;
        $this->db->select('products.*');
        $this->db->from('products');
        $this->db->join('related_products', 'products.id_products = related_products.id_related');
        $cond = array(
            'related_products.id_products' => $id_products,
        );
        $this->db->where($cond);
        $this->db->group_by('products.id_products');
        $this->db->order_by('products.title');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $products = $query->result_array();
        $results = array();
        foreach ($products as $product) {
            array_push($results, $product['id_products']);
        }
        return $results;
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function insert_product_related($id_products, $related_products)
    {
        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from('related_products');
        $cond = array('id_products' => $id_products);
        $this->db->where($cond);
        $this->db->order_by('related_products.id_related_products');
        $query = $this->db->get();
        $results = $query->result_array();


        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_related']);
            }
        }


        foreach ($related_products as $related) {
            if (!in_array($related, $old_ids)) {
                array_push($new_ids, $related);
            }
        }


        if (!empty($old_ids)) {
            foreach ($old_ids as $id) {
                if (!in_array($id, $related_products)) {
                    array_push($deleted_ids, $id);
                }
            }
        }


        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM related_products WHERE id_products = ' . $id_products . ' AND id_related IN (' . implode(',', $deleted_ids) . ')';
            $this->db->query($q);
        }
        if (!empty($new_ids)) {
            foreach ($new_ids as $new_id) {
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['id_related'] = $new_id;
                $data['id_products'] = $id_products;
                $this->db->insert('related_products', $data);
            }
        }
    }


/////////////////////////////////////////////////////////////////////////////////////////////
    public function insert_spa_projects($id_spa, $projects)
    {
        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from('projects_type_spa');
        $cond = array('id_spa' => $id_spa);
        $this->db->where($cond);
        $this->db->order_by('projects_type_spa.id_projects');
        $query = $this->db->get();
        $results = $query->result_array();
        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_projects']);
            }
        }
        foreach ($projects as $project) {
            if (!in_array($project, $old_ids)) {
                array_push($new_ids, $project);
            }
        }
        if (!empty($old_ids)) {
            foreach ($old_ids as $id) {
                if (!in_array($id, $projects)) {
                    array_push($deleted_ids, $id);
                }
            }
        }
        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM projects_type_spa WHERE id_spa = ' . $id_spa . ' AND id_projects IN (' . implode(',', $deleted_ids) . ')';
            $this->db->query($q);
        }
        if (!empty($new_ids)) {
            foreach ($new_ids as $new_id) {
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['id_projects'] = $new_id;
                $data['id_spa'] = $id_spa;
                $this->db->insert('projects_type_spa', $data);
            }
        }
    }

    public function insert_consultants_messages($id_spa)
    {
        $consultants = $this->input->post('consultants');
        $checked_input = $this->input->post('checked');
        $messages = $this->input->post('message');

        foreach ($consultants as $key => $val) {
            $consultant = $consultants[$key];
            $checked = $checked_input[$key];
            $message = $messages[$key];

            if (!empty($message) && !empty($checked)) {
                $insert['message'] = $message;
                $insert['id_consultants'] = $consultant;
                $insert['id_opening_a_new_spa'] = $id_spa;
                $this->db->insert('consultants_messages', $insert);
            }

        }

    }

    public function select_spa_cosnultants($id_spa)
    {
        //echo $id_products;exit;
        $this->db->select('consultants_messages.*, consultants.name as name');
        $this->db->from('consultants_messages');
        $this->db->join('consultants', 'consultants_messages.id_consultants = consultants.id_consultants');
        $cond = array(
            'consultants_messages.id_opening_a_new_spa' => $id_spa,
        );
        $this->db->where($cond);
        $this->db->group_by('consultants.id_consultants');
        $this->db->order_by('consultants_messages.id_consultants_messages');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $consultants_messages = $query->result_array();

        return $consultants_messages;
    }


    public function select_spa_projects($id_spa)
    {
        //echo $id_products;exit;
        $this->db->select('projects_type.*');
        $this->db->from('projects_type');
        $this->db->join('projects_type_spa', 'projects_type.id_projects_type = projects_type_spa.id_projects');
        $cond = array(
            'projects_type_spa.id_spa' => $id_spa,
        );
        $this->db->where($cond);
        $this->db->group_by('projects_type.id_projects_type');
        $this->db->order_by('projects_type.title');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $projects = $query->result_array();
        $results = array();
        foreach ($projects as $project) {
            array_push($results, $project['id_projects_type']);
        }
        return $results;
    }

/////////////////////////////////////////////////////////////////////////////////////////////
    public function insert_spa_services($id_spa, $services)
    {
        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from('services_spa');
        $cond = array('id_spa' => $id_spa);
        $this->db->where($cond);
        $this->db->order_by('services_spa.id_services');
        $query = $this->db->get();
        $results = $query->result_array();
        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_services']);
            }
        }
        foreach ($services as $service) {
            if (!in_array($service, $old_ids)) {
                array_push($new_ids, $service);
            }
        }
        if (!empty($old_ids)) {
            foreach ($old_ids as $id) {
                if (!in_array($id, $services)) {
                    array_push($deleted_ids, $id);
                }
            }
        }
        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM services_spa WHERE id_spa = ' . $id_spa . ' AND id_services IN (' . implode(',', $deleted_ids) . ')';
            $this->db->query($q);
        }
        if (!empty($new_ids)) {
            foreach ($new_ids as $new_id) {
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['id_services'] = $new_id;
                $data['id_spa'] = $id_spa;
                $this->db->insert('services_spa', $data);
            }
        }
    }

    public function select_spa_services($id_spa)
    {
        //echo $id_products;exit;
        $this->db->select('services_enquiry.*');
        $this->db->from('services_enquiry');
        $this->db->join('services_spa', 'services_enquiry.id_services_enquiry = services_spa.id_services');
        $cond = array(
            'services_spa.id_spa' => $id_spa,
        );
        $this->db->where($cond);
        $this->db->group_by('services_enquiry.id_services_enquiry');
        $this->db->order_by('services_enquiry.title');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $services = $query->result_array();
        $results = array();
        foreach ($services as $service) {
            array_push($results, $service['id_services_enquiry']);
        }
        return $results;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function generate_password($length)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    function checktoken($token)
    {
        $cond = array('token' => md5($token));
        $check = $this->getonerow('password_requests', $cond);
        if (!empty($check)) {
            $token = $this->generate_password(8);
            $this->checktoken($token);
        } else {
            return $token;
        }
    }

    function create_password_request($user)
    {
        $token = $this->generate_password(8);
        $token = $this->checktoken($token);
        //echo 'test: '.$token;exit;
        $created_date = date('Y-m-d h:i:s');
        $time = strtotime($created_date);
        $expiration_date = strtotime("+5 day", $time);
        $expiration_date = date('Y-m-d h:i:s', $expiration_date);
        $data = array(
            'id_user' => $user['id_user'],
            'created_date' => $created_date,
            'expiration_date' => $expiration_date,
            'token' => md5($token)
        );
        $this->db->insert('password_requests', $data);
        //print_r($data);exit;
        return $data;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getUsers($cond = array(), $like = array())
    {
        $this->db->select('user.*,roles.title,brands.name as person_name');
        $this->db->from('user');
        $this->db->join('roles', 'roles.id_roles = user.id_roles');

        $this->db->join('brands', 'brands.id_user = user.id_user', 'left');
        $this->db->where('user.deleted', 0);
        $this->db->where('user.id_parent', NULL);
        if (!empty($cond))
            $this->db->where($cond);
        $roles = array(3);
        $this->db->where_not_in('user.id_roles', $roles);
        if (!empty($like))
            $this->db->like($like);
        $this->db->group_by('user.id_user');
        $this->db->order_by('user.created_date desc');
        $query = $this->db->get();

        $results = $query->result_array();


        return $results;
    }

//////////////////////////////////////////////////////////////////////////////
    public function addVisit($section, $session_id, $id = "")
    {
        $q = 'SELECT * FROM visits WHERE section = "' . $section . '" AND session_id = "' . $session_id . '"';
        if ($id != "")
            $q .= ' AND record = ' . $id;
        $query = $this->db->query($q);
        $result = $query->row_array();
        if (empty($result)) {
            $insert['created_date'] = date("Y-m-d h:i:s");
            $insert['section'] = $section;
            $insert['session_id'] = $session_id;
            if ($id != "")
                $insert['record'] = $id;
            if (checkUserIfLogin()) {
                $user = $this->ecommerce_model->getUserInfo();
                $insert['id_user'] = $user['id_user'];
            }
            $this->db->insert("visits", $insert);

            if ($id != "") {
                $q1 = 'UPDATE ' . $section . ' SET visits = visits + 1 WHERE id_' . $section . ' = ' . $id;
                $this->db->query($q1);
            }

        }
        return "";
    }

//////////////////////////////////////////////////////////////////////////////
    public function createNewCaptcha()
    {
        return true;
        $img = '';
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url() . 'captcha/'
        );
        $cap = create_captcha($vals);
        if ($cap != '') {
            $data = array(
                'captcha_time' => $cap['time'],
                'ip_address' => $this->input->ip_address(),
                'word' => $cap['word']
            );
            $query = $this->db->insert_string('captcha', $data);
            $this->db->query($query);
            $img = $cap['image'];
        }
        return $img;
    }


////////////////////////////////////////////////////////////////////////////////////
    public function getBrandNameById($brand_id)
    {
        $q = "SELECT title FROM brands WHERE  id_brands = " . $brand_id;
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0]['title'];
        } else {
            return null;
        }
    }

    public function getUserTradingNameById($user_id)
    {
        $q = "SELECT trading_name FROM user WHERE  id_user = " . (int)$user_id;

        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0]['trading_name'];
        } else {
            return null;
        }
    }

    public function getCountryNameById($country_id)
    {
        $q = "SELECT title FROM countries WHERE  id_countries = " . (int)$country_id;

        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result[0]['title'];
        } else {
            return null;
        }
    }

    public function getBuyers($cond = array(), $limit = "", $offset = "")
    {

        $q = "";
        $q .= " select user.*, buyers.created_date as entry_created, buyers.updated_date as entry_updated ";
        $q .= " from user ";
        $q .= "  join buyers on user.id_user = buyers.id_user ";

        $q .= ' where buyers.id_supplier = ' .$cond['id_supplier'];

        if (isset($cond['trading_name'])) {
            $q .= ' and user.trading_name like "%' . $cond['trading_name'] . '%"';
        }

        if (isset($cond['name'])) {
            $q .= ' and user.name like "%' . $cond['name'] . '%"';
        }

        if (isset($cond['email'])) {
            $q .= ' and user.email like "%' . $cond['email'] . '%"';
        }

        if (isset($cond['id_countries'])) {
            $q .= ' and user.id_countries = ' . $cond['id_countries'];
        }

        if (isset($cond['city'])) {
            $q .= ' and user.city like "%' . $cond['city'] . '%"';
        }

        $q .= "  order by buyers.created_date desc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }

        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }

        return $data;
    }

    public function getOrderDetails($order_id)
    {
        $this->db->select('line_items.*');
        $this->db->from('line_items');
        $this->db->where('id_orders', $order_id);

        $query = $this->db->get();
        $items = $query->result_array();

        $line_items = $this->ecommerce_model->getLineItems($items);

        return $line_items;
    }
    //SELECT * FROM `orders` WHERE `payment_method` LIKE 'credit_cards' AND `payment_status` LIKE 'paid' AND `status` LIKE 'paid' ORDER BY `id_orders` DESC
    public function get_ordersAmount($id){
        $this->db->where('id_supplier',$id);
        $this->db->where('deleted',0);
        $this->db->where('payment_method','credit_cards');
        $this->db->where('payment_status','paid');
        $this->db->where('status','paid');
        $this->db->select_sum('amount_currency');
        $query = $this->db->get('orders');
        return $query->row_array();
    }
}
