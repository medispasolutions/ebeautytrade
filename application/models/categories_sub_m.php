<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categories_sub_m extends CI_Model{

public function get_categories_sub($id){
$this->db->where('id_categories_sub',$id);
$this->db->where('deleted',0);
$query = $this->db->get('categories_sub');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'categories_sub')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('categories_sub');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

public function getSubcategories_list($cond = array(),$like = array(),$limit = '',$offset = '') {
	$this->db->select("categories_sub.*");
	$this->db->from("categories_sub");
	if(isset($cond['categories_sub_categories.id_categories'])) {
		$this->db->join("categories_sub_categories","categories_sub_categories.id_categories_sub = categories_sub.id_categories_sub");
	}
	$this->db->where("categories_sub.deleted",0);
	if(!empty($cond)) {
		$this->db->where($cond);
	}
	if(!empty($like)) {
		$this->db->like($like);
	}
	if(isset($cond['categories_sub_categories.id_categories'])) {
		$this->db->group_by("categories_sub.id_categories_sub");
	}
	$this->db->order_by("categories_sub.sort_order");
	if($limit != '')
	$this->db->limit($limit,$offset);
	$query = $this->db->get();
	if($limit != '')
	$result = $query->result_array();
	else 
	$result = $query->num_rows();
	return $result;
}

}