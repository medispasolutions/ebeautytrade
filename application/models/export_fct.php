<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Export_fct extends CI_Model
{

    public function salesHistoryExportToExcel($info)
    {

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Spamiles")
            ->setLastModifiedBy("Spamiles");


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ORDER ID')
            ->setCellValue('B1', 'CREATED DATE')
            ->setCellValue('C1', 'CUSTOMER NAME')
            ->setCellValue('D1', 'E-mail')
            ->setCellValue('E1', 'Phone')
            ->setCellValue('F1', 'Amount')
            ->setCellValue('G1', 'Currency')
            ->setCellValue('H1', 'status');


        $i = 1;
        $net_price = 0;
        foreach ($info as $val) {
            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $val['id_orders'])
                ->setCellValue('B' . $i, $val['created_date'])
                ->setCellValue('C' . $i, $val['name'])
                ->setCellValue('D' . $i, $val['email'])
                ->setCellValue('E' . $i, $val['phone'])
                ->setCellValue('F' . $i, $val["amount_currency"])
                ->setCellValue('G' . $i, lang($val["currency"]))
                ->setCellValue('H' . $i, $val['status']);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Sales History');


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-type: application/vnd.ms-excel');
        $filename = "orders-" . date('Y-m-d-His');
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        $objWriter->save('php://output');
        exit;
    }


    public function invoicesExportToExcel($info)
    {
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');

//define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        /** Include PHPExcel */


// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

// Set document properties
        $objPHPExcel->getProperties()->setCreator("Spamiles")
            ->setLastModifiedBy("Spamiles")
            ->setTitle("PHPExcel Document")
            ->setSubject("PHPExcel Document")
            ->setDescription("Document for PHPExcel, generated using PHP classes.")
            ->setKeywords("office PHPExcel php")
            ->setCategory("Result file");

// Add some data
//echo date('H:i:s') , " Add some data" , EOL;


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Invoice ID')
            ->setCellValue('B1', 'CREATED DATE')
            ->setCellValue('C1', 'CUSTOMER NAME')
            ->setCellValue('D1', 'TOTAL AMOUNT')
            ->setCellValue('E1', 'TOTAL AMOUNT CREDITS')
            ->setCellValue('F1', 'SHIPPING CHARGE')
            ->setCellValue('G1', 'NET PRICE')
            ->setCellValue('H1', 'STATUS');

        $i = 1;
        $net_price = 0;

        foreach ($info as $val) {

            $i++;


            $user_name = "";
            if (!empty($val['user'])) {
                $user_name = $val['user']['name'];
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, '#' . $val['id_invoices'])
                ->setCellValue('B' . $i, date("F d,Y", strtotime($val['created_date'])))
                ->setCellValue('C' . $i, $user_name)
                ->setCellValue('D' . $i, $val["total_amount"] . ' ' . $val["currency"])
                ->setCellValue('E' . $i, $val["total_amount_credits"] . ' ' . $val["currency"])
                ->setCellValue('F' . $i, $val["total_customer_charge"] . ' ' . $val["currency"])
                ->setCellValue('G' . $i, $val["net_price"] . ' ' . $val["currency"])
                ->setCellValue('H' . $i, $val['status']);

        }
        /*$i++;
                 $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('E'.$i,'Total')
                    ->setCellValue('F'.$i,$info["total_qty_sold_by_admin"])
                    ->setCellValue('G'.$i, $info['total_qty_payable']);

        $i++;
                 $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('E'.$i,'Paid')
                    ->setCellValue('F'.$i,$info["total_qty_sold_by_admin_paid"])
                    ->setCellValue('G'.$i, $info['total_qty_payable_paid']);	*/


// Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Inventory');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);

// Save Excel 2007 file
//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
// Save Excel 2007 file

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        /*ob_end_clean();*/
// We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        $filename = "Sales Reports(" . date('Y-m-d H:i:s') . ")";
// It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        $objWriter->save('php://output');
        exit;
    }

    public function salesReportExportToExcel($info)
    {
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');

//define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        /** Include PHPExcel */


// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

// Set document properties
        $objPHPExcel->getProperties()->setCreator("Spamiles")
            ->setLastModifiedBy("Spamiles")
            ->setTitle("PHPExcel Document")
            ->setSubject("PHPExcel Document")
            ->setDescription("Document for PHPExcel, generated using PHP classes.")
            ->setKeywords("office PHPExcel php")
            ->setCategory("Result file");

// Add some data
//echo date('H:i:s') , " Add some data" , EOL;


        $i = 0;
        $net_price = 0;
        $brands = $info['brands'];
        $i++;
        $style = array(
            'font' => array(
                'bold' => false,
                'size' => 16,
                'name' => 'Verdana',
                'color' => array('rgb' => '84ddfd'),
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );

        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->setCellValueByColumnAndRow(0, $i, $info['from_date'] . '-' . $info['to_date']);
        $sheet->mergeCells('A' . $i . ':K' . $i);
        $sheet->getStyle('A' . $i)->applyFromArray($style);


        foreach ($brands as $brand) {
            $i++;
            $line_items = $brand['line_items'];


            $style = array(
                'font' => array(
                    'bold' => true,
                    'size' => 13,
                    'name' => 'Verdana',
                    'color' => array('rgb' => '000000'),
                ),
                'alignment' => array(
                    'wrap' => true,
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
            );

            $sheet = $objPHPExcel->getActiveSheet();
            $sheet->setCellValueByColumnAndRow(0, $i, $brand['title']);
            $sheet->mergeCells('A' . $i . ':K' . $i);
            $sheet->getStyle('A' . $i)->applyFromArray($style);

            $i++;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, 'Order ID')
                ->setCellValue('B' . $i, 'Customer Name')
                ->setCellValue('C' . $i, lang('trading_name'))
                ->setCellValue('D' . $i, 'Product')
                ->setCellValue('E' . $i, 'SKU')
                ->setCellValue('F' . $i, lang('barcode'))
                ->setCellValue('G' . $i, lang('shelf_location'))
                ->setCellValue('H' . $i, 'Date')
                ->setCellValue('I' . $i, 'Price(' . $info['currency'] . ')')
                ->setCellValue('J' . $i, 'Quantity')
                ->setCellValue('K' . $i, 'Return Quantity')
                /*			->setCellValue('H'.$i, lang('paid_m'))
                            ->setCellValue('I'.$i, lang('consignment_m'))*/
                ->setCellValue('L' . $i, 'Credit Cart(' . $info['currency'] . ')')
                ->setCellValue('M' . $i, 'Total Price(' . $info['currency'] . ')');

            foreach ($line_items as $val) {

                $i++;
                $product_name = '';
                //if(empty($pro['product']['sku']))
                $product_name .= $val['product']['title'];
                //else
                //$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
                if (!empty($val['options'])) {
                    $options = $val['options'];
                    $options = unSerializeStock($options);
                    $product_name .= '<br />';
                    $c = count($options);
                    $k = 0;
                    foreach ($options as $key => $opt) {
                        $k++;
                        $product_name .= $opt;
                        if ($k != $c) {
                            $product_name .= ' - ';
                        }
                    }
                }
                if (!isset($val['product']['location_code'])) {
                    $val['product']['location_code'] = "";
                }

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, '#' . $val['id_orders'])
                    ->setCellValue('B' . $i, $val['order']['user']['name'])
                    ->setCellValue('C' . $i, $product_name)
                    ->setCellValue('D' . $i, $val['trading_name'])
                    ->setCellValue('E' . $i, $val['product']['sku'])
                    ->setCellValue('F' . $i, $val['product']['barcode'])
                    ->setCellValue('G' . $i, $val['product']['location_code'])
                    ->setCellValue('H' . $i, date("F d,Y", strtotime($val['created_date'])))
                    ->setCellValue('I' . $i, $val['price'] . ' ' . $info['currency'])
                    ->setCellValue('J' . $i, $val['quantity'])
                    ->setCellValue('K' . $i, $val['return_quantity'])
                    /*			->setCellValue('H'.$i, $val['qty_sold_by_admin'])
                                ->setCellValue('I'.$i, $val['qty_payable'])*/
                    ->setCellValue('L' . $i, $val['credits_val'] . ' ' . $info['currency'])
                    ->setCellValue('M' . $i, $val['total_price'] . ' ' . $info['currency']);
            }
        }

        $i = $i + 2;


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, 'Total Amount')
            ->setCellValue('B' . $i, $info["total_amount"] . ' ' . $info['currency']);


        $i++;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, 'Total Amount Credits')
            ->setCellValue('B' . $i, $info["total_amount_credits"] . ' ' . $info['currency']);

        $i++;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, 'Shipping Charge')
            ->setCellValue('B' . $i, $info["total_customer_charge"] . ' ' . $info['currency']);

        $i++;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, 'Net Price')
            ->setCellValue('B' . $i, $info["net_price"] . ' ' . $info['currency']);


// Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Invoices');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);

// Save Excel 2007 file
//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
// Save Excel 2007 file

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        /*ob_end_clean();*/
// We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        $filename = "Products Inventory(" . date('Y-m-d H:i:s') . ")";
// It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        $objWriter->save('php://output');
        exit;
    }


    public function inventoryExportToExcel($info)
    {
//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');

//define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        /** Include PHPExcel */


// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

// Set document properties
        $objPHPExcel->getProperties()->setCreator("Spamiles")
            ->setLastModifiedBy("Spamiles")
            ->setTitle("PHPExcel Document")
            ->setSubject("PHPExcel Document")
            ->setDescription("Document for PHPExcel, generated using PHP classes.")
            ->setKeywords("office PHPExcel php")
            ->setCategory("Result file");

// Add some data
//echo date('H:i:s') , " Add some data" , EOL;

        $i = 0;
        $i++;
        $style = array(
            'font' => array(
                'bold' => false,
                'size' => 16,
                'name' => 'Verdana',
                'color' => array('rgb' => '84ddfd'),
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );

        $title = "Inventory Reports";
        if ($info['order_type'] == 1) {
            $title .= '- In Consignment';
        }

        if ($info['order_type'] == 2) {
            $title .= '-Paid stock';
        }

        $sheet = $objPHPExcel->getActiveSheet();
        $sheet->setCellValueByColumnAndRow(0, $i, $title);
        $sheet->mergeCells('A' . $i . ':D' . $i);
        $sheet->getStyle('A' . $i)->applyFromArray($style);
        foreach (range('A', 'D') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }


        $i++;
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, 'Product Name')
            ->setCellValue('B' . $i, 'SKU')
            ->setCellValue('C' . $i, 'Brand Name')
            ->setCellValue('D' . $i, 'Quantity');

        $products = $info['products'];

        $styleD = array(
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );

        $styleC = array(
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );

        foreach ($products as $val) {


            $i++;
            $product_name = '';
            //if(empty($pro['product']['sku']))
            $product_name .= $val["title"];
            //else
            //$product_name .= $pro['product']['sku'].', '.$pro['product']['title'];
            if (!empty($val['combination'])) {
                $options = $val['combination'];
                $options = unSerializeStock($options);
                $product_name .= ' - ';
                $c = count($options);
                $k = 0;
                foreach ($options as $key => $opt) {
                    $k++;
                    $product_name .= $opt;
                    if ($k != $c) {
                        $product_name .= ' - ';
                    }
                }
            }

            $sheet->getStyle('D' . $i)->applyFromArray($styleD);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $product_name)
                ->setCellValue('B' . $i, $val['sku'])
                ->setCellValue('C' . $i, $val['brand_name'])
                ->setCellValue('D' . $i, $val['quantity']);


        }


        $i++;
        $sheet->getStyle('C' . $i)->applyFromArray($styleC);
        $sheet->getStyle('D' . $i)->applyFromArray($styleD);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $i, "")
            ->setCellValue('B' . $i, "")
            ->setCellValue('C' . $i, 'Total Qty: ')
            ->setCellValue('D' . $i, $info['total_qty']);


// Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Inventory Reports');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);

// Save Excel 2007 file
//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
// Save Excel 2007 file

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        /*ob_end_clean();*/
// We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        $filename = "Inventory Reports(" . date('Y-m-d H:i:s') . ")";
// It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        $objWriter->save('php://output');
        exit;
    }


    public function productsExportToExcel($info)
    {


//error_reporting(E_ALL);
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
//date_default_timezone_set('Europe/London');

//define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        /** Include PHPExcel */


// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

// Set document properties
        $objPHPExcel->getProperties()->setCreator("Spamiles")
            ->setLastModifiedBy("Spamiles")
            ->setTitle("PHPExcel Document")
            ->setSubject("PHPExcel Document")
            ->setDescription("Document for PHPExcel, generated using PHP classes.")
            ->setKeywords("office PHPExcel php")
            ->setCategory("Result file");

// Add some data
//echo date('H:i:s') , " Add some data" , EOL;

        $i = 1;

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Product Name')
            ->setCellValue('B1', 'SKU')
            ->setCellValue('C1', 'Created Date')
            ->setCellValue('D1', 'Option(S)')
            ->setCellValue('E1', 'Brand')
            ->setCellValue('F1', 'List Price')
            ->setCellValue('G1', 'Discount')
            ->setCellValue('H1', 'Discount Expiration')
            ->setCellValue('I1', 'Price')
            ->setCellValue('J1', 'Retail Price')
            ->setCellValue('M1', 'New Arrival')
            ->setCellValue('N1', 'Exportable')
            ->setCellValue('O1', 'Height')
            ->setCellValue('P1', 'Length')
            ->setCellValue('Q1', 'Width')
            ->setCellValue('R1', 'Weight')
            ->setCellValue('S1', 'Categories')
            ->setCellValue('T1', 'Barcode');


        $i = 1;
        $net_price = 0;

        foreach ($info as $val) {
            /*echo $val['title'];exit;*/
            /*echo "<pre>";
            print_r($info);exit;*/

            $i++;
            $categories = $val['products_categories'];
            $j = 0;
            $text_categories = "";
            foreach ($categories as $cat) {
                $j++;

                if ($j == 1) {
                    $text_categories = $cat['title'];
                } else {
                    $text_categories .= "," . $cat['title'];
                }
            }

            $brand_name = $this->fct->getonecell('brands', 'title', array('id_brands' => $val['id_brands']));
            $stocks = $this->fct->getAll_cond('products_stock', 'sort_order',
                array('id_products' => $val['id_products'], 'status' => 1));
            if (!empty($stocks)) {
                foreach ($stocks as $stock) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $val['title'])
                        ->setCellValue('B' . $i, $stock['sku'])
                        ->setCellValue('C' . $i, $stock['created_date'])
                        ->setCellValue('D' . $i, $stock['combination'])
                        ->setCellValue('E' . $i, $brand_name)
                        ->setCellValue('F' . $i, $stock['list_price'])
                        ->setCellValue('G' . $i, $stock['discount'])
                        ->setCellValue('H' . $i, $stock['discount_expiration'])
                        ->setCellValue('I' . $i, $stock['price'])
                        ->setCellValue('J' . $i, $stock['retail_price'])
                        ->setCellValue('M' . $i, $val['set_as_new'])
                        ->setCellValue('N' . $i, $val['set_as_clearance'])
                        ->setCellValue('O' . $i, $val['height'])
                        ->setCellValue('P' . $i, $val['length'])
                        ->setCellValue('Q' . $i, $val['width'])
                        ->setCellValue('R' . $i, $val['weight'])
                        ->setCellValue('S' . $i, $text_categories);
                    $i++;
                }
            } else {

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $val['title'])
                    ->setCellValue('B' . $i, $val['sku'])
                    ->setCellValue('C' . $i, $val['created_date'])
                    ->setCellValue('D' . $i, "")
                    ->setCellValue('E' . $i, $brand_name)
                    ->setCellValue('F' . $i, $val['list_price'])
                    ->setCellValue('G' . $i, $val['discount'])
                    ->setCellValue('H' . $i, $val['discount_expiration'])
                    ->setCellValue('I' . $i, $val['price'])
                    ->setCellValue('J' . $i, $val['retail_price'])
                    ->setCellValue('M' . $i, $val['set_as_new'])
                    ->setCellValue('N' . $i, $val['set_as_clearance'])
                    ->setCellValue('O' . $i, $val['height'])
                    ->setCellValue('P' . $i, $val['length'])
                    ->setCellValue('Q' . $i, $val['width'])
                    ->setCellValue('R' . $i, $val['weight'])
                    ->setCellValue('S' . $i, $text_categories)
                    ->setCellValue('T' . $i, $val['barcode']);

            }


        }


// Rename worksheet

        $objPHPExcel->getActiveSheet()->setTitle('Products');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);

// Save Excel 2007 file
//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
// Save Excel 2007 file

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        /*ob_end_clean();*/
// We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        $filename = "Products(" . date('Y-m-d H:i:s') . ")";
// It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        $objWriter->save('php://output');
        exit;
    }


    public function usersExportToExcel($info)
    {

        $filename = "Users_" . date("Y-m-d-H:i:s") . '.csv';

        header('Content-type:text/csv');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: post-check-0, pre-check-0');
        header('Pragma: no-cahche');
        header('expires:0');
        $handle = fopen('php://output', 'w');

        fputcsv($handle, array(
            'Trading Name',
            'First Name',
            'Last Name',
            'Position',
            'Email',
            'Address',
            'City',
            'Country',
            'Phone',
            'Mobile',
            'Business Type',
            'Role'));

        $this->db->select('user.trading_name, user.first_name, user.last_name, user.position, user.email, user.address_1, user.city, countries.title, user.phone, user.mobile, bt.title as business, roles.title as role');
        $this->db->from('user');
        $this->db->join('business_type as bt', 'bt.id_business_type = user.business_type');
        $this->db->join('countries', 'countries.id_countries = user.id_countries');
        $this->db->join('roles', 'roles.id_roles = user.id_roles');
        $where = "user.deleted = 0 AND user.id_roles != '1' AND user.id_roles != '3'";
        $this->db->where($where);


        $query = $this->db->get();
        $data['users'] = $query->result_array();

        foreach ($data['users'] as $key => $row) {
            fputcsv($handle, $row);
        }

        fclose($handle);
        exit;
    }
    
    public function subscribedExportToExcel($info)
    {
        $filename = "subscribers_" . date("Y-m-d-H:i:s") . '.csv';

        header('Content-type:text/csv');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: post-check-0, pre-check-0');
        header('Pragma: no-cahche');
        header('expires:0');
        $handle = fopen('php://output', 'w');

        fputcsv($handle, array(
            'Trading Name',
            'First Name',
            'Last Name',
            'Position',
            'Email',
            'Address',
            'City',
            'Country',
            'Phone',
            'Mobile',
            'Business Type',
            'Role'));

        $this->db->select('user.trading_name, user.first_name, user.last_name, user.position, user.email, user.address_1, user.city, countries.title, user.phone, user.mobile, bt.title as business, roles.title as role');
        $this->db->from('user');
        $this->db->join('business_type as bt', 'bt.id_business_type = user.business_type');
        $this->db->join('countries', 'countries.id_countries = user.id_countries');
        $this->db->join('roles', 'roles.id_roles = user.id_roles');
        $where = "user.deleted = 0 AND user.is_subscribed != '1' AND user.id_roles != '1' AND user.id_roles != '5' AND user.id_roles != '3'";
        $this->db->where($where);

        $query = $this->db->get();
        $data['users'] = $query->result_array();

        foreach ($data['users'] as $key => $row) {
            fputcsv($handle, $row);
        }

        fclose($handle);
        exit;
    }
    
    public function supplierExportToExcel($info)
    {
        $filename = "supplier_" . date("Y-m-d-H:i:s") . '.csv';

        header('Content-type:text/csv');
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: post-check-0, pre-check-0');
        header('Pragma: no-cahche');
        header('expires:0');
        $handle = fopen('php://output', 'w');

        fputcsv($handle, array(
            'Trading Name',
            'First Name',
            'Last Name',
            'Position',
            'Email',
            'Address',
            'City',
            'Country',
            'Phone',
            'Mobile',
            'Business Type',
            'Role'));

        $this->db->select('user.trading_name, user.first_name, user.last_name, user.position, user.email, user.address_1, user.city, countries.title, user.phone, user.mobile, bt.title as business, roles.title as role');
        $this->db->from('user');
        $this->db->join('business_type as bt', 'bt.id_business_type = user.business_type');
        $this->db->join('countries', 'countries.id_countries = user.id_countries');
        $this->db->join('roles', 'roles.id_roles = user.id_roles');
        $where = "user.deleted = 0 AND user.id_roles != '1' AND user.id_roles != '2' AND user.id_roles != '3' AND user.id_roles != '4' AND user.id_roles != '8'";
        $this->db->where($where);

        $query = $this->db->get();
        $data['users'] = $query->result_array();

        foreach ($data['users'] as $key => $row) {
            fputcsv($handle, $row);
        }

        fclose($handle);
        exit;
    }

    public function buyersExportToExcel($buyers)
    {
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Spamiles")
            ->setLastModifiedBy("Spamiles");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Created')
            ->setCellValue('B1', 'Last update')
            ->setCellValue('C1', 'Trade name')
            ->setCellValue('D1', 'Contact person')
            ->setCellValue('E1', 'Email')
            ->setCellValue('F1', 'Phone')
            ->setCellValue('G1', 'Country')
            ->setCellValue('H1', 'City');


        $i = 1;
        foreach ($buyers as $buyer) {

            $i++;
            $country = $this->fct->getCountryNameById($buyer['id_countries']);

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $buyer['entry_created'])
                ->setCellValue('B' . $i, $buyer['entry_updated'])
                ->setCellValue('C' . $i, $buyer['trading_name'])
                ->setCellValue('D' . $i, $buyer['name'])
                ->setCellValue('E' . $i, $buyer['email'])
                ->setCellValue('F' . $i, $buyer['phone'])
                ->setCellValue('G' . $i, $country)
                ->setCellValue('H' . $i, $buyer['city']);
        }

        $objPHPExcel->getActiveSheet()->setTitle('Buyers');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        header('Content-type: application/vnd.ms-excel');
        $filename = "Buyers-" . date('Y-m-d-His');
        header('Content-Disposition: attachment; filename="' . $filename . '.xls"');
        $objWriter->save('php://output');
        exit;
    }
}