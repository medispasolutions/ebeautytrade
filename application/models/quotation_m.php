<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Quotation_m extends CI_Model{

public function get_quotation($id){
$this->db->where('id_quotation',$id);
$this->db->where('deleted',0);
$query = $this->db->get('quotation');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  quotation.* ";
        $q .= " from  quotation ";
		$q .= " where quotation.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(quotation.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}