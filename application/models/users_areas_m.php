<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_areas_m extends CI_Model{

public function get_users_areas($id){
$this->db->where('id_users_areas',$id);
$this->db->where('deleted',0);
$query = $this->db->get('users_areas');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  users_areas.* ";
        $q .= " from  users_areas ";
		$q .= " where users_areas.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(users_areas.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}