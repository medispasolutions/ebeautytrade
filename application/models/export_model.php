<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Export_model extends CI_Model{

public function getCount($section)
{
	$cc = 0;
	if($section == "products")
	$cc = $this->getproducts();
	if($section == "stock")
	$cc = $this->getstock();
	if($section == "quantities")
	$cc = $this->getQuantities();
	return $cc;
}
public function getRecords($section,$limit,$offset)
{
	$data = array();
	if($section == "products")
	$data = $this->getproducts($limit,$offset);
	if($section == "stock")
	$data = $this->getstock($limit,$offset);
	if($section == "quantities")
	$data = $this->getQuantities($limit,$offset);
	return $data;
}
/////////////////////////////////////////////////////////////////////////
private function getproducts($limit = '',$offset = '') {
	$data['title'] = 'list of products';
	$results = array();
	$data['keys'][0] = 'ID';
	$data['keys'][1] = 'Title';
	$data['keys'][2] = 'Title English';
	$data['keys'][3] = 'List Price';
	$data['keys'][4] = 'Discount';
	$data['keys'][5] = 'Price';
	$data['keys'][6] = 'SKU';
	$data['arr'] = array();
	$q = 'SELECT id_products AS id, title, title_en, price, sku, list_price, discount FROM products WHERE deleted = 0 AND id_products NOT IN (SELECT id_products FROM products_stock)';
	if($limit != '')
	$q .= ' LIMIT '.$limit.' OFFSET '.$offset;
	$query = $this->db->query($q);
	if($limit != '')$results = $query->result_array();
	else $data = $query->num_rows();
	if(!empty($results)) {
		foreach($results as $key => $val) {
			$data['arr'][$key]['id'] = $val['id'];
			$data['arr'][$key]['title'] = $val['title'];
			$data['arr'][$key]['title_en'] = $val['title_en'];
			$data['arr'][$key]['list_price'] = $val['list_price'];
			$data['arr'][$key]['discount'] = $val['discount'];
			$data['arr'][$key]['price'] = $val['price'];
			$data['arr'][$key]['sku'] = $val['sku'];
		}
	}
	return $data;
}
/////////////////////////////////////////////////////////////////////////
private function getstock($limit = '',$offset = '') {
	$data['title'] = 'list of stock';
	$results = array();
	$data['keys'][0] = 'Product ID';
	$data['keys'][1] = 'ID';
	$data['keys'][2] = 'Title';
	$data['keys'][3] = 'Title English';
	$data['keys'][4] = 'Stock';
	$data['keys'][5] = 'List Price';
	$data['keys'][6] = 'Discount';
	$data['keys'][7] = 'Price';
	$data['keys'][8] = 'SKU';
	$data['arr'] = array();
	$q = 'SELECT products_stock.id_products_stock AS id, products.title, products.title_en, products_stock.price, products_stock.sku, products_stock.combination, products_stock.list_price, products_stock.discount, products.id_products AS product_id FROM products JOIN products_stock ON products_stock.id_products = products.id_products WHERE products.deleted = 0';
	if($limit != '')
	$q .= ' LIMIT '.$limit.' OFFSET '.$offset;
	$query = $this->db->query($q);
	if($limit != '')$results = $query->result_array();
	else $data = $query->num_rows();
	if(!empty($results)) {
		foreach($results as $key => $val) {
			$stock = unSerializeStock($val['combination']);
			$stock_text = '';
			if(!empty($stock)) {
				foreach($stock as $ky => $stk) {
					$stock_text .= $stk.' ';
				}
			}
			$data['arr'][$key]['product_id'] = $val['product_id'];
			$data['arr'][$key]['id'] = $val['id'];
			$data['arr'][$key]['title'] = $val['title'];
			$data['arr'][$key]['title_en'] = $val['title_en'];
			$data['arr'][$key]['stock'] = $stock_text;
			$data['arr'][$key]['list_price'] = $val['list_price'];
			$data['arr'][$key]['discount'] = $val['discount'];
			$data['arr'][$key]['price'] = $val['price'];
			$data['arr'][$key]['sku'] = $val['sku'];
		}
	}
	return $data;
}
/////////////////////////////////////////////////////////////////////////
public function getQuantities($limit = '',$offset = '')
{
	$data['title'] = 'list of Quantities';
	$results = array();
	$data['keys'][0] = 'ID';
	$data['keys'][1] = 'SKU';
	$data['keys'][2] = 'QTY';
	$data['keys'][3] = 'UPDATED SKU';
	$data['arr'] = array();
	$q = 'SELECT id_products AS id, sku, quantity FROM products WHERE deleted = 0 AND id_products NOT IN (SELECT id_products FROM products_stock) UNION(SELECT id_products_stock AS id,sku, quantity FROM products_stock)';
	//echo $q;exit;
	if($limit != '')
	$q .= ' LIMIT '.$limit.' OFFSET '.$offset;
	$query = $this->db->query($q);
	if($limit != '')$results = $query->result_array();
	else $data = $query->num_rows();
	if(!empty($results)) {
		foreach($results as $key => $val) {
			$data['arr'][$key]['id'] = $val['id'];
			$data['arr'][$key]['sku'] = $val['sku'];
			$data['arr'][$key]['qty'] = $val['quantity'];
			$data['arr'][$key]['updated_sku'] = $val['sku'];
		}
	}
	return $data;
}
/////////////////////////////////////////////////////////////////////////
}