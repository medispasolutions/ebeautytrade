<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_categories_m extends CI_Model{

public function get_users_categories($id){
$this->db->where('id_users_categories',$id);
$this->db->where('deleted',0);
$query = $this->db->get('users_categories');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'users_categories')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('users_categories');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

///////////////////////////////////////USER CATEGORIES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
	public function select_user_categories($id_users,$cond=array())
{
 $this->db->select('categories.*');
 $this->db->from('categories');
 $this->db->join('users_categories','users_categories.id_categories = categories.id_categories');
 $cond = array(
  'users_categories.id_users'=>$id_users,
 );
 $this->db->where($cond);
 $this->db->group_by('categories.id_categories');
 $this->db->order_by('categories.title');
 $query = $this->db->get();
 $categories = $query->result_array();
 
 $results = array();
 foreach($categories as $category) {
  array_push($results,$category['id_categories']);
 }
 return $results;
}
	
	public function insert_user_categories($id_users,$categories)
{

 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('users_categories');
 $cond = array('id_users'=>$id_users);
 $this->db->where($cond);
 $this->db->order_by('users_categories.id_users_categories');
 $query = $this->db->get();
 $results = $query->result_array();
 

 
 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_categories']);
  }
 }

 foreach($categories as $category=>$val) {

  if(!in_array($val,$old_ids)) {
   array_push($new_ids,$val);
  }
 }
 

 if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$categories)) {
    array_push($deleted_ids,$id);
   }
  }
 }


 if(!empty($deleted_ids)) {
  $q = 'DELETE FROM users_categories WHERE id_categories IN ('.implode(',',$deleted_ids).') AND id_users='.$id_users;
  $this->db->query($q);
 }



 if(!empty($new_ids)) {
  foreach($new_ids as $val) {
   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_categories'] = $val;
   $data['id_users'] = $id_users;
   $this->db->insert('users_categories',$data);
  }
 }
}

	public function insert_user_categories_admin($id_users,$categories)
{

 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('users_categories');
 $cond = array('id_users'=>$id_users);
 $this->db->where($cond);
 $this->db->order_by('users_categories.id_users_categories');
 $query = $this->db->get();
 $results = $query->result_array();
 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_categories']);
  }
 }

 foreach($categories as $category=>$val) {

  if(!in_array($category,$old_ids)) {
   array_push($new_ids,$val);
  }
 }
 if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$categories)) {
    array_push($deleted_ids,$id);
   }
  }
 }
 if(!empty($deleted_ids)) {
  $q = 'DELETE FROM users_categories WHERE id_categories IN ('.implode(',',$deleted_ids).') AND id_users='.$id_users;
  $this->db->query($q);
 }
 if(!empty($new_ids)) {
  foreach($new_ids as $new_id) {
   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_categories'] = $new_id;
   $data['id_users'] = $id_users;
   $this->db->insert('users_categories',$data);
  }
 }
}

}