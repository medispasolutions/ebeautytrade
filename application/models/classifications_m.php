<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Classifications_m extends CI_Model{

public function get_classifications($id){
$this->db->where('id_classifications',$id);
$this->db->where('deleted',0);
$query = $this->db->get('classifications');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'classifications')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('classifications');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}