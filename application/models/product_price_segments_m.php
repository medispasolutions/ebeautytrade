<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_price_segments_m extends CI_Model{

public function get_product_price_segments($id){
$this->db->where('id_product_price_segments',$id);
$this->db->where('deleted',0);
$query = $this->db->get('product_price_segments');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
$this->db->where('id_stock',0);
if ($this->db->field_exists($order, 'product_price_segments')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('product_price_segments');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
$this->db->where('id_stock',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}


public function getAll_cond($cond = array(),$limit = '', $offset = ''){
$this->db->where('deleted',0);
$this->db->where('id_stock',0);
if(!empty($cond))
$this->db->where($cond);
$this->db->order_by("id_product_price_segments");
if($limit != '')
$this->db->limit($limit,$offset);
$query=$this->db->get("product_price_segments");
if($limit != '')
return $query->result_array();
else
return $query->num_rows();
}

}