<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Pages_fct extends CI_Model
{
	    ///////////////////////////////////////////////////////////////////////////////////////
    public function getEvents($cond,$limit="",$offset="",$sort_order="date",$desc="desc")
    {	
		$data=array();
	
	$date_before_week=date("Y-m-d", strtotime("-1 week"));

	
if($sort_order!="country" && !isset($cond['list_as_country']) ){

        $q = "";
        $q .= " select  events.* ";
        $q .= " from events";
        $q .= ' where events.deleted=0  ';
		$q .= ' and events.date >="'.$date_before_week.'"';

        $q .= "  order by ".$sort_order." ".$desc;
    
        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
	
        $query = $this->db->query($q);
		if($limit!=""){
		
			$results = $query->result_array();
			if(!empty($results)){
				$j=0;
			foreach($results as $res){
		
		$q = " select  countries.* ";
        $q .= " from countries join events_countries on countries.id_countries=events_countries.id_countries";
        $q .= ' where events_countries.deleted=0 and events_countries.id_events='.$res['id_events'];
		$q .= " group by countries.id_countries";
		$query = $this->db->query($q);
		$countries = $query->result_array();
		$data[$j]=$res;
		$data[$j]['countries']=$countries;
				$j++;}	
				}
        }else{
		$data = $query->num_rows();}
		}else{
		
		 $q = "";
        $q .= " select  countries.* ";
        $q .= " from events_countries join countries on countries.id_countries=events_countries.id_countries";
		$q .= "  join events on events.id_events=events_countries.id_events";
        $q .= ' where events_countries.deleted=0 and events.deleted=0 ';
		$q .= ' and events.date >="'.$date_before_week.'"';
        $q .= "  group by countries.id_countries ";
        $q .= "  order by countries.title asc";
		
      
        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
	
        $query = $this->db->query($q);
	
		if($limit!=""){
			$results = $query->result_array();
			if(!empty($results)){
				$j=0;
			foreach($results as $res){
		
		$q = " select  events.* ";
        $q .= " from events join events_countries on events.id_events=events_countries.id_events";
        $q .= ' where events_countries.deleted=0 and events.deleted=0 and events_countries.id_countries='.$res['id_countries'];
		$q .= ' and events.date >="'.$date_before_week.'"';
		$q .= " group by events.id_events";
		
	
		$query = $this->db->query($q);
		$events = $query->result_array();
		$data[$j]=$res;
		$data[$j]['events']=$events;
				$j++;}	
				}
				
				
				
        }else{
			
		$data = $query->num_rows();
			}
			
			
			}
        return $data;
    }
	
		    ///////////////////////////////////////////////////////////////////////////////////////
    public function getQuestionsAndAnswers($cond,$limit="",$offset="",$sort_order="created_date",$desc="desc")
    {
		$data=array();

        $q = "";
        $q .= "select  question_and_answer.* ";
        $q .= " from question_and_answer";
        $q .= ' where question_and_answer.deleted=0 ';
		if(isset($cond['status']) && !empty($cond['status'])){
		$status=$cond['status'];
		if($status==4){
		$status=0;}
		 $q .= ' and question_and_answer.status='.$status;
		}
		if(isset($cond['name']) && !empty($cond['name'])){
		 $q .= " and UPPER(name) like '%".strtoupper($cond['name'])."%' ";
		}
		
		if(isset($cond['email']) && !empty($cond['email'])){
		 $q .= " and UPPER(email) like '%".strtoupper($cond['email'])."%' ";
		}
		
		if(isset($cond['question']) && !empty($cond['question'])){
		 $q .= " and UPPER(question) like '%".strtoupper($cond['question'])."%' ";
		}
		
        $q .= "  order by ".$sort_order." ".$desc;
        
        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
	
        $query = $this->db->query($q);
		if($limit!=""){
		$data = $query->result_array();
		}else{
		$data = $query->num_rows();}
        return $data;
    }
	
	///////////////////////////////////////////////////////////////////////////////////////
    public function getSpa($cond,$limit="",$offset="",$sort_order="created_date",$desc="desc")
    {
		$data=array();

        $q = "";
        $q .= " select  opening_a_new_spa.* ";
        $q .= " from opening_a_new_spa";
		if(isset($cond['id_services']) && !empty($cond['id_services'])){
		 $q .= " join services_spa on services_spa.id_spa=opening_a_new_spa.id_opening_a_new_spa ";
		}
		if(isset($cond['id_projects']) && !empty($cond['id_projects'])){
		 $q .= " join projects_type_spa on projects_type_spa.id_spa=opening_a_new_spa.id_opening_a_new_spa ";
		}
		
        $q .= ' where opening_a_new_spa.deleted=0  ';

		if(isset($cond['name']) && !empty($cond['name'])){
		 $q .= " and UPPER(name) like '%".strtoupper($cond['name'])."%' ";
		}
		
		
		
		if(isset($cond['email']) && !empty($cond['email'])){
		 $q .= " and UPPER(email) like '%".strtoupper($cond['email'])."%' ";
		}
		
		if(isset($cond['id_projects']) && !empty($cond['id_projects'])){
		 $q .= " and projects_type_spa.id_projects=".$cond['id_projects'];
		}
		
			if(isset($cond['status']) && !empty($cond['status'])){
				if($cond['status']==2){
					$cond['status']=0;}
		 $q .= " and opening_a_new_spa.read=".$cond['status'];
		}
		
		if(isset($cond['id_services']) && !empty($cond['id_services'])){
		 $q .= " and services_spa.id_services=".$cond['id_services'];
		}
		
	
        $q .= "  order by ".$sort_order." ".$desc;
        
        if ($limit != '') {
			
            $q .= "  limit " . $limit . " offset " . $offset;
        }
	
        $query = $this->db->query($q);
		if($limit!=""){
		$results = $query->result_array();
		$i=0;
		foreach($results as $res){
		$data[$i]=$res;
		$q1 = " select  projects_type.* ";
        $q1 .= " from projects_type";
	
		$q1 .= " join projects_type_spa on projects_type_spa.id_projects=projects_type.id_projects_type  where projects_type_spa.id_spa=".$res['id_opening_a_new_spa'];
		$q1 .= " group by projects_type_spa.id_projects";
		$query1 = $this->db->query($q1);
		$projects = $query1->result_array();
		$data[$i]['projects']= $projects ;
		
		$q2 = " select  services_enquiry.* ";
        $q2 .= " from services_enquiry";
	
		$q2 .= " join services_spa on services_spa.id_services=services_enquiry.id_services_enquiry  where services_spa.id_spa=".$res['id_opening_a_new_spa'];
		$q2 .= " group by services_spa.id_services";
		$query2 = $this->db->query($q2);
		$services = $query2->result_array();
		$data[$i]['services']= $services ;
		$i++;
		}}else{$data = $query->num_rows();}
        return $data;
    }
	
///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getChecklistCategories($cond,$limit="",$offset="")
    {

        $q = "";
        $q .= " select  checklist_categories.*";
        $q .= " from checklist_categories ";

        $q .= ' where checklist_categories.deleted=0 ';


		if(isset($cond['id_user']) && !empty($cond['id_user'])){
		$q .=" and checklist_categories.id_user=".$cond['id_user'];	
			}
			if(isset($cond['status']) && !empty($cond['status'])){
		$q .=" and checklist_categories.status=".$cond['status'];	
			}			

	
			
			
        $q .= "  group by checklist_categories.id_checklist_categories ";
        $q .= "  order by checklist_categories.created_date desc ";
        
        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
	$query = $this->db->query($q);
	if($limit != "") {
		$data = $query->result_array();
	}
	else {
		$data = $query->num_rows();
	}
		
 
        return $data;
    }	
	
public function getUserCheckListCategories($cond, $limit="",$offset=""){
	    $q = "";
        $q .= " select  checklist_categories.*";
        $q .= " from checklist_categories ";
	    $q .= ' where checklist_categories.deleted=0 ';

		if(isset($cond['id_user']) && !empty($cond['id_user'])){
			$q .=" and (checklist_categories.id_user=".$cond['id_user'];
		/*if(isset($cond['set_as_main']) && !empty($cond['set_as_main'])){
		  $q .=" or checklist_categories.set_as_main=".$cond['set_as_main'];	
		  }*/
			$q .=" )";
			}

		if(isset($cond['status']) && !empty($cond['status'])){
		$q .=" and checklist_categories.status=".$cond['status'];	
			}

        $q .= "  group by checklist_categories.id_checklist_categories ";
        $q .= "  order by checklist_categories.created_date desc ";
       
        if ($limit != '') {
			 $q .= "  limit " . $limit . " offset " . $offset;
        }
		
	$query = $this->db->query($q);
	if($limit != "") {
		$data = $query->result_array();
	}
	else {
		$data = $query->num_rows();
	}
	return $data;
	}
	public function select_user_checklist_categories($cond=array(),$type="")
{
 $this->db->select('checklist_categories.*, user_checklist_categories.id_favorites');
 $this->db->from('user_checklist_categories');

 $this->db->join('checklist_categories','user_checklist_categories.id_checklist_categories = checklist_categories.id_checklist_categories');
/* if($type=="unselect" || $type=="select"){
	 $this->db->join('favorites','user_checklist_categories.id_favorites = favorites.id_favorites','left');
 }*/
 $cond3 = array(
  'user_checklist_categories.id_user'=>$cond['id_user'],
  'user_checklist_categories.id_checklist_categories'=>$cond['id_checklist_categories'],
 );

 $this->db->where($cond3);
/*   if($type=="unselect"){
 $this->db->where('favorites.id_favorites NOT IN (select id_favorites from user_checklist_categories where  id_user='.$cond['id_user'].' and id_checklist_categories='.$cond['id_checklist_categories'].')');
  }
  
  if($type=="select"){
 $this->db->where('favorites.id_favorites IN (select id_favorites from user_checklist_categories where  id_user='.$cond['id_user'].' and id_checklist_categories='.$cond['id_checklist_categories'].')');
  }*/
 $this->db->group_by('user_checklist_categories.id_favorites');
 $this->db->order_by('checklist_categories.title');
 $query = $this->db->get();
 $checklist_categories = $query->result_array();



 $results = array();
 foreach($checklist_categories as $category) {
  array_push($results,$category['id_favorites']);
 }

 return $results;
}


	public function insert_user_checklist_categories($cond,$checklist_categories)
{
	$product_quantity=$this->input->post('product_quantity');
	

	$product_quantity_month=$this->input->post('product_quantity_month');
	$type_section=$this->input->post('type_section');
	
 $id_user=$cond['id_user'];
 $id_checklist_categories=$cond['id_checklist_categories'];
 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('user_checklist_categories');
 $cond = array(
  'user_checklist_categories.id_user'=>$id_user,
  'user_checklist_categories.id_checklist_categories'=>$id_checklist_categories,
 );
 $this->db->where($cond);
 $this->db->order_by('user_checklist_categories.id_user_checklist_categories');
 $query = $this->db->get();
 $results = $query->result_array();
 
 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_favorites']);
 }}

 foreach($checklist_categories as $category=>$val) {
  if(!in_array($val,$old_ids)) {
   array_push($new_ids,$val);
  }}

  if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$checklist_categories)) {
    array_push($deleted_ids,$id);
   }
   
   if(isset($product_quantity[$id])){
   $data['qty'] = $product_quantity[$id];
   $data['qty_month'] = $product_quantity_month[$id];
   $data['updated_date'] = date('Y-m-d h:i:s');   
   
  
   $cond['id_user'] = $id_user;
   $cond['id_favorites'] = $id;
   
  if($this->input->post('type_section')!=""){
    $cond['id_checklist_categories'] = $id_checklist_categories;
   $this->db->where($cond);
   $this->db->update('user_checklist_categories',$data);}else{
	$this->db->where($cond);
   $this->db->update('favorites',$data);
      }

   }	   
  }
 }
 /*&& $type_section!='checklist_selected_products' && $type_section!='checklist_unselect_products'*/
 if(!empty($deleted_ids) && $type_section!='checklist_selected_products' && $type_section!='checklist_unselect_products') {
  $q = 'DELETE FROM user_checklist_categories WHERE id_favorites IN ('.implode(',',$deleted_ids).') AND id_user='.$id_user;
  $this->db->query($q);
 }
 
  if(!empty($new_ids)) {
  foreach($new_ids as $new_id) {
   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_checklist_categories'] = $id_checklist_categories;
   $data['id_user'] = $id_user;
   $data['qty'] = $product_quantity[$new_id];
   $data['qty_month'] = $product_quantity_month[$new_id];
   $data['id_favorites'] = $new_id;
   $this->db->insert('user_checklist_categories',$data);
   
  }
 }

  if($this->input->post('bulk_option')=="remove-all-list" && !empty($checklist_categories)) {
 $deleted_ids=$checklist_categories;
 $q = 'DELETE FROM user_checklist_categories WHERE id_favorites IN ('.implode(',',$deleted_ids).') AND id_user='.$id_user;
 $this->db->query($q);  }
}

public function insert_user_checklist_categories2($cond,$checklist_categories)
{
$id_user=$cond['id_user'];
$id_favorites=$cond['id_favorites']; 

  foreach($checklist_categories as $category_id) {

   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_checklist_categories'] = $category_id;
   $data['id_user'] = $id_user;
   $data['id_favorites'] = $id_favorites;
   $this->db->insert('user_checklist_categories',$data);
  }



}

	public function updateStockList($cond,$checklist_categories)
{

	$product_quantity=$this->input->post('product_quantity');
	
	$product_quantity_month=$this->input->post('product_quantity_month');
	$type_section=$this->input->post('type_section');
	
 $id_user=$cond['id_user'];

 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('favorites');
 $cond = array(
  'favorites.id_user'=>$id_user
 );
 $this->db->where($cond);
 $this->db->order_by('favorites.id_favorites');
 $query = $this->db->get();
 $results = $query->result_array();
 
 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_favorites']);
 }}

 foreach($checklist_categories as $category=>$val) {
  if(!in_array($val,$old_ids)) {
   array_push($new_ids,$val);
  }}

  if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$checklist_categories)) {
    array_push($deleted_ids,$id);
   }
   
   if(isset($product_quantity[$id])){
   $data['qty'] = $product_quantity[$id];
   $data['qty_month'] = $product_quantity_month[$id];
   $data['updated_date'] = date('Y-m-d h:i:s');   
   
  
   $cond['id_user'] = $id_user;
   $cond['id_favorites'] = $id;

   $this->db->where($cond);
   $this->db->update('favorites',$data);
   
      }}}
}
		

///////////////////////////////////////////CHECKLIST BRANCHES/////////////////////////////////////////////////////////	
public function getChecklistBranches_old($cond,$limit="",$offset="")
    {

        $q = "";
        $q .= " select  checklist_branches.*";
        $q .= " from checklist_branches ";
		$q .= " join users_addresses on checklist_branches.id_users_addresses=users_addresses.id_users_addresses ";

        $q .= ' where checklist_branches.deleted=0  and users_addresses.deleted=0 ';


		if(isset($cond['id_user']) && !empty($cond['id_user'])){
			$q .=" and checklist_branches.id_user=".$cond['id_user'];
			}
		if(isset($cond['status']) && !empty($cond['status'])){
		$q .=" and checklist_branches.status=".$cond['status'];	
			}
			
			if(isset($cond['branch']) && !empty($cond['branch'])){
		$q .=" and users_addresses.branch=".$cond['branch'];	
			}				

	
			
			
        $q .= "  group by checklist_branches.id_checklist_branches ";
        $q .= "  order by checklist_branches.created_date desc ";
       
        if ($limit != '') {
			
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
	$query = $this->db->query($q);
	if($limit != "") {
		$data = $query->result_array();
	}
	else {
		$data = $query->num_rows();
	}
		
 
        return $data;
    }
	

public function getChecklistBranches($cond,$limit="",$offset="")
    {

        $q = "";
        $q .= " select  checklist_branches.*";
        $q .= " from checklist_branches ";
		$q .= " join users_addresses on checklist_branches.id_users_addresses=users_addresses.id_users_addresses ";

        $q .= ' where checklist_branches.deleted=0  and users_addresses.deleted=0 ';


		if(isset($cond['id_user']) && !empty($cond['id_user'])){
			$q .=" and checklist_branches.id_user=".$cond['id_user'];
			}
		if(isset($cond['status']) && !empty($cond['status'])){
		$q .=" and checklist_branches.status=".$cond['status'];	
			}
			
			if(isset($cond['branch']) && !empty($cond['branch'])){
		$q .=" and users_addresses.branch=".$cond['branch'];	
			}				

	
			
			
        $q .= "  group by checklist_branches.id_checklist_branches ";
        $q .= "  order by checklist_branches.created_date desc ";
       
        if ($limit != '') {
			
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
	$query = $this->db->query($q);
	
	if($limit != "") {
		$data=array();
		$results = $query->result_array();
		$i=0;
		foreach($results as $res){
			$data[$i]=$res;
		$categories=$this->fct->getAll_cond('checklist_categories','id_checklist_categories asc',array('id_checklist_branches'=>$res['id_checklist_branches']));
		$j=0;
	
		foreach($categories as $val){
			
			$data[$i]['category'][$j]=$val;
			
$cond['id_checklist_categories']=$val['id_checklist_categories'];
$selected_checklist_categories = $this->pages_fct->select_user_checklist_categories($cond,'select');
$data[$i]['category'][$j]['selected_checklist_categories']=$selected_checklist_categories;
$j++;
}$i++;}

	}
	else {
		

		$data = $query->num_rows();
	}
		


        return $data;
    }		
	

	public function select_user_checklist_branches($cond=array())
{
 $this->db->select('checklist_branches.*, user_checklist_branches.id_favorites');
 $this->db->from('checklist_branches');
 $this->db->join('user_checklist_branches','user_checklist_branches.id_checklist_branches = checklist_branches.id_checklist_branches');
 $cond = array(
  'user_checklist_branches.id_user'=>$cond['id_user'],
  'user_checklist_branches.id_checklist_branches'=>$cond['id_checklist_branches'],
 );
 $this->db->where($cond);
 $this->db->group_by('user_checklist_branches.id_favorites');
 $this->db->order_by('checklist_branches.title');
 $query = $this->db->get();
 $checklist_branches = $query->result_array();
 
 $results = array();
 foreach($checklist_branches as $category) {
  array_push($results,$category['id_favorites']);
 }
 return $results;
}
	
	public function insert_user_checklist_branches($cond,$checklist_branches)
{
	$product_quantity=$this->input->post('product_quantity');
	$product_quantity_month=$this->input->post('product_quantity_month');
 $id_user=$cond['id_user'];
 $id_checklist_branches=$cond['id_checklist_branches'];
 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('user_checklist_branches');
 $cond = array(
  'user_checklist_branches.id_user'=>$id_user,
  'user_checklist_branches.id_checklist_branches'=>$id_checklist_branches,
 );
 $this->db->where($cond);
 $this->db->order_by('user_checklist_branches.id_user_checklist_branches');
 $query = $this->db->get();
 $results = $query->result_array();

 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_favorites']);
  }
 }

 foreach($checklist_branches as $category=>$val) {
  if(!in_array($val,$old_ids)) {
   array_push($new_ids,$val);
  }
 }

 if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$checklist_branches)) {
    array_push($deleted_ids,$id);
   }else{
   $data['qty'] = $product_quantity[$id];
   $data['qty_month'] = $product_quantity_month[$id];
   $data['updated_date'] = date('Y-m-d h:i:s');   
   $cond['id_checklist_branches'] = $id_checklist_branches;
   $cond['id_user'] = $id_user;
   $cond['id_favorites'] = $id;
   $this->db->where($cond);
   $this->db->update('user_checklist_branches',$data);
	   
	   }
  }
 }
 

 if(!empty($deleted_ids)) {
  $q = 'DELETE FROM user_checklist_branches WHERE id_favorites IN ('.implode(',',$deleted_ids).') AND id_user='.$id_user;
  $this->db->query($q);
 }

 
 if(!empty($new_ids)) {
  foreach($new_ids as $new_id) {
	$data['qty'] = $product_quantity[$new_id];
	$data['qty_month'] = $product_quantity_month[$new_id];
   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_checklist_branches'] = $id_checklist_branches;
   $data['id_user'] = $id_user;
   $data['id_favorites'] = $new_id;
   $this->db->insert('user_checklist_branches',$data);
   
  }
 }
}	


	public function getUserWishlist_byCategories($cond=array())
{
 $this->db->select('favorites.*, user_checklist_categories.qty as qty');
 $this->db->from('user_checklist_categories');
 $this->db->join('favorites','favorites.id_favorites = user_checklist_categories.id_favorites');
 $cond = array(
  'user_checklist_categories.id_user'=>$cond['id_user'],
  'user_checklist_categories.id_checklist_categories'=>$cond['id_checklist_categories'],
 );
 $this->db->where($cond);
 $this->db->group_by('user_checklist_categories.id_favorites');
 $this->db->order_by('favorites.id_favorites');
 $query = $this->db->get();
 $checklist_categories = $query->result_array();


 return $checklist_categories;
}

public function getUserWishlist_byBranches($cond=array())
{
 $this->db->select('favorites.*, user_checklist_branches.qty as qty');
 $this->db->from('user_checklist_branches');
 $this->db->join('favorites','favorites.id_favorites = user_checklist_branches.id_favorites');
 $cond = array(
  'user_checklist_branches.id_user'=>$cond['id_user'],
  'user_checklist_branches.id_checklist_branches'=>$cond['id_checklist_branches'],
 );
 $this->db->where($cond);
 $this->db->group_by('user_checklist_branches.id_favorites');
 $this->db->order_by('favorites.id_favorites');
 $query = $this->db->get();
 $checklist_categories = $query->result_array();


 return $checklist_categories;
}
	


		
	}