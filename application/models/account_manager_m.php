<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account_manager_m extends CI_Model{

public function get_account_manager($id){
$this->db->where('id_account_manager',$id);
$this->db->where('deleted',0);
$query = $this->db->get('account_manager');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  account_manager.* ";
        $q .= " from  account_manager ";
		$q .= " where account_manager.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(account_manager.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}