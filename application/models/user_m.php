<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class user_m extends CI_Model{

public function get_user($id){
$this->db->where('id_user',$id);
$this->db->where('deleted',0);
$query = $this->db->get('user');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'user')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('user');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

    public function getparentuser($cond, $limit = "", $offset = "")
    {
        $q = "";
        $q .= " select  user.* ";
        $q .= " from  user ";

        if (isset($cond["id_user"]) && !empty($cond["id_user"])) {

            $q .= "Where user.id_user = " . $cond["id_user"] . "";
        }
        
        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }

        return $data;
    }
}