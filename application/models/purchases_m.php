<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchases_m extends CI_Model{

public function get_purchases($id){
$this->db->where('id_purchases',$id);
$this->db->where('deleted',0);
$query = $this->db->get('purchases');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset,$cond){
$cond['deleted']=0;
$this->db->where($cond);
if ($this->db->field_exists($order, 'purchases')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('purchases');
return $query->result_array();	
}

public function getAll($table,$order,$cond){
	$cond['deleted']=0;
$this->db->where($cond);
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}