<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Trends_and_updates_m extends CI_Model{

public function get_trends_and_updates($id){
$this->db->where('id_trends_and_updates',$id);
$this->db->where('deleted',0);
$query = $this->db->get('trends_and_updates');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  trends_and_updates.* ";
        $q .= " from  trends_and_updates ";
		$q .= " where trends_and_updates.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(trends_and_updates.brief) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}