<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact_list_m extends CI_Model{

public function get_contact_list($id){
$this->db->where('id_contact_list',$id);
$this->db->where('deleted',0);
$query = $this->db->get('contact_list');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset,$cond){
	if(isset($cond['title'])){
	$this->db->like('UPPER(title)',strtoupper($cond['title']));
	unset($cond['title']);}
	$cond['deleted']=0;
$this->db->where($cond);
if ($this->db->field_exists($order, 'contact_list')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('contact_list');
return $query->result_array();	
}

public function getAll($table,$order,$cond){
	if(isset($cond['title'])){
	$this->db->like('UPPER(title)',strtoupper($cond['title']));
	unset($cond['title']);}
	$cond['deleted']=0;
$this->db->where($cond);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}