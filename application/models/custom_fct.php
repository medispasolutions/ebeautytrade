<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Custom_fct extends CI_Model
{
    public function informProductModificationsToAdmin($product)
    {

        $cond['id_user'] = $this->session->userdata('user_id');
        $user = $this->fct->getonerecord('user', $cond);

        if (checkIfSupplier_admin()) {
            $update_product["published"] = 1;
            $update_product["status"] = 2;
            if ($product['inform_admin'] == 0) {
                $update_product['inform_admin'] = 1;
                $this->load->model("send_emails");
                $this->send_emails->informProductModificationsToAdmin($user, $product);
            }
            $this->db->where("id_products", $product['id_products']);
            $this->db->update('products', $update_product);
        }

    }


    /////////////////////////////////////Suppliers//////////////////////////////////////////////////
    public function getSuppliers($cond, $limit = "", $offset = "")
    {
        $q = "";
        $q .= " select user.*";
        $q .= " from user ";
        $q .= "  join brands on brands.id_user = user.id_user ";

        $q .= ' where user.id_roles=5 and user.status=1 and user.deleted=0 and brands.deleted=0 and brands.status=1';
        // $q .= ' and user.logo!=""  ';
        if (isset($cond['display_in_menu'])) {
            $q .= ' and user.display_in_menu=1  ';
        }

        if (isset($cond['id_brands'])) {
            $q .= ' and brands.id_brands=' . $cond['id_brands'];
        }

        $q .= "  group by user.id_user  order by user.sort_order asc ";
        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
        $query = $this->db->query($q);

        if ($limit != "") {

            $data = $query->result_array();
            if (isset($cond['list'])) {
            }
            $i = 0;
            foreach ($data as $val) {
                $brands = $this->fct->getAll_cond('brands', 'sort_order asc', array('id_user' => $val['id_user']));

                $b = 0;
                foreach ($brands as $brand) {
                    $cond2['id_brands'] = $brand['id_brands'];
                    $sub_categories = $this->custom_fct->getSubCategories3($cond2, 100, 0);
                    $brands[$b]['sub_categories'] = $sub_categories;
                    $b++;
                }

                $data[$i]['brands'] = $brands;
                $i++;


            }
        } else {
            $data = $query->num_rows();
        }


        return $data;

    }

    /////////////////////////////////////STATUS//////////////////////////////////////////////////
    public function getUserPendingMiles($id_user = 0)
    {
        $pending_miles = 0;

        $q = "";
        $q .= " select sum(orders.miles) as miles";
        $q .= " from orders ";
        /*$q .= " left join supplier_info on user.id_user = supplier_info.id_user ";*/

        $q .= ' where orders.deleted=0 and orders.status="pending" and orders.id_user=' . $id_user;

        $query = $this->db->query($q);
        $data = $query->row_array();

        if (isset($data['miles']) && !empty($data['miles'])) {

            $pending_miles = $data['miles'];
        }
        return $pending_miles;
    }

    public function getSubCategories($cond, $limit = "", $offset = 0)
    {
        $data = array();

        $q = "";
        $q .= ' select  categories.* ';
        $q .= " from  categories ";
        $q .= " where categories.deleted=0 ";
        if (isset($cond["id_parent"])) {
            $q .= " and categories.id_parent=" . $cond['id_parent'];
        }
        if (isset($cond["ids_parent"]) && !empty($cond["ids_parent"])) {
            $q .= " and categories.id_parent IN (" . implode(',', $cond['ids_parent']) . ")";
        }
        $q .= "  group by categories.id_categories  order by categories.sort_order asc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
        $query = $this->db->query($q);

        if ($limit != "") {
            $results = $query->result_array();

            $i = 0;
            foreach ($results as $res) {
                //$cond2['category']=$res['id_categories'];

                $id_category = $res['id_categories'];
                /*	$items = $this->custom_fct->getCategoriesPages($id_category,array(),array(),10000,0);
                    $categories_ids = $this->custom_fct->getCategoriesIdsTreeParentLevels($items,$id_category);
                    $cond2['categories'] =  implode(",",$categories_ids);*/
                /*$cond2['category']=$id_category;*/
                $cond2['id_categories'] = $id_category;
                $cond2['status'] = 0;
                $cond2['expire_date'] = date("Y-m-d G:i:s");
                if (isset($cond['id_brands'])) {
                    $cond2['brand'] = $cond['id_brands'];
                }

                if (isset($cond['keywords'])) {
                    $cond2['keywords'] = $cond['keywords'];
                }

                $cc = $this->ecommerce_model->getProducts($cond2);

                if ($cc != 0 || isset($cond['all_categories'])) {
                    $data[$i] = $res;
                    $data[$i]['cc_products'] = $cc;
                    $i++;
                }
            }
        } else {
            $data = $query->num_rows();
        }


        return $data;


    }

    public function getSubCategories3($cond, $limit = "", $offset = 0)
    {
        $data = array();

        $q = "";
        $q .= ' select  categories.* ';
        $q .= " from  products_categories ";
        $q .= " join  categories on products_categories.id_categories=categories.id_categories ";
        $q .= " join  products on products_categories.id_products=products.id_products ";
        $q .= " JOIN brands on brands.id_brands = products.id_brands ";
        $q .= " where categories.deleted=0 and categories.title != brands.title and categories.status = 0";
        if (isset($cond['id_brands'])) {
            $q .= " and products.id_brands=" . $cond['id_brands'] . " and categories.id_categories not in (select id_parent from categories where id_parent!='')";
        }

        $q .= " and categories.deleted=0 ";

        $q .= "  group by products_categories.id_categories  order by categories.sort_order asc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
        $query = $this->db->query($q);

        if ($limit != "") {
            $results = $query->result_array();

            $i = 0;
            foreach ($results as $res) {
                //$cond2['category']=$res['id_categories'];

                $id_category = $res['id_categories'];
                /*	$items = $this->custom_fct->getCategoriesPages($id_category,array(),array(),10000,0);
                    $categories_ids = $this->custom_fct->getCategoriesIdsTreeParentLevels($items,$id_category);
                    $cond2['categories'] =  implode(",",$categories_ids);*/
                /*$cond2['category']=$id_category;*/
                $cond2['id_categories'] = $id_category;
                $cond2['status'] = 0;
                $cond2['expire_date'] = date("Y-m-d G:i:s");
                if (isset($cond['id_brands'])) {
                    $cond2['brand'] = $cond['id_brands'];
                }

                if (isset($cond['keywords'])) {
                    $cond2['keywords'] = $cond['keywords'];
                }

                $cc = $this->ecommerce_model->getProducts($cond2);


                if ($cc != 0 || isset($cond['all_categories'])) {
                    $data[$i] = $res;
                    $data[$i]['cc_products'] = $cc;
                    $i++;
                }
            }

        } else {
            $data = $query->num_rows();
        }


        return $data;


    }

    ////////////////////////////////////BLOGS//////////////////////////////////////////////////
    public function getBlgs()
    {
        $url = 'https://www.ebeautytrade.com/news-and-events/wp-content/themes/flownews/json/listing.php';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //execute
        $result = curl_exec($ch);
        
        $array = json_decode($result);
        $results = array();
        /*$result=jsonToArray($array);*/

        $i = 0;
        foreach ($array as $arr) {

            $categories = $arr->category;
            
            $results[$i]['id'] = $arr->id;
            $results[$i]['title'] = $arr->title;
            $results[$i]['image'] = $arr->image;
            $results[$i]['link'] = $arr->link;
            $results[$i]['formatted_date'] = $arr->formatted_date;
            
            $j = 0;
            foreach ($categories as $category) {
                if($category->name == "Homepage Left"){
                    continue;
                }
                $categoryname = $category->name;
                $j++;
            }
            $results[$i]['category'] = $categoryname;
            
            $i++;
            if ($i == 4) {
                break;
            }
        }

        return $results;
    }
    
    public function getfeaturedBlgs()
    {
        $url = 'https://www.ebeautytrade.com/news-and-events/wp-content/themes/flownews/json/feature-main-product.php';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //execute
        $result = curl_exec($ch);
        
        $array = json_decode($result);
        $results = array();
        /*$result=jsonToArray($array);*/

        $i = 0;
        foreach ($array as $arr) {
            
            $categories = $arr->category;
            
            $results[$i]['id'] = $arr->id;
            $results[$i]['title'] = $arr->title;
            $results[$i]['image'] = $arr->image;
            $results[$i]['link'] = $arr->link;
            $results[$i]['formatted_date'] = $arr->formatted_date;
            
            $j = 0;
            foreach ($categories as $category) {
                if($category->name == "Homepage Left"){
                    continue;
                }
                $categoryname = $category->name;
                $j++;
            }
            $results[$i]['category'] = $categoryname;
            
            $i++;
            if ($i == 4) {
                break;
            }
        }

        return $results;
    }


    /////////////////////////////////////STATUS//////////////////////////////////////////////////
    public function getStatus($status)
    {

        switch ($status) {
            case '1':
                $result = '<span class="label-status label-success">Publish</span>';
                break;

            case '0':
                $result = '<span class="label-status label-important">UnPublish</span>';
                break;

            case '2':
                $result = '<span class="label-status label-important">Under Review</span>';
                break;

            default:
                $result = "";
        }

        return $result;


    }


    /////////////////////////////////////BRANDS//////////////////////////////////////////////////
    public function getBrands($cond, $limit = "", $offset = "")
    {

        $q = "";
        $q .= " select  brands.*";
        $q .= " from user ";
        /*$q .= " left join supplier_info on user.id_user = supplier_info.id_user ";*/
        $q .= " join brands on brands.id_user = user.id_user ";
        $q .= ' where brands.deleted=0';
        if (isset($cond['set_as_featured']) && $cond['set_as_featured'] == 1) {
            $q .= " and brands.set_as_featured=1";
        }
        if (isset($cond['promote_to_front']) && !empty($cond['promote_to_front'])) {
            $q .= " and brands.promote_to_front=" . $cond['promote_to_front'];
        }

        if (isset($cond['logo']) && !empty($cond['logo'])) {
            $q .= " and brands.logo!=''";
        }

        if (isset($cond['id_user']) && !empty($cond['id_user'])) {
            $q .= " and user.id_user=" . $cond['id_user'];
        }

        if (isset($cond['status']) && !empty($cond['status'])) {
            $q .= " and brands.status=" . $cond['status'];
        }

        /*$q .=' and (brands.expire_date >="'.date("Y-m-d").'" or brands.expire_date="0000-00-00" or brands.expire_date="" )	';	*/

        $q .= " and (brands.status=1 or brands.status=2)";
        $q .= "  group by brands.id_brands ";
        $q .= "  order by brands.title asc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }


        $query = $this->db->query($q);
        if ($limit != "") {
            $data = $query->result_array();
        } else {
            $data = $query->num_rows();
        }


        return $data;
    }

    public function getProductsInventoryDetails($cond)
    {


        $q = "";
        if ($cond['type'] == "product") {
            $product = $this->fct->getonerecord('products', array('id_products' => $cond['id_products']));
            $product['brand_name'] = $this->fct->getonecell('brands', 'title', array('id_brands' => $product['id_brands']));
            $id_product_purchases = $product['id_products'];

            $cond_delivery_line_item['id_products'] = $product['id_products'];
            $cond_delivery_line_item['type'] = 'product';

            $delivery_order_line_items = $this->admin_fct->delivery_order_line_items($cond_delivery_line_item);


            $data['orders_line_items'] = $delivery_order_line_items;
        }

        if ($cond['type'] == "stock") {
            $stock = $this->fct->getonerecord('products_stock', array('id_products_stock' => $cond['id_products']));
            $product = $this->fct->getonerecord('products', array('id_products' => $stock['id_products']));
            $stock['brand_name'] = $this->fct->getonecell('brands', 'title', array('id_brands' => $product['id_brands']));
            $stock['title'] = $product['title'];
            $product = $stock;
            $id_product_purchases = $cond['id_products'];

            ///////////////QUANTITIES//////////////


            $cond_delivery_line_item['id_products'] = $stock['id_products_stock'];
            $cond_delivery_line_item['type'] = 'option';

            $delivery_order_line_items = $this->admin_fct->delivery_order_line_items($cond_delivery_line_item);
            $data['orders_line_items'] = $delivery_order_line_items;
        }


        $data['product'] = $product;

        /*		$cond['id_products']=$id_product_purchases;
                $cond['type']=$cond['type'];
                $purchases      = $this->ecommerce_model->getPurchases($cond);
                $data['product']['qty_sold_by_admin']= $purchases['qty_sold_by_admin'];
                $data['product']['qty_payable']= $purchases['qty_payable'];
                $data['product']['purchases']=$purchases['purchases'];*/

        $q2 = "";
        $q2 .= " select  line_items.*, orders.id_orders, orders.status  ";
        $q2 .= " from orders  ";
        $q2 .= " join line_items on line_items.id_orders=orders.id_orders ";
        $q2 .= " where orders.deleted=0 and  line_items.deleted=0 ";

        $q2 .= "and line_items.id_products=" . $product['id_products'];

        if (isset($product['combination']) && !empty($product['combination'])) {
            $q2 .= " and line_items.options_en ='" . $product['combination'] . "'";

        }

        $query2 = $this->db->query($q2);
        $line_items = $query2->result_array();
        $data['line_items'] = $line_items;

        return $data;


    }

    public function getOneBrand($id_brands, $id_user = "")
    {

        $q = "";
        $q .= " select  brands.*, user.id_user, user.email as email ";
        $q .= " from user ";
        $q .= " join brands on brands.id_user = user.id_user ";
        $q .= ' where brands.deleted=0 AND brands.status=1';
        if ($id_user != "") {
        } else {
            $q .= ' and brands.status=1 ';
        }
        $q .= ' and brands.id_brands="' . $id_brands . '"';
        $q .= "  group by brands.id_brands ";
        $q .= "  order by brands.created_date desc ";


        $query = $this->db->query($q);
        $data = $query->row_array();


        return $data;
    }

    public function getCategoryBrandTraining($cond)
    {

        $q = "";
        $q .= " select  training_categories.title, training_categories.id_training_categories ";
        $q .= " from products_training ";
        $q .= " join training_categories on training_categories.id_training_categories = products_training.id_training_categories ";
        //$q .= " join products on products.id_products = brand_certified_training.id_products ";
        $q .= " join brand_certified_training on brand_certified_training.id_brand_certified_training = products_training.id_brand_certified_training ";
        $q .= ' where training_categories.deleted=0 ';

        //$q .= ' and brand_certified_training.set_as_training=1';
        $q .= " group by training_categories.id_training_categories ";
        $q .= " order by training_categories.sort_order asc ";

        $query = $this->db->query($q);
        $data = $query->result_array();
        return $data;
    }

    public function getBanner()
    {
        $q = "";
        $q .= " select  * ";
        $q .= " from banner ";
        $q .= ' where banner.deleted=0 and banner.background!="" and banner.version=1  ';
        $q .= "  order by banner.sort_order asc ";
        $q .= "  limit 8 offset 0 ";
        $query = $this->db->query($q);
        $data = $query->result_array();
        return $data;

    }

    /////////////////////////////////////////CREDITS//////////////////////////////////////////////
    public function getCredits($cond, $limit = "", $offset = "")
    {


        $q = "SELECT concat(id_users_credits,'s:credits','s:Credits') as id, id_users_credits as rand, amount, currency, default_currency, title, created_date  FROM users_credits where deleted =0 and status=1 and id_user= " . $cond['id_user'];

        $q .= " UNION (SELECT concat(id_orders,'s:orders','s:Orders') as id, rand, amount, currency, default_currency, title, created_date FROM orders  where deleted =0 and status='paid' and payment_method='credits' and id_user= " . $cond['id_user'] . ")";


        $q .= " ORDER BY created_date DESC";
        if ($limit != "") {
            $q .= " LIMIT " . $limit . " OFFSET " . $offset;
        }

        $query = $this->db->query($q);
        if ($limit != "") {
            $results = $query->result_array();
        } else {
            $results = $query->num_rows();
        }
        return $results;
    }

    function getBannerAds($page = "", $type = "", $block = "", $categories = "", $user_id = 0)
    {
        $page = str_replace('\'', '\\\'', $page);
        $block = str_replace('\'', '\\\'', $block);
        $categories = str_replace('\'', '\\\'', $categories);
        $q = "SELECT * FROM banner_ads WHERE  expiry_date>='" . date('Y-m-d h:i:s') . "' and deleted = 0";

        if ($page != "")
            $q .= " AND (pages LIKE '%" . $page . "%' || pages LIKE '%all%' )";

        if ($block != "")
            $q .= " AND positions LIKE '%" . $block . "%'";

        if ($categories != "")
            $q .= " AND categories LIKE '%" . $categories . "%'";

        if ($user_id != 0)
            $q .= " AND (id_user = " . $user_id . ")";

        if ($type != "")
            $q .= " AND type='" . $type . "'";

        $q .= " ORDER BY sort_order";

        //echo $q;exit;
        $query = $this->db->query($q);
        $results = $query->result_array();

        if ($page == "inside_ad" && $this->router->method == "details" && empty($results)) {
            $q = "SELECT * FROM banner_ads WHERE deleted = 0";

            if ($page != "")
                $q .= " AND pages LIKE '%" . $page . "%'";

            if ($block != "")
                $q .= " AND positions LIKE '%" . $block . "%'";

            if ($categories != "")
                $q .= " AND categories LIKE '%" . $categories . "%'";

            // if($user_id != 0)
            // $q .= " AND (id_website_users = ".$user_id." OR id_website_users = 0)";

            $q .= " ORDER BY sort_order";

            $query = $this->db->query($q);
            $results = $query->result_array();
        }

        //print '<pre>'; print_r($results); exit;
        return $results;
    }

    /*************************************************************************/
    /*function getCategoriesPages($id_category = 0,$cond = array(), $like = array(), $limit = "", $offset = 0,$json = FALSE)
    {
        $this->db->select("c.*");
        $this->db->from("categories c");
        $this->db->where("c.deleted",0);
        if(!empty($cond))
        $this->db->where($cond);
        if(!empty($like))
        $this->db->like($like);
        $this->db->where("c.id_parent",$id_category);
        $this->db->order_by("c.sort_order");
        if(!empty($limit))
        $this->db->limit($limit,$offset);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if(!empty($limit)) {
            $results = $query->result_array();

            foreach($results as $k => $res) {

                 $results[$k]=$res;
                // get gallery
                $results[$k]['gallery'] = $this->getCategoriesGallery($res['id_categories']);

                // get sub levels
                $results[$k]['sub_levels'] = $this->getCategoriesPages($res['id_categories'],array(),array(),$limit,$offset);

                // get parent levels
                $results[$k]['parent_level'] = $this->getCategoriesTreeParentLevels($res['id_parent']);

            }
        }
        else {
            $results = $query->num_rows();
        }
        return $results;
    }
    */

    function getCategoriesGallery($id)
    {

        $q = "SELECT * FROM categories_gallery WHERE id_categories = " . $id . " ORDER BY sort_order";
        $query = $this->db->query($q);
        $results = $query->result_array();
        return $results;
    }

    function getCategoriesTreeParentLevels($id)
    {
        $this->db->select("*");
        $this->db->from("categories");
        $this->db->where("id_categories", $id);
        $query = $this->db->get();
        $row = $query->row_array();
        /*	if(!empty($row)){
            array_push($arr_cat,$row['id_categories']);
            }*/


        if (!empty($row) && $row['id_parent'] != 0 && $row['id_parent'] != $row['id_categories']) {

            $row['parent_level'] = $this->getCategoriesTreeParentLevels($row['id_parent']);


        }


        return $row;
    }

    function getCategoriesIdsTreeParentLevels($arr, $id_category)
    {

        $string = '';
        //print '<pre>'; print_r($arr); exit;

        $arr_cat = array($id_category);
        foreach ($arr as $row) {
            array_push($arr_cat, $row['id_categories']);
            if (!empty($row['sub_levels'])) {
                foreach ($row['sub_levels'] as $row2) {
                    array_push($arr_cat, $row2['id_categories']);
                    if (!empty($row2['sub_levels'])) {
                        foreach ($row2['sub_levels'] as $row3) {
                            array_push($arr_cat, $row3['id_categories']);
                        }
                    }
                }
            }

        }

        return $arr_cat;

    }


    public function searchWebsite($keywords, $limit = "", $offset = "")
    {
        $sqla = "SELECT concat(products.id_products,'s:products','s:Products') as id, products.title_url ,products.title, products.created_date, products.image as image , products.description FROM products";
        $sqla .= " where  products.status = 0 AND  products.deleted=0 ";
        $sqla .= " AND (UPPER(products.title) like '%" . strtoupper($keywords) . "%' OR UPPER(products.description) like '%" . strtoupper($keywords) . "%' OR UPPER(products.sku) like '%" . strtoupper($keywords) . "%')   group by products.id_products";

        /* $sqla.=" UNION (SELECT concat(id_categories,'s:categories','s:Categories') as id, title_url , title, created_date, image , description  FROM categories where status = 0 AND deleted=0  ";
         $sqla.="AND (UPPER(categories.title) like '%".strtoupper($keywords)."%' OR UPPER(categories.description) like '%".strtoupper($keywords)."%')   ) ";

        $sqla.=" UNION (SELECT concat(id_brands,'s:brands','s:Brands') as id, title_url , title, created_date, logo as image , overview as description  FROM brands where status = 0 AND deleted=0  ";
         $sqla.="AND (UPPER(brands.title) like '%".strtoupper($keywords)."%' OR UPPER(brands.overview) like '%".strtoupper($keywords)."%')   ) ";
         */
        //echo $sqla;exit;
        $sqla .= " ORDER BY created_date DESC";

        if ($limit != "") {
            $sqla .= " LIMIT " . $limit . " OFFSET " . $offset;
        }

        //echo $sqla;exit;
        $query = $this->db->query($sqla);
        if ($limit != "") {
            $results = $query->result_array();

        } else {
            $results = $query->num_rows();
        }

        return $results;
    }


    function displayCategoriesLeftSide($arr, $index, $id = "", $select_categories = "", $id_category = "")
    {
        $string = "";
        //print '<pre>'; print_r($arr); exit;
        foreach ($arr as $k => $ar) {
            $cl = "";

            $string .= '<div class="category_bx" id="category_' . $id_category . '"><a class="category_toggle" id="category_toggle_' . $ar['id_categories'] . '" href="' . route_to('products/category/' . $ar['id_categories']) . '">' . $ar['title'] . '</a></div>';
            if (!empty($ar['sub_levels'])) {

                $string .= $this->displayCategoriesLeftSide($ar['sub_levels'], $index + 1, $id, $select_categories, $ar['id_categories']);

            }
        }

        return $string;
    }

    function getCategoriesPages($id_category = 0, $cond = array(), $like = array(), $limit = "", $offset = 0, $json = FALSE)
    {
        $this->db->select("c.*");
        $this->db->from("categories c");
        $this->db->where("c.deleted", 0);

        if (isset($cond['no_id_categories'])) {
            $this->db->where_not_in('c.id_categories', $cond['no_id_categories']);
            unset($cond['no_id_categories']);
        }

        if (!empty($cond))
            $this->db->where($cond);
        if (!empty($like))
            $this->db->like($like);

        $this->db->where("c.id_parent", $id_category);

        $this->db->order_by("c.sort_order");
        if (!empty($limit))
            $this->db->limit($limit, $offset);
        $query = $this->db->get();


        if (!empty($limit)) {
            $results = $query->result_array();

            foreach ($results as $k => $res) {

                // get gallery
                $results[$k]['gallery'] = $this->getCategoriesGallery($res['id_categories']);
// get listing
                $cond_s = array();
                if (isset($cond['status'])) {

                    $cond_s['status'] = $cond['status'];
                }
                // get sub levels
                $results[$k]['sub_levels'] = $this->getCategoriesPages($res['id_categories'], $cond_s, array(), $limit, $offset);

                // get parent levels
                $results[$k]['parent_level'] = $this->getCategoriesTreeParentLevels($res['id_parent']);

                $results[$k]['parent_level'] = arrangeParentLevels($results[$k]['parent_level']);

            }
        } else {
            $results = $query->num_rows();
        }
        return $results;
    }

///////////////////////////////////////Event Countries\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    public function select_event_countries($id_events, $cond = array())
    {
        $this->db->select('countries.*');
        $this->db->from('countries');
        $this->db->join('events_countries', 'events_countries.id_countries = countries.id_countries');
        $cond = array(
            'events_countries.id_events' => $id_events,
        );

        $this->db->where($cond);
        $this->db->group_by('countries.id_countries');
        $this->db->order_by('countries.title');
        $query = $this->db->get();
        $countries = $query->result_array();

        $results = array();
        foreach ($countries as $country) {
            array_push($results, $country['id_countries']);
        }
        return $results;
    }

    public function insert_event_countries($id_events, $countries)
    {

        $old_ids = array();
        $new_ids = array();
        $deleted_ids = array();
        $this->db->select('*');
        $this->db->from('events_countries');
        $cond = array('id_events' => $id_events);
        $this->db->where($cond);
        $this->db->order_by('events_countries.id_events_countries');
        $query = $this->db->get();
        $results = $query->result_array();

        if (!empty($results)) {
            foreach ($results as $res) {
                array_push($old_ids, $res['id_countries']);
            }
        }


        foreach ($countries as $country => $val) {

            if (!in_array($val, $old_ids)) {
                array_push($new_ids, $val);
            }
        }


        if (!empty($old_ids)) {
            foreach ($old_ids as $id) {
                if (!in_array($id, $countries)) {
                    array_push($deleted_ids, $id);
                }
            }
        }


        if (!empty($deleted_ids)) {
            $q = 'DELETE FROM events_countries WHERE id_countries IN (' . implode(',', $deleted_ids) . ') AND id_events=' . $id_events;
            $this->db->query($q);
        }


        if (!empty($new_ids)) {
            foreach ($new_ids as $new_id) {
                $data['created_date'] = date('Y-m-d h:i:s');
                $data['id_countries'] = $new_id;
                $data['id_events'] = $id_events;
                $this->db->insert('events_countries', $data);
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    public function getMilesUsed($cond)
    {

        $q = "";
        $q .= " select  sum(miles) as miles ";
        $q .= " from users_vouchers ";
        $q .= ' where users_vouchers.deleted=0 and  users_vouchers.id_user=' . $cond['id_user'];

        $query = $this->db->query($q);
        $data = $query->row_array();

        return $data['miles'];
    }

}