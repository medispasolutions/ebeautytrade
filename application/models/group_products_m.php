<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Group_products_m extends CI_Model{

public function get_group_products($id){
$this->db->where('id_group_products',$id);
$this->db->where('deleted',0);
$query = $this->db->get('group_products');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  group_products.* ";
        $q .= " from  group_products ";
		$q .= " where group_products.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(group_products.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}