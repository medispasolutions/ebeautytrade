<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products_stock_m extends CI_Model{

public function get_products_stock($id){
$this->db->where('id_products_stock',$id);
$this->db->where('deleted',0);
$query = $this->db->get('products_stock');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  products_stock.* ";
        $q .= " from  products_stock ";
		$q .= " where products_stock.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(products_stock.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}