<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Call_supplier_m extends CI_Model{

public function get_call_supplier($id){
$this->db->where('id_call_supplier',$id);
$this->db->where('deleted',0);
$query = $this->db->get('call_supplier');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  call_supplier.* ";
        $q .= " from  call_supplier ";
		$q .= " where call_supplier.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(call_supplier.title) like '%".strtoupper($cond["keyword"])."%' ";}
		 
		 if(isset($cond["brand"]) && !empty($cond["brand"])){
		$q .= " and call_supplier.id_brands=".$cond["brand"];	 }
		
			 if(isset($cond["user"]) && !empty($cond["user"])){
		$q .= " and call_supplier.id_user=".$cond["user"];	 }	
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}