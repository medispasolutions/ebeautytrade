<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Consultants_messages_m extends CI_Model{

public function get_consultants_messages($id){
$this->db->where('id_consultants_messages',$id);
$this->db->where('deleted',0);
$query = $this->db->get('consultants_messages');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  consultants_messages.* ";
        $q .= " from  consultants_messages ";
		$q .= " where consultants_messages.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		
		 $q .= " and UPPER(consultants_messages.title) like '%".strtoupper($cond["keyword"])."%' ";}
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}