<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_addresses_m extends CI_Model{

public function get_users_addresses($id){
$this->db->where('id_users_addresses',$id);
$this->db->where('deleted',0);
$query = $this->db->get('users_addresses');
return $query->row_array();
}



///////////////////////////////////////////CHECKLIST CATEGORIES/////////////////////////////////////////////////////////	
public function getRecords($cond,$limit="",$offset="")
    {	
		$q = "";
        $q .= " select  users_addresses.* ";
        $q .= " from  users_addresses ";
		$q .= " where users_addresses.deleted=0 ";

		if(isset($cond["keyword"]) && !empty($cond["keyword"])){
		$q .= " and UPPER(users_addresses.title) like '%".strtoupper($cond["keyword"])."%' ";}
		 
		if(isset($cond["pickup"]) && !empty($cond["pickup"])){
		$q .= " and pickup=".$cond["pickup"];} 
		
        if ($limit != "") {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
		
		$query = $this->db->query($q);
		if($limit != "") {
		$data = $query->result_array();
		}else{$data = $query->num_rows();}
		
 		return $data;
    }	



}