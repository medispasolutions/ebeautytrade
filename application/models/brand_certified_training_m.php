<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Brand_certified_training_m extends CI_Model{

public function get_brand_certified_training($id){
$this->db->where('id_brand_certified_training',$id);
$this->db->where('deleted',0);
$query = $this->db->get('brand_certified_training');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'brand_certified_training')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('brand_certified_training');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

////////////////////////////////////////CATEGORIES\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
public function select_product_training($id_products)
{
 $this->db->select('training_categories.*');
 $this->db->from('training_categories');
 $this->db->join('products_training','products_training.id_training_categories = training_categories.id_training_categories');
 $cond = array(
  'products_training.id_brand_certified_training'=>$id_products,
 );
 $this->db->where($cond);
 $this->db->group_by('training_categories.id_training_categories');
 $this->db->order_by('training_categories.id_training_categories');
 $query = $this->db->get();
 $training = $query->result_array();
 $results = array();
 foreach($training as $category) {
  array_push($results,$category['id_training_categories']);
 }
 return $results;
}
	
public function insert_product_training($id_brand_certified_training,$training)
{

 $old_ids = array();
 $new_ids = array();
 $deleted_ids = array();
 $this->db->select('*');
 $this->db->from('products_training');
 $cond = array('id_brand_certified_training'=>$id_brand_certified_training);
 $this->db->where($cond);
 $this->db->order_by('products_training.id_products_training');
 $query = $this->db->get();
 $results = $query->result_array();
 

 if(!empty($results)) {
  foreach($results as $res) {
   array_push($old_ids,$res['id_training_categories']);
  }
 }
 foreach($training as $category) {
  if(!in_array($category,$old_ids)) {
   array_push($new_ids,$category);
  }
 }
 if(!empty($old_ids)) {
  foreach($old_ids as $id) {
   if(!in_array($id,$training)) {
    array_push($deleted_ids,$id);
   }
  }
 }
 if(!empty($deleted_ids)) {
  $q = 'DELETE FROM products_training WHERE id_training_categories IN ('.implode(',',$deleted_ids).') AND id_brand_certified_training='.$id_brand_certified_training;
  $this->db->query($q);
 }
 
 
 if(!empty($new_ids)) {

  foreach($new_ids as $new_id) {
   $data['created_date'] = date('Y-m-d h:i:s');
   $data['id_training_categories'] = $new_id;
   $data['id_brand_certified_training'] = $id_brand_certified_training;
   $this->db->insert('products_training',$data);
  }
 }
}

}