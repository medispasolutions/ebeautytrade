<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categories_m extends CI_Model{

public function get_categories($id){
$this->db->where('id_categories',$id);
$this->db->where('deleted',0);
$query = $this->db->get('categories');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'categories')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('categories');
return $query->result_array();	
}

public function getAll($table,$order){
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

public function getAll_cond($cond = array(),$limit = '',$offset = '') {
	$this->db->select('*');
	$this->db->from('categories');
	$this->db->where('deleted',0);
	if(!empty($cond))
	$this->db->where($cond);
	$this->db->order_by('sort_order');
	if($limit != '')
	$this->db->limit($limit,$offset);
	$query = $this->db->get();
	//echo $this->db->last_query();exit;
	if($limit != '') {
		$results = $query->result_array();
		if(!empty($results)) {
			foreach($results as $key=>$res) {
				$this->db->select('*');
				$this->db->from('categories');
				$this->db->where('deleted',0);
				$this->db->where('id_parent',$res['id_categories']);
				$this->db->order_by('sort_order');
				$query = $this->db->get();
				$resss = $query->result_array();
				$results[$key]['sub_levels'] = $resss;
			}
		}
	}
	else {
		$results = $query->num_rows();
	}
	return $results;
}
}