<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class supplierimport_model extends CI_Model {

	private $_batchImport;

	public function setBatchImport($batchImport) {
		$this->_batchImport = $batchImport;
	}

	// save data
	public function importData() {
		$this->load->database();
		$data = $this->_batchImport;
		$this->db->insert_batch('products', $data);
	}
	// get employee list
	public function skuisAlreadyExists($sku) {
		$this->load->database();
		$this->db->select('sku');
		$this->db->from('products');
        $this->db->where('sku',$sku);
		$query = $this->db->get();
		return $query->num_rows();
	}
    // get product id
	public function getproductidfromsku ($sku) {
		$this->load->database();
		$q = 'select * from products where `sku` = "'.$sku.'"';
		$query = $this->db->query($q);
		$data = $query->result_array();
		return $data[0]['id_products'];
	}
	function getimg($url, $saveto) {                
        //$user_agent = 'php';         
        $ch = curl_init($url);                
        curl_setopt($ch, CURLOPT_HEADER, 0);         
        //curl_setopt($ch, CURLOPT_USERAGENT, $user_agent); //check here         
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);         
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);         
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $return = curl_exec($ch);         
        curl_close($ch);
    	if(file_exists($saveto)){
       		unlink($saveto);
    	}
        $fp = fopen($saveto,'x');
        fwrite($fp, $return);
        fclose($fp);         
        return $return;     
    } 
}

?>
