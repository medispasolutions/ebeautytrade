<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Attribute_options_m extends CI_Model{

public function get_attribute_options($id){
$this->db->where('id_attribute_options',$id);
$this->db->where('deleted',0);
$query = $this->db->get('attribute_options');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset,$cond = array()){
$this->db->where('deleted',0);
$this->db->where('lang',$this->lang->lang());
if(!empty($cond))
$this->db->where($cond);
if ($this->db->field_exists($order, 'attribute_options')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('attribute_options');
return $query->result_array();	
}

public function list_All($order,$cond = array()){
$this->db->where('deleted',0);
if(!empty($cond))
$this->db->where($cond);
if ($this->db->field_exists($order, 'attribute_options')){
$this->db->order_by($order); }
$query=$this->db->get('attribute_options');
return $query->result_array();	
}

public function getAll($table,$order,$cond = array()){
$this->db->where('deleted',0);
$this->db->where('lang',$this->lang->lang());
if(!empty($cond))
$this->db->where($cond);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}