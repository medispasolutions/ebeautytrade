<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Consultants_m extends CI_Model{

public function get_consultants($id){
$this->db->where('id_consultants',$id);
$this->db->where('deleted',0);
$query = $this->db->get('consultants');
return $query->row_array();
}


public function list_paginate($order,$limit, $offset,$cond){
	
	if(isset($cond['keyword'])) {
		$this->db->like('UPPER(name)',strtoupper($cond['keyword']));
		unset($cond['keyword']);
	}
	
$this->db->where('deleted',0);
if ($this->db->field_exists($order, 'consultants')){
$this->db->order_by($order); }
$this->db->limit($limit,$offset);
$query=$this->db->get('consultants');
return $query->result_array();	
}

public function getAll($table,$order,$cond,$like){
	
	if(isset($cond['keyword'])) {
		$this->db->like('UPPER(name)',strtoupper($cond['keyword']));
		unset($cond['keyword']);
	}
	
$this->db->where('deleted',0);
if ($this->db->field_exists($order, $table)){
$this->db->order_by($order); }
$query=$this->db->get($table);
return $query->num_rows();
}

}