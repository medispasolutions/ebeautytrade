<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ecommerce_model extends CI_Model
{

    /**
     * @param int|string $product_url Product name (from url) or it's id
     * @param array $cond
     * @param array $cond_list
     * @return mixed
     */
    public function getOneProduct($product_url, $cond = array(), $cond_list = array())
    {

        $this->db->select('products.*');
        $this->db->from('products');
        $this->db->where('products.deleted', 0);

        if (is_numeric($product_url)) {
            $cond['products.id_products'] = $product_url;
            $this->db->where($cond);
        } else {
            $cond['products.title_url' . getFieldLanguage($this->lang->lang())] = $product_url;
            $this->db->where($cond);
        }

        $this->db->order_by('products.sort_order');
        $query = $this->db->get();

        $results = $query->row_array();
        $data = $this->updateProduct($results);

        if ((int)$data['set_general_miles'] == 0) {
            $data['miles'] = 0;
        } else {
            $data['miles'] = round($data['miles'], 2);
        }

        if ((int)$data['set_as_redeemed_by_miles'] == 0) {
            $data['redeem_miles'] = 0;
        }

        if (!empty($results)) {
            if (!isset($cond_list['list'])) {

                $this->db->select('categories.*');
                $this->db->from('categories');
                $this->db->join('products_categories', 'products_categories.id_categories = categories.id_categories');
                $cnd = array('products_categories.id_products' => $results['id_products']);
                $this->db->where($cnd);
                $this->db->order_by('categories.sort_order');
                $query = $this->db->get();
                $categories = $query->result_array();
                $data['categories'] = $categories;

                // product brand
                $this->db->select('brands.*');
                $this->db->from('brands');
                $cnd = array(
                    'id_brands' => $results['id_brands']
                );
                $this->db->where($cnd);
                $query = $this->db->get();
                $type = $query->row_array();
                $data['brand'] = $type;
                // product user
                if (isset($type['id_user'])) {
                    $this->db->select('user.*');
                    $this->db->from('user');
                    $cnd = array(
                        'id_user' => $type['id_user']
                    );
                    $this->db->where($cnd);
                    $query = $this->db->get();
                    $user = $query->row_array();
                } else {
                    $user = array();
                }
                $terms = array();
                if (!empty($user)) {
                    $terms = getTerms($user);
                }

                $data['user'] = $user;
            }

            //training specific (not strictly a product)
            if (isset($cond_list['set_as_training'])) {
                //  $q .= ", products.datasheet as datasheet, brand_certified_training.duration as duration, brand_certified_training.title as training_title, brand_certified_training.description as training_description, brand_certified_training.image as training_image, brands.logo as brands_image, brands.title as brands_title, brand_certified_training.id_brand_certified_training, brand_certified_training.set_as_training as set_as_training_n ";

                $brand_certified_training = $this->fct->getonerecord('brand_certified_training', array('id_products' => $results['id_products']));
                if (!empty($brand_certified_training)) {
                    $data['duration'] = $brand_certified_training['duration'];
                    $data['training_title'] = $brand_certified_training['title'];
                    $data['training_description'] = $brand_certified_training['description'];
                    $data['training_image'] = $brand_certified_training['image'];
                    $data['set_as_training_n'] = $brand_certified_training['set_as_training'];
                    $data['brands_image'] = $type['logo'];
                    $data['id_brand_certified_training'] = $brand_certified_training['id_brand_certified_training'];
                }
            }
            $cond['id_products'] = $data['id_products'];
            $data['products_stock'] = array();
            if (isset($cond_list['with_products_stock'])) {
                $data['products_stock'] = $this->getProductsStock($cond);
            }

            // get gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $cnd = array('id_products' => $results['id_products']);
            $this->db->where($cnd);
            $this->db->order_by('sort_order');
            $query = $this->db->get();
            $gallery = $query->result_array();
            $data['gallery'] = $gallery;
            // get information
            if (!isset($cond_list['list'])) {
                $this->db->select('product_information.*');
                $this->db->from('product_information');
                $cnd = array(
                    'id_products' => $results['id_products'],
                    'deleted' => 0
                );
                $this->db->where($cnd);
                $query = $this->db->get();
                $informations = $query->result_array();
                $new_informations['id_product_information'] = 0;
                $new_informations['title'] = 'Description';
                $new_informations['description'] = $results['description'];
                $new_informations['video'] = $results['youtube'];
                $merge_informations[0] = $new_informations;
                $i = 0;
                foreach ($informations as $tab) {
                    $i++;
                    $merge_informations[$i] = $tab;
                }
                $data['informations'] = $merge_informations;
            }


            $attributes = array();
            if (!isset($cond['set_as_training'])) {

                $attributes = $this->getProductOptions($results['id_products']);
                $data['attributes'] = $attributes;
            }
            if (!isset($cond_list['list'])) {
                $products_categories = $this->getProductCategories($results['id_products']);
                $data['products_categories'] = $products_categories;
            }

            if (isset($cond_list['inventory']) && $cond_list['inventory'] == 1) {

                $cond_inventory_2['id_products'] = $res['id_products'];
                $purchases = $this->getPurchases($cond_inventory_2);
                $data[$j]['qty_sold_by_admin'] = $purchases['qty_sold_by_admin'];
                $data[$j]['qty_payable'] = $purchases['qty_payable'];
            }


            // get stock
            if (!empty($attributes)) {

                $data['first_available_stock'] = $this->getProductStock(array('id_products' => $data['id_products'], 'available_in_stock' => 1), 1);
                if (empty($data['first_available_stock'])) {
                    $data['first_available_stock'] = $this->getProductStock(array('id_products' => $data['id_products']), 1);
                }

                //Check Miles
                $check = $this->fct->getonecell('products_stock', 'miles', array('id_products' => $data['id_products'], 'set_general_miles' => 1, 'miles >' => 0));

                if (!empty($check)) {
                    $data['set_general_miles'] = 1;
                } else {
                    $data['set_general_miles'] = 0;
                }

                //Check Miles
                $check = $this->fct->getonecell('products_stock', 'redeem_miles', array('id_products' => $data['id_products'], 'set_as_redeemed_by_miles' => 1, 'redeem_miles >' => 0));
                if (!empty($check)) {
                    $data['set_as_redeemed_by_miles'] = 1;
                } else {
                    $data['set_as_redeemed_by_miles'] = 0;
                }

            }
        }
        return $data;
    }

    public function upload_gal_image($content_type, $id_gallery, $type)
    {

        if (isset($_FILES['gallery']["name"][0]) && $_FILES['gallery']["name"][0] != "") {


            foreach ($_FILES['gallery'] as $key => $val) {
                $i = 1;
                foreach ($val as $v) {
                    $field_name = "file_" . $i;
                    $_FILES[$field_name][$key] = $v;
                    $i++;
                    if ($i > 5)
                        break;
                }
            }


            $type_record = $this->fct->getonerow('content_type', array('name' => $content_type));
            $thumb_vals = array();
            if (!empty($type_record['thumb_val_gal'])) {
                $thumb_vals = explode(',', $type_record['thumb_val_gal']);
            }
            $s = 0;
            unset($_FILES['gallery']);
            if ($type == "image") {

                foreach ($_FILES as $field_name => $file) {
                    $s++;

                    $image_name = $this->fct->uploadImage($field_name, $content_type . "/gallery");

                    if (!empty($thumb_vals)) {
                        foreach ($thumb_vals as $val) {
                            if ($val != '') {
                                $this->fct->createthumb1($image_name, $content_type . "/gallery", $val);
                            }
                        }
                    }

                    //$id_fiche = $this->input->post('id_fiche');
                    //$title = $this->input->post('gallery_name');
                    $data = array(
                        'created_date' => date('Y-m-d H:i:s'),
                        'image' => $image_name,
                        'type' => $type,
                        'id_' . $content_type => $id_gallery,
                        'sort_order' => $s,
                    );

                    $this->db->insert($content_type . "_gallery", $data);

                    unset($_FILES[$field_name]);
                }
                //$_FILES = array();


                //print '<pre>';print_r($thumb_vals);exit;

            }
        }
    }

    /**
     * @param array $product
     * @param string $type
     * @return mixed Stub. Always returns same thing it received
     */
    function updateProduct($product, $type = "product")
    {
        $data = $product;
        return $product;
        if ($type == "stock") {
            $product['id_group_categories'] = $this->fct->getonecell('products', 'id_group_categories', array('id_products' => $product['id_products']));
        }
        if ($this->session->userdata('group_category') == 2 && ($product['id_group_categories'] == 2 || empty($product['id_group_categories']))) {
            $data['price'] = $product['retail2_price'];

            $data['list_price'] = $product['retail_list_price'];
            $data['discount'] = $product['retail_discount'];
            $data['discount_expiration'] = $product['retail_discount_expiration'];
            $data['discount_status'] = $product['retail_discount_status'];
        }

        return $data;

    }

    public function updateGroupCategoryProducts($id_category = 0)
    {
        $this->session->unset_userdata('group_category');
        if (!empty($id_category)) {
            $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($id_category);
            $parent_levels = arrangeParentLevels($parent_levels);
            $parent_ids = $this->ecommerce_model->getIds('id_categories', $parent_levels);
            if (isset($parent_ids[0]) && !empty($parent_ids[0])) {
                $group_category = $this->fct->getonecell('categories', 'id_group_categories', array('id_categories' => $parent_ids[0]));
                $this->session->set_userdata('group_category', $group_category);
            }
        } else {
            if (checkUserIfLogin()) {
                $user = $this->ecommerce_model->getUserInfo();
                if ($user['id_roles'] == 2) {

                    $this->session->set_userdata('group_category', 2);
                } else {
                    $this->session->set_userdata('group_category', 1);
                }
            } else {
                $this->session->set_userdata('group_category', 2);
            }
        }
    }


    public function getIds($field = "", $arr)
    {
        $ids = array();
        foreach ($arr as $val) {
            array_push($ids, $val[$field]);
        }
        return $ids;
    }

    public function checkUserFavorites($id_product)
    {
        $favorites = array();
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();


            $q = " SELECT favorites.id_favorites from favorites ";
            //$q .= " join favorites on user_checklist_categories.id_favorites=favorites.id_favorites ";
            $q .= " where favorites.id_products=" . $id_product;
            $q .= " and favorites.id_user=" . $user['id_user'];

            $query = $this->db->query($q);
            $favorites = $query->result_array();
        }
        $favorites = array();
        return $favorites;
    }

    public function checkProductSegements($id_product)
    {
        $multi_prices = array();
        if (checkUserIfLogin()) {
            $user = $this->ecommerce_model->getUserInfo();


            $q = " SELECT product_price_segments.id_product_price_segments from product_price_segments ";
            $q .= " where product_price_segments.deleted=0 and product_price_segments.id_products=" . $id_product;

            $query = $this->db->query($q);
            $multi_prices = $query->result_array();
        }

        return $multi_prices;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function add_to_cart($post)
    {
        $result = 0;
        $txt_result = "";
        $rand = rand();
        $id_brands = "";
        $redeem_bool = true;
        $guest = true;

        //checkProductGroup returns always true, and $guest is always true, so this is not achieved
        if (!checkProductGroup() && $guest == false) {
            /*
            $this->session->set_userdata('redirect_link', $_SERVER['HTTP_REFERER']);
            $this->session->set_flashdata('error_message', lang('log_to_buy'));

            $return['result'] = 0;
            $return['redirect_link'] = route_to('user/login');
            die(json_encode($return));
            */
        } else {
            if (isset($post['redeem']) && $post['redeem'] == 1) {
                $redeem = 1;
            } else {
                $redeem = 0;
            }
            $options_p = $this->input->post('');

            // load cart content after days
            $cart_items = $this->cart->contents();

            if (empty($cart_items)) {
                $cart_items = $this->ecommerce_model->loadUserCart();
            }

            $lang = 'en';
            $id = $post['product_id'];
            $stock_text = '';
            $product = $this->ecommerce_model->getOneProduct($id);

            //this is what decides who is the shown deliverer in checkout
            if (true || (isset($product['brand']['deliver_by_supplier']) && $product['brand']['deliver_by_supplier'] == 1)) {
                $id_brands = $product['brand']['id_brands'];
            }

            $stock_status = $product['stock_status'];


            //	print '<pre>';print_r($product);exit;
            $list_price = $product['list_price'];
            $discount_expiration = $product['discount_expiration'];
            $price = $product['price'];

            $redeem_miles = 0;
            $miles = 0;
            $inhouse_quantity = $product['quantity'];

            //quantity of everything is set to high, as we don't have stock
            $inhouse_quantity = 999;

            $options = array();
            $options_p = array();
            if ($product['set_as_redeemed_by_miles'] == 1 && $redeem == 1) {
                $redeem_miles = $product['redeem_miles'];
            }
            $options['redeem_miles'] = $redeem_miles;
            if ($product['set_general_miles'] == 1 && $redeem != 1) {
                $miles = round($product['miles']);
            }

            $options['miles'] = $miles;

            if (isset($post['options'])) {

                $options_p = $post['options'];
                $results = $this->ecommerce_model->getOptionsByIds($options_p);
                $combination = serializecartOptions($results);

                //we don't have stock
                //$stock = $this->fct->getonerow('products_stock', array('id_products' => $id, 'combination' => $combination));
                //$stock = $this->updateProduct($stock, 'stock');
                $stock = array(
                    //'id_products_stock' => '?', //this is used further to check price segment
                );

                $options['product_options'] = $options_p;

                $combination_arr = unSerializeStock($combination);
                $i = 0;
                foreach ($combination_arr as $ar) {
                    $stock_text .= $results[$i]['title' . getFieldLanguage($lang)] . ' ';
                    $i++;
                }
            }


            //echo $inhouse_quantity;exit;
            //echo $price;exit;
            $qty = 1;
            if (isset($post['qty'])) {
                $qty = $post['qty'];
            }

            $price = displayCustomerPrice($list_price, $discount_expiration, $price);


            //echo $qty;exit;
            $flag = TRUE;
            $dataTmp = $this->cart->contents();
            foreach ($dataTmp as $item) {
                $ok = 0;
                if (!empty($options_p)) {

                    if ($item['id'] == $id && isset($item['options']['product_options']) && $item['options']['product_options'] == $options_p && (($item['options']['redeem_miles'] != "" && $redeem == 1) || ($item['options']['redeem_miles'] == "" && $redeem == 0))) {
                        $ok = 1;

                    }
                } else {
                    if ($item['id'] == $id && (($item['options']['redeem_miles'] != "" && $redeem == 1) || ($item['options']['redeem_miles'] == "" && $redeem == 0))) {
                        $ok = 1;
                    }
                }
                if ($ok == 1) {
                    $newQTY = $item['qty'] + $qty;

                    $flag = FALSE;
                    $newrowid = $item['rowid'];
                    $data = array(
                        'rowid' => $item['rowid'],
                        'qty' => $newQTY
                    );

                    // check price segment
                    $id_stock = 0;
                    if (isset($stock) && !empty($stock))
                        $id_stock = $stock['id_products_stock'];
                    $price_segment = $this->ecommerce_model->checkPriceSegment($id, $id_stock, $newQTY);

                    if ($price_segment != '' && $price_segment < $price) {
                        $price = $price_segment;
                        //$data['price'] = $price;
                    }

                    /*if($general_miles>=0 && $general_miles<=100 && $set_general_miles==1){
	$miles=getMiles($general_miles,$price);
}*/
                    $options['miles'] = $miles;
                    $options['id_brands'] = $id_brands;
                    if (!empty($options)) {
                        $data['options'] = $options;
                    }


                    if ($this->input->post('redeem') == 1) {

                        $result_miles = $this->updateMilesRedeem($newrowid, 1, $newQTY, $redeem_miles);
                        if ($result_miles['result'] == 0) {
                            $result = 0;
                            $redeem_bool = false;
                            $txt_result = $result_miles['message'] . '<br>';
                        }
                    }

                    $data['price'] = $price;
                    // end check price segment
                    if ((($inhouse_quantity < $newQTY) && (empty($stock_status) || $stock_status == 0)) || !$redeem_bool) {
                        if ($redeem_bool) {
                            $data3[$rand] = $data;
                            $data3[$rand]['id_products'] = $id;

                            $error_text = 'The Quantity you are asking for "' . $product['title' . getFieldLanguage($lang)] . '" is not available for now, <a onclick="sendInquiry("' . route_to('cart/sendInquiry/' . $rand) . '")" >Click Here</a> to inquiry.';
                            $this->session->set_userdata('product_stock_inquiry', $data3);
                            //$this->session->set_flashdata('error_message',$error_text);
                            $txt_result .= $error_text;
                        } else {

                        }


                        /*$this->session->set_flashdata('error_message',lang('stock_not_available').' '.$product['title'.getFieldLanguage($lang)].' '.$stock_text);*/
                    } else {

                        $this->cart->update($data);
                        // update in database: shopping cart
                        $update['updated_date'] = date('Y-m-d H:i:s');
                        $update['qty'] = $newQTY;
                        $update['miles'] = $miles;
                        $update['id_brands'] = $id_brands;
                        $update['price'] = $price;
                        $cond_uu = array();
                        $cond_uu['rowid'] = $item['rowid'];
                        if ($this->session->userdata('login_id') == '') {
                            $cond_uu['sessionid'] = $this->session->userdata('session_id');
                        } else {
                            $cond_uu['id_user'] = $this->session->userdata('login_id');
                        }

                        $result = 1;
                        $this->db->update('shopping_cart', $update, $cond_uu);

                        // end update in database: shopping cart
                        // update in database: stock
                        $comb = '';
                        if (isset($combination)) $comb = $combination;
                        //echo $newQTY;exit;
                        $txt_result .= $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . lang('cart_updated');
                        // end update in database: stock
                        //$this->session->set_flashdata('success_message',$product['title'.getFieldLanguage($lang)].' '.$stock_text.lang('cart_updated'));
                    }
                    break;
                }
            }
            ///echo 'aaa'.$price;exit;
            if ($flag) {

                $newQTY = $qty;
                $data = array(
                    'id' => $id,
                    'qty' => $newQTY,
                    'price' => $price,
                    'name' => 'product'
                );


                // check price segment
                $id_stock = 0;
                if (isset($stock) && !empty($stock))
                    $id_stock = $stock['id_products_stock'];
                $price_segment = $this->ecommerce_model->checkPriceSegment($id, $id_stock, $newQTY);

                if ($price_segment != '' && $price_segment < $price) {
                    $price = $price_segment;
                    $data['price'] = $price;
                }

                /*if($general_miles>=0 && $general_miles<=100 && $set_general_miles==1){
	$miles=getMiles($general_miles,$price);
}*/
                $options['miles'] = $miles;
                $options['id_brands'] = $id_brands;
                if (!empty($options)) {
                    $data['options'] = $options;

                }

                if ($this->input->post('redeem') == 1) {
                    $redeem_bool = true;


                    $result_miles = $this->updateMilesRedeem('', 1, $newQTY, $redeem_miles, true);

                    if ($result_miles['result'] == 0) {

                        $result = 0;
                        $redeem_bool = false;
                        $txt_result = $result_miles['message'];
                    }
                }


                // end check price segment
                if ((($inhouse_quantity < $newQTY) && (empty($stock_status) || $stock_status == 0)) || !$redeem_bool) {

                    if ($redeem_bool) {
                        $data3[$rand] = $data;
                        $data3[$rand]['id_products'] = $id;

                        $error_text = 'The Quantity you are asking for "' . $product['title' . getFieldLanguage($lang)] . '" is not available for now, <a onclick="sendInquiry(' . route_to('cart/sendInquiry/' . $rand) . ')" >Click Here</a> to inquiry.';
                        $this->session->set_userdata('product_stock_inquiry', $data3);
                        //$this->session->set_flashdata('error_message',$error_text);
                        $txt_result .= $error_text;
                    }
                } else {


                    if ($redeem_miles == "") {
                        $redeem_miles = 0;
                    }


                    $newrowid = $this->cart->insert($data);
                    if ($redeem == 1) {
                        $result_miles = $this->updateMilesRedeem($newrowid, 1, $newQTY, $redeem_miles);
                    }
                    // insert in database: shopping cart

                    $insert['rowid'] = $newrowid;
                    $insert['created_date'] = date('Y-m-d H:i:s');
                    $insert['id_products'] = $id;
                    $insert['sessionid'] = $this->session->userdata('session_id');
                    $insert['qty'] = $newQTY;
                    $insert['price'] = $price;
                    $insert['miles'] = $miles;
                    $insert['id_brands'] = $id_brands;
                    $insert['redeem_miles'] = $redeem_miles;
                    $insert['name'] = 'product';
                    if ($this->session->userdata('login_id') != '') {
                        $insert['id_user'] = $this->session->userdata('login_id');
                    }
                    if (!empty($options_p)) {
                        $results = $this->ecommerce_model->getOptionsByIds($options_p);
                        $insert['options'] = serializecartOptions($results);
                    }


                    $this->db->insert('shopping_cart', $insert);
                    // end insert in database: shopping cart
                    // update in database: stock
                    $comb = '';
                    if (isset($combination)) $comb = $combination;
                    /*$this->ecommerce_model->UpdateProductStockInDatabase($id,$inhouse_quantity,0,$newQTY,$comb);*/
                    // end update in database: stock
                    $txt_result .= $product['title' . getFieldLanguage($lang)] . ' ' . $stock_text . ' ' . lang('cart_added');
                    $result = 1;
                    // $this->session->set_flashdata('success_message',$product['title'.getFieldLanguage($lang)].' '.$stock_text.' '.lang('cart_added'));
                }
            }


            if (!isset($post['ajax'])) {

                /*redirect($_SERVER['HTTP_REFERER']);*/
                $return['result'] = $result;
                $return['id'] = $id;
                if ($redeem_miles > 0 && $result == 1) {
                    /*			$this->session->set_flashdata('success_message',$txt_result);
			$return['message'] =lang('system_is_redirecting_to_page');
			$return['redirect_link']=route_to('cart')*/;
                    $user = $this->getUserInfo();
                    $cartPrices = $this->getCheckoutAttr();
                    $user_miles = $user['miles'] - $cartPrices['total_redeem_miles'];
                    if ($user_miles > 0) {
                        $available_miles = " (" . $user_miles . ")";
                    } else {
                        $available_miles = " (0)";
                    }

                    $return['remaining_miles'] = $available_miles;
                    $return['return_popup_message'] = $txt_result;
                    $return['message'] = "";
                } else {
                    $return['return_popup_message'] = $txt_result;
                    $return['message'] = "";
                }
                $return['count_items'] = $this->cart->total_items();;
                die(json_encode($return));
            } else {

                return $txt_result;
            }
        }
    }

    public function updateOrderQuantities($cond)
    {
        $rand = $cond['rand'];
        $id_order = $cond['id_orders'];
        $order = $this->getOrder($id_order, array('rand' => $rand, 'all_line_items' => 1));
        $this->createLpoInvoices($order);

        //////////////////////////UPDATE QUANTITIES/////////////
        if (!empty($order)) {
            $line_items = $order['line_items'];
            foreach ($line_items as $line_item) {
                $stock_status = $line_item['stock_status'];
                if ($stock_status != 4) {
                    $this->ecommerce_model->UpdateProductStockInDatabase2($line_item['id_line_items']);

                    //////////////////Threshold//////
                    if (!empty($line_item['options_en'])) {
                        $cond_p['combination'] = $line_item['options_en'];
                        $cond_p['id_products'] = $line_item['id_products'];
                        $product_tshold = $this->fct->getonerow('products_stock', $cond_p);
                    } else {
                        $cond_p = array();
                        $cond_p['id_products'] = $line_item['id_products'];
                        $product_tshold = $this->fct->getonerow('products', $cond_p);

                    }
                    $product_tshold['quantity'] = $product_tshold['quantity'] - $line_item['quantity'];

                    if ($product_tshold['quantity'] < 0) {
                        $product_tshold['quantity'] = 0;
                    }
                    $this->admin_fct->checkThreshold($product_tshold);

                }
            }

        } else {
            return false;
        }


    }

    public function createLpoInvoices($order, $admin = 0)
    {
//This function to create lpo invoices for stockout which is not in inventory
        $line_items = $order['line_items'];
        foreach ($line_items as $line_item) {
            $stock_out_quantity = 0;
            $id_line_item = $line_item['id_line_items'];
            if ($admin == 1) {
                $line_item = $this->fct->getonerecord('group_products_line_items', array('id_group_products_line_items' => $id_line_item));
                $combination = $line_item['options'];

                $line_item['id_line_items'] = $line_item['id_group_products_line_items'];
            } else {
                $line_item = $this->fct->getonerecord('line_items', array('id_line_items' => $id_line_item));
                $combination = $line_item['options_en'];
            }
            $line_item['admin'] = 1;
            $newQty = $line_item['quantity'];
            $id_products = $line_item['id_products'];
            $stock_status = $line_item['stock_status'];
            //echo $updatedQty;exit;
            if ($combination != '') {
                $stock = $this->fct->getonerecord('products_stock', array('id_products' => $id_products, 'combination' => $combination));
                $updatedQty = $stock['quantity'] - $newQty;

            } else {

                $product = $this->fct->getonerecord('products', array('id_products' => $id_products));
                $updatedQty = $product['quantity'] - $newQty;

            }
            $stock_out_quantity = 0;
            if ($updatedQty < 0 && $stock_status == 1) {
                $stock_out_quantity = -$updatedQty;
            }

            if ($stock_status == 2) {
                $stock_out_quantity = $newQty;
            }

            $update_line_item['stock_out_quantity'] = $stock_out_quantity;
            $this->db->where(array('id_line_items' => $line_item['id_line_items']));
            $this->db->update('line_items', $update_line_item);

        }
        $rand = $order['rand'];
        $id_order = $order['id_orders'];
        $order = $this->getOrder($id_order, array('rand' => $rand, 'all_line_items' => 1));
        $this->createAndSendLpoInvoices($order);
        return true;
    }

    public function createAndSendLpoInvoices($order)
    {

        ///////////////GET SUPPLIERS/////////////
        $q = 'select brands.id_user from products ';
        $q .= ' join line_items on products.id_products=line_items.id_products ';
        $q .= ' join brands on products.id_brands=brands.id_brands ';
        $q .= ' where  line_items.id_orders=' . $order['id_orders'];
        $q .= ' and  line_items.stock_out_quantity>0';
        $q .= ' group by brands.id_user order by brands.id_user ';
        $query = $this->db->query($q);
        $suppliers = $query->result_array();

        foreach ($suppliers as $supplier) {
            $q = 'select line_items.*, brands.id_brands  from products ';
            $q .= ' join line_items on products.id_products=line_items.id_products ';
            $q .= ' join brands on products.id_brands=brands.id_brands ';
            $q .= ' where  line_items.id_orders=' . $order['id_orders'];
            $q .= ' and  line_items.stock_out_quantity>0';
            $q .= ' and brands.id_user=' . $supplier['id_user'];
            $q .= ' group by line_items.id_line_items order by line_items.id_line_items ';
            $query = $this->db->query($q);
            $line_items = $query->result_array();


            ////////////////////CREATE LPO INVOICE//////////////////
            $rand = rand();
            $site_info = $this->fct->getonerow('settings', array('id_settings' => 1));
            $delivery_order['id_user'] = $supplier['id_user'];
            $delivery_order['created_date'] = date('Y-m-d h:i:s');
            $delivery_order['order_conditions'] = $site_info['order_conditions'];
            $delivery_order['payment_terms'] = 45;
            $delivery_order['rand'] = $rand;
            $delivery_order['type'] = 2;
            $user = $this->ecommerce_model->getUserInfo($supplier['id_user']);
            $delivery_order['id_role'] = 1;
            $delivery_order['status'] = 5;
            $delivery_order['currency'] = $this->config->item('default_currency');
            $this->db->insert('delivery_order', $delivery_order);
            $id_delivery_order = $this->db->insert_id();
            ////////////////////INSERT LPO LINE ITEMS INVOICE///////
            $j = 0;
            $total_cost = 0;
            $total_price = 0;
            foreach ($line_items as $line_item) {
                $j++;

                // Miles
                $id_products = $line_item['id_products'];
                $qty = $line_item['stock_out_quantity'];
                $id_brands = $line_item['id_brands'];
                $price = $line_item['price'];
                $cost = $line_item['price'];
                $combination = $line_item['options_en'];
                $brand_title = $this->fct->getonecell('brands', 'title', array('id_brands' => $id_brands));
                if (!empty($line_item['options_en'])) {
                    $type = 'option';
                } else {
                    $type = "product";
                }

                if ($type == "option") {
                    $stock = $this->fct->getonerecord('products_stock', array('id_products' => $id_products, 'combination' => $line_item['options_en']));
                    $combination = $stock['combination'];
                    $id_products = $stock['id_products_stock'];
                    $line_items3 = $this->fct->getAll_cond_1('delivery_order_line_items', 'id_delivery_order_line_items', array('type' => 'option', 'id_products' => $stock['id_products_stock']), 'desc');
                    if (!empty($line_items3)) {
                        $cost = $line_items3[0]['cost'];
                    }

                } else {
                    $product = $this->fct->getonerecord('products', array('id_products' => $id_products));

                    $line_items3 = $this->fct->getAll_cond_1('delivery_order_line_items', 'id_delivery_order_line_items', array('type' => 'product', 'id_products' => $id_products), 'desc');
                    if (!empty($line_items3)) {
                        $cost = $line_items3[0]['cost'];
                    }
                }

                $total_cost = $total_cost + ($qty * $cost);
                $total_price = $total_price + ($qty * $price);
                $delivery_order_line_item['created_date'] = date('Y-m-d h:i:s');
                $delivery_order_line_item['id_products'] = $id_products;
                $delivery_order_line_item['quantity'] = $qty;
                $delivery_order_line_item['price'] = $price;
                $delivery_order_line_item['cost'] = $cost;
                $delivery_order_line_item['id_orders'] = $id_delivery_order;
                $delivery_order_line_item['options'] = $combination;
                $delivery_order_line_item['brands_title'] = $brand_title;
                $delivery_order_line_item['id_brands'] = $id_brands;
                $delivery_order_line_item['type'] = $type;

                // options
                $this->db->insert('delivery_order_line_items', $delivery_order_line_item);
            }

            $cond['id_delivery_order'] = $id_delivery_order;
            $update_order['total_cost'] = $total_cost;
            $update_order['total_price'] = $total_price;
            $this->db->where($cond);
            $this->db->update('delivery_order', $update_order);

            $orderData = $this->admin_fct->getDeliveryOrder($id_delivery_order, array('rand' => $rand));


            $type = 2;
            $export = 0;
            $section = 'lpo';
            $page_title = 'LPO';
            $order_name = 'D-P';
            $form = 'export';
            $section_name = 'Purchase Order';

            $orderData['section'] = $section;
            $orderData['form'] = $form;
            $orderData['type'] = $type;
            $orderData['export'] = $export;
            $orderData['page_title'] = $page_title;
            $orderData['order_name'] = $order_name;
            $orderData['section_name'] = $section_name;

            $this->load->model('send_emails');
            //$this->send_emails->inform_supplier_deliveryOrder($orderData);

            //I think we can disable that 18.01.2017
            //$this->send_emails->inform_admin_deliveryOrder($orderData);

        }
    }

    public function updateMilesRedeem($rowid = "", $checked = "", $new_qty = 0, $new_redeem_miles = 0, $newRow = false)
    {
        $return = array();
        $user = $this->ecommerce_model->getUserInfo();
        $user_miles = $user['miles'];

        if ($rowid == "" && !$newRow) {
            $rowid = $this->input->post('rowid');
            $checked = $this->input->post('checked');
        }
        $shippingCharge = $this->input->post('shippingCharge');
        $dataTmp = $this->cart->contents();
        $product_quantities = $this->session->userdata('product_quantities');
        $selected_redeem = $this->session->userdata('selected_redeem');
        if (empty($selected_redeem)) {
            $selected_redeem = array();
        }
        if (empty($product_quantities)) {
            $product_quantities = array();
        }

        $status = 0;
        if ($newRow) {
            //$total_redeem_miles=$new_redeem_miles;
            $total_redeem_miles = 0;
        } else {
            $total_redeem_miles = 0;
        }

        if (in_array($rowid, $selected_redeem) && $checked == 0 && !$newRow) {
            $status = 3;
            $return['result'] = 3;
            $return['message'] = "Removed";
            $selected_redeem = $this->removeRedeemMileRowId($selected_redeem, $rowid);
            $this->session->set_userdata('selected_redeem', $selected_redeem);
        } else {

            foreach ($dataTmp as $item) {
                $qty = $item['qty'];
                if ((in_array($item['rowid'], $selected_redeem) || $item['rowid'] == $rowid) && !$newRow) {
                    $qty = 0;

                }
                $redeem_miles = $item['options']['redeem_miles'] * $qty;
                $total_redeem_miles = $total_redeem_miles + $redeem_miles;
            }
            $total_redeem_miles = $total_redeem_miles + ($new_qty * $new_redeem_miles);

            /*foreach ($dataTmp as $item){
        if($item['rowid']==$rowid){

			$qty=$item['qty'];

		  $redeem_miles=$item['options']['redeem_miles']*$qty;
		   $old_redeem_miles=$item['options']['redeem_miles']*$old_qty;
	      $total_redeem_miles=$total_redeem_miles+$redeem_miles-$old_redeem_miles;
 }}	*/


            if ($total_redeem_miles >= $user_miles) {

                $return['result'] = 0;
                $return['message'] = "Sorry! You have exceeded your available miles";

            } else {
                if (!$newRow) {
                    $product_quantities[$rowid] = $qty;
                    $this->session->set_userdata('product_quantities', $product_quantities);
                    array_push($selected_redeem, $rowid);
                    $this->session->set_userdata('selected_redeem', $selected_redeem);
                    $return['result'] = 1;
                    $return['message'] = "Added";
                } else {
                    $return['result'] = 1;
                }
            }
        }
        if (empty($rowid) && !$newRow) {
            $cart_items = $this->cart->contents();
            if (empty($cart_items))
                $cart_items = $this->ecommerce_model->loadUserCart();
            $data['products'] = $this->ecommerce_model->getCartProducts($cart_items);
            $data['section'] = "checkout";
            $data['shippingCharge'] = $shippingCharge;
            $return['cart'] = $this->load->view('cart/cart', $data, true);
            die(json_encode($return));
        } else {
            return $return;
        }

    }

///////////////////////////////////////////////////////////////////////////////////////
    public function multi_add()
    {
        $checklist = $this->input->post('checklist');
        $product_id = $this->input->post('product_id');
        $options = $this->input->post('options');
        $qty = $this->input->post('qty');
        $text_result = "";
        if (!empty($checklist)) {
            foreach ($checklist as $key => $val) {
                $post['product_id'] = $product_id[$key];
                if (isset($options[$key])) {
                    $post['options'] = $options[$key];
                }

                if (isset($qty[$key])) {
                    $post['qty'] = $qty[$key];
                }
                $post['ajax'] = 1;
                $result = $this->ecommerce_model->add_to_cart($post);
                $text_result .= "<br>" . $result;
            }
            return $text_result;
        } else {
            return false;
        }
    }

///////////////////////////////////////////////////////////////////////////////////////
    public function createChecklistCategories($id_user)
    {
        $checklist_categories = $this->fct->getAll('created_checklist_categories', 'sort_order asc');

        foreach ($checklist_categories as $val) {
            $check = $this->fct->getonerecord('checklist_categories', array('id_user' => $id_user, 'id_created_categories' => $val['id_created_checklist_categories']));
            if (empty($check))
                $data['id_user'] = $id_user;
            $data['title'] = $val['title'];
            $data['id_created_categories'] = $val['id_created_checklist_categories'];
            $this->db->insert('checklist_categories', $data);
        }
        return true;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getProductsAddToCart($cond, $limit = "4", $sort_order = "products.sort_order")
    {

        $data = array();
        $q = " SELECT products.title, products.id_products, line_items.created_date, count(line_items.id_products) as  cc FROM products ";
        $q .= " join line_items on line_items.id_products=products.id_products ";
        if (isset($cond['id_user'])) {
            $q .= " join brands on brands.id_brands=products.id_brands ";
        }
        $q .= " WHERE products.status = 0 AND products.deleted = 0 ";
        if (isset($cond['id_user']) && !empty($cond['id_user'])) {
            $q .= "  AND brands.id_user = " . $cond['id_user'];
        }

        $q .= " GROUP BY products.id_products ORDER BY " . $sort_order;

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset 0 ";
        }
        $query = $this->db->query($q);
        $results = $query->result_array();

        return $results;
    }

    public function getProductStock($cond, $limit = 0)
    {
        $a1 = array();
        $a2 = array();
        $products_attributes = $this->fct->getAll_cond('product_attributes', 'id_product_attributes desc', array('id_products' => $cond['id_products']));

        foreach ($products_attributes as $val) {
            array_push($a1, $val['id_attributes']);
        }

        $sqla = "SELECT products_stock.* FROM products_stock ";
        $sqla .= " join product_attributes on  product_attributes.id_products=products_stock.id_products ";
        if (isset($cond['id_brands'])) {
            $sqla .= " join products on  products.id_products=products_stock.id_products ";
        }
        $sqla .= " where products_stock.deleted=0 and  products_stock.status=1 ";

        if (isset($cond['id_products'])) {
            $sqla .= "  and product_attributes.id_products=" . $cond['id_products'];;
        }


        if (isset($cond['available_in_stock'])) {
            $sqla .= " and products_stock.quantity>0 ";
        }

        if (isset($cond['id_brands'])) {
            $sqla .= " and products.id_brands=" . $cond['id_brands'];
        }

        if (isset($cond['threshold'])) {
            $sqla .= " and products_stock.threshold=" . $cond['threshold'];
        }

        $sqla .= " group by products_stock.id_products_stock ";
        $query = $this->db->query($sqla);
        $products_stock = $query->result_array();

        $i = 0;
        $stock = array();
        foreach ($products_stock as $s) {
            $options = unSerializeStock($s['combination']);
            $a2 = array();
            $bool = true;
            foreach ($options as $key => $val) {
                $id_attribute = $this->fct->getonecell('product_options', 'id_attributes', array('id_attribute_options' => $key));
                array_push($a2, $id_attribute);
            }


            $diff1 = array_diff($a2, $a1);
            $diff2 = array_diff($a1, $a2);
            if (!empty($diff1) || !empty($diff2)) {
            } else {
                if ($limit != 1) {
                    $stock[$i] = $s;
                    $i++;
                }

                if ($limit == 1 && empty($stock)) {
                    $stock = $s;
                }
            }
        }

        return $stock;

    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getTotalOrdersAmount($cond = array())
    {

        /*	echo "<pre>";
        print_r($cond);exit;*/
        $this->db->select('orders.*');
        $this->db->from('orders');
        $this->db->join('user', 'orders.id_user = user.id_user');

        /*if(isset($cond['user.name'])) {
        $this->db->like('UPPER(name)',strtoupper($cond['user.name']));
        unset($cond['user.name']);
        }*/


        if (!empty($cond)) {
            $this->db->where($cond);
        }
        $this->db->group_by('orders.id_orders');
        $this->db->order_by('id_orders DESC');

        $query = $this->db->get();

        $result = $query->result_array();
        $amount = 0;
        foreach ($result as $res) {
            $amount = $amount + $res['amount'];
        }


        return $amount;
    }


    ///////////////////////////////////////////////////////////////////////////////////////

    public function getCheckoutAttr($supplier = array(), $delivery_admin = 0)
    {
        $product_quantities = $this->session->userdata('product_quantities');
        $selected_redeem = $this->session->userdata('selected_redeem');
        if (empty($selected_redeem)) {
            $selected_redeem = array();
        }
        $cart_items = $this->cart->contents();
        if (empty($cart_items))
            $cart_items = $this->ecommerce_model->loadUserCart();

        if (!empty($supplier)) {
            $ids_brands = array();
            $brands = array();
            if (!empty($supplier['id_user'])) {
                $brands = $this->fct->getAll_cond('brands', 'sort_order asc', array('id_user' => $supplier['id_user']));
                $ids_brands = $this->ecommerce_model->getIds('id_brands', $brands);
            } else {
            }
            $cond['ids_brands'] = $ids_brands;
            $products = $this->ecommerce_model->getCartProducts($cart_items, "", $cond);
        } else {
            $products = $this->ecommerce_model->getCartProducts($cart_items);
        }

        $net_price = 0;
        $net_discount_price = 0;
        $weight = 0;
        $number_of_pieces = 0;
        $i = 0;
        $miles = 0;
        $total_redeem_miles = 0;
        $total_miles = 0;
        $net_redeem_miles_price = 0;
        $net_discount_redeem_miles_price = 0;
        foreach ($products as $product) {
                $i++;
            /////////////////MILES///////////
            $number_of_pieces = $number_of_pieces + $product['qty'];
            if ($product['product_info']['exclude_from_shipping_calculation'] != 1) {
                $weight = $weight + $product['product_info']['weight'];
            }

            $selected_miles_redeem = $this->input->post('miles_redeem');
            $qty = $product['qty'];
            $miles = $product['options']['miles'] * $qty;
            $redeem_miles = $product['options']['redeem_miles'] * $qty;
            if ($redeem_miles > 0) {
                $miles = 0;
            }
            $total_miles = $total_miles + $miles;
            $selected_miles_redeem = $this->input->post('miles_redeem');
            if (isset($selected_miles_redeem[$product['rowid']])) {
                $total_redeem_miles = $total_redeem_miles + $redeem_miles;
            }
        
            $name = $product['product_info']['title'];
            $q = "SELECT DISTINCT u.delivery_shipping_from AS shipping FROM user AS u INNER JOIN brands AS b ON b.id_user = u.id_user INNER JOIN products AS p ON p.id_brands = b.id_brands WHERE p.title LIKE '" . $name . "'";
            $query = $this->db->query($q);
    
            foreach ($query->result() as $row){
                $shippingAddress = $row->shipping;
            }
        
            $discountamount = 0;
            $discountBrand = "";
            
            $promocodeQuery = "SELECT pc.amount, sc.id_brands  FROM `shopping_cart` AS sc INNER JOIN `promo_code` AS pc ON pc.code = sc.promo_code Where sc.`id_brands` = " . $product['product_info']['id_brands'];
            $promocodeQuery = $this->db->query($promocodeQuery);
            
            foreach ($promocodeQuery->result() as $row){
                $discountamount = $row->amount;
                $discountBrand =  $row->id_brands;
            }
        
            $list_price = $product['product_info']['list_price'];
            $total_price = $list_price * $product['qty'];

            if($shippingAddress == 'United Arab Emirates'){
                $total_discount_price = ($product['price'] * $product['qty']) + ($product['price'] * $product['qty'] * 0.05);
                $total_tax_price = $product['price'] * $product['qty'] * 0.05 + $total_tax_price;
            } else {
                $total_discount_price = $product['price'] * $product['qty'];
                $total_tax_price = 0;
            }


            if ($redeem_miles > 0) {
                $total_redeem_miles = $total_redeem_miles + $redeem_miles;
                /*	$net_redeem_miles_price=$net_redeem_miles_price + $total_price;$net_discount_redeem_miles_price=$net_discount_redeem_miles_price + $total_discount_price;*/
            } else {
                $net_price = $net_price + $total_price;
                $net_discount_price = $net_discount_price + $total_discount_price;
            }
            
            $productBrand = $product['product_info']['id_brands'];
            
            if($productBrand = $discountBrand){
                $promocode_discount_price = $promocode_discount_price +  ($total_price * $discountamount)/100;
            } else {
                $promocode_discount_price = $promocode_discount_price + 0;
            }

        }

        $payment_method_discount = 0;

        if ($this->session->userdata('payment_method') == "credits" || $this->session->userdata('payment_method') == "credit_cards" || $this->input->post('payment_method') == "credits" || $this->input->post('payment_method') == "credit_cards") {

        //$payment_method_discount=(2.5*$net_discount_price)/100;

            $this->session->unset_userdata('payment_method');
        }

        $net_discount_price = $net_discount_price - $payment_method_discount;

        $discount = ($net_price + $net_redeem_miles_price) - ($net_discount_price + $net_discount_redeem_miles_price);
        if ($this->session->userdata('discount_groups') && $this->session->userdata('discount_groups') != "") {
            $discount_groups = $this->session->userdata('discount_groups');
            $new_discount = getDiscount($net_price, $discount_groups['discount']);
            if ($new_discount > $discount) {
                $discount = $new_discount;
                $net_discount_price = $net_price - $new_discount;
            }
        }

        $return['weight'] = $weight / 1000;
        $return['net_price'] = $net_price;
        $return['miles'] = $miles;
        $return['number_of_pieces'] = $number_of_pieces;
        $return['discount'] = $discount;
        $return['net_discount_price'] = $net_discount_price;
        //$return['net_promocode_discount'] = $promocode_discount_price;

        $return['discount'] = $discount;
        $return['net_discount_price'] = $net_discount_price - $promocode_discount_price;
        $return['net_redeem_miles_price'] = $net_redeem_miles_price;
        $return['net_discount_redeem_miles_price'] = $net_discount_redeem_miles_price;
        $return['total_miles'] = $total_miles;
        $return['total_redeem_miles'] = $total_redeem_miles;
        $return['sub_total'] = changeCurrency($net_price + $net_redeem_miles_price);
        $return['total_tax_price'] = $total_tax_price;

        $q = " select id_orders from  orders where 1 ";
        $query = $this->db->query($q);
        $count = $query->num_rows();
        $count = $count + 1;
        $return['id_orders'] = $count;
        return $return;
    }

    public function getClassificationGuide($classifications)
    {
        $id_classifcations = "";

        foreach ($classifications as $val) {
            if ($val['id_classifications'] == 2) {
                $id_classifcations = $val['id_classifications'];
            }
        }

        if (!empty($classifications) && !empty($id_classifcations)) {
            $id_classifcations = $classifications[0]['id_classifications'];
        }

        switch ($id_classifcations) {
            case '1':
                $page = 'woman';
                break;

            case '2':
                $page = 'unisex';
                break;

            case '3':
                $page = 'woman';
                break;

            case '4':
                $page = 'lifestyle';
                break;

            default:
                $page = "";
        }

        return $page;


    }


    public function getPaymentMethod($payment_method)
    {

        switch ($payment_method) {
            case 'cash':
                $payment = 'Cash on Delivery';
                break;

            case 'credits':
                $payment = 'Credits';
                break;

            case 'credit_cards':
                $payment = 'Credit Card';
                break;

            case 'checque':
                $payment = 'Cheque on Delivery';
                break;

            case 'paypal':
                $payment = 'Paypal';
                break;


            default:
                $payment = "";
        }

        return $payment;


    }

    public function getPaymentMethods()
    {
        $sqla = "SELECT  payment_method from orders ";
        $sqla .= " where orders.deleted=0  and  payment_method!='' ";
        $sqla .= " group by  orders.payment_method  ";
//echo $sqla;exit;
        $query = $this->db->query($sqla);
        $results = $query->result_array();
        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getSortOrder($sort_order)
    {

        switch ($sort_order) {

            case 'most_relevant':
                if (isset($_GET['dir'])) {
                    $order = 'products.visits ';
                } else {
                    $order = 'products.visits desc';
                }
                break;
            case 'newIn':
                $order = 'products.sort_order';
                break;
            case 'category':
                $order = 'products_categories.id_categories';
                break;
            case 'date':
                $order = 'products.created_date';
                break;
            case 'name':
                $order = 'products.title';
                break;
            case 'ascending':
                if (isset($_GET['dir'])) {
                    $order = 'products.price';
                } else {
                    $order = 'products.price desc';
                }
                break;
            case 'descending':
                if (isset($_GET['dir'])) {
                    $order = 'products.price';
                } else {
                    $order = 'products.price asc';
                }
                break;
            default:
                $order = 'products.sort_order';
        }
        return $order;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getUserAddresses($cond, $limit = '', $offset = '')
    {


        $q = "";
        $q .= " select  users_addresses.*, countries.title as countries_title  ";
        $q .= " from users_addresses ";
        $q .= " join user on users_addresses.id_user = user.id_user ";
        $q .= " join countries on users_addresses.id_countries = countries.id_countries ";
        if (isset($cond['branch']) && $cond['branch'] == 1) {
            $q .= " join checklist_branches on checklist_branches.id_users_addresses=users_addresses.id_users_addresses ";
        }
        $q .= 'where users_addresses.deleted=0';

        if (isset($cond['no_billing'])) {
            $q .= ' and users_addresses.id_users_addresses!=' . $cond['no_billing'];
        }

        if (isset($cond['no_shipping'])) {
            $q .= ' and users_addresses.id_users_addresses!=' . $cond['no_shipping'];
        }

        if (isset($cond['billing'])) {
            $q .= ' and users_addresses.billing=' . $cond['billing'];
        }

        if (isset($cond['shipping'])) {
            $q .= ' and users_addresses.shipping=' . $cond['shipping'];
        }

        if (isset($cond['branch']) && $cond['branch'] == 1) {
            $q .= ' and users_addresses.branch=' . $cond['branch'];
        }

        if (isset($cond['no_branch'])) {
            $q .= ' and users_addresses.branch!=' . $cond['no_branch'];
        }


        if (isset($cond['branch'])) {
            $q .= ' and users_addresses.branch=' . $cond['branch'];
        }

        if (isset($cond['keyword'])) {
            $q .= " and ( ";
            $q .= "  UPPER(countries.title) like '%" . strtoupper($cond['keyword']) . "%' ";
            $q .= "  OR UPPER(users_addresses.city) like '%" . strtoupper($cond['keyword']) . "%' ";
            $q .= "  OR UPPER(users_addresses.street_one) like '%" . strtoupper($cond['keyword']) . "%' ";
            $q .= "  OR UPPER(users_addresses.street_two) like '%" . strtoupper($cond['keyword']) . "%' ";
            $q .= ")";
        }

        if (isset($cond['id_user'])) {
            $q .= ' and users_addresses.id_user=' . $cond['id_user'];
        }

        if (isset($cond['id_users_addresses'])) {
            $q .= ' and users_addresses.id_users_addresses=' . $cond['id_users_addresses'];
        }

        if (isset($cond['pickup'])) {
            $q .= ' and users_addresses.pickup=' . $cond['pickup'];

        }

        $q .= "  group by users_addresses.id_users_addresses ";
        $q .= "  order by users_addresses.updated_date desc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
        $query = $this->db->query($q);

        if ($limit == 1) {
            $data = $query->row_array();
        } else {
            $data = $query->result_array();
        }
        if ($limit == "num") {
            $data = $query->num_rows();
        }

        return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getProducts($cond, $limit = '', $offset = '', $order = 'products.sku', $desc = "asc")
    {
        $sales = false;
        if (isset($cond['keywords'])) {
            $left = "";
        } else {
            $left = "";
        }

        $data = array();
        $q = "";

        /* $q .= "select  products.title, products.set_as_clearance, products.length, products.height,  products.retail_price,  products.created_date,  products.deleted_supplier, products.redeem_miles, products.id_miles, products.brief, products.quantity, products.id_brands,  products.id_products ,  products.id_designers,  products.list_price,  products.hide_price,  products.price,  products.published,  products.discount,  products.discount_expiration,  products.availability,  products.title_url,  products.sku,  products.display_in_home,  products.set_as_new,  products.status ";*/
        $q .= "select products.id_products, products.title, products.title_url ";

        if (isset($cond['category']) || (isset($cond['id_categories']))) {
            $q .= ", products_categories.id_categories as id_categories ";
        }

        if (isset($cond['set_as_training'])) {
            $q .= ", products.datasheet as datasheet, brand_certified_training.duration as duration, brand_certified_training.title as training_title, brand_certified_training.description as training_description, brand_certified_training.image as training_image, brands.logo as brands_image, brands.title as brands_title, brand_certified_training.id_brand_certified_training, brand_certified_training.set_as_training as set_as_training_n ";
        }

        if (isset($cond['product_options.id_attribute_options'])) {
            $q .= " ,product_options.id_attribute_options ";
        }

        $q .= " from products ";
        //print_r($product_options);exit;

        if (isset($cond['category']) || isset($cond['categories']) || (isset($cond['id_categories']))) {
            $q .= " " . $left . " join products_categories on products_categories.id_products = products.id_products ";
        } else {
            if ($this->input->get('sort') == 'category' || (isset($cond['id_parent']) && !empty($cond['id_parent']))) {
                $q .= "  join products_categories on products_categories.id_products = products.id_products ";
            }
        }

        if (
            isset($cond['specials'])
            && (
                $cond['specials'] == 'has_bulk'
                || $cond['specials'] == 'all'
            )
        ) {
            $q .= " left join product_price_segments on product_price_segments.id_products = products.id_products ";
        }

        if (isset($cond['brand']) || (isset($brands_ids) && !empty($brands_ids))) {
            $q .= " " . $left . " join brands on brands.id_brands = products.id_brands ";
        }

        if (isset($cond['set_as_training'])) {
            /*$q .= " ".$left." join products_training on products_training.id_products = products.id_products ";*/
            $q .= " " . $left . " join brand_certified_training on brand_certified_training.id_products = products.id_products ";
            $q .= " " . $left . " join brands on brands.id_brands = products.id_brands ";
        }

        if (isset($cond['classifications'])) {
            $q .= " " . $left . " join products_classifications on products_classifications.id_products = products.id_products ";
        }

        if (isset($cond['options'])) {
            $q .= " " . $left . " join product_options on products.id_products = product_options.id_products ";
        }

        $q .= " where products.deleted=0 ";
        if (!isset($cond['admin'])) {
            $q .= " and products.status=0 and products.published=0";
        }


        if (isset($cond['title'])) {
            /*$this->db->like('UPPER(products.title'.getFieldLanguage().')',strtoupper($cond['products.title'.getFieldLanguage()]));*/
            $q .= " and UPPER(products.title) like '%" . strtoupper($cond['title']) . "%' ";
            unset($cond['title']);
        }

        if (isset($cond['sku'])) {
            $q .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['id_brands'])) {

            $q .= " and products.id_brands=" . $cond['id_brands'];
        }
        
        if (isset($cond['set_as_oed'])) {
            $q .= " and products.set_as_oed= 1 and DATE_FORMAT(products.discount_expiration, '%Y-%m-%d') >= CURDATE() ";
        }

        if (isset($brands_ids) && !empty($brands_ids)) {
            $q .= " and products.id_brands  IN (" . implode(',', $brands_ids) . ")";
        }
        //and products.discount >= '50'
        if (isset($cond['set_as_oed'])) {
            $q .= " and products.set_as_oed= 1 and DATE_FORMAT(products.discount_expiration, '%Y-%m-%d') >= CURDATE() ";
        }

        if (isset($cond['brands_ids']) && !empty($cond['brands_ids'])) {
            if (isset($cond['admin'])) {
                $q .= " and (products.id_brands  IN (" . implode(',', $cond['brands_ids']) . ") || products.id_user=" . $cond['id_admin'] . ")";
            } else {
                $q .= " and products.id_brands  IN (" . implode(',', $cond['brands_ids']) . ")";
            }
        }

        if (isset($cond['brands_arr']) && !empty($cond['brands_arr'])) {
            $q .= " and products.id_brands  NOT IN (" . $cond['brands_arr'] . ")";
        }

        if (isset($cond['options'])) {
            $q .= " and product_options.deleted=0  and product_options.status=1";
        }

        if (isset($cond['id_categories']) && !empty($cond['id_categories'])) {
            $q .= " and (products_categories.id_categories=" . $cond['id_categories'] . " || FIND_IN_SET(" . $cond['id_categories'] . ",products_categories.ids_parent)>0) ";
        }

        if (isset($cond['id_products'])) {
            $q .= " and products.id_products=" . $cond['id_products'];
        }

        if (isset($cond['group_products']) && !empty($cond['group_products'])) {
            if ($cond['group_products'] == 99) {
                $cond['group_products'] = 0;
            }
            $q .= " and products.group_products=" . $cond['group_products'];
        }

        if (isset($cond['set_as_news'])) {

            $q .= "  AND  products.set_as_new= " . $cond['set_as_news'];
        }

        if (isset($cond['set_as_new'])) {
            $q .= "  AND  products.set_as_new= " . $cond['set_as_new'];
        }
        if (isset($cond['set_as_special'])) {
            $q .= "  AND  products.set_as_special= " . $cond['set_as_special'];
        }
        if (isset($cond['set_as_clearance'])) {
            $q .= "  AND  products.set_as_clearance= " . $cond['set_as_clearance'];
        }
        if (isset($cond['set_as_soon'])) {
            $q .= "  AND  products.set_as_soon= " . $cond['set_as_soon'];
        }
        if (isset($cond['set_as_pro'])) {
            $q .= "  AND  products.set_as_pro= " . $cond['set_as_pro'];
        }
        if (isset($cond['set_as_non_exportable'])) {
            $q .= "  AND  products.set_as_non_exportable= " . $cond['set_as_non_exportable'];
        }

        if (isset($cond['promote_to_front'])) {
            $q .= "  AND  products.promote_to_front= " . $cond['promote_to_front'];
        }

        if (isset($cond['specials']) && $cond['specials'] == 'has_active_discount') {
            $q .= "  AND  products.discount_active = 1 ";
        }

        if (isset($cond['specials']) && $cond['specials'] == 'has_discount') {
            $q .= "  AND  products.discount > 0 ";
        }

        if (isset($cond['specials']) && $cond['specials'] == 'has_bulk') {
            $q .= "  AND  product_price_segments.min_qty > 0 ";
        }

        if (isset($cond['specials']) && $cond['specials'] == 'all') {
            $q .= " AND (
                        products.discount > 0
                        or product_price_segments.min_qty > 0
                        or products.set_as_new = 1
                        or products.set_as_special = 1
                        or products.set_as_clearance = 1
                        or products.set_as_soon = 1
                    )
            ";
        }

        if (isset($cond['set_as_trends_updates'])) {
            $q .= "  AND  products.set_as_trends_updates= " . $cond['set_as_trends_updates'];
        }
        if (isset($cond['status'])) {
            if (is_array($cond['status'])) {
                $q .= "  AND  products.status in (" . implode(',', $cond['status']).") ";
            } else {
                $q .= "  AND  products.status=" . $cond['status'];
            }


        }

        if (isset($cond['display_in_home'])) {
            $q .= "  AND  products.display_in_home=1 ";
        }

        if (isset($cond['set_as_training'])) {
            /*$q .= "  AND  brand_certified_training.set_as_training=1 ";*/
        }

        if (isset($cond['clearance'])) {
            $q .= "  AND  products.set_as_clearance=1 ";
        }

        if (isset($cond['id_user'])) {
            $q .= " and products.id_user=" . $cond['id_user'];
        }

        if (isset($cond['published'])) {
            if ($cond['published'] != 2) {
                $q .= "  AND ( products.published=" . $cond['published'];
                $q .= "  or  products.deleted_supplier=1)";
            }
        }

        if (isset($cond['miles'])) {
            $q .= "  AND  products.set_as_redeemed_by_miles=1 ";
        }
        if (isset($cond['offers_month'])) {
            $time = strtotime(date("Y-m-d h:i:s"));
            $date_before_month = date("Y-m-d h:i:s", strtotime("-1 month", $time));
            $q .= "  AND  (products.created_date>='" . $date_before_month . "' OR products.updated_date>='" . $date_before_month . "')";
            $q .= ' AND products.discount_status=1 ';
        }

        if (isset($cond['availability'])) {
            $q .= " and products.availability=" . $cond['availability'];
        }


        if (isset($cond['categories'])) {
            $q .= " and products_categories.id_categories in (" . $cond['categories'] . ") ";
        }

        if (isset($cond['id_parent']) && !empty($cond['id_parent'])) {
            $q .= " and (products_categories.id_categories=" . $cond['id_parent'] . " or products_categories.id_categories in ( select id_categories from categories where id_parent=" . $cond['id_parent'] . ")) ";
        }


        if (isset($cond['category'])) {
            $q .= " and products_categories.id_categories=" . $cond['category'];
        }

        if (isset($cond['training'])) {
            $q .= " and brand_certified_training.id_brand_certified_training IN (select id_brand_certified_training from products_training where id_training_categories=" . $cond['training'] . ")";
        }


        if (isset($cond['categories']) && $this->session->userdata('group_category') != "") {

            $q .= " and (products.id_group_categories=" . $this->session->userdata('group_category') . " or products.id_group_categories='')";
        }


        if (isset($cond['set_as_training'])) {
            $q .= " and brand_certified_training.deleted=0 ";
        }
        if (isset($cond['brand']) && !empty($cond['brand'])) {
            $q .= " and products.id_brands=" . $cond['brand'] . " and products.set_as_oed = 0";
        }

        if (isset($cond['group_category']) && !empty($cond['group_category'])) {
            $q .= " and products.id_group_categories=" . $cond['group_category'];
        }

        if (isset($cond['classifications'])) {
            $q .= " and products_classifications.id_classifications=" . $cond['classifications'];
        }

        if (isset($cond['barcode']) && !empty($cond['barcode'])) {
            $q .= " and products.barcode='" . $cond['barcode'] . "'";
        }

        if (isset($cond['min_price']) && isset($cond['max_price'])) {
            $q .= " and products.price >=" . $cond['min_price'] . " and products.price <=" . $cond['max_price'];
        }

        if (isset($cond['sales']) && $cond['sales'] == 1) {
            $q .= ' AND products.discount_status=1 ';
        }
        if (isset($cond['sales']) && $cond['sales'] == 0) {

            $q .= ' AND products.discount_status=0 ';
        }

        /*        if (isset($cond['All'])) {
        } else {
            if ($sales) {
                $q .= ' AND products.discount_status=1 ';

            } else {
				$q .= ' AND products.discount_status=0 ';}
        }*/

        //$cond['products.lang'] = $this->lang->lang();
        //print_r($cond);exit;
        /*$this->db->where($cond);*/

        if (isset($cond['options'])) {
            /*$this->db->where_in('product_options.id_attribute_options',$cond_arr);*/
            $q .= " and product_options.id_attribute_options IN (" . $cond['options'] . ")";
        }

        if (isset($cond['keywords'])) {

            $q .= " and (UPPER(products.title) like '%" . strtoupper($cond['keywords']) . "%' ";
            $q .= " or UPPER(products.sku) like '%" . strtoupper($cond['keywords']) . "%' ";
            $q .= " or UPPER(products.description) like '%" . strtoupper($cond['keywords']) . "%' ";
            $q .= " or UPPER(products.barcode) like '%" . strtoupper($cond['keywords']) . "%' ";
            $q .= " or UPPER(products.title_url) like '%" . strtoupper($cond['keywords']) . "%' ";
            $q .= " ) ";

        }

        if (isset($cond['set_as_training'])) {
            $q .= "  group by brand_certified_training.id_brand_certified_training ";
        } else {
            $q .= "  group by products.id_products ";
        }
        $q .= "  order by " . $order . ' ' . $desc;


        /* echo $q;exit;*/
        if ($limit != '') {

            if ($this->input->get('sort') != 'category') {
                $q .= "  limit " . $limit . " offset " . $offset;

            }
        }

        /*echo $q;exit;*/
        /*echo $q;exit;*/

        $query = $this->db->query($q);

        if ($limit == '') {

            $data = $query->num_rows();
        } else {

            $results = $query->result_array();
            if (!isset($cond['autocomplete'])) {

                $j = 0;
                $i = 0;
                foreach ($results as $res) {
                    /*$data[$i]=$this->getOneProduct($res['id_products'],array(),$cond);*/
                    $data[$i] = $this->getOneProduct($res['id_products'], array(), $cond);
                    $i++;
                }
            } else {
                $data = $results;
            }

        }
        return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getProductsGroupByCategories($cond, $limit = '', $offset = '', $order = 'products.sort_order', $desc = "asc")
    {
        $sales = false;
        if (isset($cond['keywords'])) {
            $left = "";
        } else {
            $left = "";
        }


        $data = array();
        $q = "";

        /* $q .= "select  products.title, products.set_as_clearance, products.length, products.height,  products.retail_price,  products.created_date,  products.deleted_supplier, products.redeem_miles, products.id_miles, products.brief, products.quantity, products.id_brands,  products.id_products ,  products.id_designers,  products.list_price,  products.hide_price,  products.price,  products.published,  products.discount,  products.discount_expiration,  products.availability,  products.title_url,  products.sku,  products.display_in_home,  products.set_as_new,  products.status ";*/
        $q .= "select categories.* ";
        $q .= ", products_categories.id_products ";
        $q .= " from products_categories ";
        //print_r($product_options);exit;
        $q .= " join products on products_categories.id_products = products.id_products ";
        $q .= " join categories on categories.id_categories = products_categories.id_categories ";

        if (isset($cond['brand']) || (isset($brands_ids) && !empty($brands_ids))) {
            $q .= " " . $left . " join brands on brands.id_brands = products.id_brands ";
        }

        if (isset($cond['keywords'])) {
            $q .= " " . $left . " join designers on designers.id_designers = designers.id_designers ";
        }

        if (isset($cond['options'])) {
            $q .= " " . $left . " join product_options on products.id_products = product_options.id_products ";
        }

        $q .= " where products.deleted=0 and categories.deleted=0  ";
        if (!isset($cond['admin'])) {
            $q .= " and products.status=0 and products.published=0";
        }

        if (isset($cond['title'])) {
            /*$this->db->like('UPPER(products.title'.getFieldLanguage().')',strtoupper($cond['products.title'.getFieldLanguage()]));*/
            $q .= " and UPPER(products.title) like '%" . strtoupper($cond['title']) . "%' ";
            unset($cond['title']);
        }

        if (isset($cond['sku'])) {
            $q .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['id_brands'])) {

            $q .= " and products.id_brands=" . $cond['id_brands'];
        }

        if (isset($brands_ids) && !empty($brands_ids)) {
            $q .= " and products.id_brands  IN (" . implode(',', $brands_ids) . ")";
        }

        if (isset($cond['options'])) {
            $q .= " and product_options.deleted=0  and product_options.status=1";
        }

        if (isset($cond['id_products'])) {
            $q .= " and products.id_products=" . $cond['id_products'];
        }

        if (isset($cond['set_as_news'])) {

            $q .= "  AND  products.set_as_new= " . $cond['set_as_news'];
        }

        if (isset($cond['status'])) {
            $q .= "  AND  products.status=" . $cond['status'];
        }

        if (isset($cond['display_in_home'])) {
            $q .= "  AND  products.display_in_home=1 ";
        }

        if (isset($cond['set_as_training'])) {
            $q .= "  AND  brand_certified_training.set_as_training=1 ";
        }

        if (isset($cond['clearance'])) {
            $q .= "  AND  products.set_as_clearance=1 ";
        }

        if (isset($cond['id_user'])) {
            $q .= " and products.id_user=" . $cond['id_user'];
        }

        if (isset($cond['published'])) {
            if ($cond['published'] != 2) {
                $q .= "  AND ( products.published=" . $cond['published'];
                $q .= "  or  products.deleted_supplier=1)";
            }
        }

        if (isset($cond['miles'])) {
            $q .= "  AND  products.set_as_redeemed_by_miles=1 ";
        }
        if (isset($cond['offers_month'])) {
            $time = strtotime(date("Y-m-d h:i:s"));
            $date_before_month = date("Y-m-d h:i:s", strtotime("-1 month", $time));
            $q .= "  AND  (products.created_date>='" . $date_before_month . "' OR products.updated_date>='" . $date_before_month . "')";
            $q .= ' AND products.discount_status=1 ';
        }

        if (isset($cond['availability'])) {
            $q .= " and products.availability=" . $cond['availability'];
        }

        if (isset($cond['products.designer'])) {

            $q .= " and products.id_designers in (" . $cond['products.designer'] . ") ";
        }

        if (isset($cond['categories'])) {
            $q .= " and products_categories.id_categories in (" . $cond['categories'] . ") ";
        }

        if (isset($cond['category'])) {

            $q .= " and products_categories.id_categories=" . $cond['category'];
        }

        if (isset($cond['training'])) {
            $q .= " and products_training.id_training_categories=" . $cond['training'];
        }

        if (isset($cond['brand']) && !empty($cond['brand'])) {
            $q .= " and products.id_brands=" . $cond['brand'];
        }

        if (isset($cond['classifications'])) {
            $q .= " and products_classifications.id_classifications=" . $cond['classifications'];
        }

        if (isset($cond['min_price']) && isset($cond['max_price'])) {
            $q .= " and products.price >=" . $cond['min_price'] . " and products.price <=" . $cond['max_price'];
        }

        if (isset($cond['sales']) && $cond['sales'] == 1) {

            $q .= ' AND products.discount_status=1 ';
        }

        if (isset($cond['sales']) && $cond['sales'] == 0) {

            $q .= ' AND products.discount_status=0 ';
        }


        if (isset($cond['options'])) {
            /*$this->db->where_in('product_options.id_attribute_options',$cond_arr);*/
            $q .= " and product_options.id_attribute_options IN (" . $cond['options'] . ")";

        }


        $q .= "  group by products_categories.id_categories ";
        $q .= "  order by " . $order . ' ' . $desc;


        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;

        }

        /*echo $q;exit;*/
        /*echo $q;exit;*/
        $query = $this->db->query($q);


        if ($limit == '') {
            $data = $query->num_rows();
        } else {

            $data = $query->result_array();

            /*
                $j = 0;
                foreach ($results as $res) {
					$data[$j]=$res;
					$cond['category']=$res['id_categories'];
					$cc = $this->ecommerce_model->getProducts($cond);
					$data[$j]['products'] = $this->ecommerce_model->getProducts($cond,$cc,0,$order,$desc);

					$j++;
					}*/

        }
        return $data;
    }

    public function destroyItems($row_ids)
    {
        foreach ($row_ids as $prodrowid) {
            $data = array(
                'rowid' => $prodrowid,
                'qty' => 0
            );
            $this->cart->update($data);
            // delete from database: shopping cart
            $this->db->where('rowid', $prodrowid);
            $this->db->delete('shopping_cart');
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getBrandsByCategory($id_category)
    {

        $q = 'SELECT brands.* FROM products';
        $q .= " join brands on brands.id_brands = products.id_brands ";
        $q .= " join products_categories on products_categories.id_products = products.id_products ";
        $q .= 'WHERE brands.deleted = 0 and products.deleted=0 ';
        if (isset($id_category) && !empty($id_category)) {
            $q .= " and (products_categories.id_categories=" . $id_category . " || FIND_IN_SET(" . $id_category . ",products_categories.ids_parent)>0) ";
        }
        $q .= " group by brands.id_brands order by brands.title ";


        $query = $this->db->query($q);
        $brands = $query->result_array();


        return $brands;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getCategoriesByBrand($cond)
    {
        $parent_arr = array();
        $q = 'SELECT categories.id_categories FROM products_categories';
        $q .= " join categories on categories.id_categories = products_categories.id_categories ";
        $q .= " join products on products.id_products = products_categories.id_products ";
        $q .= " WHERE products.deleted=0 and products.id_brands=" . $cond['id_brands'];

        $q .= " group by products_categories.id_categories order by categories.title";

        $query = $this->db->query($q);
        $categories = $query->result_array();

        $data = array();
        $i = 0;
        foreach ($categories as $val) {

            $category = $this->getParentCategoryLevel($val['id_categories'], 0);
            if (!in_array($category['id_categories'], $parent_arr)) {
                array_push($parent_arr, $category['id_categories']);
                $data[$i] = $category;
                $i++;
            }

        }


        return $data;

    }


    public function getParentCategoryLevel($id_category = 0, $level = 2)
    {
        $parent = array();

        $parent_levels = $this->custom_fct->getCategoriesTreeParentLevels($id_category);
        $parent_levels = arrangeParentLevels($parent_levels);

        $i = 0;
        foreach ($parent_levels as $category) {
            if ($i == $level) {
                $parent = $category;
            }
            $i++;
        }

        return $parent;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getBrandsWithLetters()
    {

        $letters = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z";

        $data = array();
        $letterArray = explode(" ", $letters);

        $i = 0;
        foreach ($letterArray as $res) {
            $q = 'SELECT brands.* FROM brands';
            $q .= " join user on brands.id_brands = user.id_brands ";
            $q .= " join supplier_info on supplier_info.id_supplier_info = user.id_supplier_info ";
            $q .= 'WHERE brands.deleted = 0';
            $q .= " and UPPER(brands.title) like '" . $res . "%' ";
            $q .= " group by brands.id_brands ";


            $query = $this->db->query($q);
            $brands = $query->result_array();

            $result[$i]['letter'] = $res;
            $result[$i]['brands'] = $brands;

            $i++;
        }

        return $result;
    }

    public function getPrices($arr)
    {
        $o_price = "";
        $n_price = "";
        $id_product = $arr['id_products'];
        $list_price = $arr['list_price'];
        $price = $arr['price'];
        $qty = $arr['qty'];
        $discount_product = $arr['discount_product'];
        $discount_product_expiration = $arr['discount_product_expiration'];
        $withSegment = $arr['withSegment'];

        if (isset($withSegment) && !empty($withSegment)) {
            $segmentPrice = $this->ecommerce_model->checkPriceSegment($id_product, '', $qty);
        }
        $customerPrice = displayCustomerPrice($list_price, $discount_product, $price);
        $productDiscountPrice = displayProductDiscountPrice($discount_product, $discount_product_expiration, $list_price);
        if (!empty($withSegment)) {
            $new_price = min($productDiscountPrice, $customerPrice);
        } else {
            $new_price = min($productDiscountPrice, $customerPrice);
        }
        if ($new_price < $list_price) {
            $o_price = $list_price;
        }
        $n_price = $new_price;
        $result['o_price'] = $o_price;
        $result['n_price'] = $n_price;
        return $result;
    }
    ///////////////////////////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////////////////////
    public function getRelatedProducts($id_product, $limit = "4")
    {
        $data = array();

        $q = "SELECT products.* FROM products ";
        //$q .= " AND (";

        $q .= " WHERE products.status = 0 AND products.deleted = 0 and products.id_products!=" . $id_product;
        $q .= " and products.id_products IN (select id_related from related_products where id_products=" . $id_product . " ) ";

        //$q .= " )";
        $q .= " GROUP BY products.id_products ORDER BY products.sort_order";
        if (!empty($limit)) {
            $q .= " LIMIT " . $limit;
        }


        $query = $this->db->query($q);
        $results = $query->result_array();


        if (!empty($results)) {
            $j = 0;


            $results = $query->result_array();


            $j = 0;
            foreach ($results as $res) {

                // get gallery
                $this->db->select('*');
                $this->db->from('products_gallery');
                $cnd = array(
                    'id_products' => $res['id_products']
                );
                $this->db->where($cnd);
                $this->db->order_by('sort_order');
                $query = $this->db->get();
                $gallery = $query->result_array();

                $data[$j] = $res;
                /*if($res['brief']==0){
					$data[$j]['brief']="";	}*/
                $data[$j]['gallery'] = $gallery;

                if (!isset($cond['set_as_training'])) {
                    $attributes = $this->getProductAttributes($res['id_products']);
                    $data[$j]['attributes'] = $attributes;

                    $products_options = $this->getProductOptions($res['id_products']);
                    $data[$j]['product_options'] = $products_options;
                }

                $products_categories = $this->getProductCategories($res['id_products']);
                $data[$j]['products_categories'] = $products_categories;

                $miles = $this->fct->getonerecord('miles', array('id_miles' => $res['id_miles']));
                $data[$j]['miles'] = $miles;

                if (isset($cond['inventory']) && $cond['inventory'] == 1) {


                    $cond_inventory_2['id_products'] = $res['id_products'];
                    $purchases = $this->getPurchases($cond_inventory_2);


                    $data[$j]['qty_sold_by_admin'] = $purchases['qty_sold_by_admin'];
                    $data[$j]['qty_payable'] = $purchases['qty_payable'];
                }

                // get stock
                if (!empty($attributes)) {
                    $data[$j]['first_available_stock'] = $this->getProductStock(array('id_products' => $data[$j]['id_products'], 'available_in_stock' => 1), 1);
                    /*                       $data[$j]['first_available_stock'] = $this->fct->getonerow('products_stock', array(
                            'status' => 1,
                            'id_products' => $data[$j]['id_products'],
                            'quantity !=' => 0
                        ));*/
                    if (empty($data[$j]['first_available_stock']))
                        $data[$j]['first_available_stock'] = $this->getProductStock(array('id_products' => $data[$j]['id_products']), 1);
                    /*                            $data[$j]['first_available_stock'] = $this->fct->getonerow('products_stock', array(
                                'status' => 1,
                                'id_products' => $data[$j]['id_products']
                            ));*/
                }
                $j++;
            }


        }
        return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getDesignersByLetters()
    {

        $letters = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z";

        $data = array();
        $letterArray = explode(" ", $letters);


        $i = 0;
        foreach ($letterArray as $res) {
            $q = 'SELECT designers.* FROM designers  WHERE designers.deleted = 0';
            $q .= " and UPPER(designers.title) like '" . $res . "%' ";
            $q .= " group by designers.id_designers ";


            $query = $this->db->query($q);
            $designers = $query->result_array();

            $result[$i]['letter'] = $res;
            $result[$i]['designers'] = $designers;


            $i++;
        }

        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function RecentlyViewed($id = "", $limit = "", $admin = false)
    {
        $data = array();
        $id_user = 0;

        if ($this->session->userdata('login_id') != "") {
            $id_user = $this->session->userdata('login_id');
        }
        $session_id = $this->input->ip_address();


        $q = 'SELECT products.id_products FROM products JOIN visits ON visits.record = products.id_products WHERE products.deleted = 0';
        $q .= ' AND visits.section = "products"';
        if (!$admin) {
            if ($id != "")
                $q .= ' AND products.id_products != ' . $id;
            if ($id_user != 0) {
                $q .= ' AND (visits.session_id = "' . $session_id . '" OR visits.id_user = ' . $id_user . ')';
            } else {
                $q .= ' AND visits.session_id = "' . $session_id . '"';
            }
            if ($limit != "") {
                $q .= ' GROUP BY products.id_products ORDER BY visits.id_visits desc';
                $q .= ' limit ' . $limit . ' offset 0';
            } else {
                $q .= ' GROUP BY products.id_products ORDER BY visits.id_visits desc';
            }
        } else {
            if ($limit != "") {
                $q .= ' GROUP BY products.id_products ORDER BY visits.id_visits desc';
                $q .= ' limit ' . $limit . ' offset 0';
            } else {
                $q .= ' GROUP BY products.id_products ORDER BY visits.id_visits desc';
            }
        }


        //echo $q;exit;
        $query = $this->db->query($q);
        $results = $query->result_array();


        if (!empty($results)) {

            $j = 0;
            foreach ($results as $res) {
                $data[$j] = $this->getOneProduct($res['id_products']);
                $j++;
            }
        }
        return $data;
    }

    public function updateUserMilesLineItems($id_user = 0)
    {

        $q = " SELECT line_items.*, products.redeem_miles as products_redeem_miles, products.miles as products_miles from line_items ";
        $q .= " join products on line_items.id_products=products.id_products ";
        $q .= " join orders on orders.id_orders=line_items.id_orders ";
        $q .= " where ";

        $q .= "  orders.id_user=" . $id_user;


        $q .= " group by line_items.id_line_items";
        /*echo $q;exit;*/
        $query = $this->db->query($q);
        $results = $query->result_array();

        foreach ($results as $val) {
            $update = array();
            if ($val['products_redeem_miles'] < $val['redeem_miles'] && !empty($val['redeem_miles'])) {
                $update['redeem_miles'] = $val['products_redeem_miles'];
            }
            if ($val['products_miles'] < $val['miles'] && !empty($val['miles'])) {
                $update['miles'] = $val['products_miles'];

            }

            $cond = array('id_line_items' => $val['id_line_items']);
            if (!empty($update)) {
                /*echo 'user_miles='.$user_miles.'   id_user='.$val['id_user'];*/
                $this->db->where($cond);
                $this->db->update('line_items', $update);
            }
        }
    }

    public function updateAllUsersMiles($id_user = 0)
    {
        $q = " SELECT user.* from user";
        $q .= " where 1";
        if ($id_user != 0) {
            $q .= "  and user.id_user=" . $id_user;
        }

        $query = $this->db->query($q);
        $results = $query->result_array();

        foreach ($results as $val) {
            $user_miles = $this->calculateUserMiles($val['id_user']);

            $cond = array('id_user' => $val['id_user']);
            $update['miles'] = $user_miles;

            $this->db->where($cond);
            $this->db->update('user', $update);

            /*$this->updateUserMilesLineItems($val['id_user']);*/
        }


        return true;
    }

    public function calculateUserMiles($id_user = 643)
    {
        $cond = array('redeem' => 1, 'id_user' => $id_user);
        $redeem_miles = $this->checkUserMiles($cond);

        $cond = array('miles' => 1, 'id_user' => $id_user, 'paid' => 1);
        $miles = $this->checkUserMiles($cond);

        $remaining_miles = $miles - $redeem_miles;

        return $remaining_miles;
    }

    public function checkUserMiles($cond = array())
    {
        $q = " SELECT line_items.*, .products.redeem_miles as products_redeem_miles from line_items ";
        $q .= " join products on line_items.id_products=products.id_products ";
        $q .= " join orders on orders.id_orders=line_items.id_orders ";
        $q .= " where ";

        $q .= "  orders.id_user=" . $cond['id_user'];

        if (isset($cond['redeem'])) {
            $q .= " and line_items.redeem=1";
        }
        if (isset($cond['paid'])) {
            $q .= " and (orders.status='paid' || orders.status='completed')";
        }
        $q .= " group by line_items.id_line_items";

        $query = $this->db->query($q);
        $results = $query->result_array();


        $amount = 0;
        if (isset($cond['redeem'])) {
            foreach ($results as $val) {
                $amount = $amount + ($val['redeem_miles'] * $val['quantity']);
            }
        }

        if (isset($cond['miles'])) {
            foreach ($results as $val) {
                $amount = $amount + ($val['miles'] * $val['quantity']);
            }
        }

        return $amount;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getProductsCategories($category_url = '', $limit = '', $offset = '')
    {
        if ($category_url != "") {
            $res = $this->getSubCategories($category_url, $limit, $offset);
        } else {
            $res = $this->getMainCategories($limit, $offset);
        }
        return $res;
    }

    public function getMainCategories($limit = '', $offset = '')
    {
        $this->db->select('*');
        $this->db->from('categories');
        $cond = array(
            'deleted' => 0
        );
        $this->db->where($cond);
        $this->db->group_by("id_categories");
        $this->db->order_by('sort_order');
        if ($limit != "")
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        if ($limit == "") {
            $results = $query->num_rows();
        } else {
            $results = $query->result_array();
            if (!empty($results)) {
                foreach ($results as $key => $res) {
                    // get sub categories
                    $this->db->select('categories_sub.*');
                    $this->db->from('categories_sub');
                    $this->db->join('categories_sub_categories', 'categories_sub_categories.id_categories_sub = categories_sub.id_categories_sub');
                    $cond = array(
                        'categories_sub.deleted' => 0,
                        'categories_sub_categories.id_categories' => $res['id_categories']
                    );
                    $this->db->where($cond);
                    $this->db->order_by('categories_sub.sort_order');
                    $query = $this->db->get();
                    $subs = $query->result_array();
                    $results[$key]['categories_sub'] = $subs;
                    // get types
                }
            }
        }
        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getSubCategories($category_url, $limit = '', $offset = '')
    {
        $this->db->select('categories_sub.*');
        $this->db->from('categories_sub');

        if ($category_url != "") {
            $this->db->join("categories_sub_categories", "categories_sub_categories.id_categories_sub = categories_sub.id_categories_sub");
            $this->db->join("categories", "categories_sub_categories.id_categories= categories.id_categories");
            $cond['categories.title_url'] = $category_url;
        }
        $cond['categories_sub.deleted'] = 0;
        $this->db->where($cond);
        $this->db->group_by("id_categories_sub");
        $this->db->order_by('categories_sub.sort_order');
        if ($limit != "")
            $this->db->limit($limit, $offset);
        $query = $this->db->get();
        if ($limit == "") {
            $results = $query->num_rows();
        } else {
            $results = $query->result_array();
        }
        return $results;
    }


    public function getSubCategories2($cond, $limit = "", $offset = 0)
    {
        $data = array();
        $q = "";
        $q .= " select  categories.*";
        $q .= " from  categories ";
        $q .= " where categories.deleted=0 ";
        if (isset($cond["id_parent"])) {
            $q .= " and categories.id_parent=" . $cond['id_parent'];
        }
        $q .= "  group by categories.id_categories  order by categories.sort_order asc ";

        if ($limit != '') {
            $q .= "  limit " . $limit . " offset " . $offset;
        }
        $query = $this->db->query($q);
        if ($limit != "") {
            $results = $query->result_array();
            $i = 0;
            foreach ($results as $res) {
                //$cond2['category']=$res['id_categories'];

                $id_category = $res['id_categories'];
                $items = $this->custom_fct->getCategoriesPages($id_category, array(), array(), 10000, 0);
                $categories_ids = $this->custom_fct->getCategoriesIdsTreeParentLevels($items, $id_category);
                $cond2['categories'] = implode(",", $categories_ids);

                $cc = $this->getProducts($cond2);
                if ($cc != 0) {
                    $data[$i] = $res;
                    $data[$i]['cc_products'] = $cc;
                    $i++;
                }
            }
        } else {
            $data = $query->num_rows();
        }


        return $data;


    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getAttributeOptionLeftSide($cond)
    {


        $sales = false;

        if (isset($cond['sales'])) {

            unset($cond['sales']);
            $sales = true;
        }

        $data = array();
        $q = "";
        $q .= "select  products.id_products ";


        $q .= "  from products ";
        //print_r($product_options);exit;
        if (isset($cond['category'])) {
            $q .= " join products_categories on products_categories.id_products = products.id_products ";
        }

        $q .= " join products_gallery on products_gallery.id_products = products.id_products ";

        if (isset($cond['classifications'])) {
            $q .= " join products_classifications on products_classifications.id_products = products.id_products ";
        }

        if (isset($cond['options'])) {
            $q .= " join product_options on products.id_products = product_options.id_products ";
        }


        /*	if(isset($cond['brands.title_url'.getUrlFieldLanguage($this->lang->lang())])) {
        $this->db->join('brands','brands.id_brands = products.id_brands');
        }*/
        /*	if(isset($cond['categories_sub.title_url'.getUrlFieldLanguage($this->lang->lang())])) {
        $this->db->join('categories_sub','categories_sub.id_categories_sub = products.id_categories_sub');
        }*/
        $q .= " where products.deleted=0 and products.status=0 ";
        if (isset($cond['title'])) {
            /*$this->db->like('UPPER(products.title'.getFieldLanguage().')',strtoupper($cond['products.title'.getFieldLanguage()]));*/
            $q .= " and UPPER(products.title) like '%" . strtoupper($cond['title']) . "%' ";
            unset($cond['title']);
        }

        if (isset($cond['sku'])) {
            $q .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['options'])) {
            $q .= " and product_options.deleted=0  and product_options.status=1";
        }

        if (isset($cond['id_products'])) {
            $q .= " and products.id_products=" . $cond['id_products'];
        }

        if (isset($cond['set_as_news'])) {
            $q .= "  AND  products.set_as_new=1 ";

        }

        if (isset($cond['display_in_home'])) {
            $q .= "  AND  products.display_in_home=1 ";
        }
        if (isset($cond['products.designer'])) {
            $q .= " and products.id_designers in (" . $cond['products.designer'] . ") ";
        }

        if (isset($cond['availability'])) {
            $q .= " and products.availability=" . $cond['availability'];
        }


        if (isset($cond['category'])) {
            $q .= " and products_categories.id_categories=" . $cond['category'];
        }

        if (isset($cond['classifications'])) {

            $q .= " and products_classifications.id_classifications=" . $cond['classifications'];
        }

        if (isset($cond['min_price']) && isset($cond['max_price'])) {
            $q .= " and products.price >=" . $cond['min_price'] . " and products.price <=" . $cond['max_price'];
        }

        if (isset($cond['All'])) {
        } else {
            if ($sales) {
                $q .= ' AND products.id_products IN ( select (
CASE 
WHEN
 price < list_price THEN id_products  
ELSE  0 
END
) from products where deleted =0) ';

            } else {
                $q .= ' AND products.id_products IN ( select (
CASE 
WHEN
 list_price <= price THEN id_products  
ELSE  0 
END
) from products where deleted = 0) ';
            }
        }

        //$cond['products.lang'] = $this->lang->lang();
        //print_r($cond);exit;
        /*$this->db->where($cond);*/


        ///////////////////////////////////////SEARCH\\\\\\\\\\\\\\\\\\\\\\\\\\
        if (isset($cond['keywords'])) {
            $keywords = $cond['keywords'];
            $search1 = " select products_categories.id_products  from products_categories  join categories on products_categories.id_categories= products_categories.id_categories  where categories.deleted=0 ";
            $search1 .= "AND (UPPER(categories.title) like '%" . strtoupper($keywords) . "%')";

            $search2 = " select products_classifications.id_products  from products_classifications  join classifications on products_classifications.id_classifications= products_classifications.id_classifications  where classifications.deleted=0 ";
            $search2 .= "AND (UPPER(classifications.title) like '%" . strtoupper($keywords) . "%')";

            $search3 = " select products.id_products  from products  join designers on designers.id_designers= products.id_designers  where designers.deleted=0 ";
            $search3 .= "AND (UPPER(designers.title) like '%" . strtoupper($keywords) . "%')";

            $search4 = " select product_options.id_products  from product_options  join attribute_options on product_options.id_attribute_options= attribute_options.id_attribute_options  where attribute_options.deleted=0 ";
            $search4 .= "AND (UPPER(attribute_options.title) like '%" . strtoupper($keywords) . "%')";

            $q .= " and (products.id_products IN (" . $search1 . ") OR UPPER(products.title) like '%" . strtoupper($keywords) . "%'";
            $q .= " OR products.id_products IN (" . $search2 . ")";
            $q .= " OR products.id_products IN (" . $search3 . ")";
            $q .= " OR products.id_products IN (" . $search4 . ")";
            $q .= ")";
        }

        $q .= "  group by products.id_products ";
        $q .= "  order by products.id_products desc";
        $attributes = $this->fct->getAll('attributes', 'sort_order');
        $i = 0;
        foreach ($attributes as $attr) {
            $q1 = ' select attribute_options.*, product_options.id_products, product_options.id_attribute_options';
            $q1 .= ' from attribute_options';
            $q1 .= ' join attributes  on attribute_options.id_attributes= attributes.id_attributes ';
            $q1 .= ' join product_options  on product_options.id_attribute_options= attribute_options.id_attribute_options ';
            $q1 .= ' where attribute_options.deleted=0 and product_options.deleted=0  and product_options.status=1 and attributes.deleted=0 ';

            $q1 .= ' and attributes.id_attributes=' . $attr['id_attributes'];

            $q1 .= ' AND product_options.id_products IN (' . $q . ') ';


            $q1 .= ' group by  attribute_options.id_attribute_options';
            $q1 .= ' order by  attribute_options.sort_order';

            $query = $this->db->query($q1);

            $options = $query->result_array();
            $results[$i] = $attr;
            $results[$i]['options'] = $options;
            $i++;
        }

        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getDesigners($cond, $limit = '', $offset = '')
    {


        $sales = false;

        if (isset($cond['sales'])) {

            unset($cond['sales']);
            $sales = true;
        }

        $data = array();
        $q = "";
        $q .= "select  products.id_products ";


        $q .= "  from products ";
        //print_r($product_options);exit;
        if (isset($cond['category'])) {
            $q .= " join products_categories on products_categories.id_products = products.id_products ";
        }

        $q .= " join products_gallery on products_gallery.id_products = products.id_products ";

        if (isset($cond['classifications'])) {
            $q .= " join products_classifications on products_classifications.id_products = products.id_products ";
        }

        if (isset($cond['options'])) {
            $q .= " join product_options on products.id_products = product_options.id_products ";
        }


        /*	if(isset($cond['brands.title_url'.getUrlFieldLanguage($this->lang->lang())])) {
        $this->db->join('brands','brands.id_brands = products.id_brands');
        }*/
        /*	if(isset($cond['categories_sub.title_url'.getUrlFieldLanguage($this->lang->lang())])) {
        $this->db->join('categories_sub','categories_sub.id_categories_sub = products.id_categories_sub');
        }*/
        $q .= " where products.deleted=0 and products.status=0 ";
        if (isset($cond['title'])) {
            /*$this->db->like('UPPER(products.title'.getFieldLanguage().')',strtoupper($cond['products.title'.getFieldLanguage()]));*/
            $q .= " and UPPER(products.title) like '%" . strtoupper($cond['title']) . "%' ";
            unset($cond['title']);
        }

        if (isset($cond['sku'])) {
            $q .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['options'])) {
            $q .= " and product_options.deleted=0  and product_options.status=1";
        }

        if (isset($cond['id_products'])) {
            $q .= " and products.id_products=" . $cond['id_products'];
        }

        if (isset($cond['set_as_news'])) {
            $q .= "  AND  products.set_as_new=1 ";
        }

        if (isset($cond['display_in_home'])) {
            $q .= "  AND  products.display_in_home=1 ";
        }

        if (isset($cond['availability'])) {
            $q .= " and products.availability=" . $cond['availability'];
        }


        if (isset($cond['category'])) {
            $q .= " and products_categories.id_categories=" . $cond['category'];
        }

        if (isset($cond['classifications'])) {

            $q .= " and products_classifications.id_classifications=" . $cond['classifications'];
        }

        if (isset($cond['min_price']) && isset($cond['max_price'])) {
            $q .= " and products.price >=" . $cond['min_price'] . " and products.price <=" . $cond['max_price'];
        }

        if (isset($cond['All'])) {
        } else {
            if ($sales) {
                $q .= ' AND products.id_products IN ( select (
CASE 
WHEN
 price < list_price THEN id_products  
ELSE  0 
END
) from products where deleted =0) ';

            } else {
                $q .= ' AND products.id_products IN ( select (
CASE 
WHEN
 list_price <= price THEN id_products  
ELSE  0 
END
) from products where deleted = 0) ';
            }
        }

        //$cond['products.lang'] = $this->lang->lang();
        //print_r($cond);exit;
        /*$this->db->where($cond);*/

        if (isset($cond['options'])) {
            /*$this->db->where_in('product_options.id_attribute_options',$cond_arr);*/
            $q .= " and product_options.id_attribute_options IN (" . $cond['options'] . ")";

        }

        ///////////////////////////////////////SEARCH\\\\\\\\\\\\\\\\\\\\\\\\\\
        if (isset($cond['keywords'])) {
            $keywords = $cond['keywords'];
            $search1 = " select products_categories.id_products  from products_categories  join categories on products_categories.id_categories= products_categories.id_categories  where categories.deleted=0 ";
            $search1 .= "AND (UPPER(categories.title) like '%" . strtoupper($keywords) . "%')";

            $search2 = " select products_classifications.id_products  from products_classifications  join classifications on products_classifications.id_classifications= products_classifications.id_classifications  where classifications.deleted=0 ";
            $search2 .= "AND (UPPER(classifications.title) like '%" . strtoupper($keywords) . "%')";

            $search3 = " select products.id_products  from products  join designers on designers.id_designers= products.id_designers  where designers.deleted=0 ";
            $search3 .= "AND (UPPER(designers.title) like '%" . strtoupper($keywords) . "%')";

            $search4 = " select product_options.id_products  from product_options  join attribute_options on product_options.id_attribute_options= attribute_options.id_attribute_options  where attribute_options.deleted=0 ";
            $search4 .= "AND (UPPER(attribute_options.title) like '%" . strtoupper($keywords) . "%')";

            $q .= " and (products.id_products IN (" . $search1 . ") OR UPPER(products.title) like '%" . strtoupper($keywords) . "%'";
            $q .= " OR products.id_products IN (" . $search2 . ")";
            $q .= " OR products.id_products IN (" . $search3 . ")";
            $q .= " OR products.id_products IN (" . $search4 . ")";
            $q .= ")";
        }

        $q .= "  group by products.id_products ";
        $q .= "  order by products.id_products desc";


        $q1 = ' select designers.*';
        $q1 .= ' from designers';
        $q1 .= ' join products  on designers.id_designers= products.id_designers ';
        $q1 .= ' where designers.deleted=0 and products.deleted=0 ';


        $q1 .= ' AND products.id_products IN (' . $q . ') ';


        $q1 .= ' group by  designers.id_designers';
        $q1 .= ' order by  designers.sort_order';

        if ($limit != "") {
            $q1 .= ' limit ' . $limit . ' offset ' . $offset;
        }

        $query = $this->db->query($q1);
        if ($limit != '') {
            $results = $query->result_array();


        } else {
            $results = $query->num_rows();
        }

        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getClassifications($cond, $limit = '', $offset = '')
    {


        $sales = false;
        if (isset($cond['sales'])) {

            unset($cond['sales']);
            $sales = true;
        }

        $q = ' select classifications.*';
        $q .= ' from classifications';
        $q .= ' join products_classifications  on classifications.id_classifications= products_classifications.id_classifications ';
        $q .= ' where classifications.deleted=0';

        if ($sales) {
            $q .= ' AND products_classifications.id_products IN ( select (
CASE 
WHEN
 price < list_price THEN id_products  
ELSE  0 
END
) from products where deleted =0) ';
        } else {
            $q .= ' AND products_classifications.id_products IN ( select (
CASE 
WHEN
 list_price <= price THEN id_products  
ELSE  0 
END
) from products where deleted =0) ';
        }

        $q .= ' group by  classifications.id_classifications';
        $q .= ' order by  classifications.sort_order';


        $query = $this->db->query($q);
        $results = $query->result_array();
        $i = 0;
        $data = array();
        foreach ($results as $res) {
            $q3 = 'select products_classifications.id_products as id_products from products_classifications where products_classifications.deleted=0 AND  products_classifications.id_classifications=' . $res['id_classifications'];
            $q2 = 'select categories.*, products_categories.id_products';
            $q2 .= ' from categories';
            $q2 .= ' join products_categories on categories.id_categories= products_categories.id_categories';
            $q2 .= ' where categories.deleted=0 AND products_categories.id_products IN (' . $q3 . ')';

            $q2 .= "  group by categories.id_categories ";
            $q2 .= "  order by categories.sort_order asc ";

            if ($limit != "")
                $q2 .= ' limit ' . $limit . ' offset ' . $offset;

            $query2 = $this->db->query($q2);

            $categories = $query2->result_array();

            $data[$i] = $res;
            $data[$i]['categories'] = $categories;
            $gallery = $this->fct->getAll_cond('classifications_gallery', 'sort_order', array(
                'id_classifications' => $res['id_classifications']
            ));
            $data[$i]['gallery'] = $gallery;
            $i++;
        }

        return $data;
    }

    //////////////////////////////////////////////////////////////////////////////
    public function getSubCategoriesByCategory($id_categories)
    {
        $this->db->select('categories_sub.*');
        $this->db->from('categories_sub');
        $this->db->join('categories_sub_categories', 'categories_sub_categories.id_categories_sub = categories_sub.id_categories_sub');
        $cond = array(
            'categories_sub.deleted' => 0,
            'categories_sub_categories.id_categories' => $id_categories
        );
        $this->db->where($cond);
        $this->db->group_by('categories_sub.id_categories_sub');
        $this->db->order_by('categories_sub.sort_order');
        $query = $this->db->get();
        $results = $query->result_array();
        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getSubCategoriesByMultipleCategories($id_categories = array())
    {
        $this->db->select('categories_sub.*');
        $this->db->from('categories_sub');
        if (!empty($id_categories))
            $this->db->join('categories_sub_categories', 'categories_sub_categories.id_categories_sub = categories_sub.id_categories_sub');
        $cond = array(
            'categories_sub.deleted' => 0
        );
        $this->db->where($cond);
        if (!empty($id_categories))
            $this->db->where_in('categories_sub_categories.id_categories', $id_categories);
        $this->db->group_by('categories_sub.id_categories_sub');
        $this->db->order_by('categories_sub.sort_order');
        $query = $this->db->get();
        $results = $query->result_array();
        return $results;
    }

    public function getSubCategoriesByMultipleCategories2($id_categories = array())
    {
        $this->db->select('categories.*');
        $this->db->from('categories');

        $cond = array(
            'categories.deleted' => 0
        );
        $this->db->where($cond);
        if (!empty($id_categories))
            $this->db->where_in('categories.id_parent', $id_categories);
        $this->db->group_by('categories.id_categories');
        $this->db->order_by('categories.sort_order');
        $query = $this->db->get();
        $results = $query->result_array();

        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getCartDeliveredByBrands($cond)
    {

        $q = 'select distinct user.* ';
        $q .= ' from brands';
        $q .= ' join user on brands.id_user=user.id_user ';
        $q .= ' join products on brands.id_brands=products.id_brands ';
        $q .= ' where brands.deleted=0 and products.deleted=0';
        $q .= ' and products.id_products IN (' . implode(',', $cond['ids_products']) . ')';
        if (isset($cond['id_brands_s']) && !empty($cond['id_brands_s'])) {
            $q .= ' and brands.id_brands=' . $cond['id_brands_s'];
        }
        //$q .= ' and brands.deliver_by_supplier=1 ';
        $q .= "  group by brands.id_brands ";
        $q .= "  order by brands.id_brands asc ";

        $query = $this->db->query($q);
        $results = $query->result_array();

        if (isset($cond['id_brands_s']) && !empty($cond['id_brands_s'])) {
            $i = 0;
            $brands = array();
        } else {
            $i = 1;
            $brands[0]['id_user'] = "";
            $brands[0]['title'] = "spamiles";
            $brands[0]['deliver_by'] = "Delivered by spamiles";
        }


        foreach ($results as $val) {
            $brands[$i] = $val;
            $brands[$i]['deliver_by'] = "Supplier: " . $val['trading_name'];
            $i++;
        }

        return $brands;
    }

    public function getCartProducts($cart_items, $section = "", $cond = array())
    {


        $data = array();
        if (!empty($cart_items)) {
            $i = 0;
            $j = 0;

            foreach ($cart_items as $item) {

                
                $product = $this->getOneProduct($item['id']);
                $bool = true;

                if (isset($cond['ids_brands']) && !empty($cond['ids_brands']) && isset($item['options']['id_brands']) && !in_array($item['options']['id_brands'], $cond['ids_brands'])) {
                    $bool = false;
                }

                if (isset($cond['ids_brands']) && !empty($cond['ids_brands']) && !isset($item['options']['id_brands']) && !empty($product['id_brands']) && !in_array($product['id_brands'], $cond['ids_brands'])) {
                    $bool = false;

                }

                if ($bool) {

                    if (isset($cond['ids_brands']) && empty($cond['ids_brands']) && (true || $product['brand']['deliver_by_supplier'] == 1)) {

                        $bool = false;
                    }

                    if (isset($cond['ids_brands']) && !empty($cond['ids_brands']) && false) {
                        $bool = false;
                    }
                }

                if ($bool) {
                    $product = $this->getOneProduct($item['id']);
                    $sku = $product['sku'];
                    $gallery = $this->fct->getAll_cond('products_gallery', 'sort_order', array(
                        'id_products' => $item['id']
                    ));
                    $options = array();

                    if (!empty($item['options']['product_options'])) {
                        $opts = $item['options']['product_options'];
                        foreach ($opts as $key => $opt) {
                            $dd = $this->getCartProductOption($opt);
                            array_push($options, $dd);
                        }
                    }

                    if (!empty($options)) {

                        $options_en = serializecartOptions($options);
                        $cond_p['combination'] = $options_en;
                        $cond_p['id_products'] = $item['id'];
                        $stock = $this->fct->getonerow('products_stock', $cond_p);

                        $sku = $stock['sku'];
                    }

                    if (empty($section) || ($section == 'miles' && !empty($item['options']['redeem_miles'])) || ($section == 'products' && empty($item['options']['redeem_miles']))) {
                        $data[$i] = $item;
                        $data[$i]['product_info'] = $product;
                        $data[$i]['cart_options'] = $options;
                        $data[$i]['sku'] = $sku;
                        $data[$i]['redeem_miles'] = $item['options']['redeem_miles'];
                        $data[$i]['miles'] = $item['options']['miles'];
                        if (isset($item['options']['id_brands'])) {
                            $data[$i]['id_brands'] = $item['options']['id_brands'];
                        } else {
                            $data[$i]['id_brands'] = "";
                        }
                        $data[$i]['product_info']['gallery'] = $gallery;

                        $i++;
                    }
                }
            }
        }
        return $data;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getUserInfo($id_user = "")
    {
        $data = array();
        if ($id_user == "") {
            $id_user = $this->session->userdata('login_id');

            $correlation_id = $this->session->userdata('correlation_id');
        }
        $data = array();
        $this->db->select('user.*');
        $this->db->from('user');
        $cond['id_user'] = $id_user;
        if (isset($correlation_id)) {
            $cond['correlation_id'] = $correlation_id;
        }
        $this->db->where($cond);
        $query = $this->db->get();
        $results = $query->row_array();


        if (!empty($results)) {
            $this->db->select('orders.*');
            $this->db->from('orders');
            $cond = array(
                'id_user' => $id_user
            );
            $this->db->where($cond);
            $query = $this->db->get();
            $orders = $query->result_array();
            $data = $results;
            $data['orders'] = $orders;
            $data['result'] = 1;

            $this->db->select('trading_license.*');
            $this->db->from('trading_license');
            $cond = array(
                'id_user' => $id_user
            );
            $this->db->where($cond);
            $query = $this->db->get();
            $trading_license = $query->result_array();
            $data['trading_license'] = $trading_license;

            ///////////NEWSLETTER/////////////
            $newsletter = $this->fct->getonerecord('newsletter', array('id_user' => $results['id_user']));

            $data['newsletter'] = $newsletter;
            return $data;
        } else {
            /*            $this->removeSession();
            $this->session->set_flashdata('error_message', lang('access_denied'));
            $data['result'] = 0;
            redirect(route_to('user/login'));*/
        }
    }

    public function removeSession()
    {
        $this->session->unset_userdata('discount_auto_login');
        $this->session->unset_userdata('login_id');
        //$this->session->unset_userdata('uid');
        /*		$this->session->unset_userdata('roles');
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');*/
        $this->session->unset_userdata('type');
        $this->session->unset_userdata('account_type');
        $this->session->unset_userdata('last_login');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getAddressBook($id_user = '', $type = '', $cond = array())
    {
        $this->db->select('address_book.*,countries.title' . getFieldLanguage($this->lang->lang()) . ' AS country_name');
        $this->db->from('address_book');
        $this->db->join('countries', 'countries.id_countries = address_book.id_countries');
        if ($type != '') {
            $cond['address_book.' . $type] = 1;
        }
        if ($id_user != '') {
            $cond['address_book.id_user'] = $id_user;

        }

        if (isset($cond['keyword'])) {
            /*
			$this->db->or_like($like);*/
            $like['UPPER(countries.title)'] = strtoupper($cond['keyword']);
            $like['UPPER(address_book.city)'] = strtoupper($cond['keyword']);
            $like['UPPER(address_book.street_one)'] = strtoupper($cond['keyword']);
            $like['UPPER(address_book.street_two)'] = strtoupper($cond['keyword']);
            $this->db->or_like($like);

            unset($cond['keyword']);

        }
        $this->db->where($cond);
        $this->db->group_by('address_book.id_address_book');
        $this->db->order_by('address_book.id_address_book DESC');


        $query = $this->db->get();
        $results = $query->result_array();

        return $results;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*public function getUserAddresses($id_user = '',$type='')
    {
    $this->db->select('users_addresses.*,countries.title'.getFieldLanguage($this->lang->lang()).' AS country_name');
    $this->db->from('users_addresses');
    $this->db->join('countries','countries.id_countries = address_book.id_countries');
    if($type != '') {
    $cond['users_addresses.'.$type] = 1;
    }
    if($id_user != '') {
    $cond['users_addresses.id_user'] = $id_user;
    $this->db->where($cond);
    }
    $this->db->group_by('users_addresses.id_users_addresses');
    $this->db->order_by('users_addresses.id_users_addresses DESC');


    $query = $this->db->get();
    $results = $query->result_array();

    return $results;
    }*/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getUserOrders($id_user)
    {
        $this->db->select('orders.*');
        $this->db->from('orders');
        $cond = array(
            'id_user' => $id_user
        );
        $this->db->where($cond);
        $this->db->order_by('id_orders DESC');
        $query = $this->db->get();
        $results = $query->result_array();

        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function checkIfUserOrder($id_orders, $id_user)
    {
        $cond = array(
            'id_orders' => $id_orders,
            'id_user' => $id_user
        );
        $check = $this->fct->getonerow('orders', $cond);
        return $check;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getOrders($cond = array(), $limit = '', $offset = '')
    {

        /*		if(isset($cond['group_products']) && $cond['group_products']==1){
		$left="left";}else{
		$left="";	}*/
        $left = "left";


        /*	echo "<pre>";
        print_r($cond);exit;*/
        $this->db->select('orders.*, user.name, user.trading_name, user.email, user.phone ');
        //I think it's not needed - PW
        //,address_book.first_name,address_book.last_name, address_book.phone
        $this->db->from('orders');
        $this->db->join('user', 'orders.id_user = user.id_user', $left);
        if (isset($cond['brands.id_user'])) {
            $this->db->join('brands', 'brands.id_user = orders.id_user', $left);
        }

        //I think it's not needed - PW
        //$this->db->join('address_book', 'orders.billing_id = address_book.id_address_book', $left);

        if (isset($cond['user.first_name'])) {
            $this->db->like('UPPER(user.first_name)', strtoupper($cond['user.first_name']));
            unset($cond['user.first_name']);
        }

        if (isset($cond['user.name'])) {
            $this->db->like('UPPER(user.name)', strtoupper($cond['user.name']));
            unset($cond['user.name']);
        }

        if (isset($cond['user.city'])) {
            $this->db->like('UPPER(user.city)', strtoupper($cond['user.city']));
            unset($cond['user.city']);
        }

        if (isset($cond['user.surname'])) {
            $this->db->like('UPPER(user.last_name)', strtoupper($cond['user.surname']));
            unset($cond['user.surname']);
        }

        if (!empty($cond)) {
            $this->db->where($cond);
        }
        $this->db->group_by('orders.id_orders');
        $this->db->order_by('id_orders DESC');

        if ($limit != '') {
            $this->db->limit($limit, $offset);

        }
        $query = $this->db->get();


        if ($limit != '') {
            $result = $query->result_array();

        } else {
            $result = $query->num_rows();
        }

        return $result;
    }

    public function getpaymentNotOnline($cond = array(), $limit = '', $offset = '')
    {

        $left = "left";

        $this->db->select('orders.*, user.name, user.trading_name, user.email, user.phone ');

        $this->db->from('orders');
        $this->db->join('user', 'orders.id_user = user.id_user', $left);
        if (isset($cond['brands.id_user'])) {
            $this->db->join('brands', 'brands.id_user = orders.id_user', $left);
        }

        if (isset($cond['user.first_name'])) {
            $this->db->like('UPPER(user.first_name)', strtoupper($cond['user.first_name']));
            unset($cond['user.first_name']);
        }

        if (isset($cond['user.name'])) {
            $this->db->like('UPPER(user.name)', strtoupper($cond['user.name']));
            unset($cond['user.name']);
        }

        if (isset($cond['user.city'])) {
            $this->db->like('UPPER(user.city)', strtoupper($cond['user.city']));
            unset($cond['user.city']);
        }

        if (isset($cond['user.surname'])) {
            $this->db->like('UPPER(user.last_name)', strtoupper($cond['user.surname']));
            unset($cond['user.surname']);
        }

        if (!empty($cond)) {
            $this->db->where($cond);
        }
        
        $this->db->or_where('payment_method', 'cheque-on-delivery');
        $this->db->or_where('payment_method', 'checque');
        $this->db->or_where('payment_method', 'cash'); 
        
        $this->db->group_by('orders.id_orders');
        $this->db->order_by('id_orders DESC');

        if ($limit != '') {
            $this->db->limit($limit, $offset);

        }
        $query = $this->db->get();

        if ($limit != '') {
            $result = $query->result_array();

        } else {
            $result = $query->num_rows();
        }

        return $result;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getOrder($order_id, $cond = array())
    {
        $cond2 = $cond;
        if (isset($cond['all_line_items'])) {
            $all_line_items = 1;
            unset($cond['all_line_items']);
        }

        if (isset($cond['id_brands_s'])) {
            unset($cond['id_brands_s']);
        }
        $data = array();
        $this->db->select('orders.*');
        $this->db->from('orders');
        $this->db->where('id_orders', $order_id);
        if (isset($cond)) {
            $this->db->where($cond);
        }
        $query = $this->db->get();
        $order = $query->row_array();
        if (!empty($order)) {
            //get payment method
            $this->db->select('payment_methods.*');
            $this->db->from('payment_methods');
            $this->db->where('index', $order['payment_method']);
            $query = $this->db->get();
            $paymentmethod = $query->row_array();
            //get user
            $this->db->select('user.*');
            $this->db->from('user');
            $this->db->where('id_user', $order['id_user']);
            $query = $this->db->get();
            $user = $query->row_array();
            // get billing information
            $this->db->select('address_book.*');
            $this->db->from('address_book');
            $this->db->where('id_address_book', $order['billing_id']);
            $query = $this->db->get();
            $billing = $query->row_array();
            // get billing country
            if (!isset($billing['id_countries'])) {
                $billing['id_countries'] = 0;
            }
            $this->db->select('countries.*');
            $this->db->from('countries');
            $this->db->where('id_countries', $billing['id_countries']);
            $query = $this->db->get();
            $country = $query->row_array();
            $billing['country'] = $country;
            // get delivery information
            $this->db->select('address_book.*');
            $this->db->from('address_book');

            $this->db->where('id_address_book', $order['delivery_id']);
            $query = $this->db->get();
            $delivery = $query->row_array();
            // get delivery country
            if (!isset($delivery['id_countries'])) {
                $delivery['id_countries'] = 0;
            }
            $this->db->select('countries.*');
            $this->db->from('countries');
            $this->db->where('id_countries', $delivery['id_countries']);
            $query = $this->db->get();
            $country = $query->row_array();
            $delivery['country'] = $country;
            // line items
            $line_items = array();
            $this->db->select('line_items.*');
            $this->db->from('line_items');
            $this->db->where('id_orders', $order['id_orders']);
            if (!isset($all_line_items)) {
                $this->db->where('redeem', 0);
            }
            $query = $this->db->get();
            $items = $query->result_array();

            $line_items = $this->ecommerce_model->getLineItems($items);
            if (!isset($all_line_items)) {
                $line_items_redeem_by_miles = array();
                $this->db->select('line_items.*');
                $this->db->from('line_items');
                $this->db->where('id_orders', $order['id_orders']);
                $this->db->where('redeem', 1);
                $query = $this->db->get();
                $items = $query->result_array();

                $line_items_redeem_by_miles = $this->ecommerce_model->getLineItems($items);
            }

            // set data
            $data = $order;
            $data['user'] = $user;
            $data['billing'] = $billing;
            $data['delivery'] = $delivery;
            $data['line_items'] = $line_items;


            /*	if(!isset($all_line_items)){
		$data['line_items_redeem_by_miles']    = $line_items_redeem_by_miles;}*/

            $data['line_items_brands'] = $this->getLineItemsByBrands($order, $cond2);

            $data['paymentmethod'] = $paymentmethod;
            /*print '<pre>';
        print_r($data);
        exit;*/
        }
        return $data;
    }

    public function getLineItems($items)
    {
        $i = 0;
        $line_items = array();
        foreach ($items as $item) {
            $this->db->select('products.*');
            $this->db->from('products');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $res = $query->row_array();
            // product gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $gallery = $query->result_array();
            $line_items[$i] = $item;
            $line_items[$i]['product'] = $res;
            $line_items[$i]['product']['gallery'] = $gallery;
            $i++;
        }
        return $line_items;
    }

    public function getLineItemsByBrands($order, $cond)
    {


        $data = array();
        if (isset($cond['all_line_items'])) {
            $all_line_items = 1;
            unset($cond['all_line_items']);
        }

        if (isset($cond['id_brands_s'])) {

            $cond_2['id_brands_s'] = $cond['id_brands_s'];
        }


        $line_items = array();
        $this->db->select('line_items.*');
        $this->db->from('line_items');
        $this->db->where('id_orders', $order['id_orders']);
        $query = $this->db->get();
        $items = $query->result_array();

        $ids_products = $this->ecommerce_model->getIds('id_products', $items);
        $cond_2['ids_products'] = $ids_products;

        $suppliers = $this->ecommerce_model->getCartDeliveredByBrands($cond_2);


        $i = 0;
        foreach ($suppliers as $supplier) {  // line items
            $line_items = array();
            if (empty($supplier['id_user'])) {
                $supplier['id_user'] = 0;
            }

            $cond_line_items = array();
            if (!isset($all_line_items)) {
                $cond_line_items['redeem'] = 0;
            }
            $cond_line_items['id_orders'] = $order['id_orders'];
            $cond_line_items['id_supplier'] = $supplier['id_user'];

            $items = $this->ecommerce_model->getlineItemsBySupplier($cond_line_items);

            $line_items = $this->ecommerce_model->getLineItems($items);

            if (!isset($all_line_items)) {
                $line_items_redeem_by_miles = array();

                $cond_line_items = array();
                $cond_line_items['id_orders'] = $order['id_orders'];
                $cond_line_items['id_supplier'] = $supplier['id_user'];
                $cond_line_items['redeem'] = 1;
                $items = $this->ecommerce_model->getlineItemsBySupplier($cond_line_items);

                $line_items_redeem_by_miles = $this->ecommerce_model->getLineItems($items);
            }
            if (!empty($line_items_redeem_by_miles) || !empty($line_items)) {
                $data[$i] = $supplier;
                $data[$i]['line_items'] = $line_items;
                if (!isset($all_line_items)) {
                    $data[$i]['line_items_redeem_by_miles'] = $line_items_redeem_by_miles;
                }
            }

            $i++;
        }

        return $data;
    }

    public function getlineItemsBySupplier($cond)
    {
        $q = 'select line_items.* ';
        $q .= ' from line_items';
        $q .= ' join orders on orders.id_orders=line_items.id_orders';
        $q .= ' where orders.deleted=0';
        $q .= ' and orders.id_orders=' . $cond['id_orders'];
        $q .= ' and orders.id_supplier=' . $cond['id_supplier'];
        if (isset($cond['redeem'])) {
            $q .= ' and line_items.redeem=' . $cond['redeem'];
        }

        $q .= "  group by line_items.id_line_items ";
        $q .= "  order by line_items.id_line_items asc ";

        $query = $this->db->query($q);
        $line_items = $query->result_array();
        return $line_items;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getQuotationLineItems($items)
    {
        $i = 0;
        $line_items = array();
        foreach ($items as $item) {
            $this->db->select('products.*');
            $this->db->from('products');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $res = $query->row_array();
            // product gallery
            $this->db->select('*');
            $this->db->from('products_gallery');
            $this->db->where('id_products', $item['id_products']);
            $query = $this->db->get();
            $gallery = $query->result_array();
            $line_items[$i] = $item;
            $line_items[$i]['product'] = $res;
            $line_items[$i]['product']['gallery'] = $gallery;
            $i++;
        }
        return $line_items;
    }

    public function getQuotation($order_id, $cond = array())
    {
        $data = array();
        $this->db->select('quotation.*');
        $this->db->from('quotation');
        $this->db->where('id_quotation', $order_id);
        if (isset($cond)) {
            $this->db->where($cond);
        }
        $query = $this->db->get();
        $order = $query->row_array();
        if (!empty($order)) {

            //get user
            $this->db->select('user.*');
            $this->db->from('user');
            $this->db->where('id_user', $order['id_user']);
            $query = $this->db->get();
            $user = $query->row_array();


            // line items
            $quotation_line_items = array();
            $this->db->select('quotation_line_items.*');
            $this->db->from('quotation_line_items');
            $this->db->where('id_quotation', $order['id_quotation']);

            $query = $this->db->get();
            $items = $query->result_array();
            $quotation_line_items = $this->ecommerce_model->getQuotationLineItems($items);

            // set data
            $data = $order;
            $data['user'] = $user;
            $data['quotation_line_items'] = $quotation_line_items;
            $data['quotation_line_items_redeem_by_miles'] = array();

            /*print '<pre>';
        print_r($data);
        exit;*/
        }
        return $data;
    }

    public function updateQuotation($id_quotation, $rand)
    {
        $cond['rand'] = $rand;
        $order_details = $this->ecommerce_model->getQuotation($id_quotation, $cond);
        $quotation_line_items = $order_details['quotation_line_items'];

        $net_price = 0;
        $net_discount_price = 0;
        $weight = 0;
        $number_of_pieces = 0;
        $i = 0;
        $miles = 0;
        $total_redeem_miles = 0;
        $total_miles = 0;
        $net_redeem_miles_price = 0;
        $net_discount_redeem_miles_price = 0;
        $net_discount_price_currency = 0;
        $discount = 0;


        foreach ($quotation_line_items as $val) {
            $weight = $weight + $val['weight'];
            $total_miles = $total_miles + $val['miles'];
            $total_redeem_miles = $total_redeem_miles + $val['redeem_miles'];
            $discount = $discount + ($val['discount'] * $val['quantity']);
            $net_price = $net_price + $val['total_price_old'];
            $net_discount_price = $net_discount_price + $val['total_price'];
            $net_discount_price_currency = $net_discount_price_currency + $val['total_price_currency'];
            if ($val['redeem'] == 1) {
                $net_redeem_miles_price = $net_redeem_miles_price + $val['total_price_old'];
                $net_discount_redeem_miles_price = $net_discount_redeem_miles_price + $val['total_price'];
            }
        }

        $order_update_template['weight'] = $weight;
        $order_update_template['miles'] = $total_miles;
        $order_update_template['redeem_miles'] = $total_redeem_miles;
        $order_update_template['total_price'] = $net_price;
        $order_update_template['redeem_discount_miles_amount'] = $net_discount_redeem_miles_price;
        $order_update_template['redeem_miles_amount'] = $net_redeem_miles_price;
        $order_update_template['total_price_currency'] = $net_price;
        $order_update_template['discount'] = $discount;
        $order_update_template['amount'] = $net_discount_price;
        $order_update_template['amount_currency'] = $net_discount_price_currency;
        $order_update_template['sub_total'] = $net_price;

        $this->db->where('id_quotation', $id_quotation);
        $this->db->update('quotation', $order_update_template);

    }

    public function getProductsStock($cond)
    {

        $q = 'select products_stock.* ';
        $q .= ' from products_stock';
        $q .= ' where products_stock.deleted=0';
        $q .= ' and products_stock.id_products=' . $cond['id_products'];

        $q .= "  group by products_stock.id_products_stock ";
        $q .= "  order by products_stock.id_products_stock asc ";

        $query = $this->db->query($q);
        $products_stock = $query->result_array();

        return $products_stock;
    }

    public function getUserQuotations($id_user)
    {
        $this->db->select('quotation.*');
        $this->db->from('quotation');
        $cond = array(
            'id_user' => $id_user
        );
        $this->db->where($cond);
        $this->db->order_by('id_quotation DESC');
        $query = $this->db->get();
        $results = $query->result_array();


        return $results;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getUserCompareProducts($id_user = 0, $session_id = '')
    {
        $data = array();
        if (checkUserIfLogin()) {
            $this->db->select('products.*, compare_products.options');
            $this->db->from('products');
            $this->db->join('compare_products', 'compare_products.id_products = products.id_products');
            $this->db->where("compare_products.session_id", $session_id);
            if ($id_user != 0) {
                $this->db->or_where("compare_products.id_user", $id_user);
            }
            $this->db->group_by('compare_products.id_products');
            $this->db->order_by('products.sort_order');
            $query = $this->db->get();
            $results = $query->result_array();
            if (!empty($results)) {
                $i = 0;
                foreach ($results as $res) {
                    $this->db->select('*');
                    $this->db->from('products_gallery');
                    $cond = array(
                        'id_products' => $res['id_products']
                    );
                    $this->db->where($cond);
                    $this->db->order_by('sort_order');
                    $query = $this->db->get();
                    $gallery = $query->result_array();
                    $data[$i] = $res;
                    $data[$i]['gallery'] = $gallery;
                    $attributes = $this->getProductAttributes($res['id_products']);
                    // get stock
                    $this->db->select('*');
                    $this->db->from('products_stock');
                    $cond = array(
                        'id_products' => $res['id_products']
                    );
                    $this->db->where($cond);
                    $this->db->order_by('id_products_stock');
                    $query = $this->db->get();
                    $stock = $query->result_array();
                    $data[$i]['attributes'] = $attributes;
                    $data[$i]['stock'] = $stock;
                    $i++;
                }
            }
        } else {
            if ($this->session->userdata('compare_products'))
                $products = $this->session->userdata('compare_products');
            if (!empty($products)) {
                foreach ($products as $product) {
                    $res = $this->fct->getonerecord('products', array(
                        'id_products' => $product['id_products']
                    ));
                    $this->db->select('*');
                    $this->db->from('products_gallery');
                    $cond = array(
                        'id_products' => $res['id_products']
                    );
                    $this->db->where($cond);
                    $this->db->order_by('sort_order');
                    $query = $this->db->get();
                    $gallery = $query->result_array();
                    $data[$i] = $res;
                    $data[$i]['gallery'] = $gallery;
                    $attributes = $this->getProductAttributes($res['id_products']);
                    // get stock
                    $this->db->select('*');
                    $this->db->from('products_stock');
                    $cond = array(
                        'id_products' => $res['id_products']
                    );
                    $this->db->where($cond);
                    $this->db->order_by('id_products_stock');
                    $query = $this->db->get();
                    $stock = $query->result_array();
                    $data[$i]['attributes'] = $attributes;
                    $data[$i]['stock'] = $stock;
                    $i++;
                }
            }
        }
        /*print '<pre>';
        print_r($data);exit;*/
        return $data;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function insertIntoCompareProductBySession($array, $data)
    {
        $counts = count($array);
        $array[$counts] = $data;
        $this->session->set_userdata('compare_products', $array);
    }


    public function getUserProductsCompareIds()
    {
        $id_user = $this->session->userdata('login_id');
        $data = array();
        $this->db->select('products.id_products');
        $this->db->from('products');
        $this->db->join('compare_products', 'compare_products.id_products = products.id_products');
        $cond = array(
            'compare_products.id_user' => $id_user
        );

        $this->db->or_where("compare_products.id_user", $id_user);

        $this->db->group_by('compare_products.id_products');
        $this->db->order_by('products.sort_order');
        $query = $this->db->get();
        $results = $query->result_array();
        $processed = array_map(function ($a) {
            return array_pop($a);
        }, $results);
        /*print '<pre>';
        print_r($data);exit;*/
        return $processed;
    }

    public function checkIfInCompareProducts($id_product, $productsCompare)
    {
        if (in_array($id_product, $productsCompare)) {
            return true;
        } else {
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function checkUserCompareProduct($id_user = 0, $session_id = '', $id_product)
    {
        $q = 'SELECT * FROM compare_products WHERE id_products = ' . $id_product . ' AND ( session_id = "' . $session_id . '"';
        if ($id_user != 0)
            $q .= ' OR id_user = ' . $id_user;
        $q .= ' )';
        $query = $this->db->get();
        $results = $query->row_array();
        return $results;
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    public function getProductAttributes($id_products)
    {
        // get attributes
        $this->db->select('attributes.*');
        $this->db->from('product_attributes');
        $this->db->join('attributes', 'product_attributes.id_attributes = attributes.id_attributes');
        $this->db->join('products_stock', 'products_stock.id_products = product_attributes.id_products');
        $cnd = array(
            'product_attributes.id_products' => $id_products,
            'product_attributes.deleted' => 0,
            'products_stock.status' => 1

        );
        $this->db->where($cnd);
        $this->db->group_by('attributes.id_attributes');
        $this->db->order_by('attributes.sort_order');
        $query = $this->db->get();
        $attributes = $query->result_array();
        if (!empty($attributes)) {
            $k = 0;
            foreach ($attributes as $attr) {
                // get product attribute options
                $this->db->select('attribute_options.*');
                $this->db->from('attribute_options');
                //$this->db->join('product_options','product_options.id_attribute_options = attribute_options.id_attribute_options');
                $cnd = array(
                    //'product_options.id_products'=>$results['id_products'],
                    'attribute_options.id_attributes' => $attr['id_attributes']

                    //'product_options.status'=>1
                );
                $this->db->where($cnd);
                $this->db->group_by('attribute_options.id_attribute_options');
                $this->db->order_by('attribute_options.sort_order');
                $query = $this->db->get();
                $options = $query->result_array();

                $attributes[$k]['options'] = $options;
                $k++;
            }
        }
        return $attributes;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getProductCategories($id_products)
    {
        // get attributes
        $this->db->select('categories.*');
        $this->db->from('categories');
        $this->db->join('products_categories', 'products_categories.id_categories = categories.id_categories');
        $cnd = array(
            'products_categories.id_products' => $id_products
        );
        $this->db->where($cnd);
        $this->db->group_by('categories.id_categories');
        $this->db->order_by('categories.sort_order');
        $query = $this->db->get();
        $categories = $query->result_array();
        return $categories;
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    public function getPurchases($cond)
    {


        $q = "";
        $q .= " select  purchases.*";
        $q .= " from purchases ";

        $q .= ' where purchases.deleted=0';
        $q .= ' and purchases.id_products=' . $cond['id_products'];
        if (isset($cond['type'])) {
            $q .= ' and purchases.type="' . $cond['type'] . '"';
        }

        $q .= "  group by purchases.id_purchases ";
        $q .= "  order by purchases.created_date asc ";


        $query = $this->db->query($q);

        $purchases = $query->result_array();
        $qty_sold_by_admin = 0;
        $qty_payable = 0;
        if (!empty($purchases)) {

            foreach ($purchases as $res) {

                if ($res['status'] == 1) {
                    $qty_sold_by_admin = $qty_sold_by_admin + $res['quantity'];
                }
                if ($res['status'] == 2) {
                    $qty_payable = $qty_payable + $res['quantity'];
                }

            }

        }
        $data['qty_sold_by_admin'] = $qty_sold_by_admin;
        $data['qty_payable'] = $qty_payable;
        $data['purchases'] = $purchases;
        return $data;
    }

    public function getInventory($cond, $limit = '', $offset = '', $order = 'products.sort_order', $desc = "asc")
    {

        $sqla = "SELECT 'product' as type, products.sku, products.id_products, products.created_date as created_date FROM products ";
        if (isset($cond['category']) || isset($cond['categories'])) {
            $sqla .= "  join products_categories on products_categories.id_products = products.id_products ";
        }

        if (isset($cond['id_user']) || isset($cond['id_user'])) {
            $sqla .= "  join user on products.id_user = user.id_user ";
        }
        $sqla .= " where products.deleted=0 and products.group_products=0  and products.id_products not in (select id_products from products_stock where deleted=0 and status=1 ) ";

        if (isset($cond['id_brands']) && !empty($cond['id_brands'])) {
            $sqla .= " and products.id_brands=" . $cond['id_brands'];
        }

        if (isset($cond['threshold']) && !empty($cond['threshold'])) {
            $sqla .= " and products.threshold_flag=" . $cond['threshold'];
        }

        if (isset($cond['id_supplier']) && !empty($cond['id_supplier'])) {
            $sqla .= " and products.id_user=" . $cond['id_supplier'];
        }

        if (isset($cond['id_user']) && !empty($cond['id_user'])) {
            $sqla .= " and products.id_user=" . $cond['id_user'];
        }

        if (isset($cond['brands']) && !empty($cond['brands'])) {
            $sqla .= " and products.id_brands IN (" . $cond['brands'] . ")";
        }


        if (isset($cond['from_date']) && !empty($cond['from_date'])) {
            $sqla .= " and products.created_date>='" . $cond['from_date'] . "'";
        }

        if (isset($cond['to_date']) && !empty($cond['to_date'])) {
            $sqla .= " and products.created_date<='" . $cond['to_date'] . "'";
        }

        if (isset($cond['category']) && !empty($cond['category'])) {
            $sqla .= " and products_categories.id_categories=" . $cond['category'];
        }
        if (isset($cond['sku']) && !empty($cond['sku'])) {
            $sqla .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['product_name']) && !empty($cond['product_name'])) {
            $find = array('"');
            $replace = array('');
            $product_name = str_replace($find, $replace, $cond['product_name']);
            $sqla .= ' and (UPPER(products.title) like "%' . strtoupper($product_name) . '%"';
            $find = array("'");
            $replace = array('');
            $product_name = str_replace($find, $replace, $cond['product_name']);
            $sqla .= " OR UPPER(products.title) like '%" . strtoupper($product_name) . "%') ";
        }

        if (isset($cond['sales']) && $cond['sales'] == 1) {
            $sqla .= ' AND products.discount_status=1 ';
        }

        $sqla .= " group by products.id_products ";

        $sqla .= " UNION (SELECT 'stock' as type, products_stock.id_products_stock as sku, products_stock.id_products_stock as id_products, products_stock.created_date as created_date FROM  products_stock join products on products.id_products=products_stock.id_products  ";
        if (isset($cond['category']) || isset($cond['categories'])) {
            $sqla .= "  join products_categories on products_categories.id_products = products.id_products ";
        }

        if (isset($cond['id_user']) || isset($cond['id_user'])) {
            $sqla .= "  join user on products.id_user = user.id_user ";
        }
        $sqla .= "where  products.deleted=0 and products.group_products=0 and products_stock.deleted=0 and products_stock.status=1  ";

        if (isset($cond['id_brands']) && !empty($cond['id_brands'])) {
            $sqla .= " and products.id_brands=" . $cond['id_brands'];
        }

        if (isset($cond['brands']) && !empty($cond['brands'])) {
            $sqla .= " and products.id_brands IN (" . $cond['brands'] . ")";
        }

        if (isset($cond['id_user']) && !empty($cond['id_user'])) {
            $sqla .= " and products.id_user=" . $cond['id_user'];
        }

        if (isset($cond['from_date']) && !empty($cond['from_date'])) {
            $sqla .= " and products_stock.created_date>='" . $cond['from_date'] . "'";
        }

        if (isset($cond['threshold']) && !empty($cond['threshold'])) {
            $sqla .= " and products_stock.threshold_flag=" . $cond['threshold'];
        }

        if (isset($cond['to_date']) && !empty($cond['to_date'])) {
            $sqla .= " and products_stock.created_date<='" . $cond['to_date'] . "'";
        }

        if (isset($cond['category']) && !empty($cond['category'])) {
            $sqla .= " and products_categories.id_categories=" . $cond['category'];
        }
        if (isset($cond['sku'])) {
            $sqla .= " and UPPER(products.sku) like '%" . strtoupper($cond['sku']) . "%' ";
        }

        if (isset($cond['product_name']) && !empty($cond['product_name'])) {
            $find = array('"');
            $replace = array('');
            $product_name = str_replace($find, $replace, $cond['product_name']);
            $sqla .= ' and (UPPER(products.title) like "%' . strtoupper($product_name) . '%"';
            $find = array("'");
            $replace = array('');
            $product_name = str_replace($find, $replace, $cond['product_name']);
            $sqla .= " OR UPPER(products.title) like '%" . strtoupper($product_name) . "%') ";
        }


        if (isset($cond['sales'])) {
            $sqla .= ' AND products_stock.discount_status=' . $cond['sales'];
        }
        $sqla .= " group by products_stock.id_products_stock )  ";
        $sqla .= " ORDER BY sku ASC";
        if ($limit != "") {
            $sqla .= " LIMIT " . $limit . " OFFSET " . $offset;
        }


        //echo $sqla;exit;
        $query = $this->db->query($sqla);
        if ($limit != "") {
            $results = $query->result_array();

        } else {
            return $query->num_rows();
        }

        $j = 0;
        $data = array();
        $total_qty = 0;
        $total_qty_sold_by_admin = 0;
        $total_qty_payable = 0;
        $total_qty_sold_by_admin_paid = 0;
        $total_qty_payable_paid = 0;
        if ($limit != "") {

            foreach ($results as $res) {

                if ($res['type'] == "stock") {
                    $product = $this->fct->getonerecord('products_stock', array('id_products_stock' => $res['id_products']));
                    $product['product'] = $this->fct->getonerecord('products', array('id_products' => $product['id_products']));


                    $product['id_products'] = $product['id_products_stock'];
                    $product['title'] = $product['product']['title'];
                    $product['brand_name'] = $this->fct->getonecell('brands', 'title', array('id_brands' => $product['product']['id_brands']));
                }

                if ($res['type'] == "product") {
                    $product = $this->fct->getonerecord('products', array('id_products' => $res['id_products']));
                    $product['brand_name'] = $this->fct->getonecell('brands', 'title', array('id_brands' => $product['id_brands']));
                }

                $data[$j] = $product;
                $data[$j]['type'] = $res['type'];


                $attributes = $this->getProductAttributes($res['id_products']);
                $data[$j]['attributes'] = $attributes;


                /*					$miles      = $this->fct->getonerecord('miles',array('id_miles'=>$product['id_miles']));
                    $data[$j]['miles']      = $miles;
				    $cond_inventory_2['id_products']=$res['id_products'];
					$cond_inventory_2['type']=$res['type'];
					$purchases      = $this->getPurchases($cond_inventory_2);

					$data[$j]['qty_sold_by_admin']          = $purchases['qty_sold_by_admin'];
					$total_qty_sold_by_admin=$total_qty_sold_by_admin+$purchases['qty_sold_by_admin'];
                    $data[$j]['qty_payable']      = $purchases['qty_payable'];
					$total_qty_payable=$total_qty_payable+$purchases['qty_payable'];*/


/////////////////////////////////////// delivery order line items //////////////////////////////////////////////
                $product['product_type'] = $res['type'];

                $protuct_quantites = $this->admin_fct->getProductsStock($product, array(), $cond);


                $qty_sold_by_admin = $protuct_quantites['qty_sold_by_admin'];
                $qty_payable = $protuct_quantites['qty_payable'];

                $qty_sold_by_admin_paid = $protuct_quantites['qty_sold_by_admin_paid'];
                $qty_payable_paid = $protuct_quantites['qty_payable_paid'];

                $remain_consignment = $protuct_quantites['remain_consignment'];
                $remain_paid = $protuct_quantites['remain_paid'];
                $quantity = $protuct_quantites['quantity'];


                $data[$j]['qty_sold_by_admin'] = $qty_sold_by_admin;
                $data[$j]['qty_payable'] = $qty_payable;
                $data[$j]['remain_consignment'] = $remain_consignment;
                $data[$j]['remain_paid'] = $remain_paid;
                $data[$j]['quantity'] = $quantity;

                $total_qty = $total_qty + $quantity;
                $total_qty_sold_by_admin = $total_qty_sold_by_admin + $qty_sold_by_admin;
                $total_qty_payable = $total_qty_payable + $qty_payable;

                $total_qty_sold_by_admin_paid = $total_qty_sold_by_admin_paid + $qty_sold_by_admin_paid;
                $total_qty_payable_paid = $total_qty_payable_paid + $qty_payable_paid;
                $j++;
            }
            $result['products'] = $data;
            $result['total_qty_payable'] = $total_qty_payable;
            $result['total_qty_sold_by_admin'] = $total_qty_sold_by_admin;

            $result['total_qty_payable_paid'] = $total_qty_payable_paid;
            $result['total_qty_sold_by_admin_paid'] = $total_qty_sold_by_admin_paid;

            $result['total_qty_remain_paid'] = $total_qty_payable - $total_qty_payable_paid;
            $result['total_qty_consignment'] = $total_qty_sold_by_admin - $total_qty_sold_by_admin_paid;

//$result['total_qty']=$result['total_qty_remain_paid']+$result['total_qty_consignment'];
            $result['total_qty'] = $total_qty;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            return $result;

        }

    }


    public function getProfits($cond, $limit = '', $offset = '', $order = 'line_items.created_date', $desc = "asc")
    {
        $default_currency = 'AED';

////////////////////////////// SELECT BRANDS //////////////////////////////////////////////
        $order_sql = " select id_orders from orders ";
        $order_sql .= " join user on user.id_user=orders.id_user where orders.deleted=0 ";
        if (isset($cond['from_date']) && !empty($cond['from_date'])) {
            $order_sql .= " and orders.created_date>='" . $cond['from_date'] . "'";
        }
        if (isset($cond['to_date']) && !empty($cond['to_date'])) {
            $order_sql .= " and orders.created_date<='" . $cond['to_date'] . "'";
        }

        if (isset($cond['country']) && !empty($cond['country'])) {
            $order_sql .= " and user.id_countries='" . $cond['country'] . "'";
        }

        if (isset($cond['id_users_areas']) && !empty($cond['id_users_areas'])) {
            $order_sql .= " and user.id_users_areas='" . $cond['id_users_areas'] . "'";
        }


        if (isset($cond['status']) && !empty($cond['status'])) {
            $order_sql .= " and orders.status='" . $cond['status'] . "'";
        }


        if (isset($cond['account_manager']) && !empty($cond['account_manager'])) {
            $order_sql .= " and user.id_account_manager='" . $cond['account_manager'] . "'";
        }

        if (isset($cond['payment_method']) && !empty($cond['payment_method'])) {
            $order_sql .= " and orders.payment_method='" . $cond['payment_method'] . "'";
        }


        $sqla = " SELECT brands.* FROM products ";
        $sqla .= " join line_items on line_items.id_products=products.id_products ";
        $sqla .= " join brands on brands.id_brands=products.id_brands ";

        $sqla .= " where  line_items.invoice=0 and brands.deleted=0 and line_items.id_orders IN (" . $order_sql . ")";

        if (isset($cond['id_brands']) && !empty($cond['id_brands'])) {
            $sqla .= " and brands.id_brands='" . $cond['id_brands'] . "'";
        }

        if (isset($cond['supplier']) && !empty($cond['supplier'])) {
            $sqla .= " and brands.id_brands IN (select id_brands from brands where id_user=" . $cond['supplier'] . ")";
        }

        $sqla .= " group by brands.id_brands order by brands.created_date  ";


        $query = $this->db->query($sqla);
        $brands = $query->result_array();


        ///////////////////////////////////////////////////////////////////////////////////
        $data = array();
        $arr = array();
        $i = 0;
        $total_qty_sold_by_admin = 0;
        $total_qty_payable = 0;
        $total_qty_sold_by_admin_paid = 0;
        $total_qty_payable_paid = 0;
        $total_amount_credits = 0;
        $total_amount = 0;
        $total_customer_charge = 0;
        foreach ($brands as $brand) {

            $order_sql .= " select id_orders from orders where orders.deleted=0 ";
            if (isset($cond['from_date']) && !empty($cond['from_date'])) {
                $order_sql .= " and orders.created_date>='" . $cond['from_date'] . "'";
            }
            if (isset($cond['to_date']) && !empty($cond['to_date'])) {
                $order_sql .= " and orders.created_date<='" . $cond['to_date'] . "'";
            }

            $sqla = " SELECT line_items.*, orders.payment_method as payment_method, orders.id_user, orders.currency FROM line_items ";
            $sqla .= " join products on line_items.id_products=products.id_products ";
            $sqla .= " join orders on line_items.id_orders=orders.id_orders ";
            $sqla .= " left join orders_recording on orders_recording.id_line_items=line_items.id_line_items ";
            $sqla .= " where  ";
            $sqla .= "  line_items.invoice=0 and products.id_brands=" . $brand['id_brands'];
            if (isset($cond['from_date']) && !empty($cond['from_date'])) {
                $sqla .= " and orders.created_date>='" . $cond['from_date'] . "'";
            }
            if (isset($cond['to_date']) && !empty($cond['to_date'])) {
                $sqla .= " and orders.created_date<='" . $cond['to_date'] . "'";
            }

            if (isset($cond['status']) && !empty($cond['status'])) {
                $sqla .= " and orders.status='" . $cond['status'] . "'";
            }

            if (isset($cond['id_brands']) && !empty($cond['id_brands'])) {
                $sqla .= " and products.id_brands='" . $cond['id_brands'] . "'";
            }

            if (isset($cond['supplier']) && !empty($cond['supplier'])) {
                $sqla .= " and products.id_brands IN (select id_brands from brands where deleted=0 and id_user=" . $cond['supplier'] . ")";
            }

            $sqla .= " group by line_items.id_line_items order by line_items.created_date  ";


            $query = $this->db->query($sqla);
            $line_items = $query->result_array();


            $total_brand_price = 0;
            if (!empty($line_items)) {

                $arr[$i] = $brand;

                $j = 0;


                $items = array();
                foreach ($line_items as $line_item) {
                    $bool = false;
                    $order = $this->getOrder($line_item['id_orders']);
                    if (!empty($line_item['options_en'])) {
                        $product = $this->fct->getonerecord('products_stock', array('combination' => $line_item['options_en'], 'id_products' => $line_item['id_products']));
                        if (!empty($product)) {
                            $product['product'] = $this->fct->getonerecord('products', array('id_products' => $product['id_products']));
                            $product['id_products'] = $product['id_products_stock'];
                            $product['title'] = $product['product']['title'];

                            $type = "stock";
                            $bool = true;
                        }
                    }


                    if (empty($line_item['options_en'])) {
                        $type = "product";
                        $product = $this->fct->getonerecord('products', array('id_products' => $line_item['id_products']));
                        $bool = true;
                    }

                    if ($bool) {
                        $customer_charge = changeCurrency($line_item['customer_shipping_charge'], false, '', $default_currency, $line_item['currency']);
                        $price = changeCurrency($line_item['price'], false, '', $default_currency, $line_item['currency']);
                        $total_price = $price * $line_item['quantity'];
                        if ($line_item['payment_method'] == "credits") {
                            $credits_val = ($total_price * 2.5) / 100;
                        } else {
                            $credits_val = 0;
                        }
                        $line_item['trading_name'] = $this->fct->getonecell('user', 'trading_name', array('id_user' => $line_item['id_user']));
                        $line_item['credits_val'] = $credits_val;

                        $line_item['total_price'] = $total_price;
                        $line_item['customer_shipping_charge'] = $customer_charge;

                        $total_amount = $total_amount + $total_price;
                        $total_customer_charge = $total_customer_charge + $customer_charge;
                        $total_amount_credits = $total_amount_credits + $credits_val;

                        /////////////////////////////////////// delivery order line items //////////////////////////////////////////////
                        $product['product_type'] = $type;
                        $cond2['id_line_items'] = $line_item['id_line_items'];
                        $protuct_quantites = $this->admin_fct->getProductsQuantityPaidStatus($product, $cond2);
                        $qty_sold_by_admin = $protuct_quantites['qty_sold_by_admin'];
                        $qty_payable = $protuct_quantites['qty_payable'];

                        $line_item['qty_sold_by_admin'] = $qty_sold_by_admin;
                        $line_item['qty_payable'] = $qty_payable;

                        $total_qty_sold_by_admin = $total_qty_sold_by_admin + $qty_sold_by_admin;
                        $total_qty_payable = $total_qty_payable + $qty_payable;
                        $total_brand_price = $total_brand_price + $total_price;
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        $items[$j] = $line_item;
                        $items[$j]['order'] = $order;
                        $items[$j]['product'] = $product;

                        $j++;

                    }
                }

                $arr[$i]['total_brand_price'] = $total_brand_price;


                $arr[$i]['line_items'] = $items;

                $i++;
            }
        }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $results['brands'] = $arr;
        $results['currency'] = $default_currency;
        $results['total_amount_credits'] = $total_amount_credits;
        $results['total_customer_charge'] = $total_customer_charge;
        $results['total_amount'] = $total_amount;
        $results['net_price'] = $total_amount - $total_amount_credits + $total_customer_charge;

        $results['status'] = $cond['status'];
        $results['from_date'] = $cond['from_date'];
        $results['to_date'] = $cond['to_date'];
        if (isset($cond['supplier'])) {
            $results['id_user'] = $cond['supplier'];
        } else {
            $results['id_user'] = "";
        }

        return $results;


    }

    public function getInvoices($cond, $limit = '', $offset = '')
    {

        $data = array();
        $q = " SELECT invoices.* from invoices ";

        $q .= " WHERE invoices.deleted = 0 ";
        if (isset($cond['supplier']) && !empty($cond['supplier'])) {
            $q .= "  AND invoices.id_user = " . $cond['supplier'];
        }


        if (isset($cond['invoice']) && !empty($cond['invoice'])) {
            $q .= "  AND invoices.id_invoices = " . $cond['invoice'];
        }


        if (isset($cond['status']) && !empty($cond['status'])) {
            $q .= "  AND invoices.status = '" . $cond['status'] . "'";
        }

        if (isset($cond['from_date']) && !empty($cond['from_date'])) {
            $q .= " and invoices.created_date>='" . $cond['from_date'] . "'";
        }

        if (isset($cond['to_date']) && !empty($cond['to_date'])) {
            $q .= " and invoices.created_date<='" . $cond['to_date'] . "'";
        }


        $q .= "  order by id_invoices desc ";
        if ($limit != '') {
            $q .= "  limit " . $limit . " offset 0 ";
        }


        $query = $this->db->query($q);
        if ($limit == '') {
            $data = $query->num_rows();
        } else {

            $results = $query->result_array();
            $data = array();
            $i = 0;
            foreach ($results as $res) {
                $user = "";
                if (isset($res['id_user']) && !empty($res['id_user'])) {
                    $user = $this->ecommerce_model->getUserInfo($res['id_user']);
                }
                $data[$i] = $res;
                $data[$i]['user'] = $user;
                $i++;
            }
        }

        return $data;

    }

    public function getInvoice($cond)
    {

        $invoice = $this->fct->getonerecord('invoices', array('id_invoices' => $cond['id_invoices'], 'rand' => $cond['rand']));
        $user = $this->fct->getonerecord('user', array('id_user' => $invoice['id_user']));

        if (!empty($invoice)) {
            $sqla = " SELECT brands.* FROM invoices_line_items ";
            $sqla .= " join brands on invoices_line_items.id_brands=brands.id_brands ";
            $sqla .= " where invoices_line_items.id_invoices=" . $cond['id_invoices'];
            $sqla .= " group by brands.id_brands order by invoices_line_items.id_invoices_line_items  ";
            $query = $this->db->query($sqla);
            $brands = $query->result_array();

            $i = 0;
            $arr = array();
            foreach ($brands as $brand) {
                $sqla = " SELECT invoices_line_items.* FROM invoices_line_items ";
                $sqla .= " where id_brands=" . $brand['id_brands'] . ' and id_invoices=' . $cond['id_invoices'];

                $query = $this->db->query($sqla);
                $line_items = $query->result_array();
                $k = 0;
                foreach ($line_items as $line_item) {

                    $product = $this->fct->getonerecord('products', array('id_products' => $line_item['id_products']));
                    $items[$k] = $line_item;
                    $items[$k]['product'] = $product;
                    $k++;
                }
                $arr[$i] = $brand;
                $arr[$i]['line_items'] = $items;
                $i++;
            }

            $data['invoice'] = $invoice;


            $data['user'] = $user;
            $data['brands'] = $arr;

        } else {
            redirect(site_url('inventory/invoices'));
        }

        return $data;

    }

//public function getInventory($cond, $limit = '', $offset = '', $order = 'products.sort_order', $desc = "asc"){
//
//        $data = array();
//        $q    = "";
//
//        $q .= "select products.* ";
//
//        if (isset($cond['category'])) {
//            $q .= ", products_categories.id_categories as id_categories ";
//        }
//
//        $q .= " from products ";
//        //print_r($product_options);exit;
//
//		if (isset($cond['category']) || isset($cond['categories'])) {
//            $q .= " ".$left." join products_categories on products_categories.id_products = products.id_products ";
//        }
//
//        if (isset($cond['options'])) {
//            $q .= " ".$left." join product_options on products.id_products = product_options.id_products ";
//        }
//
//        $q .= " where products.deleted=0 ";
//
//		if(!isset($cond['admin'])){
//		$q .= " and products.status=0 and products.published=0";}
//
//        if (isset($cond['title'])) {
//            /*$this->db->like('UPPER(products.title'.getFieldLanguage().')',strtoupper($cond['products.title'.getFieldLanguage()]));*/
//            $q .= " and UPPER(products.title) like '%".strtoupper($cond['title'])."%' ";
//            unset($cond['title']);
//        }
//
//		if (isset($cond['sku'])) {
//			 $q .= " and UPPER(products.sku) like '%".strtoupper($cond['sku'])."%' ";
//     	 }
//
//	  	if (isset($cond['id_brands'])) {
//
//			 $q .= " and products.id_brands=".$cond['id_brands'];
//      }
//
//        if (isset($cond['options'])) {
//            $q .= " and product_options.deleted=0  and product_options.status=1";
//        }
//
//		if (isset($cond['id_products'])) {
//            $q .= " and products.id_products=".$cond['id_products'];
//        }
//
//
//
//		 if (isset($cond['status'])) {
//		 $q .= "  AND  products.status=".$cond['status'];
//
//
//        }
//
//
//		if (isset($cond['miles'])) {
//		$q .= "  AND  products.id_miles!=0 and products.id_miles!='' ";
//		}
//
//
//		if (isset($cond['category'])) {
//
//            $q .= " and products_categories.id_categories=" . $cond['category'];
//        }
//
//		if (isset($cond['brand'])) {
//            $q .= " and products.id_brands=" . $cond['brand'];
//        }
//
//
//        $q .= "  group by products.id_products ";
//        $q .= "  order by " . $order . ' ' . $desc;
//
//
//
//      /*  if ($limit != '') {
//
//            $q .= "  limit " . $limit . " offset " . $offset;
//        }*/
//
//
//        $query = $this->db->query($q);
//
//
//
//
//
//            $results = $query->result_array();
//
//
//                $j = 0;
//				$count=0;
//				$k=0;
//
//				$max_c=$offset+$limit;
//
//                foreach ($results as $res) {
//					$k++;
//
//
//				if(($max_c>=$k && $offset<=$k) || $limit==""){
//
//                    $count++;
//					$data[$j]=$res;
//
//                    $attributes               = $this->getProductAttributes($res['id_products']);
//					$data[$j]['attributes']               = $attributes;
//
//
//
//
//
//					$miles      = $this->fct->getonerecord('miles',array('id_miles'=>$res['id_miles']));
//                    $data[$j]['miles']      = $miles;
//
//
//
//					$cond_inventory_2['id_products']=$res['id_products'];
//					$cond_inventory_2['type']='product';
//					$purchases      = $this->getPurchases($cond_inventory_2);
//
//					$data[$j]['type']="product";
//					$data[$j]['qty_sold_by_admin']          = $purchases['qty_sold_by_admin'];
//                    $data[$j]['qty_payable']      = $purchases['qty_payable'];
//
//
//                    // get stock
//
//
//
//                    $j++;
//
//					$products_stocks=$this->fct->getAll_cond('products_stock','sort_order',array('id_products'=>$res['id_products']));
//
//					foreach($products_stocks as $products_stock){
//						$k++;
//						if(($max_c>=$k && $offset<=$k) || $limit==""){
//				   	$data[$j]['type']="stock";
//				    $data[$j]        = $products_stock;
//
//					$cond_inventory_1['id_products']=$products_stock['id_products_stock'];
//					$cond_inventory_1['type']='stock';
//					$purchases_s      = $this->getPurchases($cond_inventory_1);
//					$data[$j]['qty_sold_by_admin'] = $purchases_s['qty_sold_by_admin'];
//                    $data[$j]['qty_payable']      = $purchases_s['qty_payable'];
//					$j++;}
//					 }
//
//
//        } }
//		if ($limit == '') {
//
//            $data = $count;
//        }
//
//		 return $data;
//
//	 }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getProductClassifications($id_products)
    {
        // get attributes
        $this->db->select('classifications.*');
        $this->db->from('classifications');
        $this->db->join('products_classifications', 'products_classifications.id_classifications = classifications.id_classifications');
        $cnd = array(
            'products_classifications.id_products' => $id_products
        );
        $this->db->where($cnd);
        $this->db->group_by('classifications.id_classifications');
        $this->db->order_by('classifications.sort_order');
        $query = $this->db->get();
        $categories = $query->result_array();
        return $categories;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    /*    public function getProductoptions($id_products)
    {
        // get attributes
        $this->db->select('attribute_options.*');
        $this->db->from('product_options');
        $this->db->join('attribute_options', 'attribute_options.id_attribute_options = product_options.id_attribute_options');
        $this->db->join('attributes', 'attributes.id_attributes = product_options.id_attributes');
        $cnd = array(
            'product_options.id_products' => $id_products
        );
        $this->db->where($cnd);
        $this->db->group_by('product_options.id_product_options');
        $this->db->order_by('attribute_options.sort_order');
        $query           = $this->db->get();
        $product_options = $query->result_array();

        return $product_options;
    }*/


    public function getProductOptions($id_products)
    {
        // get attributes
        $this->db->select('attributes.*');
        $this->db->from('product_attributes');
        $this->db->join('attributes', 'product_attributes.id_attributes = attributes.id_attributes');

        $cnd = array(
            'product_attributes.id_products' => $id_products,
            'attributes.deleted' => 0,
            'product_attributes.deleted' => 0,
        );
        $this->db->where($cnd);
        $this->db->group_by('attributes.id_attributes');
        $this->db->order_by('attributes.sort_order');
        $query = $this->db->get();
        $attributes = $query->result_array();
        if (!empty($attributes)) {
            $k = 0;
            foreach ($attributes as $attr) {
                // get product attribute options
                $this->db->select('attribute_options.*');
                $this->db->from('attribute_options');
                $this->db->join('product_options', 'product_options.id_attribute_options = attribute_options.id_attribute_options');
                $cnd = array(
                    'product_options.id_products' => $id_products,
                    'attribute_options.id_attributes' => $attr['id_attributes'],
                    'product_options.status' => 1
                );
                $this->db->where($cnd);
                $this->db->group_by('attribute_options.id_attribute_options');
                $this->db->order_by('attribute_options.sort_order');
                $query = $this->db->get();
                $options = $query->result_array();
                $attributes[$k]['options'] = $options;

                $k++;
            }
        }
        return $attributes;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getUserFavorites($id_user, $limit = "", $id_category = 0, $type = "")
    {
        $data = array();
        $this->db->select('products.*,favorites.options, favorites.id_favorites, favorites.qty, favorites.qty_month');
        $this->db->select('products.*,favorites.options');
        $this->db->from('favorites');
        $this->db->join('products', 'favorites.id_products = products.id_products');
        $cond = array(
            'favorites.id_user' => $id_user
        );
        $this->db->where($cond);
        if ($type == "unselect") {
            $this->db->where('favorites.id_favorites NOT IN (select id_favorites from user_checklist_categories where  id_user=' . $id_user . ' and id_checklist_categories=' . $id_category . ')');
        }

        if ($type == "select") {
            $this->db->where('favorites.id_favorites IN (select id_favorites from user_checklist_categories where  id_user=' . $id_user . ' and id_checklist_categories=' . $id_category . ')');
        }

        $this->db->group_by('favorites.id_favorites');
        $this->db->order_by('products.sort_order');

        if ($limit != "")
            $this->db->limit($limit, $offset);

        $query = $this->db->get();
        $results = $query->result_array();
        if (!empty($results)) {
            $i = 0;
            foreach ($results as $res) {


                // get gallery
                $this->db->select('*');
                $this->db->from('products_gallery');
                $cond = array(
                    'id_products' => $res['id_products']
                );
                $this->db->where($cond);
                $this->db->order_by('sort_order');
                $query = $this->db->get();
                $gallery = $query->result_array();
                // get options
                $options = array();

                $opts = array();

                if (!empty($res['options'])) {
                    $opts = explode(',', $res['options']);
                }

                if (!empty($opts)) {
                    foreach ($opts as $opt) {
                        $dd = $this->getCartProductOption($opt);
                        array_push($options, $dd);
                    }
                }
                $stock = array();
                if (!empty($opts)) {
                    $results = $this->ecommerce_model->getOptionsByIds($opts);
                    $combination = serializecartOptions($results);
                    //echo $combination;exit;
                    $stock = $this->fct->getonerow('products_stock', array('id_products' => $res['id_products'], 'combination' => $combination));
                }


                $data[$i] = $res;
                $data[$i]['gallery'] = $gallery;
                $data[$i]['cart_options'] = $options;
                $data[$i]['product_stock'] = $stock;


                $product_attributes_options = array();
                if (!empty($options)) {
                    $k = 0;
                    foreach ($options as $opt) {
                        $attribute = $this->fct->getonerecord('attributes', array('id_attributes' => $opt['id_attributes']));
                        $product_attributes_options[$k]['id_attribute_options'] = $opt['id_attribute_options'];
                        $product_attributes_options[$k]['option_title'] = $opt['title'];
                        $product_attributes_options[$k]['option'] = $opt['option'];
                        $product_attributes_options[$k]['id_attribute'] = $opt['id_attributes'];
                        $product_attributes_options[$k]['attribute_name'] = $attribute['title'];
                        $k++;
                    }
                }

                $data[$i]['product_attributes_options'] = $product_attributes_options;

                $i++;
            }
        }
        return $data;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getUserProductsFavoritesIds()
    {

        $data = array();
        $this->db->select('products.id_products');
        $this->db->from('products');
        $this->db->join('favorites', 'favorites.id_products = products.id_products');
        $cond = array(
            'favorites.id_user' => $this->session->userdata('login_id')
        );
        $this->db->where($cond);
        $this->db->group_by('favorites.id_products');
        $this->db->order_by('products.sort_order');
        $query = $this->db->get();
        $results = $query->result_array();
        $processed = array_map(function ($a) {
            return array_pop($a);
        }, $results);
        return $processed;

    }

    public function checkIfInFavorites($id_product, $productsFavorites)
    {
        if (in_array($id_product, $productsFavorites)) {
            return true;
        } else {
            return false;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getNotSelectedAttributes_byProduct($id_product)
    {
        $q = 'SELECT * FROM attributes WHERE deleted = 0 AND id_attributes NOT IN(SELECT id_attributes FROM product_attributes WHERE id_products = ' . $id_product . ')';
        $query = $this->db->query($q);
        $result = $query->result_array();
        return $result;
    }

    public function getSelectedAttributes_byProduct($id_product)
    {
        $q = 'SELECT attributes.*, product_attributes.id_product_attributes FROM product_attributes';
        $q .= ' JOIN attributes ON attributes.id_attributes = product_attributes.id_attributes';
        $q .= ' WHERE product_attributes.id_products = ' . $id_product;
        $q .= ' GROUP BY product_attributes.id_attributes';
        $q .= ' ORDER BY attributes.sort_order';
        $query = $this->db->query($q);
        $result = $query->result_array();
        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getAllOptions_byProductAttribute($id_product, $id_attribute)
    {
        $data = array();
        $this->db->select('*');
        $this->db->from('attribute_options');
        $cond = array(
            'id_attributes' => $id_attribute
        );
        $this->db->where($cond);
        $this->db->order_by('sort_order');
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $res) {
                $this->db->select('*');
                $this->db->from('product_options');
                $cond1['id_attributes'] = $id_attribute;
                $cond1['id_products'] = $id_product;
                $cond1['id_attribute_options'] = $res['id_attribute_options'];
                $this->db->where($cond1);
                $query1 = $this->db->get();
                $selected_option = $query1->row_array();

                $data[$i] = $res;
                $data[$i]['selected_option'] = $selected_option;
                $i++;
            }
        }
        //print '<pre>';print_r($data);exit;
        return $data;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getAttributeByOption($id_option)
    {
        $this->db->select('attributes.*');
        $this->db->from('attributes');
        $this->db->join('attribute_options', 'attribute_options.id_attributes = attributes.id_attributes');
        $cond = array(
            'attribute_options.id_attribute_options' => $id_option
        );
        $this->db->where($cond);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $query->row_array();
        return $result;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getCartProductOption($id_option)
    {
        $this->db->select('attributes.title AS attr_title,attributes.label AS attr_label,attributes.label AS attr_label_en,attribute_options.*');
        $this->db->from('attributes');
        $this->db->join('attribute_options', 'attribute_options.id_attributes = attributes.id_attributes');
        $cond = array(
            'attribute_options.id_attribute_options' => $id_option
        );
        $this->db->where($cond);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $query->row_array();
        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getOptionsByIds($ids)
    {
        $q = 'SELECT attribute_options.* FROM attribute_options JOIN attributes ON attributes.id_attributes = attribute_options.id_attributes WHERE attribute_options.id_attribute_options IN (' . implode(',', $ids) . ') GROUP BY attribute_options.id_attribute_options ORDER BY attributes.sort_order';
        $query = $this->db->query($q);
        $results = $query->result_array();
        return $results;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getAttributeOption($id_attribute = "", $ids = "", $id_product = "")
    {

        /*	if($id_attribute==2){
        $q = 'SELECT product_options.*, attribute_options.title as title  FROM product_options join attribute_options on product_options.id_attribute_options= attribute_options.id_attribute_options  WHERE attribute_options.id_attributes='.$id_attribute.' and attribute_options.id_attribute_options IN ('.implode(',',$ids).') and product_options.id_products='.$id_product;}else{*/
        $q = 'SELECT * FROM  attribute_options WHERE attribute_options.id_attributes=' . $id_attribute . ' and attribute_options.id_attribute_options IN (' . implode(',', $ids) . ')';
        /*	}*/


        $query = $this->db->query($q);
        $results = $query->row_array();
        return $results;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function UpdateProductStockInDatabase($id_products, $defaultQty, $oldQty, $newQty, $combination = '')
    {
        if ($newQty > $oldQty) {
            $updatedQty = $defaultQty - ($newQty - $oldQty);
        } elseif ($newQty < $oldQty) {
            $updatedQty = $defaultQty + ($oldQty - $newQty);
        } else {
            $updatedQty = $defaultQty;
        }
        //echo $updatedQty;exit;
        if ($combination != '') {
            $q = 'UPDATE products_stock SET quantity = ' . $updatedQty . ' WHERE id_products = ' . $id_products . ' AND combination = "' . $combination . '"';
            //echo $q;exit;
            $this->db->query($q);

            $stock = $this->fct->getonerecord('products_stock', array('id_products' => $id_products, 'combination' => $combination));
            $product = $this->fct->getonerecord('products', array('id_products' => $id_products));
        } else {
            $q = 'UPDATE products SET quantity = ' . $updatedQty . ' WHERE id_products = ' . $id_products;
            //echo $q;exit;
            $this->db->query($q);
            $stock = array();
            $product = $this->fct->getonerecord('products', array('id_products' => $id_products));

        }


        return TRUE;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function UpdateProductStockInDatabase2($id_line_item, $admin = 0)
    {
        $stock_out_quantity = 0;
        $lineItem = $this->fct->getonerecord('line_items', array('id_line_items' => $id_line_item));
        if ($lineItem['group_products'] == 1) {
            $line_item = $this->fct->getonerecord('group_products_line_items', array('id_group_products_line_items' => $id_line_item));

            $combination = $line_item['options'];
            $line_item['id_line_items'] = $line_item['id_group_products_line_items'];
        } else {
            $line_item = $this->fct->getonerecord('line_items', array('id_line_items' => $id_line_item));
            $combination = $line_item['options_en'];
        }
        $line_item['admin'] = 1;
        $newQty = $lineItem['quantity'];
        $id_products = $lineItem['id_products'];
        //echo $updatedQty;exit;
        if ($combination != '') {
            $stock = $this->fct->getonerecord('products_stock', array('id_products' => $id_products, 'combination' => $combination));
            $updatedQty = $stock['quantity'] - $newQty;
            if ($updatedQty < 0) {
                $updatedQty = 0;
            }

            $q = 'UPDATE products_stock SET quantity = ' . $updatedQty . ' WHERE id_products = ' . $id_products . ' AND combination = "' . $combination . '"';
            //echo $q;exit;
            $this->db->query($q);

            $stock = $this->fct->getonerecord('products_stock', array('id_products' => $id_products, 'combination' => $combination));
            $product = $this->fct->getonerecord('products', array('id_products' => $id_products));
            $stock_status = $product['stock_status'];
        } else {

            $product = $this->fct->getonerecord('products', array('id_products' => $id_products));
            $stock_status = $product['stock_status'];
            $updatedQty = $product['quantity'] - $newQty;

            if ($updatedQty < 0) {
                $updatedQty = 0;
            }


            $q = 'UPDATE products SET quantity = ' . $updatedQty . ' WHERE id_products = ' . $id_products;
            if ($lineItem['group_products'] == 1 && $id_products == 1260) {
                echo $q;
                exit;
            }
            $this->db->query($q);
            $stock = array();
            $product = $this->fct->getonerecord('products', array('id_products' => $id_products));

        }

        if ($admin != 1) {
            $line_item['options_en'] = $combination;
            $line_item['group_products'] = $lineItem['group_products'];
            $line_item['id_products'] = $lineItem['id_products'];
            $line_item['quantity'] = $lineItem['quantity'];
            $line_item['id_orders'] = $lineItem['id_orders'];
            $this->admin_fct->updateDeliverOrders($line_item);
        }
        return TRUE;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function UpdateQuantityInStock($id_products, $combination, $qty)
    {
        $q = 'UPDATE products_stock SET quantity = quantity - ' . $qty . ' WHERE id_products = ' . $id_products . ' AND combination = "' . $combination . '"';
        $this->db->query($q);
        return TRUE;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function UpdateQuantityInProducts($id_products, $qty)
    {
        $q = 'UPDATE products SET quantity = quantity - ' . $qty . ' WHERE id_products = ' . $id_products;
        $this->db->query($q);
        return TRUE;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function loadUserCart($cond = array())
    {
        $cart_items = array();
        if ($this->session->userdata('login_id') != '') {
            $id_user = $this->session->userdata('login_id');
            $q = 'DELETE FROM shopping_cart WHERE id_user = ' . $id_user . ' AND DATE_FORMAT(created_date, "%Y-%m-%d") < "' . date('Y-m-d', strtotime(date('Y-m-d') . ' - 120 days')) . '"';
            $query = $this->db->query($q);
            $cond['id_user'] = $id_user;
        } else {
            $cond['sessionid'] = $this->session->userdata('session_id');
        }
        if (isset($cond['redeem'])) {
            if ($cond['redeem'] == 1) {
                $cond['redeem_miles !='] = "";
            }

            if ($cond['redeem'] == 2) {
                $cond['redeem_miles'] = "";
            }

        }


        $shopping_cart = $this->fct->getAll_cond('shopping_cart', 'id_shopping_cart', $cond);


        foreach ($shopping_cart as $cart) {

            $data = array();
            $data['rowid'] = $cart['rowid'];
            $data['id'] = $cart['id_products'];
            $data['qty'] = $cart['qty'];
            $data['price'] = $cart['price'];
            $data['name'] = $cart['name'];
            $data['sub_total'] = $cart['sub_total'];


            $options = array();
            if (!empty($cart['options'])) {
                $dd = unSerializeStock($cart['options']);
                foreach ($dd as $kk => $d)
                    array_push($options, $kk);
                //$dd = explode(',',$cart['options']);
            }

            $options['redeem_miles'] = $cart['redeem_miles'];
            $options['miles'] = $cart['miles'];
            $data['options'] = $options;


            $this->cart->insert($data);
        }
        $cart_items = $this->cart->contents();


        return $cart_items;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function exportQuotationToCart($id_order, $rand)
    {
        $this->cart->destroy();
        $cart_items = array();
        if ($this->session->userdata('login_id') != '') {
            $id_user = $this->session->userdata('login_id');
            /*$q = 'DELETE FROM shopping_cart WHERE id_user = '.$id_user.' AND DATE_FORMAT(created_date, "%Y-%m-%d") < "'.date('Y-m-d', strtotime(date('Y-m-d'). ' - 14 days')).'"';*/
            $q = 'DELETE FROM shopping_cart WHERE id_user = ' . $id_user;
            $query = $this->db->query($q);
            $this->cart->destroy();
            $quotation = $this->ecommerce_model->getQuotation($id_order, array('rand' => $rand));

            $shopping_cart = $quotation['quotation_line_items'];
            foreach ($shopping_cart as $cart) {
                $data = array();

                $data['id'] = $cart['id_products'];
                $data['qty'] = $cart['quantity'];
                $data['price'] = changeCurrency($cart['price'], 0, "AED", $quotation['currency']);
                $type = "product";
                $data['sub_total'] = $data['qty'] * $data['price'];

                $options_p = array();
                if (!empty($cart['options'])) {
                    $type = "option";
                    $dd = unSerializeStock($cart['options']);
                    foreach ($dd as $kk => $d)
                        array_push($options_p, $kk);
                    //$dd = explode(',',$cart['options']);
                }
                $data['name'] = $type;
                $options['redeem_miles'] = $cart['redeem_miles'];
                $options['miles'] = $cart['miles'];
                $options['product_options'] = $options_p;
                $options['id_brands'] = $cart['id_brands'];
                $data['options'] = $options;

                $newrowid = $this->cart->insert($data);

                // insert in database: shopping cart
                unset($data['id']);
                $data['rowid'] = $newrowid;
                $data['id_user'] = $id_user;
                $data['options'] = $cart['options'];
                $data['id_products'] = $cart['id_products'];


                $this->db->insert('shopping_cart', $data);

            }
            $this->session->set_userdata('currency', $quotation['currency']);
            $cart_items = $this->cart->contents();
        }

        return $cart_items;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function checkPriceSegment($id_products, $id_stock, $qty)
    {
        $p = '';
        $q = 'SELECT * FROM product_price_segments WHERE id_products = ' . $id_products . ' AND id_stock = ' . $id_stock . ' HAVING min_qty <= ' . $qty . ' order by min_qty desc';

        //echo $q.'<br />';
        $query = $this->db->query($q);
        $result = $query->row_array();
        if (!empty($result))
            $p = $result['price'];
        return $p;
    }

    public function getDiscountByCode($code)
    {
        $q = 'SELECT discount_groups.* FROM discount_groups WHERE discount_groups.deleted = 0 AND discount_groups.code = "' . $code . '" AND (expiry_date = "" OR expiry_date = "0000-00-00" OR expiry_date >= "' . date('Y-m-d') . '")';
        $query = $this->db->query($q);
        $res = $query->row_array();
        return $res;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    public function getProductsTabs()
    {
        $q = 'SELECT * FROM dynamic_pages WHERE id_dynamic_pages IN(14,15,16) ORDER BY id_dynamic_pages';
        $query = $this->db->query($q);
        $results = $query->result_array();
        return $results;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    public function getCategoriesBySub($id_sub)
    {
        $q = 'SELECT categories.* FROM categories JOIN categories_sub_categories ON categories_sub_categories.id_categories = categories.id_categories WHERE categories.deleted = 0 AND categories_sub_categories.id_categories_sub = ' . $id_sub . ' GROUP BY categories.id_categories ORDER BY categories.sort_order';
        $query = $this->db->query($q);
        $results = $query->result_array();
        return $results;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    public function getUserComparedProducts($id_user = 0, $session_id = '')
    {
        $ids = array();
        //if($this->session->userdata('login_id') != "") {
        //$id_user = $this->session->userdata('login_id');
        $q = 'SELECT * FROM compare_products WHERE session_id = "' . $session_id . '"';
        if ($id_user != 0)
            $q .= ' OR id_user = ' . $id_user;
        $query = $this->db->query($q);
        $result = $query->result_array();
        if (!empty($result)) {
            foreach ($result as $res) {
                if (!in_array($res['id_products'], $ids))
                    array_push($ids, $res['id_products']);
            }
        }
        //}
        return $ids;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generateInvoiceTemplate($orderData)
    {

        $condition1 = array(
            'id_settings' => 1
        );
        $admindata = $this->fct->getonerow('settings', $condition1);
        $adminemail = $admindata['email'];
        $adminname = $admindata['website_title'];
        $dir = 'ltr';
        $logo = '';
        if ($orderData['lang'] == 'ar') {
            $dir = 'rtl';
            $logo = '-rtl';
        }
        $body = '';
        $body .= '<table dir="' . $dir . '" width="95%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#cbba88" style="font-family: verdana, arial, helvetica; font-size: small;">';
        $body .= '<tr>';
        $body .= '<td>';
        $body .= '<table width="100%" border="0" cellspacing="2" cellpadding="5" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: small;">';
        $body .= '<tr valign="top">';
        $body .= '<td>';
        $body .= '<table width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">';
        $body .= '<tr>';
        $body .= '<td align="center">';
        $body .= '<a href="' . site_url() . '"><img src="' . base_url() . 'front/img/logo' . $logo . '.png" alt="' . $adminname . '" title="' . $adminname . '" />';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '<tr><td>&nbsp;</td></tr>';
        $body .= '<tr valign="top">';
        $body .= '<td colspan="">';
        $body .= '<p><b>' . lang('thank_you_for_your_order') . ' ' . $orderData['user']['first_name'] . ' ' . $orderData['user']['last_name'] . '</b></p>';
        $link = site_url();
        $body .= '<p><b>' . lang('want_to_manage_order_online') . '</b><br />' . $this->lang->line('to_check_status', $adminname, $link) . '</p>';
        $body .= '<table cellpadding="4" cellspacing="0" border="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">';
        $body .= '<tr>';
        $body .= '<td colspan="4" bgcolor="#cbba88" style="color: white;">';
        $body .= '<b>' . lang('purchasing_information') . '</b>';
        $body .= '</td>';
        $body .= '</tr>';
        /*	$body .= '<tr>';
        $body .= '<td nowrap="nowrap">';
        $body .= '<b>'.lang('email').'</b>';
        $body .= '</td>';
        $body .= '<td width="98%"><a href="mailto:'.$orderData['user']['email'].'">'.$orderData['user']['email'].'</a></td>';
        $body .= '</tr>';*/
        $body .= '<tr>';
        $body .= '<td colspan="2" >';
        $body .= '<table width="100%" cellspacing="0" cellpadding="0" style="font-family: verdana, arial, helvetica; font-size: small;">';
        $body .= '<tr>';

        $body .= '<td  valign="top" width="50%">';
        $body .= '<table border="0" cellpadding="1" cellspacing="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">';
        $body .= '<tr>';
        $body .= '<td nowrap="nowrap">';
        $body .= '<b>' . lang('order_id') . '</b>';
        $body .= '</td>';
        $body .= '<td width="98%">' . $orderData['id_orders'] . '</td>';
        $body .= '</tr>';
        $body .= '<tr>';
        $body .= '<td nowrap="nowrap">';
        $body .= '<b>' . lang('created_date') . '</b>';
        $body .= '</td>';
        $body .= '<td width="98%">' . $orderData['created_date'] . '</td>';
        $body .= '</tr>';
        $body .= '<tr>';
        $body .= '<td nowrap="nowrap"><b>' . lang('cart_total_price') . '</b></td>';
        $body .= '<td width="98%">' . $orderData['total_price_currency'] . ' ' . lang($orderData['currency']) . '</td>';
        $body .= '</tr>';
        $body .= '<tr>';
        $body .= '<td nowrap="nowrap"><b>' . lang('discount') . '</b></td>';
        $body .= '<td width="98%">' . $orderData['discount'] . ' ' . lang($orderData['currency']) . '</td>';
        $body .= '</tr>';
        $body .= '<tr>';
        $body .= '<td nowrap="nowrap"><b>' . lang('amount') . '</b></td>';
        $body .= '<td width="98%">' . $orderData['amount_currency'] . ' ' . lang($orderData['currency']) . '</td>';
        $body .= '</tr>';
        if ($orderData['default_currency'] != $orderData['currency']) {
            /*	$body .= '<tr>';
            $body .= '<td nowrap="nowrap"><b>'.lang('amount').' in '.$orderData['default_currency'].'</b></td>';
            $body .= '<td width="98%">'.$orderData['amount'].' '.lang($orderData['default_currency']).'</td>';
            $body .= '</tr>';*/
        }
        $body .= '<tr>';
        $body .= '<td nowrap="nowrap"><b>' . lang('status') . '</b></td>';
        $body .= '<td width="98%">' . lang($orderData['status']) . '</td>';
        $body .= '</tr>';
        if (!empty($orderData['comments'])) {
            $body .= '<tr>';
            $body .= '<td nowrap="nowrap"><b>Comments</b></td>';
            $body .= '<td width="98%">' . $orderData['comments'] . '</td>';
            $body .= '</tr>';
        }
        $body .= '<tr>';
        $body .= '<td nowrap="nowrap" style="padding-right:10px;">';
        $body .= '<b>' . lang('payment_method') . '</b>';
        $body .= '</td>';
        $body .= '<td width="98%">' . $orderData['payment_method'] . '</td>';
        $body .= '</tr>';
        if ($orderData['status'] == 'paid') {
            $body .= '<td nowrap="nowrap">';
            $body .= '<b>' . lang('payment_date') . '</b>';
            $body .= '</td>';
            $body .= '<td width="98%">' . $orderData['payment_date'] . '</td>';
            $body .= '</tr>';
        }

        $body .= '</table>';
        $body .= '</td>';

        $delivery_day = getDeliveryDay();

        $body .= '<td  valign="top" width="50%">';

        $body .= '<b>' . lang('delivery_information') . '</b><br />
	Name: ' . $orderData['delivery']['first_name'] . ' ' . $orderData['delivery']['last_name'] . '<br />
	E-mail: <a href="mailto:' . $orderData['delivery']['email'] . '">' . $orderData['delivery']['email'] . '</a><br />
	Phone: ' . $orderData['delivery']['phone'] . '<br />
	Company: ' . $orderData['delivery']['company'] . '<br />
	Postal Code: ' . $orderData['delivery']['postal_code'] . '<br />
	Country: ' . $orderData['delivery']['country']['title'] . '<br />
	State: ' . $orderData['delivery']['state'] . '<br />
	City: ' . $orderData['delivery']['city'] . '<br />
	Street Address: ' . $orderData['delivery']['street_address'] . '<br />
	</td>';
        $body .= '</tr>';
        if ($this->session->userdata('account_type') == "retail") {
            if ($delivery_day != 0) {
                $body .= '<tr><td colspan="2"> <span style="float:left;width:100%;color:#C00;margin-bottom:8px;font-size:13px">
	   Order will be delivered within ' . $delivery_day . ' day(s). </span></td></tr>';
            }
        } else {
            $body .= ' <tr><td colspan="2"><span style="float:left;width:100%;color:#C00;margin-bottom:8px;font-size:13px">' . lang('we_will_contact_us') . '</span></td></tr>';
        }
        $body .= '</table>';
        $body .= '</td>';
        $body .= '</tr>';
        /*	$body .= '<tr>';
        $body .= '<td nowrap="nowrap">';
        $body .= '<b>'.lang('cart_net_price').'</b>';
        $body .= '</td>';
        $body .= '<td width="98%">'.changeCurrency($orderData['amount']).'</td>';
        $body .= '</tr>';*/

        $body .= '<tr><td>&nbsp;</td></tr>';
        $body .= '<tr>';
        $body .= '<td colspan="2" bgcolor="#cbba88" style="color: white;">';
        $body .= '<b>' . lang('order_details') . '</b>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '<tr>';
        $body .= '<td colspan="2">';
        $body .= '<table border="0" cellpadding="1" cellspacing="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">';
        $body .= '<tr>';
        $body .= '<td colspan="2">';
        $body .= '<br /><br /><b>' . lang('products_on_order') . '&nbsp;</b>';
        $body .= '<table width="100%" border="1" style="font-family: verdana, arial, helvetica; font-size: small; border:1px solid #cbba88">';
        $products = $orderData['line_items'];
        $body .= '<tr>';
        $body .= '<td><b>' . lang('cart_product_name') . '</b></td>';
        $body .= '<td><b>' . lang('cart_product_price') . '</b></td>';
        $body .= '<td><b>' . lang('cart_product_quantity') . '</b></td>';
        $body .= '<td><b>' . lang('cart_total_price') . '</b></td>';
        $body .= '</tr>';

        foreach ($products as $product) {
            $product_name = '';
            if (empty($product['product']['sku']))
                $product_name .= $product['product']['title' . getFieldLanguage()];
            else
                $product_name .= $product['product']['sku'] . ', ' . $product['product']['title' . getFieldLanguage()];
            if (!empty($product['options_en'])) {
                $options = $product['options_en'];
                $options = unSerializeStock($options);
                $product_name .= '<br />';
                $c = count($options);
                $i = 0;
                foreach ($options as $key => $opt) {
                    $i++;
                    $product_name .= $opt;
                    if ($i != $c)
                        $product_name .= ' - ';
                }
            }
            $body .= '<tr>';
            $body .= '<td>' . $product_name . '</td>';
            $body .= '<td>' . $product['price_currency'] . ' ' . lang($product['currency']) . '</td>';
            $body .= '<td>' . $product['quantity'] . '</td>';
            $body .= '<td>' . $product['total_price_currency'] . ' ' . lang($product['currency']) . '</td>';
            $body .= '</tr>';
        }
        $body .= '</table>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '<tr>';
        $body .= '<td colspan="2">';
        $body .= '<hr noshade="noshade" size="1" /><br />';
        $body .= '<p>' . lang('order_email_note') . '</p>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        return $body;
    }
}
